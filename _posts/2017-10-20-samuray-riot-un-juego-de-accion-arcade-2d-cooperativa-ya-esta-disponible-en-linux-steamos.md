---
author: Pato
category: Arcade
date: 2017-10-20 17:44:13
excerpt: "<p>Luchas samurais dentro de una historia inmersiva y personajes carism\xE1\
  ticos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a5cfeb7eb858a454222c6a713718c016.webp
joomla_id: 498
joomla_url: samuray-riot-un-juego-de-accion-arcade-2d-cooperativa-ya-esta-disponible-en-linux-steamos
layout: post
tags:
- accion
- arcade
- beat-em-up
title: "'Samurai Riot' un juego de acci\xF3n arcade 2D cooperativa ya est\xE1 disponible\
  \ en Linux/SteamOS"
---
Luchas samurais dentro de una historia inmersiva y personajes carismáticos

Otro título de acción arcade en 2D con una pinta excelente nos llega a Linux/SteamOS. Se trata de Samurai Riot ,del estudio Wako Factory que nos propone un juego de lucha arcade bidimensional con el trasfondo de una historia sobre el deber y las creencias, y donde tendremos que seguir nuestro propio camino hacia el honor y donde tendremos diversos finales.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/KTzFXBknBmI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Lucha solo o en el modo cooperativo con otro jugador en diversos modos de juego, luchando con estilo hasta desvelar los diferentes finales.


'Samurai Riot no está disponible en español, pero si te va la temática y no es problema para ti, lo tienes disponible en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/658790/" style="display: block; margin-left: auto; margin-right: auto; border: 0;" width="646"></iframe></div>


 ¿Que te parece este Samurai Riot? ¿seguirás el código del Samurai?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

