---
author: Serjor
category: Rol
date: 2017-11-15 20:43:11
excerpt: <p>Obsidian ya empieza a calentar motores para la secuela</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0e0e83a31054e8919990f9e9c3a6da47.webp
joomla_id: 537
joomla_url: pillars-of-eternity-definitive-edition-ya-disponible-en-gog-y-steam
layout: post
tags:
- steam
- gog
- oferta
- rpg
- pillars-of-eternity
- obsidian-entertainment
title: Pillars of Eternity - Definitive Edition ya disponible en GOG y Steam
---
Obsidian ya empieza a calentar motores para la secuela

Buenas noticias para los amantes del rol, y es que vía [Steam](http://store.steampowered.com/sub/220633) nos enteramos de que Obsidian Entertainment, quienes también nos trajeron a GNU/Linux Tyrany y KOTOR II, han puesto a la venta la "Definitive Edition" de Pillars of Eternity, la cual cuenta con todos los DLC publicados hasta ahora, es decir las partes I y II de "The White March", y además, con motivo del lanzamiento de este pack incluyen también un nuevo DLC, completamente gratuito, que ha sido bautizado como "Deadfire Pack" en honor a la segunda parte de este juego: Pillars of Eternity II: Deadfire.


Este DLC, inspirado como decimos en la secuela de este juego, incluye nuevos ítems, como potenciadores y armas y retratos importados de la campaña de Kickstarter de la segunda parte.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/HKoDTzea79Y" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Así que si estabas pensando en hacerte con este juego pásate por [GOG](https://www.gog.com/game/pillars_of_eternity_definitive_edition) o Steam, y aprovecha este pack, que además está de oferta temporalmente.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/291650/220633/" style="display: block; margin-left: auto; margin-right: auto; border-style: none;" width="646"></iframe></div>


Cuéntanos qué te parece este lanzamiento en los comentarios o en los canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

