---
author: Pato
category: Aventuras
date: 2017-01-26 18:52:47
excerpt: <p>El juego ha sido portado gracias a Ethan Lee, responsable de una gran
  cantidad de ports de excelente calidad</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/05a977f75c8bd421cf67e35678973f79.webp
joomla_id: 195
joomla_url: owlboy-el-galardonado-juego-de-aventuras-ya-disponible-en-linux
layout: post
tags:
- plataformas
- aventura
- steam
- pixel
title: '''Owlboy'' el galardonado juego de aventuras ya disponible en Linux'
---
El juego ha sido portado gracias a Ethan Lee, responsable de una gran cantidad de ports de excelente calidad

Diréctamente desde Steam me llega la notificación de que Owlboy [[web oficial](http://www.owlboygame.com/)] ya ha llegado a Linux. ¡Llevamos esperándolo mucho tiempo!


D-Pad Studio confió en Ethan Lee que con su tecnología "FNA" ha conseguido un port para Mac y Linux. 


"Debería ser funcionalmente idéntico a la versión Windows. Continuaremos actualizando y optimizando estas versiones tan bien como podamos al igual que hemos hecho con la versión de Windows", afirman en el anuncio oficial en Steam. Puedes verlo [en este enlace](http://steamcommunity.com/gid/[g:1:4471313]/announcements/detail/583615484601248637).


'Owlboy' es un galardonado juego de aventuras donde "tendrás que explorar mundos abiertos con una mezcla única de vuelo y plataformas". Las críticas del juego son muy positivas.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="360" src="https://www.youtube.com/embed/0N4lp01tFwg" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Aprovechando la ocasión, Owlboy está de oferta con un 33% de descuento. Puedes comprarlo en el siguiente enlace:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/115800/" width="646"></iframe></div>


 ¿Qué te parece 'Owlboy'? ¿Piensas jugarlo?


Cuéntamelo en los comentarios, en el foro o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

