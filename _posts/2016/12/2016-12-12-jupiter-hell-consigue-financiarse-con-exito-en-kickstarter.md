---
author: Pato
category: Estrategia
date: 2016-12-12 19:22:09
excerpt: "<p>A\xFAn quedan unas horas para poder financiar el proyecto y alcanzar\
  \ alg\xFAn objetivo adicional.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e303e2027514497aaa0603a129a3eb42.webp
joomla_id: 150
joomla_url: jupiter-hell-consigue-financiarse-con-exito-en-kickstarter
layout: post
tags:
- indie
- rol
- estrategia
- kickstarter
title: "Jupiter Hell consigue financiarse con \xE9xito en Kickstarter"
---
Aún quedan unas horas para poder financiar el proyecto y alcanzar algún objetivo adicional.

Según publica el creador del juego ya se ha alcanzado el objetivo de financiación de Jupiter Hell:



> 
> JUPITER HELL HAS BEEN BREACHED! <https://t.co/aBmhPuw7Md>
> 
> 
> — Kornel Kisielewicz (@epyoncf) [12 de diciembre de 2016](https://twitter.com/epyoncf/status/808314420975767556)







 A partir de ahora aún quedan unas horas si quereis apoyar este proyecto en su campaña antes de su finalización y así ayudar a conseguir algún objetivo adicional.


Como [ya dijimos en su momento en Jugando En Linux](index.php/item/100-jupiter-hell-un-juego-de-tipo-roguelike-por-turnos-con-graficos-3d-esta-en-campana-en-kickstarter) Jupiter Hell es un juego tipo "roguelike" de acción por turnos del creador de D\*\*mRL (han tenido que cambiar el título original debido a una [reclamación de copyright](https://www.kickstarter.com/projects/2020043306/jupiter-hell-a-modern-turn-based-sci-fi-roguelike/posts/1754286) por parte de Zenimax, poseedora de los derechos de Doom).


Tienes toda la información en la página del juego en Kickstarter:


<div class="resp-iframe"><iframe height="360" src="https://www.kickstarter.com/projects/2020043306/jupiter-hell-a-modern-turn-based-sci-fi-roguelike/widget/video.html" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>

