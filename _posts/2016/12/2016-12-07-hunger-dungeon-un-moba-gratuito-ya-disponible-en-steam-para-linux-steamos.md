---
author: Pato
category: "Acci\xF3n"
date: 2016-12-07 17:58:05
excerpt: "<p>Se trata de un juego \"pixel art\" donde combatir\xE1s con hasta 6 jugadores\
  \ en distintas arenas para derrotar a tus oponentes.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d0d354668f69293e040aa69de3140c78.webp
joomla_id: 144
joomla_url: hunger-dungeon-un-moba-gratuito-ya-disponible-en-steam-para-linux-steamos
layout: post
tags:
- accion
- multijugador
- steam
- gratis
title: '''Hunger Dungeon'' un MOBA gratuito ya disponible en Steam para Linux/SteamOS'
---
Se trata de un juego "pixel art" donde combatirás con hasta 6 jugadores en distintas arenas para derrotar a tus oponentes.

Gracias a [linuxgameconsortium.com](https://linuxgameconsortium.com/linux-gaming-news/hunger-dungeon-adventure-releases-freetoplay-steam-42378/) descubrimos este Hunger Dungeon, un MOBA (multiplayer online battle arena) por rondas clásico para hasta 6 jugadores donde podrás elegir tu personaje de entre 10 disponibles, desbloquear múltiples habilidades y utilizar distintas estrategias para derrotar a tus oponentes. Y lo mejor de todo, ¡puedes jugarlo gratis!


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/jUTuZWNJXXM" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Hunger Dungeon no requiere gran cosa en cuanto a hardware, y con cualquier equipo compatible con OpenGL 2.0 y 512 megas de RAM y VRAM podrás jugarlo sin muchos problemas. Eso sí, no está traducido al español, pero si eso no es problema para tí, tienes disponible Hunger Dungeon en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/513560/" width="646"></iframe></div>


¿Te gustan los juegos tipo MOBA? ¿Que te parece este Hunger Dungeon? ¿Piensas jugarlo?


Cuéntamelo en los comentarios.


¿Juegas a Hunger Dungeon y quieres que publiquemos tu review? ¡[Envíanosla](index.php/contacto/envia-un-articulo)!

