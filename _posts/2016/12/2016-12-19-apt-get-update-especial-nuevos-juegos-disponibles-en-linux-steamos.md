---
author: Pato
category: Apt-Get Update
date: 2016-12-19 15:33:18
excerpt: "<p>Repasamos algunos nuevos lanzamientos que pueden ser de inter\xE9s.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/621af29360685f88fae4c26f96ed9d8c.webp
joomla_id: 154
joomla_url: apt-get-update-especial-nuevos-juegos-disponibles-en-linux-steamos
layout: post
tags:
- steamos
- steam
- nuevos-lanzamientos
title: Apt-Get Update especial Nuevos juegos disponibles en Linux/SteamOS
---
Repasamos algunos nuevos lanzamientos que pueden ser de interés.

Esta semana el Apt-Get Update va a ser algo distinto, ya que aparte de no haber sido el viernes pasado si no hoy cuando lo vamos a publicar, esta vez nos vamos a centrar en lanzamientos que han tenido lugar y que pueden ser de interés en Linux/SteamOS. Comenzamos:


#### Heroine's Quest: The Herald of Ragnarok


Se trata de un juego de rol y aventura "point and clic" del estilo de los Monkey Island o Maniac Mansion con una buena trama y una interfaz que recuerda a los juegos de la mítica Sierra. 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="360" src="https://www.youtube.com/embed/wgzU3nybN5Y" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Puede ser muy del gusto de los que les gustan las aventuras clásicas. Heroine's Quest no está en español, pero si no es problema para ti lo tienes en:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/283880/" width="646"></iframe></div>


 


#### Clustertruck


Se trata de un juego del tipo "salto de obstáculos" donde tienes que saltar "de camión en camión"... literalmente. 


Tiene una campaña, creador de niveles, habilidades y una muy buena valoración en los análisis de Steam.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="360" src="https://www.youtube.com/embed/ZLMIpok-aZ0" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Clustertruck no está disponible en español, pero si eso no es problema lo tienes disponible en:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/397950/" width="646"></iframe></div>


 


#### Security Hole


Es un galardonado juego de tipo puzzle en 3D que además tiene una trama ciberpunk detrás que hace que resolver los puzzles cobre su sentido. Interesante para los que les guste calentarse la cabeza con puzzles "espaciales".


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="360" src="https://www.youtube.com/embed/HOoVZ866_Io" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Security Hole está traducido al español y lo tienes disponible en:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/503460/" width="646"></iframe></div>


 


#### Dead Age


Se trata de un llamativo juego de tipo "estrategia" por turnos con "muerte permanente" que en ciertos aspectos recuerda al Darkest Dungeon donde tendrás que sobrevivir a una invasión zombie. 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="360" src="https://www.youtube.com/embed/LP6FS3hyVfY" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Dead Age no está disponible en español, pero si eso no es problema lo tienes disponible en:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/363930/" width="646"></iframe></div>


 


¿Algún otro título que merezca la pena y nos hemos dejado en el tintero? Seguro que si.


¿Nos lo cuentas en los comentarios?

