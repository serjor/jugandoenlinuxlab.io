---
author: leillo1975
category: Rol
date: 2016-12-02 14:55:31
excerpt: "<p>En esta ocasi\xF3n los enanos son los protagonistas.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/84ac056b57dd032fcf18a346d4a81feb.webp
joomla_id: 137
joomla_url: mas-rpg-para-linux-steamos-con-the-dwarves
layout: post
tags:
- crowdfunding
- rpg
title: "M\xE1s RPG para Linux/SteamOS con The Dwarves"
---
En esta ocasión los enanos son los protagonistas.

Hace algún tiempo que le seguía la pista a este juego en Steam, y ayer me llegó el aviso de que ya estaba disponible. Editado por THQNordic y creado por el estudio alemán [King Art Games](http://www.kingart-games.com/) (The book of the Unwrittent Tales 1 y 2, the Raven, Battle Worlds Kronos), y financiado vía [Kickstarter](https://www.kickstarter.com/projects/kingartgames/the-dwarves-a-new-storydriven-fantasy-rpg) hace algún tiempo, The Dwarves nos presenta un juego de rol basado en los mundos descritos en la novela homónima de Markus Heitz. Nos encontramos ante un RPG táctico de batallas multitudinarias donde podemos parar el tiempo para tomar decisiones. El nivel gráfico y el diseño de personajes está muy cuidado. La verdad es que estoy deseando hacerme con una clave para poder hacer un análisis más exaustivo del juego. 


Los requisitos de sistema no son elevados y también tiene soporte para AMD, por lo que nos encontramos ante un título accesible en cuanto a nivel de hardware.


Mencionar también que el juego tiene una banda sonora espectacular en la que participa el famoso grupo alemán de Heavy Metal Épico "[Blind Guardian](http://www.blind-guardian.com/)" en el tema "Children Of The Smith":


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/F5X7MXfLxEg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Sobre el juego (Descripción de Steam):*"""The Dwarves es un juego de rol y fantasía con una historia profunda y desafiantes batallas en tiempo real. Haz uso de 15 héroes jugables, cada uno con habilidades exclusivas, y planea de manera inteligente los combates. Enfréntate a una gran cantidad de orcos, ogros, albos, bogglins, zombis, magos oscuros y muchos más enemigos, y enséñales lo que es bueno.*   
   
*Basado en la novela ""Las cinco estirpes"", de Markus Heitz, con este juego podrás experimentar una historia fascinante con la mejor raza de fantasía de todos los tiempos: ¡los enanos!*   
   

* ***Batallas tácticas en tiempo real:** te enfrentarás a cientos de enemigos con apenas un puñado de héroes. Puedes pausar el juego en cualquier momento para planear tu siguiente movimiento estratégico.*
* ***Batallas multitudinarias:** todas las criaturas del campo de batalla se comportan siguiendo las leyes de la física, lo que les da a los combates un aire dinámico y real, y proporciona muchas opciones tácticas.*
* ***Una historia absorbente:** únete al enano Tungdil en su periplo por Girdlegard.*
* ***Explora el mundo:** descubre secretos, aprende más sobre el universo del juego y sus habitantes y resuelve rompecabezas opcionales.*
* ***Adéntrate en el mapa:** Girdlegard abarca desde picos nevados hasta resplandecientes desiertos. Puedes moverte por el mapa del mundo con total libertad, conocer a numerosos personajes y vivir aventuras. Pero ten cuidado: ¡muchas de tus acciones en el mapa tendrán consecuencias imprevistas!*


  
***¡Enanos!** Por fin la mejor raza de fantasía es la protagonista. Se acabaron los días en que esos barbudos con hachas no eran más que PNJ. ¡Ha llegado la era de los enanos!"""*
Si quieres adquirir The Dwarves, su precio normal es de unos 40€, pero en este momento tiene un 10% de descuento en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/403970/79083/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


Fuentes: [RafaBasa.com](http://www.rafabasa.com/2016/12/01/blind-guardian-sepultura-bullet-for-my-valentine-stephen-pearcy-xandria/), [Steam](http://store.steampowered.com/app/403970/?l=spanish), [Kickstarter](https://www.kickstarter.com/projects/kingartgames/the-dwarves-a-new-storydriven-fantasy-rpg), [dwarves-game.com](http://www.dwarves-game.com), [linuxgamenews.com](http://linuxgamenews.com/post/153919342177/the-dwarves-releases-on-steam-with-new-character#.WEEuc3XhA8o)

