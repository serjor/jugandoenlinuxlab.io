---
author: Pato
category: Rol
date: 2016-12-01 11:11:35
excerpt: "<p>La tienda digital lleva un par de d\xEDas emplaz\xE1ndonos a esperar\
  \ una sorpresa para hoy, y por fin lo han desvelado.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/475699d297afae315ef802312426354e.webp
joomla_id: 135
joomla_url: gog-comienza-sus-rebajas-de-invierno-regalando-neverwinter-nights-diamond
layout: post
tags:
- rol
- gratis
- oferta
- rebajas
title: GOG sigue con sus rebajas de invierno regalando Neverwinter Nights Diamond
---
La tienda digital lleva un par de días emplazándonos a esperar una sorpresa para hoy, y por fin lo han desvelado.

Vía este post de la [cuenta de GOG](https://twitter.com/GOGcom) en Twitter nos llega la confirmación de que lo que la tienda digital GOG lleva anunciando en los últimos días es que ahora te puedes hacer con Neverwinter Nights Diamond gratis:



> 
> 2016.12.01 is here and with it your FREE copy of [#NeverwinterNights](https://twitter.com/hashtag/NeverwinterNights?src=hash) Diamond! Grab it now - you only have 48hrs! <https://t.co/MrM9iTQ0ko> [pic.twitter.com/oYQttKiXrN](https://t.co/oYQttKiXrN)
> 
> 
> — GOG.com (@GOGcom) [1 de diciembre de 2016](https://twitter.com/GOGcom/status/804278951967412232)







 


También recordar que GOG comenzó sus rebajas de invierno con suculentos descuentos en un buen número de títulos y paquetes de juegos disponibles para nuestra plataforma.


Puedes conseguir Neverwinter Nights Diamond y ver las ofertas visitando el enlace:


<https://www.gog.com/>

