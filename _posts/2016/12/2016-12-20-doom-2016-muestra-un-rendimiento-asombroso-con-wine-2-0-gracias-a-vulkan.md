---
author: leillo1975
category: "Acci\xF3n"
date: 2016-12-20 10:31:35
excerpt: "<p>Hace mucho tiempo que estoy suscrito a <span style=\"color: #000080;\"\
  ><a href=\"https://www.youtube.com/channel/UCBu5HY4---D2BE2Au8BNz3Q\" target=\"\
  _blank\" style=\"color: #000080;\">Penguin Recordings </a><span style=\"color: #000000;\"\
  >porque a menudo nos sorprende con excelentes videos relacionados con el mundo gamer\
  \ en Linux. Se trata siempre de material muy ilustrativo y de muy buena calidad.\
  \ En este caso nos muestra algo tremendamente asombroso, y es la \xFAltima versi\xF3\
  n del cl\xE1sico Doom corriendo a una velocidad endiablada con Wine y con la API\
  \ de Vulkan. El resultado me ha dejado con la mand\xEDbula desencajada ya que el\
  \ rendimiento es fenomenal, algo que no me esperaba sinceramente. Es mejor que lo\
  \ veais y juzgueis por vosotros mismos:</span></span></p>\r\n<p>&nbsp;</p>\r\n<p><iframe\
  \ src=\"https://www.youtube.com/embed/AWZvwhwT1Sk\" width=\"560\" height=\"315\"\
  \ style=\"display: block; margin-left: auto; margin-right: auto;\" allowfullscreen=\"\
  allowfullscreen\"></iframe></p>\r\n<p>&nbsp;</p>\r\n<p>Seg\xFAn comenta en el video,\
  \ se trata de una versi\xF3n parcheada por un tal Oleg Suchilov (<span style=\"\
  color: #000080;\"><a href=\"https://github.com/thevoidnn/wine20-bcrypt-doom\" target=\"\
  _blank\" style=\"color: #000080;\">thevoidnn</a></span>) de Wine-staging 2.0 RC1\
  \ corriendo en Ubuntu 16.04 con una GTX1070 (375.26). Obviamente lo ideal ser\xED\
  a un port directo de Doom a GNU/Linux, pero a falta de carne, buenas son habas.</p>\r\
  \n<p>&nbsp;</p>\r\n<p>Fuente: <a href=\"https://www.youtube.com/channel/UCBu5HY4---D2BE2Au8BNz3Q\"\
  \ target=\"_blank\">Penguin Recordings</a></p>\r\n<p>&nbsp;</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7e58113966c67085953dd45e2ed189c9.webp
joomla_id: 156
joomla_url: doom-2016-muestra-un-rendimiento-asombroso-con-wine-2-0-gracias-a-vulkan
layout: post
tags:
- wine
- vulkan
- doom
title: DOOM 2016 muestra un rendimiento asombroso con Wine 2.0 gracias a Vulkan
---
Hace mucho tiempo que estoy suscrito a [Penguin Recordings](https://www.youtube.com/channel/UCBu5HY4---D2BE2Au8BNz3Q) porque a menudo nos sorprende con excelentes videos relacionados con el mundo gamer en Linux. Se trata siempre de material muy ilustrativo y de muy buena calidad. En este caso nos muestra algo tremendamente asombroso, y es la última versión del clásico Doom corriendo a una velocidad endiablada con Wine y con la API de Vulkan. El resultado me ha dejado con la mandíbula desencajada ya que el rendimiento es fenomenal, algo que no me esperaba sinceramente. Es mejor que lo veais y juzgueis por vosotros mismos:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/AWZvwhwT1Sk" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Según comenta en el video, se trata de una versión parcheada por un tal Oleg Suchilov ([thevoidnn](https://github.com/thevoidnn/wine20-bcrypt-doom)) de Wine-staging 2.0 RC1 corriendo en Ubuntu 16.04 con una GTX1070 (375.26). Obviamente lo ideal sería un port directo de Doom a GNU/Linux, pero a falta de carne, buenas son habas.


 


Fuente: [Penguin Recordings](https://www.youtube.com/channel/UCBu5HY4---D2BE2Au8BNz3Q)


 

