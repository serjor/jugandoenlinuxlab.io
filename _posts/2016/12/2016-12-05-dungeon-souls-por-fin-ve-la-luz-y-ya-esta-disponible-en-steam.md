---
author: Pato
category: Rol
date: 2016-12-05 17:47:03
excerpt: <p>El juego de Lamina Studios llevaba ya un tiempo en acceso anticipado.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/fac9770ae986695c80dfb6c58f312f32.webp
joomla_id: 141
joomla_url: dungeon-souls-por-fin-ve-la-luz-y-ya-esta-disponible-en-steam
layout: post
tags:
- accion
- indie
- rol
title: "Dungeon Souls por fin ve la luz y ya est\xE1 disponible en Steam"
---
El juego de Lamina Studios llevaba ya un tiempo en acceso anticipado.

Gracias a los chicos de [Linuxgameconsortium.com](https://linuxgameconsortium.com/linux-gaming-news/dungeon-souls-roguelike-dungeon-crawler-finally-launches-42303/) nos llega la noticia de que Dungeon Souls [[web oficial](http://www.laminastudios.com/)] ya ha salido de la fase de acceso anticipado y ya lo tenemos disponible para su compra.


Dungeon Souls es un juego de acción y aventuras donde cada nivel es generado proceduralmente para que cada partida sea distinta y única. Tendrás que activar todas las marcas de cada nivel para desbloquear el portal de cada mazmorra y poder escapar.


Encontrarás multitud de objetos y podrás obtener mejoras para enfrentarte a las fuerzas oscuras y distintos jefes en cada fase.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="360" src="https://www.youtube.com/embed/rasnMsUxWEk" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


Dungeon Souls no está disponible en español, pero si eso no es problema para ti tienes el juego disponible en su web de Steam rebajado un 15% hasta el 9 de Diciembre:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/383230/" width="646"></iframe></div>


¿Te gustan este tipo de juegos "Dungeon Crawler"? ¿piensas jugar este Dungeon Souls?


Cuéntamelo en los comentarios.


¿Tienes Dungeon Souls y quieres que publiquemos tu review? ¡[Mándanosla](index.php/contacto/envia-un-articulo)!

