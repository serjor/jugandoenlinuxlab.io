---
author: Pato
category: Estrategia
date: 2016-12-07 18:36:07
excerpt: "<p>La expansi\xF3n se espera para ma\xF1ana, pero Feral no ha desvelado\
  \ si estar\xE1 disponible en Linux al mismo tiempo que en otros sistemas.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8012f255a337782bffaadea968723f36.webp
joomla_id: 145
joomla_url: la-expansion-de-total-war-warhammer-realm-of-the-wood-elves-llegara-a-linux-proximamente-de-la-mano-de-feral-interactive
layout: post
tags:
- feral-interactive
- estrategia
title: "La expansi\xF3n de Total War: WARHAMMER \"Realm of the Wood Elves\" llegar\xE1\
  \ a Linux pr\xF3ximamente de la mano de Feral Interactive"
---
La expansión se espera para mañana, pero Feral no ha desvelado si estará disponible en Linux al mismo tiempo que en otros sistemas.

 Feral Interactive ha anunciado que la próxima expansión de Total War: WARHAMMER llamada "Realm of the Wood Elves" o lo que es lo mismo "el reino de los Elfos del Bosque" estará disponible próximamente en Linux:



> 
> The Elves are coming for Christmas. This Realm of the Wood Elves DLC introduces a formidable new faction to Total War: WARHAMMER for Linux. [pic.twitter.com/5K1QsRFeE4](https://t.co/5K1QsRFeE4)
> 
> 
> — Feral Interactive (@feralgames) [7 de diciembre de 2016](https://twitter.com/feralgames/status/806453846629294080)







 Esta expansión llegará a sistemas Windows mañana mismo, pero Feral no ha desvelado si estará disponible también para Linux de salida o tardará aún unos días en llegar. Lo que si parece claro es que estas Navidades tendremos la expansión disponible. Para ir abriendo boca, aquí tenéis el vídeo de presentación de la expansión:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/hoiks2rQa9o" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Las novedades que nos traerá esta expansión son:


*Nueva raza de los Elfos Silvanos en la Gran Campaña*


*Nueva moneda adicional específica para la raza*


*Condición para la victoria en la campaña relacionada a un edificio legendario*


*Los Elfos Silvanos pueden conquistar cualquier región del mapa*


*Dos nuevos Señores legendarios con los que jugar: Orión y Durthu*


*Dos nuevos tipos de Señor con cuadros de habilidades muy especializados*


*Tres nuevos tipos de héroes con cuadros de habilidades muy especializados*


*Lista de tropas exhaustiva de los Elfos Silvanos*


*Añade un mapa de la campaña adicional al juego con una historia de campaña exclusiva de los Elfos Silvanos, la "Estación de la Revelación"*


*Nuevos y únicos monstruos, héroes, magia y funciones del juego*


Si no nos llega mañana, esperemos que Feral no nos haga esperar mucho.

