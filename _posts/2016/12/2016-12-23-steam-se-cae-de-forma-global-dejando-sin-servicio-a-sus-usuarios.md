---
author: Pato
category: Software
date: 2016-12-23 18:06:25
excerpt: "<p>Lleva ya un par de horas ca\xEDdo y sin s\xEDntomas de recuperarse. Los\
  \ usuarios pueden iniciar sesi\xF3n en modo desconectado para poder jugar.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/28232e87509dedeadc0cd2b6a94ea3f5.webp
joomla_id: 163
joomla_url: steam-se-cae-de-forma-global-dejando-sin-servicio-a-sus-usuarios
layout: post
tags:
- steamos
- steam
title: Steam se cae de forma global dejando sin servicio a sus usuarios
---
Lleva ya un par de horas caído y sin síntomas de recuperarse. Los usuarios pueden iniciar sesión en modo desconectado para poder jugar.

Hace ya unos meses Steam sufrió un ataque que dejó el servicio caído durante unas horas y que supuso multitud de quejas. 


Si bien es cierto que cuando hay rebajas el servicio suele dar algún pequeño problema al recibir una avalancha de usuarios tratando de buscar y comprar las ofertas que hace la tienda, normalmente son incidencias muy puntuales y suelen ser momentáneas siendo los cortes de servicio de solo unos segundos o a lo sumo minutos.


Sin embargo lo que sucede esta tarde recuerda demasiado al ataque sufrido hace unos meses, donde un DDOS (ataque de denegación de servicio) dejó el servicio caído durante horas. El cliente de Steam no puede iniciar sesión y la web muestra un escueto mensaje de error:


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamoto16/Steamerror.webp)


Steam es un servicio normalmente muy robusto que permite descargar sus juegos y software a velocidades que otros ya quisieran, y normalmente suelen "capear" los picos de tráfico de forma mas o menos solvente.. Esto lo consiguen gracias a que tienen contratados varios servidores externos en los que recae todo el tráfico que Steam no puede gestionar directamente.


Sin un comunicado oficial de qué está pasando solo podemos especular sobre esta incidencia, que esperemos puedan resolver lo más pronto posible. ¡Sobre todo por que tenemos en marcha una campaña de rebajas navideñas!


En cuanto sepamos algo de forma oficial os lo haremos saber.


![Gracias a steamstat.us](https://i.imgur.com/dpyG6c9.webp)

