---
author: Pato
category: Estrategia
date: 2016-12-21 12:30:02
excerpt: "<p>El juego tiene una valoraci\xF3n extremadamente positiva en los an\xE1\
  lisis.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a07bb170c4a36161aa1f8f4859c19794.webp
joomla_id: 159
joomla_url: el-llamativo-kingdom-new-lands-ya-disponible-en-linux-steamos
layout: post
tags:
- indie
- pixel
- estrategia
title: El llamativo 'Kingdom New Lands' ya disponible en Linux/SteamOS
---
El juego tiene una valoración extremadamente positiva en los análisis.

SteamDB Linux es una fuente gracias a la que sabemos casi todo lo que nos llega a Linux. En este caso hablamos de "Kingdom New Lands" [[web oficial](http://www.kingdomthegame.com/)]:



> 
> Game Works:  
> Kingdom: New Lands<https://t.co/zCF25djQ2R>
> 
> 
> — SteamDB Linux Update (@SteamDB_Linux) [19 de diciembre de 2016](https://twitter.com/SteamDB_Linux/status/810822118451609604)







Se trata de un llamativo juego "pixel art" en el que tendrás que luchar y conquistar tierras. Ha sido galardonado en algún festival Indie y los análisis de Steam son extremadamente positivos.


#### Sinopsis de Steam:


*Cuenta la historia que existen unas lejanas islas con misterios que aguardan ser descubiertas. Los dirigentes necesitarán toda la fuerza de sus súbditos para navegar y encontrar nuevos reinos en estas nuevas tierras.*


*Kingdom: Las nuevas tierras toman como base el laureado estilo de juego y el misterio de Kingdom al incluir abundante contenido nuevo al título nominado en el Independent Games Festival, a la vez que se mantiene la simplicidad y profundidad que las legiones de monarcas han experimentado y disfrutado. Viaja a las nuevas tierras y recibe una avalancha de nuevas monturas, comerciantes y vagabundos que consideran estas islas su casa. Pero ten cuidado con los obstáculos que amenazan tu llegada, ya que tu camino no estará bloqueado solo por ávidas criaturas, sino que el entorno puede derrotarte por sí mismo.*


*Sé valiente, imponte y lucha hasta el amargo final, no sea que te vayan a conquistar estas tierras a ti.*


El aspecto visual desde luego salta a la vista:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/YCkhhwvKz2s" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Kingdom New Lands está traducido al español. Si te llama la atención, lo tienes disponible en [GOG](https://www.gog.com/game/kingdom_new_lands), [Humble Bundle](https://www.humblebundle.com/store/kingdom-new-lands) o en su web de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/496300/" width="646"></iframe></div>

