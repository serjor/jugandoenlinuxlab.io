---
author: Pato
category: Estrategia
date: 2016-12-20 13:00:49
excerpt: "<p>Por fin Feral nos trae este ansiado DLC con nueva raza y m\xE1s contenido.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3409e45349ec9f6b3397bfe10e87a1d0.webp
joomla_id: 157
joomla_url: el-dlc-para-total-war-warhammer-realm-of-the-whood-elves-ya-disponible-en-linux-steamos
layout: post
tags:
- steamos
- feral-interactive
- estrategia
- dlc
title: 'El DLC para Total War: WARHAMMER'' Realm of the Wood Elves ya disponible en
  Linux/SteamOS'
---
Por fin Feral nos trae este ansiado DLC con nueva raza y más contenido.

Tal y como [anunciamos](index.php/item/245-la-expansion-de-total-war-warhammer-realm-of-the-wood-elves-llegara-a-linux-proximamente-de-la-mano-de-feral-interactive) en su momento en Jugando en Linux Feral nos ha traido este nuevo DLC para Total War: WARHAMMER que incluye nuevo contenido, nueva raza "Elfos del Bosque", dos nuevos Señores Legendarios, un nuevo mapa de campaña con nueva historia y mucho mas.



> 
> Enter the Realm of the Wood Elves, new DLC for Total War: WARHAMMER on Linux. Recruit your new Elven armies: <https://t.co/XdmAOTxRWn> [pic.twitter.com/ljCJegIARj](https://t.co/ljCJegIARj)
> 
> 
> — Feral Interactive (@feralgames) [20 de diciembre de 2016](https://twitter.com/feralgames/status/811168414869979136)







 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/hoiks2rQa9o" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Puedes conseguir este DLC en la [tienda de Feral Interactive](https://store.feralinteractive.com/es/mac-linux-games/warhammertw/#warhammertwdlc7) y así apoyarás diréctamente a los responsables del port a Linux, o puedes comprarlo también en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/534331/" width="646"></iframe></div>


 ¿Que te parece esta expansión "Realm of the Whood Elves"? ¿Piensas jugarla?


Cuéntamelo en los comentarios.


¿Tienes Total War: WARHAMMER Realm of the Whood Elves y quieres que publiquemos tu review? ¡[Mándanosla](index.php/contacto/envia-un-articulo)!

