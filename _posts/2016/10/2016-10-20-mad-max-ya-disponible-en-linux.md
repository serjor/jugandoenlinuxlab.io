---
author: Pato
category: "Acci\xF3n"
date: 2016-10-20 15:59:22
excerpt: "<p>Gracias a Feral Interactive tenemos un nuevo t\xEDtulo de gran calidad\
  \ en nuestro sistema favorito.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/64d93d666355a43c4a86679a030d35b6.webp
joomla_id: 64
joomla_url: mad-max-ya-disponible-en-linux
layout: post
tags:
- accion
- aventura
- steam
- feral-interactive
title: Mad Max ya disponible en Linux
---
Gracias a Feral Interactive tenemos un nuevo título de gran calidad en nuestro sistema favorito.

Hace pocas semanas que Feral nos trajo los [Dawn of War, Chaos Rising y Retribution](index.php/item/10-warhammer-40000-dawn-of-war-ii-chaos-rising-y-retribution-ya-disponibles-para-linux-steamos) y ahora tenemos a uno de los grándes: Mad Max


Si de algo está necesitado Linux es de juegos de mundo abierto y una buena historia de calidad. Hasta ahora teníamos Borderland 2, Shadows of Mordor, pero Mad Max es un título que no puede faltar en nuestro catálogo de juegos disponibles para mantener el interés.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/w_BBzSMws-E" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


El 90% sobre 16.000 comentarios sobre el juego son positivos, por lo que cabe esperar un buen recibimiento por parte de los usuarios de Linux.


#### Requisitos mínimos:


MÍNIMO:   
SO: Ubuntu 16.04 or Steam OS 2.0 (64 bit)  
Procesador: Intel i5 3.4GHz, AMD FX8350   
Memoria: 8 GB de RAM   
Gráficos: 2GB Nvidia 660ti o superior (driver version: 367.35)   
Almacenamiento: 35 GB de espacio disponible   
  



RECOMENDADO:   
SO: Ubuntu 16.04 o Steam OS 2.0 (64 bit)  
Procesador: Intel i7 3.6 GHz   
Memoria: 16 GB de RAM   
Gráficos: 4GB Nvidia 970 o superior (driver version: 367.35)   
Almacenamiento: 35 GB de espacio disponible


Notas adicionales: Gráficas AMD e Intel no están soportadas en el momento del lanzamiento, sin embargo en declaraciones a Liam de GOL un programador de Feral confirma que con gráficas AMD de la actual generación (2Gb Vram o superiores con soporte Mesa 11.2/Radeonsi) el juego se ejecuta corréctamente. Con AMDGPUPRO también se puede jugar, pero Mesa es más rápido y estable.


También hay numerosos reportes de usuarios ejecutando el juego en gráficas AMD sin muchos problemas. Liam ha publicado un extenso análisis del juego en [GOL](https://www.gamingonlinux.com/articles/mad-max-released-for-linux-port-report-and-review-available.8368) (en inglés).


 Mad Max está traducido al español y puedes comprarlo en la [Tienda de Feral](https://store.feralinteractive.com/en/mac-linux-games/madmax/) y así apoyarás a los responsables del Port, o directamente en Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/234140/" width="646"></iframe></div>


¿Tienes pensado comprar Mad Max? ¿que te parece este port de Feral?


Cuéntame en los comentarios.

