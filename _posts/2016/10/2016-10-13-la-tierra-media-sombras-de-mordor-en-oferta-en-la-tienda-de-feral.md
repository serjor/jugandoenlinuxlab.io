---
author: Pato
category: "Acci\xF3n"
date: 2016-10-13 16:14:45
excerpt: "<p>Feral Interactive est\xE1 promocionando el juego con un suculento descuento.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f7a0a54c92471ac4480e727e4ccf93df.webp
joomla_id: 51
joomla_url: la-tierra-media-sombras-de-mordor-en-oferta-en-la-tienda-de-feral
layout: post
tags:
- accion
- tercera-persona
- oferta
- feral-interactive
title: 'La Tierra Media: Sombras de Mordor en oferta en la tienda de Feral'
---
Feral Interactive está promocionando el juego con un suculento descuento.

Seguimos con Feral Interactive. Están promocionando su port de La Tierra Media: Sombras de Mordor en su edición Juego del Año con un descuento del 60%


En concreto, puedes conseguir el juego por solo 19,98€ y además estarás apoyando diréctamente al estudio responsable del port a Linux. Es una buena oportunidad para hacerte con el, ¿verdad?


 


#### Características:


-Realiza ataques combinados brutales con tu espada, daga y poderes espectrales mágicos en dramáticos combates cuerpo a cuerpo sin restricciones.


-Entra en el mundo Espectral fantasmagórico para descubrir cosas invisibles a ojos de los mortales, domina las mentes enemigas y tumba a Uruks con ataques a distancia mortíferos.


-Desmantela las fuerzas de Sauron desde dentro matando o controlando Némesis, cuyas apariciones, miedos y habilidades se desarrollan con cada enfrentamiento.


-La Tierra Media: Sombras de Mordor Edición Game of the Year contiene todo el DLC anteriormente publicado para el juego, incluidas las misiones de El Señor de la Caza y El Señor de la Luz, la serie de desafíos Prueba de Guerra y todas las misiones de Banda, así como Runas y Aspectos.


 


#### Requisitos:


Mínimos   
Sistema Operativo: Ubuntu 14.04.2 64-bit  
Procesador: Doble núcleo de 64 bits y 2,6 GHz   
RAM: 4GB 8GB  
Disco Duro: 47GB  
Tarjeta Gráfica: Tarjeta de las series 1GB NVIDIA 640 DDR3 o superior con una versión del controlador 352.21 o superior (testeado)   
Entrada: Teclado y ratón 


Recomendados


Procesador: Intel i7 de 64 bits y 3,4 GHz


RAM: 8GB


Tarjeta de las series 4GB NVIDIA 9xx o superior con una versión del controlador 352.21 o superior (testeado)


Entrada: Mando


Puedes encontrar el juego en su tienda:


<https://store.feralinteractive.com/es/mac-linux-games/shadowofmordor/>


Y visitar su miniweb:


<http://www.feralinteractive.com/es/games/shadowofmordor/>

