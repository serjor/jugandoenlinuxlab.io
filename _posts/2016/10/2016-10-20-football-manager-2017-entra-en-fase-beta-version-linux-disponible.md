---
author: Pato
category: Estrategia
date: 2016-10-20 18:56:04
excerpt: "<p>El juego de Sports Interactive y Sega llegar\xE1 a Linux pr\xF3ximamente.\
  \ Actualmente ya puedes pre-comprarlo.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c1572c59821062c96d0fc33ad32a2983.webp
joomla_id: 66
joomla_url: football-manager-2017-entra-en-fase-beta-version-linux-disponible
layout: post
tags:
- steam
- simulador
- estrategia
- beta
title: "Football Manager 2017 entra en fase beta. Versi\xF3n Linux disponible"
---
El juego de Sports Interactive y Sega llegará a Linux próximamente. Actualmente ya puedes pre-comprarlo.

La serie Football Manager tiene tras de sí un buen historial de versiones que soportan Linux. De momento, el juego de estrategia futbolística solo estará disponible en fase beta para todo aquél que ya disponga del título en su biblioteca, es decir que haya hecho la pre-compra. El juego estará disponible oficialmente el día 4 de noviembre, aunque no se sabe si la versión Linux estará de salida.


#### Sinopsis del juego:


Asume el mando de tu equipo de fútbol favorito con Football Manager 2017, el juego de gestión futbolística más realista hasta la fecha. ¡Se acerca mucho a la ocupación de la vida real!


Con más de 2500 clubs reales que gestionar y unos 500 000 jugadores de fútbol y personal entre los que contratar, Football Manager 2017 te sumerge de lleno en el vibrante y vivo mundo de la gestión futbolística. Y tú estás en el centro de todo.


#### Requisitos del sistema:


Mínimo:  
        SO: SteamOS, Ubuntu 12.04.5 LTS – 64-bit  
        Procesador: Intel Pentium 4, Intel Core or AMD Athlon – 2.2 GHz +  
        Memoria: 2 GB de RAM  
        Gráficos: Intel HD Graphics 3000/4000, NVIDIA GeForce 8600M GT or AMD/ATI Mobility Radeon HD 2400 – 256MB VRAM  
        Almacenamiento: 3 GB de espacio disponible


Si estás interesado en participar en la beta puedes hacer la pre-compra en su página de Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/482730/" width="646"></iframe></div>

