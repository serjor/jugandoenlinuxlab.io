---
author: Pato
category: Puzzles
date: 2016-10-06 17:43:09
excerpt: <p>Conecta 3 con tus amigos en diferentes modos de juego. Puedes probarlo
  gratis!</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/71f67488b0857639cee631943a3fc6fa.webp
joomla_id: 40
joomla_url: puzzles-de-tipo-conecta-3-prueba-tumblestone
layout: post
tags:
- steam
- puzzles
- conecta-3
title: "\xBFBuscas puzzles de tipo conecta 3? prueba Tumblestone"
---
Conecta 3 con tus amigos en diferentes modos de juego. Puedes probarlo gratis!

De vez en cuando encuentras juegos que pasan desapercibidos dentro de la gran maraña de juegos indie y morralla variada, y esta vez dí con Tumblestone. Se trata de un juego de tipo puzzle conecta 3 que realmente promete diversión.


Tiene diversos modos de juego: Historia, arcade, misiones y múltiples modos de juego multijugador local y en línea. 


Según las reviews el multijugador online no está muy concurrido de jugadores, pero las valoraciones en general son muy positivas.


Para echar unas partidas en local con amigos viene de fábula. Soporta hasta 4 jugadores en el mismo equipo y además puedes probarlo gratis. No está mal...¿no?


### Acerca del juego:


Tumblestone es el primer juego de acción con puzzles en los últimos quince años. Compite con tus amigos en el modo multijuego o desafíate a ti mismo en el modo de historia. Resuelve puzzles cada vez más difíciles y creativos, ayuda a una salchicha a hacer amigos y conoce qué ha pasado con la Tumblecrown.


Multijugador competititvo: la intensa acción de resolver puzzles convertirá "unas cuantas rondas con amigos" en una maratón de toda la noche jugando a Tumblestone.   
Multijugador local: hasta cuatro Jugadores en la misma computadora.   
Multijugador en línea: juega contra tus amigos o encuentra amigos nuevos usando el sistema de emparejamiento competitivo.   
Bots: ¿te sobra un hueco? Juega contra el ordenador. Pero, ¿puede alguien vencer al bot de Pesadilla?   
Modo Historia: la campaña de más de 40 horas pondrá a prueba tu progreso resolviendo puzzles. Incluye más de 10 modificadores de juego.   
Modos Arcade: relájate con los 3 modos arcade de Tumblestone.   
Tabla de posiciones: las tablas de posiciones en línea les enseñarán a tus amigos quién es el mejor.   
Misiones: cientos de misiones únicas que completar.   
Estadísticas: cientos de estadísticas en las que puedes sumergirte por completo.   
Personalización: elige de entre más de una docena de personajes y entornos.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/-vPjY10w1CU" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Puedes encontrarlo en [[Steam](http://store.steampowered.com/app/269710)].

