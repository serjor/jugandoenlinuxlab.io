---
author: Serjor
category: Estrategia
date: 2016-10-04 04:46:24
excerpt: <p>Saca el estratega que llevas dentro pagando lo que quieras</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/fc1da7257992fc36032e11db3df7a664.webp
joomla_id: 33
joomla_url: humble-bundle-company-of-heroes
layout: post
tags:
- rts
- humble-bundle
title: 'Humble Bundle: Company of Heroes'
---
Saca el estratega que llevas dentro pagando lo que quieras

Como parte del décimo aniversario de la saga Company Of Heroes, los chicos de [Humble Bundle](https://www.humblebundle.com/company-of-heroes-10th-anniversary) están llevando a cabo una de sus jugosas ofertas. Hasta el 17 de octubre paga lo que quieras por disfrutar de los juegos de esta saga.


A partir de 1$ están disponibles los juegos del primer Company Of Heroes, de los cuales no todos son compatibles con Linux, y si pagas más que la media podrás disfrutar de CoH2, el cuál si funciona bajo Linux, varias expansiones y la banda sonora del juego. A parte, pagando más de 10$ el número de extras aumenta, y si pagas un fijo de 30$ una camiseta edición especial para este bundle.


 

