---
author: Pato
category: "Acci\xF3n"
date: 2016-10-04 09:24:32
excerpt: "<p>Parece que el juego est\xE1 teniendo actividad en SteamDB donde ha aparecido\
  \ una configuraci\xF3n para lanzar el juego en Linux.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3899dfe821816fbcb3db3e3b23f81585.webp
joomla_id: 30
joomla_url: homefront-the-revolution-muestra-signos-de-avance-en-linux
layout: post
tags:
- accion
- proximamente
- primera-persona
title: Homefront the Revolution muestra signos de avance en Linux
---
Parece que el juego está teniendo actividad en SteamDB donde ha aparecido una configuración para lanzar el juego en Linux.

Según se puede ver en la página de la actividad del juego en SteamDB [aquí](https://steamdb.info/app/223100/history/?changeid=2319678) y [aquí](https://steamdb.info/app/223100/history/?changeid=2322733) todo parece indicar que el juego de Deep Silver está en movimiento en las últimas horas de cara a un futuro lanzamiento en Linux.


Los cambios apenas tienen unas horas, pero Deep Silver ya [confirmó hace tiempo](https://twitter.com/deepsilver/status/747355819923079168?ref_src=twsrc%5Etfw) que estaban trabajando para traer Homefront The Revolution a Linux.


El juego no tiene una valoración muy positiva en su página de Steam, pero parece que hay mucha gente que sí le gusta el juego. Además aún está recibiendo soporte y actualizaciones, incluso DLCs con lo que el juego está muy vivo.


Personalmente no soy usuario de juegos de acción en primera persona, pero he de reconocer que este juego tiene muy buena pinta.


¿Tienes el juego? ¿piensas comprarlo?  Cuentamelo en los comentarios

