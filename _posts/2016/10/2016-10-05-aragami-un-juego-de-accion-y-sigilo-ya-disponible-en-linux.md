---
author: Pato
category: "Acci\xF3n"
date: 2016-10-05 06:16:11
excerpt: "<p>Convi\xE9rtete en un oscuro esp\xEDritu ninja en este juego de Lince\
  \ Works, mezcla de buena trama, acci\xF3n y sigilo. Linux est\xE1 soportado desde\
  \ su lanzamiento.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c9b002fe1bb0320831a8ae78670fdb6f.webp
joomla_id: 34
joomla_url: aragami-un-juego-de-accion-y-sigilo-ya-disponible-en-linux
layout: post
tags:
- accion
- indie
- steam
- sigilo
- gog
- tercera-persona
title: "Aragami, un juego de acci\xF3n y sigilo ya disponible en Linux"
---
Conviértete en un oscuro espíritu ninja en este juego de Lince Works, mezcla de buena trama, acción y sigilo. Linux está soportado desde su lanzamiento.

![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/aragami_banner.webp)


No abundan los ejemplos de juegos de este tipo en Linux. Aragami mezcla la acción y el sigilo en un título que llama la atención por su estética colorida. El juego recuerda poderosamente a otros títulos que nunca han llegado a Linux como Assasins Creed y similares.


Juegas como un espíritu ninja invocado por una chica que tiene que utilizar sus habilidades para esconderse entre las sombras y elementos del entorno para pasar desapercibido y poder acercarse a sus enemigos y poder acabar con ellos. El objetivo es liberar a Yamiko, que ha sido aprisionada en la ciudad fortificada de Kyuryu. 


Utilizando el poder de dominar las sombras para avanzar o atacar, tendrás que encontrarla y descubrir cual es tu identidad real.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/oOf_5cJcvk0" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Ya está disponible en [Steam](http://store.steampowered.com/app/280160/) y en [GOG](https://www.gog.com/game/aragami)


##### Acerca del juego


Eres Aragami, un espíritu vengativo con el poder de controlar las sombras. Has sido invocado por Yamiko, una chica aprisionada en la ciudad fortificada de Kyuryu. Embárcate en un oscuro viaje lleno de sangre y secretos para descubrir la verdad sobre los aragami. Infíltrate en la ciudad ocupada de Kyuryu con tus poderes sobrenaturales y combate la Luz con la Sombra. Descubre una historia de dos almas gemelas ligadas entre ellas por un destino que sobrepasa el tiempo y los recuerdos.
 
CONTROLA LAS SOMBRAS 
Crea tus propias sombras para ser invisible. Telepórtate de sombra en sombra mientras das caza a tus objetivos. Utiliza un amplio abanico de Poderes de Sombra para deshacerte de tus enemigos de manera creativa. 
ESCOGE TU ESTILO DE JUEGO 
Múltiples caminos para superar cada escenario y resolver cada situación. Juega como un emisario de la muerte sin piedad o un fantasma indetectable. Tu historia, tus decisiones. 
CONVIÉRTETE EN ARAGAMI 
Lucha contra Kaiho, el ejército de la Luz, y rescata a Yamiko – la misteriosa chica que te ha invocado y que es la clave de tu existencia.
 
##### Requisitos mínimos:


SO: Ubuntu Equivalent 64-bit Distro 
Procesador: 2 GHz 64-bit CPU 
Memoria: 4 GB de RAM 
Gráficos: OpenGL 3 Compatible GPU with 1GB Video RAM 
Almacenamiento: 6 GB de espacio disponible