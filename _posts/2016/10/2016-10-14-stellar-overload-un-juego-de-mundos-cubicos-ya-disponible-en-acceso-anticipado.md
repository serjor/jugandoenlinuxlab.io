---
author: Pato
category: Aventuras
date: 2016-10-14 19:19:04
excerpt: "<p>Es un juego de aventuras y creaci\xF3n con piezas de 20 cm. Sus creadores\
  \ acaban de lanzar la versi\xF3n Linux en Steam</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9ecd376e5371efaef9aad9bc9143aed8.webp
joomla_id: 54
joomla_url: stellar-overload-un-juego-de-mundos-cubicos-ya-disponible-en-acceso-anticipado
layout: post
tags:
- accion
- aventura
- steam
- acceso-anticipado
- exploracion
title: "Stellar Overload, un juego de mundos c\xFAbicos ya disponible en Acceso anticipado"
---
Es un juego de aventuras y creación con piezas de 20 cm. Sus creadores acaban de lanzar la versión Linux en Steam

Stellar Overload es un vistoso juego de aventura, exploración y construcción en un universo de mundos cúbicos. En él tendrás que ir descubriendo nuevos materiales y objetos con los que construir y avanzar de mundo en mundo. Aparte de esto, habrán personajes con los que interactuar, explorar zonas y craftear.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/DAKYQZn4wuo?list=PLKegiEyKUln4Bg5PoCBUmRfWGjLmtZLe_" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


El juego está todavía en Acceso Anticipado pero lo cierto es que pinta bien, y además ahora lo puedes conseguir con un 10% de rebaja.


Puedes encontrarlo en [[Steam](http://store.steampowered.com/app/397150/)]


 


¿Que piensas sobre Stellar Overload? Cuéntame en los comentarios.

