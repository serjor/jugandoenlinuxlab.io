---
author: Pato
category: Estrategia
date: 2016-10-21 21:26:44
excerpt: <p>El juego de estrategia de Paradox nos trae nuevo contenido.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/eb6c7c01c4e98e1f2578f9959463b973.webp
joomla_id: 69
joomla_url: stellaris-recibe-un-gran-parche-y-el-dlc-leviathans
layout: post
tags:
- steam
- estrategia
- contenido-descargable
title: Stellaris recibe un gran parche y el DLC Leviathans
---
El juego de estrategia de Paradox nos trae nuevo contenido.

El parche que nos trae Paradox trae una gran cantidad de cambios y nuevo contenido, por lo que si quieres saber todo su contenido lo mejor es que leas el [post oficial del lanzamiento](https://forum.paradoxplaza.com/forum/index.php?threads/heinlein-update-1-3-released-checksum-56ad-not-for-problem-reports.975869/). Es un gran cambio para el juego mejorándolo a todos los niveles.


El DLC "Leviathans" nos trae nuevas criaturas espaciales, nueva música, un nuevo evento "War in Heaven" que sumirá a toda la galaxia en guerra y mucho mas.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/bG0rc3x8Aew" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Para muchos, Stellaris se ha convertido por méritos propios en el mejor juego de estrategia disponible en Linux.


Puedes encontrar el DLC en Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/518910/" width="646"></iframe></div>


¿Que te parecen los cambios que trae el parche? ¿piensas pillar el DLC?


Cuéntamelo en los comentarios.

