---
author: Pato
category: Plataformas
date: 2016-10-19 21:19:21
excerpt: <p>Se trata de un juego dentro de un mundo abstracto de formas estilizadas
  y vivos colores.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/37a06e4a72d6cb27621f1ed829bbee81.webp
joomla_id: 63
joomla_url: spectrum-un-plataformas-peculiar-ya-disponible-en-linux
layout: post
tags:
- indie
- plataformas
- steam
title: '''Spectrum'' un plataformas peculiar ya disponible en Linux'
---
Se trata de un juego dentro de un mundo abstracto de formas estilizadas y vivos colores.

Controlas a una oscura entidad de tres ojos tratando de alcanzar portales mientras evitas diferentes obstáculos y elementos.


Escapa a través de 80 niveles repartidos en 8 misteriosos escenarios distintos, cada uno con su propia identidad.


Liam de GOL ha estado [testeandolo](https://www.gamingonlinux.com/articles/spectrum-an-abstract-platformer-now-on-linux-simple-in-looks-but-challenging.8275) y a tenor de sus sensaciones se trata de un juego del tipo "facil de jugar, dificil de dominar".


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/g1SUAQIC_ko" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Lo tienes ya disponible para Linux en Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/420360/" width="646"></iframe></div>

