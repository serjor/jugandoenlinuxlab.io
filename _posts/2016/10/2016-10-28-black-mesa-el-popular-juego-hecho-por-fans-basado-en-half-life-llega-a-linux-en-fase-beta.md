---
author: Pato
category: "Acci\xF3n"
date: 2016-10-28 22:07:10
excerpt: <p>Por fin Black Mesa llega a Linux. Y realmente pinta muy bien.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e9c724eeb5636d1c1c1a2c2e85d40377.webp
joomla_id: 81
joomla_url: black-mesa-el-popular-juego-hecho-por-fans-basado-en-half-life-llega-a-linux-en-fase-beta
layout: post
tags:
- accion
- primera-persona
- steam
- acceso-anticipado
title: '''Black Mesa'' el popular juego hecho por fans basado en Half Life llega a
  Linux en fase beta'
---
Por fin Black Mesa llega a Linux. Y realmente pinta muy bien.

Aunque sea el Acceso anticipado y en fase beta Black Mesa [[web oficial](http://www.blackmesagame.com/)] es un título que no podía faltar. Desde luego tiene mérito que Valve permita que los fans creen y publiquen un juego con su propio motor y basado en un juego oficial. No en vano Black Mesa tiene una valoración muy alta tanto en los comentarios de Steam como en Metacritic.


Todos sabéis que no soy fan de los juegos de disparos en primera persona pero eso no quita que pueda reconocer un buen título. 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/v2OH2OtjnhI" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


¿Que te parece Black Mesa? ¿Piensas pillarlo? 


Cuéntamelo en los comentarios.


Tienes ya disponible Black Mesa en Steam, con subtítulos en español:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/362890/" width="646"></iframe></div>

