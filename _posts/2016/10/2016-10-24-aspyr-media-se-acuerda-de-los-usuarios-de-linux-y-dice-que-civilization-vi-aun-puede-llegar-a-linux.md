---
author: Pato
category: Estrategia
date: 2016-10-24 21:36:36
excerpt: "<p>As\xED lo ha publicado la Compa\xF1\xEDa en un tweet.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/220c08548cac211cc7db219bb52f46cf.webp
joomla_id: 73
joomla_url: aspyr-media-se-acuerda-de-los-usuarios-de-linux-y-dice-que-civilization-vi-aun-puede-llegar-a-linux
layout: post
tags:
- estrategia
- aspyr-media
title: "Aspyr Media se acuerda de los usuarios de Linux y dice que Civilization VI\
  \ a\xFAn puede llegar a Linux"
---
Así lo ha publicado la Compañía en un tweet.

Sin muchas más explicaciones, Aspyr Media que últimamente anda más enfrascada con ports a Mac que a Linux ha publicado un escueto tweet en el que se acuerda de nosotros y nos dice que Civ VI aún puede llegar a nuestro sistema favorito.



> 
> Wanted to address a section of our fans that may feel left out recently. Dear Linux users, we haven't forgotten you! CivVI is still possible
> 
> 
> — Aspyr Media (@AspyrMedia) [24 de octubre de 2016](https://twitter.com/AspyrMedia/status/790623451660881920)







*"Buscamos contentar a una parte de nuestros fans que pueden sentirse olvidados reciéntemente. Queridos usuarios de Linux, ¡No os hemos olvidado! CivVI todavía es posible"*


No hay mucha mas información sobre el port. Hace tiempo que se anunció que Civilization VI [[Steam](http://store.steampowered.com/app/289070/)] tendría versión en Linux, pero después Aspyr Media, compañía encargada de traerlo a Mac y Linux dijo que [solo era una posibilidad](https://blog.aspyr.com/2016/10/04/sid-meiers-civilization-vi-faq/). Sin embargo, este nuevo anuncio parece indicar que algo se está moviendo.


¿Que piensas sobre esto? ¿Esperas que CivVI acabe llegando a Linux?


Sería un buen puntal. Actualmente es un juego muy popular con más de 100.000 usuarios jugando y está entre los 5 juegos más jugados en Steam.


Cuéntame en los comentarios.

