---
author: Pato
category: Plataformas
date: 2016-10-06 11:47:54
excerpt: "<p>El juego es una mezcla de acci\xF3n r\xE1pida y plataformas 2D con la\
  \ est\xE9tica de los juegos de los 90 donde abunda la sangre.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9b2c4b44fb86522964124ed80d03c5e8.webp
joomla_id: 39
joomla_url: butcher-de-transhuman-design-llega-a-linux-en-su-lanzamiento-es-brutal
layout: post
tags:
- accion
- plataformas
- pixel
title: Butcher de Transhuman Design llega a Linux en su lanzamiento. Es brutal!
---
El juego es una mezcla de acción rápida y plataformas 2D con la estética de los juegos de los 90 donde abunda la sangre.

En el juego eres un cyborg que tiene la misión de acabar con todo lo que se mueva. Elige tu arma (desde una motosierra hasta un lanzagranadas) y lánzate a masacrar a tus víctimas en un ambiente apocalíptico de ciudades debastadas, junglas etc. Si te sientes creativo, puedes eliminar a tus enemigos mediante sierras, pozos de lava o ganchos.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/FhoQljEmEP0" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


El juego no está traducido en estos momentos, pero con ese argumento... ¿quien lo necesita? 


Además, está con un 10% de descuento por su lanzamiento. ¿Piensas darle una oportunidad? Cuéntame en los comentarios


Puedes comprarlo en [[GOG](https://www.gog.com/game/butcher)] o  [[Steam](http://store.steampowered.com/app/474210/)]

