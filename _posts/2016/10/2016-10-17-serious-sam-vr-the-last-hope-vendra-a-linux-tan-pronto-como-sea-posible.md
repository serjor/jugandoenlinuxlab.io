---
author: Pato
category: "Acci\xF3n"
date: 2016-10-17 21:18:21
excerpt: "<p>Croteam nos traer\xE1 el nuevo t\xEDtulo en cuanto la Realidad Virtual\
  \ sea viable en Linux.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/19f9cefdfb07230a68581d617885a3af.webp
joomla_id: 60
joomla_url: serious-sam-vr-the-last-hope-vendra-a-linux-tan-pronto-como-sea-posible
layout: post
tags:
- accion
- primera-persona
- acceso-anticipado
- realidad-virtual
- steamvr
- vulkan
title: "'Serious Sam VR: The last Hope' vendr\xE1 a Linux tan pronto como sea posible"
---
Croteam nos traerá el nuevo título en cuanto la Realidad Virtual sea viable en Linux.

Buenas noticias para los que esperan a Sam el Serio en Linux. 'Serious Sam VR: The Last Hope' el nuevo título que Croteam [[web oficial](http://www.croteam.com/)] está desarrollando para los dispositivos de realidad virtual tendrá una versión en Linux en cuanto técnicamente sea posible, gracias a la tecnología de Vulkan.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/1UHykMkH-VQ" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Tal y como se [mostró](index.php/item/28-steam-dev-days-el-soporte-para-la-realidad-virtual-en-linux-practicamente-es-un-hecho) en los Steam Dev Days, ya se está trabajando para traer la realidad virtual a Linux e incluso se pudo ver una demo en funcionamiento, de tal manera que Croteam afirma que gracias a esto soportarán SteamVR en Linux.


Del [foro del juego](http://steamcommunity.com/app/465240/discussions/0/350543368756545137/#c348293230313547574) en Steam:



> 
> Yes, seen that demo. We will be supporting SteamVR on Linux via Vulkan as soon as it becomes available.
> 
> 
> 


Croteam es una compañía que ha lanzado sus juegos en Linux siempre que ha podido, y parece que Serious Sam VR no va a ser una excepción.


Puedes echarle un ojo al bueno de Sam en Steam, aún en acceso anticipado:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/465240/" width="646"></iframe></div>


¿Esperas a la realidad virtual en Linux? ¿qué te parece este nuevo Serious Sam?


Cuéntamelo en los comentarios.

