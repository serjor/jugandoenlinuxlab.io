---
author: Pato
category: Hardware
date: 2016-10-12 23:10:17
excerpt: "<p>Steam comenzar\xE1 a soportar mandos de otras compa\xF1\xEDas dentro\
  \ de su propia API comenzando por el de PS4</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/fa55c8bad0e242eb7986dc1135b50adb.webp
joomla_id: 49
joomla_url: steam-dev-days-valve-espera-haber-vendido-1-000-000-de-steam-controllers-para-principios-del-proximo-ano-y-dar-soporte-a-otros-mandos-en-steam
layout: post
tags:
- proximamente
- steam
- steam-controller
title: "Steam Dev Days: Valve espera haber vendido 1.000.000 de Steam Controllers\
  \ para principios del pr\xF3ximo a\xF1o y dar soporte a otros mandos en Steam"
---
Steam comenzará a soportar mandos de otras compañías dentro de su propia API comenzando por el de PS4

1.000.000 de Steam Controllers es la impresionante cifra que Valve espera haber vendido para principios del próximo año 2017. Y no es de extrañar, ya que hablamos de un mando que ha sido todo un soplo de aire fresco en el panorama de periféricos de juego con sus novedosas características y sus grandes posibilidades de personalización y programación.


En una de las charlas del Steam Dev Days han publicado algunos gráficos interesantes, como el plano temporal del progreso del Steam Controller:


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdevdays/SCtimeline.webp)


Lo interesante es ver que este mismo mes de octubre en el que se supone tendremos una gran actualización del cliente de Steam también se dará soporte a dispositivos de otras compañías dentro de su propia API. Este punto ha dado para mas de un comentario en Twitter:



> 
> Steam: We are adding support for all popular controllers, better discovery system, and a new UI.   
>   
> W10 Store: You can pause downloads now.
> 
> 
> — George Oscar Bluth (@johnd0p3) [12 de octubre de 2016](https://twitter.com/johnd0p3/status/786309523761922048)







Todo parece indicar que el primer mando de otra compañía que obtendrá soporte en la API de Steam será el de Playstation 4, a tenor de una imagen de configuración hecha pública:


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdevdays/ps4config.webp)


Por otra parte, los números de ventas también han tenido su propio gráfico:


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdevdays/SCsales.webp)


Es importante ver como hay unos 27.000 jugadores diarios que utilizan el mando. Desde luego Valve ha conseguido un buen éxito.


Fuente: [Gamasutra](http://www.gamasutra.com/blogs/LarsDoucet/20161012/283057/Steam_Dev_Days_Steam_Controller.php)

