---
author: Pato
category: Software
date: 2016-10-27 22:56:42
excerpt: "<p>No era algo que se esperara tan pronto, ni que fueran a implementarlo\
  \ en alg\xFAn port actual.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/00d9b1e39f02d57be65ad2a9a6eaa3b8.webp
joomla_id: 79
joomla_url: feral-afirma-que-esperan-implementar-vulkan-en-la-primera-mitad-del-ano-que-viene
layout: post
tags:
- feral-interactive
title: "Feral afirma que esperan implementar Vulkan en la primera mitad del a\xF1\
  o que viene"
---
No era algo que se esperara tan pronto, ni que fueran a implementarlo en algún port actual.

¡Buenas noticias para los que jugamos en Linux! Feral ha anunciado durante una de sus retransmisiones en Twitch que Deus Ex Mankind Divided vendrá con soporte OpenGL, pero que esperan poder implementar Vulkan para la primera mitad del año que viene:



> 
> Linux gamers! We just announced, live on Twitch, that we are looking towards Vulkan implementation in the first half of next year.
> 
> 
> — Feral Interactive (@feralgames) [27 de octubre de 2016](https://twitter.com/feralgames/status/791711184818999296)







Esto abre la posibilidad de que los nuevos títulos en los que trabajen vengan también con soporte para Vulkan, lo que puede ser interesante de cara a ver la ganancia de rendimiento que obtenemos frente a OpenGL y sobre todo frente a DirectX 12.


Hay que tener en cuenta que aunque los juegos implementen Vulkan eso no significa un aumento de rendimiento automático, si no que dependerá de otros factores como el motor del juego, como se haya diseñado, como se da el soporte para la API etc. Se avecinan tiempos interesantes para los juegos que nos vienen.


¿Que piensas de esto? Cuéntamelo en los comentarios.

