---
author: Pato
category: "Simulaci\xF3n"
date: 2016-10-21 22:57:47
excerpt: "<p>El simulador de combate seguir\xE1 teniendo soporte en la rama experimental.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/22c02097e4438bd2f2f3fe4a6a3ab0e1.webp
joomla_id: 70
joomla_url: arma-3-seguira-teniendo-soporte-en-linux-y-bohemia-planea-pasarlo-a-64-bits
layout: post
tags:
- accion
- steam
- simulador
title: "ArmA 3 seguir\xE1 teniendo soporte en Linux y Bohemia planea pasarlo a 64\
  \ bits"
---
El simulador de combate seguirá teniendo soporte en la rama experimental.

Buenas noticias para los seguidores de ArmA 3 en Linux. Bohemia Interactive ha publicado las líneas de desarrollo futuro para el simulador de combate militar desvelando que el desarrollo en Linux seguirá avanzando dentro de la rama experimental, desarrollada por Virtual Programming.


Puedes ver el anuncio del desarrollo [en su web](https://arma3.com/news/arma-3-roadmap-2016-17#.WAqcJhKLQ8o) (en inglés).


Además de eso, planean pasar el simulador a los 64 bits, aunque no dejan claro si será en todos los sistemas simultáneamente o irán haciendo la migración sistema a sistema.


De todos modos, aunque es bueno ver que Bohemia sigue dando apoyo a la versión de Linux hay que tener en cuenta que aún no está considerada como una versión oficial.


Puedes encontrar ArmA 3 en Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/107410/31539/" width="646"></iframe></div>

