---
author: Pato
category: Plataformas
date: 2016-09-13 18:39:53
excerpt: "<p>Rive es un juego de disparos con dos sticks fren\xE9tico que acaba de\
  \ lanzarse en Steam. Sus creadores son Two Tribes, estudio detr\xE1s de juegos exitosos\
  \ como Toki Tori, pero este ser\xE1 su \xFAltimo juego.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/94d43e327d9303539cb1e2aac7032668.webp
joomla_id: 21
joomla_url: rive-sera-el-ultimo-juego-de-two-trives-publicado-ya-en-steam
layout: post
tags:
- accion
- indie
- plataformas
title: "Rive ser\xE1 el \xFAltimo juego de Two Trives. Publicado ya en Steam"
---
Rive es un juego de disparos con dos sticks frenético que acaba de lanzarse en Steam. Sus creadores son Two Tribes, estudio detrás de juegos exitosos como Toki Tori, pero este será su último juego.

 


Two Trives tiene una grán trayectoria detrás en lo que se refiere a publicación de juegos en Linux ya que han dado soporte a todos sus títulos desde el 2012. Según han anunciado en un [artículo](http://twotribes.com/message/rive-is-our-final-game/) en su web donde explican los motivos que les han llevado a tomar esta dura decisión. Sin embargo no explican que pasaría si Rive se convierte en un grán éxito a nivel comercial. ¿Se replantearían su postura actual?


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/VneN_eKTo_c" width="640"></iframe></div>


 


El juego está disponible ya en [Steam](http://store.steampowered.com/app/278100/).


{AimyChartsBar} Apple; This Year’s Harvest; 45; #339900


Apple; Last Year’s Harvest; 40; #70B84D


Cherry; This Year’s Harvest; 30;


Cherry; Last Year’s Harvest; 28;


Peach; This Year’s Harvest; 25;


Peach; Last Year’s Harvest; 20; {/AimyChartsBar}

