---
author: Pato
category: Carreras
date: 2016-09-08 21:58:21
excerpt: "<p style=\"text-align: justify;\">Rocket League est\xE1 ya disponible para\
  \ SteamOS! Por fin despu\xE9s de largo tiempo de espera y varios retrasos de por\
  \ medio, el juego de Psyonix por fin est\xE1 disponible en nuestro sistema favorito!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e0a70f72bdae9885bfc32d7cd19a26a1.webp
joomla_id: 20
joomla_url: rocket-league-llega-a-linux-y-steamos-en-fase-beta
layout: post
tags:
- accion
- deportes
- steamos
- carreras
title: Rocket League llega a Linux y SteamOS en fase beta
---
Rocket League está ya disponible para SteamOS! Por fin después de largo tiempo de espera y varios retrasos de por medio, el juego de Psyonix por fin está disponible en nuestro sistema favorito!

Si bien es cierto que llega en fase beta y oficialmente solo está soportado en SteamOS, esto no debe ser impedimento para que funcione correctamente en otras distribuciones.


De todos modos, y según el anuncio del lanzamiento es posible que tengamos algunos problemas de salidas al menú, lentitud de carga u otros problemas, pero al ser un producto que aún está en desarrollo esperemos que estos problemas vayan desapareciendo.


No han tardado en aparecer las primeras reviews del juego en diversos medios. A continuación tenéis un vídeo de The linux gamer como ejemplo:


 


<div class="resp-iframe"><iframe height="360" seamless="seamless" src="https://www.youtube.com/embed/M4cTCaVKJFg" width="640"></iframe></div>


 


Podéis ver y comprar el juego en su [página de la tienda de Steam](http://store.steampowered.com/app/252950/).

