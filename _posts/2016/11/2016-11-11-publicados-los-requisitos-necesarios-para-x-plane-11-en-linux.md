---
author: Pato
category: "Simulaci\xF3n"
date: 2016-11-11 22:17:17
excerpt: "<p>El simulador de aviaci\xF3n civil estar\xE1 disponible en Linux al igual\
  \ que su antecesor.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ba1b7eb9b8ad142948e3b9dce300b4c6.webp
joomla_id: 102
joomla_url: publicados-los-requisitos-necesarios-para-x-plane-11-en-linux
layout: post
tags:
- proximamente
- simulador
title: Publicados los requisitos necesarios para X-Plane 11 en Linux
---
El simulador de aviación civil estará disponible en Linux al igual que su antecesor.

Laminar Research sigue publicando contenidos sobre el que será su próximo simulador de aviación civil, X-Plane 11 del que [ya nos hicimos eco](item/27-anunciado-x-plane-11-el-simulador-aereo-saldra-este-mismo-ano-con-version-linux-confirmada) de su llegada a Linux. En concreto han hecho públicos los requisitos del sistema para poder ejecutar el simulador:


#### Requisitos mínimos


CPU: Intel Core I3, i5 o i7 con 2 o más núcleos, o procesador AMD equivalente. (las CPUs de doble núcleo que estén por debajo de los 3 GHz deben probar la demo antes de comprar el simulador).


RAM: 8 gigas


Gráfica: Nvidia, AMD o Intel con al menos 512 megas de VRAM


#### Requisitos recomendados:


CPU: Intel core i5 6600K a 3,5 GHz o superior


RAM: 16 - 24 Gigas o más


Gráfica: Nvidia, AMD o Intel con al menos 4GB de VRAM (Geforce GTX 1070 o superior, o equivalente AMD)


#### Tarjetas gráficas sopotadas:


Nvidia Geforce GTX 400 series o superior (recomendadas GTX 600 series o superior)


AMD HD5000 o superior (recomendadas HD7000 con GCN o superiores)


Intel HD 2000 o superior (recomendadas HD 4000 o superior)


 


En cuanto a los requisitos específicos para Linux, Laminar Research dice:


"Aunque X-Plane correrá en Linux no soportaremos distribuciones específicas. Si quieres ejecutar X-Plane 11 en Linux tendrás que probarlo en tu distribución para saber si es compatible. Con esto en mente, tenemos desarrolladores utilizando Ubuntu 14.04 y 16.04 LTS de manera satisfactoria.


Para Linux requerimos los drivers propietarios para tarjetas Nvidia y AMD para ejecutar X-Plane 11. Puede que consigas ejecutar X-Plane 11 con los drivers Mesa/Gallium en tarjetas Intel, pero no están soportados oficialmente. No soportamos de ningún modo los drivers Open Source para las tarjetas Nvidia o AMD"


Tienes toda la información [en este enlace](http://www.x-plane.com/kb/x-plane-11-system-requirements/) (en inglés).


¿Que te parecen los requisitos para Linux de X-Plane 11? ¿le darás una oportunidad?


Cuéntame lo que piensas sobre X-Plane 11 en los comentarios.

