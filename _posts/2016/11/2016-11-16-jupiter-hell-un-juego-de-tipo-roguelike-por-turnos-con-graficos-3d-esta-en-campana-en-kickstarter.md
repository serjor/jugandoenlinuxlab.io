---
author: Pato
category: Estrategia
date: 2016-11-16 15:17:38
excerpt: "<p>El juego est\xE1 siendo desarrollado por el mismo desarrollador que hizo\
  \ DoomRL.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a63cd9e38964634741a5a3fe89055308.webp
joomla_id: 108
joomla_url: jupiter-hell-un-juego-de-tipo-roguelike-por-turnos-con-graficos-3d-esta-en-campana-en-kickstarter
layout: post
tags:
- indie
- rol
- estrategia
- kickstarter
title: "Jupiter Hell, un juego de tipo \"Roguelike\" por turnos con gr\xE1ficos 3D\
  \ est\xE1 en campa\xF1a en Kickstarter"
---
El juego está siendo desarrollado por el mismo desarrollador que hizo DoomRL.

Jupiter Hell ya está en campaña en Kickstarter y la verdad es que el juego tiene muy buena pinta, y no solo por su aspecto visual si no también por que tendrá soporte completo en Linux. Según el propio desarrollador que también nos trajo [DoomRL](https://doom.chaosforge.org/) se trata de un juego de tipo "Rogelike" en 3D por turnos en un universo de ciencia ficción de estilo de los 90'. Tendrá "muerte permanente", niveles procedurales y gráficos 3D de gran calidad.


Jupiter Hell necesita 60.000 libras para financiar con éxito su campaña y en el momento de escribir estas líneas lleva recaudados casi 18.000 con 27 días aún para terminar la campaña, por lo que parece que alcanzará su objetivo sin muchos problemas.


<div class="resp-iframe"><iframe height="360" seamless="seamless" src="https://www.kickstarter.com/projects/2020043306/jupiter-hell-a-modern-turn-based-sci-fi-roguelike/widget/video.html" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


El lanzamiento del juego con soporte confirmado en Linux está planeado para el tercer cuarto del próximo año 2017, con acceso anticipado para los contribuyentes en enero.


Tienes toda la info sobre Jupiter Hell en su [página de Kickstarter](https://www.kickstarter.com/projects/2020043306/jupiter-hell-a-modern-turn-based-sci-fi-roguelike) y en la [página oficial](https://jupiterhell.com/) del juego.


¿Que te parece la propuesta de Jupiter Hell? ¿Apoyarás su campaña en Kickstarter?


Cuéntamelo en los comentarios.

