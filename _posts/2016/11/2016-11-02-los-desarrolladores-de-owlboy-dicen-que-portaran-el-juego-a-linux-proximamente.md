---
author: Pato
category: Aventuras
date: 2016-11-02 22:45:50
excerpt: "<p>En Septiembre los desarrolladores ten\xEDan intenci\xF3n de utilizar\
  \ FNA para portar el juego a Linux.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/deb9f9efc56ef2a940bdf0d58ccaad5c.webp
joomla_id: 84
joomla_url: los-desarrolladores-de-owlboy-dicen-que-portaran-el-juego-a-linux-proximamente
layout: post
tags:
- accion
- indie
- aventura
- proximamente
title: "Los desarrolladores de Owlboy dicen que portar\xE1n el juego a Linux pr\xF3\
  ximamente"
---
En Septiembre los desarrolladores tenían intención de utilizar FNA para portar el juego a Linux.

Ahora que Owlboy [[sitio oficial](http://www.owlboygame.com/)] ya ha sido publicado en Windows los desarrolladores han vuelto a afirmar en los [foros de Steam](http://steamcommunity.com/app/115800/discussions/0/359547436744737487/?ctp=2#c312265782626144442) que tienen intención de portar el juego a Linux.


Owlboy es un interesante juego 2D de aventuras y acción con un aspecto 'pixel art':


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/fkeH5gbcjMU" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


¿Te gustaría que Owlboy llegase a Linux? Cuéntamelo en los comentarios.

