---
author: Pato
category: Rol
date: 2016-11-10 16:36:25
excerpt: "<p>El nuevo juego de Rol de Obsidian, creadores de Pillars of Eternity llega\
  \ con interfaz y subt\xEDtulos en espa\xF1ol.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/aaa082d2257ab65aecf61c2340e9c5b9.webp
joomla_id: 98
joomla_url: tyranny-ya-disponible-con-soporte-en-linux-steamos-de-salida
layout: post
tags:
- rol
- steam
title: Tyranny ya disponible con soporte en Linux/SteamOS de salida
---
El nuevo juego de Rol de Obsidian, creadores de Pillars of Eternity llega con interfaz y subtítulos en español.

Las primeras impresiones del juego están siendo bastante positivas.


Sinopsis del juego:


En Tyranny, la gran guerra entre el bien y el mal ha llegado a su fin, y las fuerzas del mal, lideradas por el Señor Supremo Kyros, han salido victoriosas. Los ejércitos implacables del Señor Supremo dominan el mundo y sus habitantes deben encontrar su lugar en este reino asolado por la guerra... incluso mientras la discordia empieza a aflorar entre las filas de los arcontes más poderosos de Kyros. Los jugadores descubrirán este nuevo mundo bajo el poder del Señor Supremo, en un juego de rol (RPG) reactivo, interactuando con los habitantes y encarnando a un poderoso forjadestinos que trabaja al servicio del Señor Supremo; este forjadestinos irá recorriendo las tierras para inspirar lealtad y miedo mientras va controlando los últimos resquicios de resistencia en las Cotas. 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/Sg0WsR3EnGg" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Requisitos del Sistema:


Mínimos:


* **SO:** 14.04 LTS
* **Procesador:** Intel Core 2 Quad Q9505 @ 2.80 GHz / AMD Athlon II X4 840 @ 3.10 GHz
* **Memoria:** 6 GB de RAM
* **Gráficos:** ATI Radeon HD 5770 or NVIDIA GeForce GTS450 with 1GB VRAM
* **Almacenamiento:** 15 GB de espacio disponible


Requisitos recomendados: 


* **SO:** 14.04 LTS
* **Procesador:** Intel Core i3-2100 @ 3.10 GHz / AMD Phenom II X4 955 @ 3.10 GHz
* **Memoria:** 8 GB de RAM
* **Gráficos:** Radeon HD 6850 or NVIDIA GeForce GTX 560 with 1GB VRAM
* **Almacenamiento:** 15 GB de espacio disponible


Puedes comprar Tyranny en [GOG](https://www.gog.com/game/tyranny_commander_edition_preorder), en la [Humble Store](https://www.humblebundle.com/store/tyranny-commander-edition) o diréctamente en la página del juego en Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/362960/" width="646"></iframe></div>


¿Piensas jugar a Tyranny? ¿Te gustan este tipo de juegos de Rol? Cuéntamelo en los comentarios.


¿Tienes Tyranny y quieres que publiquemos tu review? ¡[Envíanosla](contacto/envia-un-articulo)!

