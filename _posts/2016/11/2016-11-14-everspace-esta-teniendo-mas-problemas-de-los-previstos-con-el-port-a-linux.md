---
author: Pato
category: "Acci\xF3n"
date: 2016-11-14 18:57:20
excerpt: "<p>Los desarrolladores has posteado que no saben cuando ser\xE1n resueltos\
  \ los problemas.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ea457adccaa9e569cff05de9b4f3b04d.webp
joomla_id: 104
joomla_url: everspace-esta-teniendo-mas-problemas-de-los-previstos-con-el-port-a-linux
layout: post
tags:
- accion
- indie
- espacio
- acceso-anticipado
title: "EVERSPACE est\xE1 teniendo \"m\xE1s problemas de los previstos\" con el port\
  \ a Linux"
---
Los desarrolladores has posteado que no saben cuando serán resueltos los problemas.

EVERSPACE, [[Steam](http://store.steampowered.com/app/396750)][[página oficial](https://everspace-game.com/)] el prometedor juego de acción espacial actualmente en acceso anticipado que sacó adelante una campaña de Kickstarter donde se prometía soporte para Linux si se alcanzaban los 275.000€ está teniendo diversos problemas con su port a Linux. Al final, la campaña acabó recaudando mas de 420.000€ por lo que algunos usuarios han preguntado en los foros del juego en Steam por el estado del port, a lo que uno de los desarrolladores ha respondido que están teniendo diversos problemas con el cursor del ratón, como que se queda centrado aunque intenten desabilitarlo, queda descentrado cuando reescalan el tamaño de la ventana o que se queda anclado en una zona pequeña y no se puede utilizar en toda la pantalla. Además, afirma tener problemas con la pantalla completa que no funciona natívamente, parpadeos masivos o problemas de precisión en los shaders por que están usando una configuración de unidades demasiado grande para OpenGL pero que funciona bien con DirectX. 


Según afirma, muchos de estos problemas pueden ser resueltos, pero aún permanecen muchos problemas de iluminación con los materiales oscuros o claros. Termina su comentario con: "No es seguro si y cuando estos problemas pueden ser resueltos, es tedioso."


Puedes ver el hilo de esta conversación [en este enlace](http://steamcommunity.com/app/396750/discussions/0/333656722966793785/#c217691032443146542). (en inglés)


No sería el primer caso de desarrolladores que prometen soporte en Linux/SteamOS y luego este soporte se cae por el camino, ya sea por más dificultades de las esperadas a la hora de hacer estos ports (algunos creen que dándole a un botón se hace solo) o por falta de recursos o conocimientos, lo que no quiere decir que este EVERSPACE no llegue a publicarse en Linux/SteamOS.


De todos modos desde Jugando En Linux recomendamos que no os dejeis llevar por promesas o campañas, si no que esperéis a tener al menos una demo jugable o algo tangible (a poder ser el juego completo) antes de aportar dinero a un proyecto que luego puede dejar a los usuarios de Linux por el camino.


¿Que piensas sobre el port de EVERSPACE? ¿Participaste en la campaña? ¿te gustaría jugarlo en Linux?


Cuéntamelo en los comentarios.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/WlJrokiuSO4" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 

