---
author: Pato
category: Terror
date: 2016-11-07 18:50:06
excerpt: "<p>El juego est\xE1 siendo desarrollado por el mismo estudio que realiz\xF3\
  \ el aclamado STASIS, que tambi\xE9n tiene posibilidades de llegar a Linux.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/68b62085e41e8f225811766f8d5eb2bb.webp
joomla_id: 91
joomla_url: cayne-un-juego-gratuito-de-terror-en-vista-isometrica-llegara-a-linux-proximamente
layout: post
tags:
- proximamente
- steam
- terror
- aventura-grafica
- juego-gratuito
title: "'CAYNE' un juego gratuito de terror en vista isom\xE9trica llegar\xE1 a Linux\
  \ pr\xF3ximamente"
---
El juego está siendo desarrollado por el mismo estudio que realizó el aclamado STASIS, que también tiene posibilidades de llegar a Linux.

CAYNE es un juego del tipo "apuntar y hacer clic" basado en el mismo universo que STASIS con vista isométrica y una inquietante historia. El juego será independiente por lo que lo podrás jugar sin haber jugado el anterior.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/9f99QQf4l_w" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


#### Sinopsis del juego:


*Hadley se despierta en una instalación. Está embarazada de 9 meses, y ellos quieren a su bebé...*


*Regresa al universo distópico de STASIS con esta emocionante aventura terrorífica GRATIS.*  
*Te espera una intrépida experiencia. STASIS fue solo el comienzo de una historia mucho más larga.*


  
*CARACTERÍSTICAS*  
*Juego de aventura de apuntar y hacer clic, con toque moderno*   
*Fondos isométricos HD procesados maravillosamente*   
*Personajes 3D totalmente animados*   
*FMV con animación de calidad*   
*Voces dobladas por actores profesionales*   
*Sistema de traducción abierto*   
*Sistemas informáticos que interactúan con el mundo del juego*   
*Puzles intuitivos para probar tus habilidades deductivas*   
*Una manera innovadora de contar de nuevo el clásico juego de aventura*   
*Una historia adicional ambientada en el mundo de STASIS*   
*Compatible con monitor 6:10*


El juego llegará a Linux con soporte desde su lanzamiento y será gratuito.


Por otra parte, los desarrolladores han avisado que aún no han tirado la toalla con el port de [STASIS](http://store.steampowered.com/app/380150) a Linux. Se encuentran a la espera de que se introduzcan algunos arreglos en el motor del juego para ultimar un lanzamiento en nuestra plataforma. Según puedes leer en [este hilo](http://steamcommunity.com/app/380150/discussions/0/541907867777059868/?ctp=14) (en inglés) de su página de Steam, buscarán jugadores para testear los dos juegos llegado el momento.


El estudio se encuentra también en fase de publicar su nuevo trabajo en Kickstarter llamado "Beautiful Desolation"[[página oficial](http://www.desolationgame.com/)] para recabar fondos de cara a su próximo lanzamiento. La campaña de financiación llegará el día 24 de Enero, justo cuando se espera la llegada de CAYNE a nuestros sistemas.


Podrás instalar CAYNE gratis desde Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/532840/" width="646"></iframe></div>

