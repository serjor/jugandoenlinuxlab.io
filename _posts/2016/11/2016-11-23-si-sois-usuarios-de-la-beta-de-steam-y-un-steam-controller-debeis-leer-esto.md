---
author: leillo1975
category: Hardware
date: 2016-11-23 20:29:34
excerpt: <p>Deberemos modificar las reglas de "udev" para poder usar correctamente
  nuestro Steam Controller.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b8292acafd72142128a3481ac4b0abff.webp
joomla_id: 119
joomla_url: si-sois-usuarios-de-la-beta-de-steam-y-un-steam-controller-debeis-leer-esto
layout: post
tags:
- steam
- steam-controller
- valve
title: Si sois usuarios de la Beta de Steam y un Steam Controller debeis leer esto
---
Deberemos modificar las reglas de "udev" para poder usar correctamente nuestro Steam Controller.

Según leo en [este anuncio](http://steamcommunity.com/games/353370/announcements/detail/612883076669149884), desde la actualización de la **Beta 11/22** de nuestro cliente de Steam, se ha cambiado en nuestro Steam Controller la forma de comunicarse via USB/Bluetooth, para facilitar la funcionalidad de streaming en casa de este. Para ello es necesario modificar las reglas de "udev" en consecuencia.


Debemos modificar el siguiente archivo (con permisos de administrador) con nuestro editor favorito:


 


*/lib/udev/rules.d/99-steam-controller-perms.rules*


 


y luego modificar su contenido teniendo mucho cuidado de poner en GROUP un grupo al que pertenezca el usuario que ejecuta Steam (si queremos saber a que grupos pertenecemos usaremos el comando "groups" y luego nuestro nombre de usuario). En mi caso he usado el grupo "steamcontroller".


 


*# This rule is needed for basic functionality of the controller in Steam and keyboard/mouse emulation*  
*SUBSYSTEM=="usb", ATTRS{idVendor}=="28de", MODE="0666"*


*# This rule is necessary for gamepad emulation; make sure you replace 'pgriffais' with a group that the user that runs Steam belongs to*  
*KERNEL=="uinput", MODE="0660", GROUP="steamcontroller", OPTIONS+="static_node=uinput"*


*# DualShock 4 wired*  
*SUBSYSTEM=="usb", ATTRS{idVendor}=="054c", ATTRS{idProduct}=="05c4", MODE="0666"*  
*# DualShock 4 wireless adapter*  
*SUBSYSTEM=="usb", ATTRS{idVendor}=="054c", ATTRS{idProduct}=="0ba0", MODE="0666"*  
*# DualShock 4 slim wired*  
*SUBSYSTEM=="usb", ATTRS{idVendor}=="054c", ATTRS{idProduct}=="09cc", MODE="0666"*


*# Valve HID devices over USB hidraw*  
*KERNEL=="hidraw\*", ATTRS{idVendor}=="28de", MODE="0666"*


*# Valve HID devices over bluetooth hidraw*  
*KERNEL=="hidraw\*", KERNELS=="\*28DE:\*", MODE="0666"*


*# DualShock 4 over bluetooth hidraw*  
*KERNEL=="hidraw\*", KERNELS=="\*054C:05C4\*", MODE="0666"*


*# DualShock 4 Slim over bluetooth hidraw*  
*KERNEL=="hidraw\*", KERNELS=="\*054C:09CC\*", MODE="0666"*


 


Recordad, por ahora es para los usuarios del cliente Beta.


 

