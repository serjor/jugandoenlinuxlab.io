---
author: Pato
category: Aventuras
date: 2016-11-11 21:40:51
excerpt: "<p>Se trata de un fant\xE1stico juego hecho a base de t\xE9cnicas de dibujo\
  \ a mano. Est\xE1 cosechando excelentes cr\xEDticas.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2a14beb1aee2d71c6fecb12f25c690f7.webp
joomla_id: 101
joomla_url: candle-el-juego-del-estudio-espanol-teku-studios-ya-disponible-en-linux-steamos
layout: post
tags:
- indie
- aventura
- fantasia
- puzzles
title: "'Candle' el juego del estudio espa\xF1ol Teku Studios ya disponible en Linux/SteamOS"
---
Se trata de un fantástico juego hecho a base de técnicas de dibujo a mano. Está cosechando excelentes críticas.

Como [ya anunciamos](item/35-candle-el-primer-juego-de-teku-studios-sera-publicado-el-11-de-noviembre) en Jugando en Linux Candle, el primer juego de los españoles Teku Studios ha sido publicado hoy y las críticas están siendo bastante positivas.


Recordar que se trata de un juego dibujado a mano pantalla por pantalla lo que le da un aspecto fuera de lo normal.


#### Sinopsis del Juego:


*Candle es una aventura con puzles desafiantes. Ponte en la piel de Teku, un joven aprendiz que emprende un peligroso viaje para rescatar al chamán de su tribu de los malvados Wakcha. Pero su camino está plagado de siniestras trampas y obstáculos difíciles de sortear. Para superar estos retos, has de ser astuto y observar tu entorno; de lo contrario, el siguiente paso podría ser el último.*


*Sin embargo, Teku tiene algo que le hace especial: su mano izquierda es una vela. Deja que sea esta resplandeciente llama la que ahuyente a tus enemigos y arroje luz sobre lugares oscuros.*


*Los gráficos pintados a mano con acuarelas otorgan a Candle un toque especial, pues todos los fondos y personajes se han dibujado cuidadosamente para luego ser escaneados imagen a imagen. El juego parece un cuadro en constante movimiento.*


#### Características:


*Fíjate en cada detalle para resolver desafiantes puzles*   
*Mantente siempre alerta, ya que en cualquier momento puedes caer en una trampa*   
*Tienes el poder de la llama en la palma de tu mano*   
*Las acuarelas pintadas a mano cobran vida como una pintura en movimiento*   
*Preciosa banda sonora de inspiración suramericana*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/sCWPiHSUQ9g" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Candle está ya disponible para Linux/SteamOS completamente traducido al español, y puedes comprarlo en [GOG](https://www.gog.com/game/candle) o en su página de Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/420060/" width="646"></iframe></div>


¿Que te parece la propuesta de Candle? ¿piensas jugarlo?


Cuéntamelo en los comentarios.


¿Tienes el juego y quieres que publiquemos tu review? ¡[Mándanosla](contacto/envia-un-articulo)!

