---
author: Pato
category: Terror
date: 2016-11-21 18:13:25
excerpt: "<p>El juego de terror espacial llegar\xE1 a Linux tan pronto como estos\
  \ bugs sean resueltos en pr\xF3ximas implementaci\xF3nes del motor del juego.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/938a195f8810cb9b31c6503221891897.webp
joomla_id: 115
joomla_url: syndrome-esta-a-la-espera-de-la-resolucion-de-algunos-bugs-en-unity-para-llegar-a-linux
layout: post
tags:
- proximamente
- steam
- terror
title: "'Syndrome' est\xE1 a la espera de la resoluci\xF3n de algunos bugs en Unity\
  \ para llegar a Linux"
---
El juego de terror espacial llegará a Linux tan pronto como estos bugs sean resueltos en próximas implementaciónes del motor del juego.

Según el comunicado en los foros del juego en Steam, Syndrome [[Steam](http://store.steampowered.com/app/409320/)] está a la espera de solucionar algunos problemas con los shaders que se introdujeron al actualizar el motor Unity para dar soporte a Linux.


En concreto, [en este hilo](http://steamcommunity.com/app/409320/discussions/0/343788552534254406/?tscn=1479497404#c208684375417696711) el desarrollador explica el retraso de la versión para Linux y algunos comentarios de otros desarrolladores que también están a la espera o en contacto con Unity para la resolución de estos problemas.


Todo parece indicar que en próximas actualizaciones de Unity estos bugs serán resueltos y entonces tendremos disponible este 'Syndrome', que la verdad para los que gusten de pasar malos ratos en una estación espacial no tiene para nada mala pinta. A tenor de los comentarios en Steam el juego merece la pena siendo un juego del tipo de Alien Isolation, también disponible en Linux.


#### Sinopsis del juego:


Te despiertas aturdido y confuso, probablemente por los efectos del criosueño.   
Al principio no ves a nadie y enseguida empiezas a encontrarte los cadáveres de los miembros de la tripulación.   
La nave va a la deriva, y la cosa o persona que mató a la tripulación seguramente sigue aquí.


Syndrome es un juego de terror y supervivencia ambientado en un universo de ciencia ficción a bordo de la "Valkenburg", una nave de exploración científica equipada con los sistemas más avanzados de la flota Novacore.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/w9tfddfaLQc" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


¿Que te parece este Syndrome? ¿Piensas jugarlo cuando esté disponible?


Cuéntamelo en los comentarios.

