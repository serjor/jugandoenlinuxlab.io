---
author: Pato
category: "Simulaci\xF3n"
date: 2016-11-09 23:48:46
excerpt: "<p>El juego muestra el icono de soportar Linux/SteamOS en Steam, pero...\
  \ \"\xBFhemos llegado ya?\"</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/267b1948fa84309bc99f9c0289cabe44.webp
joomla_id: 97
joomla_url: motorsport-manager-ha-sido-publicado-oficialmente-pero-no-descarga-el-juego-en-linux
layout: post
tags:
- carreras
- steam
- simulador
title: "'Motorsport Manager' ha sido publicado oficialmente. La versi\xF3n de Linux\
  \ llegar\xE1 \"mas tarde\""
---
El juego muestra el icono de soportar Linux/SteamOS en Steam, pero... "¿hemos llegado ya?"

Motorsport Manager es un interesante juego de simulación y gestión deportiva en el que tienes que gestionar una escudería de coches de carreras. Está fuertemente enfocado al mundo de la Formula Uno aunque no cuenta con licencia oficial por lo que no tiene las escuderías, los circuitos y los nombres oficiales. 


El simulador tiene una pinta excelente y Playsport Games y Sega como editora parece que han hecho un port decente, incluso muy mejorado desde los dispositivos móviles (si, el juego es originario de android/IOS).


La polémica surge en el momento en que en la [página del juego](http://store.steampowered.com/app/415200/) en Steam aparece el logo de estar disponible en Linux/SteamOS, pero si lo adquieres con la intención de jugarlo en Linux el juego no se descarga, tal y como han reportado varios usuarios en los foros del juego en Steam, entre ellos [Liam de GoL](http://steamcommunity.com/app/415200/discussions/0/217690940939429422/).


En principio el estudio ya anunció en un [comunicado](http://www.motorsportmanager.com/content/motorsport-manager-release-date-and-minimum-specs) que la versión Linux llegaría "un poco después" del lanzamiento en Windows y Mac, pero eso no evita que en la [página de Steam](http://store.steampowered.com/app/415200/) sí aparezca el icono cuando en realidad aún no está disponible en Linux. De hecho en la [página de Steam](http://store.steampowered.com/app/415200/) aún no aparecen los requisitos mínimos ni recomendados del juego para Linux/SteamOS.


Es una lástima que Steam no lleve más cuidado con los iconos y las certificaciones de los sistemas para los juegos que afirman dar soporte para ellos, pero es lo que hay. 


¿Qué piensas sobre la salida de juegos con el icono de soporte para Linux/SteamOS y que en realidad aún no están disponibles? ¿Conoces algún otro caso? ¿Esperarás a jugar a Motorsport Manager?


Cuéntamelo en los comentarios.

