---
author: Pato
category: Aventuras
date: 2016-11-14 19:50:40
excerpt: "<p>El juego de Black Cloud Studios ofrece soporte en Linux en su campa\xF1\
  a de salida. No requieren ning\xFAn objetivo para ello.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e44a6f32e15cb53ee479b2697e759e2e.webp
joomla_id: 105
joomla_url: daedalus-un-thriller-de-ciencia-ficcion-ambientado-en-el-espacio-busca-financiacion-en-kickstarter
layout: post
tags:
- espacio
- ciencia-ficcion
- kickstarter
title: "'Daedalus' un thriller de ciencia ficci\xF3n ambientado en el espacio busca\
  \ financiaci\xF3n en Kickstarter"
---
El juego de Black Cloud Studios ofrece soporte en Linux en su campaña de salida. No requieren ningún objetivo para ello.

Daedalus será un juego de aventura espacial por episodios basado en el universo de "[After Reset](http://store.steampowered.com/app/335850/?snr=1_7_15__13)", un juego de Rol ya publicado en Steam (acceso anticipado) en el que también han publicado una Novela Visual llamada "[Fall of Gyes](http://store.steampowered.com/app/440100)", juegos que ya tienen soporte en Linux/SteamOS.


En Daedalus serás un ingeniero astronauta a bordo de la estación espacial Daedalus. En el último año el Daedalus ha hecho una exploración orbital sobre Venus y el Sol, y ahora está a tan solo treinta días hasta la próxima rotación de la tripulación. Los ánimos de la tripulación son altos por regresar a casa pero los sueños del protagonista se tornan en pesadillas de muerte y destrucción. Durante el transcurso de los días sucederán extraños sucesos, y la tripulación sucumbirá a accidentes, rumores de asesinato y teorías conspiranoicas. Es ahí donde tendrás que sobrevivir hasta llegar a casa.


Puedes visitar su página de Kickstarter:


  
  



<div class="resp-iframe"><iframe height="360" seamless="seamless" src="https://www.kickstarter.com/projects/blackcloudstudios/daedalus/widget/video.html" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Desde mi punto de vista, el hecho de que ya hayan publicado un par de juegos con soporte en Linux me da cierta confianza de que este "Daedalus" llegará a nuestro sistema. Eso sí, como siempre decimos en Jugando En Linux, no recomendamos invertir en promesas.


¿Que te parece la propuesta de Daedalus? ¿Piensas participar en la campaña?


Cuéntamelo en los comentarios.

