---
author: Pato
category: "Simulaci\xF3n"
date: 2016-11-29 15:05:19
excerpt: "<p>Adem\xE1s la expansi\xF3n trae nuevo contenido gratuito, como un editor\
  \ de escenarios y estaciones de radio.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/78d607068cd064420401737e12f83bae.webp
joomla_id: 131
joomla_url: cities-skylines-recibe-la-expansion-natural-disasters-ya-disponible-en-linux
layout: post
tags:
- steam
- simulador
- estrategia
title: "Cities Skylines recibe la expansi\xF3n 'Natural Disasters', ya disponible\
  \ en Linux"
---
Además la expansión trae nuevo contenido gratuito, como un editor de escenarios y estaciones de radio.

Cities Skylines [[Steam](http://store.steampowered.com/app/255710)][[página oficial](https://www.paradoxplaza.com/cities-skylines)] es un juego de construcción de ciudades disponible ya por algún tiempo en Linux, y ahora recibe una nueva expansión llamada Natural Disasters que incluye un catálogo de catástrofes para retar a los jugadores con características como sistemas de alerta temprana, rutas de emergencia, efectos de desastres devastadores y el cuidado de la población mientras se reconstruye. 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/w4nTvcp6rqI" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


La expansión trae el editor de escenarios y nuevo modo de juego permitiendo a los jugadores ganar o perder según sus propios criterios.


 ![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/CitiesSkylinesnov16.webp)


 Además, en esta expansión tendrás a tu disposición diferentes estaciones de radio.


Tienes Cities Skylines Natural Disasters disponible para su compa en su [página web](https://www.paradoxplaza.com/cities-skylines-natural-disasters?___store=eu&utm_source=twitter&utm_medium=social&utm_campaign=nadi_cs_twitter_20161129_rel&utm_content=tweet) o en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/515191/" width="646"></iframe></div>


¿Que te parece la propuesta de Cities Skylines?¿Jugarás esta expansión?


Cuéntamelo en los comentarios.


¿Tienes Cities Skylines y quieres que publiquemos tu review? ¡[Envíanosla](https://www.jugandoenlinux.com/contacto/envia-un-articulo)!

