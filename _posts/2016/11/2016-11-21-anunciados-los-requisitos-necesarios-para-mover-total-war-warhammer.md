---
author: Pato
category: Estrategia
date: 2016-11-21 15:56:32
excerpt: "<p>Feral Interactive los ha hecho p\xFAblicos en un nuevo comunicado. Drivers\
  \ Mesa soportados.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a42a2aa6c7440291c38ba9adc5892a56.webp
joomla_id: 112
joomla_url: anunciados-los-requisitos-necesarios-para-mover-total-war-warhammer
layout: post
tags:
- steam
- feral-interactive
- estrategia
title: 'Anunciados los requisitos necesarios para mover Total War: WARHAMMER'
---
Feral Interactive los ha hecho públicos en un nuevo comunicado. Drivers Mesa soportados.

Total Wal: WARHAMMER llegará como está previsto mañana mismo, pero tu equipo podrá mover el juego?


Feral Interactive nos saca de dudas con un nuevo comunicado en donde hace públicos los requisitos mínimos y recomendados para este nuevo port. ¿Tu máquina podrá conquistar el antíguo mundo?


Podrá si tienes un Intel Core i3-4130 o AMD FX6300 con 4GB de RAM y una gráfica Nvidia 650ti/AMD R9 270 con 2GB VRAM o mejor.


Pero para un dominio total Feral recomienda un Intel Core i7-4770 con 8GB de RAM y una gráfica Nvidia 970 o AMD RX 480 con 4GB de VRAM o mejor.


Ojo, por que las gráficas Nvidia necesitan los drivers 367.28 o superiores. AMD necesita drivers Mesa 13.0.1 compilados usando LLVM 3.9 o superior. Y para todos aquellos que usen SteamOS, las gráficas AMD sobre ese sistema no están soportadas.


Tienes el comunicado oficial [en la web de Feral](http://www.feralinteractive.com/en/news/702/).


¿Que te parecen los requisitos mínimos? ¿Tu equipo moverá este Total War: WARHAMMER?


Cuéntamelo en los comentarios.

