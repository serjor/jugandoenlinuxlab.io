---
author: Pato
category: Software
date: 2016-11-21 16:23:13
excerpt: "<p>Un usuario las ha filtrado en un tweet. \xA1Prepar\xE1d vuestras carteras!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/048731097de322302aff7e52151c991d.webp
joomla_id: 113
joomla_url: filtradas-las-fechas-de-las-proximas-rebajas-de-otono-e-invierno-de-steam
layout: post
tags:
- steamos
- steam
- rebajas
title: "Filtradas las fechas de las pr\xF3ximas rebajas de Oto\xF1o e Invierno de\
  \ Steam"
---
Un usuario las ha filtrado en un tweet. ¡Preparád vuestras carteras!

Parece que está de moda el filtrar las fechas de las rebajas de Steam. Lejos quedan los tiempos en que los usuarios esperábamos impacientes haciendo cábalas para adivinar cuando podríamos hincarle el diente a esos juegos de nuestra lista de deseados. Ahora, si no son las compañías de gestión de pagos (Paypal por ejemplo) o compañías de software, son algunos programadores con acceso a las secciones exclusivas de desarrolladores los que "filtran" las fechas. Aunque estas filtraciones al no ser oficiales hay que tomarlas con cautela también es cierto que en los últimos tiempos suelen ser vastante certeras. Esta vez en Twitter han publicado un pequeño fragmento de imagen de lo que supuestamente es el anuncio a desarrolladores que ha hecho Steam:



> 
> not that it's unexpected or news really, but here are the dates for the next 2 steam sales :) you're welcome [pic.twitter.com/b8M3k2lwYw](https://t.co/b8M3k2lwYw)
> 
> 
> — lashman (@RobotBrush) [4 de noviembre de 2016](https://twitter.com/RobotBrush/status/794618758446911488)







Como puedes ver, según esta filtración las ofertas de Otoño comenzarán este próximo 23 de Noviembre y durarán hasta el 29 de Noviembre.


Las ofertas de Invierno comenzarán el 22 de Diciembre y durarán hasta el 2 de Enero.


¿Que esperas comprar en estas rebajas de fin de año? ¿Comprarás en las de Otoño o en las de Invierno? ¿O en ambas?


Cuéntamelo en los comentarios.

