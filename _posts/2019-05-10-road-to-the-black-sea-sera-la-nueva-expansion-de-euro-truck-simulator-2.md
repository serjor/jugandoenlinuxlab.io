---
author: leillo1975
category: "Simulaci\xF3n"
date: 2019-05-10 15:03:35
excerpt: "<p><span class=\"username txt-mute\">@SCSsoftware acaba de revelar la fecha\
  \ de lanzamiento<br /></span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a1ca2d478cdfb0fd066e78050f4af09b.webp
joomla_id: 1044
joomla_url: road-to-the-black-sea-sera-la-nueva-expansion-de-euro-truck-simulator-2
layout: post
tags:
- ets2
- dlc
- scs-software
- euro-truck-simulator-2
- expansion
title: "\"Road to the Black Sea\" ser\xE1 la nueva expansi\xF3n de Euro Truck Simulator\
  \ 2 (ACTUALIZACI\xD3N)"
---
@SCSsoftware acaba de revelar la fecha de lanzamiento  



**ACTUALIZACIÓN 28-11-19:**


Cuando aún  hace poco que ha sido lanzada [Utah para American Truck Simulator](index.php/homepage/generos/simulacion/1129-lanzada-la-dlc-utah-para-american-truck-simulator). Acabamos de conocer a través del [Blog de SCS](https://blog.scssoft.com/2019/11/road-to-black-sea-release-date.html) y Twitter la fecha definitiva de salida de esta nueva DLC para Euro Truck Simulator. Exactamente dentro de una semana, **el día 5 de Diciembre**, podremos recorrer las carreteras de Rumanía, Bulgaria y la Turquía Europea, tal y como podeis ver en el siguiente tweet:



> 
> We are excited to announce that Road to the Black Sea for Euro Truck Simulator 2 will be releasing on December 5th ??  
>   
> Featuring Bulgaria, Romania & Turkey, we can't wait for you to explore these regions ?? ?? ??  
>   
> See the full feature list at our blog: <https://t.co/TZtH3FhQX1> [pic.twitter.com/WcANovBPuG](https://t.co/WcANovBPuG)
> 
> 
> — SCS Software (@SCSsoftware) [November 28, 2019](https://twitter.com/SCSsoftware/status/1200077179704528897?ref_src=twsrc%5Etfw)


  





Por supuesto, y como es habitual, os informaremos puntualmente el día de su salida con un nuevo artículo, e intentaremos como siempre mostraros a través de nuestros canales de [Youtube](https://www.youtube.com/c/jugandoenlinuxcom) y [Twitch](https://www.twitch.tv/jugandoenlinux) algún que otro recorrido por estas nuevas carreteras. Os dejamos ahora con el video oficial de esta DLC:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/DVfCDX64xiM" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**NOTICIA ORIGINAL:**


La gente de SCS software no descansa, ni deja descansar. En aras de ir completando el mapa de Europa, nos adelantan lo que va a ser su próxima expansión, que **llegará a nosotros a finales de año** como suele ser habitual con este tipo de DLC's . Tal y como pudimos ver en su blog, el juego contará con la posibilidad de recorrer 3 paises, **Rumanía**, **Bulgaria** y la parte europea de **Turquía** (Tracia). Hace unos meses nos lo hacían entrever en un [misterioso video](https://youtu.be/oRYvt79bFEU) que publicaban en su canal de Youtube y finalmente hoy se ha confirmado:



> 
> Where do those new roads lead to? ?  
>   
> It's a question that has been on everyone's minds & today we are excited to announce the next map expansion for [#ETS2](https://twitter.com/hashtag/ETS2?src=hash&ref_src=twsrc%5Etfw)!??  
>   
> Watch the announcement trailer & more at our blog ?<https://t.co/dhPnExzdPa> [pic.twitter.com/CdA1shr0Hd](https://t.co/CdA1shr0Hd)
> 
> 
> — SCS Software (@SCSsoftware) [May 10, 2019](https://twitter.com/SCSsoftware/status/1126864783788134400?ref_src=twsrc%5Etfw)


  





 Por lo que se ve los fans Españoles (y Portugueses) de ETS2 tendremos que seguir esperando por el mapa de la península Ibérica otro año más.... Mientras tanto, según comentan en el blog de SCS, **nos encontraremos con lo siguiente** (traducción):


*"""Desde los pequeños pueblos que se encuentran en el campo hasta la gran ciudad de **Estambul**, estos tres países son ricos en historia, arquitectura única y monumentos históricos como el **castillo de "Drácula"** que se encuentra en Rumanía. Todo esto lo verás en tus viajes en el camino hacia el Mar Negro.*


*Tan impresionante como las estructuras artificiales construidas en estos países son los paisajes naturales circundantes. Muchos de sus viajes pueden conducirlo a través de **bosques**, **carreteras costeras** o **cadenas montañosas** como las que se encuentran en **Transilvania**, que crean una unidad escénica para cualquier viajero.""""*


![ETS2 RtoBS](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/RoadToTheBlackSea/ETS2_RtoBS.webp)


Por supuesto os seguiremos informando con todas la novedades que haya sobre esta expansión a medida que las vayamos conociendo. Por ahora solo podemos adelantaros lo aquí escrito. Ahora os dejamos con el video del anuncio:  
 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/gsMvOcfeZOk" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Deseando conducir ya por las carreteras del Mar Negro? Contéstanos en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

