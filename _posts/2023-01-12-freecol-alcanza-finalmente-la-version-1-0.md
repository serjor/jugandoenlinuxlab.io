---
author: leillo1975
category: Estrategia
date: 2023-01-12 12:10:08
excerpt: "<p>Tras un lento pero constante ciclo de desarrollo el juego ha cumplido\
  \ con sus objetivos iniciales.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/freecol/freecol.webp
joomla_id: 1521
joomla_url: freecol-alcanza-finalmente-la-version-1-0
layout: post
tags:
- open-source
- java
- freecol
- colonization
title: "FreeCol alcanza finalmente la versi\xF3n 1.0. "
---
Tras un lento pero constante ciclo de desarrollo el juego ha cumplido con sus objetivos iniciales.


 A comienzos del año 2003, hace 20 años, comenzaba la andadura de este juego que pretendía ser un **clon libre del clásico de Microprose, Colonization** (1994). Con el paso del tiempo, el juego iba adquiriendo cada vez más forma hasta superar claramente al juego en el que se basaba teniendo mejoras tan notables como el **apartado multijugador** o la **vista isométrica**, a parte de unos **gráficos mucho más detallados** y con mayor resolución, aunque guardando la esencia del juego original, manteniéndose simples, limpios y funcionales. El juego además está **desarrollado en Java**, lo que hace posible su disfrute en múltiples sistemas operativos que soporten esta plataforma de desarrollo.


El juego, de **estrategia por turnos**, comienza en 1492, y en él tendremos que llegar en primer lugar a tierra en barco con nuestros primeros habitantes para comenzar a **colonizar America**. Tendremos que poco a poco fundar una nación compitiendo con otras potencias colonizadoras y luchar por el territorio y sus recursos. **El comercio tendrá una importancia muy grande** en el juego y tendremos que enviar barcos con recursos a Europa, así como traer más colonos que nos ayuden a prosperar en el nuevo mundo. **El objetivo final es la declaración de independencia y combatir contra las tropas de la nación Europea a la que servíamos**.


Desde la anterior versión, la 0.13.0, los **cambios** más importantes son:



* Nuevos gráficos de colonias específicos de la nación.
* Nuevos gráficos forestales
* Cambios importantes en la producción de bienes para que los valores reales coincidan con los del juego original.
* Inteligencia artificial (IA) muy mejorada para los jugadores de la computadora.
* Estabilidad mucho mejor (muchas correcciones de errores).


Podemos consultar la lista completa en la [noticia del lanzamiento](https://www.freecol.org/news/freecol-1.0.0-released.html). Si queréis ver el juego en acción podéis ver este reciente gameplay donde "El Holandés Errante" nos habla un poco del juego y comienza una partida.:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/I_AkabscGg8" title="YouTube video player" width="780"></iframe></div>
Si quieres disfrutar al fin de la **versión 1.0** de este clásico del software libre, tan solo tienes que ir a su [sección de descargas](https://www.freecol.org/download.html) de su [página web oficial](https://www.freecol.org/index.html) y escoger el paquete que más te interese. Puedes contarnos tu parecer sobre esta noticia en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux)[.](https://t.me/jugandoenlinux) Por cierto, ¿un turno más?  
  



