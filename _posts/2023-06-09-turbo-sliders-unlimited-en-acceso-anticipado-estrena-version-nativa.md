---
author: leillo1975
category: Carreras
date: 2023-06-09 14:50:06
excerpt: "<p><span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">El\
  \ desarrollador de @TurboSliders</span> acaba de anunciarlo</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/TurboSlidersUnlimited/TurboSlidersUnlimitedwLogo.webp
joomla_id: 1542
joomla_url: turbo-sliders-unlimited-en-acceso-anticipado-estrena-version-nativa
layout: post
tags:
- carreras
- multijugador
- unity3d
title: "Turbo Sliders Unlimited, en acceso anticipado, estrena versi\xF3n nativa"
---
El desarrollador de @TurboSliders acaba de anunciarlo


 Casi año y medio ha pasado desde que descubrí este juego por casualidad en Steam, y desde entonces ha ido siguiéndole la pista dada su interesante propuesta. Finalmente hace unos días su único desarrollador, [Antti Mannisto](https://store.steampowered.com/search/?developer=Antti%20Mannisto&snr=1_5_9__2000), nos confirmaba a través de los [foros de Steam](https://steamcommunity.com/app/1478340/discussions/0/4299251149980221400/) la disponibilidad de una **build experimental nativa** para nuestro sistema. Además nos pusimos en contacto con él para echarle una mano con el testeo de esta y su compatibilidad con diversos mandos. Finalmente en el día de hoy acaba de anunciar la **disponibilidad de esta de forma oficial en Steam**, tal y como podéis ver en el siguiente tweet: 



> 
> Great news to all Linux and Steam Deck users: Turbo Sliders Unlimited is now supporting Linux natively!   
>   
> All penguins are welcome to join the community!  
>   
> Steam: <https://t.co/hfVKDcd8cQ>  
> Discord: <https://t.co/dnZObE3oUW>[#gaming](https://twitter.com/hashtag/gaming?src=hash&ref_src=twsrc%5Etfw) [#steam](https://twitter.com/hashtag/steam?src=hash&ref_src=twsrc%5Etfw) [#linux](https://twitter.com/hashtag/linux?src=hash&ref_src=twsrc%5Etfw) [#multiplayer](https://twitter.com/hashtag/multiplayer?src=hash&ref_src=twsrc%5Etfw) [#indiedev](https://twitter.com/hashtag/indiedev?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/7AvHtWOYnY](https://t.co/7AvHtWOYnY)
> 
> 
> — Turbo Sliders Unlimited (@TurboSliders) [June 9, 2023](https://twitter.com/TurboSliders/status/1667175617698177026?ref_src=twsrc%5Etfw)



Para quien no lo conozca, [Turbo Sliders Unlimited](http://www.turbosliders.com/) es la **continuación de un juego lanzado hace casi 20 años** llamado [Turbo Sliders (2004)](http://ts.turbosliders.com/), título multijugador que permitía hasta **20 jugadores simultáneos y con capacidad para mods**, que entre otras cosas permitía jugar al futbol con coches mucho antes que Rocket League ([Punaball](https://youtu.be/YFrDBrbmT3Q)), o [ligas de F1](https://youtu.be/yrayMGJASAg). El juego tiene una pequeña comunidad y ha recibido actualizaciones hasta 2016. Su creador además ha trabajado para compañías como **Remedy** en [Alan Wake](https://es.wikipedia.org/wiki/Alan_Wake) y **[Quantum Break](https://es.wikipedia.org/wiki/Quantum_Break)**; y **Ubisoft**, sobre todo en la serie [Trials](https://es.wikipedia.org/wiki/Trials). Como echaba de menos los días de pequeño desarrollador, la libertad creativa que eso implica, y que seguía recibiendo aluviones de peticiones de Turbo Sliders, finalmente decídío traer la propuesta del juego a la actualidad, usando herramientas e ideas modernas.  Como veis, este **desarrollador finlandés** no es ningún novato y tiene un buen curriculum. 


Volviendo a su juego actual, está **desarrollado usando Unity en completo 3D**, con múltiples cámaras, al contrario que el juego original, que tenía vista cenital en 2D. Implementa **modernas funciones en linea** y multitud de opciones de **personalización y modding**, convirtiéndose así en una **plataforma impulsada por la comunidad** donde cualquiera puede aportar con niveles, coches o modos de juego. También incluye un modo de un solo jugador que está en desarrollo actualmente.


![TurboSlidersUnlimited01](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/TurboSlidersUnlimited/TurboSlidersUnlimited01.webp)


Al igual que el clásico, permite hasta 20 jugadores en linea, pudiendo alojar el jugador su propio servidor, publico, privado, dedicado... y usando los **mods** que quiera, **descargándose estos automaticamente en el cliente**. También es posible jugar en local a **pantalla partida hasta 4 jugadores**, para picarnos con nuestra familia y/o amigos en nuestro sofá. El juego además incluye **potentes editores para crear pistas y coches que podremos compartir a través de [Workshop](https://steamcommunity.com/app/1478340/workshop/)**. Podemos también personalizar nuestros vehículos con colores, pinturas, pegatinas, que también se podrán comprar en Steam en paquetes de personalización.


Otra posibilidad es la de grabar las partidas y luego poder reproducirlas desde múltiples vistas con diferentes velocidades de reproducción. También se puede **competir asíncronamente contra otros jugadores, viendo su coche fantasma como referencia**. Además de todos los modos de juego que están por venir (recordad que el juego está en **Early Access**), actualmente podeis disfrutar de  **Parkour / plataformas**, **Carreras fantasma**, **Sumo de coches**, **Tag game** (propagación y transferencia), **Fútbol de coches**, Carreras con terreno mortal o contactos, Carreras con drafting (fuerza ajustable), Carreras con combustible (posibilidad de repostar en boxes), Juegos de equipo.... En modo de un solo jugador actualmente se puede jugar un **modo contrarreloj**, desbloqueando nuevos niveles, pero habrá campañas contra la IA.


En definitiva, tras jugarlo online , os puedo asegurar que es muy divertido, y que hay escenarios de todo tipo, tanto con contenido oficial, como mods creados por su comunidad. Creo además que tanto si os gustan los juegos de coches, como es mi caso, como si disfrutáis de los títulos multijugador, os va a gustar. Además tiene un precio de lo más interesante teniendo en cuenta todas las posibilidades que brinda, como podéis ver en el widget aquí abajo:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1478340/" width="646"></iframe></div>


Como siempre recordaros que nosotros seguiremos informando de todas las novedades que vayamos conociendo de este juego y os dejamos con el trailer oficial dele juego: 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/wN_EGndPNHs" title="YouTube video player" width="780"></iframe></div>

