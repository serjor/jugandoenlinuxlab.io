---
author: Serjor
category: "Acci\xF3n"
date: 2018-03-30 17:34:10
excerpt: "<p>No hay fecha de salida concreta, pero llegar\xE1 a lo largo de este 2018</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/acbf17dca076404b2078b0d4b135530d.webp
joomla_id: 696
joomla_url: mark-of-the-ninja-tendra-remasterizacion
layout: post
tags:
- remasterizacion
- mark-of-the-ninja
title: "Mark of the Ninja tendr\xE1 remasterizaci\xF3n"
---
No hay fecha de salida concreta, pero llegará a lo largo de este 2018

Leemos en [eurogamer](https://www.eurogamer.es/articles/2018-03-29-mark-of-the-ninja-remastered-saldra-tambien-en-pc-ps4-y-xbox-one) que los chicos de Klei van a lanzar una remasterización de Mark of the Ninja, uno de los mejores plataformas 2d que podemos encontrar a día de hoy.


Esta remasterización traerá soporte para 4K, mejoras en el apartado sonoro y efectos de partículas.


Os dejamos con el tease trailer de la remasterización:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/sZ7ogfkR3hQ" width="560"></iframe></div>


Y tú, ¿te harás con esta remasterización? Cuéntanos qué te parece en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

