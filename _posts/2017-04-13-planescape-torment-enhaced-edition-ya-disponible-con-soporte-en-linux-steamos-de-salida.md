---
author: Pato
category: Rol
date: 2017-04-13 20:42:11
excerpt: "<p>El juego cl\xE1sico de Beamdog fue lanzado en 1999. Ahora nos llega con\
  \ diversas mejoras</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ccbca3752adf58a776d876d7a7de9edc.webp
joomla_id: 289
joomla_url: planescape-torment-enhaced-edition-ya-disponible-con-soporte-en-linux-steamos-de-salida
layout: post
tags:
- accion
- fantasia
- rol
title: '''Planescape Torment: Enhaced edition'' ya disponible con soporte en Linux/SteamOS
  de salida'
---
El juego clásico de Beamdog fue lanzado en 1999. Ahora nos llega con diversas mejoras

Para todo el que no lo haya conocido, 'Planescape Torment' fue todo un clásico en los juegos de rol llegando a ganar el galardón de juego de rol del año. Ahora nos llega esta versión "enhaced" con soporte en linux/SteamOS desde su lanzamiento. ¿Qué podemos esperar de este relanzamiento?


'Planescape Torment: Enhaced Edition' [[web oficial](http://www.planescape.com/)] ha sido rediseñado por su diseñador original para que el juego sea como en un principio fue pensado, con mejoras a todos los niveles y arreglo de fallos diversos, incluyendo mejoras a nivel jugable como el zoom de pantalla, el uso de tabulador para resaltar menús, música remasterizada, interfaz adaptada a 4k y mucho más. Además podrás activar o desactivar las mejoras a tu antojo para disfrutar Planescape Torment como tu quieras, ya sea la versión remasterizada o la versión original tal y como se dió a conocer.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/G2wXLCIpFRg" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Trístemente, 'Planescape Torment: Enhaced Edition' no viene traducido al español, pero si eso no es problema para ti y te va el rol profundo de la vieja escuela, puedes encontrarlo en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/466300/" width="646"></iframe></div>

