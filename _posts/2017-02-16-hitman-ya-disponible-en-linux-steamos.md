---
author: Pato
category: "Acci\xF3n"
date: 2017-02-16 19:33:11
excerpt: <p>Un nuevo port de Feral Interactive. Y van...</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5f9ec0b2e765a617089a13fe6f9b5c6d.webp
joomla_id: 223
joomla_url: hitman-ya-disponible-en-linux-steamos
layout: post
tags:
- accion
- steam
- sigilo
- tercera-persona
- feral-interactive
title: '''Hitman'' ya disponible en Linux/SteamOS'
---
Un nuevo port de Feral Interactive. Y van...

![HitmanLinux banner](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/banners/HitmanLinux_banner.webp)


Tal y como estaba previsto, [Feral nos ha anunciado](https://www.feralinteractive.com/es/news/739/) que 'Hitman' ha llegado hoy a Linux/SteamOS en un port que promete ser excelente.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="360" src="https://www.youtube.com/embed/pFU17zGKWns" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


*Métete en la piel del Agente 47 y conviértete en un maestro asesino frío e inmoral en una emocionante historia de espionaje ambientada en el mundo del asesinato. Viaja a Francia, Italia, Marruecos, Tailandia, los Estados Unidos y Japón para eliminar objetivos de alto nivel.*


Puedes visitar la miniweb que Feral ha preparado para el lanzamiento del juego:


<https://www.feralinteractive.com/es/linux-games/hitman/story/>


¿Tienes lo que hay que tener?:


REQUISITOS MÍNIMOS:  
SO: Ubuntu 16.04 or Steam OS 2.0  
Procesador: Intel Core i5-2500K/AMD FX-8350  
Memoria: 8 GB de RAM  
Gráficos: Nvidia GeForce GTX 680/AMD R9 270X o superior\*  
Almacenamiento: 67 GB de espacio disponible


RECOMENDADOS:  
SO: Ubuntu 16.10 or Steam OS 2.0  
Procesador: Intel Core i7 3770  
Memoria: 16 GB de RAM  
Gráficos: Nvidia GeForce GTX 970/AMD R9 290 o superior\*  
Almacenamiento: 67 GB de espacio disponible


\* Las tarjetas Nvidia requieren los drivers 375.26. Las tarjetas AMD requieren los drivers Mesa 13.0.3 o superiores. Las gráficas Intel no están soportadas en el lanzamiento.


Si quieres comprar Hitman estás de suerte puesto que este fin de semana Square Enix está llevando a cabo descuentos en sus juegos. En concreto ¡Hitman y todos sus DLCs está a un 50% de descuento!. No está nada mal, ¿verdad?.


Puedes comprar Hitman y todos sus DLCs diréctamente en la tienda de Feral con esos mismos descuentos y así estarás apoyando a los creadores del port a Linux en su tienda:


Intro Pack:


<https://store.feralinteractive.com/es/mac-linux-games/hitman/>


Primera temporada completa:


<https://store.feralinteractive.com/es/mac-linux-games/hitmanseason1/>


O también puedes comprarlo en su página de Steam:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/236870/93404/" width="646"></iframe></div>


 


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/236870/71223/" width="646"></iframe></div>


¿Qué te parece este port de Hitman? ¿Piensas comprarlo?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

