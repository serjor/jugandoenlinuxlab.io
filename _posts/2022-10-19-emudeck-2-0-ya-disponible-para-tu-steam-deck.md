---
author: Odin
category: Software
date: 2022-10-19 11:36:48
excerpt: "<p>Nueva versi\xF3n de EmuDeck totalmente redise\xF1ada.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/EmuDeck/emudeck.webp
joomla_id: 1497
joomla_url: emudeck-2-0-ya-disponible-para-tu-steam-deck
layout: post
tags:
- emulador
- retro
- steam-deck
title: EmuDeck 2.0 ya disponible para tu Steam Deck
---
Nueva versión de EmuDeck totalmente rediseñada.


Si la semana pasada comentábamos que [Batocera]({{ "/posts/batocera-ya-disponible-para-tu-steam-deck" | absolute_url }}) había publicado una versión para **Steam Deck** ahora le toca el turno a [EmuDeck](https://www.emudeck.com/). Para quien no la conozca, [EmuDeck](https://www.emudeck.com/) es una sencilla aplicación que nos permite instalar una increíble lista de emuladores totalmente optimizados para nuestra Steam Deck.


En esta nueva versión, la **2.0**, han rediseñado la interfaz gráfica para que sea más amigable permitiéndonos elegir la lista de emuladores a instalar incluyendo como novedad la emulación de **PS Vita** mediante [Vita3K](https://vita3k.org/).


![Emuladores disponibles en Emudeck](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/EmuDeck/emuladoresemudeck.webp)


A la hora de instalar, [EmuDeck](https://www.emudeck.com/) nos da amplias opciones de configuración dejándonos elegir como destino de la instalación tarjetas **microSD** en vez de la memoria interna. Otra funcionalidad interesante que han añadido son el **guardado de las partidas en nube** permitiendo almacenarlas en servicios como Google Drive, One Drive e incluso **Nextcloud**. No queremos tampoco dejar de destacar la posibilidad de configurar la relación de aspecto e incluso la aplicación de post procesado mediante un **shader CRT** que imita las televisiones de tubo, un detalle de lo más retro que nos encanta.


![relación de aspecto](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/EmuDeck/aspectratio.webp)


Gracias a [Batocera]({{ "/posts/batocera-ya-disponible-para-tu-steam-deck" | absolute_url }}), [RetroDECK](https://retrodeck.net/) y esta nueva versión de [EmuDeck](https://www.emudeck.com/) lo cierto es que la Steam Deck se ha convertido en una excelente opción para emular nuestro sistemas preferidos, desde videoconsolas antiguas como la **Game Boy** hasta la popular **Nintendo Switch**.


¿Qué te parece [EmuDeck](https://www.emudeck.com/)? ¿Lo has probado? Cuéntanos como siempre tu opinión sobre esta aplicación en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

