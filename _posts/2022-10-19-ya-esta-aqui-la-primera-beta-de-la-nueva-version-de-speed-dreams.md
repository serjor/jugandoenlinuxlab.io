---
author: leillo1975
category: Carreras
date: 2022-10-19 15:10:37
excerpt: "<p>Los desarrolladores de @speeddreams_oms lanzan esta versi\xF3n previa\
  \ a la 2.3.0.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/2.3.0-Beta1-900.webp
joomla_id: 1498
joomla_url: ya-esta-aqui-la-primera-beta-de-la-nueva-version-de-speed-dreams
layout: post
tags:
- beta
- open-source
- speed-dreams
- simracing
title: "Ya est\xE1 aqu\xED la primera BETA de la nueva versi\xF3n de Speed Dreams"
---
Los desarrolladores de @speeddreams_oms lanzan esta versión previa a la 2.3.0.


 Tras un periodo de desarrollo de más de un año, el equipo que está detrás de este simulador libre de carreras de coches, finalmente ha lanzado un a**delanto de lo que será su próxima versión oficial,** la 2.3.0, y para ello hasta ahora han trabajado como jabatos para traernos un buen puñado de novedades más que interesantes, introduciendo características que seguro que les encantarán a los aficionados a este tipo de juegos. Algunas de estas son las siguientes:


* Añadida la temperatura y degradación de neumáticos en las categorías de Supercars, 67GP, MPA11 y 12, además del existente MPA1.
* Nuevos widgets para el HUD en OSG, como el de neumáticos, fuerzas, delta o controles, además de los existentes.
![OSG HUD](https://www.speed-dreams.net/wp-content/uploads/2021/12/OSG-HUD-Widgets01.jpg)
* Nuevos sonidos en los menúes del juego, así como en el cambio de marchas o algunos motores de coches.
* Nuevos interiores en algunos modelos de Supercars, como el Boxer 96, el Cavallo 360 o el FMC GT4
![Interior Cavallo](https://www.speed-dreams.net/wp-content/uploads/2021/12/Cavallo-Interior.jpg)
* Nuevos conductores animados para Supercars
* Posibilidad de cambiar el setup del coche desde dentro el juego.
* Nuevos coches como el Mango MS7 (67GP) o los nuevos MPA12 (Deckard, Murasama y Spirit)
![Mango MS7](https://www.speed-dreams.net/wp-content/uploads/2022/02/67gp-mango-ms7-preview-300x169.jpg)
* Nueva pista basada en el circuito de Melbourne
* Añadidos montones de nuevos skins para diversos coches
* Añadidos nuevos robots para diversos coches
* Mejoras en el soporte multimonitor y posibilidad de cambiar dinamicamente el tamaño de la ventana.
* La herramienta para crear pistas, Trackeditor, recibe múltiples mejoras y se incluye en el juego.
![Trackeditor](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/Trackeditor.webp)
* El generador de pistas, Trackgen, recibe nuevas características, mejoras y correcciones.
* Estrenadas [nueva web](https://www.speed-dreams.net/) y [nuevo master server](https://speed-dreams.net/masterserver/) para registrar nuestros mejores tiempos gracias a nuestro colaborador **SonLink**.
* Abiertas nuevas cuentas de [Twitter](https://twitter.com/speeddreams_oms), [Mastodon](https://mastodon.social/@speed_dreams_official), [Youtube](https://www.youtube.com/channel/UCJtD22LkG8YJRC0EnAH8zNw) y [Peertube](https://peertube.linuxrocks.online/c/speed_dreams_oms/videos).
* Por supuesto, montones de correcciones de bugs y optimizaciones.


Como es habitual podéis descargar tanto el código de esta nueva versión beta, como la AppImage (gracias **SonLink**) de este [artículo de su página web](https://www.speed-dreams.net/en/new-2-3-0-beta-1-release/). También encontrareis la **lista completa de cambios desde la 2.2.3**, así como las **vías de comunicación con los desarrolladores para reportar bugs**, algo que os rogamos encarecidamente. Os dejamos con un video donde podéis observar algunos de los trabajos de esta última versión, como la nueva pista en Melbourne o el nuevo HUD:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/691C-fIqKxs" title="YouTube video player" width="780"></iframe></div>


Si queréis comentar algo o tenéis cualquier duda sobre Speed Dreams, podéis poneros en contacto con nosotros en nuestra [sala de Matrix "Zona Racing"](https://matrix.to/#/#zona_racing-jugandoenlinux:matrix.org) o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

