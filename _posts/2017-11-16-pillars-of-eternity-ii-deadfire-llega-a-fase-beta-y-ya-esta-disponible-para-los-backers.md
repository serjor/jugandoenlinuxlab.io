---
author: Pato
category: Rol
date: 2017-11-16 17:48:53
excerpt: "<p>Si pagaste la opci\xF3n de acceso beta en la campa\xF1a de Fig ya puedes\
  \ descargarlo y jugar</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2d8edb28c6792cb00bf850749758d146.webp
joomla_id: 540
joomla_url: pillars-of-eternity-ii-deadfire-llega-a-fase-beta-y-ya-esta-disponible-para-los-backers
layout: post
tags:
- proximamente
- fantasia
- rol
title: "'Pillars of Eternity II: Deadfire' llega a fase beta y ya est\xE1 disponible\
  \ para los \"Backers\""
---
Si pagaste la opción de acceso beta en la campaña de Fig ya puedes descargarlo y jugar

Obsidian suma y sigue. Hace unas horas que han hecho público que ya está disponible la versión beta de 'Pillars of Eternity II: Deadfire' para todos aquellos que hayan aportado lo suficiente en las campañas de financiación del juego.


Según el comunicado que puedes ver [en este enlace](http://steamcommunity.com/games/560130/announcements/detail/1450584852668543808), "*si aportaste una opción que incluyese el acceso a la Beta, o lo compraste como añadido durante la campaña de Fig, puedes obtener tu clave para Steam o GOG."*


Si no has aportado lo suficiente a las campañas del juego y quieres acceder a la beta, aún puedes hacerlo accediendo al [portal de backers](http://eternity.obsidian.net/backer) de Obsidian y actualizar la opción de compra para acceder ya a la beta del juego.


Para cualquier pregunta sobre el acceso a la beta o del juego, puedes [acceder a su foro](https://forums.obsidian.net/topic/94318-welcome-to-the-deadfire-backer-beta-now-live/) donde ofrecen más información.


'Pillars of Eternity II: Deadfire' [[web oficial](https://eternity.obsidian.net/)] es la continuación del notable 'Pillars of Eternity' [[Steam](http://store.steampowered.com/app/291650/Pillars_of_Eternity/)] donde tendremos que capitanear nuestra nave en una peligrosa aventura a través de un archipiélago de la inexplorada región de Deadfire.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/ln_plWALAoI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Los requisitos mínimos y recomendados del juego ya se han hecho públicos, aunque ya avisan que pueden variar de cara al lanzamiento oficial:


**Mínimo:**  

+ **SO:** Ubuntu 14.04 LTS 64-bit o superior
+ **Procesador:** Intel Core i3-2100T @ 2.50 GHz / AMD Phenom II X3 B73
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** ATI Radeon HD 4850 or NVIDIA GeForce 9600 GT
+ **Almacenamiento:** 14 GB de espacio disponible
+ **Notas adicionales:** Estos son requisitos de sistema preliminares y pueden variar


**Recomendado:**  

+ **SO:** Ubuntu 14.04 LTS 64-bit o superior
+ **Procesador:** Intel Core i5-2400 @ 3.10 GHz / AMD Phenom II X6 1100T
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** Radeon HD 7700 o NVIDIA GeForce GTX 570
+ **Almacenamiento:** 14 GB de espacio disponible
+ **Notas adicionales:** Estos son requisitos de sistema preliminares y pueden variar




Si quieres más información, puedes visitar la página del juego en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/560130/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Piensas jugar a Pillars of Eternity II? ¿Tienes acceso a la beta del juego?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

