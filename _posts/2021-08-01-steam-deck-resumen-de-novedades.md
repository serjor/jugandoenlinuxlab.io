---
author: P_Vader
category: Hardware
date: 2021-08-01 18:01:15
excerpt: "<p>Todas las novedades que se han ido desvelando de la #steamdeck de <span\
  \ class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">@valvesoftware</span>\
  \ en los \xFAltimos d\xEDas.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/overview-shared-library-spanish.webp
joomla_id: 1334
joomla_url: steam-deck-resumen-de-novedades
layout: post
tags:
- steamos
- hardware
- steam-deck
title: 'Steam Deck: resumen de novedades'
---
Todas las novedades que se han ido desvelando de la #steamdeck de @valvesoftware en los últimos días.


 Desde que se anunció el nuevo producto estrella de Valve no han parado de conocerse detalles y curiosidades de la misma. Vamos a comentar las últimas novedades que hemos recopilado.


 


Para empezar, un usuario preguntó en los foros de Steam sobre el futuro de Big Picture respecto al nuevo sistema de Steam Deck, un desarrollador de Valve **confirmó que Big Picture será reemplazado por el nuevo interfaz que se usará en la Steam Deck,** sin especificar aun en que fecha se producirá ese cambio:



> 
> Yes, we are replacing Big Picture with the new UI from Deck. We don't have an ETA to share yet though.
> 
> 
> - [austinp_valve 20 JUL a las 21:38](https://steamcommunity.com/groups/bigpicture/discussions/1/5167301764850695635/#c5167301764854104718)
> 
> 
> 


 


Por otro lado el prolífero desarrollador de Valve [Pierre-Loup Griffais](https://twitter.com/Plagman2) hizo un comentario en su [twitter sobre como se había colado el cursor X11](https://twitter.com/Plagman2/status/1418700238722797568?s=20) en un video de la Deck:


![steam deck tweet gamescope](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/steam_deck_tweet_gamescope.webp)


 A continuación aclaró que la **Steam Deck, en modo de video juegos, usaría su micro [compositor gamescope](https://github.com/Plagman/gamescope) sobre Wayland**, como era lógico. El cursor al que se refería era el de XWayland:



> 
> The gaming session (eg. not-desktop) is powered by gamescope, a Wayland compositor. This cursor comes from XWayland, yes.
> 
> 
> 
> 
> 
> - [@Plagman2](https://twitter.com/Plagman2/status/1418701862484594691?s=20) [24 jul.](https://twitter.com/Plagman2/status/1418701862484594691?s=20)
> 
> 
> 
> 


 


Poco antes Pierre también había dado a conocer que la Deck vendría con **un limitador de FPS en el sistema para gestionar la vida útil de la batería**, ademas de comentar como **los juegos probados (supuestamente el catálogo completo de Steam) habían superado un mínimo de 30FPS de calidad:**



> 
> The "30 FPS target" refers to the floor of what we consider playable in our performance testing; games we've tested and shown have consistently met and exceeded that bar so far. There will also be an optional built-in FPS limiter to fine-tune perf vs. battery life.
> 
> 
> - [@Plagman2 25 jul. 2021](https://twitter.com/Plagman2/status/1419087956535513093?s=20)
> 
> 
> 


 


Por último, el conocido desarrollador y divulgador de KDE [Nate Graham](https://pointieststick.com/), en sus resúmenes semanales en [This Week in KDE desveló](https://pointieststick.com/2021/07/16/this-week-in-kde-kde-powered-steamdeck-revealed/) como **Valve había contratado a través de [Blue System](https://blue-systems.com/) algunas de las mejoras llegadas en los últimos tiempos al escritorio Plasma de KDE,** para tener el escritorio listo para su PC portátil. El mismo Nate se encargó de la gestión de la calidad del software de KDE para la Steam Deck. Ahora entendemos ese avance espectacular en Wayland o la mejora en el teclado virtual.


 


No para de dar que hablar la Steam Deck. ¿Qué te parecen estas novedades? ¿Crees que está resultando positivo para el ecosistema Linux? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix.](https://matrix.to/#/+jugandoenlinux.com:matrix.org)


 


 


