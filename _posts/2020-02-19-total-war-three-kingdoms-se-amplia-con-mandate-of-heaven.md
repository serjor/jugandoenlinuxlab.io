---
author: Pato
category: Estrategia
date: 2020-02-19 18:51:55
excerpt: "<p>Como era de esperar, El nuevo DLC viene de la mando de @feralgames</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/TotalWarThreeKingdoms/Totalwarmandateofheaven.webp
joomla_id: 1175
joomla_url: total-war-three-kingdoms-se-amplia-con-mandate-of-heaven
layout: post
tags:
- feral-interactive
- estrategia
- total-war
- three-kingdoms
title: "Total War: THREE KINGDOMS se ampl\xEDa con Mandate of Heaven"
---
Como era de esperar, El nuevo DLC viene de la mando de @feralgames


Como era de esperar, **Feral Interactive** ha anunciado la disponibilidad del nuevo DLC de **Total War: THRE KINGDOMS** llamado '**Mandate of Heaven**', lo nuevo de Creative Assembly:



> 
> Total War: THREE KINGDOMS - Mandate of Heaven has dawned on macOS & Linux with 40 new units, new campaign mechanics and 6 new warlords. Buy from:  
>   
> Feral Store - <https://t.co/rDQugCbPGW>  
> Steam - <https://t.co/Nlg9zjuKUc>  
>   
> Read the Total War blog for details: <https://t.co/ZwqhfyI8xJ> [pic.twitter.com/iruKRmLIl4](https://t.co/iruKRmLIl4)
> 
> 
> — Feral Interactive (@feralgames) [February 19, 2020](https://twitter.com/feralgames/status/1230173642929119232?ref_src=twsrc%5Etfw)


  





Este nuevo DLC de Total War: THREE KINGDOMS nos trae novedades como 40 nuevas unidades, nuevas mecánicas de campaña y 6 nuevos señores de la guerra.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/7SITFWC6Ly0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


*Juega los conflictos de la rebelión de los Turbantes Amarillos y el levantamiento de Qiang con nuevos señores de la guerra, incluyendo a los hermanos Zhang y al emperador Liu Hong.*


* *Empieza una campaña en el año 182 d. C., justo antes de la rebelión de los Turbantes Amarillos. Adéntrate de lleno en el conflicto y el periodo de los Tres Reinos.*
* *Nuevos personajes y seis nuevos señores de la guerra jugables, incluidos los hermanos Zhang y el emperador Ling.*
* *Nuevos eventos, mecánicas y objetivos de campaña únicos.*
* *40 nuevas unidades de batalla y nuevas habilidades de las unidades.*
* *Juega las historias de los orígenes de personajes legendarios como Cao Cao y Liu Bei.*


Como es costumbre, este DLC nos llega tan solo unos días después de su lanzamiento para Windows y con toda la garantía de calidad a la que nos tiene acostumbrados Feral Interactive. En cuanto a los requisitos, os recordamos que son:


**MÍNIMO:**


+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** Ubuntu 18.04 64-bit
+ **Procesador:** 3.4GHz Intel Core i3-4130
+ **Memoria:** 6 GB de RAM
+ **Gráficos:** 2GB AMD R9 285 (GCN 3rd Generation), 2GB Nvidia GTX 680
+ **Almacenamiento:** 60 GB de espacio disponible
+ **Notas adicionales:**
	- \* Requiere Vulkan
	- \* Nvidia requiere driver 418.56 o superior.
	- \* AMD requiere driver Mesa 19.0.1. ​
	- \* AMD GCN 3rd Gen GPU's incluidas las R9 285, 380, 380X, Fury, Nano, Fury X.
	- \* Intel GPUs no están soportadas en el momento del lanzamiento.
	- \* Otros drivers y distribuciones modernas pueden funcionar, pero no están oficialmente soportadas.


**RECOMENDADO:**


+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** Ubuntu 18.04 64-bit
+ **Procesador:** 3.4GHz Intel Core i7-4770
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** 4GB AMD RX 480, 4GB Nvidia GTX 970
+ **Almacenamiento:** 60 GB de espacio disponible
+ **Notas adicionales:**
	- \* Requiere Vulkan
	- \* Nvidia requiere driver 418.56 o superior.
	- \* AMD requiere driver Mesa 19.0.1. ​
	- \* AMD GCN 3rd Gen GPU's incluidas las R9 285, 380, 380X, Fury, Nano, Fury X.
	- \* Intel GPUs no están soportadas en el momento del lanzamiento.
	- \* Otros drivers y distribuciones modernas pueden funcionar, pero no están oficialmente soportadas.


Si quieres saber más sobre Total War: THREE KINGDOMS Mandate of Heaven puedes visitar la [miniweb del juego](https://www.feralinteractive.com/es/games/threekingdomstw/dlc/) de Feral, o si estás interesado puedes comprar el DLC diréctamente [en su tienda oficial](https://store.feralinteractive.com/es/mac-linux-games/threekingdomstw/#threekingdomstwdlcmandateofheaven) (**mayor beneficio para Feral que es el que lo ha portado**) o también lo tienes disponible en su [página de Steam](https://store.steampowered.com/app/1154760/Total_War_THREE_KINGDOMS__Mandate_of_Heaven/) a un precio de 9,99€.


¿Te gusta la saga Total War? ¿Qué te parece THREE KINGDOMS?


Cuéntamelo en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

