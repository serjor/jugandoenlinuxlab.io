---
author: leillo1975
category: "Acci\xF3n"
date: 2020-02-15 16:11:56
excerpt: "<p>El equipo detr\xE1s de <span class=\"css-901oao css-16my406 r-1qd0xha\
  \ r-ad9z0x r-bcqeeo r-qvutc0\">@MetroVideoGame</span> confirma la fecha de salida.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/metro_exodus-4776224.webp
joomla_id: 1171
joomla_url: metro-exodus-podria-llegar-a-linux-pronto-a1
layout: post
tags:
- port
- deep-silver
- metro-exodus
- 4a-games
title: "Metro Exodus llegar\xE1 a Linux muy pronto (ACTUALIZADO)"
---
El equipo detrás de @MetroVideoGame confirma la fecha de salida.


**ACTUALIZACIÓN 25-3-21:** Más de un año ha pasado desde la última vez que os hablamos de este juego, y es que la verdad poco más se supo sobre el desarrollo de la versión de Linux desde entoces. Pero finalmente ya tenemos una fecha sobre la mesa, **el 14 de Abril**, con lo que en unas semanas ya estaremos disfrutándolo. El anuncio se ha realizado a través de twitter:  
  




> 
> Spartans, we have an update for you on the [#Mac](https://twitter.com/hashtag/Mac?src=hash&ref_src=twsrc%5Etfw) & [#Linux](https://twitter.com/hashtag/Linux?src=hash&ref_src=twsrc%5Etfw) versions of [#MetroExodus](https://twitter.com/hashtag/MetroExodus?src=hash&ref_src=twsrc%5Etfw)   
>   
> We are working hard on bringing these to you, and we can finally let you know the train has left the station. Both versions are now on track to arrive on Wednesday April 14. [pic.twitter.com/w2tB3iVnFD](https://t.co/w2tB3iVnFD)
> 
> 
> — Metro Exodus (@MetroVideoGame) [March 25, 2021](https://twitter.com/MetroVideoGame/status/1375056615880134658?ref_src=twsrc%5Etfw)



Por cierto, y aunque parezca mentira, según una respuesta al tweet anterior, [el juego soportará Ray Tracing en Linux](https://twitter.com/MetroVideoGame/status/1375081207814361088), y si no me equivoco será el primero que lo haga oficialmente. Ciertamente se echaba de menos un **título AAA** de este calado en nuestra plataforma, pues ultimamente escasean, por lo que le damos publicamente las gracias a [4A Games](https://www.4a-games.com.mt/) su esfuerzo por traernos sus geniales obras a nuestro sistema. La verdad es que un servidor ya está contando los días para echarle el guante, aunque siendo sincero con la de l**anzamientos interesantes que nos esperan el mes que viene** ([TW:Rome Remastered]({{ "/posts/total-war-rome-remastered-se-lanzara-en-linux" | absolute_url }}), [Classic Racers Elite]({{ "/posts/classic-racers-estara-de-vuelta-con-su-version-elite-a1" | absolute_url }}), [ETS2 Iberia]({{ "/posts/euro-truck-simulator-2-se-actualiza-a-la-version-1-40" | absolute_url }}) ...) no se si daré abasto. Seguimos informando....




---


  
**NOTICIA ORIGINAL (15-2-20):** En el día que finalmente [**Metro Exodus llega a Steam**](https://store.steampowered.com/app/412020/Metro_Exodus/) después de su polémico paso por la controvertida Epic Store,  nos acbamos  de lllevar un sorpresón al leer la contestación que hemos recibido a una pregunta que realizamos hace un rato en twitter:



> 
> We are working on it, more news soon.
> 
> 
> — Metro Exodus (@MetroVideoGame) [February 15, 2020](https://twitter.com/MetroVideoGame/status/1228697765007515648?ref_src=twsrc%5Etfw)


  

Pues



Como veis las noticias son muy esperanzadoras, porque no solo confirman que están trabajando en ello, sinó que pronto tendremos noticias, por lo que se supone que lo tienen bastante afianzado. Como sabeis, el juego está desarrollado por **4A Games** y distribuido por **Deep Silver**. No sabemos más que lo que anuncia el tweet, por lo que no tenemos ni idea si es un port interno o algún desarollador o estudio externo está trabajando para traérnoslo. Como sabeis los dos anteriores juegos de la saga, [Metro 2033](https://www.humblebundle.com/store/metro-2033-redux?partner=jugandoenlinux) y [Metro Last Light](https://www.humblebundle.com/store/metro-last-light-redux?partner=jugandoenlinux) tienen su correspondiente versión para nuestro sistema y funcionan muy bien.


Esperemos que más pronto que tarde os estemos contando el lanzamiento de este gran juego de acción y terror ambientado en un Moscú destruido por la guerra atómica. Os dejamos con el trailer de lanzamiento del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/l3dfIglDzAs" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Podeis darnos veustra opinión sobre este juego y los de su saga en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

