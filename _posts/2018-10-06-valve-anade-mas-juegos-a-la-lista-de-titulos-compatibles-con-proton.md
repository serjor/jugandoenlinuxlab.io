---
author: Serjor
category: Noticia
date: 2018-10-06 07:30:09
excerpt: "<p>M\xE1s t\xEDtulos han sido a\xF1adidos a la lista blanca</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/11ff87b754b564cf2c60e2628d5ec100.webp
joomla_id: 871
joomla_url: valve-anade-mas-juegos-a-la-lista-de-titulos-compatibles-con-proton
layout: post
tags:
- steam
- valve
- steam-play
- proton
title: "Valve a\xF1ade m\xE1s juegos a la lista de t\xEDtulos compatibles con Proton\
  \ (ACTUALIZADO)"
---
Más títulos han sido añadidos a la lista blanca

**ACTUALIZACIÓN 14-11-18:** Esta mañana hemos recibido la buena noticia gracias a [SteamDB](https://steamdb.info/app/891390/history/) de que han sido añadidos a la "whitelist" de Steam Play una buena cantidad de títulos completamente compatibles con Proton. Entre ellos encontramos títulos bastante interesantes entre los que destacan "**Dark Souls III**", "**LEGO Star Wars III: The Clone Wars**", "**FlatOut 2**", "**Star Wars: Dark Forces**", "**The Curse of Monkey Island**" o "**Final DOOM**". La lista completa os la dejamos aquí abajo:


* Final DOOM
* FlatOut 2™
* Bullet Candy
* AaAaAA!!! - A Reckless Disregard for Gravity
* Poker Night at the Inventory
* STAR WARS™ - Dark Forces
* LEGO® Star Wars™ III - The Clone Wars™
* Alien Shooter 2: Reloaded
* Samorost 2
* Jamestown
* Nidhogg
* South Park™: The Stick of Truth™
* Primordia
* PAC-MAN™ Championship Edition DX+
* Dungeon of the Endless™
* Year Walk
* The Room
* Tiny Bridge: Ratventure
* Mega Man Legacy Collection
* DARK SOULS™ III
* Zoombinis
* Wuppo
* STEINS;GATE
* Pathfinder Adventures
* Star Explorers
* Glass Masquerade
* The House of Da Vinci
* Mysterium: A Psychic Clue Game
* Oh...Sir! The Hollywood Roast
* 12 Labours of Hercules VII: Fleecing the Fleece (Platinum Edition)
* The Curse of Monkey Island
* A Raven Monologue
* Endless Road
* Picross Fairytale: Legend of the Mermaid
* I’m not a Monster
* Starless Night
* Hags Castle
* Acid Nimbus
* SEPTEMBER 1999


Como veis poco a poco, esta lista está creciendo y gracias a ello disponemos oficialmente de más y más juegos "compatibles" con nuestro sistema. Seguiremos atentos....


 




---


Esta mañana abríamos el ojo con un saludo mañanero de nuestro compañero lorgault:


 







 Y es que como podéis leer en el tweet original de Pierre-Loup, han añadido nuevos juegos a la lista de juegos que oficialmente funcionan con Proton. Quizás no sea el listado con los juegos más interesantes del mundo, pero es bueno ver que siguen avanzando.


La lista la podéis encontrar [aquí](https://steamdb.info/app/891390/history/?changeid=5187653), y la lista completa de los juegos soportados hasta la fecha en [este otro enlace](https://steamdb.info/app/891390/), pero si os gusta el riesgo, podéis consultar [el listado no oficial](https://spcr.netlify.com/) mantenido por la comunidad o el [hilo](index.php/foro/juegos-steam-play/127-tests-de-compatibilidad-de-tus-juegos-en-steam-play#352) en nuestro foro.


Si tienes alguno de los juegos que han anunciado y lo pruebas en tu equipo, déjanos un comentario de cómo funcionan o pásate por nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD) para hablar de ello.


 

