---
author: Pato
category: Aventuras
date: 2019-10-10 13:42:01
excerpt: <p>El llamativo juego de @Twirlbound nos trae supervivencia en mundo abierto</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e6c5ec4137a2831befad7e6d46559451.webp
joomla_id: 1119
joomla_url: pine-ya-esta-disponible-para-linux-steamos-hoy-dia-de-su-lanzamiento
layout: post
tags:
- aventura
- supervivencia
- mundo-abierto
title: "Pine ya est\xE1 disponible para Linux/SteamOS hoy d\xEDa de su lanzamiento"
---
El llamativo juego de @Twirlbound nos trae supervivencia en mundo abierto

El llamativo Pine estaba anunciado para su lanzamiento hoy mismo y los chicos de Twirlbound han cumplido su palabra teniéndolo disponible desde ya para nuestro sistema favorito. Pine es un juego de tipo supervivencia en mundo abierto donde el entorno cambia y se adapta para sobrevivir. Cada criatura posee sus propias características que emplearán para ofrecernos distintos tipos de comportamiento según nuestras propias decisiones.


Pine es un juego de simulación, aventura y acción en un mundo abierto. Transcurre en el precioso mundo del Albamare. Encarnarás a Hue, un joven inteligente que tendrá que explorar, comerciar y abrirse camino a la fuerza en un mundo vibrante lleno de criaturas mucho más listas que los humanos.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/myRD9hzp7K4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


*Mientras buscas un nuevo hogar para tu tribu, tendrás que enfrentarte a la ecología de gran escala de Albamare. Para prepararte, tendrás que explorar, hacer trueques, hablar, fabricar objetos y luchar en seis biomas diferentes, con y contra un variopinto elenco de especies que no se limitarán a ayudarte.*


*En el mundo abierto de Pine irán pasando cosas incluso sin tu participación. Todos los organismos están vivos e intentarán hacer todo lo posible por sobrevivir. Puedes hacerte amigo de una especie, luchar a su lado o atacarla, robarle la comida u obligarla a abandonar su hábitat. Todo esto hará que las especies se muevan por todo el espectro ecológico.*


Los creadores admiten claras inspiraciones en juegos como Zelda, Shadow of Mordor o Fable entre otros.


En cuanto los requisitos, son:


* Requiere un procesador y un sistema operativo de 64 bits
* **SO:** 64-bit OS
* **Procesador:** Quad Core 3.2 Ghz o equivalente
* **Memoria:** 8 GB de RAM
* **Gráficos:** NVIDIA GeForce GTX 660 or equivalente
* **Almacenamiento:** 4 GB de espacio disponible


**RECOMENDADO:**


+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** 64-bit OS
+ **Procesador:** Quad Core 3.40 Ghz o equivalente
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** NVIDIA GeForce GTX 970 or equivalente
+ **Almacenamiento:** 4 GB de espacio disponible


Pine además viene con traducción al español. Si quieres saber mas, puedes visitar su [página web oficial](https://pine-game.com/) o si estás interesado lo tienes [en GOG](https://www.gog.com/game/pine?utm_medium=social&utm_source=twitter&utm_campaign=20191010_pine_entw&utm_term=EN) o en su [página de Steam](https://store.steampowered.com/app/1042780/Pine/?snr=1_7_15__13) con un 10% de descuento por su lanzamiento.

