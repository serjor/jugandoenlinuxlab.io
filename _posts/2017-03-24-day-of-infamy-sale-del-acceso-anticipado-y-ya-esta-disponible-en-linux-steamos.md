---
author: Pato
category: "Acci\xF3n"
date: 2017-03-24 20:09:51
excerpt: "<p>El juego de los creadores de 'Insurgency' es acci\xF3n b\xE9lica en la\
  \ II Guerra Mundial</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5b62d01506bd8a53b6c4928e25fa9b8a.webp
joomla_id: 263
joomla_url: day-of-infamy-sale-del-acceso-anticipado-y-ya-esta-disponible-en-linux-steamos
layout: post
tags:
- accion
- disparos
- primera-persona
- fps
title: "'Day of Infamy' sale del acceso anticipado y ya est\xE1 disponible en Linux/SteamOS"
---
El juego de los creadores de 'Insurgency' es acción bélica en la II Guerra Mundial

Después de un tiempo en acceso anticipado, los creadores del notable "Insurgency" nos traen este 'Day of Infamy' [[web oficial](http://newworldinteractive.com/#day-of-infamy)] que podemos decir que es la continuación de aquel. En 'Day of Infamy' nos arrastraremos por los campos de batalla del sur y el oeste de Europa durante la II Guerra Mundial tomando parte en los diferentes frentes ya sea en las playas, en poblados o en campo abierto y llevando la acción a la primera línea de batalla. 


En 'Day of Infamy' Tendremos a nuestra disposición un variado y realista arsenal de armas, y tendremos que desarrollar nuestra componente estratégica de acuerdo a nuestro grupo de soldados para que nuestras decisiones durante la acción nos lleven a la victoria. Por cierto, el juego no escatima en gore. Avisados estáis:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="360" src="https://www.youtube.com/embed/q0JbGNhP7TI" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Tendremos a nuestra disposición 9 clases y 10 modos de juego basados en objetivos. Si tenemos éxito obtendremos ascensos y medallas, y podremos desbloquear unidades históricas, skins y otras mejoras visuales, pero ninguna que suponga una ventaja respecto a otros jugadores. Así se mantiene el equilibrio entre facciónes sin afectar a la jugabilidad.


El juego además viene con soporte de Steam Workshop y mods, por lo que podrás descargar mods y mejoras hechas por la comunidad.


En cuanto a los requisitos, los mínimos son:


SO: Ubuntu 12.04  
Procesador: Dual core Intel® o AMD a 2.8 GHz  
Memoria: 4 GB de RAM  
Gráficos: NVIDIA GeForce 8600/9600GT, ATI/AMD Radeon HD2600/3600 (Drivers: nVidia 310, AMD 12.11), OpenGL 2.1  
Red: Conexión de banda ancha a Internet  
Almacenamiento: 12 GB de espacio disponible  
Tarjeta de sonido: OpenAL Compatible


Y los recomendados:


Procesador: 4+ núcleos  
Memoria: 8 GB de RAM  
Gráficos: 1536MB VRAM o mas  
Red: Conexión de banda ancha a Internet  
Almacenamiento: 15 GB de espacio disponible  
Notas adicionales: Disco SSD


'Day of Infamy' no está disponible en español, pero si eso no es problema para ti, lo tienes disponible en su página de Steam:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/447820/" width="646"></iframe></div>


 

