---
author: Serjor
category: "Simulaci\xF3n"
date: 2018-08-30 19:13:10
excerpt: "<p>Un viejo cl\xE1sico en los juegos de gesti\xF3n que se moderniza</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b003cb6a8c5828399d548e534fa42cc6.webp
joomla_id: 846
joomla_url: two-point-hospital-ya-disponible
layout: post
tags:
- gestion
- lanzamiento
- two-point-hospital
title: Two Point Hospital ya disponible
---
Un viejo clásico en los juegos de gestión que se moderniza

Podemos leer gracias a GamingOnLinux que [Two Point Hospital](https://www.gamingonlinux.com/articles/two-point-hospital-released-with-same-day-linux-support.12453) está ya disponible, siendo un lanzamiento con versión nativa para GNU/Linux desde el primer día, cosa que es de agradecer, la verdad.


Este Two Point Hospital es un heredero espiritual del ya mítico Theme Hospital que a tantos nos enganchó a quienes nos pilló de jóvenes, ya que dos de los desarrolladores de Theme Hospital han formado parte en el desarrollo de este Two Point Hospital.


Para aquellos que nos conozcáis Theme Hospital o no hayáis visto nada de este Two Point Hospital os diremos que se trata de un juego en el que tendremos que gestionar un hospital, comprando material, gestionando las salas de espera, curar enfermedades... pero todo con mucho, mucho humor.


La verdad, es que no conocía este juego, y reconozco que Theme Hospital (así como Theme Park) son juegos que me marcaron y espero que este esté a la altura, se va directo a la lista de deseados :-P (¿Tendrá TH algo que ver en que me guste House y The Good Doctor?)


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/SjrcU1gwkjc" width="560"></iframe></div>


Si estás interesado puedes hacerte con él a través de este [enlace de afiliados](https://www.humblebundle.com/store/two-point-hospital?partner=jugandoenlinux) en Humble Bundle.


Si prefieres directamente desde Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/535930/" style="border: 0px;" width="646"></iframe></div>


Y tú, ¿estás dispuesto a gestionar un hospital? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

