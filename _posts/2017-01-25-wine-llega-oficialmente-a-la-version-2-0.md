---
author: leillo1975
category: Software
date: 2017-01-25 08:28:17
excerpt: "<p>&nbsp;</p>\r\n<p>&nbsp;</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6c16f95e5837b7a15cc22a32eb72fad8.webp
joomla_id: 190
joomla_url: wine-llega-oficialmente-a-la-version-2-0
layout: post
tags:
- wine
- directx
- windows
title: "Wine llega oficialmente a la versi\xF3n 2.0"
---
 


 

En el día de ayer, el conocido programa que nos permite usar programas diseñados para Windows en GNU/Linux, llegó oficialmente a su versión 2.0, lo cual representa un hito en el desarrollo de este proyecto. Como muchos sabreis Wine no es un emulador (Wine Is Not a Emulator) sinó más bien una capa de compatibilidad que se interpone entre el sistema operativo y el programa para Windows.


 


Tras un desarrollo largo de la rama 1.9 y tras varias versiones candidatas, concretamente seis, la gente de [WineHQ](https://www.winehq.org/announce/2.0) se complace en presentarnos la versión 2.0, la cual incluye numerosas novedades  y mejoras de las que muy probablemente ya tengais cuenta. Hace bien poco, por poner un ejemplo publicábamos el [rendimiento asombroso que mostraba la última entrega de DOOM](index.php/item/256-doom-2016-muestra-un-rendimiento-asombroso-con-wine-2-0-gracias-a-vulkan) con una versión de prueba. Entre las novedades más importantes que vamos a encontrar en la versión 2.0 estan:


 


- Mejor escalado en pantallas HiDPI


- Multiples mejoras en Textos y fuentes (soporte para Unicode 9)


- El soporte para el portapapeles se ha reimplementado para mejor compatibilidad


- Se implementa el soporte para mapeado de botones en controladores, así como de Force Feedack


- Los controladores de perféricos (HID) serán gestionados por Libudev


- Soporte de 64 bit para el motor Mono.


- Se ha actulizado el motor Gecko a la versión de Firefox 47


- La versión de GStreamer pasa de la versión 0.1 a la versión 1.0


- DirectSound soporta ahora el "downmixing" de 5.1 y 4.0 a Stereo.


- Mejoras en DirectDraw y DirecDX


- Soporte para Microsoft Office 2013, entre otras muchas aplicaciones.


 


Pero sin duda la caracteristica más importante, especialmente para nosotros los jugones, es la mejora espectacular en el soporte de funciones de DirectX 10 y 11, lo cual está permitiendo que multitud de juegos actuales que usan esas API's gáficas puedan ser jugados en nuestros equipos. Estas mejoras incluyen montones de instrucciones para  shaders de tipo 4 y 5, soporte para la lectura/escritura de sRGB, matrices de Texturas....


Si os quereis instalar en Ubuntu y derivadas esta última versión con las caracteristicas "Staging" solamente debeis abrir una terminal y seguir los siguientes pasos:


 



> 
> sudo add-apt-repository ppa:wine/wine-builds  
>  sudo apt-get update
> 
> 
> sudo apt-get install --install-recommends wine-staging
> 
> 
> 


Para instalar correctamente los enlaces simbólicos y no tener que teclear las rutas constantemente se recomienda instalar:



> 
> sudo apt-get install winehq-staging
> 
> 
> 


 


**Para otras distribuciones teneis las [instrucciones aquí](https://www.winehq.org/download)**


 


**FUENTES: [WineHQ](https://www.winehq.org/announce/2.0), [Reddit](https://www.reddit.com/r/linux_gaming/comments/5pyq3q/the_wine_team_is_proud_to_announce_that_the/), [OMGUbuntu](http://www.omgubuntu.co.uk/2017/01/wine-2-0-released-supports-microsoft-office-2013)**


