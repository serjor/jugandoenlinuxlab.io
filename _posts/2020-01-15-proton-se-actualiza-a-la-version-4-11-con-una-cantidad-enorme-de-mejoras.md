---
author: Pato
category: Noticia
date: 2020-01-15 09:10:00
excerpt: "<p>Nueva revisi\xF3n de la anterior rama de la herramienta de @ValveSoftware</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/88431b5dd05b1e90847e42adef0f6769.webp
joomla_id: 1083
joomla_url: proton-se-actualiza-a-la-version-4-11-con-una-cantidad-enorme-de-mejoras
layout: post
tags:
- steamos
- steam
- valve
- steam-play
- proton
title: "Proton se actualiza a la versi\xF3n 4.11-13"
---
Nueva revisión de la anterior rama de la herramienta de @ValveSoftware


**ACTUALIZACIÓN 5-3-20:** Así por sorpresa nos pilla este nuevo lanzamiento de la versión [4.11-13](https://github.com/ValveSoftware/Proton/releases/tag/proton-4.11-13). El equipo encargado de la herramienta de Steam Play da un paso atrás y actualiza **arreglando los modos baja profundidad de color que fallaban en cierto hardware**. Además de eso no hay más cambios
 


---


**Actualización 15-1-20**: Hace unas horas que nos ha llegado el aviso de una nueva versión de Proton, en este caso la versión [4.11-12](https://github.com/ValveSoftware/Proton/releases/tag/proton-4.11-12), que basicamente incorpora correcciones y actualizaciones de componentes varias. La lista de cambios es la siguiente:

*-Actualiza DXVK a la v1.5.1.*  
 *-Corrige los botones del controlador de Xbox en Elex.*  
*-Mejora el comportamiento del cursor del ratón en IL-2 Sturmovik.*  
 *-Apoya los nuevos SDKs de OpenVR. Audioshield y Dance Collider son ahora reproducibles.*  
 *-Actualiza el soporte del SDK de steamworks.*




 


---


**Actualización 19-12-19**:Si hace tan solo unos días se actualizaba esta herramienta, ahora Valve vuelve a la carga con esta pequeña revisión, la **4.11-11**, que incluye unos pocos pero importantes [cambios](https://github.com/ValveSoftware/Proton/releases/tag/proton-4.11-11):

*-Soluciona el problema de la tartamudez del cursor del ratón durante las sesiones de juego largas.*  
 *-Actualiza **DXVK a la v1.5**, que ahora **incluye D9VK**.*  
 *-La última actualización de **GTA5 ahora se puede jugar**.*  
 *-Se ha restaurado la compatibilidad con los botones de cursor del ratón asignados.*



 


---


**Actualización 13-12-19**: Hace unos segundos que hemos recibido la notificación y aquí estamos ofreciendote la información de primera mano. En esta ocasión Proton alcanza la [versión 4.11-10](https://github.com/ValveSoftware/Proton/releases/tag/proton-4.11-10), que al contrario que la anterior versión **viene cargada de importantes y jugosas novedades**, como las que podeis ver a continuación:  

*-**Halo: The Master Chief Collection** es jugable! Los usuarios de distribuciones más antiguas (con gnutls anteriores a 3.5.4) necesitarán el cliente Steam Beta por ahora. Algunos modos de juego están deshabilitados debido a la falta de soporte para EasyAntiCheat.*  
 *-**Mejoras importantes en el manejo del ratón**. En nuestras pruebas, esto mejoró el comportamiento de los ratones **Fallout 4**, Furi y Metal Gear Solid V.*  
 *-Añadido **nuevo modo de escalado de enteros**, para dar píxeles nítidos al escalar. Esto se puede habilitar con esta variable de entorno: WINE_FULLSCREEN_INTEGER_SCALING=1.*  
 *-**Arreglados un par de problemas de asignación de controladores**. Esto es conocido por mejorar los juegos Telltale con los controladores de Xbox y Cuphead e ICEY cuando se utilizan los controladores de PlayStation 4 sobre Bluetooth.*  
 *-**Mejora el manejo del force feedback, especialmente para los controladores del volante. (thx Bernat)***  
 *-**Arreglar el cuelgue de Metal Gear Solid V** en el inicio.*  
 *-**Corrección de la regresión del rendimiento del controlador de Xbox**.*  
 *-El framerate de Trine 4 ya no debe bloquearse a 30 FPS.*  
 *-Corrección de caídas frecuentes en IL-2 Sturmovik.*  
 *-**D9VK actualizado a 0.40-rc-p***  
 *-**FAudio actualizado a 19.12.***  
 *-Corrección de errores de DXVK.*


Como veis esta actualización no es moco de pavo, y parece que en Valve/Codeweavers han estado trabajando duro. Desde aquí también nos gustaría felicitar a un miembro de nuestra comunidad, **@Bernat**, desarrollador de [Oversteer](index.php/component/k2/12-software/1139-la-nueva-version-de-oversteer-nos-trae-interesantes-anadidos), [Ffbtools](https://github.com/berarma/ffbtools) y [el driver new-lg4ff](index.php/homepage/generos/software/1131-new-lg4ff-un-nuevo-driver-mucho-mas-completo-para-nuestros-volantes-logitech), por estar involucrado en la resolución de problemas en Proton relacionados con el Force Feedback en determinados juegos, lo que permitirá a partir de ahora sentir estos efectos en Project Cars 2, y si usamos [su driver](https://github.com/berarma/new-lg4ff) en muchos más, como RACE 07, GT Legends, Automobilista, rFactor, rFactor 2, entre otros.



....y ahora a probarla!
 


---


 
**Actualización 28-11-19**: Hace escasas 6 horas que los desarrolloadores de Valve que trabajan en esta herramienta de compatibilidad basada en Wine, la han actualizado una vez más. En esta ocasión alcanza la revisión número 9 de la rama 4.11, y aunque tiene pocas novedades, en esta ocasión corrigen una regresión que hace algún tiempo está latente y que notamos los que usarios de volantes Logitech, y era la falta de Force Feedback. Os listamos esta y el resto de [novedades de esta versión 4.11-9](https://github.com/ValveSoftware/Proton/wiki/Changelog):

-Arregla la regresión de rendimiento en 4.11-8 que afectaba a los juegos de 32 bits que utilizan DXVK y D9VK.  
 -Corrección de reportes sobre la falta de memoria en la GPU para determinadas GPUs.  
 -Restaura el Force Feedback en los controladores del volante.  
 -Corrige el fallo de lanzamiento de Crazy Machines 3 con ciertas GPUs



Como veis en esta ocasión, basicamente se arreglan fallos de versiones anteriores. Suponemos que estarán preparando la proxima versión ya basada en una nueva rama de Wine.
 


---


**Actualización 9-11-19:** Valve ha lanzado una nueva iteración de la versión 4.11 de Proton, la versión 4.11-8, con las siguientes [novedades](https://github.com/ValveSoftware/Proton/wiki/Changelog#411-8):
* Se incluye por defecto VKD3D, para dar soporte a juegos con Direct3D 12
* Mejoras para el lanzador de Rockstart y GTA V
* Mejor soporte de mandos y joysticks para Farming Simulator 19 y Resident Evil 2
* Corregido la entrada del ratón en Arma 3
* DmC: Devil May Cry ya es jugable
* DXVK actualizado a la versión 1.4.4
* D9VK actualizado a la versión 0.30
* FAudio actualizado a la versión 4.9.4
* Se ha reducido el tamaño en disco que ocupa Proton
* Wine y otras las librerías que se distribuyen junto con Proton se hace sin los símbolos de depuración (si fueran necesarias para reportes o para desarrolladores se puede seleccionar desde el cliente de steam)


 

---


**Actualización 10-9-19:** Una vez más os traemos la noticia de un nuevo lanzamiento de esta genial herramienta que es Proton, llegando en esta ocasión a la versión 4.11-7, que viene cargada de novedades interesantes, especialmente en lo tocante a los dispositivos de control. La [lista de cambios](https://github.com/ValveSoftware/Proton/releases/tag/proton-4.11-7) es más extensa que en la versión anterior:

*-Mejoras importantes en la **conexión en caliente del controlador.** Para los juegos que lo soportan, los controladores deberían funcionar incluso si están conectados después de que el juego haya comenzado.*  
 *-Mejora del **soporte de controladores** para juegos que utilizan la biblioteca Rewired Unity, como ICEY.*  
 *-Actualización de **wine-mono** a 4.9.3, lo que mejora la representación de fuentes y algunos problemas menores de compatibilidad de juegos como Age of Wonders: Planetfall.*  
 *-Arreglado el crasheo del arranque de **Kingdom Come: Deliverance**.*  
 *-Actualizado **DXVK** a la versión 1.4.2.*  
*-Actualizado **D9VK** a 0,22.*  
 *-Arreglado un posible fallo con algunos juegos de **VR**.*



Como siempre, podeis dejar vuestros opiniones y pruebas en los comentarios, o vuestros mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


---


**Actualización 24/09/2019:** Otra vez en un periodo de tiempo corto, Valve lanza otra versión de  su herramienta, y de nuevo vuelve a tratarse de pequeñas correcciones, pero que introducen cambios que permiten que podamos volver a jugar por ejemplo a GTAV, juego que desde hace unos días, y tras un cambio de launcher no funcionaba y traía de cabeza a un montón de gente (entre ellos yo) como se puede ver en [el "issue" dedicado a ello](https://github.com/ValveSoftware/Proton/issues/37) en el Github de Proton. En esta nueva versión encontraremos los siguientes [cambios](https://github.com/ValveSoftware/Proton/wiki/Changelog):


     -Surge 2 ahora es jugable (actualmente no funciona en radv).  
     -Principios de soporte para el nuevo lanzador de Rockstar. Hay más trabajo por hacer aquí, pero Grand Theft Auto 5 debería ser jugable nuevamente.  
     -Actualizado [DXVK a v1.4](https://github.com/doitsujin/dxvk/releases/tag/v1.4). (entre otras muchas cosas introduce importantísimas mejoras, siendo las interfaces actualizadas a la versión **D3D11.4**, que es la última disponible en Windows 10)
 




---


**Actualización 18/09/2019:** .... y aquí tenemos otra versión recien salida del horno tan solo unos días después. En esta ocasión son unas pocas correcciones, pero no por ello menos interesantes:


*-Corregido un fallo causado por ciertos dispositivos de entrada que se introdujo en Proton 4.11-4.*  
 *-Corrige los juegos que se ejecutan en escritorios virtuales y ventanas semitransparentes, problemas ambos introducidos en Proton 4.11-4.*  
 *-Optimización del rendimiento cuando se utiliza un controlador en el driver amdgpu.*  
 *-Mejora de la implementación de fsync cuando se establece WINEFSYNC_SPINCOUNT.*


 




---


**Actualización 14/09/2019:** Nueva revisión de la actual versión (4.11) recien salida del horno, que incluye las siguientes e interesantes novedades:


-Mejora el comportamiento de los mandos de PlayStation 4 y de los mandos conectados por Bluetooth.  
 -Más mejoras en la captura del ratón y en el comportamiento de pérdida de enfoque de la ventana.  
 -Farming Simulator 19 es ahora jugable.  
 Corrección de algunos problemas gráficos en "A Hat in Time" y "Ultimate Marvel vs Capcom 3".  
 -Actualizado DXVK a la versión 1.3.4  
 -Actualizad0 D9VK a 0.21-rc-p.  
 -Actualizado FAudio a 19.09.


Como veis no es una gran cantidad de cambios, pero el proyecto avanza poco a poco y de forma constante.Continuaremos informando.




---


**Actualización 27/08/2019:** Proton se vuelve a actualizar alcanzando la versión 4.11-3 que incluye las siguientes [novedades](https://github.com/ValveSoftware/Proton/wiki/Changelog):


*-Ahora los juegos intentarán acceder directamente a los gamepads, en lugar de emular los gamepads de Proton como si fueran controladores de Xbox. Esto significa que los gamepads de PlayStation 4, así como los gamepads de lucha y otros mandos se comportarán mucho más como lo hacen en Windows. Si quieres que tu gamepad sea emulado como si fuera un controlador de Xbox, por favor usa la función de asignación de controladores del cliente Steam. Si no se puede acceder directamente al controlador, debido a un problema de permisos, seguirá presentándose como un controlador de Xbox.*  
 *-fsync se bloquea y se solucionan los fallos.*  
 *-Agregar soporte para el conteo de giros configurable de fsync, puede ayudar al rendimiento pero está deshabilitado de manera predeterminada Intente configurar WINEFSYNC_SPINCOUNT = 100*  
 *-Agregafa la fuente que falta en los idiomas chino, japonés y coreano.*  
 *-Corrección en el navegador web del juego en algunos juegos incluyendo Bloons TD 6.*  
 *-Corrección de nuevos bloqueos relacionados con la entrada de texto, especialmente en los juegos de Unreal Engine 4, incluyendo Mordhau y Deep Rock Galactic.*  
 *-Actualizado [D9VK a 0.20.](https://github.com/Joshua-Ashton/d9vk/releases/tag/0.20)*  
 *-Mejoras en el soporte para títulos de RV muy antiguos.*  
 *-Soporte para las últimas versiones de Steamworks y OpenVR SDK.*




---


**Actualización 08/08/2019:** Actualización menor de la rama 4.11 ya [disponible](https://github.com/ValveSoftware/Proton/releases/tag/proton-4.11-2):



* DXVK actualizado a la versión [v1.3.2](https://github.com/doitsujin/dxvk/releases/tag/v1.3.2).
* FAudio actualizado a la versión 19.08.
* Wine-mono actualizado a la versión 4.9.2. Este cambio soluciona los problemas y DARK y sus DLC
* Para juegos antiguos que requieren una tasa de refresco de 60FPS en los monitores, ahora se informa al juego de este modo en monitores de altas velocidades de refresco
* Earth Defense Force 5 y Earth Defense Force 4.1 no se colgarán al solicitar una entrada de texto





---


**NOTICIA ORIGINAL:**  
Una nueva actualización de Proton ha sido liberada tal y como nos cuenta Pierre Loup, con mejoras significativas prácticamente a todos los niveles, y con nuevas tecnologías para seguir mejorando la compatibilidad de los juegos de Windows con Steam Play, o buscando añadir nuevos títulos a la lista de juegos compatibles (aún de manera no oficial).


Esta vez, la versión viene numerada como 4.11, y trae las siguientes novedades:


* Traslado de parches de proton sobre la versión de Wine 4.11. Esto trae más de 3300 mejoras a Wine en Proton. Se actualizaron 154 parches desde Proton 4.2 o ya no son necesarios.
* Proton ahora incluye D9VK v0.13f. D9VK es un procesador experimental Direct3D 9 basado en Vulkan. El usuario debe habilitarlo con la configuración de usuario con PROTON_USE_D9VK.
* Proton ahora incluye soporte experimental para procesos de sincronización primitivas basadas en proceso futex, que pueden reducir el uso de CPU en comparación con esync. Por ahora, esto requiere soporte especial del núcleo. Consulte [este hilo del foro](https://steamcommunity.com/app/221410/discussions/0/3158631000006906163/) para obtener instrucciones de prueba.
* La frecuencia de actualización actual de la pantalla ahora se informa a los juegos.
* Actualización DXVK a v1.3.
* Solución de problemas con el administrador de ventanas y foco del cursor del ratón.
* Correcciones en el retraso de entrada del joystick y la compatibilidad con ruidos en ciertos juegos, especialmente los títulos de Unity.
* Soporte para los últimos SDK de OpenVR.
* Actualización a FAudio a 19.07.
* Solución para problemas de red en los juegos de GameMaker.
* Muchos módulos Wine ahora se crean como archivos de Windows PE en lugar de bibliotecas de Linux. A medida que avanza el trabajo en esta área, esto eventualmente ayudará a algunos sistemas DRM y anti-trampa. Si compila Proton localmente, es probable que necesite volver a crear la máquina virtual Vagrant para construir archivos PE.


Toda la información de la versión está en la página del [proyecto en Github](https://github.com/ValveSoftware/Proton/wiki/Changelog#411-1).


Viendo toda esta cantidad de novedades, nos surgen cierta cantidad de preguntas:


¿Que os parece todos los cambios que se anuncian en esta nueva versión de Proton? ¿Creéis que todo este esfuerzo que Valve está poniendo en tecnologías para Linux tendrá una finalidad concreta? ¿Que finalidad creéis que tendrá el crear los módulos como archivos de Windows? ¿Estaremos cerca de ver la compatibilidad de los sistemas DRM con Proton?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

