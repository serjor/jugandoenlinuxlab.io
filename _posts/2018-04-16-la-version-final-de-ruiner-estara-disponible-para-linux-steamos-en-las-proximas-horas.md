---
author: Pato
category: "Acci\xF3n"
date: 2018-04-16 19:06:41
excerpt: <p>El juego ya tiene disponibilidad para nuestro sistema favorito</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4f411c7dd400d8cc3ebd00031ff0e07d.webp
joomla_id: 712
joomla_url: la-version-final-de-ruiner-estara-disponible-para-linux-steamos-en-las-proximas-horas
layout: post
tags:
- accion
- devolver-digital
- ruiner
title: "La versi\xF3n \"final\" de Ruiner esta disponible para Linux/SteamOS (actualizaci\xF3\
  n)"
---
El juego ya tiene disponibilidad para nuestro sistema favorito

**(Actualización)**


Tal como anunciamos ayer, el juego ya está disponible en nuestro sistema favorito, e incluso ya se puede conseguir en la tienda [GOG DRM free](https://www.gog.com/game/ruiner) y **con un 50% de descuento** por el lanzamiento en Linux!!


¡Corred insensatos!




---


Como ya sabéis si nos seguís desde hace algún tiempo, RUINER es un título más que interesante que [nos llegó en acceso beta](index.php/homepage/generos/accion/item/824-ruiner-disponible-en-gnu-linux-a-partir-del-6-de-abril) el pasado día 6 de este mismo mes. Durante estos días los desarrolladores han estado puliendo el juego y por lo que parece en las próximas horas tendremos la versión "final". Así lo han publicado [en el hilo](https://steamcommunity.com/app/464060/discussions/0/154644928862239593/?tscn=1523838226) donde se está siguiendo todo el desarrollo de la versión beta.   
  



*"we're doing some more tweaks today and will be releasing a final version later today\*."*


*"Estamos haciendo algunos retoques más hoy y lanzaremos una versión final más tarde."*


De hecho, el juego ya tiene el icono de SteamOS en su página de Steam.


Como sabéis, RUINER es un emocionante y brutal juego de disparos ambientado en el año 2091 en la cibermetrópolis de Rengkok. Un sociópata trastornado se rebela contra el corrupto sistema para rescatar a su hermano secuestrado y descubrir la verdad guiado por una misteriosa amiga hacker. Combina reflejos sobrenaturales, herramientas mejoradas y el arsenal de enemigos muertos con el objetivo de desmantelar a los titanes corporativos del tráfico de virtualidad en CIELO.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Rg2uB60DFS0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Ya hemos hablado largo y tendido de RUINER. Si quieres saber más, lo tienes YA disponible en su página de Steam traducido al español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/464060/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

