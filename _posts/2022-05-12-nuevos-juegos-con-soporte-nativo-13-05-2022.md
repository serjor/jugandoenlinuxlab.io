---
author: Jugando en Linux
category: Apt-Get Update
date: 2022-05-12 09:53:16
excerpt: ''
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Newterra/newterra.webp
joomla_id: 1467
joomla_url: nuevos-juegos-con-soporte-nativo-13-05-2022
layout: post
tags:
- ofertas
- novedad
title: Nuevos juegos con soporte nativo 13/05/2022
---

 Una semana más, vamos a repasar lo que nos ha ido llegando recientemente y que merezca la pena:


#### Minigolf Adventure


DESARROLLADOR: Revulo Games
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/GKtwQiV-bE8" title="YouTube video player" width="720"></iframe></div>


 
<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1830640/" width="646"></iframe></div>




---


####  NEW TERRA®


DESARROLLADOR: WINTERYEAR Studios Los Angeles
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/5C-onKfpjA0" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1806860/" width="646"></iframe></div>




---


 


#### OFERTAS


#### Blasphemous


DESARROLLADOR: The Game Kitchen
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/RpN0WeQPixU" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/774361/" width="646"></iframe></div>




---


####  A Total War Saga: THRONES OF BRITANNIA


DESARROLLADOR: CREATIVE ASSEMBLY, Feral Interactive
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/-Fu0UPnk4kc" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/712100/205990/" width="646"></iframe></div>




---


####  Hand of Fate 2


DESARROLLADOR: Defiant Development
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/rrSC9r4lVYA" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/456670/" width="646"></iframe></div>




---


#### Stellaris


DESARROLLADOR: Paradox Development Studio
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/zL0kemiI0yc" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/281990/38760/" width="646"></iframe></div>




---


#### Northgard


DESARROLLADOR: Shiro Games
 
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/mDN8PHOYnKc" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/466560/" width="646"></iframe></div>


 