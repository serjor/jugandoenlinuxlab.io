---
author: Serjor
category: Aventuras
date: 2017-04-19 20:47:13
excerpt: '<p>Arriar las velas, izar la mayor, Man O''War: Corsair desembarca en Linux</p>'
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f3b735e3300faff0e348331a5413d4d3.webp
joomla_id: 293
joomla_url: man-o-war-corsair-warhammer-naval-battles-disponible-para-linux
layout: post
tags:
- man-o-war
- warhammer
title: 'Man O'' War: Corsair - Warhammer Naval Battles disponible para Linux'
---
Arriar las velas, izar la mayor, Man O'War: Corsair desembarca en Linux

Gracias al aviso a través del grupo de [Telegram](https://t.me/jugandoenlinux), nuestro colaborador [yodefuensa](index.php/homepage/analisis/itemlist/user/210-yodefuensa) nos informa de que, de la mano de [Evil Twin Artworks](http://eviltwinartworks.com/), Man O' War: Corsair está disponible desde hoy mismo para Linux, siendo un lanzamiento con soporte desde el primer día.  
![manowar](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ManOWar/manowar.webp)


En Man O' War: Corsair capitanearemos un barco con el que podremos surcar los siete mares, asaltar barcos y hacernos con suculentos botines, todo esto dentro del universo Warhammer, así que nos podemos esperar orcos, megalodones, dragones y cualquier otro ser de este universo.


![ManOWarCorsairImage2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ManOWar/ManOWarCorsairImage2.webp)


Además el juego está de oferta de lanzamiento con un 10% de descuento, así que si quieres emular a Jack Sparrow, aprovecha el viento del norte para hacerte con él en Steam o GOG, aunque la [versión de GOG](https://www.gog.com/game/man_o_war_corsair) no indica que soporta Linux.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/kfJYghUCzW4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/344240/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


Y tú, ¿vas a conquistar tierras lejanas? Cuéntanos en los comentarios qué te parece el juego, o pásate por nuestros grupos de [Telegram](https://t.me/jugandoenlinux) o de [Discord](https://discord.gg/ftcmBjD).

