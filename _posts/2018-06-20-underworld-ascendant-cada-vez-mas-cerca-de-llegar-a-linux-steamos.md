---
author: Pato
category: Rol
date: 2018-06-20 14:32:18
excerpt: "<p><span class=\"username txt-mute\">@Underworld_Asc llegar\xE1 dentro de\
  \ muy poco<br /></span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/96cba9244d7f9076720df00586971bd0.webp
joomla_id: 778
joomla_url: underworld-ascendant-cada-vez-mas-cerca-de-llegar-a-linux-steamos
layout: post
tags:
- accion
- indie
- proximamente
- rol
title: Underworld Ascendant cada vez mas cerca de llegar a Linux/SteamOS (ACTUALIZADO
  2)
---
@Underworld_Asc llegará dentro de muy poco  


**ACTUALIZACIÓN 11-6-19**: Acabamos de leer en los [foros de Steam](https://steamcommunity.com/app/692840/discussions/0/2579854400737507730/?tscn=1560263386#c1639787494972545219) que los desarrolladores de Underworld Ascendant **lanzarán probablemente antes de fin de mes el port de su juego**, conjuntamente con la Actualización 4 del título. Si no fuese así se haría pocas semanas después. Al parecer el equipo externo que se está encargando del port ha hecho avances significativos solucionando algunos problemas con los sombreados y diversos errores visuales.


Esperemos que pronto podamos disfrutar de la versión para Linux de este interesante RPG en primera persona de [OtherSide Entertainment](https://otherside-e.com/wp/), de la cual no sabíamos nada desde hace ya demasiado tiempo. Os dejamos con su trailer para que se os haga la boca agua mientras esperais...


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/-sX3wsh02fs" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**ACTUALIZACIÓN 16-11-18:** Gracias a twitter y a [@**G4LNevertheless**](https://twitter.com/G4LNevertheless) nos llega la información de que el juego, que salió ayer a la venta en Windows, se retrasará entre 30 y 45 dias, ya que según dicen quieren lanzarlo poniendo la atención necesaria y adecuada para lanzarlos con garantías:



> 
> Unfortunately not; we are planning to release the Mac and Linux versions around 30-45 days after launch to make sure they have the attention they need. We appreciate your patience! (Also, we're looking for external playtesters for Mac and Linux to help us with this)
> 
> 
> — Underworld Ascendant (@Underworld_Asc) [November 14, 2018](https://twitter.com/Underworld_Asc/status/1062775425569579008?ref_src=twsrc%5Etfw)







Os seguiremos informando de las noticias que produzca este gran título y por supuesto de su salida oficial.




---


**NOTICIA ORIGINAL:** Por fin podemos ofrecer novedades sobre Underworld Ascendant [[web oficial](http://www.underworldascendant.com/)]. El juego es obra de **Otherside Entertainment**, creadores de la saga Ultima underworld o System Shock 3 en alianza con 505 Games. Se trata de un juego de rol con un gran nivel de libertad y donde las elecciones del jugador tendrán un peso muy importante en la jugabilidad.


*"Underworld Ascendant es un RPG de acción diseñado para maximizar las elecciones del jugador.*


*Adéntrate en terrenos desconocidos. Da rienda suelta a tu potencial creativo. Aprovecha el entorno para desequilibrar la balanza a tu favor. Planifica de antemano el plan de batalla ideal o lánzate a la aventura e improvisa sobre la marcha.*


*Cada decisión acarrea grandes oportunidades... pero también graves consecuencias. Lo que hagas a tu paso tendrá repercusiones en las vidas de los demás, que no olvidarán tus acciones. Siempre y cuando sobrevivas..."*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/GTXOQvMlucs" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Sabemos gracias a su [campaña en Kickstarter](https://www.kickstarter.com/projects/othersidegames/underworld-ascendant) que el juego tendrá versión para nuestro sistema favorito, y de hecho ya hemos visto los primeros movimientos en SteamDB para Linux, pero tal y como hemos podido saber a través de los [foros del juego](https://www.othersideentertainment.com/forum/index.php?PHPSESSID=8696f67600060b5d3f9b19dfa8ce2773&board=3.0), debido al propio proceso de desarrollo con Unity y los problemas con las versiones antiguas del motor han ido retrasando el desarrollo de forma que la versión para sistemas Linux vendrá un poco mas tarde que la de Windows que tiene una fecha estimada de lanzamiento en Septiembre de este mismo año.


Underworld Ascendant llegará primero a los backers de su campaña en fase beta. Aún no tenemos requisitos mínimos pero en cuanto tengamos más noticias os las haremos saber.


Puedes saber más de Underworld Ascendant en su [página we](http://www.underworldascendant.com/)b o en su [página de Steam](https://store.steampowered.com/app/692840/Underworld_Ascendant/).


 

