---
author: leillo1975
category: Aventuras
date: 2018-09-06 10:34:41
excerpt: "<p>@feralgames lanzar\xE1 el juego en unos d\xEDas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/84dcf8a8cba0259cf11299c1eef358f9.webp
joomla_id: 851
joomla_url: life-is-strange-before-the-storm-estara-muy-muy-pronto-en-linux
layout: post
tags:
- feral-interactive
- square-enix
- life-is-strange
- before-the-storm
- deck-nine
title: "\"Life is Strange: Before the Storm\" estar\xE1 muy, muy pronto en Linux."
---
@feralgames lanzará el juego en unos días

Desde el twitter de Feral acabamos de recibir la noticia que el juego de aventura y exploración "Life is Strange: Before the Storm" desarrollado para Windows por el estudio americano [Deck Nine](http://deckninegames.com/), y editado por [Square Enix](https://square-enix-games.com/), verá la luz en nuestro sistema en tan solo una semana, concretamente el próximo jueves 13 de Septiembre. Podeis ver el tweet del anuncio aquí:



> 
> On September 13th, Life is Strange: Before the Storm arrives on macOS and Linux!  
>   
> Pre-order on the Feral Store: <https://t.co/li1F3VtTgA> [pic.twitter.com/oC06k3kl83](https://t.co/oC06k3kl83)
> 
> 
> — Feral Interactive (@feralgames) [6 de septiembre de 2018](https://twitter.com/feralgames/status/1037647056029011968?ref_src=twsrc%5Etfw)



 Esta es la descripción de Steam del juego :



> 
> *Life is Strange: Before the Storm es una aventura independiente en tres episodios ambientada tres años antes del primer juego de esta serie que ha sido galardonada con un premio BAFTA.*  
>   
> *Juegas en el papel de Chloe Price, una adolescente rebelde que entabla una inesperada amistad con Rachel Amber, una chica guapa y popular destinada al éxito.*   
>   
> *Cuando Rachel descubre un secreto en su familia capaz de hacer que su mundo se desmorone, la nueva amistad que ha entablado con Chloe le da fuerzas para seguir adelante.*  
>   
> *Juntas tendrán que enfrentarse a sus problemas personales y encontrar una forma de superarlos.*  
>   
> *CARACTERÍSTICAS PRINCIPALES:*
> 
> 
> 


* > *Aventura narrativa con elecciones y consecuencias.*
* > *Múltiples finales dependiendo de las elecciones que tomes.*
* > *Insolencia, una modalidad de conversación donde Chloe le saca partido a su labia para provocar o salirse con la suya.*
* > *Deja huella en el mundo con pintadas ingeniosas.*
* > *Elige la ropa de Chloe y observa cómo la gente reacciona a tu estilo.*
* > *Particular banda sonora con música indie y temas originales de Daughter.*


 


Podeis reservar el juego en su edición deluxe en la [tienda de Feral](https://store.feralinteractive.com/en/mac-linux-games/lisbeforethestormdeluxe/) a un precio de 24.82€. Os dejamos con el video del lanzamiento:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/QINOnM9zRC8" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>

