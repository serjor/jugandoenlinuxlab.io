---
author: Serjor
category: "Acci\xF3n"
date: 2017-07-14 04:00:00
excerpt: "<p>La promoci\xF3n termina este 16 de julio</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9f9f79e544fb8eb705d18df3c18b9f0a.webp
joomla_id: 408
joomla_url: jotun-para-siempre-gratis-si-te-haces-con-el-este-fin-de-semana
layout: post
tags:
- free-weekend
- jotun
title: "Jotun para siempre gratis si te haces con \xE9l este fin de semana"
---
La promoción termina este 16 de julio

Nos avisaba un miembro de la comunidad en el canal de Telegram, y también nos avisaba el propio Steam:


![jotun steam](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Jotun/jotun_steam.webp)


Y es que durante este fin de semana el juego Jotun estará disponible de manera completamente gratuita, y además si nos hacemos con él, será nuestro para siempre. El motivo detrás de este evento es que quieren promocionar su nuevo juego, Sundered, el cuál también estará disponible para Linux y llegará el próximo 28 de julio.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/uB37tkDDNJA" width="560"></iframe></div>


 


No podemos sino animaros a que os hagáis con este juego si no lo tenéis ya.


 


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/323580/194057/" width="646"></iframe></div>


 


Y tú, ¿ya lo tenías o vas a aprovechar a hacerte con él durante este fin de semana? Cuéntanoslo en los comentarios o en nuestro canal de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

