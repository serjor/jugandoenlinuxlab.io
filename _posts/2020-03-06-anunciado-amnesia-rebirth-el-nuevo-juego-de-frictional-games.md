---
author: Pato
category: Terror
date: 2020-03-06 23:36:43
excerpt: "<p>Vuelve el terror m\xE1s inmersivo a nuestras pantallas. \xBFTe atrever\xE1\
  s?</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AmnesiaRebirth/amnesia_rebirth.webp
joomla_id: 1184
joomla_url: anunciado-amnesia-rebirth-el-nuevo-juego-de-frictional-games
layout: post
tags:
- indie
- primera-persona
- terror
title: Anunciado Amnesia Rebirth, el "nuevo" juego de Frictional Games
---
Vuelve el terror más inmersivo a nuestras pantallas. ¿Te atreverás?


Si llevas algunos años en este mundillo sabrás que hay nombres que por si solos tienen su propio significado. Si nos referimos a Frictional Games o decimos Amnesia, lo primero que se nos viene a la cabeza es **Terror**. Con mayúsculas.


Y es que Amnesia es por si mismo un referente en cuanto al género que consiguió mediante su fórmula de opresión ambiental, sigilo y sobresaltos un notable resultado. Pocos son los que pueden decir que han podido superar el juego con la luz apagada y unos cascos en las orejas sin haber pegado un grito o saltado de la silla.


Pues bien, **Frictional Games** ha estado desvelando mediante su web y un misterioso vídeo que estaban trabajando en un nuevo desarrollo:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/1AKyIjYlcFo" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Con esto nos han anunciado el nuevo "**Amnesia Rebirth**", lo nuevo del estudio que espera retornar a la formula que les dio el éxito y la fama y con la promesa de volver a hacérnoslo pasar mal. Volveremos a un nuevo descenso a la oscuridad. Un viaje angustioso por la desolación y la desesperación en el que exploraremos los límites de la resiliencia humana.


*No se te puede escapar ni un suspiro. La criatura está a pocos centímetros de ti. Su único propósito es alimentarse de tu terror. Te agachas en la oscuridad, intentando controlar tu miedo en aumento, intentando silenciar lo que llevas dentro.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/3NiQAlGUw_o" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


En Amnesia: Rebirth, eres Tasi Trianon y te despiertas en pleno desierto en Argelia. Han pasado días. ¿Dónde has estado? ¿Qué has hecho? ¿Dónde están los demás? Sigue las huellas del viaje, reúne los fragmentos de tu pasado hecho añicos; es tu única oportunidad de sobrevivir al despiadado horror que amenaza con devorarte.


Con decir Amnesia, sobran las palabras. Lo tendremos disponible este próximo otoño, en una fecha aún por especificar.


Prepara tus cascos.


Apaga la luz.


Si quieres saber más, Puedes visitar su [web oficial](https://amnesiarebirth.com/) o su [página de Steam](https://store.steampowered.com/app/999220/Amnesia_Rebirth/) donde puedes añadirlo a tu lista de deseados.


¿Te atreverás a jugar a un nuevo Amnesia?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

