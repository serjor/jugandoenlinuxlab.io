---
author: leillo1975
category: Carreras
date: 2017-10-27 15:48:53
excerpt: "<p>\xBFConfirmados los requisitos del sistema!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/949f13c1df30a468b76afab5b994011b.webp
joomla_id: 511
joomla_url: f1-2017-finalmente-llegara-a-gnu-linux-steamos-el-2-de-noviembre
layout: post
tags:
- feral-interactive
- conduccion
- codemasters
- racing
- f1-2017
title: "F1 2017 finalmente llegar\xE1 a GNU-Linux/SteamOS el 2 de Noviembre (ACTUALIZACI\xD3\
  N 3)"
---
¿Confirmados los requisitos del sistema!

**ACTUALIZACIÓN 31-10-17:** Nos llegan noticias de los requisitos técnicos del juego. En [palabras textuales de Feral](https://www.feralinteractive.com/es/news/823/) el juego pedirá lo siguiente:


 



> 
> *"Para jugar a F1 2017, necesitarás un procesador 3.3Ghz Intel Core i3-3225 con Ubuntu 17.04, 4 GB de RAM y una tarjeta gráfica Nvidia 680 de 2 GB o 3rd Generation AMD Graphics Core Next (Volcanic Islands) tarjeta gráfica o superior\*.*
> 
> 
> *Para un **rendimiento óptimo**, recomendamos un procesador 3.5Ghz Intel Core i5-6600K, 8 GB de RAM y una tarjeta gráfica Nvidia 1070 de 8 GB o superior.*
> 
> 
> *\*Tarjetas gráficas Intel no son compatibles. **Tarjetas gráficas Nvidia requieren la versión del controlador 384.90**+. **Tarjetas gráficas AMD requieren Mesa 17.2.2**."*
> 
> 
>  
> 
> 
> 


Como veis se trata de unos requisitos bastante comedidos, teniendo en cuenta la calidad gráfica y dificultad de procesamiento del juego. **Recordad que vuestra gráfica debe ser compatible con Vulkan**.




---


**ACTUALIZACIÓN 30-10-17:** Feral informa mediante tweet que el próximo miércoles día 1 de Noviembre a las 6:00 PM GMT, o lo que es lo mismo, a las 7 de la tarde hora de España, retransmitirán a través de [su canal de Twitch](https://go.twitch.tv/feralinteractive/) las primeras imágenes públicas del juego ejecutándose en Linux:


 



> 
> On Wednesday 1st November at 6PM GMT / 11AM PDT, [#FeralPlays](https://twitter.com/hashtag/FeralPlays?src=hash&ref_src=twsrc%5Etfw) F1™ 2017 for Linux. Get to the grandstands on Twitch – <https://t.co/d4ACzCZQ1e> [pic.twitter.com/q24FvuQM7I](https://t.co/q24FvuQM7I)
> 
> 
> — Feral Interactive (@feralgames) [October 30, 2017](https://twitter.com/feralgames/status/924955969666342914?ref_src=twsrc%5Etfw)


  







---


Feral acaba de confirmar ahora mismo que el juego solo funcionará con Vulkan por lo que no habrá forma de comparar rendimiento con OpenGL. De todas formas es una paso adelante en la implantación de esta API, ya que se deja de usar de forma experimental y secundaria para "normalizarse". El tweet que lo confirma es este:


 



> 
> [@AngryPenguinPL](https://twitter.com/AngryPenguinPL?ref_src=twsrc%5Etfw) [@NateWardawg](https://twitter.com/NateWardawg?ref_src=twsrc%5Etfw)   
> F1 2017 will indeed be Vulkan-only on Linux.
> 
> 
> — Feral Interactive (@feralgames) [October 30, 2017](https://twitter.com/feralgames/status/924963324235837445?ref_src=twsrc%5Etfw)


  







---


Así, de sorpresa, de sopetón, [Feral anuncia un secreto a voces](index.php/homepage/generos/carreras/item/560-f1-2017-podria-llegar-a-linux-steamos-si-hay-suficiente-demanda), y nos traerá  F1 2017 el jueves que viene si todo va según lo previsto. Como suele ser habitual la comunicación se ha realizado a través de las redes sociales y en su [página web](http://www.feralinteractive.com/es/news/820/) (y actualizando el [minisitio dedicado al juego](http://www.feralinteractive.com/es/games/f12017/)). Nos enterábamos del notición gracias a twitter:


 



> 
> On November 2nd, by popular demand, FORMULA ONE™ returns to Linux with F1™ 2017.  
> Swerve to our site to learn more: <https://t.co/rv7iQ75ADY> [pic.twitter.com/wJlBIpQopx](https://t.co/wJlBIpQopx)
> 
> 
> — Feral Interactive (@feralgames) [October 27, 2017](https://twitter.com/feralgames/status/923936331167711233?ref_src=twsrc%5Etfw)


  





 


Aclamado por la crítica por ser con claridad el mejor juego de Formula 1 hasta la fecha, los Linuxeros podremos volver a ponernos a los mandos de estos maravillosos monoplazas de nuevo. Tras el fallido (y poco comprendido) port de la versión de 2015, nos llega este nuevo título de Codemasters donde podremos de disfrutar de un modo carrera mucho más completo y la posibilidad de correr con coches clásicos de los últimos 30 años. Os dejamos con este sugerente video donde podreis sentir como se os hace la boca agua:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/BbBDcF7Zlwg" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


**ACTUALIZACIÓN:** Feral acaba de confirmar en twitter que el juego hará uso de la API Vulkan!!! Lo que no se aclara es si habrá posibilidad de usar OpenGL. Esperemos que el uso de esta tecnología incremente el rendimiento gráfico del juego. En pocos dias lo sabremos. Aquí teneis el tweet anunciando que F1 2017 usará Vulkan en Linux:


 



> 
> Elite racing and the latest technology are natural partners.  
> F1™ 2017 on Linux is fuelled by the [@VulkanAPI](https://twitter.com/VulkanAPI?ref_src=twsrc%5Etfw). [pic.twitter.com/o5vUBjewpX](https://t.co/o5vUBjewpX)
> 
> 
> — Feral Interactive (@feralgames) [October 27, 2017](https://twitter.com/feralgames/status/923977346561998848?ref_src=twsrc%5Etfw)


  





 


 Desde JugandoEnLinux os informaremos de todas las noticias que genere este juego, e intentaremos ofreceros material videográfico propio y un completo análisis si es posible. ¿Os atrae este título? ¿Os gusta la F1 y los juegos de coches en general? Dejanos tus opniones en los comentarios y en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

