---
author: leillo1975
category: "Simulaci\xF3n"
date: 2019-06-11 17:00:41
excerpt: <p>@SCSsoftware amplia su el mapa hacia el noroeste de los EEUU</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2735facec1d89ee0a8d2497f18c9f8af.webp
joomla_id: 1061
joomla_url: ha-llegado-washington-la-nueva-expansion-de-american-truck-simulator
layout: post
tags:
- dlc
- american-truck-simulator
- ats
- scs-software
- washington
- '135'
title: "Ha llegado \"Washington\", la nueva expansi\xF3n de American Truck Simulator."
---
@SCSsoftware amplia su el mapa hacia el noroeste de los EEUU

Hoy es día 11 de Junio, y tal y como [os adelantamos hace unos días](index.php/homepage/generos/simulacion/14-simulacion/1081-washington-sera-la-proxima-expansion-de-american-truck-simulator), es una fecha señalada para todos los fans del trabajo de [**SCS Software**](index.php/buscar?searchword=SCS&ordering=newest&searchphrase=all). Desde que se anunció a principios de año nadie podía esperar que "Washington" estuviese listo antes del verano, lo cual es una maravilla, pues nos permitirá disfrutarlo cuando muchos de nosotros tengamos más tiempo libre. El anuncio oficial nos acaba de llegar a través de twitter tal y como podeis ver aquí:



> 
> Washington for American Truck Simulator has been officially released! ???  
>   
> And with another surprise also released today, why not check out our blog for a new video trailer, pictures & more! ?? <https://t.co/Xc8bqruehD> [pic.twitter.com/0IoTcfUb49](https://t.co/0IoTcfUb49)
> 
> 
> — SCS Software (@SCSsoftware) [11 de junio de 2019](https://twitter.com/SCSsoftware/status/1138491583471349760?ref_src=twsrc%5Etfw)


Washington es el estado que está situado al norte de [Oregon](index.php/homepage/analisis/20-analisis/998-analisis-american-truck-simulator-dlc-oregon), la anterior expansión de ATS, y al sur de la frontera canadiense, **completando de esta forma la costa oeste de los Estados Unidos**. El "**Evergreen State**", tal y como reza su nombre, nos llevará a un territorio que cuenta con **enormes bosques de Coníferas** (de ahí lo de "Siempre Verde"). Sus **rios** y **lagos** hacen de él un lugar propicio para la instalación de **presas**. Sus **principales industrias** son la **maderera** (al igual que Oregon), la **aerospacial** (Boeing) y el **turismo**. Las ciudades más importates son **Olympia**, su capital, y **Seattle**, la más poblada, además de **Tacoma**, **Redmond** (¿os suena?) o **Vancouver**, ciudades que por supuesto se verán reflejadas en esta expansión.


Si estais al tanto de la actualidad que generan los juegos de SCS, sabreis que ayer mismo lanzaron la [versión 1.35 del juego base](https://blog.scssoft.com/2019/06/american-truck-simulator-update-135.html), tal y como hicieron la semana pasada con [Euro Truck Simulator 2](https://blog.scssoft.com/2019/06/euro-truck-simulator-update-135.html) ( [emitimos un video](https://youtu.be/Vj3TLvWenQY) hace unos dias repasando sus principales cambios ), siendo sus características técnicas calcadas, así como **nuevas carreteras en Oregon y Arizona**. Con esta nueva versión han allanado el camino para las principales novedades que veremos en "Washington", siendo estas las que ya os habíamos adelantado hace unos días:


*-3800 millas de densa red de carreteras*  
 *-16 nuevas ciudades (Vancouver, Longview, Aberdeen, Olympia, Tacoma, Seattle, Everett, Port Angeles, Bellingham, Omak, Wenatchee, Yakima, Kennewick, Grand Coulee, Spokane, Colville)*  
 *-Más de 20 puentes emblemáticos*  
 *-10 paradas de camiones originales*  
 *-Puerto de Keystone-Port Townsend Ferry*  
 *-Puente giratorio Spokane*  
 *-Presa de Grand Coulee*  
 *-Las emblemáticas montañas de Washington (Mount St. Helens y Mt. Rainier)*  
 *-Desafiante camino de acantilado*  
 *-Límites de velocidad dinámicos y señales LED*  
 *-Nuevas cargas únicas: motores de avión, camiones, barcos....*  
 *-Puertos grandes y realistas (Seattle, Tacoma, Everett)*  
 *-Muchas intersecciones únicas*  
 *-Diversos muelles de empresas: constructores de yates, grandes granjas, almacenes de materiales....*  
 *-Logros específicos de Washington para desbloquear*


En JugandoEnLinux estamos deseando echarle el guante y **mostraroslo en directo** a través de [nuestro canal en Twitch](https://www.twitch.tv/jugandoenlinux), por lo que permanced a tentos a nuestras redes sociales ([Twitter](https://twitter.com/JugandoenLinux) y [Mastodon](https://mastodon.social/@jugandoenlinux)) pues entre hoy o mañana r**ealizaremos un Streaming para que podais verlo de primera mano**. Por supuesto, como es costumbre, en unas semanas **tendreis un completo Análisis de esta DLC**. Os dejamos con el video oficial:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/rfMXLvxqgoI" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Podeis comprar "**Washington DLC**" para American Truck Simulator en la Humble Store (patrocinado) y en [Steam](https://store.steampowered.com/app/1015160/American_Truck_Simulator__Washington/). ¿Que os parece esta nueva adición al mapa de ATS? ¿Os gustan los juegos de SCS Software? Contesta en los comentarios o charla con nosotros sobre ello en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

