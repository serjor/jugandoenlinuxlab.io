---
author: leillo1975
category: "Acci\xF3n"
date: 2018-01-22 09:25:00
excerpt: "<p>El juego ha estrenado nueva versi\xF3n recientemente.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3de37b3f2de6564a2ea064ec2afe25d4.webp
joomla_id: 614
joomla_url: red-eclipse-un-shooter-libre-y-con-toques-de-parkour
layout: post
tags:
- open-source
- parkour
- red-eclipse
- cube-engine-2
title: Red Eclipse, un shooter libre y con toques de parkour.
---
El juego ha estrenado nueva versión recientemente.

Ayer mismo, un miembro de nuestra comunidad, **@franciscot (gracias!)**, nos apuntaba la existencia de este proyecto de **Software Libre**, del que siendo totalmente sincero desconocía su existencia. [Red Eclipse](http://www.redeclipse.net/) es un juego basado en el motor también libre [Cube 2](http://cubeengine.com/) (Sauerbraten) y en [SDL2](http://libsdl.org/), usando **OpenGL** como API gráfica. El juego es un **fork** y una evolución del descontinuado [Blood Frontier](https://sourceforge.net/projects/bloodfrontier/). El juego se ha actualizado recientemente a la **versión 1.6.0**, donde se corrigen multitud de problemas, se añaden mapas y se mejoran las mecánicas de balanceo de equipos, entre otras cosas.


Red Eclipse es un juego similar a Unreal Tournament, Quake 3 o el también libre [Xonotic](index.php/homepage/generos/accion/item/398-nueva-version-de-xonotic-con-importantes-mejoras) , donde prima la velocidad y los movimientos ágiles, y donde, como carácteristica notable, podremos hacer uso de **Parkour**, boosts y mutadores para recorrer la buena cantidad de mapas que tiene. Esto nos permitirá correr por las paredes, hacer dobles saltos o incluso dar patadas voladoras, lo que le da un **toque de originalidad** con respecto a los anteriormente mencionados. Dispone de una **buena cantidad de modos de juego**, donde podremos encontrar los típicos que encontraremos en el resto de títulos, así como varios originales, e incluso también la posibilidad de jugar offline contra bots. Es posible incluso crear niveles propios pues tiene modo de edición de mapas. Tiene un **buen número de armas**, entre las que destacan el lanzallamas, el lanzador de cohetes y la espada, entre otras.


El juego posee un **aspecto bastante cuidado y colorista**, y tiene un **rendimiento más que aceptable** en cualquier equipo que le pongamos delante, por lo que poder disfrutar de él no va a ser un inconveniente para nadie. Por lo que ya sabeis, apuntado queda para unos multis en las partidas que solemos jugar normalmente los [viernes por la noche](https://www.youtube.com/watch?v=xE-YIc9uIuc&list=PLmIplMAAvKnUXYb_QfVZS9wcjD-8pW6dd), y a las que te invitamos a participar a traves de nuestro grupo de [Telegram](https://t.me/jugandoenlinux). Podeis haceros con este fantástico juego **multiplataforma** de forma totalmente libre y gratuita en su [página de descargas](http://www.redeclipse.net/download), donde encontrareis un [comprimido](https://github.com/red-eclipse/base/releases/download/v1.6.0/redeclipse_1.6.0_nix.tar.bz2) y un [AppImage](https://github.com/red-eclipse/deploy/releases/download/appimage_continuous_stable/redeclipse-stable-x86_64.AppImage), a parte de los ejecutables para otros sistemas. Si quereis haceros una idea de como es el juego podeis ver un trailer justo aquí debajo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/oJRZHjyj7Zg" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


¿Os ha sorprendido Red Eclipse? ¿Que os parecen los toques de Parkour del juego? ¿Le dareis una oportunidad? Deja tus opiniones sobre este titulo en los comentarios, o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).


 


 

