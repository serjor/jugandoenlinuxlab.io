---
author: Pato
category: "Acci\xF3n"
date: 2019-08-02 14:50:44
excerpt: "<p>El juego de <span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo\
  \ r-qvutc0\">@GameAbandonShip</span> ya est\xE1 disponible para Linux/SteamOS en\
  \ acceso anticipado</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5104a1b2e1964d6decac501573adac6e.webp
joomla_id: 1092
joomla_url: abandon-ship-planea-la-llegara-a-linux-steamos-a-finales-del-mes-de-agosto-y-ya-tiene-disponible-una-beta-actualizado
layout: post
tags:
- accion
- indie
- estrategia
- tactico
title: Abandon Ship planea la llegada a Linux/SteamOS a finales del mes de Agosto
  y ya tiene disponible una beta -Actualizado-
---
El juego de @GameAbandonShip ya está disponible para Linux/SteamOS en acceso anticipado

(Actualización 30/08/2019)


Acaba de llegarnos la noticia de que **Abandon Ship** ya está disponible para nuestro sistema favorito y ya podemos comprarlo en su página de Steam.



> 
> You can now get Abandon Ship on Mac & Linux! ?<https://t.co/c2v1VAWz9p>
> 
> 
> — Abandon Ship (@GameAbandonShip) [August 30, 2019](https://twitter.com/GameAbandonShip/status/1167447296507895808?ref_src=twsrc%5Etfw)



Eso sí, tener en cuenta que el juego **sigue estando en fase de acceso anticipado** por lo que es posible que el juego tenga algunas cosas aún por pulir, pero según el desarrollador **Fireblade Studios** esperan poder ir puliendo el juego aún más con el apoyo de la comunidad linuxera:


*Como somos un equipo Indie muy pequeño, no tenemos acceso a una gran selección de hardware / software para probar, por lo que realmente apreciamos toda la ayuda de la comunidad para informar los resultados de sus pruebas.*


*Los jugadores de Linux pueden estar interesados en saber que si bien hemos centrado nuestras pruebas en Unbuntu 18.04, hemos recibido informes de personas que han ejecutado el juego perfectamente en los siguientes sistemas operativos:*


*Linux Mint 19.1 Cinnamon*  
 *Steam nativo de Linux Mint*  
 *Escritorio Manjaro Linux KDE, instalación de Steam Flatpak*


El estudio anima a que si se encuentra cualquier fallo o error se puede contactar con ellos vía e-mail o en Discord que tenéis [disponible en el anuncio oficial](https://steamcommunity.com/games/551860/announcements/detail/1593633208467991962).


Por otra parte, y como ya hemos dicho, el juego sigue en acceso anticipado pero el estudio también se encuentra trabajando en lo que se convertirá en la versión de lanzamiento completo que se lanzará este otoño: *Tenemos algunos planes emocionantes para el desbloqueo final de la región, el resto de la historia principal, un modo de juego libre, una renovación de la primera región para alinearla con las otras regiones, y mucho más.*


Si estás interesado, puedes saber más en su [página web oficial](http://abandonshipgame.com/) o en su [página de Steam](https://store.steampowered.com/app/551860/Abandon_Ship/).


 




---


(Noticia original)


Nos hacemos eco de la noticia de [gamingonlinux.com](https://www.gamingonlinux.com/articles/super-stylish-naval-combat-and-adventure-game-abandon-ship-now-has-a-linux-beta.14709) sobre la llegada del juego **Abandon Ship** a Linux/SteamOS. Se trata de un juego de batallas estratégicas entre barcos de vela al estilo de siglos pasados, donde los barcos de madera y los cañones surcaban los mares y dominaban las luchas navales.


"*Asume el mando de un barco clásico de la «Era de la Vela» y su tripulación, y explora un amplio mundo lleno de historias donde tus decisiones tienen consecuencias. Lucha contra barcos, fortalezas y monstruos marinos en combates tácticos con un estilo artístico inspirado por la pintura clásica naval"*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/tWym0Sz_YSY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Según nos cuenta Liam, el estudio le ha confirmado que si todo va bien lo tendremos disponible en Linux/SteamOS **para finales de este mes de Agosto**, pero que **ya tienen disponible una beta** del juego en Steam para poder probarla y testearlo.


Para poder acceder a la beta, lo primero que tenemos que hacer tener el juego en Steam, y luego acceder a la pestaña "betas" desplegando el menú de propiedades del juego haciendo clic derecho sobre el título, y allí introducir la clave "lt27Utrx5k618vS". Una vez hecho eso tendremos acceso a la rama beta de Linux del juego y lo podremos descargar.


Si quieres saber mas sobre Abandon Ship puedes visitar [su web oficial](http://abandonshipgame.com/), o si te interesa comprarlo y probar desde ya la beta del juego lo tienes disponible en su [página de Steam](https://store.steampowered.com/app/551860/Abandon_Ship/) o en [Humble Bundle](https://www.humblebundle.com/store/abandon-ship?partner=jugandoenlinux) (enlace patrocinado).

