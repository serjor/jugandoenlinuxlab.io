---
author: Pato
category: Software
date: 2019-05-30 10:43:25
excerpt: <p>El proyecto que traduce D3D9 a Vulkan sigue con su desarrollo</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6b88c34cd64b83eac2709d6c8c29fd54.webp
joomla_id: 1053
joomla_url: d9vk-lanza-su-version-0-12-en-una-gran-actualizacion-y-soluciona-multiples-bugs
layout: post
tags:
- wine
- dxvk
- d9vk
title: "D9VK lanza su versi\xF3n 0.12 en una gran actualizaci\xF3n y soluciona m\xFA\
  ltiples bugs"
---
El proyecto que traduce D3D9 a Vulkan sigue con su desarrollo

A estas alturas todos sabemos lo que proyectos como [DXVK](index.php/homepage/noticias/tag/DXVK) y Wine han cambiado el panorama del videojuego en Linux, haciendo posible ejecutar juegos no nativos gracias a la capa de traducción de DirectX 10/11 a Vulkan. Faltaba el tercero en discordia, y hace ya un tiempo que un programador llamado Joshua Ashton se lanzó a desarrollar **D9VK** para "traducir" al vuelo DirectX 9 a Vulkan, en la línea de lo que es DXVK para DX10 u 11.


El desarrollo de D9VK, por el momento en paralelo a DXVK está siendo vertiginoso y aunque aún hay que "meterlo a mano" tanto para Wine como para Proton lo cierto es que el avance y las mejoras que se obtienen con esta capa de traducción son muy significativas.


Ahora D9VK [lanza su versión 0.12](https://github.com/Joshua-Ashton/d9vk/releases/tag/0.12) llamada Cool Lookin' Blue Frog con múltiples mejoras y solución de errores, así como una reescritura en el código de texturas lo que debería mejorar notablemente el rendimiento y solucionar múltiples problemas con los juegos que se ejecutan bajo DX9.


Si quieres saber más sobre el proyecto o saber más a fondo qué trae esta nueva actualización, tienes toda la información de D9VK en su [página de Github](https://github.com/Joshua-Ashton/d9vk).


¿Qué te parece el proyecto D9VK? ¿Piensas que terminará fusionándose con DXVK?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux).

