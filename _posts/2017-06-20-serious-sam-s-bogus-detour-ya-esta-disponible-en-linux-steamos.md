---
author: Pato
category: "Acci\xF3n"
date: 2017-06-20 15:17:20
excerpt: "<p>La propuesta de acci\xF3n en vista cenital de Croteam llega con Sam m\xE1\
  s desbocado que nunca</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6ef598b10d1539793d4ace8d8b7b613f.webp
joomla_id: 381
joomla_url: serious-sam-s-bogus-detour-ya-esta-disponible-en-linux-steamos
layout: post
tags:
- accion
- indie
- aventura
- gore
- violento
title: "Serious Sam's Bogus Detour ya est\xE1 disponible en Linux/SteamOS"
---
La propuesta de acción en vista cenital de Croteam llega con Sam más desbocado que nunca

Hace tan solo unos minutos que ha llegado la noticia vía twitter de que Serious Sam's Bogus Detour [[web oficial](http://www.crackshell.dk/)] ya está disponible en Linux/SteamOS:



> 
> Serious Sam’s Bogus Detour is live on Steam now. Check the game here <https://t.co/SHBQsw7Lqw>
> 
> 
> — Croteam (@Croteam) [June 20, 2017](https://twitter.com/Croteam/status/877169025679327232)



Serious Sam's Bogus Detour ha sido desarrollado por "Crackshell", estudio responsable del excelente 'Hammerwatch y que en este caso se han aliado con Croteam para traernos este "Spin-off" de la saga Serious Sam ambientado en el mediterráneo, y donde tendremos que recorrer escenarios como plantas de armamento biológico, estaciones lunares y otros exóticos lugares destrozando cuanto se nos ponga por delante:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/8QyNusPlhBo" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


 Entre las características principales destacan:


*Modo Campaña: solo o con hasta 4 jugadores.*


*Modo Versus: Deathmatch con hasta 12 jugadores.*


*Modo supervivencia: Solo o con hasta 12 jugadores.*


*Soporte para Mods.*


Si estás deseando ayudar al bueno de Sam a acabar con hordas de enemigos, puedes conseguirlo traducido al español en su página de Steam con un 10% de descuento por su lanzamiento:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/272620/" width="646"></iframe></div>


¿Te gustan los juegos de acción en vista cenital? ¿Piensas ayudar al "serio" de Sam?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

