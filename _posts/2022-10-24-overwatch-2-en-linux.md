---
author: P_Vader
category: "V\xEDdeos JEL"
date: 2022-10-24 10:50:23
excerpt: "<p>Hemos probado el nuevo Overwatch 2 desde Battle.net</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/overwatch2/overwatch2.webp
joomla_id: 1499
joomla_url: overwatch-2-en-linux
layout: post
tags:
- proton
- overwatch-2
- bottles
- battle-net
title: Overwatch 2 en Linux
---
Hemos probado el nuevo Overwatch 2 desde Battle.net


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/R6zBNZ8Yvlw" title="YouTube video player" width="720"></iframe></div>


 


**Instrucciones con Bottles:**


1. Instala [Bottles](https://flathub.org/apps/details/com.usebottles.bottles) con [flatpak](https://flatpak.org/setup/).
2. Una vez iniciado Bottles, lo primero que haremos es ir a sus preferencias, en el menú hamburguesa de la barra superior, dentro de la pestaña "Ejecutores" desplegar el llamado "Caffe" e instala la última versión. Nosotros hemos probado con la 7.18.
3. Ahora vamos a añadir una nueva botella, de tipo juegos, con el nombre "Overwatch 2" por ejemplo.
4. Dentro de la botella, en `Preferencias > Sistema > Runner Components > Ejecutor` y vamos a cambiarlo por el Caffe que instalamos antes.
5. Por último en Instaladores descargamos e instalamos "Blizzard Battle.net!"
6. Una vez instalado podremos introducir los datos de nuestra cuenta e instalar Overwatch 2 desde el lanzador del juego.


Nota: No se recomienda activar Gamemode, nos ha fallado durante el juego.


Nota2: En ocasiones el juego pierde el foco del ratón y no puedes mover el personaje. La solución fue cambiar de ventana y al volver al juego ya funciona.


 


**Si tienes alguna duda, consulta nuestro canal de Matrix para juegos Wine/Proton:** 


**[https://matrix.to/#/#wine_proton-jugandoenlinux:matrix.org](https://matrix.to/#/#wine_proton-jugandoenlinux)**


   
  


