---
author: leillo1975
category: "Simulaci\xF3n"
date: 2021-03-08 08:34:26
excerpt: "<p>Los chicos de @SCSSoftware nos adelantan cual ser\xE1 su pr\xF3xima expansi\xF3\
  n.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/HeartOfRussia/ETS2_Heart_of_Russia.webp
joomla_id: 1285
joomla_url: heart-of-russia-sera-la-nueva-dlc-de-euro-truck-simulator-2
layout: post
tags:
- ets2
- dlc
- scs-software
- euro-truck-simulator-2
- simulacion
- heart-of-russia
title: "Heart of Russia ser\xE1 la nueva DLC de Euro Truck Simulator 2"
---
Los chicos de @SCSSoftware nos adelantan cual será su próxima expansión.


Cuando cada vez queda menos para que podamos circular por las carreteras de nuestra querida península ibérica (previsiblemente [el próximo mes de abril](https://blog.scssoft.com/2021/03/iberia-language-traffic-signs.html)), nos llega la noticia de que su próxima DLC de expansión para ETS2 dará un giro de 180º en cuanto a la ubicación, volviendo de nuevo la vista hacia Europa Oriental, concretamente hacia la parte al este de "[Beyond the Baltic Sea]({{ "/posts/analisis-euro-truck-simulator-2-beyond-the-baltic-sea-dlc" | absolute_url }})". El anuncio lo podíamos ver en twitter:  
  

> 
> Привет, дальнобойщики!👋  
>   
> Over the past few months, we have started teasing the next map expansion for [#ETS2](https://twitter.com/hashtag/ETS2?src=hash&ref_src=twsrc%5Etfw), which will come after Iberia 🤔  
>   
> Today we are happy to reveal our next destination a little bit more! 🚛  
>   
> Please read carefully and see more at ⬇️<https://t.co/qYOmDBelIk> [pic.twitter.com/bSLQ3seWiZ](https://t.co/bSLQ3seWiZ)
> 
> 
> — SCS Software (@SCSsoftware) [March 5, 2021](https://twitter.com/SCSsoftware/status/1367868370670981128?ref_src=twsrc%5Etfw)


El area a exporar será enorme y abarcará hasta la parte superior del rio Volga, incluyendo por supuesto su capital, Moscú, y bajando hacia los rios Oka y Don. Desde [SCSsoft](https://scssoft.com/), nos aseguran que el esfuerzo que están realizando para documentarse y crear este área está siendo enorme , llegando incluso a contratar a personal ruso para no incurrir en errores. Por ahora es posible ver algunas instantaneas en el [artículo dedicado a esta expansión en su blog](https://blog.scssoft.com/2021/03/heart-of-russia.html), como esta que podeis ver justo aquí debajo:


![](https://1.bp.blogspot.com/-oovwUzsfM_g/YEIs_L7XlAI/AAAAAAAAYMc/D3TBx4jicBsIkyMeecqv0jtnnPbuVazlACLcBGAsYHQ/s1920/07c.webp)


No existen fechas estimadas de lanzamiento, pues el retraso producido en [Iberia]({{ "/posts/la-proxima-expansion-de-euro-truck-simulator-2-sera-iberia" | absolute_url }}), ha modificado bastante sus planes, por lo que no prefieren no aventurarse en dar plazos, aunque ya es posible agregar la expansión a nuestra lista de deseados y de seguimiento en la página que acaban de abrir en [Steam](https://store.steampowered.com/app/1536500/Euro_Truck_Simulator_2__Heart_of_Russia/).


Por supuesto en JugandoEnLinux.com permaneceremos atentos a todas las noticias relevantes relaccionadas con esta DLC. Puedes dejarnos tus preguntas o impresiones en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

