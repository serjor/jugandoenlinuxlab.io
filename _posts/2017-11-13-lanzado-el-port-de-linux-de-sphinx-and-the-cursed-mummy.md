---
author: leillo1975
category: "Acci\xF3n"
date: 2017-11-13 09:05:45
excerpt: <p>THQ Nordic rescata este juego editado por el 2003 en consolas.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/bc53d507c26770e8f294fcbf92ef0864.webp
joomla_id: 531
joomla_url: lanzado-el-port-de-linux-de-sphinx-and-the-cursed-mummy
layout: post
tags:
- sphinx-and-the-cursed-mummy
- thq-nordic
- ps2
- gamecube
- xbox
title: Lanzado el port de Linux de Sphinx and the Cursed Mummy (ACTUALIZADO)
---
THQ Nordic rescata este juego editado por el 2003 en consolas.

**ACTUALIZACIÓN 21-2-18:** Acabamos de ver en [GamingOnLinux](https://www.gamingonlinux.com/articles/sphinx-and-the-cursed-mummy-is-out-with-linux-support-mod-tools-and-constant-patched-improvements.11275) que el juego ha recibido multitud de parches y correcciones en los últimos meses, por lo que ha mejorado sustancialmente. A parte de esto, y lo más importante, hace un par de días acaban de publicar las [herramientas de creación de forma gratuita](http://store.steampowered.com/app/750430/Sphinx_and_the_Cursed_Mummy_Authoring_Tools/), por lo que podremos crear nuestros propios mods y niveles y personajes si así lo queremos. Se trata de la misma herramienta que usaron sus creadores originales para desarrollar el juego (EngineX).  


Volviendo a las [actualizaciones](http://steamcommunity.com/games/606710/announcements/detail/1594705210010547904) ahora el juego tiene mejor soporte de gamepads, traducciones corregidas y luces dinámicas, entre otras muchas cosas. La verdad es que el soporte de los desarrolladores con respecto a la reedición de este juego no puede ser mejor. Ojalá que este tipo de actitud les sea recompensada por una buena respuesta por parte de los jugadores y se repitiese más a menudo. Si quereis comprar este juego teneis los enlaces al final de este artículo.




---


**NOTICIA ORIGINAL:** Nos llega la noticia a través de [linuxgameconsortium.com](https://linuxgameconsortium.com/linux-gaming-news/sphinx-and-the-cursed-mummy-releases-linux-games-ubuntu-59780/) de que THQ Nordic acaba de reeditar la adaptación para PC's de un juego con ya unos cuantos añitos, concretamente 14. El juego, desarrollado por Eurocom, **tuvo versiones en Playstation 2, GameCube y la primera XBOX**, y se trata de un juego de acción en tercera persona ambientado en el antiguo Egipto.


 Recientemente THQ Nordic ha decidido rescatar el juego y editarlo en GNU-Linux, Mac y Windows, y a su vez dotarlo de diferentes **mejoras** entre las que destacan:


- formato panorámico, las resoluciones HD y 4K, antialiasing y filtros anisotrópicos.


-Gráficos 3D reescritos en OpenGL 3+


-Dotación de sonido envolvente gracias a OpenAL, pudiendo usarse además del Stereo, sistemas 5.1, 6.1 y 7.1


-Soporte de controladores XBOX con force feedback.


-Texturas HD restauradas


 Esta es la descripción del juego:



> 
> *Sphinx y la Maldita Momia es un original juego de acción y aventuras en tercera persona inspirado en la mitología del antiguo Egipto. Viaja por el mundo a través de portales mágicos para desbaratar los malvados planes de Set. Sphinx necesitará todo su ingenio, agilidad y poderes especiales en su viaje. Además, contará con la ayuda de su reticente amiga, la Momia, cuando la única solución sea pasar desapercibido. Sphinx puede detener a Set y salvar el mundo si encuentra las coronas mágicas de Egipto robadas.*
> 
> 
> *Características:*
> ------------------
> 
> 
> * *Usa a Sphinx y su creciente variedad de habilidades para enfrentarte a letales monstruos míticos mientras evitas las traicioneras trampas con movimientos acrobáticos. Debilita a los enemigos y usa los escarabajos capturados para almacenar y lanzar habilidades a tu antojo.*
> * *Toma el control de la Momia cuando la fuerza y las habilidades de combate no sean necesarias. Usa su sigilo y sus trucos poco convencionales para esquivar a los enemigos, resolver puzles y realizar sabotajes delante de sus narices.*
> * *Interactúa con cientos de criaturas, enemigos y aliados únicos y repletos de fantasía.*
> * *Con gráficos fantásticos que hacen que este mundo mítico basado en las leyendas de Egipto cobre vida.*
> * *Con una historia totalmente inmersiva y una mecánica de juego épica.*
> 
> 
> *La historia:*
> --------------
> 
> 
> *Hace tiempo, un antiguo imperio egipcio universal unió varios mundos a través de portales de Discos solares. El viejo imperio acabó destruido y las civilizaciones de cada mundo sufrieron diferentes destinos, algunos mantuvieron el contacto y otros lo perdieron. Estos mundos han estado separados durante tanto tiempo que la mayoría ya no se reconocen.*  
> *La leyenda cuenta que dos fuerzas, una negativa y otra positiva, intentan recuperar el viejo imperio y todas sus coronas. Pero conocemos muy poco acerca de estas fuerzas o si existen en realidad. Otros solo intentan comprender qué pasó y la naturaleza real de cada mundo. Mientras tanto, cada mundo continúa su propio camino y su propia historia.*
> 
> 
> *Últimamente han ocurrido cosas extrañas. Han llegado noticias de problemas en muchos lugares; las aldeas que hasta ahora habían sido pacíficas se encuentran en guerra y han aparecido extraños monstruos en los valles donde una vez reinó un ambiente tranquilo. Nadie sabe a qué se deben.*
> 
> 
> *La cadena de comunicación entre los mundos permanece en parte abandonada, en parte destruida y tan inestable que usarla sería una manera segura de causar todavía más problemas y peligros. Hace mucho tiempo, se construyó la Gran muralla de Heliópolis para proteger a los ciudadanos. Mucha gente ha usado los portales y ha desaparecido sin dejar rastro. Por este motivo, nunca se sabe cómo te recibirán en cualquiera de los lugares o si realmente llegarás a alguna parte.*
> 
> 
>  
> 
> 
> 


El juego tiene unos **requisitos técnicos muy bajos** por lo que funcionará sin problemas en cualquier máquina con cualquier gráfica:


* **SO:** Ubuntu 12.04 LTS, Steam OS, Arch Linux, Linux Kernel 2.6+ (i686 arch) / glibc 2.1.3+
* **Procesador:** 1.5 GHz supporting SSE2 instructions
* **Memoria:** 1 GB de RAM
* **Gráficos:** OpenGL 3.0 Core Profile compatible
* **Almacenamiento:** 1 GB de espacio disponible


Podeis ver un gameplay del juego aquí:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/NsVZ_xi6GC4" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


  Si quereis comprar Sphinx and the Cursed Mummy, deciros que **es bastante barato**, además tiene oferta de lanzamiento y podeis conseguirlo por menos de 10€. Está disponible en [GOG.com](https://www.gog.com/game/sphinx_and_the_cursed_mummy) o en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/606710/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Qué te parece que se reediten juegos antiguos de consolas para nuestro sistema? ¿Te parece atractivo el juego? Dejanos tus impresiones en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).


 

