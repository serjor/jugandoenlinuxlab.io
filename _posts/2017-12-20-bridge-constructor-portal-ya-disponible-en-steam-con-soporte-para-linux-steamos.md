---
author: yodefuensa
category: Estrategia
date: 2017-12-20 13:38:04
excerpt: "<p>Se trata de un juego con tem\xE1tica en el universo del conocido juego\
  \ de Valve, Portal</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1c02df27d352caf7b0780fd631512bf4.webp
joomla_id: 581
joomla_url: bridge-constructor-portal-ya-disponible-en-steam-con-soporte-para-linux-steamos
layout: post
tags:
- estrategia
- simulacion
title: Bridge Constructor Portal ya disponible en Steam con soporte para Linux/SteamOS
---
Se trata de un juego con temática en el universo del conocido juego de Valve, Portal

Si hace unas semanas nos sorprendíamos con un nuevo juego con la temática Portal, hoy ya lo tenemos disponible en Steam desde 9,99€.  
   
 La desarrolladora [ClockStone](http://www.clockstone.com/WebCMS/sites/DE/index.html) ha sido la encargada de realizar el juego, desarrolladora conocida por la saga Bridge Constructor, al cual sumamos este título con la licencia de portal.


El objetivo de este nuevo título será el de construir puentes, rampas, toboganes y otras construcciones en 60 cámaras de prueba para que los Dobladores lleguen a salvo a la línea de meta. Por supuesto contando con todos los elemento ya vistos en la saga portal, Glados será la encargada de ponernos trampas para que esto no sea tan sencillo.


El lanzamiento ha venido acompañado con una ración de gameplays para que veamos la mecánicas del juego.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/TIX7-Gkw0D0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Por último es de agradecer que desde el día de lanzamiento se encuentre en Linux y con un requisitos tan accesibles.


**Mínimo:**


+ **SO:** Ubuntu 12.04+
+ **Procesador:** 2 GHz
+ **Memoria:** 2 GB de RAM
+ **Gráficos:** DirectX10 compatible
+ **Almacenamiento:** 200 MB de espacio disponible


Puedes encontrar 'Bridge Constructor Portal' en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/684410/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 Este artículo ha sido redactado por un invitado (Yodefuensa). Si quieres puedes enviarnos tus propios artículos para valorarlos. Puedes hacerlo [en este enlace](index.php/contacto/envianos-tu-articulo).

