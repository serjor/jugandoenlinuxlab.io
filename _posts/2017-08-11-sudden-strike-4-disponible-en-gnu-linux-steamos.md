---
author: leillo1975
category: Estrategia
date: 2017-08-11 17:00:00
excerpt: <p>Kite Games nos presenta una nueva entrega de esta conocida IP</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/26c195007944b894aae7335fae091351.webp
joomla_id: 428
joomla_url: sudden-strike-4-disponible-en-gnu-linux-steamos
layout: post
tags:
- estrategia
- sudden-strike-4
- kalypso-media
- kite-games
title: Sudden Strike 4 disponible en GNU-Linux/SteamOS
---
Kite Games nos presenta una nueva entrega de esta conocida IP

Tras meses de desarrollo desde la [última reseña que publicamos en nuestra web](index.php/homepage/generos/estrategia/item/428-sudden-strike-4-llegara-pronto-a-nuestros-pc-s), llega finalmente a nuestros PC's la cuarta parte de esta conocida saga de videojuegos de **Estrategia en Tiempo Real** ambientada en la **Segunda Guerra Mundial.** Cumpliendo con lo anunciado anteriormente, la editora [**Kalypso Media**](http://www.kalypsomedia.com/en/games/suddenstrike4/index.shtml) nos trae en el día de hoy este título, que viene de la mano de el estudio húngaro [**Kite Games**](http://kite-games.com/).


 


Como anteriormente comentamos, **Sudden Strike es una saga de videojuegos** que se remonta al año 1996, y que en su día fue desarrollada por la difunta compañia [Mindscape](https://es.wikipedia.org/wiki/Mindscape) con un éxito bastante notable.Con una trayectoria un tanto irregular y tras pasar la serie por diferentes estudios, finalmente nos ha llegado esta cuarta parte, que afortunadamente podemos disfrutar en Linux/SteamOS.


 


Según la descripción del juego que aparece en la tienda de Steam, en Sudden Strike 4 podreis encontrar:



> 
> """-En **Sudden Strike 4**, te embarcarás en **tres grandes campañas** ambientadas en los campos de batalla de la **Segunda Guerra Mundial**.
> 
> 
> -Tras asumir el mando de las **tropas aliadas, alemanas o soviéticas**, podrás enviar a la batalla a más de **100 unidades diferentes**; como el bombardero alemán **Heinkel He111**, el tanque ruso **T-34**, el caza británico **Hawker Typhoon** y el infame **Panzerkampfwagen VI "Tiger"** alemán.
> 
> 
> -Por primera vez en la saga **Sudden Strike**, podrás elegir a uno de los **nueve comandantes disponibles**, como **George Patton** o **Bernard Montgomery**. Cada uno de ellos ofrecerá distintos enfoques de combate y **habilidades únicas**.
> 
> 
> -Haz gala de tus dotes de estratega en **más de 20 exigentes escenarios para un jugador**, en el **modo escaramuza**, basado en desafíos, y en el ultracompetitivo **modo multijugador**.
> 
> 
> -Aprovecha los **puntos débiles** de los tanques, tiende **emboscadas**, **ocupa** edificios con la infantería, **supera** al enemigo con un posicionamiento inteligente o lanza devastadores **ataques aéreos**. ¡Tú eliges cómo quieres abordar cada misión!
> 
> 
> -Compatible con **mods** gracias a la integración en **Steam Workshop**."""
> 
> 
> 


Los requisitos técnicos del juego son moderados y son los siguientes:


**Mínimo:**  

+ **SO:** SteamOS, Ubuntu 15.10
+ **Memoria:** 6 GB de RAM
+ **Gráficos:** AMD Radeon HD 7850, NVIDIA GeForce 660 series
+ **Almacenamiento:** 12 GB de espacio disponible



**Recomendado:**  

+ **SO:** SteamOS, Ubuntu 15.10
+ **Procesador:** AMD Quad-Core @ 3.8 GHz or Intel Quad-Core @ 3.2 GHz
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** AMD Radeon RX 470 or NVIDIA GeForce 1050 Ti series and above
+ **Red:** Conexión de banda ancha a Internet
+ **Almacenamiento:** 12 GB de espacio disponible



 


El juego, según comentan en otras webs, va asombrosamente bien en nuestro sistema y dispone de una calidad gráfica notable. Desde JugandoEnLinux.com nos encantaría poder dar nuestras propias opiniones y  ofreceros un completo análisis de este título, así como algunos videos sobre este juego, pero tras intentar ponernos en contacto con el estudio y la editora aún no hemos obtenido respuesta, por lo que sentimos no poder llevar a cabo dichos contenidos. Os dejamos con el trailer del juego:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/jy6O1GAnkV0?list=PLlQXYwoUl2OeOMsJTvGToQyhhvcILTRGm" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Si quereis comprar Sudden Strike 4 podeis hacerlo en la tienda digital Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/373930/" width="646"></iframe></div>


 


 ¿Qué os parece la Saga Sudden Strike? ¿Habeis jugado antes a esta serie de juegos? ¿Os comprareis esta cuarta parte? Deja tus impresiones en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

