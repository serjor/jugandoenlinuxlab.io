---
author: Pato
category: "Acci\xF3n"
date: 2021-02-17 16:15:31
excerpt: "<p>El juego de @TheGameKitchen recibe a Miriam, de Bloodstained: Ritual\
  \ of the Night</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Blasphemous/BlasphemousStrifeNRuin.webp
joomla_id: 1278
joomla_url: blasphemous-recibira-un-dlc-gratuito-llamado-strife-ruin-con-un-crossover-especial
layout: post
tags:
- accion
- plataformas
- gotico
- blasphemous
title: "Blasphemous recibir\xE1 un DLC gratuito llamado Strife & Ruin con un crossover\
  \ especial"
---
El juego de @TheGameKitchen recibe a Miriam, de Bloodstained: Ritual of the Night


Buenas noticias para los que gustan de los buenos plataformas de acción, ya que dos de las franquicias más importantes de Team17 formarán equipo.


Hablamos del anuncio del nuevo **DLC** que The Game Kitchen lanzará próximamente **de forma gratuita** para el excelente **Blasphemous** y en el que recibirá como personaje invitado nada mas y nada menos que a Miriam, de **Bloodstained: Ritual of the Night**.


El trailer del anuncio no tiene desperdicio:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/d6t3QN2BfFw" width="560"></iframe></div>


Según comentan en el [anuncio oficial](https://store.steampowered.com/news/app/774361/view/5262989303550269543), el nuevo DLC nos traerá:


* Una nueva aliada que formará equipo con el Penitente.
* Crossover Bloodstained: Ritual of the Night
* Modo Boss Rush
* Nuevos modos de renderizado
* Nuevo Rosario
* Soporte de idioma coreano
* Corrección de errores


 Tienes **Blasphemous** nativo para Linux, en [Humble Bundle](https://www.humblebundle.com/store/blasphemous?partner=jugandoenlinux) (enlace de patrocinador) y en su [página de Steam](https://store.steampowered.com/app/774361/Blasphemous/).


 

