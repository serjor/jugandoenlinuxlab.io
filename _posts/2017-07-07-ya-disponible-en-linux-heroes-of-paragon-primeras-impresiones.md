---
author: Pato
category: Estrategia
date: 2017-07-07 12:31:00
excerpt: <p>Se trata de un juego "free to play" del estilo de Clash of Clans</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/21b9ad30781a3312d12f4eb3aa07aa4c.webp
joomla_id: 400
joomla_url: ya-disponible-en-linux-heroes-of-paragon-primeras-impresiones
layout: post
tags:
- indie
- casual
- free-to-play
- estrategia
title: Ya disponible en Linux/SteamOS 'Heroes of Paragon' - Primeras impresiones
---
Se trata de un juego "free to play" del estilo de Clash of Clans

No es muy habitual encontrar juegos de estilo casual y "free to play" de gestión y defensa de torres en Linux como el que nos ocupa. Por esto me llamó la atención el lanzamiento de este Heroes of Paragon [[web oficial](https://heroesofparagon.com/)] para Linux/SteamOS ya que básicamente no he podido encontrar una referencia parecida, y es lo que le da mérito. ¿Un juego de este tipo en Linux? vaya...


Estamos ante un 'clon' de "Clash of Clans", el afamado juego de Supercell que se ha hecho referente en el género de juegos de gestión y defensa de torres en los móviles y tablets de todo el mundo. De hecho, el juego está disponible en las distintas App Stores de los dispositivos móviles tal y como puede verse en su web. Incluso se"vincula" a nuestro Pc de modo que si queremos seguir con la partida en otro dispositivo podemos "exportar" nuestra vinculación y seguir el juego en la app del teléfono o tablet que queramos.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/dhP5tR1KIss" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


El juego en si es sencillo. Se trata de construir y gestionar tu poblado o reino donde tendrás que recolectar oro, elixir, runas y otros objetos para mejorar edificios, personajes y armas para hacer frente a los combates que habrá que afrontar para conquistar el reino que ha sido invadido por los enemigos. También lucharás contra los reinos de otros jugadores en un modo "jugador contra jugador", podrás unirte a compañeros de juego mediante un sistema de gremios, y mucho mas.


El sistema de combate es muy sencillo. Se lleva a cabo en un sistema de "base contra base" donde atacas o defiendes según te vaya, y donde deberás gestionar diréctamente a tus tropas para tener éxito. Previamente deberás haber organizado bien tus defensas pues mientras estás atacando también puedes recibir ataques que deberás repeler si no quieres sucumbir.


![Heroesofparagon2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Heroesofparagon/Heroesofparagon2.webp)


Dentro de lo que es la jugabilidad, el juego progresa a un ritmo muy rápido durante las primeras fases donde te enseña las mecánicas mientras juegas. Durante los primeros niveles recibes consejos y apoyo para que te vayas haciendo al juego, y conforme vas subiendo de nivel el progreso se hace más lento y necesitas más tiempo para realizar acciones que antes hacías enseguida. Aquí es donde entra la monetización del juego, ya que si quieres progresar más rápidamente o conseguir nuevos objetos más rápido puedes comprar gemas con dinero real que te permitirán acelerar el proceso. De otra forma te tocará esperar durante el tiempo necesario para poder avanzar y conseguir las mejoras que desbloqueen el avance del juego.


En cuanto a la historia, es bastante simplona y solo sirve de pretexto para progresar en el juego. El sonido no destaca, pero cumple. Se hace un poco repetitivo pero viniendo de un desarrollo para móviles es lo típico.


Los gráficos son más de lo mismo. Se nota que está pensado para dispositivos móviles y son de estilo "cartoon", con opciones gráficas mínimas como la resolución para pantalla completa o en modo ventana, por que como dato curioso en el menú del juego no hay opción de "salir", si no que hay que poner el juego en modo ventana y cerrarlo desde ahí. Por cierto, el juego está traducido al español de forma bastante correcta.


Por lo demás estamos ante un juego casual, correcto y sin complicaciones que se hace un tanto repetitivo una vez alcanzas el nivel 7 u 8 que es cuando empiezas a echar en falta las gemas para progresar mas deprisa, pero puede resultar divertido para poner el cerebro en encefalograma plano y pasar un buen rato haciendo cosas.


Los requisitos requeridos no son gran cosa, pudiéndose ejecutar en pcs poco potentes como laptops o netbooks. yo lo he probado sin problemas en un portatil con un Core i3 2350M, 4GB de RAM y gráficos intel HD 3000 con drivers Mesa.


Si quieres echar un vistazo o probar unas partidas, lo tienes disponible en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/614100/" width="646"></iframe></div>


 

