---
author: leillo1975
category: Software
date: 2019-05-24 07:19:14
excerpt: "<p>Steam Play incluye ahora una nueva versi\xF3n de DXVK</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4eadae682909e5571fe2c1a4fc6acd34.webp
joomla_id: 1048
joomla_url: proton-se-actualiza-a-la-version-4-2-5
layout: post
tags:
- valve
- steam-play
- dxvk
- proton
- quake-champions
title: "Proton se actualiza a la versi\xF3n 4.2-5."
---
Steam Play incluye ahora una nueva versión de DXVK

Hace solo una semana que se ha actualizado Steam Play, pero en Valve no se han estado quietos y parece que han leido nuestra mente. El caso es que cuando salió la anterior versión todos pensamos para nuestros adentros que sería mejor que incluyese la **versión 1.2 de DXVK** en vez de la 1.1.1, pues la primera habilita mejoras sustanciales en algunos títulos como **Quake Champions**, pero como sabeis se buscaba darle soporte "Dia-1" a **RAGE 2**, y suponemos que por ahí la premura de la versión anterior.


Sin más preámbulos vamos a enumeraros las tres [novedades](https://github.com/ValveSoftware/Proton/wiki/Changelog#42-5) de esta version:


*-Actualizado a [DXVK 1.2.1](https://github.com/doitsujin/dxvk/releases/tag/v1.2.1), lo que [amentaría el rendimiento](https://github.com/doitsujin/dxvk/releases/tag/v1.2) de la GPU y reduciría el consumo de GPU en algunos títulos, además de aprovechar nuevas extensiones de Vulkan.*  
 *-Soporte para las nuevas APIs de red de Steam. Debería arreglar algunos títulos más nuevos, incluyendo A Hat in Time.*  
 *-Muchos arreglos en el diseño del controlador. Debería arreglar el diseño del controlador en muchos títulos basados en Unity, incluyendo Subnautica, y algunos títulos de Ubisoft.*


... y ahora a probarlo. Esperamos vuestras pruebas e impresiones de esta nueva versión en los comentarios de esta noticia, o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

