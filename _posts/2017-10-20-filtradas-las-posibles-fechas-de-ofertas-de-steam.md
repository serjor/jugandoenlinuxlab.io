---
author: Pato
category: Noticia
date: 2017-10-20 17:30:37
excerpt: <p>SteamDB&nbsp; ha anunciado las posibles fechas en su cuenta de twitter</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a05c597f0fd5beafc6fa40acefae0330.webp
joomla_id: 497
joomla_url: filtradas-las-posibles-fechas-de-ofertas-de-steam
layout: post
tags:
- steam
- ofertas
title: Filtradas las posibles fechas de ofertas de Steam
---
SteamDB  ha anunciado las posibles fechas en su cuenta de twitter

Como siempre que se trata de filtraciones, hay que cogerlas con pinzas. Pero siendo SteamDB y dado que últimamente es muy habitual que las fechas de ofertas se filtren con un altísimo índice de probabilidad de acierto, nos hacemos eco de la noticia. En concreto, este es el tweet del anuncio: 



> 
> Here's the [@steam_games](https://twitter.com/steam_games?ref_src=twsrc%5Etfw) Sale dates:  
>   
> Halloween Sale: Oct 26 to Nov 1  
> Autumn Sale: Nov 22 to Nov 28  
> Winter Sale: Dec 21 to Jan 4
> 
> 
> — Steam Database (@SteamDB) [October 20, 2017](https://twitter.com/SteamDB/status/921414721013772288?ref_src=twsrc%5Etfw)



Como podéis ver, Halloween desde el 26 de este mes de Octubre hasta el 1 de Noviembre, Rebajas de Otoño para el 22 de Noviembre hasta el 28, y las rebajas de Invierno desde el 21 de Diciembre hasta el 4 de Enero.


¿Tenéis ya preparadas las carteras? ¿que juegos estáis esperando con más ganas en estas rebajas?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

