---
author: Pato
category: Rol
date: 2017-07-13 08:11:00
excerpt: "<p>El DLC del aclamado juego de \"rol r\xEDtmico\" hace tiempo que estaba\
  \ disponible en Linux en acceso anticipado</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/700226d13b56ab5f2f0330a17a7ee485.webp
joomla_id: 406
joomla_url: crypt-of-the-necrodancer-amplified-ha-sido-lanzado-oficialmente
layout: post
tags:
- accion
- indie
- rol
- roguelike
- ritmico
title: Crypt of the NecroDancer AMPLIFIED ha sido lanzado oficialmente
---
El DLC del aclamado juego de "rol rítmico" hace tiempo que estaba disponible en Linux en acceso anticipado

Anoche nos llegaron noticias desde los [foros oficiales](http://steamcommunity.com/games/247080/announcements/detail/1422426030718375061) del juego que Crypt of the NecroDancer: AMPLIFIED [[web oficial](http://necrodancer.com/)] ya ha sido lanzado de forma oficial. El DLC precuela del aclamado juego de rol y acción rítmica llevaba un tiempo disponible en Linux en acceso anticipado y ahora ya lo tenemos disponible.


Para el que no lo conozca, Crypt of the NecroDancer es un juego de rol y acción de tipo "roguelike" que basa sus mecánicas en el ritmo de las canciones propias del juego o de tus canciones mp3 que suenan mientras juegas para ejecutar los movimientos y acciones de tu personaje. Quizás lo mejor para hacerse una idea es verlo en acción:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/4MLt05_WXYQ" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


El juego permite jugar con teclado, con pad o incluso con alfombras de baile.


Crypt of the NecroDancer: AMPLIFIED es la precuela de Crypt of the NecroDancer en forma de DLC de pago, está traducido al español y si estás interesado en comprarlo ahora mismo está en su página de Steam rebajado un 20% por su lanzamiento:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/554000/" width="646"></iframe></div>


No tienes Crypt of the NecroDancer? ¡Pues ahora es buena oportunidad! ¡está rebajado un 80%!


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/247080/" width="646"></iframe></div>


¿Te gusta Crypt of the NecroDancer? ¿Te gustan los juegos rítmicos?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

