---
author: Pato
category: Hardware
date: 2017-10-17 13:54:11
excerpt: "<p>La nueva consola de Atari sigue desvelando detalles de cara a su campa\xF1\
  a en Indiegogo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1a1c7a0ba8794f499343f3710b15a9ab.webp
joomla_id: 491
joomla_url: ataribox-podra-reproducir-contenido-de-servicios-de-streaming-como-hulu-hbo-go-o-netflix
layout: post
tags:
- ataribox
title: "Ataribox podr\xE1 reproducir contenido de servicios de streaming como Hulu,\
  \ HBO GO o Netflix"
---
La nueva consola de Atari sigue desvelando detalles de cara a su campaña en Indiegogo

Tal y como comentamos hace unas semanas la [próxima consola de Atari](index.php/homepage/hardware/item/588-ataribox-usara-gnu-linux), la "Ataribox" llegará con Linux "debajo del capó". A falta de otros detalles, como los juegos que traerá pre-cargados, que capacidad de almacenamiento tendrá, cómo serán los mandos o qué gráfica concreta llevará, hoy han desvelado en su blog de novedades que el aparato en cuestión será capaz de reproducir contenido de diversos servicios de streaming como **Hulu, HBO GO o Netflix**. Puedes ver el post [en este enlace](https://www.ataribox.com/news/).


Atari ya anunció en su momento que llevaría a cabo una campaña en Indiegogo para según sus palabras "hacer partícipe a la comunidad y los fans de Atari de este lanzamiento", y donde se espera que al fin desvelen todos los detalles de esta "Ataribox".


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/5WNsmCpdhhY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Se espera que la "Ataribox" llegue a mediados del próximo año 2018 con juegos propios así como novedades de diversos estudios, podrá reproducir juegos de otras plataformas y tendrá un precio estimado de entre 249 a 299$. 


¿Qué te parece la propuesta de Atari? ¿Piensas que tendrá mas éxito que las Steam Machines?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

