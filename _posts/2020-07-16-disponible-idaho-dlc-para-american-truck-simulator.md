---
author: leillo1975
category: "Simulaci\xF3n"
date: 2020-07-16 14:33:18
excerpt: "<p>La expansi\xF3n acaba de ser lanzada por @SCSsoftware.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Idaho/ATS_Idaho_copy.webp
joomla_id: 1242
joomla_url: disponible-idaho-dlc-para-american-truck-simulator
layout: post
tags:
- dlc
- scs-software
- simulacion
- idaho
title: 'Disponible Idaho DLC para American Truck Simulator. '
---
La expansión acaba de ser lanzada por @SCSsoftware.


 La verdad es que hace bien poco que nos paseabamos por las carreteras de [Utah](index.php/component/k2/1-blog-principal/simulacion/1251-lanzada-la-dlc-utah-para-american-truck-simulator), y hoy ya tenemos un nuevo estado por el que conducir nuestros flamantes camiones. En esta ocasión nos vamos un poco hacia el norte, tal y como os anunciamos ya hace algún tiempo, al **estado de Idaho**. Los siempre incansables chicos de [SCS Software](https://scssoft.com/) nos anticipaban la llegada de esta DLC en sus redes sociales con el siguiente tweet:



> 
> It's official! Idaho for American Truck Simulator will be released at 18:00 CEST 🇺🇸 🙌  
>   
> We are also preparing for a release live-stream on Twitch & Steam with special guests 🎥  
>   
> Find out all the details & more at our Idaho release blog: <https://t.co/NpJlkQYNVz> [pic.twitter.com/VkO64mvaYK](https://t.co/VkO64mvaYK)
> 
> 
> — SCS Software (@SCSsoftware) [July 16, 2020](https://twitter.com/SCSsoftware/status/1283707097599877125?ref_src=twsrc%5Etfw)


  





Como sabeis, en los últimos tiempos, **en la compañía checa han pisado el acelerador,** trayéndonos cantidad de expansiones de territorio de sus dos juegos estrella, Euro Truck Simulator 2 y American Truck Simulator, yendo en este último a dos estados por año (el próximo será el estado de [Colorado](https://blog.scssoft.com/2020/03/introducing-colorado.html), que previsiblemente veremos a final de 2020). Además de esto, y para preparar la llegada de Idaho, SCS ha lanzado recientemente la [versión 1.38](https://blog.scssoft.com/2020/07/american-truck-simulator-138-release.html) del juego, que entre otras cosas le da un **repaso bestial a la ciudad de Las Vegas**, así como **mejoras gráficas** (SSAO) o las **estimaciones de llegada a los diferentes puntos de ruta**.


Volviendo a lo que nos ocupa, en el **"estado Gema"**, que es así como se le conoce a Idaho encontraremos las siguientes [características](https://blog.scssoft.com/2020/07/idaho-release.html):


*Bienvenidos a Idaho, una tierra de belleza escarpada que alberga millones de acres de tierra salvaje y un paisaje que va desde el desierto hasta las granjas fértiles. Más conocida por sus patatas mundialmente famosas, Idaho ofrece a los conductores la oportunidad de involucrarse en algunas de las mayores economías de los Estados, como la agricultura y la recolección de madera.*


*Visite **Boise**, la capital del estado con un vibrante centro urbano que se encuentra en el corazón del "**Treasure Valley**" o experimente una vista fuera de este mundo en el **Monumento Nacional Cráteres de la Luna**; Idaho ofrece a sus visitantes una interminable belleza natural y puntos de referencia para explorar.*


***Características***


*-Más de **3.300 millas de nuevos caminos** para descubrir y explorar*  
 *-**11 nuevas ciudades** - conduce por el centro de Boise, Coeur d'Alene en un precioso lago o Idaho Falls con un encantador centro turístico*  
 *-Un nuevo "**Viewpoint**" que le da una nueva perspectiva del mundo*  
 *-**Expansión de las industrias favoritas** de los fans como la agricultura, la recolección de madera o el procesamiento de la madera*  
 *-Visite el famoso **Monumento Nacional de los Cráteres de la Luna** y la **Reserva***  
 *-Más de **40 puntos reconocibles** del programa de Marcadores de Carreteras Históricas*  
 *-**Hitos geográficos específicos** de cada Estado, como cañones, valles o formaciones de roca basáltica*  
 *-Muchas *familiares* **paradas de camiones y áreas de descanso** que los camioneros conocerán del mundo real*  
 *-Diseños de carreteras finamente ajustados y señalización real...*  
 *-Logros específicos de Idaho para desbloquear*


![Idaho Road map small](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Idaho/Idaho_Road_map_small.webp)


Como veis un contenido más que apetecible para todos aquellos que amamos los juegos de SCS Software. Desde JugandoEnLinux intentaremos, como viene siendo habitual realizar una serie de Streams a través de nuestros canales de [Youtube](https://www.youtube.com/c/jugandoenlinuxcom) y [Twitch](https://www.twitch.tv/jugandoenlinux), de los que os iremos informando a través de nuestras redes sociales, por lo que estaos atentos a nuestras cuentas de [Twitter](https://twitter.com/JugandoenLinux) y [Mastodon](https://mastodon.social/@jugandoenlinux). Si nos es posible empezaremos hoy mismo.


Podeis comprar "American Truck Simulator: Idaho DLC" en [Steam](https://store.steampowered.com/app/1209470/American_Truck_Simulator__Idaho/) por un precio de **11, 99€**. También podeis haceros con el [**Bundle Especial del Pacifico Noroeste**](https://store.steampowered.com/bundle/15571/Pacific_Northwest/) donde podreis encontrar a un precio especial el [juego base](https://www.humblebundle.com/store/american-truck-simulator?partner=jugandoenlinux), así como los estados de [Oregon](index.php/component/k2/1-blog-principal/analisis/998-analisis-american-truck-simulator-dlc-oregon), [Washington](index.php/component/k2/1-blog-principal/analisis/1196-analisis-american-truck-simulator-washington-dlc) e Idaho, y los paquetes de [Maquinaria Forestal](https://store.steampowered.com/app/1076080/American_Truck_Simulator__Forest_Machinery/) y las [pinturas clasicas a rayas](https://store.steampowered.com/app/951650/American_Truck_Simulator__Classic_Stripes_Paint_Jobs_Pack/). Os dejamos en compañía del trailer de lanzamiento de "Idaho":


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/4e0uztR4Ydk" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Y bien.... ¿Qué os parecen las últimas expansiones de ATS? ¿Os gustan los juegos de camiones de SCS Software? Cuéntanoslo en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

