---
author: leillo1975
category: Software
date: 2019-01-23 08:13:11
excerpt: "<p>Los desarrolladores de <span class=\"username u-dir\" dir=\"ltr\">@WineHQ\
  \ culminan un a\xF1o de trabajos</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/08ebf71ab73222fbbefe3b10163d3783.webp
joomla_id: 955
joomla_url: wine-4-0-ya-esta-aqui
layout: post
tags:
- wine
- vulkan
- proton
- codeweavers
title: "\xA1Wine 4.0 ya est\xE1 aqui!"
---
Los desarrolladores de @WineHQ culminan un año de trabajos

Como dicta la costumbre recientemente instaurada por los desarrolladores de **Codeweavers**, veremos una versión de esta "**capa de traducción**" cada año. Casi justamente un año después del lanzamiento de la [versión 3](index.php/homepage/generos/software/12-software/732-lanzado-wine-3-0-oficialmente), nos llega el r**esultado de un año de trabajos** en la base libre de su software propietario, [Crossover](https://www.codeweavers.com/products). Como bien sabeis este año ha sido especialmente relevante para ellos, por supuesto por su **asociación con Valve** para ser la base de [Steam Play/Proton.](index.php/homepage/generos/software/12-software/960-steam-play-de-valve-da-un-salto-de-gigante-y-amplia-enormemente-el-catalogo-para-linux-steamos-actualizado)


Probablemente la mayoría de vosotros, si sois usuarios de este fantástico y polémico software, ya esteis usando la versión de desarrollo de Wine, por lo que muchos de los cambios que introduce no serán nuevos para vosotros, pero esto no quiere decir que carezcan de importancia. La **lista de cambios completa la podeis encontrar en [el anuncio oficial](https://www.winehq.org/announce/4.0)**, y como imaginais, despueś de un año de productivo trabajo, es enorme, pero aun así vamos a enumerar algunos de ellos que consideramos más importantes:


-Soporte inicial para **Direct3D 12**  
 -Se implementa un **controlador Vulkan completo**, utilizando las librerías Vulkan del host bajo X11, o MoltenVK en macOS.  
 -La función de secuencia de comandos multihilo (**Multi-Threaded Command Stream->CSMT**) está activada de forma predeterminada, lo que debería proporcionar un mejor rendimiento gráfico.  
 -Los **contextos del núcleo de OpenGL** están ahora **habilitados por defecto** para todas las tarjetas gráficas y para todas las versiones de Direct3D antes de la 12.  
 -Se implementaron **nuevas características de Direct3D 10 y 11**  
 -Varias **interfaces Direct3D** 11 han sido **actualizadas a la versión 11.2**, y varias **interfaces DXGI** han sido **actualizadas a la versión 1.6**. Esto permite que las aplicaciones que requieren las interfaces más nuevas comiencen a funcionar.  
 -Ahora hay **soporte para texturas 3D comprimidas en S3TC**. Las texturas 2D comprimidas en S3TC ya estaban soportadas, siempre y cuando los controladores OpenGL las soportaran.  
 -Los **controladores de juego HID** son compatibles con las APIs **XInput** y **Raw Input**  
 -Se implementa un **controlador SDL** para que los controladores de juegos SDL estén disponibles a través de la interfaz HID.  
 -**Soporte HiDPI para Android**. La infraestructura para establecer el reconocimiento de DPI y el escalamiento de aplicaciones que no son compatibles con DPI está implementada, pero por el momento no funciona fuera de Android.


Esperemos que este nuevo año sea al menos tan fructífero como 2018 y dentro de 12 meses, cuando estemos hablando de Wine 5.0, podamos informaros de ello. Ahora **solo queda esperar que su fork SteamPlay/Proton se sincronice con este nueva versión** para aprovechar todas sus mejoras. Como siempre si quereis instalar esta nueva versión de Wine teneis las instrucciones en su [página de descargas](https://wiki.winehq.org/Download). 


 


¿Qué opinión os suscita el proyecto Wine? ¿Qué os parecen las mejoras de esta nueva versión? Déjanos tus opiniones en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

