---
author: yodefuensa
category: Estrategia
date: 2018-11-10 17:54:48
excerpt: <p>Ya podemos seguir el torneo y ver como es el nuevo juego en SteamTV</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a0862b8703b49b62b05b477f73d6896c.webp
joomla_id: 895
joomla_url: torneo-primicia-de-artifact-el-nuevo-juego-de-valve
layout: post
tags:
- estrategia
- valve
- artifact
title: Torneo primicia de Artifact, el nuevo juego de Valve
---
Ya podemos seguir el torneo y ver como es el nuevo juego en SteamTV

Después de 5 años sin ningún juego por parte de valve, estamos a la vuelta de la esquina con Artifact. Este 28 de noviembre sera lanzado de forma oficial, estando disponible para nuestro sistema favorito desde el día uno de lanzamiento.


Para ir mitigando la espera o dar a conocer un poquito más su juego Valve ha decidido organizar un primer torneo.


Durante el día de hoy y mañana 11 de novimbre, 128 jugadores se enfrentarán por el premio de 10.000$ en Torneo primicia de Artifact. Patrocinado por Beyond The Summit y retransmitido en [SteamTV](https://steam.tv/artifact/), la fase de selección incluirá 7 rondas (sistema suizo) el sábado. Y el domingo una fase de eliminación individual para los 8 primeros decidirá  el ganador. El torneo empieza ambos días a las 6 p.m. (UTC +1).


Recordamos que el precio de salida serán 20€ y que no se espera que sea un juego pay2win aunque eso todavía esta por ver.


Aquí te dejamos el primer gamplay del juego cortesía de IGN para que vayas abriendo boca.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/MVUL48Wrnko" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>

