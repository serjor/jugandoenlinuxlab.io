---
author: Pato
category: Estrategia
date: 2019-10-22 16:24:16
excerpt: <p>El juego de <span class="css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo
  r-qvutc0">@ArcenGames nos trae estrategia espacial a gran escala</span></p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4950e1f59bb84ab9b2a63947ae2b775f.webp
joomla_id: 1125
joomla_url: ai-war-ii-ya-disponible-en-linux-steamos-hoy-dia-de-su-lanzamiento
layout: post
tags:
- indie
- espacio
- rts
- simulador
- estrategia
title: "AI war II ya disponible en Linux/SteamOS hoy, d\xEDa de su lanzamiento"
---
El juego de @ArcenGames nos trae estrategia espacial a gran escala

Buenas noticias para los amantes de la estrategia espacial mas profunda. AI War II acaba de salir de acceso anticipado y ya está disponible para nuestro sistema favorito hoy día de salida para desafiarnos con su estrategia espacial:



> 
> AI War 2 has launched out of Early Access and is now fully released on Steam, Humble Store, and GOG! ?️?? RTs/likes appreciated! <https://t.co/rbpj0wfNmb> [pic.twitter.com/B1LvWaiDdt](https://t.co/B1LvWaiDdt)
> 
> 
> — Arcen Games (@ArcenGames) [October 22, 2019](https://twitter.com/ArcenGames/status/1186660098279497729?ref_src=twsrc%5Etfw)



 


*La inteligencia artificial más desafiante y aclamada en los juegos de estrategia regresa ... con una gran cantidad de enemigos mutuos. AI War II es un juego de gran estrategia / RTS híbrido contra un enemigo abrumador e inhumano que ha conquistado la galaxia.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/Dfp7hjJgCFI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Tienes toda la información sobre el juego en su [página web oficial](https://arcengames.com/aiwar2/).


AI War II no está traducido al español, pero si eso no es problema para ti, lo tienes disponible en [Humble Bundle](https://www.humblebundle.com/store/ai-war-2?partner=jugandoenlinux) (enlace patrocinado) en [GOG](https://www.gog.com/game/ai_war_2) o en [Steam](https://store.steampowered.com/app/573410/AI_War_2/) con un 40% de descuento actualmente por lo que es un buen momento para comprarlo.

