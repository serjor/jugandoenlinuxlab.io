---
author: leillo1975
category: Carreras
date: 2021-10-20 11:07:40
excerpt: "<p>Ya podemos disfrutar de la nueva #DLC de&nbsp;@horizonchase de mano de\
  \ los chicos de @AquirisGS .</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HorizonChaseTurbo/SennaForever/screenshot_01.webp
joomla_id: 1380
joomla_url: senna-forever-disponible-para-horizon-chase-turbo
layout: post
tags:
- dlc
- horizon-chase-turbo
- aquiris
- senna-forever
title: '"SENNA FOREVER", disponible para Horizon Chase Turbo'
---
Ya podemos disfrutar de la nueva #DLC de @horizonchase de mano de los chicos de @AquirisGS .


 Pocas semanas después de que os [anunciáramos]({{ "/posts/senna-forever-sera-la-proxima-expansion-de-horizon-chase-turbo" | absolute_url }}) la llegada inminente de esta nueva expansión de contenido para [Horizon Chase Turbo](https://www.horizonchase.com/), ya está con nosotros esta esperadísima DLC que **rinde una vez  más homenaje al queridísimo e inolvidable Ayrton Senna**. Como sabeis, el estudio brasileño decidió recordar la trayectoria de "el piloto" usando la particular visión y jugabilidad de su afamado juego de carreras de coches.


La fecha no ha sido elegida al azar, pues coincide con el **30 aniversario de la consecución de su tercer mundial de F1 en 1991** (Suzuka, Japón). A bordo de su McLaren-Honda se impuso a figuras como Nigel Mansell o su nemesis, Alain Prost en la penúltima carrera del campeonato. Este contenido además ha sido creado en colaboración con [Senna Brands](https://www.ayrtonsenna.com.br/legado/marcas/senna-brand/), y **parte de las ganancias irán a parar al [Instituto Ayrton Senna](https://institutoayrtonsenna.org.br/pt-br.html), entidad benéfica que impulsa proyectos educativos por todo Brasil**.


![tricampeonato](https://www.ayrtonsenna.com.br/wp-content/uploads/2016/11/galeria3.jpg)


Sus desarrolladores prometen al menos **10 horas de nuevo contenido con el sello de Airton Senna**. En ellas te podrás poner en su piel y seguir sus pasos a lo largo de su trayectoria deportiva en el mundo del automovilismo, pudiendo revivir toda su carrera. A lo largo de 5 capítulos podrás alcanzar hasta 130 azañas, llevando siempre a nuestro héroe a la primera posición.


Durante nuestras partidas podremos además admirar detalles de diseño como los **diferentes cascos que lució el piloto**, y como no, sus **coches más icónicos**. A nivel de **escenarios**, la DLC contará con **15 basados en circuitos**. Habrá **elamentos** como los neumáticos mejorados o la aerodinámica avanzada **que influirán en el comportamiento de los más de 30 vehículos jugables**. También podremos optar por militar en los **18 equipos** que veremos en "Senna Forever". Mención especial hay que hacer de la inclusión de la **cámara en primera persona** que nos permitirá ponernos en el "cockpit" de estos bólidos.


En JugandoEnLinux.com realizaremos más pronto que tarde un directo exclusivo en nuestros canales de [Youtube](https://www.youtube.com/c/jugandoenlinuxcom) y [Twitch](https://www.twitch.tv/jugandoenlinux) donde podreis comprobar todo esto de lo que os estamos hablando, por lo que os encomendamos a estar pendientes de nuestras cuentas de [Twitter](https://twitter.com/jugandoenlinux?lang=es) y [Mastodon](https://mastodon.social/@jugandoenlinux) para que podais enteraros de la fecha y hora del stream en directo. No nos gustaría olvidarnos de **Jesús Fabre** por la inestimable ayuda prestada para realizar estos artículos y el inminente Stream.


Si quereis haceros con esta fantástica DLC para Horizon Chase Turbo, Senna Forever, podeis hacerlo en Steam por tan solo **5.99€**:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1608540/" width="646"></iframe></div>


Os dejamos con el trailer de lanzamiento:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="440" src="https://www.youtube.com/embed/3JtRXs-_Psg" title="YouTube video player" width="780"></iframe></div>
 ¿Habeis jugado ya a Horizon Chase Turbo? ¿Qué os parece esta DLC homenajeando al gran Airton Senna? Si quieres decir que te parece esta expansión o tienes cualquier pregunta sobre ella, hazlo en los comentarios o en nuestro grupo de JugandoEnLinux en [Telegram](https://t.me/jugandoenlinux) o nuestras salas de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org).

