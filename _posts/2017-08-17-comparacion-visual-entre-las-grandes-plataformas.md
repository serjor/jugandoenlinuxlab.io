---
author: Serjor
category: Editorial
date: 2017-08-17 19:29:59
excerpt: "<p>\xBFQui\xE9n dijo que no hab\xEDa juegos para GNU/Linux?</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2660be2b2df02c41fc17abdbfc676d66.webp
joomla_id: 434
joomla_url: comparacion-visual-entre-las-grandes-plataformas
layout: post
tags:
- steamos
- steam
- estadisticas
title: "Comparaci\xF3n visual entre las grandes plataformas"
---
¿Quién dijo que no había juegos para GNU/Linux?

Vía [reddit](https://www.reddit.com/r/linux_gaming/comments/6u2uwd/visual_comparison_between_the_current_big/?ref=share&ref_source=link) encontramos un post con una comparativa visual del número de juegos por plataforma de juego, y la verdad, no esperaba que las diferencias fueran tan grandes.


![Número de juegos por plataformas](https://i.imgur.com/UWBGE5n.jpg)


Es cierto que en esa cifra de juegos para SteamOS hay pocos AAA, y que la mayoría son indies y juegos con algún tiempo en sus espaldas, pero a pesar de todo es alentador ver SteamOS, o GNU/Linux según se quiera ver, tiene un estado de forma bastante saludable, considerando que salió en diciembre del 2013.


Ahora solo queda que las grandes compañías se apunten al carro y empiecen a lanzar sus juegos en SteamOS - GNU/Linux.


Y tú, ¿cómo ves el estado de los juegos en GNU/Linux? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

