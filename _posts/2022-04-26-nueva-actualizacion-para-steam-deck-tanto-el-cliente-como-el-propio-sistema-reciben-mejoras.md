---
author: Jugando en Linux
category: Hardware
date: 2022-04-26 07:34:54
excerpt: "<p>Valve sigue mejorando su m\xE1quina en todos los frentes</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/SteamDeckVerified.webp
joomla_id: 1463
joomla_url: nueva-actualizacion-para-steam-deck-tanto-el-cliente-como-el-propio-sistema-reciben-mejoras
layout: post
tags:
- steamos
- steam-deck
title: "Nueva actualizaci\xF3n para Steam Deck: tanto el cliente como el propio sistema\
  \ reciben mejoras"
---
Valve sigue mejorando su máquina en todos los frentes


Nueva actualización de Steam Deck con mejoras para todos los gustos. Esta vez la actualización incluye tanto el software del sistema como en firmware de la máquina:


**Actualización del cliente:**  
Añadida la función de pantalla de bloqueo:  
La pantalla de bloqueo es específica del dispositivo y se puede configurar para que aparezca en el modo de activación, arranque, inicio de sesión y / o al cambiar al modo Escritorio  
El PIN se puede ingresar usando la pantalla táctil o los controles


  
Añadidos teclados localizados para 21 idiomas y diseños  
Se pueden habilitar varios teclados en Configuración> Teclado> Teclados activos  
Usa la nueva tecla de globo en el teclado para cambiar entre teclados activos


  
Añadido soporte para múltiples ventanas dentro de una aplicación o juego.  
Presiona el botón Steam para ver ventanas activas y selecciona qué ventana deseas ver.  
Útil para navegadores web o juegos con lanzadores.


  
Diseño actualizado de la página de Logros, lo que facilita su carga y es más fácil de navegar:  
El nuevo menú de logros permite a los jugadores comparar rápidamente las estadísticas con cualquier amigo que también esté jugando.  
Las solicitudes de amistad y pendientes se han combinado en una sola página nueva para un mejor manejo de las solicitudes de amigos.


  
Añadida la lógica para detectar y notificar al usuario cuando una tarjeta microSD no coincide con el tamaño y las especificaciones de almacenamiento anunciadas, en lugar de intentar formatear (durante mucho tiempo).  
Problema solucionado donde Steam y el botón (...) no se puede usar con Remote Play al transmitir juegos desde PC.


  
Más mejoras de rendimiento para jugadores con bibliotecas de juegos muy grandes.


**Actualización del sistema operativo:**  
Añadidos mensajes cuando se conecta un cargador que no cumple con el nivel mínimo.


  
Añadida una configuración de framerate desbloqueado en el menú Acceso rápido> Rendimiento.


  
Añadida la opción experimental de sombreado de tasa media al menú Acceso rápido> Rendimiento, forzando el sombreado de velocidad variable 2x2 en los juegos existentes para ahorrar energía.


  
Botón combinado agregado: mantener "..."+" Volumen hacia abajo "para restablecer el contrato de PD en los casos en que Steam Deck se atasca debido a un dispositivo Tipo C incompatible.


  
LED de alimentación actualizado para atenuarse unos segundos después de los eventos de conexión de la fuente de alimentación para una mejor experiencia en entornos oscuros.


  
Compatibilidad mejorada para varios docks tipo C y PSU.


  
Mejora de la duración de la batería en escenarios de uso inactivo o muy bajo.


  
Estabilidad mejorada.


  
Corregidos los problemas en los que la pantalla táctil no funciona después de algunos arranques.


  
Corregida la compatibilidad con algunas tarjetas SD específicamente cuando se usaban como dispositivos de arranque.


  
Corregido el clic háptico adicional al presionar los trackpads.


  
Solucionado el Error ACPI en el núcleo.


Puedes ver toda la información en el post original en Steam [en este enlace](https://store.steampowered.com/news/app/1675200/view/3216142491801797532).

