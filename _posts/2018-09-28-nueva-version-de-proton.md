---
author: Serjor
category: Noticia
date: 2018-09-28 16:40:33
excerpt: <p>Valve rompiendo su propio Valve Time</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e293f59f4ae0fed75a859fdf17db3e7b.webp
joomla_id: 863
joomla_url: nueva-version-de-proton
layout: post
tags:
- steam
- valve
- steam-play
- dxvk
- proton
title: "Nueva versi\xF3n de Proton (ACTUALIZADO 2)"
---
Valve rompiendo su propio Valve Time

**ACTUALIZADO 17-10-18:** Una vez más os informamos de que se ha actualizado de nuevo la versión de Proton, concretamente a la [3.16-2](https://github.com/ValveSoftware/Proton/wiki/Changelog#316-2), que incluye los siguientes cambios:


* DXVK se construye nuevamente como un DLL de Windows, debido a incompatibilidades con el "Steam Runtime".  Esto debería solucionar fallos generalizados con los títulos de Direct3D 11.
* DXVK actualizado a 0.90.
* Arreglo par los fallos al realizar "alt-tab" en juegos de Unreal Engine 1, incluyendo Deus Ex.
* PROTON_USE_WINED3D11 ahora se llama PROTON_USE_WINED3D para reflejar que esta variable afecta más que a Direct3D 11. El nombre antiguo todavía funciona como un alias.


Como veis el proyecto avanza con buen paso y las mejoras son constantes. Seguiremos atentos a todas las novedades que se produzcan con Steam Play - Proton.




---


**ACTUALIZADO 11-10-18:** Nos llegan noticias desde [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Proton-3.7-7-Update-Plus-RADV) que la versión 3.7.7 deja de ser Beta para pasar a ser la oficial, por lo que los cambios propuestos en esta pasarán a estar disponibles para todo el mundo (ver noticia original abajo). Asi mismo se ha puesto a disposición de los usuarios más "atrevidos" la versión 3.7.8 Beta que contiene arreglos menores en compatibilidad pensados para futuros lanzamientos, según comenta Valve.


Continuaremos informando de los cambios que se produzcan en este proyecto tan puntualmente nos sea posible.




---


**NOTICIA ORIGINAL:** Leemos en [reddit](https://www.reddit.com/r/linux_gaming/comments/9jo5b5/proton_377_beta_released/) que Valve ha sacado una [nueva revisión](https://github.com/ValveSoftware/Proton/blob/proton_3.7/CHANGELOG.md#37-7) de Proton.


Esta versión, la 3.7-7, trae como novedades:


* Mejoras en el comportamiento de ALT+Tab en juegos a pantalla completa
* Mejoras en el comportamiento del punto en algunos juegos y ratones con altas velocidades de sampleo/polling (cuantas veces por segundo reportan la posición del puntero)
* **DXVK actualizado a la versión 0.80** (Podéis leer más sobre las mejoras de esta versión [aquí](index.php/homepage/generos/software/item/979-grandes-avances-en-la-ultima-version-de-dxvk))


En el momento de escribir este artículo, al menos mi cliente de Steam no se ha actualizado aún, así que suponemos que poco a poco irán desplegando esta versión.


Si a ti ya te ha llegado y has notado algún cambio importante, ya sea mejoras o problemas, no dudes en decírnoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

