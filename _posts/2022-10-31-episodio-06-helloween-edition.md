---
author: P_Vader
category: Podcast
date: 2022-10-31 19:03:30
excerpt: "<p>Hablamos de la usurpaci\xF3n de 0A.D. Canseco presenta su colaboraci\xF3\
  n en <span class=\"style-scope yt-formatted-string\" dir=\"auto\">Cataclysm: D.D.A.\
  \ </span>y otros temas sobre Steam, flatpak o la Deck.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Podcast/podcast_episodio_06.webp
joomla_id: 1501
joomla_url: episodio-06-helloween-edition
layout: post
tags:
- steam
- terror
- flatpak
- survival-horror
- podcast
- halloween
- 0ad
- steam-deck
- cataclysm-dark-days-ahead
title: "Episodio 06 \"Helloween \U0001F383\" edition"
---
Hablamos de la usurpación de 0A.D. Canseco presenta su colaboración en Cataclysm: D.D.A. y otros temas sobre Steam, flatpak o la Deck.


 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/UUivG7lI6K8" title="YouTube video player" width="720"></iframe></div>


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="200" id="audio_95127220" loading="lazy" src="https://www.ivoox.com/player_ej_95127220_4_1.html?c1=ff6600" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;"></iframe></div>


 


Sexto capítulo del podcast de jugandoenlinux.com donde comentamos:


* Publican en Steam una versión de 0ad que no tiene que ver con el equipo que está detrás del juego <https://twitter.com/play0ad/status/1583100868236546050>
* Canseco nos habla de su colaboración en Cataclysm: Dark Days Ahead <https://cataclysmdda.org/> , un interesante juego open source muy apropiado para estas fechas tan cercanas a Halloween
* La nueva beta disponible para la versión de escritorio de Steam, donde podemos probar la nueva interfaz que se usa en steam deck. Podéis ver el anuncio oficial aquí <https://store.steampowered.com/news/app/593110/view/3394051164709183116>
* Divagamos un poco mucho con un leak de la gente de steamdeckhq <https://steamdeckhq.com/news/steamos-desktop-imaging-could-be-coming-soon/> , donde nos hablan de un posible grabador de isos de SteamOS para PC.
* E intentamos entender el aumento de un 4,81% de uso de la versión para flatpak de Steam, y cómo la versión en snap está evolucionando <https://ubuntu.com//blog/what-the-steam-snap-is-evolving> a marchas forzadas, para superar a la versión flatpak
* Por último, recomendaciones varias para jugar durante el día de los muertos, con compra en directo del Resident Evil 4 HD (con error incluído).


 


¿Lo has escuchado? ¿Quieres comentarnos algo? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

