---
author: Jugando en Linux
category: Apt-Get Update
date: 2022-05-20 21:45:57
excerpt: ''
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/apico.webp
joomla_id: 1469
joomla_url: nuevos-juegos-con-soporte-nativo-20-05-2022
layout: post
title: Nuevos juegos con soporte nativo 20/05/2022
---

Nueva semana, nuevas novedades a las que echarle un ojo, por si quieres probar cosas nuevas:


#### APICO


DESARROLLADOR: TNgineers
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/4JVHHh-n34c" title="YouTube video player" width="720"></iframe></div>

<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1390190/" width="646"></iframe></div>

---

#### Hovercars 3077: Underground racing

DESARROLLADOR: Airem, Airem Motorsport Tech
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/ahw3GJ9MxqM" title="YouTube video player" width="720"></iframe></div>

<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1793420/643920/" width="646"></iframe></div>

---

#### Old World

DESARROLLADOR: Mohawk Games
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/qw1d_7MvCBo" title="YouTube video player" width="720"></iframe></div>

<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/597180/" width="646"></iframe></div>

---

#### I'm on Observation Duty 5

DESARROLLADOR: Notovia
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/E8rB2JpOYDM" title="YouTube video player" width="720"></iframe></div>

<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1850550/" width="646"></iframe></div>

---

#### The Night of the Scissors

DESARROLLADOR: Tomás Esconjaureguy
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/vRH7Q-ecTLo" title="YouTube video player" width="720"></iframe></div>

<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1975180/" width="646"></iframe></div>

---

#### OFERTAS

Hearts of Iron IV

DESARROLLADOR: Paradox Development Studio
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/e9BiUtINy4w" title="YouTube video player" width="720"></iframe></div>

<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/394360/" width="646"></iframe></div>

---

#### Cities: Skylines

DESARROLLADOR: Colossal Order Ltd.
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/CpWe03NhXKs" title="YouTube video player" width="720"></iframe></div>

<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/255710/62700/" width="646"></iframe></div>

---

#### Stellaris

DESARROLLADOR: Paradox Development Studio
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/zL0kemiI0yc" title="YouTube video player" width="720"></iframe></div>

<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/281990/38760/" width="646"></iframe></div>

---

#### Slime Rancher

DESARROLLADOR: Monomi Park
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/jDZUhN8pU9c" title="YouTube video player" width="720"></iframe></div>

<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/433340/" width="646"></iframe></div>
