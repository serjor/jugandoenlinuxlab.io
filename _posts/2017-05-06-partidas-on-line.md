---
author: Serjor
category: Editorial
date: 2017-05-06 10:00:00
excerpt: <p>Hoy mismo (11/05/2017) nos echaremos unas partidas con todos los que quieran
  pasar por nuestro Discord a las 22:15 UTC+2</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f75f45065e491a3adc61e72a384867bb.webp
joomla_id: 292
joomla_url: partidas-on-line
layout: post
tags:
- comunidad
- online
title: "Partidas on-line (Actualizaci\xF3n)"
---
Hoy mismo (11/05/2017) nos echaremos unas partidas con todos los que quieran pasar por nuestro Discord a las 22:15 UTC+2

Actualización (11/05/2017)


Hoy mismo comenzamos la primera quedada linuxera. Esta noche a las 22:15h UTC+2(Hora peninsular española) estaremos en nuestro [canal de Discord](https://discord.gg/ftcmBjD) donde estaremos echando unas partidas al Grid Autosport y charlando con todo aquel que pase por allí. ¡Estais todos invitados!


 


Actualización: 


Ya tenemos juegos ganadores, Grid y TF2


Por si no soléis visitar nuestro foro muy amenudo, os recomendamos que os paséis por allí, se está empezando a cocinar una quedada linuxera para compartir un rato jugando entre nosotros.


Pasaros por este hilo: [https://jugandoenlinux.com/index.php/foro/foro-general/18-animate-a-echar-unas-partidas-con-nuestra-comunidad](index.php/foro/foro-general/18-animate-a-echar-unas-partidas-con-nuestra-comunidad) y aportar vuestro granito. No es imprescindible jugar usando linux, pero si usáis otros sistemas el pique sano está asegurado ;-)


 

