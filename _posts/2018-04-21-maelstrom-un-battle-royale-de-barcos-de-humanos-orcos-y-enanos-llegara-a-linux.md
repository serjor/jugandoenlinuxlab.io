---
author: Pato
category: "Acci\xF3n"
date: 2018-04-21 20:36:44
excerpt: "<p>El juego est\xE1 en fase de acceso anticipado solo en Windows</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ce5a90e759e6e136127a80d546625866.webp
joomla_id: 716
joomla_url: maelstrom-un-battle-royale-de-barcos-de-humanos-orcos-y-enanos-llegara-a-linux
layout: post
tags:
- accion
- indie
- proximamente
- estrategia
- battle-royale
title: "'Maelstrom' un \"Battle Royale\" de barcos de humanos, Orcos y Enanos llegar\xE1\
  \ a Linux"
---
El juego está en fase de acceso anticipado solo en Windows

Seguimos con la cuenta atrás esperando al primer "battle royale" en nuestros sistemas. En este caso hablamos de 'Maelstrom', un juego que se aparta del concepto de "batalla" entre héroes para pasar a una "batalla entre barcos". Bueno, no solo entre barcos...


Por que en Maelstrom, aparte de tres facciones a saber, humanos, enanos y orcos también tendremos que defendernos de monstruos de las profundidades en una batalla para acumular oro y más oro, hundiendo a todos los demás barcos rivales hasta quedar el último en pié... o a flote.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/TDI7eihcgvg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 En cuanto a la versión para Linux/SteamOS, los desarrolladores lo dejan bien claro en su hilo de [preguntas frecuentes](https://steamcommunity.com/app/764050/discussions/0/1698293068428954856/) donde confirman que de momento el juego está en acceso anticipado solo para Windows, y que más tarde saldrá para Linux.


Si queréis saber más sobre este Maelstrom podéis echar un vistazo a su [página web oficial](http://www.gunpowdergames.com/) o en su [página de Steam](http://store.steampowered.com/app/764050/Maelstrom/).


¿Qué te parece el concepto de Maelstrom? ¿Te gustaría jugarlo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

