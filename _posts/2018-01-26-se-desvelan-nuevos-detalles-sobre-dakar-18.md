---
author: leillo1975
category: Carreras
date: 2018-01-26 09:59:37
excerpt: <p class="ProfileHeaderCard-screenname u-inlineBlock u-dir" dir="ltr"><span
  class="username u-dir" dir="ltr">@BigmoonEnt nos desvela la fecha de salida y un
  nuevo</span><span class="username u-dir" dir="ltr"> trailer</span></p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8dd1b15b07ee959fff716f219ff43fb5.webp
joomla_id: 619
joomla_url: se-desvelan-nuevos-detalles-sobre-dakar-18
layout: post
tags:
- simulador
- unreal-engine
- dakar-18
- bigmoon-entertaiment
title: "Se desvelan nuevos detalles sobre Dakar 18(ACTUALIZACI\xD3N)"
---
@BigmoonEnt nos desvela la fecha de salida y un nuevo trailer

**ACTUALIZACIÓN 7-4-18**: Hacía bastante tiempo (medio año) que no os ofrecíamos información sobre este juego. El caso es que en su cuenta de [reddit](https://www.reddit.com/r/DakartheGame/), han ido dando información y material gráfico para los que seguimos el juego, pero ayer finalmente han lanzado un nuevo trailer donde se hace oficial la fecha de salida del juego, que queda finalmente fijada **el día 11 de Septiembre**. Como os volvemos a reiterar , **no existe confirmación oficial de que el juego finalmente disponga de una versión para nuestros sistemas**, por lo que debemos tomar esta información como todas las anteriores, con cautela y sin hacer castillos en el aire. Os dejamos con el video:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/t-dN5wivBGo" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 Por supuesto, en JugandoEnLinux.com os iremos actualizando con las noticias más importantes que genere este juego, y más concretamente en lo que tiene que ver con el soporte a nuestro sistema, tanto si se confirma como si finalmente no llega a producirse.




---


**NOTICIA ORIGINAL:** Como sabeis la semana pasada [os hablábamos](index.php/homepage/generos/carreras/item/729-los-desarrolladores-de-dakar-18-estan-valorando-la-posibilidad-de-traer-su-juego-a-gnu-linux-steamos) de la **posibilidad real** de que este juego llegase a GNU-Linux/SteamOS. Una vez más volvemos a repetiros que **todo esto debe ser agarrado con pinzas**, y aunque el estudio, [Bigmoon Entertaiment](http://www.bigmoonstudios.com), ha manifestado que está en su lista de cosas para hacer, **no están en disposición de asegurarnos que puedan llevarlo a cabo**. Lo [último que sabemos](https://www.reddit.com/r/DakartheGame/comments/7q7yzq/dakar_rally_official_game_information_so_far/dt89euw/) con respecto a este tema es que depende de las etapas finales del desarrollo, y que puede estar en el lanzamiento, o añadirse después, o incluso dejarse para futuras ediciones.


Recientemente se ha abierto un [**subreddit oficial**](https://www.reddit.com/r/DakartheGame/) donde hemos encontrado cantidad de información sobre el juego. Nosotros os vamos a resumir lo más importante, pero siempre podeis consultar todas las [novedades](https://www.reddit.com/r/DakartheGame/comments/7q7yzq/dakar_rally_official_game_information_so_far/) por vosotros mismos.



> 
> -En cuanto a la **condución**, podremos salir del vehículo que pilotemos y explorar a pie, y reparar nuestro vehículo o el de otro. Estos vehiculos pueden ser **coches, camiones, motos, quads y UTV's**. Cada una de las categorías tendrá múltiples equipos, todos ellos oficiales. Por supuesto habrá una vista desde el interior del vehículo (cockpit), así como multiples desde el exterior. Tendremos herramientas de navegación como las de la vida real y será muy importante hacer uso de ellas. Habrá diferencia en pilotar por arena, barro o agua, así como los diferentes climas y tiempo que podamos encontrar.
> 
> 
> -Si hablamos de los **Pilotos**, estos tendrán cuerpo entero, no solo las cabezas y los brazos, y serán escaneados de los cuerpos reales y de fotografías de estos. **No podremos crear nuestro propio personaje**, solo podremos escoger entre los existentes.
> 
> 
> -El **mapa del juego** será extensísimo (más de 15000Km2), y de **mundo abierto**, con multitud de climas y condiciones metereológicas dinámicas. Este mapa representará  Perú, Bolivia y Argentina, siendo estos paises escaneados, a parte de usar fotos 4K para ser proyectadas en el mapa 3D usando filtros. El juego estará dividido en **14 fases** que podrán tener una duración de entre 1 o 2 horas según tu pericia conduciendo.
> 
> 
> -Como **motor gráfico** y para las **físicas** se ha usado el **Unreal Engine 4, PhysX y tecnologías propias** (Para asegurarse que todos los detalles, los aspectos , las suspensión de los vehículos es fidedigna). Se ha consultado con expertos y pilotos para trasladar esas físicas al juego, tanto en coches como terrenos. También existe la posibilidad de incluir la Realidad Virtual, pero admiten que es una posibilidad lejana.
> 
> 
> -Se cuenta con **pilotos reales** para el desarrollo y el **testeo** del juego que todas las semanas van probando, dando puntos de vista que ayuden a aproximar la experiencia real a la del juego. Estos pilotos son [Pedro Bianchi](http://www.bianchiprata.com/) y [Mário Patrao](http://mariopatraomotosport.com/).
> 
> 
> -El **juego** será lanzado durante este verano y se espera que haya una edición anual de este. Está enfocado a la **simulación**, y se intentará dar soporte a todos los volantes de marcas y modelos más conocidos. Dispondrá de **multijugador**, tanto online como **Local**, así como la posibilidad de jugar a **pantalla partida**, y modo "**hot-seat**".
> 
> 
> 


Como veis el juego tiene unas posiblilidades tremendas si todo esto que ahora describen se consigue implementar en el juego. Sigamos rezando a nuestro dios Tux para finalmente nuestros libres escritorios también puedan llenarse de arena o enfangarse en el barro. Seguiremos atentos!!


¿Te ha sorprendido alguna de estas características? ¿Qué opinas de que el juego sea un simulador?

