---
author: leillo1975
category: "Simulaci\xF3n"
date: 2017-06-07 13:28:04
excerpt: "<p>SCS se encarga ahora de dotar de esta expansi\xF3n a ATS</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/60cac5bf67bfb0259c686e8da95fd599.webp
joomla_id: 363
joomla_url: lanzado-el-dlc-heavy-cargo-para-american-truck-simulator
layout: post
tags:
- dlc
- american-truck-simulator
- ats
- scs-software
- heavy-cargo
title: Lanzado el DLC Heavy Cargo para American Truck Simulator.
---
SCS se encarga ahora de dotar de esta expansión a ATS

 Hace poco más de un mes Euro Truck Simulator [recibía una actualización que permitía llevar camiones con carga doble](index.php/homepage/generos/simulacion/item/430-scs-software-permitira-camiones-de-doble-remolque-en-sus-juegos). Poco después recibíamos una nueva DLC, la Heavy Cargo Pack que permitía que engancharamos remolques especiales con cargas de gran tonelaje. En JugandoEnLinux.com ilustrabamos esta expansión de pago con [un video en nuestro canal de Youtube](https://youtu.be/s1SaGrMcTXQ). Sin mucho que esperar ahora le toca su parte al "hermano" americano, pudiendo realizar trabajos de estas caracteristicas por las carreteras de la costa Oeste de Estados Unidos.


 



> 
> ⚠️ OVERSIZE LOAD ⚠️  
>   
> Heavy Cargo Pack is out for ATS! <https://t.co/LpMnEd7REu>  
>   
> Get the Cargo Addict bundle: <https://t.co/l1OVlhsdx5> [pic.twitter.com/C3pNcaiNBi](https://t.co/C3pNcaiNBi)
> 
> 
> — SCS Software (@SCSsoftware) [7 de junio de 2017](https://twitter.com/SCSsoftware/status/872407082082983936)



 


Con esta DLC podremos conducir estos autenticos "monstruos" entre las diferentes ciudades de los estados de California, Nevada y Arizona, y proximamente en [Nuevo México](index.php/homepage/generos/simulacion/item/416-scs-software-lanzara-nuevo-contenido-para-american-truck-simulator). Tendremos la oportunidad  de maniobrar con camiones kilométricos y con varios puntos de articulación, poniendonos las cosas, si cabe, bastante más dificiles en intersecciones y carreteras estrechas y reviradas. Estas voluminosas y pesadas cargas pueden ser Tractores Oruga, Excavadoras, Gruas, Transformadores... Podeis ver el ilustrativo video que ha dejado SCS Software en Youtube.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/I7MtoAkM-1A" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Nosotros desde JugandoEnLinux,**si tenemos la oportunidad**, estamos deseando probar esta DLC para ofreceros como mínimo otro video como el que realizamos con ETS2 Heavy Cargo Pack, y si podemos una mini-review analizando las caracteristicas más importantes de este. Si quereis haceros con esta expansión para American Truck Simulator podeis hacerlo en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/620610/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Sois usuarios de American Truck Simulator? ¿Pensais haceros con esta expansión? Puedes respondernos en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD) donde os estamos esperando.

