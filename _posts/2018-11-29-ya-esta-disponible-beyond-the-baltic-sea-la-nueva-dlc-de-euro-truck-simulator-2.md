---
author: leillo1975
category: "Simulaci\xF3n"
date: 2018-11-29 17:43:24
excerpt: "<p>@SCSsoftware expande al B\xE1ltico su juego m\xE1s conocido.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c69e8f1ae422a507cd1a8d70ce0bd45c.webp
joomla_id: 915
joomla_url: ya-esta-disponible-beyond-the-baltic-sea-la-nueva-dlc-de-euro-truck-simulator-2
layout: post
tags:
- ets2
- dlc
- scs-software
- euro-truck-simulator-2
- beyond-the-baltic-sea
title: "Ya est\xE1 disponible \"Beyond The Baltic Sea\", la nueva DLC de Euro Truck\
  \ Simulator 2"
---
@SCSsoftware expande al Báltico su juego más conocido.

Como en más de una ocasión he dicho, SCS Software debe ser una de las compañías más trabajadoras y prolíficas que conozco, ya que si seguis las noticias que generan vereis que están constantemente mejorando sus productos y lanzando complementos de todo tipo para estos. Pero hay unas DLC's que sobresalen con respecto a otras, y estas son las que amplian el territorio por el que nos podemos mover. En este caso hablamos de una de estas, pues acaba de ser lanzada "Beyond The Baltic Sea".  


Si nos vamos un poco hacia atrás veremos que hace ya bastantes meses os anunciábamos que estaban trabajando en esta expansión, y concretamente [la semana pasada nos daban la fecha de salida](index.php/homepage/generos/simulacion/14-simulacion/790-beyond-the-baltic-sea-sera-la-nueva-expansion-de-euro-truck-simulator-2). Para preparar este expansión previamente se ha lanzado la versión [1.33 de ETS2](http://blog.scssoft.com/2018/11/euro-truck-simulator-2-update-133.html) hace tan solo unos días, y que incluye importantes novedades y mejoras, como el salvado en la nube, mejores efectos de lluvia, nuevos traileres planos o mejores efectos de suspensión, entre otras muchas cosas. Después de haber disfrutado hace tan solo unas semanas del lanzamiento de [Oregon](index.php/homepage/analisis/20-analisis/998-analisis-american-truck-simulator-dlc-oregon), la expansión de American Truck Simulator, nos llega este añadido que nos permitirá visitar paises tan particulares como **Estonia**, **Letonia**, **Lituania**, el sur de **Finlandia** y hasta la parte más occidental de **Rusia**. Pero la cosa no se queda ahí, ya que también encontraremos:


*-Más de **13 mil kilómetros** de nuevos caminos en el juego*  
 *-Lituania, Letonia y Estonia para explorar*  
 *-Sur de Finlandia, que contiene las **principales ciudades y la industria***  
 *-Territorio ruso, incluidos **San Petersburgo** y **Kaliningrado**.*  
 *-**24 nuevas ciudades principales** y muchos pueblos más pequeños*  
 *-Arquitectura característica del Báltico*  
 *-Hitos famosos y lugares reconocibles*  
 *-Nuevos y únicos modelados 3D*  
 *-Exuberante vegetación acorde con el clima*  
 *-Trenes locales de inteligencia artificial, tranvías y coches de tráfico*  
 *-Más de **30 nuevos muelles e industrias** de empresas locales*  
 *-Transporte de alta capacidad sólo en Finlandia (un tractor y dos remolques largos)*  
 *-Logros de la región báltica para desbloquear*


![balt blog map big](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/BeyondTheBalticSea/balt_blog_map_big.webp)


*Detalle de la zona ampliada en esta DLC con respecto a lo ya existente*


En JugandoEnLinux.com esperamos poder ofreceros en exclusiva, como suele ser habitual con los productos de SCS Software, uno o varios gameplays de esta DLC, así como en un par de semanas después un análisis en profundidad de esta expansión


Podeis comprar "Beyond The Baltic Sea" en [Steam](https://store.steampowered.com/app/925580/Euro_Truck_Simulator_2__Beyond_the_Baltic_Sea/). Os dejamos con el video oficial de esta Expansión:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/vTGAW3vuY0g" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 ¿Sois seguidores de ETS2 y ATS? ¿Comprareis esta nueva expansión? Contadnos vuestras impresiones en los comentarios o charlad sobre ello en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

