---
author: Pato
category: Aventuras
date: 2018-07-05 08:57:48
excerpt: <p>El juego de @WeappyStudio nos lo trae @THQNordic</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/19b2674528b4d5fa396dcc8c20062871.webp
joomla_id: 796
joomla_url: this-is-the-police-2-llegara-a-linux-steamos-el-proximo-2-de-agosto
layout: post
tags:
- accion
- indie
- aventura
- proximamente
title: "'This Is the Police 2' llegar\xE1 a Linux/SteamOS el pr\xF3ximo 2 de Agosto"
---
El juego de @WeappyStudio nos lo trae @THQNordic

Leemos en [linuxgameconsortium.com](https://linuxgameconsortium.com/linux-gaming-news/this-is-the-police-2-official-august-release-date-linux-67995/) que la secuela de 'This Is the Police', el juego mezcla de aventura "noir" con una historia dramática llegará a nuestro sistema favorito el próximo día 2 de Agosto.


'This Is the Police 2' sigue los pasos del primer juego donde tendremos que dirigir el departamento del Sheriff, administrar a tus policías, investigar, interrogar, encarcelar, tomar decisiones difíciles, ¡y tratar de no entrar en la carcel tu mismo! - en esta mezcla de aventura y estrategia guiada por la historia, ambientada en una fría ciudad fronteriza plagada de violencia.


Además, en el [anuncio oficial](https://steamcommunity.com/games/785740/announcements/detail/1683666519583278401) en Steam han hecho público un vídeo gameplay:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/iu-xKlBpT_4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 **Características:**


* Sumérgete en una sorprendente mezcla de estilos de juego. Al igual que su predecesor, This Is the Police 2 se basa en diferentes géneros y mecanismos, combinándolos en una experiencia única guiada por la historia, esta vez añadiendo giros aún más inesperados. ¿Es un juego de aventuras?  ¿estrategia? ¿simulación? Un juego de gestión? Una novela visual? ¿Un rompecabezas? ¡Es todo esto y más!
* Participa en una historia donde nadie es completamente inocente. La gente de Sharpwood es ignorante y cruel.
* Mantén a tus amigos cerca y a tus enemigos más cerca. Tus subordinados no son solo recursos; son personas vivas con sus propias fortalezas, debilidades, miedos y prejuicios, y tendrás que contar con todas estas cosas para poder sobrevivir.


Tendremos disponible 'This Is the Police 2' el próximo día 2 de Agosto en [GOG](https://www.gog.com/game/this_is_the_police_2), [Humble Store](https://www.humblebundle.com/store/this-is-the-police-2?partner=jugandoenlinux) (link afiliado) y [Steam](https://store.steampowered.com/app/785740/This_Is_the_Police_2/) traducido al español.

