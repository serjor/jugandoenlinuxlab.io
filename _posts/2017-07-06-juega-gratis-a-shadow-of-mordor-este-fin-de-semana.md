---
author: Pato
category: Ofertas
date: 2017-07-06 20:23:55
excerpt: <p>Una buena oportunidad para probar este gran port de Feral</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8240a1907e29481b04619a0df33df9ab.webp
joomla_id: 398
joomla_url: juega-gratis-a-shadow-of-mordor-este-fin-de-semana
layout: post
tags:
- accion
- aventura
- feral-interactive
title: Juega gratis a Shadow of Mordor este fin de semana
---
Una buena oportunidad para probar este gran port de Feral

Tras haber estado a precio de saldo durante estas últimas rebajas de Steam donde algunos no pudimos resistirnos a comprarlo, ahora 'La Tierra Media: Sombras de Mordor' estará disponible para jugar gratis durante este fin de semana. Es una buena oportunidad para probar este gran juego y gran port que nos trajo Feral Interactive.


Además, si al final te convence y terminas comprándolo, lo podrás hacer al mismo precio que durante las ofertas de verano de Steam, ya que estará rebajado un 80%.


¿A qué esperas para probarlo? lo tienes disponible en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/241930/51209/" width="646"></iframe></div>


Por cierto, cuéntame que te parece el juego en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

