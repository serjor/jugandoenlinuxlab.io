---
author: leillo1975
category: Carreras
date: 2018-12-24 10:55:23
excerpt: <p>Sus desarrolladores, Thorsten Folkers, siguen mejorando el juego.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/18372a9dcced8d7110e2e800fab5ea12.webp
joomla_id: 936
joomla_url: el-desarrollo-de-drag-continua-con-importantes-novedades
layout: post
tags:
- drag
- thorsten-folkers
title: El desarrollo de DRAG continua con importantes novedades.
---
Sus desarrolladores, Thorsten Folkers, siguen mejorando el juego.

Hace casi un año que nos enteramos de la existencia de este proyecto, y desde el minuto uno nos impresionó. El juego, desarrollado unicamente por dos hermanos alemanes ([Thorsten Folkers](https://tfolkers.artstation.com/)) y usando Linux, muestra un nivel de detalle y unas características que en principio, sobre el papel parecen dignas de un estudio mucho más grande. Si recordais ya publicamos unas cuantas actualizaciones ([1](index.php/homepage/generos/carreras/2-carreras/735-pronto-veremos-drag-un-juego-de-carreras-en-acceso-anticipado) y [2](index.php/homepage/generos/carreras/2-carreras/931-los-desarrolladores-de-drag-nos-muestran-nuevos-avances)) desde entonces, aunque en los últimos meses estas dejaron de producirse generando en nosotros las primeras dudas.


El día 22 de diciembre, finalmente han publicado nuevas noticias en su [cuenta de Polycount,](https://polycount.com/discussion/183339/ong-offroad-racer-indie-game-dev-log/p3) donde nos desvelan en que han estado ocupados durante tanto tiempo, y como vereis el silencio de estos meses ha sido más que justificado. Nosotros vamos a intentar resumiros algunas de estas, ya que el lenguaje utilizado es demasiado técnico:


-**Grabación en el motor y captura de vídeo:** Han mejorado la forma de capturar el video del juego de forma que pueden reproducir desde diferentes puntos de vista usando el motor del juego, usando muchos menos recursos y espacio en disco que de la forma que estaban trabajando hasta ahora. A mi juicio esto les debe ser especialmente útil para las repeticiones desde diferentes cámaras. Al parecer desarrollar esta nueva característica ha sido una pesadilla pues han tenido que cambiar un montón de cosas en el resto de los elementos del juego (scripts, sonido, fisicas, reloj...) para que esto funcionase de forma correcta.


-**Follaje de tierra añadido:** Aunque aun no está incluido en todas las pistas,  han añadido follaje de tierra que se basa en el "paquete de plantas de tierra" de [MAWI United](https://mawiunited.com/). Han tenido que ajustarlo un poco para adaptarlo a sus necesidades, pero ha supesto un gran ahorro de tiempo.


![](https://us.v-cdn.net/5021068/uploads/editor/gl/7t2o6kav7vxx.jpg)


-**Configuración de detalles de geometría:** Han añadido una opción al menú de configuración para bajar el nivel de geometría. Esto es visible principalmente en el follaje reducido del suelo y está destinado a ser utilizado en sistemas de gama baja.


-Mapas normales de la textura del suelo y del terreno rediseñados


-Se agregaron superficies de cañones y acantilados.


-Se han deshecho de los anteriores intentos de corrección de color.


-Mejora de la atmósfera con densidad del aire, dispersión y fijación de la luz ambiental.



-**Se agregaron diferentes configuraciones del clima:** Con la nueva atmósfera y las opciones de densificación/dispersión del aire, probaron diferentes configuraciones climáticas y mantuvieron otras. Las diferencias son en su mayoría sutiles, pero dan un poco de variación.


![](https://us.v-cdn.net/5021068/uploads/editor/ky/1198hwulmufk.jpg)


-**Nuevo circuito (Zona-C):** Como se mencionó en la actualización anterior, han querido que el segundo ecosistema fuera completamente diferente del de Taiga / Montaña / Bosque que tienen para las pistas existentes. Han empezado trabajar en un ecosistema inspirado en la Antártida / Ciencia Ficción / Offshore. Esta pista de carreras tiene algunos cambios de elevación y combinaciones de curvas rápidas que son perfectas para las transiciones deriva. Actualmente están trabajando en detalles adicionales como bordillos, barandillas laterales, pero también en los grandes pilares que soportan la estructura.


-**Se ha corregido el error de vulnerabilidad (bug) que ha sido reportado por nuestros probadores beta:** Han aumentado la fricción física de las barandillas para que los jugadores ya no puedan aprovecharse de la conducción contra la pared.


-**LODs mejorados para las barandillas:** Debido a que las barandillas están dobladas a lo largo de la pista, necesitan una gran cantidad de polígonos para poder verse bien de cerca. Mantener la silueta y el sombreado intactos es un gran problema. Han tratado de hacer todos los LODs sin un cambio visual notable a una distancia razonable mientras han reducido el número de triángulos a una tasa del 40-45% con cada LOD. Dependiendo de la complejidad de la malla han utilizado hasta cuatro LODs en algunos casos:


![](https://us.v-cdn.net/5021068/uploads/editor/b1/bvm7lfk41s1v.gif)


-**Mejora de la dirección y de la configuración del chasis:** Ahora existen tres configuraciones de chasis diferentes. Cada uno tiene sus propias características de manejo. Han afinado la rigidez de los muelles y las barras estabilizadoras, así como la división de potencia y la distribución del peso para obtener el mejor rendimiento de conducción.
-**Se corrigió el error de escalamiento del colisionador y la reimplementación de cascos convexos****:** Ahora existen detecciones de CPU en tiempo de ejecución, e implementaciones sse2 y avx completas para cascos convexos.


-Terminado la pantalla de carga y los últimos elementos del menú


-**Carga del sombreador de asíncronos:** Este es uno de los temas de bajo nivel que esperaban haber resuelto hace mucho tiempo, pero no ha sido posible.  Cada vez que añadas o cambies algo en el mundo del juego o incluso en el menú, es posible que tengas que esperar a que esté listo. Esto tiene que estar oculto de alguna manera o tiene que precargarlo. Ahora podemos preinstalar sombreadores no sólo para los materiales del juego, sino también para los menús y el postprocesamiento.


-Selección de objetos reelaborada


-Se agregó la visualización de sobregiros y mejoras de sobregiros en el editor del motor.


![](https://us.v-cdn.net/5021068/uploads/editor/44/lnvnahb4bota.gif)


-**Pruebas continuas y optimización de la jugabilidad:** Actualmente, alrededor de una docena de probadores beta están jugando a DRAG para mejorar en el juego, así como resolver problemas técnicos. Surgieron algunas cosas menores pero ya están resueltas. 


-Han añadido algo novedades al modo para un solo jugador.


-Han empezado a trabajar en un nuevo y divertido modo de juego multijugador arcade.


-Siguen trabajando en el sistema de partículas.

Como podeis ver no han estado para nada ociosos y el juego avanza a buen ritmo. Esperemos que las próximas actualizaciones sean más regulares y pronto veamos una versión jugable de este prometedor juego. Como siempre, mientras tanto nosotros os seguiremos informando de todas las novedades que este produzca.

¿Conocíais DRAG? ¿Qué impresión os produce este desarrollo? Déjanos tu opinión en los comentarios o charla con nosotros sobre este juego en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

