---
author: Pato
category: Hardware
date: 2021-11-15 15:42:22
excerpt: "<p>El evento para desarrolladores dej\xF3 algunas novedades que no debemos\
  \ dejar pasar.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/Steam_deck_product.webp
joomla_id: 1387
joomla_url: conferencia-steam-deck-repasamos-los-puntos-mas-importantes-de-la-maquina-de-valve
layout: post
tags:
- steam
- valve
- steam-deck
title: "Conferencia Steam Deck: Repasamos  los puntos m\xE1s importantes de la m\xE1\
  quina de Valve"
---
El evento para desarrolladores dejó algunas novedades que no debemos dejar pasar.


Cada vez queda menos para que la nueva aventura de Valve en el terreno del hardware de comienzo apoyada en la **Steam Deck**, su nuevo Pc-consola portable **y el videojuego en Linux**. A pesar del retraso del lanzamiento de Steam Deck, lo cierto es que si se terminan cumpliendo los plazos quedan poco más de tres meses para que llegue a los primeros afortunados que pudieron reservarla, un tiempo que puede valerle a Valve para pulir aún mas la experiencia de usuario y lanzar un producto "redondo". Por eso se están sucediendo distintos eventos sobre todo de cara a que los desarrolladores tengan la mayor cantidad de información respecto al desarrollo de juegos para la consola y lo que se puede esperar de ella en cuanto a herramientas y rendimiento. Es un punto vital para el éxito de Steam Deck.


De este modo, tal y como [os anunciamos]({{ "/posts/el-lanzamiento-de-steam-deck-se-retrasa-hasta-febrero-de-2022" | absolute_url }}), el viernes tuvo lugar una conferencia en la que varios de los responsables del desarrollo de Steam Deck ofrecieron información y respondieron a varias preguntas por parte de los desarrolladores, y en la que repitieron mucha información que ya conocíamos, pero también algunos puntos interesantes que o no estaban del todo claros o que desconocíamos.


Antes de entrar en materia, vamos a repasar los puntos clave de la Steam Deck:


Steam Deck es un PC "Handheld" (de bolsillo) construido sobre un **SOC personalizado** por Valve y AMD bautizado con el nombre "Aerith" y que estará compuesto por **4 núcleos Zen2 con 8 hilos** de procesamiento a una frecuencia de 2.40/3.50 GHz (base/turbo), junto a una **gráfica Radeon RDNA2 con 512 Stream Processors** y a una frecuencia de 1.0/1.60 GHz (base/turbo). Contará con **16GB de RAM LPDDR5 a 5500 MHz** unificada y para el apartado gráfico reservará **1Gb de NVRAM**. 


En cuanto a poder gráfico puede que parezca poco, pero hablamos de **1,6 TFlops de potencia gráfica y un ancho de banda de hasta 88Gbps**, para alimentar una **pantalla IPS táctil de 7 pulgadas y una resolución de solo 1280x800 píxeles a 60Hz** por lo que debería ser más que suficiente para mover cualquier juego con garantías.


En el apartado del almacenamiento, la Steam Deck contará con tres versiones, siendo este el único apartado diferencial en cuanto a hardware ya que el resto de componentes será igual en todas las versiones. **La versión de 64Gb de almacenamiento contará con memoria eMMC 5.1**, mientras que l**as versiones de 256 y 512 GB contarán con discos duros SSD NVMe** lo que les dará en teoría un extra de rendimiento.


Además, por si nos parece poco espacio para juegos, la Steam Deck contará con un **slot para tarjetas microSD** con el que podremos ampliar la memoria de almacenamiento, eso sí seleccionando como es lógico tarjetas microSD de alto rendimiento. Algo que suele pasar desapercibido en este aspecto es que desde Valve confirmaron que **los juegos mostrados** en los [vídeos oficiales de IGN](https://www.youtube.com/watch?v=oLtiRGTZvGM) e**staban ejecutándose desde la tarjeta microSD.**En cuanto a este aspecto, puede ser interesante ver una de las imágenes que nos mostraron el la conferencia sobre los tiempos de carga según la tecnología de almacenamiento que usemos:


![Steam deck storage](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/Steam_deck_storage.webp)


En cuanto al resto de características, la Steam Deck contará con salida **DisplayPort 1.4** y sonido envolvente mediante un **puerto USB-C** además de sonido mediante **jack stereo de 3.5**, contará con dos altavoces frontales y micrófono incorporado, y respecto a la conectividad, tendrá **WIFI 802.11ac y Bluetooth 5.0**. La batería de 40 Wh ofrecerá una **autonomía de juego de entre 2 y 8 horas,** y contará con **cargador por USB-C de carga rápida de 40W**para un consumo estimado de la máquina de entre 4 y 15 Watios (reposo/carga).


Pasemos ya a lo que dio de si la conferencia del Viernes.


* El primer apartado de la conferencia estaba destinado al **desarrollo para Steam Deck sin un Dev-Kit**. El diseñador de la máquina Lawrence Yang confirmó que los Dev-Kits son exactamente la misma máquina que se podrá comprar por el público, y que **la Steam Deck hará uso de Wayland por defecto**, por lo que la mejor forma de desarrollar si no se tiene un Dev-Kit a mano es montar un equipo de características similares (p. ej. [AMD NUC Box](https://www.amazon.com/UM700-Windows-Desktop-Computer-Graphics/dp/B08SC66X5S/) - Ryzen 7 3750H, Radeon RX Vega 10, 16GB RAM  DDR4 ) junto a **Manjaro KDE** y un monitor 720p. No se recomienda usar máquinas virtuales ya que es difícil poder configurar bien la potencia gráfica de ese modo. En cualquier caso, confirmaron que **SteamOS 3 estará disponible para su descarga** en futuras fechas sin concretar.
* También se recomienda implementar limitadores a 60 y 30fps para ahorrar cuanta más batería mejor. La Steam Deck contará con un modo "suspensión". **Los juegos guardados se sincronizarán lo antes posible para que puedas jugar un juego en la consola, pausarlo y luego continuarlo en otro PC sin problemas**. Para que funcione, los desarrolladores tendrán que añadir algunos parámetros sobre cómo procesar los juegos guardados, pero según Valve debería ser un cambio sencillo. Desde Valve recomiendan a los desarrolladores comprobar bien su funcionamiento.
* Valve implementará el almacenamiento de assets para la Steam Deck y así evitar el tener que descargar assets de gran tamaño (p.ej. para texturas 4K) cuando estos no son necesarios, aunque su uso quedará a la discreción de los desarrolladores.


El siguiente apartado de la conferencia estaba destinado a repasar el hardware de la Steam Deck, pero a estas alturas ya pocas sorpresas nos podían mostrar. El siguiente punto fue sobre **Steam en Deck**:


* Se confirma que el frontend de la Steam Deck sustituirá al modo Big Picture en el cliente de escritorio después de la salida de Deck al mercado.
* ![steam deck bigpicture tv](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/steam_deck_bigpicture_tv.webp)
* Además, se podrá acceder al modo escritorio en SteamOS 3 para **instalar otro software a través de flatpak**. También se podrán instalar otras tiendas. En ese sentido el sistema está abierto.
* También se confirmó que **el modo de actualización del sistema será a través de imágenes** que se descargarán e instalarán de forma transparente de cara al usuario. De este modo, los directorios raiz del sistema estarán bloqueados (como hemos dicho, cualquier otro software tendrá que ser instalaldo mediante flatpak).
* Habrá una nueva pantalla para la configuración de mandos dentro del nuevo modo Big Picture.


![Steam deck control](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/Steam_deck_control.webp)


A continuación, el apartado de  **soporte bajo Proton**:


* En este apartado, el propio Pierre Loup nos indicó que de cara al desarrollo **lo ideal es contar con Vulkan** como backend en vez de DirectX/OpenGL ya que produce menos "sobrecarga" de procesamiento. En este aspecto también recomiendan habilitar Vulkan en los juegos para Windows si es posible.
* Valve no tiene preferencias en cuanto a desarrollar para Proton o en nativo para Linux, aunque reconocen que si se tienen los conocimientos y recursos para desarrollar y mantener el juego en nativo es mejor, pero si no no hay ningún problema en desarrollar para Proton.
* Se desaconseja el uso de lanzadores externos, así como formatos multimedia propietarios. Aún así, afirman que los lanzadores creados con Qt no deberían dar muchos problemas.
* Tanto AMD como Valve están trabajando en distintas herramientas de cara a controlar posibles problemas de rendimiento, cuellos de botella en el hardware etc. Estas herramientas estarán disponibles en un futuro próximo.
* También se está trabajando con distintos motores como Unreal, Unity y Godot para obtener soporte.
* Valve liberará distintas herramientas de testeo de juegos y configuración de los mismos para Steam Deck.


 A continuación, hicieron una conexión con Sebastien Nussbaum, uno de los responsables de negocio de semipersonalizados de AMD que explicó una parte muy técnica y específica sobre la APU que han creado junto a Valve. Como una imagen vale mas que mil palabras:


![AMDapu](https://i.imgur.com/TpywqUF.jpg)


La penúltima conferencia fue sobre **Steam Input**:


* Steam Deck hará uso de Xinput y Steam Input API.
* Se podrá utilizar el giroscopio de la Steam Deck presionando uno de los paneles táctiles (hápticos).


Por último, hubo una ronda de preguntas y respuestas por parte de los desarrolladores de ambos lados:


* Durante toda la conferencia se pudo ver una Steam Deck de color blanco en una de las estanterías.
* ![steam deck white](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/steam_deck_white.webp)
* Al final confirmaron que es un prototipo con temática Portal y no está destinada para la venta. Aún así, afirmaron que en un futuro podrían haber partidas de Steam Deck con distintos colores.
* Confirmaron que ya se está pensando en sucesivas generaciones de Steam Deck.
* El rendimiento de la Steam Deck será el mismo tanto si está conectada al Dock, como si se usa con baterías o con el cargador.
* No habrán juegos exclusivos para Steam Deck por parte de Valve.
* Quizás la pregunta más repetida fue si se podrían instalar tiendas fuera de Steam, como EGS o GOG. La respuesta es si. (suponemos que mediante Lutris o el mismo Proton).


Espero que este resumen os haya despejado algunas de las dudas que pudieran haber respecto al desarrollo de la Steam Deck.


Agradecer a todos mis colaboradores, sin los que no podría haber hecho este resumen.


Cuéntame tus impresiones en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

