---
author: Pato
category: Ofertas
date: 2018-06-21 08:35:25
excerpt: "<p>Hasta el d\xEDa 5 de julio cientos de juegos rebajados para tu Linux\
  \ favorito</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/37d94cffab20228fab482582b7bd4773.webp
joomla_id: 782
joomla_url: comienzan-las-rebajas-de-verano-de-steam
layout: post
tags:
- steam
- rebajas
- ofertas
title: Comienzan las rebajas de verano de Steam
---
Hasta el día 5 de julio cientos de juegos rebajados para tu Linux favorito

Como se suele decir, durante el año hay cosas inamovibles: En Junio Rafa Nadal gana Roland Garros, en julio San Fermín y las rebajas de Steam. Hace ya tiempo que [se filtró](https://twitter.com/SteamDB/status/1009482146464043009) la fecha de comienzo de estas rebajas de Verano donde Steam tira la casa por la ventana con rebajas de precios en algunos casos de escándalo.


A partir de hoy y hasta el día 5 de julio tendremos la oportunidad de conseguir nuestros juegos con descuentos y además conseguir la **insignia de las Rebajas de Verano de Steam 2018** realizando las tareas que se nos propondrán para adquirir los cromos correspondientes. Además, si te hacen falta se pueden vender y conseguir alguno dinerito, comprar si te falta alguno o intercambiarlo con algún amigo que esté interesado. En  [nuestro foro](index.php/foro/foro-general/48-steam-badges-cards-trade) también tenemos un hilo al respecto donde podéis preguntar y ofrecer vuestras propuestas.


Además como viene siendo habitual, podremos jugar a un minijuego en el que conseguiremos entrar en sorteos de juegos gratuitos. Además podremos unirnos en grupos con nuestros amigos y conquistar planetas si al final el nuestro es el grupo con más casillas en un planeta al final del juego.


También hasta el 5 de julio tanto el Steam Controller como el Steam Link están de oferta. De hecho el Steam Link durante estas ofertas tiene un precio de 2,75€ + gastos de envío.


Recordar que desde hace unos años ya no se hacen las "ofertas flash" si no que las ofertas son las mismas durante todo el periodo de campaña. A modo de propuesta, aquí tienes algunos de nuestros juegos favoritos:


* Rocket League a 9,99€
* Portal 2 a 1,67€
* Euro Truck Simulator 2 a 4,99€
* Mark of the Ninja a 3,74 €
* F1 2017 a 13,74€
* Borderlands 2 a 7,49€


Y tu, ¿Qué juegos esperas comprar estas rebajas? ¿Has visto alguna oferta a la que no te has podido resistir? ¿Que te parecen las ofertas de este año?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/bUo1PgKksgw" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 

