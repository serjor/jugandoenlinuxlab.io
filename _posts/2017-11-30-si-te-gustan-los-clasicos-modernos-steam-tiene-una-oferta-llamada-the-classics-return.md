---
author: Pato
category: Ofertas
date: 2017-11-30 18:07:03
excerpt: "<p>Las ofertas de fin de semana nos traen un buen n\xFAmero de ofertas de\
  \ g\xE9neros cl\xE1sicos actualizados disponibles en Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e9b0f857826ab994b1b1904c051bbf59.webp
joomla_id: 563
joomla_url: si-te-gustan-los-clasicos-modernos-steam-tiene-una-oferta-llamada-the-classics-return
layout: post
tags:
- ofertas
title: "Si te gustan los \"cl\xE1sicos modernos\", Steam tiene una oferta llamada\
  \ \"The Classics Return\""
---
Las ofertas de fin de semana nos traen un buen número de ofertas de géneros clásicos actualizados disponibles en Linux

Nos llegan avisos de que Steam está llevando a cabo en sus ofertas de fin de semana unas rebajas muy interesantes en juegos "clásicos" que están de moda actualmente, de ahí el nombre de la promoción "The Classics Return".


Y lo mejor de todo es que la gran mayoría de los títulos ¡están disponibles para nuestro sistema favorito! Si estás esperando alguno, no dejes de repasar las ofertas.


Títulos como Age of Wonders III, Wasteland 2, Divinity Original Sin, Shadowrun Returns y Dragonfall, Torment Tides of Numerena o Yooka Laylee con hasta un 75% de descuento.


Puedes encontrar las ofertas en este enlace:


<http://store.steampowered.com/sale/theclassicsreturn/>

