---
author: Serjor
category: "Exploraci\xF3n"
date: 2018-08-08 08:49:50
excerpt: "<p>Hasta el propio Thor tendr\xEDa problemas para sobrevivir en este mundo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6f393c73a9c9a064c4c50137fe9679b4.webp
joomla_id: 822
joomla_url: valheim-un-survival-horror-ambientado-en-la-mitologia-nordica
layout: post
tags:
- itchio
- valheim
- survival
title: "Valheim, un survival horror ambientado en la mitolog\xEDa n\xF3rdica"
---
Hasta el propio Thor tendría problemas para sobrevivir en este mundo

Gracias a gamingonlinux nos [enteramos](https://www.gamingonlinux.com/articles/valheim-an-interesting-survival-game-inspired-by-norse-mythology-and-viking-culture-currently-free.12300) de la existencia de Valheim, un survival procedural ambientado en la mitología nórdica, cuyo desarrollo se encuentra en fase alpha, y en contra de la tónica habitual que es cobrar por betas y alphas, es por el momento, gratuito, aunque el propio desarrollador [comenta](https://dvoidis.itch.io/valheim/devlog/38690/thanks) que espera tener una versión en Early Access en Steam para finales de este año.


Después de hacer una prueba rápida, y considerando que el juego está en alpha, mis sensaciones no pueden ser mejores. No he probado a jugar en ningún servidor, así que no sé cómo se comportará o si tiene problemas de latencia o algo, pero en modo local me ha gustado.


![vallheim puesta sol](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/vallheim-puesta-sol.webp)


Ahora que ya la versión para GNU/Linux de Rust no se [anuncia](https://www.gamingonlinux.com/articles/facepunch-are-no-longer-selling-the-linux-version-of-the-survival-game-rust.12236) en Steam porque la nueva versión de Unity3D que están usando les está [causando problemas](https://www.reddit.com/r/playrust/comments/92ftm2/facepunch_are_no_longer_selling_the_linux_version/e35jcs8) en la versión para GNU/Linux, tener más opciones dentro de este género, nunca está de más.


Está disponible en la tienda de [itch.io](https://dvoidis.itch.io/valheim)


Y tú, ¿le darás una oportunidad a este Valheim? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

