---
author: leillo1975
category: Software
date: 2018-05-09 18:41:44
excerpt: "<p>Ya est\xE1 disponible la App en fase Beta para Android. Est\xE1 pendiente\
  \ de aprobaci\xF3n para dispositivos Apple</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a58db2deb88133d8d82116fd2b40cd68.webp
joomla_id: 730
joomla_url: steam-link-app-y-steam-video-app-llegaran-proximamente-a-nuestros-dispositivos-moviles-actualizacion
layout: post
tags:
- steam
- valve
- android
- steam-link
- steam-video
- ios
title: "Steam Link App y Steam Video App llegar\xE1n proximamente a nuestros dispositivos\
  \ m\xF3viles (actualizaci\xF3n)"
---
Ya está disponible la App en fase Beta para Android. Está pendiente de aprobación para dispositivos Apple

**(Actualización 17/05/2018)**


Leemos en **PcGamer** que acaba de salir la App de Steam Link en la Google Play Store. De momento, la aplicación está en fase beta, y necesita para poder hacer streaming desde el PC hacia un dispositivo android una conexión por Wifi de 5Ghz o red cableada. Por otra parte, la aplicación para dispositivos de Apple está pendiente de aprobación para ser publicada, por lo que se espera que esté disponible en breve.


Si quieres probar la App en tu dispositivo android puedes descargarla [en este enlace](https://play.google.com/store/apps/details?id=com.valvesoftware.steamlink).


Fuente: [PcGamer](https://www.pcgamer.com/steam-link-app-now-available-for-android-ios-version-still-pending-apple-review/?utm_content=buffere7bed&utm_medium=social&utm_source=twitter&utm_campaign=buffer-pcgamertw)




---


**(Post original):**


De nuevo, gracias a [GamingOnLinux](https://www.gamingonlinux.com/articles/valve-have-announced-the-steam-link-app-and-the-steam-video-app.11741), nos enteramos de que Valve lanzará pronto dos nuevas aplicaciones para dispositivos móviles. La primera, **Steam Link App** llegará en día 21 de este mismo mes y permitirá utilizar las pantallas de nuestras tablets, TVs y teléfonos como pantalla para "stremear" nuestros juegos favoritos, teniendo soporte para nuestros Steam Controllers, y gamepads MFI. Por supuesto, nuestro sistema podrá hacer uso de ella. Más tarde, a finales de verano, se hará lo propio con **Steam Video App**, que permitirá disfrutar de miles de Películas y Shows a través de internet. El [comunicado original](https://store.steampowered.com/news/39998/) dice lo siguiente (traducido por [DeepL Translator](https://www.deepl.com/translator)):



> 
> *""Dos nuevas aplicaciones gratuitas - la aplicación Steam Link y la aplicación Steam Video - se preparan para su lanzamiento en las próximas semanas, ambas diseñadas para ampliar el conjunto de servicios y la accesibilidad de Steam.*
> 
> 
> *La aplicación Steam Link, cuyo lanzamiento está previsto para la semana del 21 de mayo, permite a los jugadores experimentar su biblioteca de juegos Steam en sus dispositivos Android (teléfono, tableta, TV) e iOS (iPhone, iPad, Apple TV) mientras están conectados a través de una red de 5Ghz o Ethernet cableada a un sistema host (Mac o PC), con acceso Android inicialmente ofrecido en beta. La aplicación Steam Link será compatible con Steam Controller , los controladores MFI y mucho más en ambas plataformas.*
> 
> 
> *A finales de este verano, la aplicación Steam Video se lanzará al mercado, lo que permitirá a los usuarios disfrutar de las miles de películas y programas disponibles en Steam directamente a través de sus dispositivos Android e iOS a través de Wi-Fi o LTE. En respuesta directa a los comentarios de los clientes, ofrecerá la posibilidad de disfrutar del contenido en modo offline y streaming.*
> 
> 
> *Steam es una plataforma líder de entretenimiento digital que ofrece miles de juegos, películas y películas a millones de personas en todo el mundo. Para obtener más información, visite [www.steampowered.com](http://www.steampowered.com""")"""*
> 
> 
> 


¿Qué os parece este nuevo movimiento de Valve? ¿Pensais utilizar vuestros dispositivos móviles como pantalla para vuestros juegos? Déjanos tu opinión en los comentarios o en nuestros grupos de [Telegram](https://t.me/jugandoenlinux) y [Discord](https://discord.gg/VZGNYP).

