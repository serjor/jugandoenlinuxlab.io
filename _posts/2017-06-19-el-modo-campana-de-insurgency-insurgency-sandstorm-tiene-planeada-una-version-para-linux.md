---
author: Serjor
category: "Acci\xF3n"
date: 2017-06-19 20:46:32
excerpt: <p>New World Interactive regala el primer <span class="username u-dir" dir="ltr">@<b
  class="u-linkComplex-target">insurgencygame </b></span>y confirma el lanzamiento
  en nuestro sistema</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9b8bb3eb74bd3cd97bb3679c60950d61.webp
joomla_id: 380
joomla_url: el-modo-campana-de-insurgency-insurgency-sandstorm-tiene-planeada-una-version-para-linux
layout: post
tags:
- fps
- cooperativo
- insurgency
title: "El modo campa\xF1a de Insurgency, Insurgency:Sandstorm tiene planeada una\
  \ versi\xF3n para Linux (ACTUALIZADO)."
---
New World Interactive regala el primer @**insurgencygame** y confirma el lanzamiento en nuestro sistema

**ACTUALIZADO 13-8-18:** Con motivo de la proximidad de la salida oficial de Insurgency Sandstorm (el día 18 del próximo mes de Septiembre), sus desarrolladores [están regalando en Steam su anterior juego, Insurgency](https://steamcommunity.com/games/insurgency/announcements/detail/1703936520914355488) ("a secas"). Como sabreis, [New World Interactive](http://newworldinteractive.com/), creadores también del fantástico [Day of Infamy](https://www.humblebundle.com/store/day-of-infamy?partner=jugandoenlinux), lanzaron esta primera parte en 2014, usando para su desarrollo el conocido motor Source (Half Life 2, Portal, Team Fortress 2...) consiguiendo un juego multijugador de acción de lo más divertido y obteniendo unos muy buenos resultados en cuanto a éxito y ventas. Si quereis quedaros con él para siempre, en las próximas 48 horas podeis conseguirlo simplemente pulsando el botón de jugar e instalándolo (Enlace: <https://store.steampowered.com/app/222880/Insurgency/> ). Desde JugandoEnLinux.com os recomendamos encarecidamente que lo hagais, pues vale mucho la pena.


También y nuevamente gracias a [GamingOnLinux](https://www.gamingonlinux.com/articles/insurgency-sandstorm-is-looking-real-good-in-the-latest-videos-linux-version-should-come-in-the-first-couple-updates.12339), nos enteramos que **los desarrolladores continúan con el desarrollo de la versión para nuestro sistema**, el cual esperan tener listo en sus primeras actualizaciones, y continuando con la senda de dar suporte para nuestro sistema en sus juegos, tal y como lo hicieron en el pasado. Os dejamos con el ultimo video que han publicado en youtube donde podeis ver su jugabilidad:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/CljMKeczS98" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**NOTICIA ORIGINAL**: Gracias a [GamingOnLinux](https://www.gamingonlinux.com/articles/insurgency-sandstorm-looks-absolutely-stunning-still-planned-to-release-on-linux.9855) nos enteramos de que el modo campaña del juego Insurgency, que fue presentado en el pasado E3, muy probablemente tendrá versión para Linux, al menos es lo que tienen previsto los desarrolladores:


 



> 
> That's the plan
> 
> 
> — Insurgency (@insurgencygame) [17 de junio de 2017](https://twitter.com/insurgencygame/status/876165425419124736)



Esperemos que sus planes salgan bien, y podamos disfrutar del modo campaña, el cuál además se podrá jugar en modo cooperativo.


Os dejamos con el trailer presentación del E3 para ir abriendo boca:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/mym-5ne2_tc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/222880/37125/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


Y tú, ¿qué piensas de esta ampliación del juego original? Déjanos un comentario o pásate por nuestros grupos de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).


 

