---
author: Pato
category: Estrategia
date: 2021-07-27 17:43:24
excerpt: "<p>La compa\xF1\xEDa alega que desde la salida de la herramienta de Valve\
  \ la demanda ha disminuido</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/TotalWarTroy/troy-totalwarCANCELED.webp
joomla_id: 1332
joomla_url: feral-cancela-el-port-de-a-total-war-saga-troy-debido-a-proton
layout: post
tags:
- feral-interactive
- estrategia
- total-war
- troy
title: 'Feral cancela el port de A Total War Saga: Troy debido a Proton'
---
La compañía alega que desde la salida de la herramienta de Valve la demanda ha disminuido


Malas noticias para los que esperan un port nativo del próximo juego de Creative Assembly. Y es que tras el [anuncio](https://twitter.com/feralgames/status/1420032558390292490) de la llegada del juego a macOS por parte de Feral interactive, **sin ninguna mención a la versión de Linux** en twitter, Liam de [gamingonlinux.com](https://www.gamingonlinux.com/2021/07/feral-no-longer-porting-a-total-war-saga-troy-to-linux-citing-less-demand-since-proton/page=2#comments) les ha preguntado por dicha versión, a lo que la compañía ha respondido que **tras la exclusividad con la tienda de Epic habían puesto el port a nuestro sistema en alto**, y ahora ya no van a retomar el trabajo debido a que la demanda ha caído **debido a Proton**:




> 
> The Linux port was put on hold while TROY was exclusive to Epic, and we are not resuming development for the Steam release. We will continue to assess the feasibility of porting games to Linux, but there is generally less demand for native titles since Valve’s launch of Proton.
> 
> 
> — Feral Interactive (@feralgames) [July 27, 2021](https://twitter.com/feralgames/status/1420036822466502667?ref_src=twsrc%5Etfw)



Como podéis ver, la compañía, que tantos ports nos trajo en otro tiempo **no se cierra a estudiar futuros ports** a Linux, pero la herramienta de Valve les ha dejado sin buena parte de ese mercado. Aún así aseguran que [seguirán dando soporte](https://twitter.com/feralgames/status/1420041969611513856) a los juegos que ya han lanzado para Linux.


En este caso, hay que apuntar que si bien estamos empezando a ver las consecuencias de Proton "matando un port nativo", también hay que decir que **buena parte de los jugadores en Linux han acabado acostumbrándose a lanzar estos ports mediante proton** igualmente debido a diferencias de rendimiento y/o imposibilidad de jugar con/contra el mismo juego en otros sistemas. Por citar un ejemplo claro, muchos jugadores preferían lanzar Company of Heroes mediante Proton por que la diferencia de rendimiento era insignificante y sin embargo la versión nativa les impedía jugar contra otros sistemas, ya que **la versión de Feral Interactive nos forzaba a jugar solo contra los jugadores en Linux o Mac**, con lo que las opciones a encontrar partidas se reducían drasticamente. También hay que decir que buena parte de estos "problemas" tampoco venían por parte de Feral, ya que eran los própios motores de los juegos los que hacían imposible este tipo de cosas, por mucho que la companía quisiera.


Por otra parte, tampoco ayuda que los ports de Feral hayan sido por regla general, lanzados bastante tiempo después (a veces varios años) del lanzamiento de los juegos para otros sistemas, con lo que buena parte de los jugadores o ya lo han jugado mediante Proton o directamente el interés en jugar dichos juegos ha caído muchos enteros.


En cualquier caso, seguiremos la pista de Feral por si vuelven a lanzar juegos en Linux en un futuro.


¿Que te parece la cancelación de "A Total War Saga: Troy"? ¿crees que Feral hace lo correcto? ¿Piensas que es preferible una versión nativa aunque sea con un retraso en el tiempo o piensas que por el momento Proton es una buena opción?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://twitter.com/JugandoenLinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

