---
author: Pato
category: Ofertas
date: 2018-12-13 17:40:10
excerpt: <p>La conocida tienda "DRM Free" da el pistoletazo de salida a los descuentos</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4723ef876aca4c7cd452b3e97715d01b.webp
joomla_id: 932
joomla_url: good-old-games-comienza-sus-rebajas-de-invierno
layout: post
tags:
- gog
- rebajas
- ofertas
- gogcom
title: Good Old Games comienza sus rebajas de invierno
---
La conocida tienda "DRM Free" da el pistoletazo de salida a los descuentos

Como ya va siendo costumbre, la temporada de rebajas de invierno es inminente, tanto que ya mismo han dado comienzo las rebajas de invierno de GOG.com. La tienda "DRM Free Good Old Games" tiene en oferta un gran número de juegos disponibles para nuestro sistema favorito, y además está promocionando el juego [Full Throttle](https://www.gog.com/game/full_throttle_remastered) gratis aparte de las ofertas.


¡No te las pierdas!


[GOG.com](https://www.gog.com/)


Por ciero... ¿estás esperando también las rebajas de Steam? ¿Qué juegos piensas comprar?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [discord](https://discord.gg/ftcmBjD).

