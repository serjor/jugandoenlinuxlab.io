---
author: leillo1975
category: "Acci\xF3n"
date: 2021-02-23 14:43:38
excerpt: "<p>Importantes cambios en este juego centrado en el sigilo, <span class=\"\
  css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">@thedarkmod</span> .</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DARKMOD/DarkMod209.webp
joomla_id: 1282
joomla_url: the-dark-mod-alcanza-la-version-2-09
layout: post
tags:
- vulkan
- open-source
- idtech-4
- doom-3
- the-dark-mod
- broken-glass-studios
title: "\"The Dark Mod\" alcanza la versi\xF3n 2.09. "
---
Importantes cambios en este juego centrado en el sigilo, @thedarkmod .


 Ultimamente no nos llegan más que buenas noticias por parte de los proyectos de **Software Libre**, y si hoy mismo hablábamos del [excelente 0 A.D.]({{ "/posts/lanzada-la-alpha-24-de-0-a-d-xsayarsa" | absolute_url }}), ahora le toca a otro de los grandes juegos del Open Source, "[The Dark Mod](https://www.thedarkmod.com/main/)", que viene cargadito de novedades interesantísimas. Realmente esta nueva actualización se ha hecho pública hace ya unos días, y venimos con cierto retraso, pero como dice el refrán, "nunca es tarde si la dicha es buena".


Si nos seguís desde hace tiempo, sabreis que ya [no es la primera vez que os hablamos de este juego]({{ "/posts/publicada-nueva-version-the-the-dark-mod" | absolute_url }}) en la web, aunque ciertamente el juego merecería mucha más atención. Para quien no esté al tanto hay que decir que "The Dark Mod" **inició su andadura en 2009**, como reza su nombre, siendo un **mod de Doom 3**, aunque posteriormente, **en 2013, el juego se hizo independiente**, siendo esto gracias a la [liberación de IDTech 4](https://es.wikipedia.org/wiki/Id_Tech_4). Es curioso que este motor gráfico, muy superior a su predecesor no tenga más proyectos de software libre que lo usen, o al menos que yo sepa con mi limitado conocimiento (si sabeis de alguno más no dudeis en dejar un comentario).


En esta ocasión sus desarrolladores, **Broken Glass Studios**,  destacan la inclusión, como no de un **novedoso backend de renderizado que hace uso de Vulkan**, consiguiendo así un **enorme salto en el rendimiento del juego**. En esta nueva versión 2.09 encontraremos además los siguientes [cambios](https://www.thedarkmod.com/posts/the-dark-mod-2-09-has-been-released/):


**-Mejor rendimiento:** Se ha añadido un nuevo Backend que utiliza UBO, Persistent Mapping, Bindless textures y Multi-Draw. La "Aceleración del Frontend" (renderizado basado en trabajos multinúcleo basado en el BFG de Doom 3) ahora funciona como se esperaba. La compresión de los mapas normales ahora utiliza RGTC, lo que resulta en un menor almacenamiento, requisitos de VRAM y menos artefactos (los tiempos de carga deberían mejorar en futuras versiones de TDM cuando se añadan texturas RGTC precomprimidas). Se ha mejorado la carga de las texturas de fondo. Esto debería acortar los tiempos de carga y recarga.


**-Mejores efectos visuales:** Se ha añadido un nuevo Filtro de Enfoque. Puede hacer que el 70% de la escala de renderizado se parezca a la resolución nativa (más rendimiento).  También limpia algunas texturas borrosas para todas las escalas de renderizado. La iluminación del mapa de protuberancias es más suave, por lo que la iluminación no se apaga instantáneamente cuando se acerca a los bordes de los objetos ocluidos. La física de las partículas se ha mejorado sustancialmente tanto en precisión como en rendimiento. La pantalla completa sin bordes funciona correctamente en Windows. El agua y otros efectos de la interfaz gráfica que no respondían a la Gamma ahora se iluminan correctamente. Se han añadido correcciones de AMD e Intel para el renderizado SSAO.


**-Mejora de la jugabilidad**: Se ha añadido soporte experimental para el Gamepad. Creep funciona en el modo de ejecución continua.


**-Mejor mapeo:** Un nuevo sistema de enlace permite que The Dark Mod actúe como una vista previa en tiempo real de Dark Radiant. Las partículas rediseñadas tienen ahora una configuración mejorada. Se han creado correcciones automáticas para los modelos "pirateados por la rotación. Los prefabricados han visto numerosas correcciones y reorganización para hacerlos más fáciles de usar.


**-Mejor sonido**: La conversación y otros sonidos ya no se detienen al guardar o ir al menú principal. Se ha añadido una reverberación EFX a la "Misión 1: Un nuevo trabajo". Las lámparas de gas apagadas ya no hacen ruido cuando se apagan.


**-Nuevos activos:** Dragofer, Epifire, Bikerdude, LDAsh, Kingsal, Dram han añadido actualizaciones sustanciales de activos que van desde animaciones actualizadas de Werebeast, motores de vapor, luciérnagas


**-Más estable**: Un gran número de áreas de código frágiles han sido revisadas para arreglar fallos o prevenir probables fallos debidos al riesgo de división por cero, valores no inicializados o punteros colgantes (ajustes sobrantes) en entidades y funciones.


Ahora ya sabeis, solo queda que os lo bajeis y disfruteis de este gran juego, por supuesto de forma totalmente gratuita y libre. Para ello tan solo teneis que pasaros por la [sección de descargas de su web](https://www.thedarkmod.com/downloads/) y seguir las instrucciones que allí vereis. Aquí os queda el trailer oficial del juego. 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/brJqHnXmpgE" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


 


¿Te gustan los juegos de sigilo? ¿Habías jugado antes a "The Dark Mod"? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

