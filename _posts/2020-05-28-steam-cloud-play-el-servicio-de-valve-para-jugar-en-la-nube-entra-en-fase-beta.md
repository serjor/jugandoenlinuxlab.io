---
author: Pato
category: Noticia
date: 2020-05-28 08:49:37
excerpt: "<p>Valve est\xE1 conectando Steam al servicio Nvidia GeForce NOW para permitir\
  \ jugar en streaming</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/headers/steam-wallpaper.webp
joomla_id: 1224
joomla_url: steam-cloud-play-el-servicio-de-valve-para-jugar-en-la-nube-entra-en-fase-beta
layout: post
tags:
- steam
- beta
- valve
title: Steam Cloud Play, el servicio de Valve para jugar en la nube entra en fase
  beta
---
Valve está conectando Steam al servicio Nvidia GeForce NOW para permitir jugar en streaming


Desde hace unos días se vienen sucediendo noticias de que **Steam Cloud Play** está en desarrollo muy activo. En concreto, el conocido "*Steam data mine*r" **Pavel Djundik** anunciaba que se había encontrado código en el cliente beta de Steam que apuntaba a una especie de servicio en la nube con suscripciones, demos y código que aparentemente está vinculado con el servicio de Nvidia Geforce Now:




> 
> Steamworks documentation for [@Steam](https://twitter.com/Steam?ref_src=twsrc%5Etfw) Cloud Play (Beta) is now live, as expected with GFN as the first supported service. <https://t.co/eoo05qqzs5> <https://t.co/uQ7HUtrpQO>
> 
> 
> — Pavel Djundik (@thexpaw) [May 28, 2020](https://twitter.com/thexpaw/status/1265885598780440577?ref_src=twsrc%5Etfw)



 


Poco después, igualmente el mismo Pavel Djundik publicaba otro tweet apuntando a que el servicio Steam Cloud Play ya está activo **en fase beta**, y por lo que se puede leer en la documentación publicada en Steamworks las características del servicio están actualmente en desarrollo, sin especificar mucho más. En el comunicado se afirma que se están aceptando un número limitado de juegos en el servicio a la vez que se están añadiendo nuevas características y **se están adaptando las capacidades de los servidores** para los jugadores. 


A continuación, y para desconcierto de los que pensábamos que Valve se iba a volcar en su propio servicio de juego en la nube "en exclusiva", afirman que el [sic] **"primer servicio que están conectando a Steam para permitir a los usuarios jugar juegos desde su propia librería de Steam desde la nube es Nvidia GeForce NOW"**de forma que los clientes deberán seguir pagando los juegos en Steam, y los desarrolladores seguirán recibiendo sus pagos y comisiones como hasta ahora, pero sin embargo los clientes pueden tener que pagar por el servicio de streaming o el uso de servidores virtuales.


El comunicado continúa detallando la forma en que los desarrolladores pueden optar a entrar en el servicio de Steam Cloud Play, para lo cual deberán firmar un "añadido" a su contrato de Steam ya existente. Sinceramente, todo esto puede estar viniendo por los movimientos que se han estado viendo de editoras y desarrolladoras sacando sus juegos del servicio de Nvidia al no estar claro que el servicio está en consonancia con los contratos de distribución que ya están firmados con Valve.


Por otra parte, y tal y como afirman en el primer párrafo, parece que sí que Valve está trabajando en un servicio de ese mismo estilo, al afirmar que están en una fase beta, y que están adaptando la capacidad de los servidores.


Tampoco es descartable que Valve esté pensando en una jugada a dos bandas, ofreciendo la posibilidad de que terceras compañías accedan al catálogo del gigante de la distribución de videojuegos para servir de puente a los servicios de streaming de dichas compañías. 


¿Jugada redonda?, ¿solo para Nvidia? ¿Tendrá que ver algo de esto con Proton?


Ahora mismo todo son preguntas sin respuesta. Ya veremos la evolución que va tomando el servicio Steam Cloud Play. Puedes ver el [anuncio en Steamworks en este enlace](https://partner.steamgames.com/doc/features/cloudgaming).


¿Qué piensas del movimiento de Valve? ¿Piensas que al final terminarán lanzando su propio servicio de streaming? ¿Cómo te gustaría que fuera?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

