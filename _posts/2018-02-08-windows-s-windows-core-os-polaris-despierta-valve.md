---
author: Jugando en Linux
category: Editorial
date: 2018-02-08 17:17:40
excerpt: "<p>Los rumores y noticias sobre un pr\xF3ximo Windows \"cerrado\" se han\
  \ ido acrecentando en las \xFAltimas semanas. Se avecinan tiempos revueltos, o interesantes\
  \ en el mundo del PC</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e34f0e1c02fdcca3c7fab2bae7fa299d.webp
joomla_id: 638
joomla_url: windows-s-windows-core-os-polaris-despierta-valve
layout: post
title: "Windows S, Windows Core OS, Polaris... \xA1Despierta Valve!"
---
Los rumores y noticias sobre un próximo Windows "cerrado" se han ido acrecentando en las últimas semanas. Se avecinan tiempos revueltos, o interesantes en el mundo del PC

Si sois de los asiduos de las noticias tecnológicas en cuanto al mundo del PC ya sabréis de que estamos hablando. Para los que no, vamos a ponernos en antecedentes:


Desde hace más de un año se sabe que Microsoft andaba detrás de conseguir un sistema operativo cada vez más centrado en su propio ecosistema para intentar "cerrarlo" sobre su propia Store. La idea la quieren vender con la "excusa" de que así el sistema será más seguro, confiable y sobre todo será mucho más ligero en cuanto a ejecución de aplicaciones. Pero vamos a comenzar por el principio.


A estas alturas todos sabemos que esto saltó a la luz pública tras el anuncio de Windows 8 y el lanzamiento de UWP (Universal Windows Plattform). En aquel momento no pasó de "mera anécdota" ya que todas las aplicaciones que existían seguían corriendo sin problemas en el entonces flamante Windows 8. Pero las intenciones eran ya claras y evidentes para unos cuantos "avispados", que veían peligrar el ecosistema de aplicaciones que existían en ese momento de seguir por el camino que Microsoft parecía querer emprender. Entre ellos Gabe Newell, que llegó a tildar a Windows 8 como una catástrofe y motivó poco después la aparición de Steam en sistemas Linux y la creación de su propia distribución Linux para juegos, SteamOS.


La explosión de los sistemas "móviles" iOS y Android hizo que Microsoft llegase a la movilidad tarde. Windows RT para sistemas ARM fue un fracaso, pero la llegada de Windows 10 al escritorio y sobre todo Windows 10 S (ya descaradamente cerrado sobre la Store y UWP) fue la confirmación de que Microsoft quería un Sistema Operativo centrado sobre si mismo al estilo de lo que tenían Google o Apple, pues ha quedado más que demostrado que el futuro para ellos depende del Big Data y el tráfico de la información de sus propios usuarios, y ante la llegada de los Chromebooks han pisado el acelerador.


De ahí la "gratuidad" durante más de un año de Windows 10. De ahí el interés en "empujar" las actualizaciones a Windows 10, incluso a veces sin el permiso de sus usuarios. De ahí el "rastreo" y monitorización de los usuarios de un Windows 10 que ha pasado de ser un sistema operativo bajo licencia a ser un "Software como Servicio". (SaaS)


Con las [últimas informaciones sobre el desarrollo](https://www.muycomputer.com/2018/01/26/polaris-nuevo-windows-10/) de un próximo Windows 10 modular para ordenador bajo "Core OS" [con nombre en clave Polaris](https://www.windowscentral.com/windows-core-polaris?utm_source=wc_tw&utm_medium=tw_card&utm_content=53593&utm_campaign=social), que dejaría de lado las aplicaciones convencionales "Win32" para centrarse exclusivamente en la Store y su UWP y que por supuesto sería la opción por defecto para nuevos equipos, lo que antes eran "sospechas infundadas" pasan a ser prácticamente una confirmación en toda regla. Ni siquiera quedaría la opción que tiene Windows 10 S de migrar a otra versión de Windows de forma gratuita.


Podemos argumentar que "solo" sería para equipos de tipo "Cromebook", de pocos recursos, o destinados a movilidad, no a los juegos o aplicaciones profesionales. O que los que quieran ejecutar aplicaciones Win32 tendrán la opción de comprar Windows 10 Pro. Podemos incluso argumentar que el próximo Windows podrá "emular" las aplicaciones win32 o "permitir" la ejecución de otras Stores, desactivando eso sí las opciones de seguridad del sistema.


Cabe preguntarse: ¿Cuanto tiempo pasará hasta que Microsoft decida que todo tiene que pasar por sus manos? La tentación de cobrar por cualquier cosa que se ejecute bajo su sistema al igual que hacen Apple o Google es demasiado grande. ¿Que porcentaje de usuarios se saltarán las advertencias de seguridad para ejecutar aplicaciones fuera de la Store de Windows?


Con la posibilidad certera ya de que Windows tendrá su próxima versión cerrada sobre su Store, Steam y otras tiendas "menores" como Origin, Uplay o GoG están en una posición poco menos que comprometida. Es casi inevitable que vean como la base de usuarios irá poco a poco mermando a la vez que las aplicaciones UWP vayan popularizándose gracias a la aplastante superioridad en cuanto a usuarios de PC de los sistemas de Microsoft. Es por esto que estas y otras compañías están ya tomando cartas en el asunto, y viendo que el movimiento gira en torno a sistemas cerrados han tomado básicamente dos caminos: El streaming o "juego bajo demanda" tipo Netflix, o el desarrollo para sistemas alternativos: Steam para Linux o SteamOS. Y es aquí donde Valve tiene que comenzar a mover ficha si quiere "depender de si misma". Necesita mover ficha a no mucho tardar, o puede encontrarse con que ha llegado demasiado tarde.


Es necesario tener una alternativa viable y atractiva al usuario, apoyada en grandes juegos y grandes franquicias. Es hora de que en Valve recuerden quienes son, de donde vienen y lo que han conseguido, revisando si es preciso su propio código ético pues la competencia no piensa pararse a pedir permiso. Ni siquiera a pedir perdón.


Es cierto que han levantado prácticamente ellos solos el juego actual en Linux. Es cierto que [siguen contratando desarrolladores](https://www.phoronix.com/scan.php?page=news_item&px=Daniel-Joins-Valve-Driver) para Mesa. Pero no es menos cierto que SteamOS no consiguió la base de usuarios que esperaban y el porcentaje de usuarios en Linux [está estancada](http://store.steampowered.com/hwsurvey?platform=combined) en torno al 1,5 o 2% en el mejor de los casos. Si no son capaces de dar un golpe encima de la mesa (redundancia?) podemos encontrarnos en una situación cuanto menos comprometida con desarrolladoras pasándose a UWP y dejando "seco" a Steam a medio plazo.


Sea lo que sea que estén tramando en Valve, ya sea un remodelado de Steam, una nueva edición de Steam Machines, una nueva y renovada versión de SteamOS (¿con Wine incrustado? Por soñar...)  o nuevos juegos de los que "venden" sistemas lo tendremos que ver a no mucho tardar, por que la jugada es de órdago a la grande y ya no es a largo plazo.


¡Despierta Valve!


 


*Esto es un artículo de opinión, y como tal puede que no estés de acuerdo con lo que aquí exponemos, o puede que tengas otras ideas. Nos las puedes contar en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).*

