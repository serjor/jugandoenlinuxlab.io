---
author: Pato
category: Ofertas
date: 2017-06-07 12:05:15
excerpt: <p>La tienda celebra sus ofertas de temporada estival</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7ae73efe4c50580e061849ef2d5b5ea4.webp
joomla_id: 362
joomla_url: gog-comienza-las-rebajas-de-verano-con-suculentos-descuentos
layout: post
tags:
- gog
- ofertas
- summer-sale-2017
title: GOG comienza las rebajas de verano con suculentos descuentos
---
La tienda celebra sus ofertas de temporada estival

Un año más comienzan las rebajas de verano para [gog.com](https://www.gog.com/) (Good Old Games) la tienda de CD Projekt popular por su política de publicación sin DRM, y al igual que el año pasado se ha adelantado a las de Steam.


En concreto son más de 1500 juegos los rebajados con porcentajes de hasta el 90% y donde hay rebajados verdaderos juegazos para Linux. Solo por destacar algunos, tenemos disponibles [Pillars of Eternity](https://www.gog.com/game/pillars_of_eternity_hero_edition) al 60%, [Broforce](https://www.gog.com/game/broforce) al 75%, o [Shadow Tactics: Blades of the Shogun](https://www.gog.com/game/shadow_tactics_blades_of_the_shogun) al 25%. Las rebajas de verano de GOG estarán activas durante dos semanas.


Si quieres ver la lista completa de las ofertas para Linux, puedes verla en [este enlace](https://www.gog.com/games?system=lin_mint,lin_ubuntu&price=discounted&sort=date&page=1).


¿Y las rebajas de Steam? pues [según los rumores](https://www.reddit.com/r/Steam/comments/6aec94/steam_summer_sale_2017_date/) todo apunta a que será entre el 22 de Junio y el 5 de Julio, aunque aún no se sabe oficialmente. Eso sí, en cuanto se sepa algo lo sabrás puntualmente aquí, en jugandoenlinux.com.


¿Que te parecen las ofertas de gog.com? ¿Piensas pillar algo? ¿Esperarás a las rebajas de Steam?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

