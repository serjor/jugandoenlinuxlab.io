---
author: leillo1975
category: Estrategia
date: 2019-05-23 11:05:50
excerpt: <p>@feralgames acaba de anunciar el soporte de este juego de @CAGames</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f3b7859ccdd2d7e0b388a9167e2f3d9a.webp
joomla_id: 1046
joomla_url: disponiible-total-war-three-kingdoms-para-linux-steamos
layout: post
tags:
- feral-interactive
- estrategia
- sega
- total-war
- creative-assembly
- three-kingdoms
title: 'Disponiible "Total War: THREE KINGDOMS" para Linux/SteamOS'
---
@feralgames acaba de anunciar el soporte de este juego de @CAGames

El pasado Septiembre [os anunciábamos](index.php/homepage/generos/estrategia/8-estrategia/981-total-war-three-kingdoms-llegara-a-linux-steamos-la-proxima-primavera) que como es costumbre, Feral Interactive se encargaría del port a nuestro sistema de esta nueva saga de juegos Total War. Gracias a ello, hoy al fin ha llegado el lanzamiento del juego y podemos disponer de él al igual los usuarios de Windows y Mac, desde el primer día. El anuncio nos llegaba  por parte de Feral a través del correo electrónico, y también de las redes sociales como podeis ver en este tweet:



> 
> Total War: THREE KINGDOMS is out now for macOS and Linux, same day as Windows.  
>   
> Buy from the Feral Store to stake your claim: <https://t.co/2F1HFXrBa6>   
>    
> Consult the minisite for background on the heroes at the heart of the game: <https://t.co/VULSimJPYR> [pic.twitter.com/hg4qBHavI9](https://t.co/hg4qBHavI9)
> 
> 
> — Feral Interactive (@feralgames) [May 23, 2019](https://twitter.com/feralgames/status/1131515129563111425?ref_src=twsrc%5Etfw)


  





Según la descripción que nos ha facilitado Feral nos encontraremos lo siguiente:




*Total War: THREE KINGDOMS es el primer juego de la renombrada serie de estrategia que se ubica en medio de los conflictos épicos de la antigua China.*


*China en el año 190 d. C., un país grande pero fracturado, reclama un nuevo liderazgo. Es momento de unirlo bajo tu mandato, forja la próxima gran dinastía y construye un legado que perdurará a lo largo de los tiempos.*


*Elige entre 12 legendarios señores de la guerra y conquista el Reino del Medio con una combinación fascinante de tácticas por turnos y batallas colosales en tiempo real. Recluta generales, trata con otros poderosos señores de la guerra y supera a tus rivales en cuatro frentes: militar, tecnológico, político y económico.*


***LAS LEYENDAS MAS GRANDES DE CHINA***   
 *Forja un imperio como uno de los 12 señores de la guerra legendarios y recluta comandantes valientes, guerreros poderosos y estadistas excepcionales para que sean tus generales.*


***SISTEMA GUANXI***   
 *Inspirado en guanxi, el concepto chino de interrelaciones dinámicas, THREE KINGDOMS adopta un nuevo enfoque a los personajes. Cada señor de la guerra y general tiene su propia personalidad, motivaciones y relaciones distintas, inspirándolos a luchar contra sus rivales, vengarse de sus amigos caídos o traicionar a un líder no deseado.*


***CAMPAÑA Y BATALLAS INTEGRADAS***   
 *Las acciones en batalla afectan las relaciones de tus generales con su señor de la guerra y sus compañeros, haciendo que la campaña por turnos y las batallas en tiempo real de THREE KINGDOMS estén más conectadas que en cualquier otro juego de Total War hasta la fecha.*


***RESTAURA LA ARMONÍA DE LA ANTIGUA CHINA***   
 *Lucha a través de selvas subtropicales, montañas cubiertas de nieve y magníficos lugares emblemáticos, desde la Gran Muralla China hasta el río Yangtze.*


Para poder disfrutar de Total War: THREE KINGDOMS, necesitais cumplir al menos los siguientes requisitos técnicos:



+ **SO:** Ubuntu 18.04 64-bit
+ **Procesador:** 3.4GHz Intel Core i3-4130
+ **Memoria:** 6 GB de RAM
+ **Gráficos:** 2GB AMD R9 285 (GCN 3rd Generation), 2GB Nvidia GTX 680
+ **Almacenamiento:** 60 GB de espacio disponible
+ **Notas adicionales:**
	- \* Requires Vulkan
	- \* Nvidia requires 418.56 or newer drivers.
	- \* AMD requires Mesa 19.0.1. ​
	- \* AMD GCN 3rd Gen GPU's include the R9 285, 380, 380X, Fury, Nano, Fury X.
	- \* Intel GPUs are not supported at time of release.
	- \* Other modern drivers and distributions are expected to work but are not officially supported.


Podeis comprar "Total War: THREE KINGDOMS" en la [tienda de Feral](https://store.feralinteractive.com/es/mac-linux-games/threekingdomstw/), en la **[Humble Store (patrocinado)](https://www.humblebundle.com/store/total-war-three-kingdoms?partner=jugandoenlinux)**, o en [Steam](https://store.steampowered.com/app/779340/Total_War_THREE_KINGDOMS/). Os dejamos con el trailer de lanzamiento del juego:  
  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/NMGN0ZZn6D8" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>




