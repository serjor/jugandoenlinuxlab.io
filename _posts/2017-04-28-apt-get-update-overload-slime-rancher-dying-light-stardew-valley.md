---
author: Pato
category: Apt-Get Update
date: 2017-04-28 16:57:53
excerpt: <p>Llegamos de nuevo a Viernes y ya toca un Update</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/00c636e00bee0ba03b364841363f738b.webp
joomla_id: 300
joomla_url: apt-get-update-overload-slime-rancher-dying-light-stardew-valley
layout: post
title: Apt-Get Update Overload & Slime Rancher & Dying Light & Stardew Valley...
---
Llegamos de nuevo a Viernes y ya toca un Update

Llevamos un par de semanas de lo más tranquilo sin apenas novedades que destaquen, y las que destacan os las traemos como siempre aquí, en Jugando en Linux. Pero como la semana pasada no hubo Update, lo haremos hoy. Comenzamos:


Desde [www.gamingonlinux.com:](http://www.gamingonlinux.com)


- 'Overload' el título de acción en entorno con 6 grados de libertad ha recibido un update con suculentas mejoras. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/overload-the-really-great-six-degree-of-freedom-shooter-has-a-major-update.9550).


- 'Slime Rancher' y 'Terraria' también han recibido su ración de actualizaciónes con diversas mejoras. Puedes verlo en [este enlace](https://www.gamingonlinux.com/articles/massive-slime-rancher-update-released-that-introduces-rewards-for-your-monies.9532) y en [este enlace](https://www.gamingonlinux.com/articles/terraria-update-adds-new-languages-improved-linux-stability-support-for-high-resolutions-and-more.9533) respectivamente.


- Techland dice que ha resuelto los problemas multijugandor cooperativo en 'Dying Light'. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/techland-claim-to-have-finally-fixed-linux-co-op-in-dying-light.9539).


- 'Stardew Valley' también ha recibido una buena actualización que trae mas idiomas, mejores controles etc... Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/stardew-valley-12-is-out-with-more-languages-better-gamepad-support-and-more.9562).


No hay mucho más destacable. Vamos con los vídeos de la semana:


Overload


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/l6rhz_I8KWE" width="640"></iframe></div>


Dying Light


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/jYReqCgJZvM" width="640"></iframe></div>


Slime Rancher 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/TkWDGUyigfU" width="640"></iframe></div>


Stardew Valley


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/ot7uXNQskhs" width="640"></iframe></div>


 


Y esto es todo por este Apt-Get. La semana que viene volveremos con más novedades. Mientras tanto, puedes contarme lo que te parezca sobre estos u otros juegos que se nos puedan haber quedado en el tintero en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

