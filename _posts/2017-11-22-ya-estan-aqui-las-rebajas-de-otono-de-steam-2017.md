---
author: Pato
category: Ofertas
date: 2017-11-22 18:10:47
excerpt: "<p>\xA1Corred insensatos!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3b3137a08b8bf22969ae75f6bfeed67f.webp
joomla_id: 549
joomla_url: ya-estan-aqui-las-rebajas-de-otono-de-steam-2017
layout: post
tags:
- steamos
- steam
- rebajas
title: "\xA1Ya est\xE1n aqu\xED las rebajas de Oto\xF1o de Steam 2017!"
---
¡Corred insensatos!

Ya se sabía desde hace un tiempo. Ya os lo contamos aquí en Jugando en Linux, y ¡hoy por fin tenemos ya las rebajas de Otoño de Steam 2017! ¿tenéis preparadas las carteras?


Desde hoy mismo hasta el próximo día 28 de Noviembre tendremos grandes descuentos en un amplio catálogo de juegos. ¿Cuales estás esperando con más ganas?


Por mi parte, hay un buen número de juegos que me gustaría comprar... te dejo algunos de mi lista personal:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/374050/" width="646"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/380150/71040/" width="646"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/415200/" width="646"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/396750/" width="646"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/362960/133810/" width="646"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/573660/" width="646"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/270880/" width="646"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/272330/36651/" width="646"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/73010/7420/" width="646"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/214490/51156/" width="646"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/237990/36614/" width="646"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/314790/" width="646"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/291650/61326/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Que te parece mi lista? ¿Cuales son los juegos que más estás esperando comprar en estas rebajas? ¿Has comprado alguno ya?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

