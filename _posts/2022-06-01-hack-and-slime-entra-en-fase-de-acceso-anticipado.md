---
author: leillo1975
category: Plataformas
date: 2022-06-01 16:22:05
excerpt: "<p>@BULLWAREsocial y SpoonBox Studio dan un paso m\xE1s en el desarrollo\
  \ de @<span class=\"highlight\">Hack</span><span class=\"highlight\">And</span><span\
  \ class=\"highlight\">Slime . </span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HackAndSlime/HackandSlimeEA.webp
joomla_id: 1471
joomla_url: hack-and-slime-entra-en-fase-de-acceso-anticipado
layout: post
tags:
- indie
- metroidvania
- espanol
- hack-and-slime
- spoonbox
- bullware-soft
title: '"Hack and Slime" entra en fase de Acceso Anticipado '
---
@BULLWAREsocial y SpoonBox Studio dan un paso más en el desarrollo de @HackAndSlime . 


 Si leeis habitualmente nuestras noticias sabreis que desde principios de este año 2022, [estamos siguiendo]({{ "/tags/hack-and-slime" | absolute_url }}) el desarrollo de este videojuego de las compañías Castellonenses [Bullware Soft](https://bullwaresoft.com/) y [SpoonBox](https://www.spoonboxstudio.com), Tras completar algunas etapas iniciales, y lanzar una demo pública, finalmente han llegado a la **versión Alpha 3.0.0.0**, donde encontraremos una buena ristra de [novedades, mejoras y arreglos](https://store.steampowered.com/news/app/1773700/view/3293832755489317651https://store.steampowered.com/news/app/1773700/view/3293832755489317651), tal y como podeis ver aquí mismo:


* *Se han modificado las tiradas del drop de equipamiento, ahora es más común que caiga equipamiento con rareza superior a normal en la mazmorra.*
* *Se ha añadido un codigo de control para que las funciones de F1 y F12 se encuentren deshabilitadas*
* *Se ha corregido un error que permitia equipar el mismo objeto muchas veces.*
* *Se ha añadido un plugin de Steam para incluir compatibilidad con sus funciones.*
* *Se ha ampliado la capacidad del bolsillo para guardar mas equipamiento de 9 a 18 slots.*
* *Se ha actualizado el panel de estadísticas para mejorar la lectura de la información.*
* *En el menú del inventario se han añadido ventanas con la información del equipamiento y de los objetos del bolsillo.*
* *Se ha reajustado la probabilidad de caida de equipamiento en la mazmorra para hacer las partidas más dinámicas y divertidas.*
* *Se ha corregido el gráfico de la esfera de drop del "casco normal".*
* *Se ha corregido un error que permitía el drop de más de una pieza de equipamiento por tirada.*
* *Se ha corregido un error que afectaba a efectos de sonido en el inventario.*
* *Se ha cambiado el color de los puntos de skills disponibles en el instructor para mejorar su lectura.*
* *Corrección de algunos errores de diseño en varios niveles.*
* *Se ha mejorado el rendimiento del menu de estadisticas.*
* *Se ha mejorado el rendimiento del menu del poblado.*
* *Ahora se pueden reparar piezas del equipamiento en la herrera.*
* *Ahora en la herrera se puede vender equipamiento.*
* *Se ha añadido un botón en la herrera para reparar todo el equipo a la vez.*
* *Se ha actualizado el HUD en algunos comercios de Humble Villaje para mostrar el oro, orbes y orbes refinados.*
* *Se ha añadido el nombre de la planta en el HUD de combate.*
* *Se han añadido iconos al HUD de combate para avisar al jugador del estado y durabilidad del equipamiento.*
* *Se han corregido muchas traducciones en inglés y en español.*
* *Se ha corregido un error respecto a la zona de colisión de Corid cuando se jugaba con control-pad.*
* *Se ha corregido un error que impedía contabilizar el tiempo jugado total.*
* *Se ha corregido un error relacionado con el cálculo de precios de las ofrendas del santuario.*
* *Ahora el menú de estadísticas e inventario funciona correctamente desde el pueblo.*
* *Se ha corregido un error que creaba gráficos de agua cuando Corid lanzaba el Dash*
* *Se corregido el muestreo del botón del portal al jugar con mando (solo Humble Village)*
* *Ahora las piezas de nivel 1 no requieren fuerza para ser equipadas.*
* *Muchos ajustes relacionado con estadisticas de Corid y de los monstruos.*
* *Se han actualizado los gráficos de "botón arriba" en puertas, pozos y templos.*
* *Se ha modificado el comportamiento del "Ogrog" para que sea más amenazante.*
* *Ahora Corid no aumenta su daño físico de forma pasiva al subir de nivel.*


También nos indican que tienen algunos cambios pendientes:


* *Completar el Alijo.*
* *Añadir la nueva versión de Humble Village en la siguiente actualización.*
* *Añadir el nuevo bioma "The Dungeons" a Warpmazon.*
* *Añadir un panel de misiones.*
* *Mejorar mucho el comportamiento de los enemigos existentes.*
* *Añadir un nuevo tipo de enemigo en la siguiente actualizacion.*
* *Incluir de forma efectiva algunas estadísticas que no están funcionando en la versión actual.*
* *Mejorar el diseño y la jugabilidad en "Slime King".*
* *Muchas mejoras de balanceo.*


Además tenemos que comentaros que el juego tiene un precio más que interesante mientras esté en Acceso Anticipado, ya que cuesta 3.99€, pero si lo compras antes del 8 de Junio, podrás beneficiarte de un descuento del 15% (**3.39€**):


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1773700/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


Os volvemos a comentar que en JugandoEnLinux **estamos involucrados en el proceso de testeo de la versión nativa** de este juego, y que por supuesto os seguiremos informando sobre las novedades que vaya presentando. Os dejamos con este video que grabamos hace unos meses:


 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/AQ7QySR7oKU" style="display: block; margin-left: auto; margin-right: auto;" title="YouTube video player" width="780"></iframe></div>

