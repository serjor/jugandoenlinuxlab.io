---
author: Pato
category: "Acci\xF3n"
date: 2017-06-09 14:49:17
excerpt: <p>El juego es obra del estudio mexicano "wetheforce"</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c3698948bbef8e01b61b372d4a29088b.webp
joomla_id: 366
joomla_url: randall-un-juego-de-accion-y-aventuras-ya-esta-disponible-en-linux-steamos-pero-sin-traduccion
layout: post
tags:
- accion
- indie
- aventura
- 2d
title: "Randall un juego de acci\xF3n y aventuras ya est\xE1 disponible en Linux/SteamOS\
  \ pero sin traducci\xF3n"
---
El juego es obra del estudio mexicano "wetheforce"

La verdad es que nunca está de más el saber de buenos juegos, y este 'Randall' [web oficial] desde luego tiene buena pinta. Para todos aquellos a los que les gusta la acción y aventuras en 2D y plataformeo puede ser una buena opción, si bien es cierto que en este género los usuarios de Linux ya vamos bien servidos:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/Q7cuzr9qhSw" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 



> 
> *El mundo está bajo una dictadura totalitaria gobernado por una corporación minera que creció demasiado. Controla la única fuente de combustible y ha esclavizado a la mayoría de la población con técnicas de control mental. Solo unos pocos han escapado, y otros han sido atrapados por la policía militar.*
> 
> 
> *Randall tendrá que encontrar su propio camino a través de hordas de enemigos si quiere sobrevivir. Su posibilidad de gobernar la mente de otros le abre un nuevo mundo de posibilidades. Explora el mundo en esta aventura de tipo "metroidvania" y desarrolla todo tu potencial para derrotar a tus enemigos más poderosos.*
> 
> 
> 


En cuanto a sus requisitos mínimos son:


* SO: Ubuntu 12.04 o superior
* Procesador: Intel Core I5 2.9Ghz
* Memoria: 4 MB de RAM
* Gráficos: Intel HD Graphics 4000 Series
* Almacenamiento: 2 GB de espacio disponible


Randall es obra del estudio mexicano "wetheforce", lo que hace que sea curioso que el juego no esté traducido al español. Si esto no es impedimento para ti, lo tienes disponible en su página de Steam con un 10% de descuento por su lanzamiento hasta el día 13:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/603840/" width="646"></iframe></div>


 


 

