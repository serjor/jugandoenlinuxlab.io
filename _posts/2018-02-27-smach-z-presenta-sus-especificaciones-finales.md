---
author: leillo1975
category: Hardware
date: 2018-02-27 09:43:17
excerpt: <p>Ya se puede reservar</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/55d4420dd6cd56f2e756dd2f75804f62.webp
joomla_id: 660
joomla_url: smach-z-presenta-sus-especificaciones-finales
layout: post
tags:
- steamos
- steam
- portatil
- steam-machines
title: Smach Z presenta sus especificaciones finales (ACTUALIZADO 2)
---
Ya se puede reservar

**ACTUALIZACIÓN 15-3-18**: Desde ahora mismo ya se puede reservar este dispositivo pues acaban de abrir los encargos.  Las compras realizadas con antelación tienen un **descuento del 10%,**  siendo cancelables en cualquier momento y estarán **disponibles en el mes de Septiembre**. Los medios de pago son los habituales incluyendo Paypal y bitcoin. En la pagina además podremos comprar complementos tales como camisetas, fundas de goma o un Stick de conexión 4G. S**e podrá encargar en varios colores** (Midnight Blue, Silver, Radioactive Green, Indigo, Pearl White, Fire Red o Deep Black).


La consola vendrá por defecto con ¿**SmachOS**? (Linux), pudiendo instalarle Windows (quien de los aquí presentes querría tal cosa) . Si quereis encargarla o ver más información podeis pasaros por su [página web](https://www.smachz.com/shop/). Desde JugandoEnLinux.com deseamos a sus creadores toda la suerte del mundo con este proyecto y esperamos verla en acción pronto en cuanto estén listas las primeras unidades.


![faq consoles.png 800x400 q85 subsampling 2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SmachZ/faq-consoles.png__800x400_q85_subsampling-2.webp)




---


**ACTUALIZACIÓN 5-3-18**: Se acaba de presentar la [página oficial](https://www.smachz.com/) de la Smach Z donde hay una cuenta atrás para reservar la "consola" (a partir del 15 de Marzo) y más información sobre esta. También se puede ver un nuevo video sobre este dispositivo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/V74zEOy4HPs" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**NOTICIA ORIGINAL:** No se si lo recordais, pero hace ya bastante tiempo [os hablamos](index.php/homepage/hardware/item/47-el-micropc-portatil-para-jugar-smach-z-vuelve-a-kickstarter) de esta "consola" portátil que usaba SteamOS. Pues ahora volvemos a la carga con este tema a cuento de que, por fin, **se han completado las especificaciones finales** de este dispositivo. Si recordais, Smach Z había conseguido una financiación bastante importante a través de una bastante exitosa campaña de [Kickstarter](https://www.indiegogo.com/projects/smach-z-the-handheld-gaming-pc-games#/), donde hasta ahora se han recaudado unos 683.000$, una cantidad que la verdad no está nada mal. Ultimamente, y debido a la falta de noticias sobre el proyecto **se había especulado con la posibilidad de que el proyecto tuviese problemas o fuese abandonado**. Al parecer problemas con uno de los proveedores de componentes han lastrado el desarrollo y acrecentado los costes, por lo que el proyecto sufrirá un retraso y finalmente las primeras unidades llegarán al público a finales de este año, siendo los que han contribuido los primeros pudiéndo disfrutar este hardware unos meses antes, concretamente en el mes de Mayo.


![SMACH CORE 680x342](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SmachZ/SMACH-CORE-680x342.webp)


*Imagen tomada de https://liliputing.com/*


En cuanto al hardware, la Smach Z dispondrá finalmente de un **procesador AMD Ryzen V1605B**, un procesador embebido de última hornada que duplicará las capacidades iniciales de la consola. Gracias a este procesador dispondremos de **4 núcleos y 8 hilos**, una velocidad de **2 a 3.6Ghz** en modo turbo, 1.1Ghz de velocidad en la gráfica (**Radeon Vega 8**), soporte de memoria de 2400Mhz, y un consumo que podrá ir de 12 a 25W. Como veis, unas capacidades más que apetitosas, sobre todo teniendo en cuenta que es un hardware portátil. En cuanto al resto de componentes dependerán de si escogemos la versión normal o la Pro. Podeis ver las caracteristicas en la siguiente tabla (sacada de [liliputing.com](https://liliputing.com/2018/02/smach-z-handheld-gaming-pc-specs-finalized-ryzen-v1605b-8gb-ram.html)):


![SmachZSpecs](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SmachZ/SmachZSpecs.webp)


Si os fijais en el último punto, pone que el Sistema operativo puede ser Linux o Windows 10, sin mencionar SteamOS en ningún momento. Esperemos poder aclarar esto proximamente. También se podrá comprar con más memoria (16GB) o capacidad de alamacenamiento (256GB) si lo especificamos en la web cuando podamos realizar el pedido. En cuanto a la **batería** se estima que tenga una duración de **5 horas** en juego, lo cual está genial.


Los precios de la Smach Z serán de 699€ la versión normal, y 899€ la versión Pro, siendo estos sensiblemente mayores a los anunciados en un principio, 299€ y 499€ repectivamente. La compañía aclara que los precios anteriores eran solo para los bakers de Kickstarter, y que los precios para el público general serían superiores, además está también provocado por la actualización del SoC de AMD.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/-Fr1C-TzcLM" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 Como muchos de vosotros sabreis, Smach Z es un proyecto con diseño y firma española, y será fabricado en nuestro pais enteramente. Es muy agradable ver que iniciativas como esta también salen de nuestro territorio. Dejanos tus impresiones sobre este proyecto en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 


 

