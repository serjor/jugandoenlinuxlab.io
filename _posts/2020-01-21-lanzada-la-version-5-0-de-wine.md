---
author: Pato
category: Noticia
date: 2020-01-21 19:33:19
excerpt: "<p>Esta nueva versi\xF3n viene con gran cantidad de novedades y mejoras.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/WineLogo.webp
joomla_id: 1150
joomla_url: lanzada-la-version-5-0-de-wine
layout: post
title: "Lanzada la versi\xF3n 5.0 de Wine"
---
Esta nueva versión viene con gran cantidad de novedades y mejoras.


Esta tarde se ha anunciado la disponibilidad de la nueva versión estable de **Wine 5.0**, el *"wrapper"* para ejecutar programas y juegos de Windows sobre nuestro sistema favorito.


Este nuevo lanzamiento viene con una gran cantidad de mejoras que hasta ahora estaban en las ramas de desarrollo, con lo que ahora las podremos disfrutar en la rama estable.


**A destacar entre las mejoras:**


- Integración de FAudio como una mejor implementación de XAudio2 y en parte gracias a CodeWeavers / Valve como parte de los esfuerzos de Proton.  
  
- Soporte Vulkan 1.1.  
  
- Soporte para instalar controladores plug-and-play.  
  
- Muchas DLL ahora creadas como archivos PE por defecto.  
  
- Las texturas comprimidas DXTn / S3 Texture Compression soportan por defecto ahora que las patentes en torno a ese formato de textura comprimida han expirado.  
  
- Soporte de NT-kernel spin-locks.  
  
- Implementaciones basadas en Futex de sincronización primitiva.  
  
- Varias mejoras DirectWrite.  
  
- Soporte para claves criptográficas ECC.  
  
- Mayor soporte para las API de Windows Media Foundation.  
  
- Soporte para un Wine-Mono compartido para ahorrar espacio en lugar de necesitar esta implementación de código abierto .NET por prefijo Wine.  
  
- Soporte Unicode 12.0 y 12.1.  
  
- Mejor enumeración de las salidas de pantalla, particularmente para configuraciones de Linux de monitores múltiples alrededor de Xinerama.  
  
- Implementación inicial del Servicio HTTP (HTTP.sys) como reemplazo del uso de la API Winsock por IIS para un mejor rendimiento que la API de Windows Sockets.  
  
- Mejor compatibilidad con depuradores de Windows.  
  
- Mejor compatibilidad con LLVM MinGW y mejoras de compilación cruzada de WineGCC por separado.


Esperemos ver todas estas mejoras en la próxima actualización de **Protón**, que presumiblemente Valve lanzará ya con Wine 5.0.


Toda la información en el [post del anuncio oficial](https://www.winehq.org/pipermail/wine-announce/2020-January/000476.html).

