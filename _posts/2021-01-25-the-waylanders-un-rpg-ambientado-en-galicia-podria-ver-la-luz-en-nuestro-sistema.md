---
author: leillo1975
category: Rol
date: 2021-01-25 09:13:35
excerpt: "<p><span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">@ferprietoprado</span>\
  \ , director del estudio gallego <span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo\
  \ r-qvutc0\">@GatoStudioGames nos comenta que est\xE1 en sus planes. <br /></span></p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Waylanders/WaylandersGameplay.webp
joomla_id: 1271
joomla_url: the-waylanders-un-rpg-ambientado-en-galicia-podria-ver-la-luz-en-nuestro-sistema
layout: post
tags:
- rpg
- the-waylanders
- gato-studio
- celta
- galicia
title: "\"The Waylanders\", un RPG ambientado en Galicia, podr\xEDa llegar a Linux "
---
@ferprietoprado , director del estudio gallego @GatoStudioGames nos comenta que está en sus planes.   



 Si os soy sincero hace años que sigo el desarrollo de este videojuego, primero por la lógica proximidad, pues se trata de un **estudio gallego**; segundo por idioma, pues es uno de los pocos estudios que **traducen al gallego sus productos**; y tercero, por que la temática de su último videojuego me interesa una barbaridad, pues en el encontraremos un **mundo basado en las leyendas celtas, así como en la edad media de mi tierra, Galicia**. Con estos ingredientes no es extraño que yo y muchos otros le hayamos echado el ojo.


Gracias a una conversación via twitter de la cuenta [@GalegoTwitch](https://twitter.com/GalegoTwitch), finalmente pudimos ponernos en contacto con [Fernando Prieto](https://twitter.com/ferprietoprado), director de [Gato Studio](http://gatosalvajestudio.com/home/), donde a través de una pregunta a través de mi cuenta personal obtuvimos finalmente respuesta, tal y como podeis ver en el siguiente tweet (en Gallego):



> 
> Está previsto, pero máis adiante. En Early Access teremos que pulir primeiro todo, e despóis comezar as versións. Levar agora todo el paralelo sería moi compricado. Pasa o mesmo coa versión para Mac.  
> Se nada sae mal, a versión final poderá tener versión Linux
> 
> 
> — Fernando Prieto (@ferprietoprado) [January 24, 2021](https://twitter.com/ferprietoprado/status/1353405202422853637?ref_src=twsrc%5Etfw)


  





Creo que se entiende bastante bien, pero por si acaso os lo traduzco:


*"Esta previsto, pero para más adelante. En Acceso Anticipado tenemos que pulir primero todo, y después empezar las versiones. Llevar ahora todo en paralelo sería muy difícil. Pasa lo mismo con la versión para Mac. Si nada sale mal, la versión final podrá tener versión de Linux."*


Para quien no conozca esta compañía, les recordamos que **son los responsables de la saga de aventuras gráficas [AR-K](https://store.steampowered.com/app/269890/ARK/?l=spanish), así como de [Bullshot](https://store.steampowered.com/app/436530/Bullshot/?l=spanish)**,un shooter en 2D con aspecto y jugabilidad retro. Estos juegos que en el pasado también recalaron en nuestro sistema con versión nativa forman parte del bagaje de este estudio. Pero es sin duda este Waylanders su proyecto más grande y ambicioso, para el cual se han embarcado en la contratación de muchas más personas para un trabajo más especializado y de mayor calidad, contando incluso con la **colaboración de personas muy conocidas en el sector**, como **Mike Laidlaw** (Mass Effect, Dragon Age) como guionista y consultor, o [Inon Zur](https://es.wikipedia.org/wiki/Inon_Zur) (Saga Fallout, Dragon Age, Prince of Persia), como creador de su banda sonora. El proyecto pretende mirar de tu a tu a grandes del género como Dragon Age Origins, Baldur's Gate, o incluso el aclamado The Witcher, juegos en los que busca inspiración e influencias. Según la **descripción oficial** que podemos encontrar en Steam, en "The Waylanders" encontraremos un juego de Rol con las siguientes caracteríticas:


* **Viaja entre dos épocas** - Viaja a través del tiempo entre dos eras distintas: celta y medieval. Conoce a viejos aliados y descubre reencarnaciones mientras viajas al futuro y percibes grandes cambios en los lugares que ya conoces
* **Una historia como ninguna otra** - The Waylanders fusiona los mitos celtas de Galicia con una emocionante aventura de fantasía
* **Fieles compañeros** - Entabla amistades con nueve compañeros, mortales e inmortales, que te acompañarán en tus viajes a través de la Galicia celta y medieval. Descubre su pasado, supera sus misiones especiales y ten un romance con tus favoritos
* **Múltiples lugares para explorar en dos épocas** - Explora más de 50 regiones llenas de misiones, secretos y enemigos tanto en la era celta como en la medieval
* **Múltiples finales** - Las decisiones tomadas en un período tienen terribles consecuencias en el futuro. Los jugadores sentirán el impacto de sus decisiones de formas sorprendentes e impactantes
* **Combate profundo y táctico** - Las habilidades especiales y las múltiples clases disponibles proporcionan tácticas de combate tácticas profundas y variadas. Las diferentes opciones para crear tu grupo de aventureros permiten realizar formaciones especiales como la Punta de Flecha, el Orbe, el Gólem o la Torre de Mago
* **Completa personalización del personaje** - Elige entre seis clases básicas y especialízate aún más con una de las 30 clases avanzadas. Crea tu personaje a partir de cinco razas jugables basadas en la mitología celta, cada uno con orígenes diferentes
* **Banda sonora inmersiva** - Disfruta de la música de The Waylanders, compuesta por el ganador del premio Emmy y tres veces nominado al BAFTA, Inon Zur
* **Modo foto** - Captura la emoción y la magia del mundo de The Waylanders mientras lo descubres con las funciones fotográficas recién añadidas


Para seros sinceros, en bastantes ocasiones y hace bastante tiempo, tratamos de contactar por múltiples medios con el estudio para preguntar por la disponibilidad del juego en nuestro sistema, pero hasta ahora nunca tuvimos respuesta. Celebramos que finalmente podamos daros la noticia de que este interesante juego pueda llegar en un futuro a nuestro sistema. Como veis el juego tiene los mimbres necesarios para ser un superventas, y está actualmente en Acceso Anticipado, pudiendo encontrarse a la venta tanto en [Steam](https://store.steampowered.com/app/957710/The_Waylanders/) como en [GOG](https://www.gog.com/game/the_waylanders?pp=a93c168323147d1135503939396cac628dc194c5) . Os dejamos con el trailer de lanzamiento en esta última tienda:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/lt20zvGwTZE" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


¿Qué os parece la temática de este "Waylanders"? ¿Conocíais la trayectoria del estudio gallego Gato Salvaje? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

