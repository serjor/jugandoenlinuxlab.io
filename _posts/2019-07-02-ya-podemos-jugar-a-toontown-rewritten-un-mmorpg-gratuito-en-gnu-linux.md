---
author: leillo1975
category: Rol
date: 2019-07-02 13:22:27
excerpt: <p>El juego se puede instalar facilmente mediante un paquete Snap.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/550d4948377bf6f7b418a7e39b30d860.webp
joomla_id: 1070
joomla_url: ya-podemos-jugar-a-toontown-rewritten-un-mmorpg-gratuito-en-gnu-linux
layout: post
tags:
- gratis
- mmorpg
- snap
- disney
title: Ya podemos jugar a "ToonTown Rewritten", un MMORPG gratuito en GNU/Linux.
---
El juego se puede instalar facilmente mediante un paquete Snap.

Gracias a nuestro amigo **@lordgault** y a lo que acabamos de leer en [Ubunlog,](https://ubunlog.com/toontown-rewritten-instalar-snap-ubuntu/) hemos conocido que tenemos disponible en la tienda Snapcraft "**ToonTown Rewritten**", un videojuego de rol masivo (MMORPG) que retoma el ya  en 2013 "desconectado" juego original de Disney "ToonTown Online". Para ello una serie de voluntarios decidieron coger el toro por los cuernos y continuar manteniendo la esencia del juego recreando lo visto en el juego original. Pero la cosa no solo se quedó ahí, sinó que han seguido desarrollándolo implementando **nuevas características**, nuevos **eventos**,  y se actualiza regularmente dotándolo de **nuevos contenidos**.


Para quien no lo conozca o recuerde, "**Disney’s Toontown Online**" era un juego orientado a las familias que comenzó su andadura hace 16 años, y permitía a los **usuarios de todas las edades** jugar conectados. Para ello se creaba un personaje "Toon" con el que conjuntamente con el resto de jugadores podíamos disfrutar de montones de minijuegos y actividades en las que debíamos combatir a los "Cogs", los malos de turno que querían convertir la ciudad en su último proyecto de negocios. Como comentamos arriba, en ToonTown Rewritten, la esencia es la misma y podremos seguir jugando de la misma forma. Además desde su relanzamiento ha tenido un **éxito bastante reseñable**, teniendo registrados más de 1.4 millones jugadores, con miles de ellos jugando a la vez.


Es importante decir que el juego no tiene relación alguna con Disney, más que que se basa en un producto de esta compañía, siendo **completamente gratuito**, sin necesidad de realizar micropagos, subscripciones o teniendo que soportar anuncios, quedando el **contenido y funciones enteramente a disposición del jugador**. Además el equipo de TTR no acepta ningún tipo de donaciones, siendo ellos mismos los que soportan los gastos de mantener el juego vivo, algo encomiable en estos tiempos.


Para instalar este juego en nuestro querido GNU/Linux, tenemos desde hace algún tiempo en la Snapcraft Store, un **paquete Snap** que nos permitirá instalar de una forma muy sencilla este divertido videojuego, incluyendo para ello modificaciones en el [ejecutable oficial](https://www.toontownrewritten.com/play), como el soporte para Python3 y adaptaciones para el mejor funcionamiento en este sistema de paquetería. Para ello podremos simplemente **pinchar en "Install"** en la [página de esta tienda](https://snapcraft.io/toontown) o escribir en una terminal:


`sudo snap install toontown`


Recordad que es necesario que vuestra sistema tenga soporte para snap. En caso contrario podeis instalarlo facilmente siguiendo las [instrucciones para cada una de las distribuciones](https://docs.snapcraft.io/installing-snapd). Os dejamos con el video del anuncio de Toontown Rewritten:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/oLmzui5STfE" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>  
  



¿Estais listos para probar este juego? ¿Qué os parece la filantrópica iniciativa de sus desarrolladores? Deja tu opinión en los comentarios, en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

