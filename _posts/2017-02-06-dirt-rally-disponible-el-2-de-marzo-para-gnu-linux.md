---
author: leillo1975
category: Carreras
date: 2017-02-06 14:55:00
excerpt: "<p>Feral Interactive nos trae otro juego de conducci\xF3n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ce11944b98d0742eead2b016a253afd2.webp
joomla_id: 204
joomla_url: dirt-rally-disponible-el-2-de-marzo-para-gnu-linux
layout: post
tags:
- feral-interactive
- conduccion
- dirt-rally
title: 'DIRT Rally disponible el 2 de Marzo para GNU/Linux (ACTUALIZADO: Requisitos)'
---
Feral Interactive nos trae otro juego de conducción

Noticia bomba. Feral Interactive acaba de anunciar a través de twitter y su [página web](https://www.feralinteractive.com/en/news/727/) que proximamente lanzará DIRT Rally para GNU/Linux, concretamente el 2 de marzo. También han abierto un [minisitio para el juego](https://www.feralinteractive.com/es/linux-games/dirtrally/). Aquí teneis el tweet donde se anuncia:



> 
> Challenge yourself to the most authentic rally game ever made with DiRT Rally, coming to Linux on March 2.  
>   
> Trailer: <https://t.co/YesxsQPI5o> [pic.twitter.com/FeWwwVv4By](https://t.co/FeWwwVv4By)
> 
> 
> — Feral Interactive (@feralgames) [February 2, 2017](https://twitter.com/feralgames/status/827124492593950722)



Esta era una noticia que se veia venir, pues hace mucho tiempo que había rumores que Feral estaba portándolo, incluso en SteamDB había entradas que así lo apuntaban. No se han dado más detalles sobre el port, pero esperamos que con el tiempo suelten algo más de información al respecto. En caso de que esto ocurra, JugandoEnLinux.com os informará cumplidamente de ello.


Para quien no lo conozca, DIRT Rally es un juego, que como su nombre indica está enfocado en el mundo de los Rallys, pero no como en anteriores entregas de DIRT, sino desde un aspecto más realista. Si buscais un poco podreis ver que el juego tiene unas [excelentes críticas en internet](http://www.metacritic.com/game/pc/dirt-rally). Ahora a contar los dias para pdoer disfrutar de este cañonazo de juego. Os dejo con el trailer del anuncio


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Pp9qUjtD8Zc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


 Podeis dejar cualquier impresión sobre este anuncio en los comentarios, o en nuestros caneles de [Telegram](https://telegram.me/jugandoenlinux) o [Discord.](https://discord.gg/ftcmBjD)


**ACTUALIZACIÓN 1:** Feral nos confirma en el siguiente Tweet que el juego utilizará **OpenGL** y no la API de Vulkan:



> 
> [@JugandoenLinux](https://twitter.com/JugandoenLinux) OpenGL.
> 
> 
> — Feral Interactive (@feralgames) [February 2, 2017](https://twitter.com/feralgames/status/827180437021732864)



 


**ACTUALIZACIÓN 2:** Feral acaba de anunciar en [t](https://twitter.com/feralgames/status/828604425098911744)[witter](https://twitter.com/feralgames/status/828604425098911744) y [publicar en su página](https://www.feralinteractive.com/es/news/732/) los requisitos técnicos necesarios para jugar a Dirt Rally en Linux. En este caso, afortunadamente, nos encontramos antes un título que podrán disfrutar los poseedores de gráficas AMD con drivers libres superiores a Mesa 13.0.2. Concretamente los requisitos son los siguientes: 


Mínimos


Intel Core i3-4130T o AMD FX6300


Ubuntu 16.10 64bit or SteamOS 2.0 : 2.110


8GB de RAM


NVIDIA 650ti/AMD R9 270 de 1GB\* o superior.


 


Recomendados


Intel Core i7-4770


8GB de RAM


NVIDIA 970 de 4GB\* o superior.


\*Las tarjetas gráficas Nvidia requiere la versión de controlador 370.28. Lás gráficas AMD e Intel necesitan MESA 13.0.2 o posterior.


 


FUENTES: [Twitter](https://twitter.com/feralgames/status/827124492593950722), [Feral Interactive](https://www.feralinteractive.com/en/news/727/)

