---
author: Serjor
category: Rol
date: 2017-10-30 21:34:37
excerpt: "<p>Un RPG de tintes cl\xE1sicas con mec\xE1nicas actuales</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/78afaacbba3bca41a1ce9f675afbba4f.webp
joomla_id: 514
joomla_url: the-untold-legacy-inicia-su-campana-en-kickstarter
layout: post
tags:
- kickstarter
- rpg
- the-untold-legacy
title: "The Untold Legacy inicia su campa\xF1a en Kickstarter"
---
Un RPG de tintes clásicas con mecánicas actuales

Leemos en Twitter que la gente de [Iconic Games](http://www.iconicgames.net/) presenta su campaña de [Kickstarter](https://www.kickstarter.com/projects/1214305480/the-untold-legacy?ref=4k8jas) para su juego [The Untold Legacy](http://www.the-untold-legacy.com/):


 



> 
> We are proud to present the [#UntoldLegacy](https://twitter.com/hashtag/UntoldLegacy?src=hash&ref_src=twsrc%5Etfw) [#Kickstarter](https://twitter.com/hashtag/Kickstarter?src=hash&ref_src=twsrc%5Etfw) <https://t.co/Jp3FuQ2Cqx> [#indiedev](https://twitter.com/hashtag/indiedev?src=hash&ref_src=twsrc%5Etfw) [#gamedev](https://twitter.com/hashtag/gamedev?src=hash&ref_src=twsrc%5Etfw) [#madewithunity](https://twitter.com/hashtag/madewithunity?src=hash&ref_src=twsrc%5Etfw) [#madewithspine](https://twitter.com/hashtag/madewithspine?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/DLlT9OWzAG](https://t.co/DLlT9OWzAG)
> 
> 
> — Matt P. Vile (@Iconic_Games) [26 de octubre de 2017](https://twitter.com/Iconic_Games/status/923585849501077505?ref_src=twsrc%5Etfw)


 


Por lo que podemos ver este The Untold Legacy es un heredero espiritual de los clásicos Zelda y recuerda mucho a otros como Ittle Dew, ya que estamos ante un Action-RPG en el que tendremos que recorrer el mundo de Loomia, dónde tendremos que craftear nuestros items, ganar experiencia, resolver puzzles o superar mazmorras.


La verdad es que tiene muy buena pinta, y visualmente luce muy bien, esperemos que esta campaña llegue a buen puerto, porque da la sensación de que el juego merecerá mucho la pena, sobretodo porque un RPG de corte tan clásico siempre es agradecido en nuestro sistema.



Necesitas un navegador compatible con HTML5 para ver este vídeo.



Y tú, ¿Participarás en esta campaña? Cuéntanos qué te parece este The Untold Legacy en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o Discord(<https://discord.gg/ftcmBjD>).

