---
author: Pato
category: Arcade
date: 2017-01-20 13:24:22
excerpt: <p>2Awesomwe Studio le da una nueva vuelta de tuerca a los juegos de este
  estilo</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d632334130a2b9d194362b7d857b88bf.webp
joomla_id: 184
joomla_url: dimension-drive-un-interesante-shoot-em-up-llegara-el-proximo-16-de-febrero-en-acceso-anticipado
layout: post
tags:
- indie
- proximamente
- acceso-anticipado
- arcade
title: "'Dimension Drive' un interesante \"Shoot 'Em Up\" llegar\xE1 el pr\xF3ximo\
  \ 16 de Febrero en Acceso Anticipado"
---
2Awesomwe Studio le da una nueva vuelta de tuerca a los juegos de este estilo

Dimension Drive [[web oficial](http://www.dimensiondrive.com/)] es un juego de tipo "Shoot 'em up" donde tendrás que disparar a todo lo que se mueva. Hasta aquí nada del otro mundo, salvo que esta vez tendrás que jugar... ¡con dos pantallas a la vez!.


Es decir, jugarás en un universo multidimensional donde jugarás en dos campos de batalla "paralelos" y tendrás la posibilidad de teletrasportarte de uno a otro según tu criterio conforme se va desarrollando la partida. Este nuevo "concepto" suena de verdad interesante, y más viendo vídeos que el estudio ya ha publicado del juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/2tQw9K_beXQ" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Como no nos quedaba claro si el juego llegaría a Linux, y si lo haría al mismo tiempo que en otros sistemas preguntamos al estudio... ¡y nos lo han confirmado!



> 
> [@JugandoenLinux](https://twitter.com/JugandoenLinux) No... la versión de Linux está confirmada, disponible y funcionando hace ya tiempo. Saldrá también el 16 de Febrero :)
> 
> 
> — 2Awesome Studio (@2AwesomeStudio) [20 de enero de 2017](https://twitter.com/2AwesomeStudio/status/822440933270683648)



 


'Dimensional Drive' estará disponible en Linux el próximo 16 de Febrero en acceso anticipado. Puedes reservarlo diréctamente en su [página web](http://www.dimensiondrive.com/) o verlo en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/394430/" width="646"></iframe></div>


 ¿Qué te parece el concepto de este 'Dimensional Drive'? ¿Piensas jugarlo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

