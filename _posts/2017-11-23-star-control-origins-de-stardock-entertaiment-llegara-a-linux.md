---
author: leillo1975
category: Arcade
date: 2017-11-23 09:16:21
excerpt: <p>Los desarrolladores de Ashes of Singularity lo incluyen en la lista de
  plataformas a desarrollar.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ac9eaae1d1c1dba787e6549c691345ac.webp
joomla_id: 550
joomla_url: star-control-origins-de-stardock-entertaiment-llegara-a-linux
layout: post
tags:
- arcade
- stardock-entertaiment
- star-control-origins
title: "Star Control: Origins, de Stardock Entertaiment, llegar\xE1 a Linux."
---
Los desarrolladores de Ashes of Singularity lo incluyen en la lista de plataformas a desarrollar.

Como podeis ver en la noticia actualizada hace un momento sobre [Ashes of Singularity](index.php/homepage/generos/estrategia/item/468-ashes-of-the-singularity-escalation-podria-tener-version-para-linux), donde confirmabamos la llegada de este último a Linux, también llegará a nuestro sistema otro desarrollo de la misma compañía. Se trata de Star Control: Origins y por lo que se puede adivinar en los videos y la información ofrecida en [Steam](http://store.steampowered.com/app/271260/Star_Control_Origins/) y su [página oficial](https://www.starcontrol.com/), parece ser un arcade de batallas espaciales con posibilidades de  personalización. El [tema de los foros de Steam](http://steamcommunity.com/app/271260/discussions/1/2381701715713320871/#c1480982971179825250) donde se confirmaba esta noticia decía exactamente lo siguiente:




> 
>  *[Draginol](http://steamcommunity.com/id/draginol)  [desarrollador]  7 NOV a las 2:55*
> *Targeted platforms:*
>  
> *Mac*
> *Linux*
> *PS4*
> *Switch*
> *XBox One*
>  
> *The order in which they're focused on will be based on ROI of course.*
> 



 


Esta es la descripción oficial del juego en Steam:



> 
> *¡Explora una galaxia viviente repleta de civilizaciones alienígenas, nuevos y exóticos mundos, emocionantes combates y una trama compleja y elaborada que deberás desentrañar!*
> 
> 
> *¡Tras dos décadas, se acerca un nuevo juego de Star Control! Star Control: The Ur-Quan Masters te llevó a un rico universo habitado por los malévolos kohr-ah, los misteriosos orz y los asustadizos spathi. Star Control: The Kessari Quadrant te trasladó a una nueva sección de la galaxia para enfrentarte a los eternos. Ahora, en Star Control: Origins, tendrás que salvar la Tierra y resolver el misterio del multiverso.*
> 
> 
> *Características:*
> ------------------
> 
> 
> * *Interactúa con alienígenas tan malvados como ridículos.*
> * *Explora un universo viviente que, sinceramente, no acaba de creerse que vayas montado en esa... cosa.*
> * *Recorre mundos exóticos y sorprendentes en busca de reliquias, tesoros y artefactos de los precursores.*
> * *Ábrete camino por miles de planetas en un universo con una extensa historia que abarca cientos de miles de años.*
> * *¡Diseña tus propias naves o descárgalas de Steam para crear tus flotas y combate con ellas en batallas de la flota!*
> * *¡Habla directamente con el equipo de desarrollo, publica tus ideas y forma parte de algo increíble!*
> 
> 
> 


 


La verdad, es de agradecer que Stardock Entertaiment se haya involucrado en lanzar sus juegos para GNU/Linux. Desde JugandoEnLinux.com os iremos informando de las noticias que vaya generando este juego.Os dejamos con el trailer del juego para que podais haceros una idea de como es:


 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/a-hhgTxiB8Y" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


  

Si estás interesado en este juego o tienes cuelquier tipo de opinión sobre él, déjala en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).



