---
author: leillo1975
category: "Simulaci\xF3n"
date: 2019-01-30 14:45:39
excerpt: "<p class=\"ProfileHeaderCard-screenname u-inlineBlock u-dir\" dir=\"ltr\"\
  ><span class=\"username u-dir\" dir=\"ltr\">Ya est\xE1 disponible la beta del juego\
  \ espacial de @EGOSOFT <br /></span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8a545da277e2e99414e6557c4344e1c0.webp
joomla_id: 960
joomla_url: x4-foundations-contara-con-una-beta-para-linux-proximamente-actualizado-2
layout: post
tags:
- estrategia
- simulacion
- x4-foundations
- egosoft
title: "\"X4:Foundations\" tendr\xE1 una beta para Linux proximamente (actualizaci\xF3\
  n 2)"
---
Ya está disponible la beta del juego espacial de @EGOSOFT   


**(Actualización-2 26/02/2019)**


Según podemos ver en el [foro de soporte](https://forum.egosoft.com/viewtopic.php?f=180&t=414524&sid=a1cd585d8efccc1cbe77967db13a9313) de X4 Foundations de Egosoft el juego ya está disponible para descargar en la rama beta. No hay aún requisitos para Linux/SteamOS publicados, pero si sabemos que es necesario tener una gráfica capaz de soportar Vulkan y el soporte para Vulkan instalado.


Si quieres saber mas o probar la beta, puedes visitar su [página de la tienda](https://store.steampowered.com/search/?snr=1_4_4__12&term=x4+foundations) en Steam.




---


**(Actualización 26/02/2019)**


Egosoft [publicó ayer tarde la actualización](https://steamcommunity.com/games/392160/announcements/detail/1768130575201859779) de **X4 Foundations** a la versión 2.00 que introduce notables cambios en el juego, tanto en el contenido con la posibilidad de construir y gestionar equipamiento y naves en tu propia base, como el rebalanceo de la economía y la lucha con la modificación de parámetros del armamento y poder ofensivo de diferentes naves. Además, introduce **la integración de Steam Workshop** para facilitar el poder desarrollar e instalar mods desde Steam.


Pero lo realmente importante del comunicado es que anuncian **la llegada de la versión beta para Linux en los próximos días**:


"*en los próximos días lanzaremos la versión beta para Linux de X4: Foundations. Si tienes un PC gaming con Linux y eres capaz de ayudarnos a testear el juego en esta plataforma, por favor pruébalo!. Los problemas específicos de Linux pueden ser reportados en el [hilo de soporte de Linux](https://forum.egosoft.com/viewtopic.php?f=180&t=414524) en el foro de Egosoft."*


Estaremos atentos a las novedades para informar cuando se publique la beta.




---


**(Artículo original)**


Si recordáis, poco después del verano [os informábamos de la intención de la compañía Egosoft de lanzar una beta para Linux con Vulkan a finales de noviembre](index.php/homepage/generos/simulacion/14-simulacion/988-x4-foundations-llegaria-a-linux-steamos-el-30-de-noviembre-en-forma-de-beta). Como podreis comprobar en su página de [Humble Bundle](https://www.humblebundle.com/store/x4-foundations?partner=jugandoenlinux), seguimos meses después sin el codiciado soporte para nuestro sistema. Este pasado lunes, **con motivo de la actualización 1.60,** [la desarrolladora comunicaba](https://steamcommunity.com/games/392160/announcements/detail/1712958942380447969)**,** entre otras muchas cosas que seguía manteniendo **la intención de lanzar la prometida beta**, pero en este caso sin establecer una fecha si no el **periodo comprendido entre las actualizaciones 2.0** (la siguiente versión que llegaría el próximo mes) **y la 2.5.**


Para los que no conozcáis la Saga X de videojuegos, debéis saber que llevan la friolera de 18 años en el candelero, ofreciendo a los jugadores unos **completos simuladores espaciales con grandes toques de estrategia**. Se trata de una serie de videojuegos con una profundidad y dificultad elevadas, y que abarcan muchos aspectos de juego, a parte de lo que es el pilotaje de todo tipo de naves espaciales. En el juego podremos construir estaciones espaciales y fábricas, mejorar nuestras aeronaves, comerciar con diferentes recursos, investigar nuevas tecnologías, etc... todo ello con el fin de expandir nuestra raza para convertirla en un imperio que influya en el universo. Desde nuestro sillón podremos ordenar a golpe de click todas estas acciones y tomar parte en la lucha a bordo de nuestras naves espaciales.


Como veis se trata de un juego muy completo (y complejo), pero que no por ello deja de ser divertido. Esperemos que más pronto que tarde podamos volver a disfrutar de este nuevo episodio de esta saga en nuestros ordenadores, al igual que sus anteriores partes ([X3: Gold Edition](https://www.humblebundle.com/store/x3-gold-edition?partner=jugandoenlinux), [X3: Terran Confict](https://www.humblebundle.com/store/x3-terran-conflict?partner=jugandoenlinux) y [X3: Albion Prelude](https://www.humblebundle.com/store/x3-albion-prelude?partner=jugandoenlinux)). Os dejamos disfrutando con un gameplay oficial del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/N6s9sXjqCME" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Sois jugadores de la saga X de videojuegos? ¿Jugareis también a X4: Foundations? Contesta en los comentarios o en charla con nosotros sobre el juego en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

