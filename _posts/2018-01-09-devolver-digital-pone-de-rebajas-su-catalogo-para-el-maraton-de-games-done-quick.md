---
author: Pato
category: Ofertas
date: 2018-01-09 10:45:21
excerpt: "<p>Parte de los beneficios ir\xE1n para una fundaci\xF3n de prevenci\xF3\
  n contra el c\xE1ncer</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ead47d36e50510c345eb86a3568f763f.webp
joomla_id: 598
joomla_url: devolver-digital-pone-de-rebajas-su-catalogo-para-el-maraton-de-games-done-quick
layout: post
tags:
- ofertas
- devolver-digital
title: "Devolver Digital pone de rebajas su cat\xE1logo para el marat\xF3n de \"Games\
  \ Done Quick\""
---
Parte de los beneficios irán para una fundación de prevención contra el cáncer

Ya hemos hablado en varias ocasiones de Devolver Digital, la editora que nos ha traído la mayor parte de su catálogo a Linux/SteamOS y de la que esperamos que nos siga trayendo sus futuros proyectos. Pero lo que nos ocupa ahora es la oferta que están llevando a cabo gracias al maratón de [Games Done Quick](https://gamesdonequick.com/). Gran parte de su catálogo está ahora mismo rebajado con hasta un 80% de descuento incluyendo Serious Sam BFE o The Talos Principle.


Recordar que un porcentaje de los beneficios que consigan irá destinado a una fundación de prevención contra el cáncer, por lo que además que obtener los títulos con suculentos descuentos estaremos ayudando a una buena obra social.


Tenéis todas las ofertas en [devolverdonequick.com](http://store.steampowered.com/sale/devolverdonequick/)

