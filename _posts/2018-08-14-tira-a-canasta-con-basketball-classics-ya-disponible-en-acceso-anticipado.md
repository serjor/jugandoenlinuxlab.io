---
author: Pato
category: Deportes
date: 2018-08-14 10:04:08
excerpt: "<p>Baloncesto cl\xE1sico con fuerte sabor retro... Ding Dong!!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1990ac324e26b7e716caf3c87ed6c195.webp
joomla_id: 829
joomla_url: tira-a-canasta-con-basketball-classics-ya-disponible-en-acceso-anticipado
layout: post
tags:
- deportes
- indie
- acceso-anticipado
title: Tira a canasta con 'Basketball Classics', ya disponible en acceso anticipado
---
Baloncesto clásico con fuerte sabor retro... Ding Dong!!

Cuando hablamos de las "carencias" que tenemos en cuanto a géneros de juego en Linux, hay un referente que siempre sale a la luz: Los juegos deportivos. Y cuando hablamos de juegos deportivos hablamos de los grandes del género como los juegos de fútbol o baloncesto que tanta afición (y jugadores) atraen. En este sentido, el baloncesto es si cabe aún mas sangrante al no disponer de un título siquiera de referencia, pero hoy nos hacemos eco de '**Basketball Classics**', un juego que aunque está aún en desarrollo viene al rescate en nuestro sistema favorito obra de un pequeño estudio indie "Namo Camo" [[web oficial](http://namogamo.com/)].


Se trata de un juego en apariencia no muy "grande" pero que hace recordar a los grandísimos juegos del deporte de la canasta **de la era de los 8 bits**, trayéndonos una jugabilidad clásica de tipo mas o menos arcade con solo tres botones de acción pero con la jugabilidad del mejor deporte de la canasta.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/N4kW4y5zyTw" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


**Sobre Basketball Classics:**


*5 contra 5 Pro Basketball ha nacido de la época dorada de los juegos deportivos. Este sucesor espiritual de los juegos de 8 bits cierra la brecha entre los juegos de simulación retro y los de hoy.*


*Acción de arcade sudorosa y desplazamiento lateral  
Juego simplificado de 3 botones  
Décadas de listas con más de 175 equipos  
Más de 900 jugadores con atributos individualizados  
Sensacionales escenas  
El Modo historia para 1 jugador desbloquea equipos secretos  
Multijugador local desenfrenado  
Sensaciones Retro en abundancia  
Sin moneda virtual,sin paga para ganar, sin micro transacciones para siempre.*


El juego aún está en desarrollo y el estudio acaba de lanzar [una nueva actualización](https://steamcommunity.com/games/819630/announcements/detail/1693803421761697008) con nuevas características y opciones, y aunque se trata de un juego de acceso anticipado al parecer está ya bastante avanzado. Revisando las características, posee una buena cantidad de equipos clásicos y jugadores míticos.


Por otra parte, el juego llamó la atención recientemente en la [campaña de Atari VCS](index.php/homepage/hardware/item/884-atari-vcs-consigue-superar-los-10-000-apoyos-y-se-acerca-a-los-3-000-000) en Indiegogo, ya que al parecer es uno de los juegos que estarán disponibles en la próxima consola de Atari  y aparecía en el video promocional de la campaña.


Si viviste la época dorada del baloncesto NBA, puede ser un buen juego para desahogar los calores de este verano. ¿Quien no recuerda a Ramon Trecet o Andrés montes en aquellas ya míticas sesiones nocturnas de "basket"?


Si te gusta el baloncesto y quieres saber mas, ahora tienes 'Basketball Classics' en acceso anticipado en su [página de Steam](https://store.steampowered.com/app/819630/Basketball_Classics/). ¡Por fin baloncesto! ¡DING DONG!

