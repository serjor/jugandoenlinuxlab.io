---
author: Pato
category: Noticia
date: 2019-06-06 16:48:55
excerpt: <p>Repasamos precios, requisitos y juegos</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c79100bd9a1c81231de8bfb99f98f395.webp
joomla_id: 1059
joomla_url: te-explicamos-lo-que-sabemos-de-google-stadia-tras-su-retransmision
layout: post
tags:
- stadia
- google
title: "Te explicamos lo que sabemos de Google Stadia tras su retransmisi\xF3n"
---
Repasamos precios, requisitos y juegos

Pues ya se acabó la escueta pero intensa retransmisión de Stadia Connect, donde se han desvelado algunas de las incógnitas que pudiéramos tener respecto al servicio de Google. Te contamos:


* Stadia viene **como un servicio de juego en la nube** a través de tu conexión de internet, para lo que necesitarás una **velocidad mínima estable de 10 megas** como mínimo para jugar en resolución 720p, 20 megas estables para jugar como mínimo a 1080p y 35 megas o más estables como mínimo para jugar a 4K.
* El servicio Stadia estará dividido en dos modalidades: **Stadia Pro y Stadia Base**.
* **Stadia Pro costará 9,99€ al mes**, lo que dará acceso a jugar con resolución hasta 4K, 60FPS, sonido 5.1, Juegos gratuitos periódicamente (empezando por Destiny 2: The Collection) y descuentos exclusivos para juegos específicos en Stadia. Esta modalidad **estará disponible este mismo año 2019**.
* **Stadia Base será gratuito** y dará acceso a jugar con resolución hasta 1080p, 60 FPS, sonido stereo, sin juegos gratuitos periódicamente y sin descuentos exclusivos para juegos específicos de Stadia. Esta modalidad **estará disponible a lo largo de 2020**.
* Como habréis podido deducir de los dos apartados anteriores, solo la modalidad Stadia Pro te dará acceso a juegos periódicos gratis, al estilo de la Epic Game Store, por lo que **si queremos jugar a los que no estén disponibles gratuítamente tendremos que pagarlos en la Tienda de Google Stadia**. Si te das de baja en la modalidad de Stadia Pro seguirás pudiendo a jugar a los juegos que hayas comprado en tu cuenta que pasará a la modalidad Stadia Base (con las características obvias de Stadia Base), eso sí, perderás el acceso a los juegos periódicos gratuitos y los descuentos.
* Stadia estará disponible en un **paquete llamado "Founder's Edition"** que incluirá 3 meses de Stadia Pro, un "Buddy pass" para regalar a un amigo que le dará acceso a Stadia Pro durante 3 meses, el mando de Stadia exclusivo en color azul noche, un Chromecast Ultra para poder ejecutar Stadia en tu televisor y la exclusividad de poder elegir tu nombre Stadia, **todo por 129€** y estará disponible en noviembre de 2019 si lo reservas ahora.  
![Stadiabundle](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/Stadiabundle.webp)
* En cuanto a los juegos, aquí tienes una imagen que vale mas que mil palabras:  
![Stadiagames](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/Stadiagames.webp)
* Aparte de en el Chromecast Ultra y algunos dispositivos de la gama Pixel, **podrás ejecutar el servicio Stadia en cualquier dispositivo que sea capaz de ejecutar el navegador Google Chrome**, es decir, móviles, tablets y ordenadores. Además se podrá utilizar cualquier mando compatible en estos dispositivos aunque se advierte que para sacar el máximo potencial será necesario usar el mando Stadia.


Y de momento eso es todo. Que no es poco. Si quieres saber más sobre Stadia, o reservar tu pack Founder's Edition pueder visitar lu página en la [Tienda de Google](https://store.google.com/product/stadia_founders_edition?srp=/magazine/stadia).


¿Qué te parece el Servicio de Google Stadia? ¿Te parece atractivo el precio? ¿Probarás a ejecutar Stadia en tu Pc con Linux a través del navegador Chrome?


Si tienes alguna pregunta o comentario, puedes decírmelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux).

