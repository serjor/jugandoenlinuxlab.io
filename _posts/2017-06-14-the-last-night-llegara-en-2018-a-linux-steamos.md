---
author: leillo1975
category: Aventuras
date: 2017-06-14 07:36:55
excerpt: "<p>El a\xF1o que viene disfrutaremos de este t\xEDtulo de la compa\xF1\xED\
  a Odd Tales</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/42a35505dabe860dcdeb51f92d5be768.webp
joomla_id: 376
joomla_url: the-last-night-llegara-en-2018-a-linux-steamos
layout: post
tags:
- cyberpunk
- the-last-night
- pixel-art
- e3
title: "The Last Night llegar\xE1 en 2018 a Linux/SteamOS"
---
El año que viene disfrutaremos de este título de la compañía Odd Tales

Esta mañana, dándonos una vuelta por Twitter, encontré un mensaje de Ekianjo ([@BoilingSteam](https://twitter.com/BoilingSteam)) donde anunciaba [la llegada de un nuevo juego](https://twitter.com/BoilingSteam/status/874842941017264129) para Linux/SteamOS en 2018. Inmediatamente busqué información sobre el, y mi sorpresa fué mayúscula al ver un juego con un arte y temática tan llamativos.


 


Se trata de The Last Light, desarrollado por la compañía Inglesa [Odd Tales](http://oddtales.net/), y fué presentado ayer en el E3. Como podemos ver en la imagen y video (abajo), se trata de una aventura de scroll lateral donde se mezcla el 2D y 3D con el pixel art, todo ello adornado con efectos de composición y movimientos cinemáticos de cámara que le dan un aspecto único. Su temática es post-cyberpunk y tiene claras y nada escondidas influencias en **Blade Runner**, lo cual aun lo hace más atractivo aun, si cabe. Sobre este juego, [su página de Steam dice](http://store.steampowered.com/app/612400/The_Last_Night/):


 



> 
> *Los humanos conocieron primero la era de la supervivencia. Después conocieron la era del trabajo. Y ahora viven en la era del ocio. Las máquinas han sobrepasado la mano de obra humana no solo en fuerza sino también en precisión, intelecto y creatividad. Al haber encontrado la estabilidad en la renta universal, la gente lucha por encontrar su vocación, su identidad y se define por lo que consume, en lugar de lo que crea.*
> 
> 
> *The Last Night te sumerge en el día a día de Charlie, un ciudadano de segunda clase, que vive en una ciudad colmada de aumentos y gente con una existencia ludificada, algo que Charlie no puede experimentar en sus carnes por culpa de un accidente que tuvo cuando era niño. Apático y desanimado por el mundo aparentemente inútil que le rodea, a Charlie se le presenta una oportunidad para tomar las riendas del asunto. Pero, ¿a qué precio?*
> 
> 
> 


  


No se conoce ni fecha exacta de salida, ni requisitos técnicos, pero teniendo en cuenta que aun saldrá el año que viene es completamente normal. Desde aquí, en JugandoEnLinux.com, lo seguiremos de cerca y te informaremos puntualmente de todas las noticias que pueda generar este título tan llamativo. Sin más, os dejo con el hermoso trailer que nos ofrecieron en el E3:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/pupdeq1MoVw" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


 ¿Te ha llamado la atención este juego? ¿Te parece una propuesta interesante para el año que viene? Deja tus impresiones en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord.](https://discord.gg/ftcmBjD)

