---
author: Pato
category: "Acci\xF3n"
date: 2019-08-28 14:28:13
excerpt: "<p>Los responsables del juego @WarThunder esperan as\xED atajar a los tramposos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e9e897649b8945e5b66f572a62765473.webp
joomla_id: 1102
joomla_url: war-thunder-implementara-la-proteccion-easy-anti-cheat-eac-en-la-proxima-actualizacion
layout: post
tags:
- accion
- easy-anti-cheat
title: "War Thunder implementar\xE1 la protecci\xF3n Easy Anti Cheat (EAC) en la pr\xF3\
  xima actualizaci\xF3n"
---
Los responsables del juego @WarThunder esperan así atajar a los tramposos

**War Thunder** es uno de los (pocos) juegos multijugador masivos que tenemos oficialmente disponibles para Linux, y con el que en mas de una ocasión hemos echado alguna partida que otra aquí en la comunidad de Jugando en Linux. Ahora, los responsables del juego acaban de anunciar que en la próxima actualización (1.91) del juego van a implementar en conocido sistema anti-trampas **Easy Anti Cheat** (EAC), el mismo que implementan multitud de juegos actuales como Crossout, Hunt Shodown o Fortnite, por citar solo unos ejemplos.



> 
> [#EasyAntiCheat](https://twitter.com/hashtag/EasyAntiCheat?src=hash&ref_src=twsrc%5Etfw) is coming to War Thunder. In the upcoming update, we will make another important step to protect fair play with the integration of additional mechanics against unauthorized modifications in the game. Game performance will not be affected - <https://t.co/zxoFC7Z3xz> [pic.twitter.com/H7RlJDV4IS](https://t.co/H7RlJDV4IS)
> 
> 
> — War Thunder (@WarThunder) [August 28, 2019](https://twitter.com/WarThunder/status/1166714539209035782?ref_src=twsrc%5Etfw)



 Según el [comunicado oficial](https://warthunder.com/en/news/6347-news-easyanticheat-comes-to-war-thunder-en), hasta ahora han estado utilizando métodos propios para detectar a los tramposos, así como denuncias por parte de la comunidad, pero a partir de ahora combinarán estos métodos con este conocido sistema anti-trampas para intentar paliar el problema de las trampas en el juego asegurando que no tendrá un impacto significativo en el rendimiento del mismo.


#### Un momento... pero Easy Anti Cheat no es el sistema que me está impidiendo jugar a (X) juego en Linux vía Wine-Proton?


Efectivamente, Easy Anti Cheat (EAC) es un sistema que **SI está disponible y soportado para juegos bajo Linux**, pero sin embargo (aun) no ofrece soporte para poder jugar los juegos que se ejecutan bajo Wine-Proton, cosa que según rumores [está intentando solventar Valve](https://jugandoenlinux.com/index.php/homepage/noticias/39-noticia/1094-valve-esta-trabajando-con-eac-para-quitar-el-bloqueo-de-los-juegos-ejecutados-con-steam-play).


Según el [propio comunicado](https://warthunder.com/en/news/6347-news-easyanticheat-comes-to-war-thunder-en), "para poder implementar Easy Anti Cheat será necesario instalarlo indicando un directorio en el que se tengan permisos suficientes una vez lanzado el instalador", aunque esto puede ser un apunte de cara a sistemas Windows, y habrá que ver si esto también será de aplicación para las instalaciones sobre Linux.


Si quieres saber más o descargar War Thunder, puedes visitar su [página web oficial](https://warthunder.com/en/) o su [página de Steam](https://store.steampowered.com/app/236390/War_Thunder/) donde se puede instalar y jugar ya que es "free to play".

