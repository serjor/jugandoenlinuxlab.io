---
author: Serjor
category: Estrategia
date: 2019-03-30 10:26:59
excerpt: <p>Aseguran cambios escuchando a los jugadores</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/17b6a71885f45d0c136e6a32c71ba242.webp
joomla_id: 1008
joomla_url: valve-acepta-su-fracaso-con-artifact-y-anuncia-que-realizara-cambios-importantes-en-el-juego
layout: post
tags:
- valve
- artifact
title: "Valve acepta su fracaso con Artifact y anuncia que realizar\xE1 cambios importantes\
  \ en el juego"
---
Aseguran cambios escuchando a los jugadores

Interesante reflexión la que nos [ha dejado](https://steamcommunity.com/games/583950/announcements/detail/1819924505115920089) la gente de Valve que trabaja en Artifact.


Y es que no creo que nadie a estas alturas se sorprenda del fracaso que ha sido Artifact, hasta el punto que Richard Garfield, el diseñador de Artifact y Magic: The Gathering, [dejó el proyecto](https://arstechnica.com/gaming/2019/03/richard-garfields-leaves-valve-puts-artifacts-future-in-question/) a principios de Marzo.


El caso es que Valve ha reconocido el error, y como podéis leer, van a trabajar en mejorar el juego desde su base:



> 
> Cuando lanzamos Artifact, esperábamos que fuese el comienzo de un largo viaje, que sentará las bases de los próximos años. Nuestro plan era usar inmediatamente la estrategia típica de lanzar una serie de actualizaciones, basadas en la interacción con los miembros de la comunidad y entre nosotros mismos.
> 
> Obviamente, las cosas no han salido como esperábamos. Artifact representa la discrepancia más grande entre nuestras expectativas sobre cómo se recibiría uno de nuestros juegos y el resultado real. Pero no creemos que los jugadores estén malinterpretando nuestro juego, o que lo estén jugando mal. Artifact ahora representa una oportunidad para que podamos mejorar nuestro talento artístico y usar ese conocimiento para construir mejores juegos.
> 
> Desde el lanzamiento hemos estado comprobando cómo interactúan los jugadores con el juego y recopilando comentarios. Se ha hecho patente que el juego tiene problemas de base y que nuestra estrategia original de actualizaciones que lanzarían funciones y cartas nuevas no sería suficiente para resolver los problemas de base. En cambio, creemos que la mejor forma de corregir esos problemas es tomar medidas más amplias, reconsiderar las decisiones que hemos tomado a lo largo del tiempo respecto al 
> diseño del juego, su economía, la experiencia social de jugar y otros aspectos.
> 
> ¿Qué significa esto?
> 
> A partir de ahora, estaremos enfocados en resolver los problemas de base, en lugar de lanzar actualizaciones. Si bien esperamos que este proceso de experimentación y desarrollo llevará tiempo, nos entusiasma la idea de resolver este desafío, y pensamos ponernos en contacto con vosotros tan pronto como estemos listos.
> 


Supongo que a parte de cambiar alguna de las mecánicas, tomarán la decisión de hacerlo free to play, como han hecho recientemente con CS:GO, y siguiendo la inercia del mercado para este tipo de juegos.


Va a ser interesante cómo Valve reacciona al que quizás haya sido su mayor fracaso en años, y ante un escenario en el que no está muy acostumbrada a desenvolverse.


Y tú, ¿juegas a Artifact? Cuéntanos qué te parece esta decisión en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

