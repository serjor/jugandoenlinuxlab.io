---
author: Serjor
category: "Acci\xF3n"
date: 2017-09-09 14:23:07
excerpt: "<p>Acaban de presentar su juego al p\xFAblico, aunque en Early Access</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d8c427c7dd6445d50d9342ac3fcef82e.webp
joomla_id: 457
joomla_url: hive-el-moba-de-catness-games-tendra-version-para-linux
layout: post
tags:
- early-access
- moba
- hive
title: "HIVE, el MOBA de Catness Games, tendr\xE1 versi\xF3n para Linux"
---
Acaban de presentar su juego al público, aunque en Early Access

Fantásticas noticias las que nos trae la gente del estudio español [Catness Games](http://catnessgames.com/) y es que según nos confirman en un tweet, están trabajando en la versión para GNU/Linux



> 
> Sii, nos está costando más de lo esperado, la versión de Mac la conseguimos sacar hace poco y aún esta buggy, pero estará para Linux!!
> 
> 
> — Catness Games (@CatnessGames) [8 de septiembre de 2017](https://twitter.com/CatnessGames/status/906197175172026370)


HIVE es un MOBA en 2.5D, es decir, nos moveremos en horizontal y vertical, pero la cámara nos mostrará la acción desde una perspectiva en la que se note la profundidad del escenario, donde toda la acción ocurre en un hexágono en el que las diferentes secciones presentan gravedades diferentes, y que el pasado día 8 de septiembre ha sido lanzado en steam en Early Access.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/EE7KyzUFE0c?rel=0" width="560"></iframe></div>


Actualmente está solamente disponible en Early Access para Mac y Windows, pero cómo podéis ver en respuesta a nuestro tweet, también llegará a GNU/Linux.


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/468330/" width="646"></iframe></div>


Y tú, ¿qué opinas de este HIVE? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

