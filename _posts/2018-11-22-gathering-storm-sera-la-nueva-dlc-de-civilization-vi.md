---
author: leillo1975
category: Estrategia
date: 2018-11-22 10:17:21
excerpt: "<p>El juego portado por <span class=\"username txt-mute\">@AspyrMedia se\
  \ expandir\xE1 pronto</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/33e2c7a4f18f997a0fbcf33ef09e1b42.webp
joomla_id: 909
joomla_url: gathering-storm-sera-la-nueva-dlc-de-civilization-vi
layout: post
tags:
- aspyr-media
- dlc
- civilization-vi
- firaxis
- gathering-storm
title: "\"Gathering Storm\" ser\xE1 la nueva DLC de Civilization VI."
---
El juego portado por @AspyrMedia se expandirá pronto

Cuando ya han pasado 8 meses desde el lanzamiento de [Rise & Fall](index.php/homepage/analisis/20-analisis/829-analisis-civilization-vi-rise-fall-dlc), la anterior expansión del fantástico [Civilization VI](index.php/homepage/analisis/item/352-analisis-sid-meier-s-civilization-vi), nos llega el anuncio por parte de **Firaxis Games** de que el día de los enamorados (buen regalo), lanzarán su nueva expansión "[Gathering Storm](https://civilization.com/es-ES/news/entries/civilization-vi-gathering-storm-new-expansion-release-date-pc-february-14-2019/)". Aún no sabemos si este lanzamiento se producirá simultaneamente en Linux también, pero teniendo en cuenta el tweet de Aspyr, se supone que al menos trabajarán en él (teniendo en cuenta sus ciclos de trabajo se puede esperar que llege un mes o dos después):



> 
> Learn all about the new content coming in this blog post: <https://t.co/tu6jBMVpe3> <https://t.co/KjhgE69oLv>
> 
> 
> — Aspyr Media (@AspyrMedia) [November 20, 2018](https://twitter.com/AspyrMedia/status/1064974008670658560?ref_src=twsrc%5Etfw)



 En Gathering Storm **nos enfrentaremos a las fuerzas de la naturaleza**, teniendo que soportar los envites de Tormentas, Inundaciones, sequías, Volcanes o el Cambio Climático (vaya, ¿de que me suena?). Todas estas inclemencias obviamente supondrán un efecto negativo con el que tendremos que lidiar, pero también puede tener efectos positivos si aprendemos a usarlo en nuestro beneficio. Se añadirán también **nuevas tecnologías avanzadas**, **proyectos de ingeniería**, el **congreso mundial**, **8 nuevas civilizaciones** y **9 líderes** (¡aun más!), 7 maravillas, y muchas nuevas unidades, edificios, distritos y mejoras. **Los recursos naturales ahora se utilizarán para generar energía** (carbón, petroleo, renovables). Está claro que no será una expansión cualquiera, y conviene estar muy atentos a las noticias que genere, por lo que permaced atentos a nuestra web. Mientras tanto podeis disfrutar con el trailer que anuncia la expansión:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/trNUE32O-do" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Estais preparados para una nueva vuelta de tuerca en Civilization VI? Pues ya sabes, comenta  en este artículo o charla con nosotros sobre ello en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

