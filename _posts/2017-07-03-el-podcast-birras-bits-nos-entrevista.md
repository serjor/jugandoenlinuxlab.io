---
author: leillo1975
category: Editorial
date: 2017-07-03 10:51:53
excerpt: <p>Escucha a Pato el en conocido Podcast</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c501a702ef05e90d163a1eeeb1633357.webp
joomla_id: 392
joomla_url: el-podcast-birras-bits-nos-entrevista
layout: post
tags:
- entrevista
- podcast
- birras-y-bits
title: El Podcast "Birras & Bits" nos entrevista
---
Escucha a Pato el en conocido Podcast

Ayer se publicó una nueva entrega del conocido **[podcast semanal de Birras y Bits](https://birrasybits.wordpress.com/2017/07/02/byb-1x13-el-podcast-que-emociono-spielberg/)** en él que nuestro "jefe" Pato explicó a los oyentes todo lo relativo a JugandoEnLinux.com. Nos gustaría dar las **gracias a Miguel**,  nuestro usuario de Telegram que nos propuso la entrevista. Podeis escuchar su sección de videojuegos "De Tux a Tux" a partir del minuto 1:27, aunque nosotros desde aquí os animamos a que lo escucheis entero pues es un podcast muy ameno y desenfadado donde se tocan muchos temas que nos interesan a todos los Linuxeros.


 


Desde JugandoEnLinux.com nos gustaría dar las gracias también a todo el plantel  de "Birras y Bits" por la entrevista  y la oportunidad que nos ha dado para darnos a conocer a sus oyentes, y animarlos a que sigan semana tras semana con el podcast, porque realmente vale mucho la pena.


 


¿Que os ha parecido la entrevista? ¿Conocíais este Podcast? Cuentanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

