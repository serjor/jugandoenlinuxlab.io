---
author: Pato
category: Rol
date: 2017-01-26 22:42:44
excerpt: <p>"Bienvenidos de nuevo a Eora"</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/157c6c6cd616f458d56a6caf427711f8.webp
joomla_id: 197
joomla_url: obsidian-abre-la-campana-de-crowdfunding-para-pillars-of-eternity-ii-deadfire-en-fig-co
layout: post
tags:
- rol
- crowdfunding
- figco
title: "Obsidian abre la campa\xF1a de crowdfunding para 'Pillars of Eternity II:\
  \ Deadfire' en fig.co"
---
"Bienvenidos de nuevo a Eora"

Esta misma tarde nos llegaba la noticia mediante twitter:



> 
> Return to Eora.  
>   
> Welcome to [#Deadfire](https://twitter.com/hashtag/Deadfire?src=hash).<https://t.co/tApBcDd8rL> [pic.twitter.com/JE94XBoJ5r](https://t.co/JE94XBoJ5r)
> 
> 
> — Obsidian (@Obsidian) [January 26, 2017](https://twitter.com/Obsidian/status/824678299368562688)



No contentos con habernos traido 'Tyranny' [[Steam](http://store.steampowered.com/app/362960/)] hace apenas dos meses, Obsidian nos trae "Pillars of Eternity II:Deadfire" [[web oficial](http://eternity.obsidian.net/)] la secuela del excelente "PoE" [[Steam](http://store.steampowered.com/app/291650/?snr=1_5_9__300)] que comienza su campaña de financiación para llevarnos de nuevo a Eora a a vivir nuevas aventuras en otro juego de rol que a poco que cumpla lo que promete se llevará incontables horas de juego. A continuación el vídeo de la campaña:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/ln_plWALAoI" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


'Pillars of Eternity II: Deadfire' tendrá versión Linux tal y como puedes comprobar tanto en su web oficial como en la de la campaña, al igual que su antecesor y como prácticamente todos los juegos que publica Obsidian, por lo que no tengo dudas de que el soporte a Linux está garantizado.


"PoE II: Deadfire" contará con un mundo vivo donde encontraremos personajes que ya conocemos del juego anterior al igual que nuevos personajes. Además tendrá clima dinámico de modo que irá cambiando durante la partida y unido al lavado de cara del motor de iluminación y sombreado hará que este juego luzca mejor que nunca.


Si quieres ver todos los detalles, así como las opciones y recompensas por contribuir en la campaña de financiación puedes hacerlo [en este enlace](https://www.fig.co/campaigns/deadfire) donde además conforme vaya transcurriendo la campaña irán desvelando nuevos detalles sobre el juego.


¿Te gusta Pillars of Eternity? ¿Piensas aportar en la campaña de financiación?


Cuéntamelo en los comentarios, en el foro o en los canales de Jugando en Linux en Telegram o Discord.

