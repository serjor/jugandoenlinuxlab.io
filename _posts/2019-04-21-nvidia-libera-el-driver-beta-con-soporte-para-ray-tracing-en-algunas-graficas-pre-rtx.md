---
author: Serjor
category: Software
date: 2019-04-21 10:15:17
excerpt: "<p>Gracias a este backport podemos esperar algo m\xE1s a actualizar nuestras\
  \ gr\xE1ficas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2aa996f39965e7bd57256a4683c8eee7.webp
joomla_id: 1026
joomla_url: nvidia-libera-el-driver-beta-con-soporte-para-ray-tracing-en-algunas-graficas-pre-rtx
layout: post
tags:
- drivers
- beta
- nvidia
- ray-tracing
title: "NVIDIA libera el driver beta con soporte para Ray-Tracing en algunas gr\xE1\
  ficas pre-RTX"
---
Gracias a este backport podemos esperar algo más a actualizar nuestras gráficas

Leemos en [phoronix](https://www.phoronix.com/scan.php?page=news_item&px=NVIDIA-418.52.05-Released) que NVIDIA ha liberado una versión beta de sus drivers para GNU/Linux, con soporte para la extensión de Vuklkan de Ray-Tracing (VK_NV_ray_tracing) en gráficas anteriores a la familia RTX, en concreto:


* **Pascal:** TITAN Xp, TITAN X, GeForce GTX 1080 Ti, GeForce GTX 1080, GeForce GTX 1070 Ti, GeForce GTX 1070, GeForce GTX 1060 6GB
* **Volta:** TITAN V
* **Turing:** GeForce GTX 1660 Ti, GeForce GTX 1660


Como apuntan en el propio artículo de phoronix, ahora no hay mucho en GNU/Linux con lo que probar estos drivers, así que hasta que algún juego y/o demo técnica ponga a prueba esta extensión y drivers, nos quedaremos con la ilusión de saber que a futuro podremos probar en nuestras gráficas no RTX el maravilloso mundo (o no) del Ray-Tracing.


Y tú, ¿qué juego te gustaría ver con soporte a esta extensión? Cuéntanoslo en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

