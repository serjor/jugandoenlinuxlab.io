---
author: leillo1975
category: Estrategia
date: 2018-07-13 08:52:40
excerpt: <p>@kalypsomedia adquire los derechos de varias IP's de Pyro Studios</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/af6038056f6231c664e3ef073c45af10.webp
joomla_id: 804
joomla_url: la-saga-commandos-podria-llegar-en-un-futuro-a-linux-steamos
layout: post
tags:
- kalypso-media
- pyro-studios
- commandos
- praetorians
- espanol
- imperial-glory
title: "La saga Commandos podr\xEDa llegar en un futuro a Linux/SteamOS."
---
@kalypsomedia adquire los derechos de varias IP's de Pyro Studios

Hace bien poquito os hablábamos de ["Desperados: Wanted Dead or Alive"](index.php/homepage/generos/estrategia/item/917-desperados-wanted-dead-or-alive-puede-estar-en-camino-para-linux-steamos), un juego claramente influenciado por una de las sagas más exitosas del software español. Hablamos obviamente del aclamado [Commandos](https://es.wikipedia.org/wiki/Commandos) y todas sus continuaciones. Detrás de esta franquicia y más juegos como Praetorians e Imperial Glory, estaba una icónica desarrolladora madrileña  [Pyro Studios](https://es.wikipedia.org/wiki/Pyro_Studios), empresa que por desgracia ya no existe, pero que nos deja un legado de videojuegos con una gran calidad y acabado.


Llendo al meollo del asunto, ayer saltaba la [noticia](https://www.kalypsomedia.com/eu/news/commandos-returns-kalypso-secures-all-iconic-pyro-studios-franchises) de que la conocida distribuidora alemana de videojuegos **Kalypso Media** había adquirido los derechos sobre estas IPs, como podeis ver en el siguiente tweet:



> 
> ICYMI: Commandos returns! Get the full lowdown on the return of the legendary strategy franchise here: <https://t.co/Ca5oKRB0Z1> [pic.twitter.com/mOY7MzhwHQ](https://t.co/mOY7MzhwHQ)
> 
> 
> — Kalypso Media UK (@KalypsoMediaUK) [12 de julio de 2018](https://twitter.com/KalypsoMediaUK/status/1017416449869004802?ref_src=twsrc%5Etfw)



Obviamente da un poco de pena ver que uno de "nuestros" juegos mas aclamados cambia de manos, pero no hay mal que por bien no venga, pues [Kalypso Media](https://www.kalypsomedia.com/eu) es una editora que dispone de un gran catálogo de juegos con soporte para nuestro sistema ([Railway Empire](https://www.humblebundle.com/store/railway-empire?partner=jugandoenlinux), [Tropico 5](https://www.humblebundle.com/store/tropico-5?partner=jugandoenlinux), [Dungeons 3](https://www.humblebundle.com/store/dungeons-3?partner=jugandoenlinux) o [Sudden Strike 4](https://www.humblebundle.com/store/sudden-strike-4?partner=jugandoenlinux)) y es muy probable que estos juegos acaben siendo restaurados y editados con soporte. En las propias palabras de los responsables de Kalypso y Pyro podemos verlo (traducción automática www.DeepL.com/Translator):



> 
> *"Estamos muy orgullosos y muy honrados de poder añadir marcas tan grandes y exitosas como Commandos, Praetorians e Imperial Glory al portafolio de Kalypso Media" comentó **Simon Hellwig**, fundador y Director General Global de Kalypso Media Group, "Las oportunidades que surgen de esta adquisición ayudarán a crear las mejores condiciones posibles para el crecimiento futuro de Kalypso Media Group".*
> 
> 
> ***Ignacio Pérez**, fundador de Pyro Studios, añade: "Desde hace mucho tiempo buscamos un socio adecuado para la continuación de nuestros productos y marcas. Kalypso tiene una gran experiencia en el reinicio de títulos conocidos, y estamos encantados de tener nuestra IP en tan buenas manos".*
> 
> 
> ***Simon Hellwig** continúa: "Tenemos un gran respeto por lo que el equipo de Pyro Studios ha logrado y creado a lo largo de la historia del estudio. Por lo tanto, consideramos que es nuestra responsabilidad querer revivir y seguir desarrollando estos queridos juegos para los aficionados de todo el mundo. Por supuesto, esto incluirá el desarrollo de un juego completamente nuevo para todas las plataformas, pero también una **adaptación extensiva de los títulos existentes para las tecnologías y plataformas contemporáneas**. Estamos increíblemente entusiasmados con las oportunidades, y comenzaremos a hablar con potenciales estudios de desarrollo en un futuro cercano".*
> 
> 
> 


Esperemos poder jugar pronto todo este catálogo de forma nativa en nuestros PC's con mejoras como adaptación a las resoluciones actuales, mejoras en las texturas, etc; y por que no, un nuevo juego con todos los elementos propios de Commandos. Nosotros, por supuesto estaremos atentos para informaros si esto se produce. Si no conoceis "Commandos", o quereis rememorar viejos tiempos podeis ver el siguiente video:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/YJtJ5y5wnYo" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 


¿Conoceis la saga Commandos y Pyro Studios? ¿Habeis disfrutado con sus juegos? ¿Qué opinión os merece que Kalypso se haga con estas tan jugosas IP's? Déjanos tus impresiones en los comentarios o en nuestros grupos de [Telegram](https://t.me/jugandoenlinux) y [Discord](https://discord.gg/fgQubVY).


 

