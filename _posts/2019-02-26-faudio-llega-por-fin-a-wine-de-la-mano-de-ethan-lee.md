---
author: Pato
category: Software
date: 2019-02-26 10:54:00
excerpt: <p>El conocido programador @flibitijibibo lleva trabajando en Codeweavers
  desde hace unos meses</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/576634a42c4a68b90d534f03316d9d02.webp
joomla_id: 981
joomla_url: faudio-llega-por-fin-a-wine-de-la-mano-de-ethan-lee
layout: post
tags:
- wine
- ethan-lee
- faudio
title: FAudio llega por fin a Wine de la mano de Ethan Lee
---
El conocido programador @flibitijibibo lleva trabajando en Codeweavers desde hace unos meses

Desde hace unos meses el conocido programador **Ethan Lee**, desarrollador de tecnologías como [FNA](https://fna-xna.github.io/) (reimplementación de XNA propietaria de Microsoft) y que gracias a su esfuerzo nos han llegado [multitud de desarrollos](http://www.flibitijibibo.com/index.php?page=Portfolio/Ports), está **trabajando para Codeweavers**, compañía que está detrás del desarrollo de Wine y que como sabéis tiene como "socio" a Valve con Steam Play/Proton cuyo objetivo es poder ejecutar los juegos de Windows en Linux.


Ahora gracias a [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Wine-Lands-FAudio-XAudio2) nos llegan noticias de que el código de Ethan Lee está ya siendo integrado en el desarrollo de Wine. Dentro de Codeweavers, Ethan se ha encargado de desarrollar una reimplementación de la tecnología XAudio2 llamada **FAudio**. Esta reimplementación de las librerías del "runtime" de audio de Microsoft viene para **mejorar el soporte de audio en los juegos** que nos llegan desde la API DirectX (antiguamente DirectSound) ya que hasta ahora Wine ha estado soportando XAudio2 sobre la capa OpenAL, pero FAudio ofrece soporte mucho más avanzado y completo para la API de sonido de Windows o XBox.


Esto debería significar un mejor soporte para los juegos bajo Wine/Proton e incluso la llegada de nuevos juegos que hasta ahora no era posible ejecutar por problemas con estas librerías.


Esperemos que los esfuerzos de Ethan Lee sigan trayéndonos jugosas novedades y quedamos a la espera de que estas mejoras nos lleguen en las próximas versiones de Wine y que Steam Play debería implementar próximamente con la llegada de la nueva versión de Wine en la rama beta de Proton.


Puedes ver la implementación de FAudio en [este enlace](https://source.winehq.org/git/wine.git/commit/3e390b1aafff47df63376a8ca4293c515d74f4ba).

