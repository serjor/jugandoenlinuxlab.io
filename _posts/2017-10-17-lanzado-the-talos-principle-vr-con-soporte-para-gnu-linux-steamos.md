---
author: leillo1975
category: Puzzles
date: 2017-10-17 19:54:14
excerpt: <p>Sorpresa, Croteam lo ha vuelto a hacer.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4a606feeb074f807e1a3d37a1e32f62f.webp
joomla_id: 492
joomla_url: lanzado-the-talos-principle-vr-con-soporte-para-gnu-linux-steamos
layout: post
tags:
- croteam
- vr
- htc-vive
- the-talos-principle-vr
title: Lanzado The Talos Principle VR con soporte para GNU-Linux/SteamOS.
---
Sorpresa, Croteam lo ha vuelto a hacer.

Gracias a la información que recogemos de [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Talos-Principle-VR-Launched), nos hemos enterado que el estudio croata ([Croteam](index.php/component/search/?searchword=croteam&searchphrase=all&Itemid=101)) acaba de lanzar la adaptación a la **realidad virtual** de su juego de puzzles en primera persona, The Talos Principle, y con soporte para nuestro sistema, si disponemos de unas **HTC Vive**. Como veis los desarrolladores de esta compañía son merecedores de  toda nuestra admiración por el magnífico soporte que están brindando a toda la comunidad linuxera con sus productos. Como todos sabreis, Croteam ha lanzado la mayor parte de su catálogo para linux, son pioneros en el uso de al API Vulkan en las remasterizaciones de sus juegos, y por supuesto sin olvidarnos de ser de los pocos, por no decir los únicos que lanzan juegos VR en nuestro sistema. Aquí teneis el mensaje que han dejado en Twitter:


 



> 
> The Talos Principle VR is LIVE on Steam! Experience it the way it was always meant to be - in virtual reality.<https://t.co/PJ7OML5AoM> [pic.twitter.com/9ciGbNndTX](https://t.co/9ciGbNndTX)
> 
> 
> — The Talos Principle (@TalosPrinciple) [October 17, 2017](https://twitter.com/TalosPrinciple/status/920318838905942017?ref_src=twsrc%5Etfw)



 


Los requisitos para poder disfrutar de este juego en realidad virtual son los mismos que en Windows (un i5, 6GB de RAM, y una gráfica R9 290 o una GTX970). El precio es de 36.99€, pero si disponemos del juego en su versión normal obtendremos un descuento del 25%. El juego **incluye la ultima expansión "The Road to Gehenna"** de forma gratuita. teneis más info en su [página web](http://www.croteam.com/talos-principle-vr-available-now-steam/). Podeis ver el  Teaser del trailer en este video de Youtube:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/yeDt1Bivcc4" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 Si sois los afortunados poseedores de unas HTC Vive podeis haceros con el juego en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/552440/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Qué os parece el trabajo de Croteam? ¿Habeis jugado a The Talos Principle? Deja tus respuestas en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

