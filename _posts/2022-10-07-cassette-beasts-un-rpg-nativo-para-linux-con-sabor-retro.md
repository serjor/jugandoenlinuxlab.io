---
author: Odin
category: Rol
date: 2022-10-07 17:38:36
excerpt: "<p>De la mano del estudio indie <a href=\"https://bytten-studio.com/devlog/\"\
  \ target=\"_blank\" rel=\"noopener\">Bytten Studio</a> nos llega esta interesante\
  \ propuesta de corte <strong>JRPG cl\xE1sico.</strong></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/CassetteBeasts/CassetteBeasts_keyart_16_9.webp
joomla_id: 1491
joomla_url: cassette-beasts-un-rpg-nativo-para-linux-con-sabor-retro
layout: post
tags:
- rol
- demo
- godot
title: Cassette Beasts, un RPG nativo para Linux con sabor Retro
---
De la mano del estudio indie [Bytten Studio](https://bytten-studio.com/devlog/) nos llega esta interesante propuesta de corte **JRPG clásico.**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/C0zv4r-DTPg" title="YouTube video player" width="720"></iframe></div>


En [JEL](https://jugandoenlinux.com) hemos probado la demo, que han publicado con motivo del [NextFest](https://store.steampowered.com/sale/nextfest), y creemos que tiene potencial para convertirse en un juego de rol que hará las delicias de los amantes del género.


En nuestro caso la [demo](https://store.steampowered.com/app/1321440/Cassette_Beasts/) ha durado 40 minutos. Empieza con nuestro protagonista que ha naufragado en la playa de una isla misteriosa de la que no sabemos nada. Tras explorar un poco, nos encontraremos con un pequeño poblado, y justo ahí empezará una batalla que servirá de tutorial para iniciarnos el sistema de combate del juego.


A partir de ese momento empieza a desgranarse algunos de los conceptos originales del juego. Por lo que hemos podido comprobar, no luchamos con los personajes directamente, sino que se transforman en criaturas que tienen habilidades que nos servirán en el campo de batalla y a veces incluso en el mapeado del mundo. La forma de transformarse es escuchando una cinta de música en un **walkman**, un detalle de los más **noventero**, que permite transformarse en la criatura que haya sido previamente grabada en la cinta. Si en los videojuegos de Pokemon atrapamos las criaturas en una capsula (Poké Balls), en este caso podemos grabar los diferentes enemigos en cintas, que luego podremos escoger de nuestra creciente colección para las batallas. Una vez transformados, participaremos en un combate por turnos, en el que podemos gastar puntos de acción para realizar diferentes ataques y que le dan un mayor profundidad táctica.


![SistemaCombate](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/CassetteBeasts/SistemaCombate.webp) 


Otro punto relevante del juego es que la interacción con los distintos habitantes del pueblo no será únicamente de importancia a la hora de progresar en la historia, sino también en los combates ya que hay un sistema de fusiones en el combate que depende de la afinidad que tengan los personajes.


[Cassette Beasts](https://store.steampowered.com/app/1321440/Cassette_Beasts/) tiene ese punto de [Stardew Valley](https://www.stardewvalley.net/) en las relaciones de los personajes y en el ambiente del pueblo en el que nos alojaremos, que nos parece que va muy bien encaminado y que sirve como paréntesis tras las misiones. La narrativa y los diálogos, como buen representante del género, son una de las bases del juego, y aquí también tenemos otro punto muy positivo, que es que tiene **traducción al español,** y al contrario que en otros casos, la calidad de la traducción es buena.


![poblado](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/CassetteBeasts/poblado.webp)


En lo que respecta al apartado técnico, teniendo en cuenta que es un juego todavía en desarrollo, hay que decir que tiene un buen acabado. Los gráficos, aunque sencillos y  sin llegar a la calidad de juegos como Octopath Traveler si que transmiten bien lo que es llevar el estilo de sprites de los **JRPG** de principios de los 90 al 3d. El juego lo hemos probado en una [Steam Deck](https://store.steampowered.com/steamdeck), y aunque ahora mismo no está incluido ni como verificado ni jugable, la verdad es que funciona casi perfecto, tanto en el mapeo de los controles, como a la hora de ejecutarse. Solamente hemos podido observar algunos bugs con el botón de salto del protagonista y con ralentizaciones puntuales que suponemos serán corregidos en la versión final. Es realmente de agradecer que [Bytten Studio](https://bytten-studio.com/devlog/), se moleste en hacer una versión nativa para Linux y que usen [Godot](https://godotengine.org/), un motor de videojuegos de código abierto y con un buen soporte para Linux.


Con respecto al apartado sonoro hay que destacar que está bien cuidado, combina bien con el estilo que se quiere dar el juego, y  las melodías se hacen amenas de escuchar mientras exploramos el mapeado sin hacerse pesadas en ningún momento.


En resumen, si sois aficionados los **JRPG** y a los juegos indies, os recomendamos que probéis cuanto antes la [demo](https://store.steampowered.com/app/1321440/Cassette_Beasts/), ya que solo estará disponible durante el [NextFest](https://store.steampowered.com/sale/nextfest), y seguramente os pasará como a nosotros, que estamos con muchas ganas de ver la versión final del juego de este prometedor [Cassette Beasts](https://store.steampowered.com/app/1321440/Cassette_Beasts/).


¿Qué te parece Cassette Beasts? ¿Lo has probado? Cuéntanos como siempre tu opinión sobre este título tan particular en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

