---
author: Serjor
category: Noticia
date: 2018-12-03 14:41:18
excerpt: "<p>Uno de los motores de f\xEDsicas m\xE1s potentes se hace open source</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f44997cb312b542bbd55c895274ee230.webp
joomla_id: 917
joomla_url: nvidia-libera-el-sdk-de-physx-bajo-bsd
layout: post
tags:
- nvidia
- physx
- bsd
title: NVIDIA libera el SDK de PhysX bajo BSD
---
Uno de los motores de físicas más potentes se hace open source

[Noticias](https://www.gamingonlinux.com/articles/nvidia-have-now-made-physx-open-source.13089) como esta nos hacen pensar que como en menos de un mes es invierno en este lado del emisferio, el infierno debe estar a punto de congelarse.


Y es que, aunque no es la noticia que todos queríamos leer, NVIDIA [ha liberado el SDK de PhysX](https://github.com/NVIDIAGameWorks/PhysX-3.4) bajo una licencia open source (BSD para ser exactos).


Es cierto que no son sus drivers (cosa que de suceder, confirmaría la congelación del averno), pero siempre es bueno ver cómo las grandes empresas dan estos pasos y liberan sus soluciones de manera que los desarrolladores que hacen uso de estas tecnologías puedan investigar cómo funcionan, cómo mejorarlas y cómo usarlas en cualquier circunstancia.


Por supuesto, el uso en GNU/Linux está incluído bajo esta nueva licencia (por ejemplo consolas no), así que esperemos que esto permita que los juegos que hagan uso de esta tecnología puedan funcionar mejor, y que además se pueda conseguir que funcione en otras gráficas que no sean NVIDIA (actualmente solamente corre sobre CUDA).


Os dejamos con el vídeo del anuncio de la versión 4.0 de PhysX, la cuál será liberada el 20 de diciembre, y que a parte de servir cómo motor de físicas para videojuegos, aspira ser útil en proyectos de inteligencia artificial y robótica.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/K1rotbzekf0" width="560"></iframe></div>


Y tú, ¿qué piensas de este movimiento por parte de NVIDIA? Déjanos tu opinión en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

