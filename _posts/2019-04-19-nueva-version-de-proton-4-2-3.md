---
author: Serjor
category: Software
date: 2019-04-19 07:12:09
excerpt: <p>La capa de compatibilidad de Valve sigue mejorando poco a poco</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/cb1c367cf984f0ad2cae4fa928f517fc.webp
joomla_id: 1024
joomla_url: nueva-version-de-proton-4-2-3
layout: post
tags:
- steam
- wine
- valve
- proton
- mono
title: "Nueva versi\xF3n de Proton, 4.2-3"
---
La capa de compatibilidad de Valve sigue mejorando poco a poco

Si ayer nos [hacíamos eco](index.php/homepage/generos/software/12-software/1145-lutris-hace-posible-ejecutar-la-epic-games-store-gracias-wine) de que Lutris ya es capaz de ejutar la tienda de Epic, hoy os traemos una actualización de Proton:




> 
> Proton 4.2-3 is now available with wine-mono and DXVK 1.0.3! [pic.twitter.com/vja1euxUVb](https://t.co/vja1euxUVb)
> 
> 
> — Pierre-Loup Griffais (@Plagman2) [18 de abril de 2019](https://twitter.com/Plagman2/status/1118971601310011393?ref_src=twsrc%5Etfw)



Y es que como podemos ver en el tweet esta revisión de la rama 4.2 de proton, aún siendo una revisión menor, trae una lista de cambios bastante interesante:


* A partir de ahora Proton incluye wine-mono, lo que habilita una gran cantidad de juegos basados en XNA, juegos desarrollados con Unreal Engine 3, lanzadores y más
* El launcher y actualizador de Warframe es funcional
* Solucionada la entrada de texto en Age of Empires II HD
* NARUTO SHIPPUDEN: Ultimate Ninja STORM 4 y Evochron Mercenary son jugables
* Siguen con el mantenimiento de la funcionalidad de Uplay
* DXVK ha sido actualizado a la versión 1.0.3
* FAudio ha sido actualizado a la versión 19.04-13-ge8c0855


Habrá que estar atentos a los reportes en [protondb](protondb.com), porque es de esperar que con la inclusión de wine-mono, y las nuevas versiones de DXVK y FAudio muy probablemente hagan que mejore la compatibilidad de muchos juegos.


Y tú, ¿qué juego esperas que funcione bajo Proton como si fuera nativo? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

