---
author: Pato
category: Hardware
date: 2018-10-11 08:13:49
excerpt: <p>Otra alternativa que nos permite configurar los equipos a nuestro gusto
  para jugar</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2d9c6ec24bc1fb99260bf12d63333c7c.webp
joomla_id: 870
joomla_url: la-gama-kymera-de-slimbook-ahora-disponible-con-procesadores-y-graficas-amd
layout: post
title: "La gama Kymera de Slimbook ahora disponible con procesadores y gr\xE1ficas\
  \ AMD"
---
Otra alternativa que nos permite configurar los equipos a nuestro gusto para jugar

Hace unos días [nos hacíamos eco](index.php/homepage/hardware/item/977-slimbook-entra-en-el-mercado-de-los-pcs-gaming-con-kymera) de que Slimbook, la marca de ordenadores compatibles con Linux había lanzado una línea de ordenadores sobremesa con configuraciones pensadas para exprimir la potencia de su hardware, ya sea con programas que necesiten gran rendimiento o videojuegos.


En aquel momento Slimbook se centró en la configuración de procesadores y gráficas Intel - Nvidia, pero **ahora nos presenta una alternativa lanzando la misma línea Kymera pero con hardware de AMD.**


En concreto, en la configuración Kymera Ventus Slimbook parte de un procesador Ryzen 3 2200G con gráfica Radeon Vega integrada, con opción de montar procesadores como:


Ryzen 5 2400G con gráfica Radeon Vega integrada


Ryzen 5 2600X


Ryzen 7 2700X


En cuanto a las gráficas dedicadas, podemos optar por AMD Radeon:


RX 570 4GB


RX 580 8GB


Vega 64 8GB


La refrigeración corre a cargo, aparte de la de Stock de opciones de la marca NOX como los H-190 y H-312, y en cuanto a memoria RAM partimos desde los 8GB a 2400 Mhz y podemos llegar a montar hasta 64 GB a 3200 Mhz.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/slimbook/Kymeraoptions.webp)


Respecto al precio, en la gama Ventus partimos de una configuración base con gráfica Vega integrada (APU) **desde 495€**.


#### Kymera Aqua también tiene su alternativa AMD


En cuanto a la gama Kymera Aqua, también tenemos disponible la opción de montar hardware de AMD. Sin embargo siguiendo la filosofía de la serie Aqua donde Slimbook ofrece las configuraciones mas potentes las opciones están algo más limitadas ya que nos ofrece como opción **solo el procesador Ryzen 7 2700X acompañado de una Radeon Vega 64 con 8GB** y refrigeración premium por circuito de agua.


![kymeraaquaamd](https://slimbook.es/images/new/kymera-aqua/galeria/05.jpg)


En cuanto al resto de configuraciones, Slimbook ofrece las mismas que ya comentamos en el anterior artículo que dedicamos a la serie Kymera incluyendo las distintas opciones de instalación de sistemas Linux, y que podéis ver [en este enlace](index.php/homepage/hardware/item/977-slimbook-entra-en-el-mercado-de-los-pcs-gaming-con-kymera).


La gama Aqua con AMD parte de un **precio base de 1.875,00€**.


Puedes ver las distintas configuraciones en la web de Slimbook, ya sea para configurar la gama Ventus con AMD [en este enlace](https://slimbook.es/pedidos/slimbook-kymera/kymera-ventus-amd-comprar), como para configurar la gama Aqua con AMD [en este enlace](https://slimbook.es/pedidos/slimbook-kymera/kymera-aqua-amd-comprar).


¿Qué te parecen las opciones de Slimbook con AMD? ¿Te gustaría tener uno de estos Kymera con hardware de AMD?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

