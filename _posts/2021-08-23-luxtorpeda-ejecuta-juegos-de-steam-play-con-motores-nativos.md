---
author: Son Link
category: Software
date: 2021-08-23 20:23:16
excerpt: "<p>Otra opci\xF3n para jugar ciertos t\xEDtulos de forma \"nativa\"</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Luxtorpeda/Luxtorpeda.webp
joomla_id: 1345
joomla_url: luxtorpeda-ejecuta-juegos-de-steam-play-con-motores-nativos
layout: post
title: 'Luxtorpeda: ejecuta juegos de Steam Play con motores nativos'
---
Otra opción para jugar ciertos títulos de forma "nativa"


Desde hace unos días a través del subreddit de [linux_gaming](https://www.reddit.com/r/linux_gaming) he sabido de [Luxtorpeda](https://github.com/luxtorpeda-dev/luxtorpeda), una herramienta de compatibilidad para **Steam Play** gracias al cual podremos jugar usando **motores nativos** para nuestro sistema en lugar de tirar de **Proton** a diversos juegos, sobre todos clásicos, como los **Doom**, **Quake**, **Tomb Raider** (los primeros) y **Hexen**, entre otros. Este proyecto cuenta con una buena lista de juegos y variantes, pero cualquiera puede colaborar añadiendo otros motores, incluso hay juegos que pueden jugarse en más de un motor, de hecho la herramienta nos preguntara que motor queremos usar.


Instalación
-----------


Para instalar esta herramienta debemos seguir una serie de sencillos pasos:


1. Cerrar Steam.
2. Descargar la ultima [versión disponible](https://github.com/luxtorpeda-dev/luxtorpeda/releases).
3. Una vez descargado debemos descomprimir el contenido del paquete en una de estas rutas, o crearla en caso de no existir:
* ~/.local/share/Steam/compatibilitytools.d/
* ~/.steam/root/compatibilitytools.d/
4. Abrimos Steam y en la ventana de **propiedades** **del juego** (click derecho del ratón sobre el juego y pulsar en **Propiedades**) vamos a **Compatibilidad** y marcamos la casilla. Una vez marcada nos saldrá un listado con las herramientas instaladas y seleccionamos **luxtorpeda**. Una vez hecho cerramos la ventana.  
![luxtorpeda01](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Luxtorpeda/luxtorpeda01.webp)


Una vez hecho esto al arrancar el juego se descargara el motor y arrancara el juego. Si un juego puede ser usado en más de un motor nos preguntara cual de ellos usar. En [esta página](https://luxtorpeda-dev.github.io/packages.html) tenéis un listado con los juegos soportados y sus motores, así como la licencia de estos, si soportan 64 bits, versión del motor, etc.


Sin duda alguna una herramienta muy útil y que facilita la tarea, sobre todo para cuando salga a la venta la Steam Deck ;)


Fuente original: [linux_gaming](https://www.reddit.com/r/linux_gaming/comments/p8n5rf/quake_quake_mission_packs_and_quake_iis_native/)

