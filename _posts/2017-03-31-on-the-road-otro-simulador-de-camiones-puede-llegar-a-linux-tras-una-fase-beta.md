---
author: leillo1975
category: "Simulaci\xF3n"
date: 2017-03-31 08:29:05
excerpt: "<p>Ayer, navegando un poco por la web de la editora Aerosoft, tras ver la\
  \ noticia de Xplane me encontr\xE9 con que <span style=\"color: #000080;\"><a href=\"\
  http://www.aerosoft.com/en/bustruck/news/on-the-road-early-access-available-now\"\
  \ target=\"_blank\" rel=\"noopener noreferrer\" style=\"color: #000080;\">esta editora\
  \ tambi\xE9n dispone de juego de Simulaci\xF3n de Camiones</a></span>. Como gran\
  \ aficionado que soy a los juegos de SCSSoft (ETS2 y ATS) , me pudo la curiosidad\
  \ y decid\xED indagar un poco m\xE1s sobre este t\xEDtulo.</p>\r\n<p>&nbsp;</p>\r\
  \n<p>El juego est\xE1 siendo desarrollado por el estudio alem\xE1n <strong><span\
  \ style=\"color: #000080;\"><a href=\"http://toxtronyx.com/\" target=\"_blank\"\
  \ rel=\"noopener noreferrer\" style=\"color: #000080;\">Toxtronyx</a></span></strong>\
  \ y en este momento est\xE1 en <strong>Acceso Anticipado. </strong>Actualmente solo\
  \ hay soporte para Windows y Mac, pero buscando respuestas en los foros de Steam<span\
  \ style=\"color: #000080;\"><a href=\"http://steamcommunity.com/app/285380/discussions/0/133261124638474002/\"\
  \ target=\"_blank\" rel=\"noopener noreferrer\" style=\"color: #000080;\"> decid\xED\
  \ dejar una pregunta</a></span> sobre si se le dar\xEDa a los usuarios de GNU-LInux/SteamOS\
  \ la posibilidad de jugarlo, y la respuesta fu\xE9 que ya est\xE1n trabajando en\
  \ ello y tienen una version que funciona pero que tienen que probar en m\xE1s m\xE1\
  quinas. Esperemos que pronto podamos nosotros tambi\xE9n poder disfrutarlo.</p>\r\
  \n<p>&nbsp;</p>\r\n<p><img src=\"images/Articulos/OnTheRoadTruckSim.jpg\" alt=\"\
  \" class=\"pull-center\" style=\"display: block; margin-left: auto; margin-right:\
  \ auto;\" width=\"800\" height=\"450\" /></p>\r\n<p>&nbsp;</p>\r\n<p>Por lo que\
  \ he visto, el juego permite conducir camiones por carreteras alemanas, y dispone\
  \ ya de algunas ciudades como Rostock, Berlin o Hamburgo, pero est\xE1n en preparaci\xF3\
  n muchas m\xE1s. El juego en los videos luce bastante bien dentro de lo que cabe\
  \ y teniendo en cuenta que est\xE1 en desarrollo. La idea es ir ampliando poco a\
  \ poco la lista de paises. Tambi\xE9n se pretende aumentar el n\xFAmero de camiones,\
  \ as\xED como los detalles de personalizaci\xF3n. A grandes rasgos se puede observar\
  \ que las ciudades son un poco m\xE1s amplias que las ofrecidas por ETS2, y los\
  \ efectos clim\xE1ticos e iluminaci\xF3n y sombras parecen est\xE1r bastante trabajados.\
  \ Dispondr\xE1 tambi\xE9n de un sistema de gesti\xF3n econ\xF3mica donde podremos\
  \ fundar nuestra propia compa\xF1\xEDa y ir adquiriendo vehiculos.</p>\r\n<p>&nbsp;</p>\r\
  \n<p>En principio la idea del juego no dista mucho de lo que ofrece Euro Truck Simulator\
  \ 2, pero esperemos que a medida que avanza el desarrollo podamos ver elementos\
  \ originales y diferenciadores que puedan hacer de este una compra justificada.\
  \ Seguiremos con atenci\xF3n este desarrollo e informaremos de cualquier noticia\
  \ que se produzca con respecto a \xE9l. Aqu\xED os dejo un video en alem\xE1n donde\
  \ podeis ver algunos detalles del estado actual del juego:</p>\r\n<p>&nbsp;</p>\r\
  \n<p><iframe src=\"https://www.youtube.com/embed/nz2cLM9iJyE\" width=\"560\" height=\"\
  315\" style=\"display: block; margin-left: auto; margin-right: auto;\" allowfullscreen=\"\
  allowfullscreen\"></iframe></p>\r\n<p>&nbsp;</p>\r\n<p><strong>Si en un futuro,\
  \ cuando haya soporte para GNU-Linux</strong>, quereis adquirir este juego podeis\
  \ hacerlo en la<span style=\"color: #000080;\"> <a href=\"http://www.aerosoft.com/en/bustruck/truck-simulation/1978/early-access-version-on-the-road-truck-simulation\"\
  \ target=\"_blank\" rel=\"noopener noreferrer\" style=\"color: #000080;\">tienda\
  \ de Aerosoft</a> </span>o en su p\xE1gina de Steam:</p>\r\n<p>&nbsp;</p>\r\n<p><iframe\
  \ src=\"https://store.steampowered.com/widget/285380/\" width=\"646\" height=\"\
  190\" style=\"display: block; margin-left: auto; margin-right: auto;\"></iframe></p>\r\
  \n<p>&nbsp;</p>\r\n<p>\xBFQue impresiones os deja este desarrollo? \xBFOs parece\
  \ interesante su propuesta? Podeis responder en los comentarios o usar nuestros\
  \ canales de <span style=\"color: #000080;\"><a href=\"https://telegram.me/jugandoenlinux\"\
  \ target=\"_blank\" style=\"color: #000080;\">Telegram</a></span> y <span style=\"\
  color: #000080;\"><a href=\"https://discord.gg/ftcmBjD\" target=\"_blank\" style=\"\
  color: #000080;\">Discord</a></span> donde os estamos esperando.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/15d406f06ce12f2ac57cb5137d1afc69.webp
joomla_id: 271
joomla_url: on-the-road-otro-simulador-de-camiones-puede-llegar-a-linux-tras-una-fase-beta
layout: post
tags:
- simulador
- beta
- on-the-road
- camiones
- toxtronyx
title: '''On the Road'',  otro simulador de camiones puede llegar a Linux tras una
  fase beta'
---
Ayer, navegando un poco por la web de la editora Aerosoft, tras ver la noticia de Xplane me encontré con que [esta editora también dispone de juego de Simulación de Camiones](http://www.aerosoft.com/en/bustruck/news/on-the-road-early-access-available-now). Como gran aficionado que soy a los juegos de SCSSoft (ETS2 y ATS) , me pudo la curiosidad y decidí indagar un poco más sobre este título.


 


El juego está siendo desarrollado por el estudio alemán **[Toxtronyx](http://toxtronyx.com/)** y en este momento está en **Acceso Anticipado.** Actualmente solo hay soporte para Windows y Mac, pero buscando respuestas en los foros de Steam [decidí dejar una pregunta](http://steamcommunity.com/app/285380/discussions/0/133261124638474002/) sobre si se le daría a los usuarios de GNU-LInux/SteamOS la posibilidad de jugarlo, y la respuesta fué que ya están trabajando en ello y tienen una version que funciona pero que tienen que probar en más máquinas. Esperemos que pronto podamos nosotros también poder disfrutarlo.


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/OnTheRoadTruckSim.webp)


 


Por lo que he visto, el juego permite conducir camiones por carreteras alemanas, y dispone ya de algunas ciudades como Rostock, Berlin o Hamburgo, pero están en preparación muchas más. El juego en los videos luce bastante bien dentro de lo que cabe y teniendo en cuenta que está en desarrollo. La idea es ir ampliando poco a poco la lista de paises. También se pretende aumentar el número de camiones, así como los detalles de personalización. A grandes rasgos se puede observar que las ciudades son un poco más amplias que las ofrecidas por ETS2, y los efectos climáticos e iluminación y sombras parecen estár bastante trabajados. Dispondrá también de un sistema de gestión económica donde podremos fundar nuestra propia compañía y ir adquiriendo vehiculos.


 


En principio la idea del juego no dista mucho de lo que ofrece Euro Truck Simulator 2, pero esperemos que a medida que avanza el desarrollo podamos ver elementos originales y diferenciadores que puedan hacer de este una compra justificada. Seguiremos con atención este desarrollo e informaremos de cualquier noticia que se produzca con respecto a él. Aquí os dejo un video en alemán donde podeis ver algunos detalles del estado actual del juego:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/nz2cLM9iJyE" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


**Si en un futuro, cuando haya soporte para GNU-Linux**, quereis adquirir este juego podeis hacerlo en la [tienda de Aerosoft](http://www.aerosoft.com/en/bustruck/truck-simulation/1978/early-access-version-on-the-road-truck-simulation) o en su página de Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/285380/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Que impresiones os deja este desarrollo? ¿Os parece interesante su propuesta? Podeis responder en los comentarios o usar nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD) donde os estamos esperando.

