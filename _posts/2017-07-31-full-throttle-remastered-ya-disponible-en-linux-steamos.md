---
author: Pato
category: Aventuras
date: 2017-07-31 18:47:01
excerpt: "<p>El cl\xE1sico juego por fin llega a Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/16fd96deba85a06311383433f152ed24.webp
joomla_id: 423
joomla_url: full-throttle-remastered-ya-disponible-en-linux-steamos
layout: post
tags:
- aventura
- steam
- aventura-grafica
- point-and-clic
title: Full Throttle Remastered ya disponible en Linux/SteamOS
---
El clásico juego por fin llega a Linux

Full Trottle Remastered es... ¿alguien no conoce a Tim Schafer o el estudio Double Fine?


La descripción del juego habla por si misma:



> 
> *Full Throttle [[web oficial](http://fullthrottle.doublefine.com/)] es una aventura gráfica diseñada por el legendario Tim Schafer que fue publicada por LucasArts en 1995. Cuenta la historia de Ben Throttle, el rudo líder de una banda de moteros conocida como los Polecats, que se ve envuelto en una trama de motos y muerte.  
> Ahora, más de 20 años después, Full Throttle vuelve con una edición remasterizada en la que los jugadores disfrutarán de imágenes en 3D de alta resolución diseñadas a mano, así como de música y sonido mejorados.*
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/cNFkmjC6-aI" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Si te gustan las aventuras "point and clic" no lo dejes escapar. Apenas pide requisitos y es un buen juego para las horas de ocio veraniegas.


Lo tienes disponible ya traducido al español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/228360/" width="646"></iframe></div>


¿Qué te parece "Full Throttle"? ¿Piensas jugarlo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

