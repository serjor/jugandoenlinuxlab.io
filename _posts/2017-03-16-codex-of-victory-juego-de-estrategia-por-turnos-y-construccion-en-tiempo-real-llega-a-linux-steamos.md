---
author: Pato
category: Estrategia
date: 2017-03-16 19:22:13
excerpt: <p>Seguimos con los lanzamientos...</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b48f2c03bbd159814922841bfb3fe7d7.webp
joomla_id: 253
joomla_url: codex-of-victory-juego-de-estrategia-por-turnos-y-construccion-en-tiempo-real-llega-a-linux-steamos
layout: post
tags:
- rts
- steam
- estrategia
- turnos
title: "'Codex of Victory' juego de estrategia por turnos y construcci\xF3n en tiempo\
  \ real llega a Linux/SteamOS"
---
Seguimos con los lanzamientos...

Hoy la cosa está caliente en cuanto a nuevos lanzamientos. 'Codex of Victory' [[web oficial](http://codexofvictory.com/)] es el nuevo lanzamiento del estudio Ino-Co Plus, creadores de la saga "[Warlock](http://store.steampowered.com/search/?developer=Ino-Co%20Plus&snr=1_5_9__408)" también disponible en Linux.


Se trata de un juego de estrategia por turnos de ciencia ficción con construcción en tiempo real, defenderás los territorios humanos contra los Aumentados.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="360" src="https://www.youtube.com/embed/CkdECgrsgTo" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Sobre el Juego: 


*"Codex of Victory contiene una extensa campaña narrativa de un jugador que te permite construir y dirigir un ejército de alta tecnología compuesto por drones, tanques y robots. Te ofrece una emocionante mezcla entre construcción en tiempo real, estrategia a escala mundial y combate por turnos. Mientras viajas entre planetas y territorios, tu único objetivo será detener a los Aumentados.*


*Inspirado por los mejores clásicos del género, el sistema de construcción de la base te permite expandir tu cuartel general subterráneo en tiempo real. Consigue planos de salas, construye fábricas, centros de investigación, talleres, laboratorios y otras edificaciones para prepararte para las inevitables batallas. Al cambiar al mapa planetario, verás al momento dónde necesitas desplegar tu ejército."*


Entre las características destacan:


Combinación de combate por turnos y construcción en tiempo real.  
Más de 20 horas de campaña narrativa de un jugador.  
Batallas multijugador en línea con múltiples opciones de personalización de ejército.  
Más de 25 unidades únicas, cada una con incontables variaciones.  
Alta rejugabilidad asegurada gracias a las misiones generadas aleatoriamente.  
Ambientación neofeudal de ciencia ficción con vívidos gráficos cel shading.


Si te van los juegos de estratégia por turnos y quieres echarle un ojo, lo tienes traducido al español en su página de Steam:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/464100/" width="646"></iframe></div>


 

