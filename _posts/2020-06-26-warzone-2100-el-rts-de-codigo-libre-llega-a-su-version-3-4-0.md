---
author: Serjor
category: Estrategia
date: 2020-06-26 10:39:35
excerpt: "<p>Un listado enorme de mejoras y correcciones para todo un veterano de\
  \ guerra open source</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Warzone2100/Warzone_2100_1.webp
joomla_id: 1237
joomla_url: warzone-2100-el-rts-de-codigo-libre-llega-a-su-version-3-4-0
layout: post
tags:
- rts
- open-source
- warzone-2100
- codigo-abierto
- estrategia-en-tiempo-real
title: "Warzone 2100, el RTS de c\xF3digo libre, llega a su versi\xF3n 3.4.0"
---
Un listado enorme de mejoras y correcciones para todo un veterano de guerra open source


 Esta mañana amanecíamos en el canal de Telegram con el siguiente mensaje:


 



Y es que como podemos ver, Warzone 2100, un juego de estrategia en tiempo real que lleva con nosotros una larga cantidad de años, y que no deja de recibir mejoras, ha recibido oficialmente la versión [3.4.0](https://wz2100.net/news/version-3-4-0/).


La lista de cambios desde la versión [3.3.0](index.php/component/k2/1-blog-principal/estrategia/1229-warzone-2100-se-actualiza-a-la-version-3-3-0) que os anunciamos hace unos meses es tremendamente larga, podéis verla al completo [aquí](https://raw.githubusercontent.com/Warzone2100/warzone2100/3.4.0/ChangeLog), pero los cambios que los propios desarrolladores destacan es:


* Mejoras gráficas y de interfaz de usuario
* Añadida al menú la opción directa para continuar desde el último guardado
* Añadido el guardado rápido
* Añadido el autoguardado
* Añadida la opción de cambiar casi cualquier parámetro del juego desde el menú de pausa
* Añadido un atajo para directamente al menú principal tras ganar (si alguien me explica esto guay)
* Añadido el nivel de tecnología T4 donde toda la investigación está completada
* En los modos de juego mutijugador y escaramuza se ha añadido un botón para generar de manera aleatoria las opciones de la partida
* Añadido el soporte para cambiar el mapa, nombre de la partida y el nombre del usuario que está alojando la partida después de inicar el hosting de la misma
* Añadida la opción de configurar el modo OpenAL-HRTF
* Ahora hay widgets con notificaciones in-game
* Se puede cambiar La combinación de teclas para el desplazamiento de la cámara
* Soporte para Discord
* Contador de unidades y muertes en la interfaz gráfica
* Las IAs BoneCrusher!, Cobra, y Nexus se han importado del juego original
* Asignación rápida de la IA desde un slot existente al resto de slots
* Traducciones actualizadas
* Solución de errores y balance de parámetros en el modo campaña
* Errores varios (si sois jugadores asiduos, la lista es muy larga, igual os interesa echarle un ojo con detenimiento)


La verdad es que no pinta nada mal, y se agradece que un juego open source siga en activo después de tanto tiempo.


Y tú, ¿juegas a Warzone 2100? Cuéntanos qué te parecen estos cambios en los comentarios o en nuestros canales de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux)

