---
author: leillo1975
category: Estrategia
date: 2018-02-19 12:09:19
excerpt: "<p>@feralgames lanzar\xE1 el juego proximamente</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/32a94b4f860c9ea010c2508169f36f36.webp
joomla_id: 650
joomla_url: a-total-war-saga-thrones-of-britannia-llegara-a-linux-proximamente
layout: post
tags:
- feral-interactive
- total-war
- creative-assembly
- thrones-of-britannia
title: "\"A Total War Saga: Thrones of Britannia\" llegar\xE1 a Linux proximamente\
  \ (ACTUALIZADO 3)"
---
@feralgames lanzará el juego proximamente

**ACTUALIZACIÓN 22-5-18**: Anteriormente os habíamos comunicado que el juego se demoraría un poco con respecto a la versión de Windows. El juego , que llegará el jueves a MacOS , será lanzado para nuestros sistemas el próximo mes de Junio, sin concretar una fecha. Los requisitos técnicos serán comunicados también dentro de poco. Esperemos que sea más pronto que tarde y podamos disfrutar cuanto antes esta nueva entrega de la serie Total War. Teneis el anuncio de Twitter justo aquí debajo:



> 
> Linux generals! THRONES OF BRITANNIA, the first in the new Total War Saga series, is coming to Linux in June.  
>   
> System requirements will be announced shortly. In the meantime, prepare for your campaign of conquest on the minisite: <https://t.co/zFVPI5Gkns> [pic.twitter.com/GuAv52OmeE](https://t.co/GuAv52OmeE)
> 
> 
> — Feral Interactive (@feralgames) [May 22, 2018](https://twitter.com/feralgames/status/998865824961171456?ref_src=twsrc%5Etfw)



 




---


**ACTUALIZACIÓN 2-5-18**: Acabamos de leer en el Twitter de [@feralgames](https://twitter.com/feralgames?lang=es) que el juego **se retrasará uno o dos meses** después de la salida en Windows, que se espera para mañana, dia 3 de Mayo. Por lo que se ve tendremos que esperar un poco más de lo esperado para poder disfrutar de este nuevo título de Total War.



> 
> With THRONES OF BRITANNIA being released for Windows this week, we have an update for macOS and Linux monarchs.  
>   
> We are closing fast on the macOS and Linux versions, and are currently \*aiming\* for macOS and Linux releases one to two months after the Windows release on May 3rd. [pic.twitter.com/VfkTMpYlHu](https://t.co/VfkTMpYlHu)
> 
> 
> — Feral Interactive (@feralgames) [May 1, 2018](https://twitter.com/feralgames/status/991372884105990147?ref_src=twsrc%5Etfw)



 Esperemos que Feral pueda completar el port de "Thrones of Britannia" lo antes posible y no sufra nuevos retrasos.




---


**ACTUALIZACiÓN 22-3-18**: Acabamos de leer en [GamingOnLinux](https://www.gamingonlinux.com/articles/total-war-saga-thrones-of-britannia-has-been-dela.11447) que el juego se retrasará unas semanas, pasando (la versión de Windows) de salir el 19 de Abril al 3 de Mayo (el port llegará poco después).El medio británico explica que Creative Assembly,  tras mostrar el título a los medios especializados ha decidido aplazar ligeramente la fecha de salida. Esto se debe a que escuchando las opiniones de estos quieren retocar ciertos apectos del juego como la interfaz del usuario, la inteligencia artificial, el equilibrado de la campaña,  o que la condición de "Victoria definitiva" no esté estrictamente ceñida a los eventos históricos. Podeis consultar todos los detalles en el [Blog Oficial de Total War](https://www.totalwar.com/blog/britannia-release-date-change).


 




---


**NOTICIA ORIGINAL**: Tras una temporada bastante tranquila, los chicos de Feral vuelven a la carga, primero con el anuncio de [Rise of Tomb Raider](index.php/homepage/generos/accion/item/763-rise-of-the-tomb-raider-20-anniversario-llegara-a-gnu-linux-proximamente-de-la-mano-de-feral-interactive), y ahora con este **TW: Thrones of Britannia**. El juego saldrá, según ellos mismos, poco después de su llegada a Windows que se espera para el 19 de Abril. Normalmente Feral no suele retrasarse mucho cuando anuncia títulos con soporte "poco después", por lo que se supone que tardarán una semana o dos en traérnoslo. El anuncio de su próxima disponibilidad nos llegaba como suele ser habitual a través de Twitter:



> 
> A Total War™ Saga: THRONES OF BRITANNIA is coming to macOS and Linux shortly after Windows.  
>   
> Pre-order now from the Feral Store: <https://t.co/PHqtLnbufu>  
> or Steam: <https://t.co/fIS8ku4a2n>  
>   
> Kings will rise. One will rule. [pic.twitter.com/Xk18jH5QYG](https://t.co/Xk18jH5QYG)
> 
> 
> — Feral Interactive (@feralgames) [February 19, 2018](https://twitter.com/feralgames/status/965543600670085120?ref_src=twsrc%5Etfw)



 Según la descripción del juego que podemos encontrar en Steam, en Thrones of Britannia podemos encontrar:



> 
>   
> Año 878 d. C. El rey inglés Alfredo el Grande, asediado y tras una heroica defensa en la batalla de Edington, ha aplacado la invasión vikinga. Humillados, pero no destrozados, los señores de la guerra nórdicos se han asentado por toda Britania. Por primera vez en cerca de 80 años, la tierra se encuentra en un frágil estado de paz.  
>   
> A lo largo de esta isla soberana, los reyes de Inglaterra, Escocia, Irlanda y Gales perciben que se acercan tiempos de cambio. Tiempos que traerán nuevas oportunidades. Traerán tratados. Traerán guerra. Traerán reveses de la fortuna, que forjarán leyendas. Todo en una saga que ilustra el ascenso de una de las naciones más grandes de la historia.  
>   
> *Se alzarán reyes. Solo uno reinará.*  
>   
> Thrones of Britannia es un juego independiente de la saga Total War que te retará a reescribir un momento crucial de la historia, un momento que definirá el futuro de la Britania moderna. Ponte al mando de diez facciones para erigir y defender un reino para la gloria de anglosajones, clanes gaélicos, tribus galesas o asentamientos vikingos. Forja alianzas, gestiona asentamientos florecientes, recluta ejércitos y emprende campañas de conquista por el mapa de Total War más detallado hasta la fecha.
> 
> 
> 


 


Como veis el juego estará ambientado en la **baja edad media**, donde podremos encarnar a **Vikingos** (si, Vikingos), sajones, Gaélicos, etc; en una batalla por dominar Gran Bretaña, algo que podemos ver en series tan de moda como [Vikings](https://es.wikipedia.org/wiki/Vikingos_(serie_de_televisi%C3%B3n)) o [The Last Kingdom](https://es.wikipedia.org/wiki/The_Last_Kingdom). Una localización y periodo histórico que como nos suelen tener acostumbrados en Creative Assembly, destaca e interesa a partes iguales. Como muchos de vosotros sabreis, no es la primera vez que Feral y Creative Assembly nos tren juegos de la serie Total War, pues en el pasado ya pudimos disfrutar de **Empire**, **Medieval**, **Attila**, [Shogun II](index.php/homepage/analisis/item/487-analisis-total-war-shogun-2-y-fall-of-the-samurai) o **Warhammer**. Por lo que podemos intuir estos juegos deben de dsfrutar de buena aceptación por parte de la comunidad, y de ahí el soporte tan destacable que tenemos de estos productos.


Por el momento aun no tenemos información sobre los requerimientos del sistema, o si hará uso de Vulkan o OpenGL (o ambos), pero como suele ser habitual  este tipo de cuestiones se suelen resolver a pocos días de la fecha definitiva. El juego podremos adquirirlo como suele ser habitual, a traves de la [tienda de Feral (recomendado)](https://store.feralinteractive.com/es/mac-linux-games/thronesofbritanniatw/), o [Steam](http://store.steampowered.com/app/712100/Total_War_Saga_Thrones_of_Britannia/). Como siempre desde JugandoEnLinux os ofreceremos toda la información disponible sobre este título a medida que la vayamos conociendo. Os dejamos con el video del anuncio:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/rXB33OWu7_I" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 ¿Os gustan los juegos de la serie Total War? ¿Qué opinais de la temática elegida para esta nueva saga? Dinos lo que piensas en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

