---
author: leillo1975
category: Software
date: 2019-03-26 22:54:19
excerpt: "<p class=\"ProfileHeaderCard-screenname u-inlineBlock u-dir\" dir=\"ltr\"\
  ><span class=\"username u-dir\" dir=\"ltr\">Nueva versi\xF3n en esta \xFAltima rama<br\
  \ /></span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4ab6f5fd703b5bb734c4bf88679e8b92.webp
joomla_id: 1007
joomla_url: actualizacion-importante-de-steam-play-proton-alcanza-la-version-4-2
layout: post
tags:
- wine
- valve
- steam-play
- dxvk
- proton
title: "Actualizaci\xF3n Importante de Steam Play: Proton alcanza la versi\xF3n 4.2\
  \ (ACTUALIZADO)"
---
Nueva versión en esta última rama  


**ACTUALIZACIÓN 3-4-19:** 


Acabamos de saber que la última versión de Proton acaba de actualizarse alcanzando la 4.2-2. En este caso los cambios no parecen tan importantes, pero demuestran el paso constante de Valve en aumentar la compatibilidad y rendimiento de su herramienta. Las [novedades](https://github.com/ValveSoftware/Proton/wiki/Changelog) son las siguientes (traducción online):


-Se han corregido los parámetros de la línea de comandos de algunos juegos, incluidos los juegos de Wadjet Eye como Blackwell Epiphany.  
 -Arreglados algunos juegos que fallaban o se cerraban con ciertas localizaciones como el Turco.  
 -Actualizado FAudio a 19.03-25-g8105923.  
 -Arreglado un fallo al hacer Alt-Tab en Deus Ex.  
 -Restablecida la funcionalidad anterior del instalador de.NET.


 




---


**NOTICIA ORIGINAL:**   
Justo hoy mismo se hablaba de esto en nuestro grupo de [Telegram](https://t.me/jugandoenlinux), pues ciertamente hace un par de meses que [la versión 4 de Wine](index.php/homepage/generos/software/12-software/1077-wine-4-0-ya-esta-aqui) está oficialmente entre nosotros, y como sabeis este proyecto es junto con **DXVK** la parte más importante de Steam Play-Proton. La noticia nos llegaba, como suele ser habitual, a través de  [Pierre-Loup Griffais](https://twitter.com/Plagman2), de Valve Software en twitter:



> 
> Proton 4.2 has just been released!  
>   
> You'll want to locate it in your Library ("Tools" category) and install it manually before setting any games to use it. That process is supposed to be automated, but there is currently a bug with games that use another redistributable. :( [pic.twitter.com/MmO0kcbzrY](https://t.co/MmO0kcbzrY)
> 
> 
> — Pierre-Loup Griffais (@Plagman2) [26 de marzo de 2019](https://twitter.com/Plagman2/status/1110665993590759425?ref_src=twsrc%5Etfw)


  





Como podeis ver, en el propio tweet incluye las [**numerosas novedades**](https://github.com/ValveSoftware/Proton/wiki/Changelog), que podeis ver aquí traducidas a nuestro idioma (traducción online):


- Rebasados los parches de Proton sobre el Wine 4.2. Hay más de 2.400 mejoras en Wine entre esas versiones. 166 parches de Proton 3.16 se han actualizado o ya no son necesarios.  
- Actualizado DXVK a 1.0.1.  
- Actualizado FAudio a 19.03-13-gd07f69f.  
- Mejoras en el comportamiento del cursor del mouse para juegos incluidos Resident Evil 2 y Devil May Cry 5.  
- Correcciones para redes en NBA 2K19 y NBA 2K18.  
- Se corrigió la duplicación del controlador en RiME y otros juegos que usan SDL2.  
- Mejoras en las configuraciones regionales CJK y soporte de fuentes.  
- Vulkan de Wine ahora es compatible con la versión 1.1.104 y anuncia la compatibilidad con la versión 1.1 a las aplicaciones.  
 -El hack de pantalla completa de Proton ahora funciona para juegos basados en GDI.  
- Mejor soporte para juegos que usan IVRInput para la entrada del controlador en VR.  
- Otras mejoras y nuevas características en el sistema de compilación "ruta fácil". Ejecute "make help" en el directorio de Proton para la documentación.


Si quereis descargar esta nueva versión **debeis descargarlo de la sección "Herramientas" de vuestra Biblioteca** de Steam (buscad Proton y lo vereis enseguida). Pues nada, no perdamos más tiempo y empecemos a probar y disfrutar de nuestros juegos. Podeis dejar vuestros opiniones sobre Steam Play- Proton en los comentarios o charlar con nosotros sobre ello en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

