---
author: leillo1975
category: Software
date: 2020-01-29 14:27:35
excerpt: "<p>\"Con la calidad como prioridad\" nos llega este \xFAltimo lanzamiento\
  \ de @GodotEngine</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/GODOT/GodotLogo.webp
joomla_id: 1156
joomla_url: disponible-godot-3-2
layout: post
tags:
- open-source
- motor-grafico
- godot
title: 'Disponible Godot 3.2. '
---
"Con la calidad como prioridad" nos llega este último lanzamiento de @GodotEngine


 El progreso y proyección de este motor de videojuegos en los últimos tiempos es asombroso. Como sabeis el salto dado con el lanzamiento de la [versión 3.0](index.php/component/k2/12-software/745-el-motor-de-juegos-godot-llega-finalmente-a-su-version-3-0) hace un par de años, y su [primera revisión](index.php/component/k2/12-software/1117-godot-alcanza-la-version-3-1) un año después ha sido enorme, **poniendo este software muy cerca de soluciones privativas comerciales**. Teniendo en cuenta que la próxima meta del proyecto es el lanzamiento de la versión 4.0, donde los cambios serán muy importantes, los desarrolladores han querido convertir este nuevo lanzamiento en un una **versión pensada en ofrecer un entorno de desarrollo muy estable y funcional** a los usuarios de Godot.


A pesar de que la mayor parte de la carne que se ha puesto en el asador actualmente está en la **implementación de Vulkan para la 4.0**, de la cual tenemos numerosos reportes de [progreso](https://godotengine.org/article/vulkan-progress-report-6), esto no quiere decir que lo que nos vamos a encontrar en la 3.2 sea menos importante. En un principio se pensó que sería una versión menor con basicamente correcciones, optimizaciones y caracteríticas menores, pero finalmente se ha ampliado el desarrollo convirtiéndola en algo tan importante como lo fué en su día la 3.1. **Todo esto se ha hecho se ha realizado sin perder de vista la compatibilidad con las versiones anteriores**, aunque es posible que en proyectos muy complejos haya que realizar algunos ajustes. El equipo de Godot recomienda a los desarrolladores que actualicen a esta nueva versión


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/N-UReOuxAP0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


No vamos a entrar al detalle en los cambios que vamos a encontrar en esta nueva versión porque resultan en muchos casos tremendamente técnicos para tratar aquí (estaría basicamente intentando explicar algo que no entiendo), por lo os recomendamos que vayais al [artículo oficial](https://godotengine.org/article/here-comes-godot-3-2) donde se describen. Entre otros muchos [cambios](https://github.com/godotengine/godot/blob/master/CHANGELOG.md#32---2020-01-29), vamos a simplemente a enumerar los siguientes:


*-Documentación: Más contenido, mejor tema y Traducciones*  
 *-Mono/C#: Soporte para Android y WebAssembly*  
 *-AR/VR: Oculus Quest y el apoyo de ARKit*  
 *-Revisión de los sombreadores visuales*  
 *-Mejoras en los gráficos y la representación*  
 *-Canalización de assets 3D: glTF 2.0 y FBX*  
 *-Trabajo en Red: WebRTC y WebSocket*  
 *-Sistemas de construcción y plugin para Android*  
 *-Caracterísiticas nuevas del Editor*  
 *-Herramientas de código*  
 *-2D: Pseudo 3D, Atlas de texturas, AStar2D*  
 *-GUI: Anchor/margins workflow, efectos RichTextLabel*  
 *-Generadores de audio y analizador de espectro*  
 *-Mejora de la descomposición convexa*


Como veis el trabajo ha sido intensísimo en este último año para poder lanzar esta nueva versión 3.2 justamente dos años despueś de la 3.0. Ahora solo queda desearles lo mejor para que continuen avanzando para llegar con éxito a la 4.0, donde los cambios serán de mucho más calado. Nosotros por nuestra parte permaneceremos atentos para informaros puntualmente.


![Godot3.2](/https://godotengine.org/storage/app/media/vshader2019/vs_copy_paste.webp)


¿Habeis usado Godot Engine en alguna ocasión? ¿Qué opinión os merece este software Open Source? Puedes contarnos lo que quieras sobre esta noticia en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

