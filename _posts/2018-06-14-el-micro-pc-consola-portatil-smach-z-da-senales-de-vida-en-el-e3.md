---
author: Pato
category: Hardware
date: 2018-06-14 11:22:42
excerpt: "<p>Al parecer @SMACH_Team est\xE1 promocionando el hardware con vistas a\
  \ recabar apoyos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c230427c303c0684b5582388f5d0dfd7.webp
joomla_id: 771
joomla_url: el-micro-pc-consola-portatil-smach-z-da-senales-de-vida-en-el-e3
layout: post
tags:
- portatil
- hardware
- smach-z
title: "El \"micro-pc-consola\" portatil Smach Z da se\xF1ales de vida en el E3"
---
Al parecer @SMACH_Team está promocionando el hardware con vistas a recabar apoyos

Ayer nos llegaban noticias [vía twitter](https://twitter.com/SMACH_Team/status/1006612711193505792) de que el "micro-pc-consola" portatil **Smach Z** está dando señales de vida en este E3 donde parece que están mostrando algunos prototipos de diseño. Como ya sabéis, se trata de una portátil desarrollada por una empresa española que ha estado [frecuentando](index.php/homepage/hardware/item/47-el-micropc-portatil-para-jugar-smach-z-vuelve-a-kickstarter) las plataformas de crowfunding durante algún tiempo, y de la que ya hemos hablado en otras ocasiones. Incluso se han publicado las que serían las especificaciones finales, montando una **APU Ryzen con gráficos Radeon**, una pantalla de 6" tactil y dos pads hápticos al estilo del Steam Controller y tendrá la **opción de montar un sistema operativo Linux propio** desarrollado por la compañía. De hecho, esta es la opción que ellos mismos han recomendado. Puedes ver mas detalles en el [artículo](index.php/homepage/hardware/item/782-smach-z-presenta-sus-especificaciones-finales) que publicamos en su momento.


Ahora, aprovechando el tirón del E3 la compañía ha publicado un vídeo promocional:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/g68bH2E8B3g" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Por otra parte, y tirando un poco del "hilo" parece que la compañía **quiere mostrar un prototipo funcional para la Gamescom** (al parecer aún no han mostrado una Smach Z en funcionamiento) y planean un lanzamiento oficial para final de año (En la tienda de su página oficial aparece Septiembre) con un precio de partida de unos 630€, que se irá incrementado según modelos y mayores especificaciones.


Si quieres saber más, puedes visitar la [página oficial](https://www.smachz.com/) de Smach Z donde puedes ver todas las opciones.


Hay muchas voces que opinan que Smach Z es scam, y que no llegarán a lanzar el producto. ¿Vosotros que pensáis? ¿Llegarán a lanzar la consola a final de año? ¿Crees que tendrá éxito esta Smach Z? ¿Te gustaría comprar una?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

