---
author: Pato
category: Plataformas
date: 2018-08-22 17:07:35
excerpt: <p>Koji Igarashi hace un nuevo comunicado para explicar el motivo</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1e8d6dc02986e7e13576b16a6f372759.webp
joomla_id: 841
joomla_url: bloodstained-ritual-of-the-night-sufre-un-nuevo-retraso-y-se-va-a-2019
layout: post
tags:
- accion
- plataformas
- proximamente
title: 'Bloodstained: Ritual Of The Night sufre un nuevo retraso y se va a 2019'
---
Koji Igarashi hace un nuevo comunicado para explicar el motivo

Bloodstained: Ritual Of The Night lleva camino de ser un desarrollo de lo mas accidentado, acumulando retraso tras retraso. Esta vez ha sido el propio Igarashi el que ha publicado un comunicado explicando que necesitan mas tiempo para pulir el juego e introducir algunas de las sugerencias y novedades que los usuarios de las versiones beta les han reportado. Puedes ver el vídeo a continuación:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/dYjitCWM4ew" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Bloodstained: Ritual Of The Night quiere recoger el testigo del Castlevania Simphony of the Night desarrollado por el propio Iga y que creó escuela, siendo un título de lo más notable dentro de la saga Castlevania.


Tienes mas información en su [página de Kickstarter](https://www.kickstarter.com/projects/iga/bloodstained-ritual-of-the-night) o en [Steam](https://store.steampowered.com/app/692850/Bloodstained__Ritual_of_the_Night/).

