---
author: Jugando en Linux
category: Terror
date: 2017-10-31 10:27:55
excerpt: "<p>Sigue nuestra retransmisi\xF3n en vivo del unboxing de <strong>&gt;Observer_</strong>\
  \ y participa en el sorteo de una clave de Outlast</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b4787a4a5d6711adf08fa27fc1cba139.webp
joomla_id: 515
joomla_url: noche-de-halloween-en-jugando-en-linux-streaming-y-sorteo
layout: post
tags:
- streaming
- sorteo
title: "Noche de Halloween en Jugando en Linux: \xA1Streaming y sorteo!"
---
Sigue nuestra retransmisión en vivo del unboxing de **>Observer_** y participa en el sorteo de una clave de Outlast

¡Vaya noche de Halloween nos espera! tenemos un streaming preparado para la ocasión. **Gracias a la generosidad de Aspyr Media**, Serjor nos hará el **"unboxing" de '>Observer_'** y junto a el nos adentraremos en esta aventura terrorífica en [nuestro canal de Twitch](https://www.twitch.tv/jugandoenlinux), (también estará disponible en nuestro [canal de Youtube](https://www.youtube.com/channel/UC4FQomVeKlE-KEd3Wh2B3Xw)).


Además de esto, ¡vamos a sortear durante la partida **una clave del juego "[Outlast](http://store.steampowered.com/app/238320/Outlast/)"** entre todos los que entréis en el chat del canal de [twitch](https://www.twitch.tv/jugandoenlinux) o [discord](https://discord.gg/ftcmBjD) y saludéis! ¡No te lo pierdas!


Os dejamos dos vídeos de los dos juegos:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/M-10n_U6FeQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/2GPf3MdVOKI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>

