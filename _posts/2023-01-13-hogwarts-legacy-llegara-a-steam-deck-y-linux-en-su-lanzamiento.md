---
author: Pato
category: Rol
date: 2023-01-13 21:04:02
excerpt: "<p>Steam Deck sigue sumando lanzamientos importantes a su cat\xE1logo</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HogwartsLegacy/Hogwarts_Legacy1.webp
joomla_id: 1522
joomla_url: hogwarts-legacy-llegara-a-steam-deck-y-linux-en-su-lanzamiento
layout: post
tags:
- rol
- exploracion
- steam-deck
- hogwarts-legacy
title: "Hogwarts Legacy llegar\xE1 a Steam Deck y Linux en su lanzamiento"
---
Steam Deck sigue sumando lanzamientos importantes a su catálogo


El éxito de la Steam Deck sigue aumentando, y la prueba es que sigue sumando títulos importantes a su catálogo ya sea como jugables o como en el caso que nos ocupa como verificados directamente.


El próximo juego del universo de Harry Potter se llama **Hogwarts Legacy**, y es uno de los más deseados y esperados dentro de la comunidad de este año. Tanto es así que se encuentra aún en pre-reserva y figura como el título más deseado en Steam. 


Se trata de un juego de rol de mundo abierto para un jugador en el que podremos explorar el famoso castillo de Hogwarts y sus alrededores dentro de un universo situado en algún momento del siglo XIX, por lo que no veremos los famosos personajes de las películas, pero en el que podremos vivir nuestras propias y mágicas aventuras, volar en escobas, aprender hechizos, visitar poblados cercanos y mucho más en el icónico colegio Hogwarts de magia y hechicería.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/BsC-Rl9GYy0" title="YouTube video player" width="560"></iframe></div>


 


 Como es lógico dada la amplitud y elementos gráficos dentro del juego, se trata de un título con unos requisitos para PC bastante exigentes por lo que algunos usuarios andaban preocupados por si podrían disfrutar del título en sus Steam Decks. Uno de ellos preguntó directamente en Twitter en la cuenta de Warner Bros Games, a lo que tras consultar con el equipo de programación confirmaron la noticia:



> 
> Hello again, David! We reached out to the Hogwarts Legacy team for you and were able to confirm that the game WILL be Steam Deck verified on launch. We hope this helps with your decision! Take care.
> 
> 
> — WB Games Support (@WBGamesSupport) [January 12, 2023](https://twitter.com/WBGamesSupport/status/1613595212982489090?ref_src=twsrc%5Etfw)



*Hola de nuevo, David! Nos pusimos en contacto con el equipo de Hogwarts Legacy por ti y pudimos confirmar que el juego SERÁ verificado para Steam Deck en el lanzamiento. Esperamos que esto ayude con tu decisión! Cuídate.*


Como podéis ver, **el juego será verificado para Steam Deck de salida**, y además **lo podremos jugar en nuestros PCs con Linux.**


En cualquier  caso y por si ya os estáis viendo recorrer los pasillos de Hogwarts, los requisitos para PC no son muy asequibles. Aquí los tenéis:




| Requisitos mínimos |
| SO | Windows 10 de 64 bits |
| CPU | Intel Core i5-6600 (3.3 GHz) o AMD Ryzen 5 1400 (3.2 GHz) |
| RAM | 16 GB |
| GPU | NVIDIA GeForce GTX 960 4GB o AMD Radeon RX 470 4GB |
| VERSIÓN DX | DX 12 |
| ALMACENAMIENTO | 85 GB HDD |
| NOTAS | SSD (preferido), HDD (compatible), 720p/30 fps, ajustes de calidad baja |




| Requisitos recomendados/Altos |
| SO | Windows 10 de 64 bits |
| CPU | Intel i7-8700 (3.2 GHz) o AMD Ryzen 5 3600 (3.6 GHz) |
| RAM | 16 GB |
| GPU | NVIDIA GeForce 1080 Ti o AMD Radeon RX 5700 XT o INTEL Arc A770 |
| VERSIÓN DX | DX 12 |
| ALMACENAMIENTO | 85 GB SSD |
| NOTAS | SSD, 1080p/60 fps, ajustes de calidad alta |




| Requisitos ultra |
| SO | Windows 10 de 64 bits |
| CPU | Intel Core i7-10700K (3.80 GHz) o AMD Ryzen 7 5800X (3.80 GHz) |
| RAM | 32 GB |
| GPU | NVIDIA GeForce RTX 2080 Ti o AMD Radeon RX 6800 XT |
| VERSIÓN DX | DX 12 |
| ALMACENAMIENTO | 85 GB SSD |
| NOTAS | SSD, 1440p / 60fps, ajustes de calidad ultra |




| Requisitos ultra 4k |
| SO | Windows 10 de 64 bits |
| CPU | Intel Core i7-10700K (3.80 GHz) o AMD Ryzen 7 5800X (3.80 GHz) |
| RAM | 32 GB |
| GPU | NVIDIA GeForce RTX 3090 Ti o AMD Radeon RX 7900 XT |
| VERSIÓN DX | DX 12 |
| ALMACENAMIENTO | 85 GB SSD |
| NOTAS | SSD, 2160p / 60fps, ajustes de calidad ultra |


Además de esto, también está confirmado que el juego soportará las tecnologías **Nvidia DLSS y AMD - FSR** por lo que será posible tirar de reescalado para lo que haga falta.



> 
> Hi there Matheus and thanks for reaching out! AMD FSR is supported! Please let us know if you have any further questions. We're here to help!
> 
> 
> — WB Games Support (@WBGamesSupport) [January 13, 2023](https://twitter.com/WBGamesSupport/status/1613912030855004163?ref_src=twsrc%5Etfw)



Por último, recordaros que el juego de Warner Bros Games está siendo desarrollado por Avalanche y Portkey Games, y **llegará a nuestros PC/Steam Decks el próximo 10 de Febrero**. Si quereis saber más sobre Hogwarts Legacy podéis visita[r su web oficial](https://www.hogwartslegacy.com/es-es), o si queréis reservar/comprar podéis ir [a su página en Steam](https://store.steampowered.com/app/990080/Hogwarts_Legacy/).


Cuéntame que te parece que un triple A como Hogwarts Legacy llegue con soporte de salida para nuestro sistema en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

