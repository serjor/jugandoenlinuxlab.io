---
author: Pato
category: Hardware
date: 2022-02-25 19:15:23
excerpt: "Steam a\xF1ade un apartado donde puedes ver la tienda con todos los t\xED\
  tulos que puedes ejecutar"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/valve_steamdeck_photo_skus.webp
joomla_id: 1439
joomla_url: steam-deck-ya-esta-aqui-y-con-un-corto-jugable-de-valve-para-la-deck
layout: post
tags:
- steam
- steam-deck
title: "Steam Deck ya est\xE1 aqu\xED! y con un corto jugable de Valve para la Deck!"
---
Steam añade un apartado donde puedes ver la tienda con todos los títulos que puedes ejecutar


Pues ya está!. Steam Deck es una realidad y los correos están siendo eviados. El embargo se ha terminado y ahora mismo hay una plétora de medios ofreciendo toda clase de contenidos.


Además, Steam ha añadido un apartado para que puedas ver en la tienda los títulos compatibles con la máquina de un vistazo. ¿Donde?


Aquí: <https://store.steampowered.com/greatondeck>


Quieres saber más?... La página oficial de Steam Deck ha sido actualizada y ahora ya puedes ver todo lo que la máquina puede ofrecer. ¿Donde?


Aquí: <https://www.steamdeck.com/es/>


Además, Valve ha hehco público un vídeo promocional de Steam Deck:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/AvokyBOYe8Y" title="YouTube video player" width="560"></iframe></div>


Y como no, no podía faltar algo por parte de Valve... En este caso, un corto jugable enfocado en la Steam Deck:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/mVDFJRM6F9k" title="YouTube video player" width="560"></iframe></div>


Puedes encontrarlo aquí: <https://store.steampowered.com/app/1902490/Aperture_Desk_Job/>


¿Quieres mas?... Unete a nosotros esta noche a las 22:30 GMT+1 en nuestro Podcast de Jugando en Linux donde hablaremos largo y tendido sobre toda la información y reviews que ya están saliendo a la luz. ¡No te lo pierdas!. ¿Donde?:


[Twitch](https://www.twitch.tv/jugandoenlinux)

