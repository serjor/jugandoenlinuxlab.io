---
author: leillo1975
category: Estrategia
date: 2018-03-22 08:14:17
excerpt: "<p><span class=\"username u-dir\" dir=\"ltr\">@<b class=\"u-linkComplex-target\"\
  >AspyrMedia</b></span> ha completado el port de esta expansi\xF3n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/223d3e825305be1390f95e96c8b494f2.webp
joomla_id: 686
joomla_url: rise-fall-la-nueva-expansion-para-civilization-vi-disponible-para-linux-steamos
layout: post
tags:
- dlc
- civilization-vi
- firaxis
- expansion
- aspyr
- rise-fall
title: "Rise & Fall, la nueva expansi\xF3n para Civilization VI, disponible para Linux/SteamOS\
  \ (ACTUALIZADO)"
---
@**AspyrMedia** ha completado el port de esta expansión

**ACTUALIZACIÓN**: El juego acaba de ser anunciado oficialmente en este tweet:



> 
> [@CivGame](https://twitter.com/CivGame?ref_src=twsrc%5Etfw) [#RiseandFall](https://twitter.com/hashtag/RiseandFall?src=hash&ref_src=twsrc%5Etfw) brings new choices, strategies, and challenges for players as they guide a civilization through the ages. Now Available for [#Mac](https://twitter.com/hashtag/Mac?src=hash&ref_src=twsrc%5Etfw) and [#Linux](https://twitter.com/hashtag/Linux?src=hash&ref_src=twsrc%5Etfw)! <https://t.co/X4VAmjH4xv> [#OneMoreTurn](https://twitter.com/hashtag/OneMoreTurn?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/QtsdXQD2yO](https://t.co/QtsdXQD2yO)
> 
> 
> — Aspyr Media (@AspyrMedia) [March 22, 2018](https://twitter.com/AspyrMedia/status/976851191878574086?ref_src=twsrc%5Etfw)



También estamos en disposición de ofreceros pronto nuevos contenidos sobre esta expansión gracias a la clave facilitada por [@**AspyrMedia**](https://twitter.com/AspyrMedia), por lo que pronto haremos algún **streaming** y/o **videos**, y en unas semanas un **completo análisis** con las novedades que presenta esta DLC.


 




---


Hace unos meses [os ofrecíamos expectantes la noticia](index.php/homepage/generos/estrategia/item/678-rise-fall-sera-la-proxima-gran-expansion-de-civilization-vi) de que Firaxis Games estaba preparando una nueva expansión que llegaría en el pasado mes de Febrero. También, poco después os confirmaríamos que Aspyr tardaría un poco más en tenerla lista. Finalmente se ha demorado poco más de un mes y ya la tenemos aquí, por lo que la espera ha sido corta. La conocida compañía de ports no ha confirmado oficialmente su disponibilidad pero en Steam el juego ya muestra el famoso iconito de SteamOS como podeis ver en el widget al fondo de este artículo. 


Ya ha pasado más de un año desde que Civilization VI vió la luz en nuestros sistemas, y en JugandoEnLinux.com os ofrecimos un [completo análisis](index.php/homepage/analisis/item/352-analisis-sid-meier-s-civilization-vi) de este título. También,  como os dijimos en el anterior artículo, **las novedades de este Rise & Fall son muchas y de calado**, destacando entre ellas que nuestra civilización pasará por edades de explendor y tiempos de crisis. También la diplomacia y la influencia sobre nuestros ciudadanos y las ciudades neutrales será esencial, así como el el uso de gobernadores. Podeis ver todo esto con más detalle en la descripción oficial de la expansión:



> 
> *Civilization VI* es un juego que trata de construir un imperio que resista el paso del tiempo, y la ampliación *Rise and Fall* aporta nuevas opciones, estrategias y desafíos a los jugadores a medida que estos conducen una civilización por distintas épocas. ¿Serás capaz de inspirar lealtad a todas las gentes del mundo? ¿Fundarás una edad de oro para tu civilización o te verás abocado a una época oscura? En *Civilization VI: Rise and Fall*, te convertirás en un líder de verdad a través de los tiempos.  
>   
> Si diriges bien una civilización, podrás llevarla a una próspera edad de oro, pero regazarse puede conllevar la llegada de una época oscura. Responde bien a los desafíos, y tu civilización podrá resurgir y alcanzar una época heroica.  
>   
> Alienta la lealtad de los ciudadanos para mantener tus fronteras intactas o inspira lealtad a otras civilizaciones para expandir tu imperio. Las fronteras del mundo se mueven y cambian constantemente mientras de los imperios emergen ciudades libres y los países vecinos compiten por su lealtad por todo el mapa.  
>   
> Con el nuevo sistema de gobernadores, los jugadores pueden personalizar y especializar todavía más sus ciudades, así como reaccionar a los nuevos retos que plantean las épocas oscuras y la lealtad. Cada uno de los siete gobernadores tiene su propio árbol de ascensos, que se adapta a distintos estilos y estrategias de juego.   
>   
> Además de estos nuevos sistemas, *Civilization VI: Rise and Fall* introduce ocho civilizaciones y nueve líderes más. Se pueden construir ocho maravillas del mundo adicionales, así como gran variedad de nuevas unidades, distritos, edificios y mejoras. Ahora hay más maneras que nunca de construir, conquistar e inspirar.  
>   
> 
> 
> 
> 


* > 
> #### ÉPOCAS
> 
> 
> a medida que tu civilización pase por distintos altibajos y alcances momentos históricos clave, avanzarás hacia una época oscura o una edad de oro y cada una de ellas te ofrecerá retos y bonificaciones específicas en función de tus acciones en el juego. Resurge triunfalmente de una época oscura, y tu siguiente edad de oro será todavía más pujante, una época heroica.
* > 
> #### LEALTAD
> 
> 
> ahora las ciudades muestran su propia lealtad a tu liderato: como esta baje demasiado, pagarás las consecuencias en forma de rendimientos escasos, revueltas y la posible pérdida de la ciudad en manos de otra civilización o porque esta se independice. Pero la pérdida de una civilización puede redundar en tu favor si inspiras lealtad a las ciudades del mapa y expandes todavía más tus fronteras.
* > 
> #### GOBERNADORES
> 
> 
> recluta, nombra y mejora a personajes poderosos con sus propias bonificaciones de especialización y árboles de ascenso para modelar tus ciudades y reforzar su lealtad
* > 
> #### ALIANZAS MEJORADAS
> 
> 
> un sistema avanzado de coaliciones permite a los jugadores formar distintos tipos de alianzas y conseguir bonificaciones con el tiempo.
* > 
> #### EMERGENCIAS
> 
> 
> cuando una civilización se vuelve demasiado poderosa, otras civilizaciones pueden unirse en un pacto para acabar con esa amenaza y obtener recompensas o penalizaciones cuando cese la emergencia.
* > 
> #### CRONOLOGÍA
> 
> 
> repasa la historia de tu civilización en cualquier momento con esta nueva función, un viaje visual por los momentos históricos que has encontrado en tu camino a la victoria.
* > 
> #### NUEVOS LÍDERES Y CIVILIZACIONES
> 
> 
> se añaden nueve líderes y ocho civilizaciones más, cada uno con sus propias bonificaciones y maneras de jugar, así como un total de ocho unidades, dos edificios, cuatro mejoras y dos distritos, todos ellos exclusivos.
* > 
> #### MÁS CONTENIDO GLOBAL
> 
> 
> se han añadido ocho maravillas del mundo y siete maravillas naturales, cuatro unidades, dos mejoras, dos distritos, catorce edificios y tres recursos, todos ellos nuevos.
* > 
> #### SISTEMA DE JUEGO MEJORADO
> 
> 
> el sistema de gobierno se ha visto mejorado con nuevas políticas y se han introducido más mejoras en los sistemas existentes.


 


Os dejamos con el trailer de esta jugosa expansión:


 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/pZX9m2XN_LA" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Podeis adquirir Civilization VI: Rise and Fall en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/645402/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


 ¿Habeis disfrutado ya de las bondades de Civilization VI? ¿Que os parecen las novedades que ofrece esta DLC? Déjanos tus impresiones en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

