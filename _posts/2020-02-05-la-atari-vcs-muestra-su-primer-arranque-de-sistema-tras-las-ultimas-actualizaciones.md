---
author: Pato
category: Hardware
date: 2020-02-05 21:41:00
excerpt: "<p>La consola con coraz\xF3n Linux sigue en fase de pre-producci\xF3n</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/Ataribox1.webp
joomla_id: 1165
joomla_url: la-atari-vcs-muestra-su-primer-arranque-de-sistema-tras-las-ultimas-actualizaciones
layout: post
tags:
- atari
- atarivcs
title: "La Atari VCS muestra su primer arranque de sistema tras las \xFAltimas actualizaciones"
---
La consola con corazón Linux sigue en fase de pre-producción


Lenta pero constante, parece que **Atari** va mostrando el progreso en el desarrollo de la que será su **Atari VCS**, la consola con corazón Linux que supondrá la vuelta de la mítica compañía al desarrollo de hardware de entretenimiento.


Esta vez, tras la [última actualización](https://medium.com/@atarivcs/ces-2020-recap-from-the-atari-vcs-meeting-suite-1ab66df4b6a) de Enero donde mostraban las primeras fases de pre-producción de la consola y después de pasar por el CES, la feria tecnológica donde afirman haber contactado con diversas compañías interesadas en desarrollar para la nueva consola ahora el equipo ha vuelto de China, donde se está llevando a cabo la fase de pre-producción.


![Atariprepro1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/Atariprepro1.webp)


Según nos cuentan se van cumpliendo fases en el refinamiento y ensamblaje del producto, aunque se están centrando en tiradas cortas destinadas a la entrega en acceso temprano para los desarrolladores y partners.


![Atariprepro2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/Atariprepro2.webp)


Según la información, las placas base de estas nuevas tiradas ya han sido actualizadas con las nuevas mejoras, aunque las cajas de ensamblaje aún no han sido modificadas. Por otra parte, tras volver de China el equipo se está centrando en el desarrollo del sistema operativo de la consola, y han publicado un vídeo en el que muestran algunas de sus características.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/v1wHGgt5BbI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Según podemos ver, **se trata de una unidad de pre-producción** en la que vemos su conexión y puesta en marcha ejecutando juegos mediante Antstream, Atari Vault y diversos servicios de streaming por internet mediante el navegador incorporado. Por supuesto, todo es WIP, aunque afirman que el sistema ha sido actualizado desde el CES.


Recordamos que el sistema de la Atari VCS **está basado en Debian** y que será capaz de ejecutar los juegos programados para Linux, además de ser capaz de arrancar otros sistemas operativos mediante el modo sandbox con los que podremos ejecutar otros programas como Steam, por ejemplo.


Si quieres saber más, tienes toda la información y un buen puñado de fotos en su [última actualizacion en Medium](https://medium.com/@atarivcs/atari-vcs-manufacturing-update-cdec2835afa7).


Y tu, ¿qué piensas de el progreso de la Atari VCS? ¿Te parece interesante el desarrollo de una consola con sistema operativo Linux?


Cuéntanoslo en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

