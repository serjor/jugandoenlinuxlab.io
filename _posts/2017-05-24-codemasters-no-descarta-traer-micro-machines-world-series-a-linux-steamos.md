---
author: leillo1975
category: Arcade
date: 2017-05-24 14:18:01
excerpt: <p>Hay posibilidades de que disfrutemos en un futuro de el nuevo juego de
  la popular franquicia de juguetes</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9189082f4804c1ab16e77d2cfe8d09d4.webp
joomla_id: 339
joomla_url: codemasters-no-descarta-traer-micro-machines-world-series-a-linux-steamos
layout: post
tags:
- codemasters
- micro-machines
- steamdb
title: "[EXCLUSIVA] Codemasters no descarta traer Micro Machines World Series a Linux/SteamOS\
  \ (ACTUALIZACI\xD3N)"
---
Hay posibilidades de que disfrutemos en un futuro de el nuevo juego de la popular franquicia de juguetes

Esta mañana mi compañero Pato nos llamaba la atención sobre la aparición del iconito de Tux en la sección de Micro Machines World Series en SteamDB. Al tratarse de un juego de Codemasters esta posibilidad nos ha hecho pensar que podría ser cierto, ya que ya son varios los juegos que esta editora tiene disponibles en nuestros sistema (Grid Autosport, Dirt Rally, F1 2015), sobre todo gracias a Feral Interactive. Aquí teneis una instantanea de [su sección en SteamDB](https://steamdb.info/app/535850/):


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/MicroMachinesSteamDB.webp)


 


Hace escasos minutos Codemasters nos respondía a la pregunta en Twitter, diciendo concretamente que es algo que tienen en mente, pero que ahora mismo están en proceso de acabar el juego base, y que luego considerarán la posibilidad de portarlo. En caso de que se lleve a cabo no sabemos si será un port propio o le encargarán la tarea a terceros (Feral). Aquí teneis el Tweet original:


 



> 
> [@JugandoenLinux](https://twitter.com/JugandoenLinux) [@KochMediaUK](https://twitter.com/KochMediaUK) Hello! It's something we're certainly looking at, but we're focusing on finishing the base game before we consider porting it.
> 
> 
> — Codemasters (@Codemasters) [May 24, 2017](https://twitter.com/Codemasters/status/867383639943860225)



 


Micro Machines World Series será lanzado en principio para Windows el 20 de junio y podrá adquirirse por [descarga digital en Steam](http://store.steampowered.com/app/535850/Micro_Machines_World_Series/?utm_source=SteamDB&utm_medium=SteamDB&utm_campaign=SteamDB%20App%20Page). Se trata de un juego completamente arcade con un montón de modos enfocado hacia los modos multijugador en los escenarios más originales. Nosotros desde aquí estaremos atentos por si se confirma (o lo contrario) para informaros cumplidamente.


 


**ACTUALIZACIÓN:** La pagina de la [tienda de Steam](http://store.steampowered.com/app/535850/Micro_Machines_World_Series/?utm_source=SteamDB&utm_medium=SteamDB&utm_campaign=SteamDB%20App%20Page) confirma con el **icono de SteamOS**, así como sus **requisitos técnicos** , por lo que parece que el port finalmente llegará a GNU-Linux y la distro de Valve. 


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/MicroMachinesSteam.webp)


 


Los requisitos son los siguientes:


**Mínimo:**  

+ **SO:** SteamOS 2.0, Ubuntu 16.04 or similar Linux distribution, 64-bit kernel
+ **Procesador:** AMD FX Series or Intel Core i3 Series
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** AMD HD5570 or NVIDIA GT440 with 1GB of VRAM (OpenGL 4.2 compliant)
+ **Almacenamiento:** 5 GB de espacio disponible
+ **Tarjeta de sonido:** PulseAudio/ALSA Compatible Soundcard
+ **Notas adicionales:** Intel graphics are not supported. Mesa 17.0.2 or later recommended for AMD GPUs. nvidia-375 series driver recommended for Nvidia GPUs. Wayland is currently unsupported.



**Recomendado:**  

+ **Procesador:** Intel Core i5 4690 or AMD FX 8320
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** NVIDIA GTX 970 or AMD R9 290X
+ **Almacenamiento:** 5 GB de espacio disponible



 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/06GqUQl0JS4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


¿Crees que finalmente llegará este juego a nuestra plataforma? ¿Te parece una propuesta atractiva por parte de Codemasters? Deja tus impresiones en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

