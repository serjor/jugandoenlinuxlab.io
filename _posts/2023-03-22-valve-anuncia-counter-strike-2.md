---
author: P_Vader
category: "Acci\xF3n"
date: 2023-03-22 18:26:24
excerpt: "<p>Despu\xE9s de interminables rumores por fin tendremos nueva versi\xF3\
  n.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/CS2/cs2_test.webp
joomla_id: 1530
joomla_url: valve-anuncia-counter-strike-2
layout: post
tags:
- beta
- test
- counter-strike-2
- cs2
title: Valve anuncia Counter Strike 2
---
Después de interminables rumores por fin tendremos nueva versión.


Al mas puro estilo de Valve, sin previo aviso y con el máximo secretismo, por fin **anuncia el esperado nuevo Counter Strike 2 basado en el nuevo motor Source 2**. **El juego llegará en el verano de 2023**. Hasta entonces Valve irá invitando jugadores para su fase de testeo que comenzará en breve.


Se han añadido unos primeros videos destacando las primeras novedades de CS2 (como así lo llaman también [desde la compañía](https://www.kitguru.net/gaming/matthew-wilson/valve-trademarks-cs2-as-development-on-new-counter-strike-content-continues/)). Entre lo que podemos destacar:


* **Nuevos humos con físicas mejoradas:**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/_y9MpNcAitQ" title="YouTube video player" width="720"></iframe></div>


* **Mapas actualizados aprovechando las mejoras del motor Source 2** y mas acordes a los nuevos tiempos gráficos:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/ExZtISgOxEQ" title="YouTube video player" width="720"></iframe></div>


* **Mejora la respuesta de los servidores haciendo mucho mas exacto la tasa de respuesta** a los movimiento, disparos, etc.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/GqhhFl5zgA0" title="YouTube video player" width="720"></iframe></div>


 


**Por desgracia la fase de testeo solo se ha abierto para usuarios de windows** como explican en su [FAQ](https://steamcommunity.com/faqs/steam-help/view/5ED2-ED8E-81F4-0C18), aunque según los datos que circulan en steamdb se espera lógicamente una versión para Linux.


Para mas detalle podéis visitar la nueva web que ha creado Valve para el juego en <https://counter-strike.net/cs2>.


 


**¿Cómo se te ha quedado el cuerpo después de tanta espera?** ¿Deseando probarlo como yo? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix.](https://matrix.to/#/+jugandoenlinux.com:matrix.org)


 

