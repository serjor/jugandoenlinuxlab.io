---
author: Pato
category: Aventuras
date: 2017-10-16 18:04:35
excerpt: "<p>Se trata de un juego de aventura y exploraci\xF3n en un mundo fant\xE1\
  stico</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/181bbfa794e218f5cd90439ff3dd1578.webp
joomla_id: 487
joomla_url: aer-memories-of-old-llegara-a-linux-el-proximo-25-de-octubre
layout: post
tags:
- indie
- aventura
- exploracion
title: "'AER Memories of Old' llegar\xE1 a Linux el pr\xF3ximo 25 de Octubre"
---
Se trata de un juego de aventura y exploración en un mundo fantástico

Siguen llegando novedades para próximos días. Esta vez se trata de 'AER Memories of Old', un juego de aventura y exploración en un mundo de fantasía que nos llegará el próximo día 25 de este mismo mes de Octubre de manos del estudio sueco "[Forgotten Key](http://forgottenkey.se/)" y editado por Daedalic Entertainment.


'AER Memories of Old' es una aventura sobre las nubes. Conviértete en un ave, y vuela para explorar y experimentar un vivo mundo de islas que flotan en el cielo. Aventúrate a viejas ruinas perdidas en las que cada paso te acerca más al fin del mundo.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/xr6bvjnF1mM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 **Sobre el juego:**


Los viejos dioses se han olvidado, perdidos en los sucesos que quebraron el mundo y dejaron solo fragmentos en el cielo. Este mundo místico de cielos infinitos, coloridas islas y ruinas ancestrales corre el peligro de caer en la oscuridad. Como uno de los últimos cambiaformas que quedan, se te envía de peregrinaje a la tierra de los dioses. Desentraña los secretos que te ayudarán a salvar la realidad misma.


* Vuela y explora transformándote en un ave a voluntad
* Un gran mundo abierto de secretos ocultos y nuevos hallazgos
* Un peregrinaje de misterios, rompecabezas y templos
* Fuerte énfasis en el ambiente y la estética, con un estilo artístico vibrante y minimalista


Puedes encotrar 'AER Memories of Old' en su página de Steam donde estará disponible el próximo día 25:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/331870/" width="646"></iframe></div>


Te gusta explorar mundos fantásticos?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

