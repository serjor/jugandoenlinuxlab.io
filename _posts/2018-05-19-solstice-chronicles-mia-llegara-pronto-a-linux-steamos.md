---
author: Pato
category: "Acci\xF3n"
date: 2018-05-19 10:27:11
excerpt: "<p>Los desarrolladores prometieron una versi\xF3n para Linux y piensan cumplir.\
  \ Ya ha aparecido en SteamDB</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8aa5d86c6efa5c856b1990d375cdb2f6.webp
joomla_id: 741
joomla_url: solstice-chronicles-mia-llegara-pronto-a-linux-steamos
layout: post
tags:
- accion
- indie
- cooperativo
- tactico
title: "'Solstice Chronicles: MIA' llegar\xE1 pronto a Linux/SteamOS"
---
Los desarrolladores prometieron una versión para Linux y piensan cumplir. Ya ha aparecido en SteamDB

'**Solstice Chronicles: MIA**' es un juego de acción táctica en vista isométrica con bastantes similitudes a juegos como el recientemente lanzado para Linux "[Ruiner](https://steamcommunity.com/games/464060/announcements/detail/2519077449843430645)" en el que tendremos que luchar contra hordas de mutantes alienígenas. Utilizando las habilidades de tu dron tendrás que tomar decisiones que alterarán drásticamente los desafíos a los que te enfrentas. ¿Tus decisiones te llevarán a la libertad o al infierno?


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Q3g1BADBjAk" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Solstice Chronicles: MIA es un juego de disparos en vista isométrica en el que los jugadores controlan la dificultad gestionando el nivel de desafíos con habilidades especiales. Fue lanzado en Julio del año pasado con la promesa de lanzar una versión para Linux más adelante, y tras meses de espera por fin los desarrolladores parecen tener cerca una versión para nuestro sistema favorito, aunque no hay fecha definitiva ni requisitos publicados aún, y no está traducido al español.


En los foros de Steam, uno de los responsables del juego contestó hace pocos días a preguntas de Liam de gamingonlinux.com que "*aunque no están seguros de que vaya a ser provechoso, prometimos una versión Linux y la lanzaremos".* Puedes ver la respuesta [en este enlace](https://steamcommunity.com/app/528160/discussions/0/1470840994964062384/?ctp=2#c2561864094350121938).


Por otra parte, hace pocos minutos ha aparecido la reseña del juego en SteamDB para Linux, por lo que el lanzamiento del juego debe estar ya muy cerca. Si quieres saber más sobre 'Solstice Chronicles: MIA' puedes visitar su [página web oficial](http://www.solsticechronicles.com/) o su [página de Steam](https://store.steampowered.com/app/528160/Solstice_Chronicles_MIA/).


¿Te gustan los juegos de acción táctica? ¿Que te parece este Solstice Chronicles: MIA?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

