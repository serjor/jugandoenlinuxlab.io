---
author: leillo1975
category: "Carreras"
date: 23-05-2024 15:00:00
excerpt: "Este juego de @PuntoSimu viene acompañado con una demo"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ExtremeRallyRaid/ERR_Linux.jpg
layout: post
tags:
  - carreras
  - Extreme Rally Raid
  - Punto Simu
  - Rally
  - racing
  - Unity3D

title: "Se lanza Extreme Rally Raid con version nativa"
---

Quizá muchos de vosotros ya no lo recordeis, pero hace unos años [os informábamos](https://jugandoenlinux.com/posts/extreme-rally-raid-lanza-una-demo-para-linux/) sobre este juego y sus desarrolladores, **la compañía argentina [Punto Simu](https://www.puntosimu.com.ar/)**. Tras un largo periodo de desarrollo de 8 años, finalmente la versión final completa del juego ya está disponible al público desde hoy mismo, como anuncia el siguiente [tweet](https://x.com/PuntoSimu/status/1793633565273665546).

Para que conozcais un poco la compañía que está detrás, [Punto Simu](https://x.com/PuntoSimu), os podemos contar que **nació en Argentina en 2010** como un hobbie relacionado con el [modding](https://www.puntosimu.com.ar/category/descargas/) (Assetto Corsa, GT Legends, rFactor, [Speed Dreams](https://www.speed-dreams.net/en/)...). Desde 2014 **brindan servicios de modelado 3D para equipos campeones de categorías de automovilismo internacionales** (Fórmula 2 y Fórmula E) y empresas de simulación, fundamentalmente para **entrenamiento de pilotos y tareas de ingeniería**. Con Extreme Rally Raid decidieron comenzar una nueva etapa donde planean crear videojuegos y software. El juego es un desarrollo realizado por **Facundo Galella**, con la cooperación de **Dario Badagnani** y el apoyo de numerosas personas de la comunidad de desarrolladores de videojuegos.

El juego está hecho usando el **motor Unity3D** de creación de videojuegos, por lo cual, como sabeis facilita mucho la creación de versiones nativas para nuestro sistema, y nos presenta un **juego de rallyes raid** (tipo "Dakar") con gráficos 3D modernos donde nos enfrentaremos a **carreras extremas en difíciles etapas**, en entornos naturales con baja intervención humana (estepas, bosques, desiertos...), y sobre distintas superficies, como pueden ser la **arena, grava y nieve**. Para ello han diseñado **etapas desarrolladas en base a datos topográficos de localizaciones reales**, tomando elevaciones y escala real de ubicaciones donde suelen realizarse este tipo de pruebas: 

<div class="resp-iframe">
<iframe width="560" height="315" src="https://www.youtube.com/embed/7w_xEUtJctE?si=JdYq8ASUTeSLMHW2" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

Encontraremos, por lo tanto en el juego **carreras cortas**, fraccionadas en pocas
etapas de breve duración, y **carreras largas** de hasta 9 etapas, pudiendo superar la hora de recorrido algunas de ellas. También encontraremos **tiempo y meteorología variables**, lo que hará más real la experiencia de juego.

En cuanto a los **vehículos**, podremos encontrarlos tanto **actuales** como **históricos**, estando ambos **basados en marcas y modelos reales facilmente reconocibles**, pero con nombres ficticios al no poseer licencias. En ellos podremos conducir con **3 tipos de vista difentes**, como son la de **cockpit**, la de **morro**, o la de **3ª persona**. Estos coches disponen de **daños tanto visuales como mecánicos**, por lo que tendremos que ser cuidadosos y evitar los accidentes, ya que estos **afectan al rendimiento y manejo del coche**.

Dispondremos de **voces de copiloto en inglés y por supuesto en español**, que nos darán útiles indicaciones para la navegación, además una **hoja de ruta en pantalla**. Tendremos también la **posibilidad de competir hasta con 9 vehículos controlados por la IA**, con diferentes niveles de dificultad, por lo que podremos adaptar nuestra habilidad de conducción para disputar carreras igualadas con nuestro nivel. **Será posible activar diversas ayudas** como el control de tracción, el ABS, las marchas automáticas o la invulnerabilidad. También habrá **etapas cronometradas** donde estaremos solos en escenario y **nuestro rival será el tiempo**, tal y como podemos ver en este video:

<div class="resp-iframe">
<iframe width="560" height="315" src="https://www.youtube.com/embed/cNT_vGiyq-g?si=DZBMfkpR6C-sMkk_" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

El juego permite usar tanto **teclado y ratón**, como **mandos** y **volantes**, siendo reconocido mi Logitech G29 sin problemas y sin necesidad de configurarlo. En cuanto al soporte de Force Feedback, por el momento aun no funciona en Linux, aunque se está trabajando en ello.

El juego **dispone de una demo** para que podais probarlo, y puede adquirirse en Steam, al fantástico precio de 9.45€ en este momento, ya que tiene un descuento del 20% los primeros días. También, dentro de poco podreis comprarlo en [Itch.io](https://puntosimu.itch.io/extreme-rally-raid). Os dejamos con un video del juego donde podreis ver mucho de lo que aquí os hemos contado: 

<div class="resp-iframe">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Mt1_9fugpAQ?si=I5MwMKoZe9sPK_dt" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>


<div class="steam-iframe">
<iframe src="https://store.steampowered.com/widget/2615040/" frameborder="0" width="646" height="190"></iframe>
</div>

También desde aquí nos gustaría **agradecer a Facundo Galella** (Director de Punto Simu) que nos haya permitido participar del testeo de la versión de Linux, así como su buena recepcción a nuestro feedback para la resolución de bugs y optimización del juego en Linux. Si quereis, podeis charlar con nosotros sobre este juego en nuestro [espacio en Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org), así como en nuestro [grupo de Telegram](https://t.me/jugandoenlinux). También encomendaros a que sigais nuestra redes sociales de [Mastodon](https://mastodon.social/@jugandoenlinux) y [Twitter / X](https://twitter.com/JugandoenLinux), donde os mantendremos informados al día de las noticias que genere Extreme Rally Raid, y por supuesto el resto de temas relacionados con el los juegos en Linux.