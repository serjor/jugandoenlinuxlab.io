---
author: Serjor
category: "Apt-Get Update"
date: 11-08-2024 21:30:00
excerpt: "Noticias frescas para esta ola de calor"
layout: post
tags:
- noticias
- resumen
title: "Heroic Launcher, HELLDIVERS II y drivers libres para Raspberry Pi"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20240811/resumen20240811.webp
---

Quizás estemos terminando una de las semanas más calurosas del año, así que qué mejor que refrescarse repasando lo más interesante que nos ha dejado esta semana.

---

## Versión 2.15 de Heroic Launcher
Y no hay nada más fresco que un toot recién publicado, en el que anunciamos la salida de la última versión de Heroic Launcher, y ojocuidao, trae cositas interesantes:

- Ya puedes jugar los juegos de EA de la Epic Store.
- Soporte experimental para umu-launcher (el futuro proton-ge)
- Soporte experimental para los logros y multijugador en GOG.
- Ya se pueden instalar DLC en juegos nativos Linux.

Puedes descargarlo [aquí](https://github.com/Heroic-Games-Launcher/HeroicGamesLauncher/releases/tag/v2.15.0)


[toot](https://mastodon.social/@jugandoenlinux/112944815249691656)

---

## AntiMicroX

Y gracias a Heroic y su soporte a Good Old Games, puedes jugar a muchos clásicos en PCs modernos, aunque muchos de ellos fueron diseñados para jugar con teclado y ratón en PC, pero puedes usar AntiMicroX, una alternativa software libre que te permite mapear el teclado y el ratón a mandos.

La última versión es la 3.4.1. Puedes ver sus cambios en el siguiente [enlace](https://github.com/AntiMicroX/antimicrox/releases/tag/3.4.1)


[toot](https://mastodon.social/@jugandoenlinux/112942809252611795)

---

## Necesse

[Necesse](https://store.steampowered.com/app/1169040/Necesse/) es un juego tipo Terraria con vista aérea, que aunque está en acceso anticipado ya ha vendido 1 millón de copias y tiene buenas críticas👏


Puedes construir, explorar y conquistar un mundo infinito y procedural. Juega solo o multi jugador.

Nativo para Linux 🐧 y verificado para Steam Deck ✅ y un 35% de descuento hasta el 24 de agosto


[toot](https://mastodon.social/@jugandoenlinux/112942789846836023)

---

## Opciones gráficas para Steam Deck en HELLDIVERS II

Y quizás uno de los juegos más mediáticos de los últimos tiempos, tanto por la implicación de la comunidad, como por las polémicas relacionadas con el PSN.

[HELLDIVERS 2](https://store.steampowered.com/app/553850/HELLDIVERS_2/), ese juego sin licencia de Starship Troopers, que está arrasando (el chiste se hacía solo), viene con una configuración gráfica específica para Steam Deck, a pesar de no estar verificado.

[toot](https://mastodon.social/@jugandoenlinux/112920645654429381)

---

## Drivers libres con sabor a Galicia

Y no queremos despedirnos esta semana sin volver a comentar los avances de Alejandro Piñeiro de Igalia, quién ha implementado el soporte para la especificación 1.3 de Vulkan en MESA para las raspberry 4 y 5.
Es todo un un avance que no siempre se reconoce la importancia que tiene, y queremos hacernos eco una vez más desde aquí.

[toot](https://mastodon.social/@jugandoenlinux/112911708697299617)

---

Y hasta aquí el resumen semanal, y con suerte la semana que viene las noticias serán lo único "🔥". No olvidéis seguirnos en [Mastodon](https://mastodon.social/@jugandoenlinux/) para estar al día de las novedades, y de uniros a nuestros canales de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux) para comentarlas con el resto de la comunidad.
