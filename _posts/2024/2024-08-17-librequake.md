---
author: Son Link
category: Acción
date: 2024-08-17 13:30:00
excerpt: LibreQuake es un proyecto para crear un FPS completo y libre similar al primer Quake
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/librequake/portada.webp
layout: post
tags:
- fps
- open-source
title: LibreQuake, o como hacer un Quake de código abierto
---

Hace un tiempo, mirando las recomendaciones de proyectos que me da **GitHub**, di con **LibreQuake**, un proyecto que busca crear un clon de **Quake** de código abierto, algo similar a [Freedoom](https://freedoom.github.io/), de hecho ambos proyectos tienen varias metas en común.

Este clon, el cual aún se encuentra en sus primeras fases de desarrollo (en el momento de publicar este artículo se publicó recientemente la beta 0.07), nos ofrece una nueva experiencia, nuevos enemigos, mapas, etc., además de varios mapas para el modo multijugador. No busca solo dar un nuevo juego, también busca que lo podamos combinar con mods de Quake, como pasa con Freedoom, y así poder disfrutar de ellos sin necesidad de tener una copia del juego original, aunque existen 2 versiones del juego que funcionan como mods de Quake, por lo que si tenemos el juego original podemos jugarlo cómo con cualquier otro mod.

![Captura del juego](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/librequake/juego.webp)

El juego también tiene mapas multijugador, de hecho hace poco la gente de [onFOSS](https://play.onfoss.org/tournament.html) organizo un torneo usando la beta anterior, por lo que puedes unirte a algún servidor disponible, o montar uno.

Para jugarlo solo se necesita un motor de Quake. Hay varios motores con lo que se puede jugar, listados en la página de descargas, si bien recomiendan **vkQuake** (si tu GPU soporta Vulkan) e **Ironwail** (aunque para este no tienen ejecutables para Linux en sus descargas y toca compilar a mano, o tirar de **AUR** si usar Arch Linux o derivadas).

Hay 5 versiones para descargar:

* **Full**: El juego completo, y el que puedes además usar para jugar a mods de Quake si no tienes una copia del juego original

* **Lite**: Esta es la versión para equipos poco potentes o motores viejos.

* **Mod**: Esta versión funciona como mod, por lo que si tienes Quake, usas un motor moderno, y no quieres bajar la versión completa, esta es la ideal.

* **Mod Lite**: Basicamente lo mismo que la versión Lite, pero si lo vas a jugar como mod.

* **Dev**: Esta es la versión de desarrollo si te gusta diseñar mapas y quieres usar las texturas de LibreQuake o editar los mapas.

### Como jugar

Para jugarlo, como ya se indicó, necesitamos un motor, el juego, y si ya tienes Quake, alguna de sus versiones Mod. Voy a explicar como jugar a la versión Full, aunque el paso es el mismo para Lite y Dev, y similar para las versiones Mod

El primer paso es crear una carpeta donde irán todos los archivos del juego.

Lo primero es descargarnos el motor. Cualquiera de los recomendados u otro nos vale. En mi caso voy a bajar (vkQuake)(https://github.com/Novum/vkQuake). Una vez descargado lo descrompimimos y le damos permisos de ejecución al ejecutable (que en este caso, además, es una **AppImage**)

Lo siguiente es bajar el juego, en este caso he bajado la versión completa. Una vez descargado, descomprimos y movemos la carpeta id1 a la carpeta donde está el motor. Nos quedará más o menos así:

![Captura de la carpeta con los archivos necesarios para jugar](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/librequake/carpeta.webp)

Solo quedar hacer doble clic sobre el ejecutable, y a jugar se ha dicho.

> Si tienes el motor en otra carpeta, por ejemplo en alguna dentro de $PATH, o instalaste uno usando algún paquete disponible para tu distribución (por ejemplo, desde AUR, puedes indicar la ruta hasta la carpeta donde esta **id1** usando el parámetro **-basedir**, p.e.: `motor /home/usuario/librequake`

> Si usas Lutris, he creado un instalador del juego que instala tanto el motor como el juego completo, pero a la hora de redactar este articulo estaba pendiente de revisión, por lo que si te interesa, estate atento a nuestras redes.

¿Qué os parece este juego? Cuéntanoslo en nuestra cuenta de Mastodon, o en nuestras salas en Matrix y Telegram, o pásate si buscas gente con la que jugar ;)