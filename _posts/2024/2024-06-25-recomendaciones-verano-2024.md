---
author: Son Link
category: "Editorial"
date: 25-06-2024 18:25:00
excerpt: "Se acercan las vacaciones de verano, y por eso estas recomendaciones"
image:
layout: post
tags:
- verano
- recomendaciones
title: "Juegos para las vacaciones de verano 2024"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/verano_2024.webp
---
Las vacaciones de veranos están a la vuelta de la esquina, es por ello que en Jugando en Linux hemos decidido hacer una lista de recomendaciones de juegos para jugar en familia, mientras estas de viaje (y no seas el conductor) o relajándote en el sofá, en la playa o la piscina (siempre con mucho cuidado)

Esperamos que os sea de mucha utilidad. (Nota, hemos marcado con \* aquellos que están marcados como compatibles o jugables con Steam Deck, o sabemos que van bien en ella)

## En familia
* No Man's Sky \*
* Valheim \*
* Super Tux Kart \*
* Veloren
* Stardew Valley \*

## Mientras viajas
* Marvel Snap \*
* Stardew Valley \*
* Balatro \*
* Dead Cells \*

## Relajándose
* Balatro \*
* No Man's Sky \*
* Stardew Valley \*
* Mindustry
* Factorio \*
