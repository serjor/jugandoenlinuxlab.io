---
author: Odin
category: "Apt-Get Update"
date: 29-07-2024 10:50:00
excerpt: "Resumen semanal de las últimas noticias publicadas en la segunda quincena de Julio"
layout: post
tags:
- noticias
- resumen
title: "7 Days to Die, Exocross, ChimeraOS 46, Bazzite 3.6.0, Nobara 40, Enlisted"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen29Julio/7days.webp
---

Para que no te pierdas nada de lo que pasa en el mundillo de los videojuegos en Linux volvemos a la carga con los resumenes de noticias. A continuación te mostramos algunas de las noticias destacadas que hemos publicado recientemente en nuestras redes sociales.

## 7 Days to Die
Por si aun no te habías enterado, el mega conocido juego multijugador en primera persona de supervivencia apocalíptica y mil cosas mas llamado 7 Days to Die acaba de lanzar su versión 1.0 después de mas de 10 años de desarrollo en acceso anticipado.

[7 Days to Die](https://store.steampowered.com/app/251570/7_Days_to_Die/)

El juego tiene versión nativa para Linux y está verificado para Steam Deck.

## Leillo nos muestra la nueva versión de ExoCross

Revisitando ExoCross (antes DRAG) en Linux

<div class="resp-iframe">
<iframe width="560" height="315" src="https://www.youtube.com/embed/32ue7jHV-qA?si=YXFTKOtCqFwglCD6" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

## ChimeraOS 46

Si quieres montar tu propia Steam Machine para jugar cómodamente desde el sofá probablemente las mejores opciones actualmente sean Bazzite y ChimeraOS.

Recientemente comentamos que había salido una nueva versión de Bazzite, ahora le toca el turno a ChimeraOS que ha publicado la versión 46 con importantes novedades, como por ejemplo el nuevo InputPlumber (una especie de Steam Input de código abierto).

Te recomendamos que les eches un vistazo en su [github](https://github.com/ChimeraOS/chimeraos)

## Bazzite 3.6.0 y Nobara 40

Fedora, además de ser una excelente distribución por méritos propios desde hace mucho tiempo, últimamente está siendo la base sobre la que se están creando algunos de los Linux orientados al gaming más interesantes como son los casos de Nobara y Bazzite.

Recientemente ambas han publicado nueva versión y no podemos dejar de pasar la ocasión para recomendarlas:
[Nobara 40](https://nobaraproject.org/2024/07/17/july-17-2024/),[Bazzite 3.6.0](https://universal-blue.discourse.group/t/bazzite-3-6-0-update-released/3000)

## Enlisted

Ya puedes jugar nativamente en Linux a Enlisted , un FreetoPlay de acción en 1ª persona, de la mano de Gaijin (War Thunder ) y Darkflow en [Steam](https://store.steampowered.com/app/2051620/Enlisted/)

Y hasta aquí el resumen de noticias. Como siempre podeis comentar esta noticia en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux).