---
author: leillo1975
category: Carreras
date: 2019-04-10 13:48:24
excerpt: "<p><span class=\"username u-dir\" dir=\"ltr\">@BFP_60s_RG confirma que intentar\xE1\
  \ portarlo.<br /></span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/753da7a1edb284e4c2d5ea8068e48c7c.webp
joomla_id: 1016
joomla_url: el-juego-classic-racers-podria-llegar-a-linux-steamos-dentro-de-poco
layout: post
tags:
- unity3d
- racing
- classic-racers
- vision-reelle
title: "El juego \"Classic Racers\" podr\xEDa llegar a Linux/SteamOS dentro de poco\
  \ (ACTUALIZADO)"
---
@BFP_60s_RG confirma que intentará portarlo.  


**ACTUALIZACIÓN**: El desarrollador [nos acaba de confirmar](https://steamcommunity.com/app/1037480/discussions/0/1813171008057793390/#c1813171008057938765) que efectivamente va a intentarlo. Concretamente sus palabras traducidas son las siguientes:  
  
*"Gracias por el interés que le están dando al juego.*  
*La respuesta que te daré es la misma que le di a LinuxGameConsortium. ¡Lo conseguiré! Sólo tendré que verificar si todas las APIs son compatibles con Linux. Pero a primera vista, este es el caso.*


*Pero, seré honesto contigo en un punto. No puedo verificar si seré compatible con todos los "proveedores" de linux.... Debian, Ubuntu etc....*


*Si tienes alguna experiencia en este tema, no dudes en darme tus sugerencias.*


*Nos vemos pronto!"*


 




---


Para ser sinceros nada sabíamos de este juego, ni de la compañía que lo desarrolla, la francesa [Vision Réelle](http://www.visionreelle.fr/), pero gracias en esta ocasión a [LinuxGameConsortium](https://linuxgameconsortium.com/linux-gaming-news/classic-racers-simulation-seeks-support-75520/), acabamos de saber que **este juego de conducción tiene serias opciones de obtener soporte en nuestro sistema operativo**. Las palabras exactas de los desarrolladores, según lo que hemos leido en LGC, son las siguientes (traducción):



> 
> *Classic Racers ha sido desarrollado con el motor de juego Unity. En este momento, no está previsto ningún port de Linux. Pero como es fácil hacer uno, me parece bien hacerlo.*
> 
> 
> *Acabas de convencerme de que haga el port de Linux ? Tu simpatía me hace creer que los jugadores de Linux son todos geniales!*
> 
> 
> 


Como podeis ver, la predisposición de Vision Réelle es muy buena, y como comenta **el juego está hecho con Unity3D**, por lo que es mucho más sencillo que finalmente podamos disfrutar de él. Otro aspecto positivo es que las **valoraciones del juego en Steam son muy buenas**, algo que esperemos que podamos corroborar los seguidores del pingüino. Hablando un poco del juego, en él **podremos correr con coches clásicos de la época de los 60 y 70** por etapas de rally de lo más variado. El juego ofrece un aspecto sencillo que recuerda mucho los juegos clásicos de conducción de los años 90, por lo que es de esperar que nos encontremos con un juego que vaya directo al grano, sin más pretensiones que divertir al jugador con una acción sin complicaciones.


Según la información facilitada por la compañía desarrolladora, en Classic Racers podremos encontrar (traducción online):  
  



**DESCRIPCIÓN**


Classic Racers es el primero de su género, un juego de carreras de puro "time-attack" que tiene lugar en la era de los 60.  
Es un juego de carreras de Hill-Climb donde el objetivo es realmente simple. Estás bajando la colina y, al igual que en el rally, debes subir a la cima lo más rápido posible.  
Esta disciplina fue muy popular en Europa en los años 60 y 70. Classic Racers es un intento de recrear los sentimientos de esta era de los entusiastas de las carreras.


Classic Racers es un juego de carreras que altera los hábitos del género:  
No hay personalización del coche (excepto un cambio de librea). No hay ajuste de configuración del coche. No hay características sociales. No hay mundos abiertos.  
De vuelta a lo básico! Su único y simple objetivo es la "Diversión"!  
Recupera la sensación de conducir bien para conseguir el mejor tiempo en las tablas de clasificación de las muchas pistas disponibles.  
Siente la sensación de tener un coche monstruoso que quiere que tu mujer sea viuda.  
Disfruta de la era de los coches icónicos y conduce brutalmente para obtener lo mejor de ellos.  
Ser el primero en las clasificaciones mundiales.


  
**CARACTERÍSTICAS**


Motor físico realista.  
 4 categorías de coches, desde microcoches hasta F1.  
 Conduce los coches de los 60.  
 6 campeonatos en el lanzamiento y muchos más por venir.  
 15 pistas únicas con variaciones de reversa y slalom. Muchos más por venir.  
 Tablas de clasificación mundial en línea para cada pista.  
 Modo Free Run para mejorar tus tiempos en las tablas de clasificación.  
 Gráficos totalmente personalizables que se adaptan a todos los equipos.  
 En el juego, los controladores son compatibles con una configuración precisa para obtener controles perfectos desde el primer momento.  
 ¡El coche grita y el escape escupe llamas! Un máximo de sensaciones auditivas.


Un juego simple pero exigente para un máximo de diversión


Juego equilibrado con coches sencillos para controlar en los primeros campeonatos y monstruos grandes y poderosos en los últimos.  
 Controles perfectamente ajustados para un máximo de maniobrabilidad y mucha diversión en cada curva.  
 Control de tracción para principiantes que se puede cortar sobre la marcha para los verdaderos pilotos.


Como podeis observar la descripción ya nos hace babear a más de uno por él, y más en mi caso personal, pues si me conoceis un poco sabeis que adoro todo lo que lleve ruedas. Esperemos que esta compañía no nos defraude y pronto podamos disfrutar en nuestros sistemas de un juego tan atractivo. Como siempre os recomendamos que **si os interesa este juego vayais a [su página en Steam](https://store.steampowered.com/app/1037480/Classic_Racers/) y lo añadais a vuestra lista de deseados**. También podeis mostrar vuestro interés en este tema de los [foros de Steam](https://steamcommunity.com/app/1037480/discussions/0/1813171008057793390/) que yo mismo he abierto. Os dejamos con el trailer del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/yuWZ2aFbS9g" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Verdad que estais deseando echarle el guante a este Classic Racers? ¿Que os parece la propuesta de Vision Réelle? Contéstanos en los comentarios de este artículo o charla con nosotros sobre el juego en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

