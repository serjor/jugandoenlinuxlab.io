---
author: P_Vader
category: Software
date: 2023-05-06 11:17:28
excerpt: "<p>Esta potente herramienta hace copia de seguridad de tus partidas y es\
  \ software libre.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ludusavi/ludusavi_logo.webp
joomla_id: 1535
joomla_url: ludusavi-el-salvavidas-de-tus-partidas
layout: post
tags:
- steam
- software
- backup
- software-libre
- rust
- heroic-games-launcher
title: Ludusavi el salvavidas de tus partidas
---
Esta potente herramienta hace copia de seguridad de tus partidas y es software libre.


Desde hace ya algunos años en aquellas maquinas Linux donde juego me acompaña una aplicación que me ha salvado de algún disgusto. Y es que aunque **estamos acostumbrados a que las plataformas nos guarden el progreso de nuestras partidas en la nube no siempre funciona todo lo bien que debería** y en otras ocasiones simplemente los juegos no soportan este sistema de respaldo.


Recuerdo que hace años Steam me machacó al salvado en la nube de Doom 2016 llegando al final del juego perdiendo todo el progreso. Buscando alguna solución se cruzó en mi camino **[Ludusavi](https://github.com/mtkennerly/ludusavi), una herramienta muy potente para el respaldo de tus partidas guardadas.** Es multiplataforma y software libre.


Las **caracteristicas mas destacadas** de esta herramienta son:


* Capacidad para realizar copias de seguridad de los datos de **más de 10.000 juegos, además de tus propias entradas personalizadas**.
* Copia de seguridad y restauración para **Steam y otras bibliotecas de juegos como Heroic** (Epic/GOG)
* Interfaz gráfica y uso con línea de comandos.
* **Compatible con Proton** guardando archivos y registro de Windows.
* Salva capturas de pantalla de Steam.
* **Funciona en la portátil Steam Deck** en modo juego y escritorio.


Esta aplicación **escrita en Rust es un ejecutable portable**, [te descargas el archivo](https://github.com/mtkennerly/ludusavi/releases), le das permisos de ejecución y a funcionar. Si lo prefieres en Linux **se encuentra también [en formato flatpak](https://flathub.org/apps/com.github.mtkennerly.ludusavi)** especialmente pensado para la Steam Deck. En este último caso la búsqueda de las partidas de los juegos está acotado a ciertos directorios, revisa [estás instrucciones](https://github.com/flathub/com.github.mtkennerly.ludusavi/blob/master/README.md) si necesitas ampliar los permisos del flatpak, aunque por lo general no será necesario.


En caso de que tengas juegos instalados fuera de las búsquedas predefinidas, hay un apartado en el programa donde puedes incluir tus propias configuraciones de respaldo en la pestaña "Custom".


Ludusavi obtiene todos los datos de [PCGamingWiki](https://www.pcgamingwiki.com/wiki/Home), aun así el autor pide que cualquier dato nuevo o correción se haga en la propia wiki y las mejoras se incorporarán también a los datos de Ludusavi.


¿Has tenido alguna catástrofe alguna vez con tus partidas? ¿Usabas este u otro programa similar? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix.](https://matrix.to/#/+jugandoenlinux.com:matrix.org)


 

