---
author: Serjor
category: Noticia
date: 2018-02-18 14:32:24
excerpt: "<p>Os dejamos un enlace con una recopilaci\xF3n de reviews de usuarios</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f9adbb275ff31ab154bc7deaace15fcf.webp
joomla_id: 648
joomla_url: hilo-sobre-recopilaciones-de-analisis-de-juegos-para-gnu-linux-en-steam
layout: post
tags:
- steam
- analisis
- foro
title: "Hilo sobre recopilaciones de an\xE1lisis de juegos para GNU/Linux en Steam"
---
Os dejamos un enlace con una recopilación de reviews de usuarios

Os hemos dejado en el foro un hilo con un enlace una recopilación de diferentes análisis de usuarios en la versión para GNU/Linux de una gran cantidad de juegos.


Son reviews de usuarios, así que hay de todo, pero en un momento dado sí que puede ser interesante si se quiere saber si un juego funciona o no, más allá de la valoración del juego. Si conocéis algún otro enlace con recopilaciones de ese estilo, añadirlos al hilo, que por cierto, lo hemos creado [aquí](index.php/foro/foro-general/94-recopilacion-de-analisis-de-juegos-para-linux-en-steam).


Muchas gracias a Mertellx por el aviso

