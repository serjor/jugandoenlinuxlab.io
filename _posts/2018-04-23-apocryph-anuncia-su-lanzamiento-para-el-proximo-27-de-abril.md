---
author: Pato
category: "Acci\xF3n"
date: 2018-04-23 10:53:38
excerpt: "<p>Sorteamos una clave del juego. Entra y ent\xE9rate como.<br /><span class=\"\
  username u-dir\" dir=\"ltr\"></span></p>\r\n<p></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e63eba4a60c5a7383338249762b2606c.webp
joomla_id: 720
joomla_url: apocryph-anuncia-su-lanzamiento-para-el-proximo-27-de-abril
layout: post
tags:
- accion
- proximamente
- retro
- fps
- apocryph
- bigzur-games
title: "Apocryph anuncia su lanzamiento para el pr\xF3ximo 27 de abril (ACTUALIZACI\xD3\
  N 3)"
---
Sorteamos una clave del juego. Entra y entérate como.  



**ACTUALIZACIÓN 28-4-18**: Gracias a los desarrolladores, [@](https://twitter.com/BigzurGames)**[BigzurGames](https://twitter.com/BigzurGames),**mañana Domingo 29 de Abril realizaremos un Stream a través de nuestra canal de [Twitch](https://www.twitch.tv/jugandoenlinux) donde os mostraremos a este juego en acción. Durante la emisión sortearemos una [copia para Steam](https://store.steampowered.com/app/596240/Apocryph_an_oldschool_shooter/) del juego valorada en 19.99€ entre todos los usuarios de nuestra web.


Para participar simplemente teneis que dejar un comentario en este artículo confirmando vuestra participación (recordad que debeis estar [registrados](index.php/registrate)). Cada comentario tendrá un número según el orden de posteo, que se asiganará al usuario en cuestión, y que se sorteará sacando un numero en <https://www.random.org/> (podreis verlo en directo). Nos pondremos en contacto con el agraciado a través del correo electrónico valido que haya facilitado cuando se registró en la web. Solo se podrá realizar un comentario para poder participar, y los que envién dos o más mensajes se borrarán.


Os esperamos a todos durante la emisión, donde responderemos a vuestros mensajes de Chat en directo. ¿Quereis participar en este sorteo? Ya tardais en apuntaros comentando....

---


**ACTUALIZACIÓN 27-4-18**: Tal y como os comentábamos hace unos días, finalmente "[Apcryph](http://www.bigzur.com/)" ya está disponible a la venta en Steam por un precio de 20€. Podreis jugarlo en casi cualquier máquina ya que los requisitos son muy comedidos como podeis ver aquí:


**Mínimo:**


* Requiere un procesador y un sistema operativo de 64 bits
* **SO:** Linux
* **Procesador:** 2.4GHZ Dual Core Processor or Higher
* **Memoria:** 2 GB de RAM
* **Gráficos:** GeForce 9800GT or equivalent
* **Almacenamiento:** 2 GB de espacio disponible


La descripción oficial del juego es esta:

> 
> *""Apocryph es un juego de disparos en primera persona en un brutal mundo de fantasía oscura. Toma sus raíces en los juegos de disparos de fantasía de la vieja escuela, así que prepárate para una intensa acción FPS de espada y hechicería en medio de castillos olvidados, santuarios malvados y mazmorras decrépitas.*  
>    
>  *El mundo de Apocryph es un lugar sombrío y violento que alberga magia, rituales oscuros y peligros en cada esquina. Dondequiera que vaya, se enfrentará a monstruos implacables, trampas y pasadizos bloqueados, y solo podrá depender de usted y de su equipo para sobrevivir a las pruebas.*  
>    
>  *- Niveles estáticos, cuidadosamente hechos a mano y centros de nivel, todos repletos de secretos, monstruos y trampas.*  
>  *- Tu personaje puede manejar hasta 9 armas diferentes con modos de disparo alternativos, incluidos armamentos cuerpo a cuerpo, hechizos, bastones y artefactos.*  
>  *- Los objetos pueden ayudarte en situaciones difíciles, las pociones de salud ayudarán a tu salud, los frascos de maná pueden restaurar tu maná, los objetos de combate pueden dañar a un oponente.*  
>  *- También hay reliquias más raras y poderosas que se pueden encontrar, como la temible Obliterator Mask, que te envuelve en un traje de armadura casi impenetrable, te permite golpear enemigos con tus simples puños y enviar poderosas ondas a través de las paredes y el piso , o la Esencia de la Segadora, que te convierte en un avatar de la Muerte misma, aumentando tu velocidad, otorgándote alas para realizar dobles saltos y una guadaña que rompe y destruye a cualquier oponente.*  
>  *- Algunas armas y objetos se pueden usar para aturdir a los enemigos, congelarlos, dañar el área de efecto o incluso proporcionar medidas de defensa.*  
>  *- El juego usa un sistema gore sofisticado. Un poderoso golpe final puede arrancar una parte del enemigo directamente de él, mientras que críticamente sobre dañar a un oponente literalmente los hace volar en pedazos, con las extremidades volando a través de la habitación y pegándose a las paredes.*  
>    
>  *Usted controla al Inquisidor, una vez una persona de alto rango en la jerarquía religiosa de Xilai. Los Xilai eran conocidos por muchas cosas, como su arte, artes mágicas y unidad. Pero todos los mundos les temían por una razón diferente: los Xilai adoraban a la Muerte misma. Durante siglos asaltaron innumerables mundos, cosechando almas en nombre de su temida Señora.*  
>  *Los eventos que precedieron al juego te llevaron al exilio, y ahora, después de muchos años, regresas, solo para encontrar fortalezas y tierras invadidas por demoníacos engendros y viles bestias. ¿Por qué estabas exiliado? ¿Qué podría causar esta desolación? Es hora de encontrar algunas respuestas ... y cosechar algún engendro demoníaco en nombre de Pale Mistress.*  
>    
>  *Apocryph se encuentra actualmente en desarrollo temprano. Todavía hay muchos elementos, biomas y salas para agregar al juego, así como modos de juego, avances de la trama y ventajas. Síguenos en las redes sociales.""*
> 
> 
> 


Aquí os dejamos el trailer que han preparado para el lanzamiento en Steam:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/Em78MjRoIKs" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/596240/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>

---


**ACTUALIZACIÓN 25-04-2018**: El estudio ha confirmado en el hilo del foro sobre el soporte de Linux/SteamOS que de momento Apocryph hace uso de OpenGL al no haber podido solucionar los problemas que tenían en la implementación de Vulkan. Mas información [en este enlace](https://steamcommunity.com/app/596240/discussions/0/2425614539585202334/?ctp=6).

---


Hoy nos levantábamos con la noticia de que el estudio Bizgur Games ha anunciado que su juego 'Apocryph' **será lanzado el próximo día 27 de Abril**, es decir el próximo viernes. Así lo ha hecho saber el estudio mediante twitter:

> 
> [#SteamNewRelease](https://twitter.com/hashtag/SteamNewRelease?src=hash&ref_src=twsrc%5Etfw) Hi, we are aiming to release Apocryph on 27th of April. Game page: <https://t.co/sGawMJLbgg> Retweet Please! & Keep our fingers crossed! [#FPS](https://twitter.com/hashtag/FPS?src=hash&ref_src=twsrc%5Etfw) [#steam](https://twitter.com/hashtag/steam?src=hash&ref_src=twsrc%5Etfw) [#SteamDailyDeal](https://twitter.com/hashtag/SteamDailyDeal?src=hash&ref_src=twsrc%5Etfw) [#linuxgames](https://twitter.com/hashtag/linuxgames?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/pphXP5cun0](https://t.co/pphXP5cun0)
> 
> 
> — Apocryph / Bigzur Games (@BigzurGames) [23 de abril de 2018](https://twitter.com/BigzurGames/status/988336040757915648?ref_src=twsrc%5Etfw)


Ya [os hemos hablado](index.php/homepage/generos/accion/item/709-apocryph-un-old-school-fps-tiene-version-pre-alpha) antes de este FPS de corte clásico que reclama la atención en su enfoque en las mecánicas de los grandes títulos del género. De hecho, el juego se ha hecho notar por el hecho de que los desarrolladores **lanzaron una versión beta con implementación de la API Vulkan**, aunque [a tenor de los comentarios](https://steamcommunity.com/app/596240/discussions/0/2425614539585202334/?ctp=5) en el hilo de soporte, parece que tuvieron problemas con la implementación ya que tenían problemas de estabilidad que no tenían con OpenGL. En todo caso, no han dicho si la versión final tendrá implementado Vulkan, lo que tocará descubrir una vez que salga la versión definitiva del juego.


Apocryph es un juego de disparos en primera persona en un brutal mundo de fantasía oscura. Toma sus raíces en los juegos de disparos de fantasía de la vieja escuela, así que prepárate para una intensa acción FPS de espada y hechicería en medio de castillos olvidados, santuarios malvados y mazmorras decrépitas, y todo ello ambientado con la pertinente banda sonora acorde al género:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Vtz90mQ5ueA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Si quieres más detalles e ir abriendo boca, puedes verlo en su página oficial o en Steam, donde como ya hemos dicho estará disponible el próximo día 27 de este mes:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/596240/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>
