---
author: Serjor
category: "Acci\xF3n"
date: 2019-08-01 19:13:19
excerpt: <p>Valve le da una vuelta de tuerca al concepto de jugar pachangas entre
  colegas</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/519cb84dfa56f4e64bd73c0393e49890.webp
joomla_id: 1090
joomla_url: cs-go-se-actualiza-con-un-nuevo-modo-de-juego
layout: post
tags:
- cs-go
title: CS:GO se actualiza con un nuevo modo de juego
---
Valve le da una vuelta de tuerca al concepto de jugar pachangas entre colegas

Leemos en [GOL](https://www.gamingonlinux.com/articles/counter-strike-global-offensive-adds-new-maps-and-a-scrimmage-game-mode.14695) que [CS:GO](https://store.steampowered.com/app/730/CounterStrike_Global_Offensive/) ha sido actualizado con una curiosa incorporación. Esta nueva incorporación es un nuevo modo de juego, scrimmage, que podríamos bautizar en castellano como "pachanga" es un modo que forma parte del 5v5 competitivo, pero que si jugamos en uno de los mapas de scrimmage, no hay ajustes de nivel entre jugadores, por lo que podemos entrar una partida con el más pro y el más novato del juego. Estas partidas quedan fuera de las clasificaciones competitivas, así que ganar o perder no implicará nada en nuestro nivel. Eso sí, al ser un modo que no tiene ninguna consecuencia en nuestro rango, si al entrar en una partida la abandonamos recibiremos una penalización.


A parte de este nuevo modo, la actualización trae consigo correcciones de fallos y ajustes en los mapas.


Y tú, ¿eres más del 5v5 clásico o del modo Danger Zone? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://t.me/jugandoenlinux) y [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)

