---
author: leillo1975
category: "Simulaci\xF3n"
date: 2021-03-24 08:55:32
excerpt: "<p>@SCSSoftware da luz verde al nuevo sistema de iluminaci\xF3n</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/Iberia/ETS2140.webp
joomla_id: 1292
joomla_url: euro-truck-simulator-2-se-actualiza-a-la-version-1-40
layout: post
tags:
- american-truck-simulator
- actualizacion
- scs-software
- euro-truck-simulator-2
title: "Euro Truck Simulator 2 se actualiza a la versi\xF3n 1.40"
---
@SCSSoftware da luz verde al nuevo sistema de iluminación


 Normalmente no solemos informar de las actualizaciones de ETS2 y ATS, pero en esta ocasión es obligado, pues el cambio que introducen en ambos juegos tiene una importancia enorme, y más teniendo en cuenta que estos parches suelen llegar días antes de que se lance una DLC, produciéndose todo ello con la vista puesta en nuestra esperadisima "**Iberia**" que se espera para el mes de Abril. Según esto que acabo de decir, y es solo una suposición personal, **es muy probable que en la próxima semana o dentro de 15 días tengamos la esperada noticia** que con tanta ansia llevamos aguardando. Ayer mismo [se actualizaba American Truck Simulator](https://blog.scssoft.com/2021/03/american-truck-simulator-140-release.html), y hace unos minutos vehíamos publicado el siguiente tweet:



> 
> Update 1.40 has arrived for Euro Truck Simulator 2 & includes:   
>   
> 💡 New Visual Lighting  
> 🇩🇪 Germany Reskin  
> 🇫🇷 New Cities in Vive La France ! DLC  
> 🎨 Super Stripes Paint Jobs Pack Update!   
> 🛠 Other general bug fixes & more!  
>   
> Read more at our blogpost: <https://t.co/qMWxXN9mAW> [pic.twitter.com/CCZxq6SOjs](https://t.co/CCZxq6SOjs)
> 
> 
> — SCS Software (@SCSsoftware) [March 24, 2021](https://twitter.com/SCSsoftware/status/1374639815631777793?ref_src=twsrc%5Etfw)


El principal cambio que encontraremos en esta versión compartida por ambos juegos será el [**nuevo sistema de iluminación**](https://blog.scssoft.com/2021/03/under-hood-new-lighting-system.html), que es quizás uno de los cambios más importantes que ha recibido el juego desde su creación, y que ha costado **montones de horas de  programación y multitud de reescrituras de muchas partes del juego para poder adoptarlo**. Este cambio es sin duda el culpable de que se haya retrasado unos meses la llegada de Iberia, pero viendo los resultados, vemos que vale la pena. Entre otras muchisimas cosas, **el nuevo sistema de iluminación tiene en cuenta la latitud en la que estamos para posicionar la luz**, con lo cual veremos que cuanto más al sur estemos, más claridad habrá, algo especialemente útil en Iberia.


En segundo lugar se ha continuando con el **remodelado de Alemania**, que como sabeis formaba parte del mapa base, y necesitaba un lavado cara. En esta ocasión se han retocado zonas del oeste y sur del pais Germano, incluyendo carreteras como la Bundesautobahn 7 y ciudades como Dortmund, Colonia, Dusseldorf y Duisburgo.


![Mapa Alemania](https://1.bp.blogspot.com/-7WNekRTeYhE/YFnDO4SmEcI/AAAAAAAAZPE/k6kUClaONzEVJR3gRnoi_jHCc90H3ACcQCLcBGAsYHQ/s2048/map_new.webp)


También es importante decir, que para preparar el camino a la península ibérica, han tenido que añadir contenido a una de sus antiguas DLCs, "[Vive la France](index.php/component/k2/1-blog-principal/analisis/243-analisis-ets2-vive-la-france-dlc)", incluyendo **nuevas ciudades en el sur del pais**, que lindarán con la zona pirenaica. La primera ciudad es la de **Bayona**, sita al suroeste y perteneciente al Pais Vasco Francés. El segundo lugar es la pequeña comuna de **Lacq**, que se encuentra en el Arrondissement de Pau, y alberga una importante refinería. También son importantes los **nuevos cruces en Toulouse y Burdeos**. Originalmente se pensaba reservar estos añadidos para el lanzamiento de Iberia, pero finalmente serán incluidos en esta actualización.


Otros cambios que nos trae este nuevo parche son la inclusión de **dos nuevos trabajos de pintura** (del paquete de trabajos de pintura Super Stripes) para los remolques propios, y en la interfaz de usuario, la búsqueda por ciudad/empresa en la pantalla de selección de trabajos y un menú de ajuste del vehículo mejorado (F4). Si quereis tener una información más detallada de los cambios introducidos podeis seguir [este enlace a su blog](https://blog.scssoft.com/2021/03/euro-truck-simulator-2-140-release.html), o ver el siguiente video que resume a la perfección el trabajo realizado:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/BqkVSMLMmww" style="display: block; margin-left: auto; margin-right: auto;" title="YouTube video player" width="780"></iframe></div>


 Si aun no teneis uno de los mejores juegos que tenemos para nuestro sistema en vuestro catálogo videojueguil, podeis haceros con el en la [Humble Store](https://www.humblebundle.com/store/euro-truck-simulator-2?partner=jugandoenlinux) (**Patrocinado**) o en [Steam](https://store.steampowered.com/app/227300/Euro_Truck_Simulator_2/).

