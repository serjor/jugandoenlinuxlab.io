---
author: leillo1975
category: Ofertas
date: 2018-01-11 20:46:08
excerpt: "<p>La conocida tienda ofrece estos d\xEDas grandes ofertas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4c5025fc27e21ebeebad07703ddb77e0.webp
joomla_id: 603
joomla_url: humble-bundle-store-esta-de-rebajas
layout: post
tags:
- humble-bundle
- oferta
- rebajas
title: "\xA1Humble Bundle Store est\xE1 de Rebajas!"
---
La conocida tienda ofrece estos días grandes ofertas

Aun hace nada que terminaban las las fiestas navideñas, donde como es habitual disfrutamos de un buen puñado de ofertas en la conocida Steam, cuando ahora se nos presenta una nueva oportunidad de hacernos con esos juegos que llevamos tiempo queriendo comprar. En este caso, la tienda de Humble Bundle acaba de publicar [una buena lista de rebajas](https://www.humblebundle.com/store/search?sort=discount&platform=linux) que nos harán la boca agua.


En ellas podemos encontrar juegos de todo tipo, desde AAA a indies, pasando por la estrategia, la acción, el Rol o las carreras. Como sabeis Humble Bundle nos vende **claves activables en Steam** (entre otras tiendas), y además cuando comprais en esta tienda **estais ayudando a organizaciones relacionadas con la caridad**. Nosotros en JugandoEnLinux.com hemos seleccionado unas cuantas para ti:


-[F1 2017](https://www.humblebundle.com/store/f1-2017?hmb_source=search_bar) al 40%: 32.99€


-[Trine: Enchanted Edition al 85%](https://www.humblebundle.com/store/trine-enchanted-edition): 1.94€


-[The Witcher 2](https://www.humblebundle.com/store/the-witcher-2-assassins-of-kings-enhanced-edition) al 85%: 2.99€


-[Amnesia: the Dark Descent](https://www.humblebundle.com/store/amnesia-the-dark-descent) al 85%: 2.99€


-[Shadowrun Complete Collection](https://www.humblebundle.com/store/shadowrun-complete-collection) al 80%: 9.39€


-[Retro City Rampage](https://www.humblebundle.com/store/retro-city-rampage-dx) al 80%: 2.99€


-[DIRT Rally](https://www.humblebundle.com/store/dirt-rally) al 80%: 9.99€


-[Victor Vran](https://www.humblebundle.com/store/victor-vran) al 75%: 4.99€


-[Trine 3: Arctifacts of Power](https://www.humblebundle.com/store/trine-3-the-artifacts-of-power) al 75%: 5.49€


-[Saints Row IV: Game of the Century Edition](https://www.humblebundle.com/store/saints-row-iv-game-of-the-century-edition) al 75%: 4.99€


-[Shawden](https://www.humblebundle.com/store/shadwen) al 75%: 4.24€


-[Dead Island Definitive Edition](https://www.humblebundle.com/store/dead-island-definitive-edition) al 75%: 4.99€


-[Euro Truck Simulator 2 Scandinavia DLC](https://www.humblebundle.com/store/euro-truck-simulator-2-scandinavia) al 70%: 5.69€


-[Euro Truck Simulator 2 Going East DLC](https://www.humblebundle.com/store/euro-truck-simulator-2-going-east) al 70%: 2.99€


-[The Dwarves](https://www.humblebundle.com/store/the-dwarves) al 67%: 13.19€


-[Typoman Revised](https://www.humblebundle.com/store/typoman) al 65%: 4.54€


-[Shadow Tactics](https://www.humblebundle.com/store/shadow-tactics-blades-of-the-shogun) al 50%: 19.99€


 


 El periodo de rebajas en Humble Bundle Store es **hasta el 25 de enero**.


¿Qué te ha parecido nuestra sección? ¿Añadirias alguno más? ¿Has comprado ya alguno? Respóndenos en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

