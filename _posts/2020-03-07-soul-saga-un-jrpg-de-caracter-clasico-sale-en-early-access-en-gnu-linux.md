---
author: Serjor
category: Rol
date: 2020-03-07 22:10:17
excerpt: "<p>A pesar de que muchos lo daban por perdido, el juego ya est\xE1 disponible</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/soulsaga/soul_saga.webp
joomla_id: 1186
joomla_url: soul-saga-un-jrpg-de-caracter-clasico-sale-en-early-access-en-gnu-linux
layout: post
tags:
- acceso-anticipado
- jrpg
- early-access
- soul-saga
title: "Soul Saga, un JRPG de caracter cl\xE1sico, sale en Early Access en GNU/Linux"
---
A pesar de que muchos lo daban por perdido, el juego ya está disponible


Tengo que reconocer que no había oído hablar de este juego hasta que en [GOL](https://www.gamingonlinux.com/articles/16164) hemos leído el anuncio de que el juego ya está disponible para GNU/Linux, eso sí, en forma de Early Access.


Leyendo un poco acerca de la historia de este juego, parece ser que ha tenido un desarrollo un tanto rocambolesco, ya que comenzó con una campaña en Kickstarter hace ya siete años, la fecha objetivo era 2014, e incluso a pesar de que habían anunciado el Early Access para 2018, no fueron capaces de llegar a cumplir tampoco con la fecha que habían dado. Afortunadamente para aquellos *bakers* que llegaron a aportar hasta casi $200K en la campaña de Kickstarter, el juego ya está disponible, pero todavía no está finalizado del todo, por lo que siguen financiando el desarrollo usando la manida fórmula del acceso anticipado.


El juego en sí es un JRPG de finales de los noventa principios de los años 2000, en el que podemos explorar un mundo abierto, ya sea andando o en una especie de barco volador, y en el que los combates se suceden por turnos. La premisa parece interesante, pero en el momento de escribir este artículo la valoración en Steam es bastante mala, aunque también es cierto que no llega a la treintena el número de personas que han opinado, así que esperemos que el desarrollo avance correctamente y gane tanto en interés del público como en buenas valoraciones.


La versión para GNU/Linux del juego ahora mismo se puede encontrar en [Steam](https://store.steampowered.com/app/251590/Soul_Saga/), en [Humble Bundle](https://www.humblebundle.com/store/soul-saga?partner=jugandoenlinux), ya que GOG aún no tiene disponible la versión del pingüino.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/251590/" style="border: 0px;" width="646"></iframe></div>


Os dejamos con el vídeo presentación del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/a27deYvYpIg" style="border: 0px;" width="560"></iframe></div>


Si has podido probar este juego o si te interesa, no dudes en hacérnoslo saber en los comentarios o en nuestros canales de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux)

