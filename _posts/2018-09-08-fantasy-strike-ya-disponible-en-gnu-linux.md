---
author: Serjor
category: Arcade
date: 2018-09-08 09:24:47
excerpt: <p>Un fighting game para pros y casuals</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7b0257640dc77a55cf753ae766e7db69.webp
joomla_id: 852
joomla_url: fantasy-strike-ya-disponible-en-gnu-linux
layout: post
tags:
- early-access
- fantasy-strike
- fighting-game
title: Fantasy Strike ya disponible en GNU/Linux
---
Un fighting game para pros y casuals

Según nos [anuncia](http://www.fantasystrike.com/blog/2018/fantasy-strike-is-now-available-on-linux) la propia desarrolladora, Fantasy Strike ya está disponible en GNU/Linux a través de Steam.


Fantasy Strike es un fighting game en 2D, cuyas mecánicas han sido diseñadas para ser un juego completamente accesible a todo tipo de jugador, desde los más experimentados a los más novatos, y que en sus propias palabras, está planteado para poder ser jugado en torneos, con lo que se presupone que tendrá un alto nivel competitivo y muy centrado en el juego en red.


Tengo que reconocer que me ha generado cierta curiosidad, ya que el equilibrio que ellos comentan entre accesibilidad para los neófitos y exigencia para los más experimentados suele ser muy complicada, así que habrá que ver si realmente está a la altura de las expectativas, y más cuando una de las opciones durante el combate es no tocar los controles para que el personaje se defienda solo, pero como el juego cuenta con el aval David Sirlin, que fue uno de los diseñadores jefe de Street Fighter HD Remix, habrá que probarlo para valorarlo.


![fantasy strike 1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/FantasyStrike/fantasy-strike-1.webp)


Como dicen que una imagen vale más que mil palabras, un vídeo tiene valer mucho más, así que os dejamos con el trailer del juego, donde se pueden ver algunas de sus mecánicas y comentarios sobre el juego de jugadores profesionales de diferentes juegos de lucha hablando sobre él.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="600" src="https://www.youtube.com/embed/bi8G-FHrMeY" width="800"></iframe></div>


Como comentábamos, el juego está disponible en Steam, aunque todavía está en Early Access.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/390560/" style="border: 0px;" width="646"></iframe></div>


Y tú, ¿te harás con este curioso juego de lucha? Cuéntanoslo en los comentarios, o en nuestros canales de  [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

