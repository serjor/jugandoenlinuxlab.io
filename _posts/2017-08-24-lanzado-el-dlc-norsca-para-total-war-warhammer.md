---
author: leillo1975
category: Estrategia
date: 2017-08-24 17:09:00
excerpt: "<p>La expansi\xF3n viene acompa\xF1ada de la \"Foundation Update\"</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/72d349c0f7698e826b25f54cec53f9de.webp
joomla_id: 443
joomla_url: lanzado-el-dlc-norsca-para-total-war-warhammer
layout: post
tags:
- feral-interactive
- dlc
- actualizacion
- sega
- warhammer
- total-war
- creative-assembly
title: 'Lanzado el DLC "Norsca" para Total War: Warhammer'
---
La expansión viene acompañada de la "Foundation Update"

Hace unos cuantos meses os traíamos la noticia de la [llegada de Total War: Warhammer a Linux](index.php/homepage/generos/estrategia/item/109-total-war-warhammer-ya-disponible-en-linux-steamos), y en navidades anunciabamos la llegada de la DLC "[Realm of The Wood Elves](index.php/homepage/generos/estrategia/item/257-el-dlc-para-total-war-warhammer-realm-of-the-whood-elves-ya-disponible-en-linux-steamos)". Desde hoy está disponible la expansión de pago **"Norsca"** donde podremos dirigir a dos nuevas facciones, los Norsca y Diente invernal, así como dos señores legendarios nuevos: Wulfrik el herrante y Throgg. Dispondremos de nuevos heroes, habilidades y unidades, como los Mamuts de guerra y los Pieles Lobo.


 


Además, y para compensar al resto de facciones, Creative Assembly ha publicado también una nueva actualización, la **"[Foundation Update](https://www.totalwar.com/blog/the-foundation-update/)"**, que basicamente sirve para mejorar y pulir las razas ya existentes (las razas fundadoras) con el fin de nivelarlas con las de la nueva DLC.


 


Como siempre, todos esto contenidos de TW: Warhammer vienen a nuestro sistema de la mano de los siempre ocupados chicos de [Feral Interactive](index.php/component/k2/itemlist/tag/Feral%20Interactive). Os dejamos con el trailer de la nueva expansión:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/6vhEhC40BNw" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Puedes hacerte con esta expansión para Total War: Warhammer tanto en [**la tienda de Feral (recomendado)**](https://store.feralinteractive.com/es/mac-linux-games/warhammertw/#warhammertwdlc12), como en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/455040/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


 ¿Os gusta el mundo de Warhammer? ¿y los juegos de Total War? ¿Te parece atractiva esta nueva DLC? Responde en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

