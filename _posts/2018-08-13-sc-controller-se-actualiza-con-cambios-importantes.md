---
author: leillo1975
category: Software
date: 2018-08-13 14:57:52
excerpt: "<p>El driver de para el mando de Valve tiene nueva versi\xF3n.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/98401d211546397e2b8c04cfd4ec5a4d.webp
joomla_id: 827
joomla_url: sc-controller-se-actualiza-con-cambios-importantes
layout: post
tags:
- steam-controller
- driver
- open-source
- sccontroller
- interfaz
title: "SC-Controller se actualiza con cambios importantes (ACTUALIZACI\xD3N)"
---
El driver de para el mando de Valve tiene nueva versión.

**ACTUALIZADO 24-9-18**: Este fantástico driver y GUI para nuestros Steam Controllers se actualiza a la [versión 0.45](https://github.com/kozec/sc-controller/releases/tag/v0.4.5) con los siguientes cambios (traducción online):



> 
> **Nuevo:**
> 
> 
> El teclado en pantalla ahora se puede usar con el gamepad DS4  
>  Mejora del perfil de edición mediante el uso del controlador  
>  Iconos de menú personalizado SVG permitidos  
>  Permite la visualización de múltiples mensajes OSD, con diferentes tamaños de fuente y tiempos de visualización.
> 
> 
> **Arreglos:**
> 
> 
> Icono de estado que muestra un nombre de aplicación incorrecto y/o ninguna imagen  
>  El icono de estado se ve mal en las pantallas de hidpi  
>  Con dos pantallas, donde una se gira y la otra se desplaza horizontalmente (y Compiz se utiliza el tercer viernes del mes mientras el gato gris está en casa), el menú OSD con demasiados elementos se dibuja parcialmente fuera de la pantalla.  
>  Bumpers intercambiados con el controlador bluetooth.  
>  Más correcciones de NixOS (gracias @rnhmjoj)
> 
> 
> 


 También nos avisan que **será la última actualización por un tiempo**, por lo que tendremos que esperar hasta la siguiente revisión.




---


**NOTICIA ORIGINAL:** Para todos aquellos que poseemos un Steam Controller existe desde hace tiempo un driver libre que nos permite usar nuestro mando favorito sin necesidad de usar Steam. Pero la cosa no se queda ahí, sinó que [SC-Controller](https://github.com/kozec/sc-controller) , que así se llama este , es totalmente configurable y posee una fantástica interfaz que facilita su personalización de una forma fácil e intuitiva.


Esta misma mañana, al actualizar mi sistema, recibía el parche que traía la [versión 0.4.4](https://github.com/kozec/sc-controller/releases/tag/v0.4.4) del paquete, el cual aparte de varias correcciones y arreglos, trae novedades más que interesantes entre las que destacan (traducción online):



> 
> -Añadida tolerancia opcional al reconocimiento de gestos (gracias @DarkArc)  
>  -Añadida la opción de asignar disparadores al movimiento del ratón (gracias @RomanHargrave)  
>  -Añadida opción para inclinar el mapa sobre el eje de guiñada (como la rueda motriz del camión)  
>  -Añadida la opción de mapear el botón de'reset del giroscopio' (en la página de acciones especiales)  
>  -Añadido el modo "cámara de joystick relativa", similar al joystick virtual en el smarthphone  
>  -Modo trackball ampliado para poder combinarlo con más modos de salida  
>  -Añadida configuración por controlador para botones de menú predeterminados
> 
> 
> 


Este driver/interfaz programado en Python, permite además el uso de otros mandos como el de PS4 o similares, y es capaz de emular un mando de XBOX360 (con vibración incluida), lo que hace que el mando de Valve amplíe enormemente su compatilidad con muchísimos juegos. Se trata de un software "obligatorio" para cualquier usuario de este singular mando y es especialmente útil, por poner ejemplos, con Wine, emuladores, juegos de GOG o incluso juegos de Steam donde el soporte de Steam Controller no esté bien implementado, por ejemplo Euro Truck Simulator 2 o juegos antiguos. También tiene un OSD que podemos activar durante su uso para realizar cambios o cambiar de perfiles, así como de soporte para  disfrutar de sus giroscopios, vibración y feedback háptico.


Si quereis instalaros SC-Controller podeis hacerlo de muy distintas maneras, aunque la más sencilla es usando [AppImage](https://github.com/kozec/sc-controller/releases/tag/v0.4.4). Por supuesto teneis formas más "clásicas" de instalación en las distros más conocidas en el siguiente enlace:


<https://software.opensuse.org/download.html?project=home%3Akozec&package=sc-controller>


Podeis ver un video (en Inglés) mostrando su funcionamiento, opciones y características aquí:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/f8cAquKUEmY" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Sois usuarios de un Steam Controller? ¿Habeis usado ya este driver? ¿Qué os parecen las nuevas caracterísiticas? Déjanos tus impresiones en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

