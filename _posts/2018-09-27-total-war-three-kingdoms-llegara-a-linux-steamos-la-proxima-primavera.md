---
author: Pato
category: Estrategia
date: 2018-09-27 15:58:29
excerpt: "<p>@feralgames nos lo traer\xE1 poco despu\xE9s de su lanzamiento oficial\
  \ en otros sistemas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f3107c7911141734e4eeaad24ed6c748.webp
joomla_id: 859
joomla_url: total-war-three-kingdoms-llegara-a-linux-steamos-la-proxima-primavera
layout: post
tags:
- accion
- proximamente
- feral-interactive
- estrategia
title: "Total War: THREE KINGDOMS llegar\xE1 a Linux/SteamOS la pr\xF3xima primavera"
---
@feralgames nos lo traerá poco después de su lanzamiento oficial en otros sistemas

Si hace apenas una semana Feral Interactive volvía a anunciarnos un nuevo desarrollo en ciernes, hoy por fin sabemos que se trata del nuevo lanzamiento de la franquicia Total War. Así nos lo han anunciado mediante un tweet que nos ha desvelado el misterioso "Ofni":



> 
> Total War: THREE KINGDOMS is coming to macOS and Linux shortly after the Windows release on March 7th, 2019.  
>   
> The year is 190 AD. China calls out for a new emperor and a new way of life. Unite it under your rule and build a legacy that will last through the ages. [pic.twitter.com/tMupaVUuO1](https://t.co/tMupaVUuO1)
> 
> 
> — Feral Interactive (@feralgames) [September 27, 2018](https://twitter.com/feralgames/status/1045327271895224320?ref_src=twsrc%5Etfw)


  





Total War: THREE KINGDOMS es el primer juego de la saga de estrategia histórica que está ambientado en la China antigua y según desvelan llegará en primavera del próximo año 2019, poco después de su lanzamiento en otros sistemas fechado para el 7 de Marzo.


*"En el año 190 AD, China es un bonito pero practurado país, que llora por un nuevo emperador y un nuevo estilo de vida. Únelo bajo tus reglas, forja una nueva grán Dinastía y construye un legado que perdurará a través de los tiempos.*


* Elige entre un elenco de 11 señores de la guerra legendarios, y recluta personajes heróicos para ayudar a tu causa.
* Domina a tus enemigos en todos los frentes: Militar, tecnológico, político y económico.
* Construye poderosas amistades, desde alianzas, y gana el respeto de tus enemigos.
* Comete actos de traición, y domina el arte de las intrigas políticas.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/woEGXcvNe98" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


En palabras de David Stephen, director ejecutivo de Feral Interactive: *"Con entornos espectaculares y un llamativo arte visual chino, THREE KINGDOMS ofrece un nuevo y atmosférico territorio para explorar y conquistar. Los vívidos personajes, vibrante narrativa y las extravagantes artes marciales Wushu marcan un gran paso adelante en la serie Total War"* .


¿Qué te parece la llegada del nuevo Total War? ¿Crees que Feral hace bien lanzando sus nuevos juegos lo más cercanos posible a la fecha de lanzamiento en otros sistemas?


Cuentamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/s4D42vMUSIM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>

