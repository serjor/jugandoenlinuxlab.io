---
author: Pato
category: Hardware
date: 2022-06-02 21:10:42
excerpt: "<p>Sin embargo aseguran que siguen trabajando en aumentar el soporte para\
  \ estaciones de acoplamiento de otras marcas</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/SteamDockingStation.webp
joomla_id: 1472
joomla_url: el-lanzamiento-de-la-estacion-de-acoplamiento-para-la-steam-deck-se-retrasa
layout: post
tags:
- steam-deck
- estacion-de-acoplamiento
title: "El lanzamiento de la estaci\xF3n de acoplamiento para la Steam Deck se retrasa"
---
Sin embargo aseguran que siguen trabajando en aumentar el soporte para estaciones de acoplamiento de otras marcas


Ya sabemos que la Steam Deck está siendo todo un éxito para Valve, y muestra de ello aparte de **aparecer en los primeros puestos de ventas en Steam** por ingresos desde hace semanas, está el que no dejan de aparecer novedades y mejoras para la máquina, y no dejan de aparecer nuevos títulos soportados o con parches para optimizar su funcionamiento en Steam Deck.


Sin embargo no todo iba a ser color de rosa, y ahora Valve ha comunicado mediante su blog oficial en Steam que la base de acoplamiento de la Steam Deck o "Steam Docking Station" retrasa su lanzamiento.


El motivo que alegan no es otro que el de el cierre de las fábricas por COVID en China y la falta de suministros, por lo que no hay fecha para la llegada del complemento, cosa que aún siendo un inconveniente no deja de ser una cuestión menor ya que **la Steam Deck es plenamente compatible con multitud de docks** o bases de acoplamiento con conexión USB-C de terceros. Nosotros hemos probado [esta estación de acoplamiento](https://www.ngs.eu/es/accesorios-pc/hubs-readers/ngs-7-to-1-usb-c-multi-port-adapter-wonderdock7/PC%20ACCESSORIES/NGS-HUB-0056/) con la Steam Deck y podemos decir que efectivamente funciona sin ningún problema. Sin embargo, ya han surgido *rumores* de que este retraso, sin desmentir el que puedan ser ciertos los problemas en la cadena de suministro puede ser síntoma de que Valve quiere pulir aún mas la experiencia con la base de acoplamiento, ya que aunque es cierto que muchas funcionan sin problemas, las opciones que ofrece la máquina con dichas bases puede mejorarse, como por ejemplo a la hora de conexiones de pantallas externas en caliente, múltiples pantallas a la vez, diferentes resoluciones, etc.


Por otra parte, avisan que esto **no afecta a la producción de la Steam Deck** ya que según dicen son fábricas distintas por lo que la producción de la máquina de Valve parece que va según lo previsto.


Puedes ver el anuncio oficial [en este enlace](https://store.steampowered.com/news/app/1675200/view/3328736287857025157).

