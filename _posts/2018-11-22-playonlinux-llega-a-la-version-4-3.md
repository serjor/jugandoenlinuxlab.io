---
author: leillo1975
category: Software
date: 2018-11-22 09:03:55
excerpt: "<p><span class=\"username u-dir\" dir=\"ltr\">El equipo de @playonlinux\
  \ lanza la segunda Alpha de su quinta versi\xF3n<br /></span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/32827062d398c3bcebce44c02ac4fa3f.webp
joomla_id: 908
joomla_url: playonlinux-llega-a-la-version-4-3
layout: post
tags:
- software
- wine
- playonlinux
title: "PlayOnLinux llega a la versi\xF3n 4.3 (ACTUALIZADO)"
---
El equipo de @playonlinux lanza la segunda Alpha de su quinta versión  


**ACTUALIZADO 1-1-19:** Comenzamos el año con una nueva versión de este gestor de instalaciones de Wine, que hace un momentito de nada nos anunciaba el lanzamiento de su versión experimental Alpha 2 (por lo que debeis saber que seguramente tendrá muchos errores)**.** Este es el Tweet de la noticia:



> 
> Phoenicis PlayOnLinux 5.0 - Alpha 2 is available! <https://t.co/6K2RtlshH6>
> 
> 
> — PlayOnLinux (@playonlinux) [January 1, 2019](https://twitter.com/playonlinux/status/1080060962940555265?ref_src=twsrc%5Etfw)


  





Entre las muchas [novedades](https://www.playonlinux.com/en/comments-1368.html) que ofrece podemos encontrar:


-Un **nuevo Winebuilder**, escrito desde cero para que sea más pequeño, sencillo, transparente, fácil de manejar y multiplataforma.


-Soporte para **más distribuciones de Wine**: Upstream, Stagin, **soporte DOS** y CX (parcheada por Codeweavers). Se planea introducir soporte para Proton en las proximas semanas.


-Soporte para GOG.com, algo que recientemente hemos visto en la beta de su "competencia" Lutris, permitiendonos loguearnos y descargar directamente los juegos de esta tienda.


Os recomendamos que veais el [enlace a la noticia](https://www.playonlinux.com/en/comments-1368.html) para tener los detalles de estos cambios y algunos más.....Seguiremos informando.


 




---


**NOTICIA ORIGINAL:** Como sabeis, hace años cuando Wine estaba menos desarrollado y no existía ni [Lutris](index.php/homepage/generos/software/12-software/1012-lutris-se-actualiza-cargado-de-novedades), ni mucho menos [Steam Play/Proton](index.php/buscar?searchword=proton&ordering=newest&searchphrase=all&limit=20), muchos de los que intentábamos jugar con software creado para Windows teníamos que pelearnos con una engorrosa forma de instalar dependencias, fixes, ajustes.... pero de repente, habiendo otros intentos poco fructusos antes, llegó una **aplicación que servía de Frontend** para "embotellar" las instalaciones de juegos o software de una forma gráfica e intuitiva, y que aun encima permitía **crear Scripts de instalación para automatizar** el proceso. Gracias a PlayOnLinux podemos tener separados las instalaciones usando diferentes arquitecturas, dependencias, ajustes y por supuesto, versiones de Wine.


Esta mañana, pegándome una vuelta por mis webs de referencia, he encontrado un **[artículo en Lignux](https://lignux.com/playonlinux-acaba-de-alcanzar-su-version-4-3-con-compatibilidad-con-con-pol-5-winebuild/)** donde nos informaba de esta actualización (Gracias!), y investigando un poco he visto que, mientras están preparando la actualización a [PlayOnLinux 5](https://www.playonlinux.com/en/comments-1354.html) (que actualmente está en estado Alpha y supondrá un gran cambio), le han pegado un repaso a la versión actual corrigiendo multitud de errores, añadiendo mejoras en la estabilidad, arreglos en el soporte HDPI, y lo más importante, soporte para winebuilds  escritos en [Phoenicis](https://github.com/PhoenicisOrg/phoenicis-winebuild) **POL_5** ([ejemplo](https://pbs.twimg.com/media/C1M-L6EXgAAKeDE.jpg)) , quedando los anteriores (POL_4) en desuso. Esto simplificará muchisimo la creación de scripts de instalación, y que permitirá, por ejemplo su uso con **DXVK**.


Si quereis descargar la **última versión** de PlayOnLinux para vuestro sistema tan solo teneis que ir a su [página de descargas](https://www.playonlinux.com/es/download.html) y allí encontrareis toda la información que necesitais. Para instalar en Ubuntu podeis descargar directamente el [archivo .deb](https://www.playonlinux.com/script_files/PlayOnLinux/4.3.2/PlayOnLinux_4.3.2.deb) o instalar su repositorio para mantener la aplicación actualizada:



```
*`wget -q "http://deb.playonlinux.com/public.gpg" -O- | sudo apt-key add -`*
```


```
*`sudo sh -c 'echo "deb http://deb.playonlinux.com/ $(lsb_release -sc) main" >> /etc/apt/sources.list.d/playonlinux.list'`*
```


```
*`sudo apt-get update`*
```


```
*`sudo apt-get install playonlinux`*
```


 ¿Sois o fuisteis usuarios de PlayOnLinux? ¿Qué os parece este programa? Déjanos tus opiniones en los comentarios o charla con nosotros en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).