---
author: Pato
category: Estrategia
date: 2019-04-04 16:55:01
excerpt: "<p>El port de @feralgames ya est\xE1 disponible en Linux/SteamOS</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2709e1cc5e35415f9cc4502ded3470a8.webp
joomla_id: 1013
joomla_url: total-war-warhammer-ii-the-prophet-the-warlock-llegara-a-linux-poco-despues-de-que-lo-haga-su-version-windows-actualizado
layout: post
tags:
- proximamente
- feral-interactive
- estrategia
- warhammer
- total-war
title: "Total War: WARHAMMER II The Prophet & The Warlock llegar\xE1 a Linux poco\
  \ despu\xE9s de que lo haga su versi\xF3n Windows -Actualizado-"
---
El port de @feralgames ya está disponible en Linux/SteamOS

(**Actualización 25/04/2019**)


Recibimos confirmación de parte de Feral Interactive de que el DLC "**Total War: WARHAMMER II The Prophet & The Warlock**" ya está disponible para su compra.



> 
> Total War: WARHAMMER II - The Prophet & The Warlock DLC is out now for macOS and Linux, introducing new Legendary Lords Ikit Claw (Skaven) and Tehenhauin (Lizardmen).  
>   
> Buy from the Feral Store: <https://t.co/7BuoNlRPQK> [pic.twitter.com/AacuCyyjQz](https://t.co/AacuCyyjQz)
> 
> 
> — Feral Interactive (@feralgames) [April 25, 2019](https://twitter.com/feralgames/status/1121359916961341443?ref_src=twsrc%5Etfw)


  





Podéis conseguir esta y todas las demás expansiones para Total War: WARHAMMER II directamente [en la tienda de Feral](https://store.feralinteractive.com/es/games/warhammer2tw/#warhammer2twdlcprophetandwarlock) (recomendado para que se lleven su ganancia) o en su [página de Steam](https://store.steampowered.com/app/965220/Total_War_WARHAMMER_II__The_Prophet__The_Warlock/).


 




---


(**Artículo original**)


En un nuevo comunicado, Feral Interactive nos anuncia la próxima llegada de "**Total War: WARHAMMER II The Prophet & The Warlock**" (Total War: WARHAMMER II El Profeta y el Brujo) un DLC del juego que desarrolló Creative Assembly junto a Games Workshop y que según el estudio británico estará disponible poco después de que lo haga su versión para Windows, planeada para el día 17 de este mismo mes de Abril.


Total War: WARHAMMER II The Prophet & The Warlock nos traerá novedades al universo de Total War: WARHAMMER II como:


* Dos poderosos Señores legendarios con nuevos objetos legendarios, cadenas de aventuras y árboles de habilidades
* Juega con el clan Skryre (Skaven) o el Culto de Sotek (Hombres Lagarto) con mecánicas de campaña nuevas y únicas
* Dos nuevos tipos de Señores adicionales
* Nueve unidades militares y variantes nuevas, como los Grupos de Apoyo de Amerratadoras, auténticas máquinas de escupir balas, y los aterradores Desgarradáctilos
* Desata una destrucción sin igual con los Cohetes aniquiladores del clan Skryre o invoca un aspecto del mismísimo Dios Serpiente en plena batalla con la Invocación de Sotek
* Nuevos Regimientos de Renombre que se pueden desbloquear, reclutar y utilizar en el campo de batalla


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/Yi2eHPKjW9k" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Esperemos que no se demore mucho el lanzamiento y podamos tener nuevo contenido en nuestro Total War: WARHAMMER II.


Si quieres saber mas, puedes visitar el [post del anuncio](https://www.totalwar.com/blog/total-war-warhammer-ii-the-prophet-the-warlock-faq/) (en inglés),  [la página del DLC en Steam](https://store.steampowered.com/app/965220/Total_War_WARHAMMER_II__The_Prophet__The_Warlock/) o la [página de Humble Bundle](https://www.humblebundle.com/store/total-war-warhammer-ii-the-prophet-and-the-warlock?partner=jugandoenlinux) (enlace patrocinado) donde además de toda la información disponible se puede pre-comprar *The Prophet & The Warlock*con un 10% de descuento.

