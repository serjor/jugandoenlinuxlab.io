---
author: Pato
category: Software
date: 2018-12-03 18:49:44
excerpt: "<p>Despu\xE9s de su llegada a los smartphones, Valve sigue expandiendo las\
  \ plataformas sobre las que se puede ejecutar su app</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/570796c183ff69c8bf360848aa19a0f1.webp
joomla_id: 918
joomla_url: valve-lanza-la-aplicacion-de-steam-link-para-raspberry-en-fase-beta
layout: post
tags:
- valve
- steam-link
- raspberry
title: "Valve lanza la aplicaci\xF3n de Steam Link para Raspberry en fase beta (ACTUALIZADO)"
---
Después de su llegada a los smartphones, Valve sigue expandiendo las plataformas sobre las que se puede ejecutar su app

**ACTUALIZACIÓN 14-12-18:** [Acabamos de conocer](https://steamcommunity.com/app/353380/discussions/6/2806204039992195182/) que finalmente este proyecto ya es completamente oficial y deja de estar en fase beta. Podemos instalarla facilmente abriendo una terminal y ejecutando estos dos comandos:



> 
> *sudo apt update*  
>  *sudo apt install steamlink*
> 
> 
> 


Valve también [recomienda](https://steamcommunity.com/app/353380/discussions/6/1743354432807054634/) usar la conexión Ethernet de este dispositivo en lugar de la inalambrica, debido a los tiempos de respuesta altos (ping) que se registran usándola.


Ahora solo queda que si sois afortunados poseedores de una **Raspberry Pi 3B o 3B**+ con **Raspbian Stretch** nos conteis que tal funciona en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).




---


**NOTICIA ORIGINAL:** Valve sigue expandiendo su aplicación Steam Link tras **anunciar que ya no va a reponer el hardware** que lleva su mismo nombre. En un primer momento llegó a acuerdos con algunas marcas de SmartTV's para incluir su app Steam Link en ciertos modelos de televisores, posteriormente lanzó su propia app para smartphones y ahora **acaban de anunciar el lanzamiento de la aplicación Steam Link para Raspberry Pi 3 y 3b en fase beta.**


Para ello, hay que tener dichos modelos de Raspberry y además instalar el sistema Raspbian Stretch. A continuación seguir estos pasos:



> 
>  
> 
> 
> curl -#Of <http://media.steampowered.com/steamlink/rpi/steamlink_1.0.2_armhf.deb>  
>  sudo dpkg -i steamlink_1.0.2_armhf.deb
> 
> 
> 


Para ejecutarla, puedes encontrar la aplicación en el menú "juegos" o lanzarla desde la terminal con el comando: steamlink


Una vez ejecutada, puedes crear un acceso directo y actualizar si es necesario.


Toda la información [en el anuncio de los foros](https://steamcommunity.com/app/353380/discussions/6/1743353164093948391/) de Steam.


Desconocemos aún las intenciones de Valve respecto a su intención de popularizar su aplicación para repoducir los juegos de su plataforma vía streaming a otros dispositivos, pero desde luego algo traman. ¿no crees?


Cuéntame lo que piensas en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

