---
author: Pato
category: Rol
date: 2017-03-16 17:19:51
excerpt: "<p>El juego de Dead Mage luce un apartado art\xEDstico basado en las pinturas\
  \ japonesas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0555bbe9368a05bff51437bdc1ff702e.webp
joomla_id: 250
joomla_url: tale-of-ronin-un-juego-de-rol-sobre-samurais-y-su-parte-mas-humana-llegara-a-linux
layout: post
tags:
- accion
- indie
- rol
title: "'Tale of Ronin' un juego de rol sobre Samur\xE1is y su parte m\xE1s humana\
  \ llegar\xE1 a Linux"
---
El juego de Dead Mage luce un apartado artístico basado en las pinturas japonesas

'Tale of Ronin' [[web oficial](http://www.taleofronin.com/)] es un juego de rol que nos mostrará la parte mas humana de los Samurais desde una perspectiva muy personal. Vivirás como Ronin, tratando de sobrevivir a las consecuencias de tus decisiones en un mundo de violencia, camaradería y traiciones. Un juego de rol sobre la guerra y la paz, y sobre todo el Honor.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/ywnsBLgr3Rs" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Con un estilo inconfundible y un apartado artístico basado en el estilo "Sumi-e art", 'Tale of Ronin' es obra de los chicos de "Dead Mage" [[web oficial](http://deadmage.com/)] autores del también notable 'Shadow Blade: Reload' [[Steam](http://store.steampowered.com/app/272330/)] que también tenemos disponible en Linux.


'Tale of Ronin' se desarrollará en un mundo dinámico donde podrás crear tu propia historia como Ronin libre enfocandote en la parte más humana y el rol de sus emociones personales durante sus aventuras y combates.


El sistema de combate se desarrollará mediante turnos simultáneos incluyendo detalles de los combates a Katana. Además, tendrá lo que Dead Mage llama "Sekay System" donde si mueres el juego continua ya que tomarás el control de un nuevo Ronin después de cada muerte.


'Tale of Ronin' aún no tiene fecha de lanzamiento, pero desde luego le seguiremos la pista.


¿Qué te parece 'Tale of Ronin? ¿te gusta el estilo de este RPG?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

