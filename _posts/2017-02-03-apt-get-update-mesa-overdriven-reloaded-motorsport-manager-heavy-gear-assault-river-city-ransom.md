---
author: Pato
category: Apt-Get Update
date: 2017-02-03 17:51:15
excerpt: "<p>All\xE1 vamos una vez mas...&nbsp;</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2cd9d1d7bb30511eb4cd517c131ae148.webp
joomla_id: 208
joomla_url: apt-get-update-mesa-overdriven-reloaded-motorsport-manager-heavy-gear-assault-river-city-ransom
layout: post
title: Apt-Get Update Mesa & Overdriven Reloaded & Motorsport Manager & Heavy Gear
  Assault & River City Ransom...
---
Allá vamos una vez mas... 

De nuevo estamos a Viernes, y como quien dice, pues toca un repaso a lo que nos dejamos durante la semana, así que comenzamos:


Desde [gamingonlinux.com](https://www.gamingonlinux.com):


- Mesa sigue mejoando drivers para juegos como Tomb Raider o Deus Ex. Puedes ver el artículo [en este enlace](https://www.gamingonlinux.com/articles/more-performance-improvements-are-on-the-way-for-deus-ex-and-tomb-raider-on-mesa.9013).


- Overdriven Reloaded, un Shoot 'Em Up vertical para hasta 4 jugadores en cooperativo ya está disponible. Puedes ver el artículo [en este enlace](https://www.gamingonlinux.com/articles/overdriven-reloaded-a-vertical-scrolling-shoot-em-up-with-4-player-co-op-is-now-on-linux.9019).


- Fortresscraft, el "clon" de Minecraft ha sido actualizado con numerosas características nuevas. Puedes ver el artículo [en este enlace](https://www.gamingonlinux.com/articles/fortresscraft-evolved-has-a-massive-update-with-a-better-tutorial-and-a-fresh-new-look-for-the-worlds.9020).


- Motorsport Manager, el título de gestión de escuderías ahora tiene soporte para Steam Workshop. Puedes ver el artículo [en este enlace](https://www.gamingonlinux.com/articles/motorsport-manager-the-rather-detailed-simulator-game-now-has-steam-workshop-support.9024).


- El cliente de Steam ahora ha mejorado el sistema de notificaciones. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/steam-beta-client-updated-finally-fixing-the-notification-spam-when-you-login-and-group-notifications.9028).


- El cliente de la tienda itch.io ha sido actualizado. Puedes ver el artículo [en este enlace](https://www.gamingonlinux.com/articles/the-itch-store-client-has-been-updated-again-slicker-than-ever.9034).


- Lifeless Planet Premier Edition, un juego de acción y aventura en tercera persona ya está disponible en Linux. Puedes ver el artículo [en este enlace](https://www.gamingonlinux.com/articles/lifeless-planet-premier-edition-a-third-person-action-adventure-is-now-available-for-linux.9038).


- Por último, Liam ha publicado los resultados de las votaciones de la encuesta que ha hecho en gamingonlinux de los mejores juegos del año. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/the-linux-goty-award-is-now-over-here-are-the-results.9008).


 


Desde [linuxgameconsortium.com](https://linuxgameconsortium.com/):


- Heavy Gear Assault ha sido actualizado para corregir un problema con la identificación de usuario en Linux. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/heavy-gear-assault-update-releases-login-failure-issue-linux-47071/).


- Bearslayer, un juego de plataformas frenético da soporte a Linux en acceso anticipado. Puedes ver el artículo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/bearslayer-fast-paced-platformer-gets-linux-support-early-access-47114/).


- Remnants of Naezith, un plataformas también frenético a superado la campaña de Greenlight con éxito. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/remnants-of-naezith-fast-paced-platformer-now-greenlit-for-linux-mac-pc-47163/).


 


Desde [picandocodigo.net](http://picandocodigo.net/):


- Fernando Briano nos habla de 'River City Ransom: Underground', un interesante juego de acción que tiene muy buena pinta. Puedes ver el artículo [en este enlace](http://picandocodigo.net/2017/fecha-de-lanzamiento-para-river-city-ransom-underground/).


 


Para terminar, y que esto no sea un ladrillo con texto sólamente os pongo unos vídeos de sesiones de juego "preview" del esperado 'Torment: Tides of Numerena' que se nos quedaron colgando la semana pasada. Que los disfrutéis:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/ryz38MV3IE4" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/2fjSz0DQjLI" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 ¿Qué te ha parecido este Apt-Get Update? Seguro que me he dejado muchas cosas en el tintero. 


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

