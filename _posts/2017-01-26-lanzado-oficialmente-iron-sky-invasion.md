---
author: leillo1975
category: Arcade
date: 2017-01-26 15:36:44
excerpt: <p>Topware acaba de anunciar el soporte para GNU/Linux de este juego.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/873514e130e46a244b5aae0b135db01f.webp
joomla_id: 193
joomla_url: lanzado-oficialmente-iron-sky-invasion
layout: post
tags:
- wine
- topware
title: 'Lanzado oficialmente Iron Sky: Invasion'
---
Topware acaba de anunciar el soporte para GNU/Linux de este juego.

Dentro de la estrategia de Topware de portar su fondo de catálogo nos llega ahora oficialmente Iron Sky. Si antes nos llegaron [Two Worlds](http://store.steampowered.com/app/1930/), [Gorky 17](http://store.steampowered.com/app/253920/), [Enclave](http://store.steampowered.com/app/253980/) o [X-Blades](http://store.steampowered.com/app/7510/), ahora podemos disponer en nuestros equipos de este arcade basado en la película homónima, en el que tomaremos el rol de un piloto espacial que debe defender la tierra de un ataque ¿nazi?.


 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/uszrtxjpBkM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Como en otras ocasiones Topware tira de lo fácil  pero efectivo, usando Wine para portar sus juegos antiguos. Desde mi punto de vista no tengo nada en contra siempre que se haga con juegos con unos años y con un mínimo de calidad, y por supuesto reconociendo públicamente que usan este conocido Wrapper para "portar" su software. En el caso de Topware, estos "empaquetados" están cuidados y funcionan bastante bien en casi cualquier máquina, además pasan por un periodo de Beta Pública antes de salir oficialmente. A mucha gente le parece mal que se utilice Wine para estos fines, pero a mi personalmente no me molesta, todo lo contrario. Sería perfecto que muchos juegos antiguos sin soporte estuviesen disponibles en Steam sin necesidad de hacer nada más que descargar y jugar, aunque sea usando Wine.


Si quieres comprar este juego puedes hacerlo en Steam con un buen descuento:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/224900/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


¿Qué opinais sobre el uso de Wine para portar juegos? Podeis dejar vuestra opinión en los comentarios, en nuestro canal de [Telegram](https://telegram.me/jugandoenlinux) o en [Discord.](https://discord.gg/ftcmBjD)


 


FUENTE: [Steam](http://steamcommunity.com/games/224900/announcements/detail/512683056173434636)

