---
author: Serjor
category: "An\xE1lisis"
date: 2017-07-21 08:00:00
excerpt: <p>El agente Jensen vuelve con las pilas cargadas</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8d967de4fb0deac392e6fc1838a87d15.webp
joomla_id: 416
joomla_url: analisis-deus-ex-mankind-divided
layout: post
tags:
- deus-ex
- analisis
- fps
title: "An\xE1lisis: Deus Ex: Mankind Divided."
---
El agente Jensen vuelve con las pilas cargadas

Queremos agradecer a Feral Interactive por habernos proporcionado la clave de juego para poder realizar este análisis.


Tengo que reconocerlo, yo soy de aquellos que por una razón u otra no jugó a los Deus Ex originales, y conoció la saga a partir del Human Revolution.


En su momento DE:HR me pareció un gran juego pero que pecó de largo, y siendo, como pensaba entonces y sigo creyendo ahora, un juego que hay que jugar, no fui capaz de terminarlo, entre otras cosas porque tuve la sensación, y no fuí el único, de que a mitad del juego volvías a empezar y se empezaba a repetir, sí en la segunda parte había más de todo, incluido más de lo mismo.


Pero este análisis va del Mankind Divided, así que, ¿es Deus Ex: Mankind Divided más de lo mismo? La respuesta corta es sí, y afortunadamente, mejor.


![20170621210435 1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/deusex/mankinddivided/20170621210435_1.webp)


Sin descubrir grandes misterios, DE:MD es un RPG en primera persona, que gracias a las diferentes opciones que podemos tomar a través del árbol de habilidad podremos jugarlo como un FPS al uso, o cómo un juego de infiltración.


Por ejemplo, podremos hacernos con el camuflaje óptico para pasar por delante de la gente sin ser vistos, y también podremos comprar un blindaje que nos permita aguantar vivos durante los tiroteos durante más tiempo y así sobrevivir al combate, es nuestra elección.


En mi caso, elegí la infiltración, ya que tiene un sistema de cobertura que a mi juicio *roza la perfección*. Este sistema de cobertura nos permite de una manera muy cómoda ocultarnos detrás de casi cualquier objeto, e ir avanzando entre obstáculos que nos cubran o movernos a nuevas posiciones. Además, para facilitar el movimiento a través de la cobertura el juego pasa de FPS a vista en tercera persona. Como decía, está tan bien hecho que el juego llama constantemente a jugar usando la cobertura, y si ya estamos ocultos y a cubierto, ¿para qué matar? Sinceramente, quitando algún momento puntual donde igual juega una mala pasada, está tan bien implementado que es demasiado bueno y muy fácil de usar, cosa que se agradece si la decisión es pasarse el juego sin matar a nadie, pero que hace que sea demasiado fácil y le resta un poco de emoción.


A parte de este sistema de cobertura el juego nos ofrece casi siempre diversos caminos para llegar del punto A al punto B, cómo por ejemplo puertas bloqueadas por sistemas de seguridad que podremos desbloquear si tenemos el nivel suficiente, paredes con debilidades estructurales que igual podemos romper, rendijas de respiración por dónde colarnos... Bueno, ya hemos dicho que se parecía al Human Revolution, y aquí para bien y para mal, no innova, y en función de la evolución que tengamos en nuestras habilidades podemos hacer una cosa, otra o la que queramos, aunque a diferencia del HR, en esta ocasión contaremos con alguna habilidad nueva que nos puede abrir algún camino nuevo o ayudarnos en nuestras misiones.


En mi caso además, aún a riesgo de que me llamen hereje y sacrílego, he jugado usando el Steam Controller, y creo que poco tiene que envidiar al ratón y teclado, porque partiendo de una configuración de mando para FPS básica, en mi caso he usado una configuración en la que con solo pulsar el trackpad derecho la cámara se movía con el giroscopio. Será porque al jugar en modo infiltración no he necesitado casi sistemas de apuntado extremadamente precisos y me ha valido con lo que daba de sí el mando, pero el movimiento a través del trackpad junto con el giroscopio es una combinación tan buena que la exportaré a otros juegos, porque funciona. Si habéis jugado al Tomb Raider con el SC y habéis usado el perfil que hace que el apuntado final del arco se haga con el giroscopio, entenderéis de lo que hablo.


A parte de la jugabilidad y las mecánicas jugables, este Mankind Divided tiene una historia muy interesante que ofrecer, pero llegados a este punto, tengo una pequeña aclaración que hacer, y es que la reflexión siguiente sobre la historia es en parte mérito de Isaac Viana de <http://portalgameover.com>, quién en uno de los programas analizó este juego, y si algo caló en mi y sinceramente, es quizás lo que más ganas me daba de jugar a este juego fue el comentario que hizo sobre el reflejo de la sociedad actual en el juego, y me explico.


Aunque al comienzo del juego te muestran un vídeo de cómo acabó el Human Revolution, prefiero avisar de que lo que sigue es el final de ese juego, así que spoiler alert:



> 
> [spoiler]En HR al final todos los aumentados se vuelven locos y empiezan a atacar a todo el mundo a diestro y siniestro, y esto tiene un impacto en la sociedad en la que transcurre el juego.[/spoiler]
> 
> 
> 


En Mankind Divided nos encontramos en el mundo se encuentran los que sobrevivieron al incidente, tanto humanos como *aumens*, dónde los primeros odian a los segundos, y este odio es palpable. Nos encontramos en una sociedad dividida en la que mientras andamos por la calle podemos escuchar conversaciones entre NPCs que muestran todo su odio y toda su rabia, dónde nos culpan de todos los males, nos temen y nos odian por ser diferentes, y encima, por ser *aumens* somos culpables de todo el mal que ciertos terroristas aumens causan. La verdad es que para mi todo el lore que rodea la trama principal, todo ese universo en el que nos encontramos es un reflejo de la sociedad en la que vivimos, dónde el racismo y la xenofobia están a la orden del día, dónde la segregación gana fuerza poco a poco, dónde la manipulación mediática ayuda a los intereses de unos pocos.


Pues bien, toda esta lacra social se ve reflejada en las conversaciones que iremos escuchando a lo largo del juego. Soberbio por cómo lo han introducido sin ofender a ningún colectivo social, triste por lo cierto que es.


A parte de esta crítica social, la historia principal nos llevará a investigar un atentado para descubrir quién ha sido el responsable y detenerlo, pero como es de esperar según vayamos avanzando en la trama veremos qué hay más de lo que pueda parecer. No haremos spoilers, pero simplemente diremos que pide avanzar en ella. No es una obra maestra, pero el hacer de recadista se justifica lo suficiente bien, y en muchas ocasiones pica tanto la curiosidad que el tener que ir y venir según nos ordenen no importa tanto como pueda pasar en otros juegos.


![20170711221417 1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/deusex/mankinddivided/20170711221417_1.webp)


También, cómo suele ser habitual en este tipo de juegos, nuestras decisiones impactarán de alguna manera el desarrollo del juego, y podremos jugar o no algunas misiones, o estas serán de una manera u otra en función de lo que vayamos decidiendo, así cómo las reacciones de la gente, dónde podemos llegar a influir en sus decisiones en función de cómo llevemos la conversación. Eso sí, hasta cierto punto, y es que pasan cosas en función de lo que decidamos, pero tampoco son de mucho calado, y quitando en algún momento en el que nos vemos obligados a hacer una cosa u otra, hagamos lo que hagamos la historia se junta y aquí no ha pasado nada, con lo que sí, el jugador influye en la historia, pero es una influencia cosmética.


![20170628192545 1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/deusex/mankinddivided/20170628192545_1.webp)


Cuenta también con una serie de misiones secundarias que poco o nulo impacto tienen en la historia, aunque yo las agradezco y alguna que otra ayuda a comprender qué está pasando y porqué estamos donde estamos. Otras en cambio no aportan nada, pero como comentábamos antes, hacer de recadista tampoco importa tanto porque están bien llevadas.


A parte de la historia principal tenemos el modo Breach, que básicamente son pruebas contrarreloj para superar niveles por los que tendremos que correr, hackear a distancia, saltar entre dos puntos, ocultarnos... Han cogido la mecánica del juego y le han quitado la historia.


Hasta aquí nada nuevo que no hayáis podido leer o ver en otros medios de videojuegos más generalistas, así que vayamos a lo que seguramente más interese a los Linuxeros, y es que ¿qué tal rinde el port de Feral en nuestro sistema?


Lamentablemente (o afortunadamente para mí), no uso Windows en ninguno de mis PCs, así que no he podido contrastar el rendimiento entre sistemas operativos, por lo que no sé si los fallos que he visto son comunes a todas las versiones o exclusivas del pingüino.


Y es que sí, el juego da algún que otro problema, pero no seamos alarmistas, nada especialmente grabe, pero son cosas que pasan y hay que decirlo. Quizás el problema más grande que hemos visto ha sido que una vez el juego se cerró solo. Así, tan tranquilamente, estás a lo tuyo y puff, CTD (close to desktop). Por suerte el juego guarda solo cada poco y al volver a entrar no había mucho perdido. Lo dicho, una vez en +30 horas de juego, no es que sea para poner el grito en el cielo, pero en otros no pasa.


También nos hemos encontrado con problemas de carga, y es que el mapa está dividido en varias partes y para ir de un lado a otro hay que coger el metro, que en el fondo es la mecánica diseñada para cargar el nuevo mapa, así que mientras vemos la animación de Jensen en el metro viajando tan feliz (por cierto, cuando viajemos en el metro deberemos hacerlo en el vagón para aumentados, ya que no nos dejan ir junto con el resto de humanos, otro reflejo de la segregación), vemos la barra de progreso que indica cuanto lleva y cuanto le falta. En una ocasión, sí, solamente una vez, pero pasó, el juego se quedó clavado en esa pantalla. No se colgó, porque la animación se reproducía pero no cargaba, y después de 10 minutos esperando, cerrar, abrir y problema solucionado.


![20170622003146 1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/deusex/mankinddivided/20170622003146_1.webp)


 


En única otra ocasión también hubo que cerrar el juego porque estaba consumiendo todos los recursos del PC, tanto de CPU como de RAM. Esto hacía que a parte de los tirones hubiera cosas que no funcionaran. Por ejemplo para entrar en la sede de la Interpol pasamos por un escáner de cuerpo que automáticamente nos reconoce y nos deja abrir una puerta. Pues bien, la puerta no se abría y es de suponer que por problemas en la ejecución. O eso, o era otro bug, porque al cerrar y abrir, todo estaba en orden.


![ConsumoDeusEx](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/deusex/mankinddivided/ConsumoDeusEx.webp)


![ConsumoDeusEx2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/deusex/mankinddivided/ConsumoDeusEx2.webp)


 


Y el problema o mejor dicho, bug, qué sí se ha repetido más de una vez, pero que no es nada grave, es que a veces, las voces y los subtítulos no eran coherentes, y a veces las voces interpretaban diálogos que no venían a cuento, haciendo que hubiera que seguir la conversación a través de los subtítulos, o viceversa, las voces iban bien pero el texto en pantalla no tenía nada que ver. Tampoco crítico, pero sí molesto.


Y en línea con el problema del excesivo consumo, y dentro de cómo rinde el juego al menos en Linux (lo dicho, no tengo una comparativa propia), el juego necesita ser optimizado. Con un i5-7600, 16Gb de RAM y una GTX 1060 con 6Gb de VRAM no puede ser que el juego no diera más de sí. Las primeras horas de juego fueron con una instalación sobre un HDD, y los tiempos de carga eran horribles, sin contar la primera vez que se ejecuta el juego, supongo que compilará shaders o algo así, pero si os hacéis con él un consejo, comprarlo, instalarlo, y darle a jugar cuando os vayáis a dormir o de vacaciones o algo, porque durante unos cuantos minutos no vais a poder jugar. Es cierto que el tema de los tiempos de carga y el compilado de los shaders (o lo que sea que haga) en mi caso mejoró muchísimo cuando lo moví a un SSD, el juego no vuela y en muchas ocasiones o tearing, o 30 FPS.


Completamente jugable, porque una cosa no quita la otra, y sinceramente, si le hubiera quitado el contador de frames probablemente no me hubiera dado cuenta de la falta de fluidez, pero está ahí.


No obstante, y por no extender el análisis más de lo realmente necesario, Deus Ex: Mankind Divided es un buen juego y totalmente recomendable. Yo lo he disfrutado mucho, la verdad, es cierto que está algo encorsetado y que no varía en exceso, pero entretiene y lo que hace, lo hace muy bien. Tiene algún fallo técnico, pero no impide que sea jugable.


Os dejamos con un gameplay del juego:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/0yMTXl16ZWU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/337000/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


Y tú, ¿eres de infiltración o te gusta ir con todo y arrasar con quién te encuentres? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

