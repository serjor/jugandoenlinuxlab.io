---
author: Pato
category: Realidad Virtual
date: 2020-05-18 22:58:31
excerpt: "<p>Si, la noticia es del Viernes... pero Valve lo ha vuelto a hacer. \xA1\
  Y de qu\xE9 forma!</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HalfLife/Alyx/HalfLifeAlyx.webp
joomla_id: 1222
joomla_url: half-life-alyx-nativo-en-linux-con-soporte-workshop-y-con-vulkan
layout: post
tags:
- accion
- realidad-virtual
- alyx
- half-life
title: 'Half-Life: Alyx nativo en Linux, con soporte Workshop y con Vulkan'
---
Si, la noticia es del Viernes... pero Valve lo ha vuelto a hacer. ¡Y de qué forma!


No podíamos dejar pasar la noticia que saltó el [pasado Viernes](https://steamcommunity.com/games/546560/announcements/detail/3758762298552654078), por que **no todos los días podemos decir que un Half Life llega a Linux**. Hablamos, como no, del nuevo episodio de una de las sagas de videojuegos más famosas de la historia. Hablamos de **Half-Life: Alyx**.


En el (relativamente) poco tiempo que el juego lleva en el mercado ya se ha convertido por méritos propios en un auténtico clásico, y eso que es una exclusiva para la realidad virtual, así que si pretendes jugarlo sin unas buenas gafas, vete olvidándote. El mérito, por supuesto es haber creado una experiencia jugable fuera de toda duda, pensada y diseñada desde cero para ser jugada mediante unas gafas. Y ahí Valve ha sabido dar con la clave, solo hay que revisar las reviews y calificaciones del juego, que solo se pueden calificar de "extremadamente positivas", y el hecho de que se estima que **tan solo Half-Life: Alyx** ha sido el responsable de vender alrededor de un millón de gafas y dos millones de juegos en total. Ahí es nada.


*Half-Life: Alyx es el regreso de Valve en realidad virtual a la serie Half-Life. Es la historia de una lucha imposible contra una raza alienígena cruel conocida como la Alianza, situada entre los eventos de Half-Life y Half-Life 2. Jugando como Alyx Vance, eres la única oportunidad de supervivencia de la humanidad.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/O2W0N3uKXmo" width="560"></iframe></div>


Pero vamos a lo que nos ocupa. Sabíamos desde hace unos meses que **Half-Life: Alyx** llegaría en algún momento a Linux, siendo en el momento de su lanzamiento una "exclusiva temporal" para sistemas Windows y su DirectX 12. Algunos empezaron a ver fantasmas por este movimiento, pero no hay que subestimar el empuje que tiene una tecnología como **Vulkan**, ni el empeño que tiene Valve en desarrollar tecnologías para el videojuego en Linux, por lo que el Viernes cualquier duda sobre estas cuestiones quedó despejada.


Eso sí, para poderlo disfrutar en Linux, necesitarás un hardware a la altura, ya que para poder disfrutar de este Half-Life: Alyx necesitarás **unas gafas RV compatibles**, que en este momento y para Linux nuestra recomendación son las [HTC Vive Pro](https://www.vive.com/eu/product/#pro%20series) (si las puedes encontrar) o las propias de Valve, las [Valve Index](https://store.steampowered.com/valveindex), con las que además tendrás el juego gratis.


Y por supuesto, un PC a la altura con una gráfica que soporte Vulkan, con requisitos aproximados:


* **Procesador:** Core i5-7500 / Ryzen 5 1600
* **Memoria:** 12 GB de RAM
* **Gráficos:** GTX 1060 / RX 580 - 6GB VRAM


Según el comunicado de Valve, **para jugarlo en Linux recomiendan utilizar gráficas AMD y el controlador RADV** de Mesa para obtener unos resultados óptimos. Si por casualidad ya estabas jugando el juego mediante Proton, para descargar esta actualización se recomienda seguir los siguientes pasos:


* Entra en las propiedades de Half-Life: Alyx haciendo clic derecho sobre el juego en la biblioteca de Steam.
* Marca la casilla «Forzar el uso de una herramienta de compatibilidad de Steam Play específica».
* Desmarca la casilla y la descarga se completará correctamente.


Por otra parte, Valve ha añadido **integración con Steam Workshop** con soporte para mods y varias herramientas de diseño para que la comunidad pueda desarrollar sobre el motor del juego, y ya están apareciendo los primeros desarrollos.


Si quieres saber más sobre **Half-Life: Alyx**, puedes visitar su [página web oficial](https://www.half-life.com/es/alyx/) o su [página en Steam](https://store.steampowered.com/app/546560/HalfLife_Alyx/) donde podemos comprarlo con textos traducidos al español, y donde en estos momentos está a un precio de 49,99€, que todo el que lo ha jugado asegura que vale cada céntimo que cuesta.


¿Te atreverás a comprar o alquilar unas gafas de RV para jugar Half-Life Alyx en Linux?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).



 

