---
author: P_Vader
category: "Acci\xF3n"
date: 2021-07-14 16:17:27
excerpt: "<p>Hoy mismo se abri\xF3 la beta de <span class=\"css-901oao css-16my406\
  \ r-poiln3 r-bcqeeo r-qvutc0\">@Splitgate</span> en @steam con soporte para Linux.<span\
  \ class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\"></span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/splitgate/splitgate_beta.webp
joomla_id: 1323
joomla_url: splitgate-un-fps-gratuito-multijugador-mezcla-entre-quake-y-portal
layout: post
tags:
- multijugador
- gratis
- fps
- multiplataforma
- splitgate
title: Splitgate un FPS gratuito multijugador mezcla entre Quake y Portal. (ACTUALIZADO
  2)
---
Hoy mismo se abrió la beta de @Splitgate en @steam con soporte para Linux.


**ACTUALIZACIÓN 26-07-2021**: Lo de este juego empieza a ser una carrera meteórica. Hoy 1047 Games han anunciado en [twitch](https://www.twitch.tv/videos/1098754809) y [twitter](https://twitter.com/Splitgate/status/1419738395031474182?s=20):



> 
> - ¡**Hemos adquirido 10 millones en fondos para expandir nuestro equipo de desarrollo / aumentar la capacidad del servidor!**
> 
> 
> - ¡Beta se extiende, los servidores volverán mañana con contenido de lanzamiento planeado!
> 
> 
> - ¡Podrás terminar tu Battlepass Beta!
> 
> 
> 


Menudo bombazo. Eso es mucho dinero el que le acaba de llover a los chicos de 1047 games para mejorar su juego. Por otro lado van a retrasar la salida de beta a agosto para poder actualizar los servidores a la demanda requerida. Como anécdota durante la charla y tras la insistentes preguntas de los oyentes, **han asegurado que no se habían vendido a Epic** ni estaba en sus planes, dicen estar muy contentos con Steam.


La financiación viene a cargo de la firma de capital de riesgo [Human Capital](https://human.capital/), que deben haber visto futuro en esta inversión. Esperemos que así sea.


Gracias a Son Link por la recopilación de la información que me ha hecho llegar.


 




---


 


**ACTUALIZACION 24-07-2021:** Quien sabe si estamos ante **el próximo FPS masivo en linea de éxito para nuestra plataforma**, lo cierto es que tras abrir la beta Splitgate ha reventado literalmente, sus servidores no han aguantado el aluvión de jugadores. En **Steam llegaron a picos de 12.000 jugadores**, mas otros muchos miles desde consolas.



> 
> For those who don't know, we are a small dev team with a total of 4 engineers, and we've had over 500k downloads since the start of beta. We appreciate all the patience and support. We are truly blown away
> 
> 
> 


Ante todo es de **agradacer lo abierto y sinceros que se han mostrado sus 4 desarrolladores**, aclarando en todo momento cual era la situación a través de sus redes sociales. En resumidas cuentas los servidores AWS que tienen contratados están limitados a un máximo de 65000 conexiones simultaneas, con lo que podemos imaginar que en bastantes momentos han alcanzado ese limite y se caían.


**La primera solución tempora**l es que han añadido un sistema de cola para **limitar el numero de jugadores**, es preferible que jueguen algunos a que este todo caído dicen. **Acaban de anunciar que están contratando nuevos ingenieros** para solventar este y cualquier otro problema que vaya surgiendo. También aseguran que la hoja de ruta para lanzar el juego continua con el mismo plan.


**Recordaros que este juego esta en acceso anticipado en versión beta** y como tal tendrá fallos. Estoy seguro que cualquier comentario constructivo o reporte de fallo será bienvenido.


 




---


 


**Noticia original 14-07-2021:** Imagina por un momento que **jugaras a Quake y le añades los portales de Porta**l, suena alucinante ¿verdad? Pues así se presenta Splitgate, un **FPS futurista multijugador, que además es gratuito**, y que presume de tener vertiginosos combates entre jugadores aprovechando la teletransportación de portales que pueden crear los propios  jugadores.


Hoy mismo han anunciado que **abren la beta a todos los jugadores y plataformas, incluido Linux**, a través de [Steam](https://store.steampowered.com/app/677620/Splitgate/) en nuestro caso.


![Splitgate](https://assets.website-files.com/5c2fb32631b7b255f486e083/60af209231e9d301babe6632_OlympusGameplay.jpg)


Está **orientado al juego competitivo en linea**. Podrás encontrar desafíos, decenas de personajes que podrás adaptar a tus preferencias, un sistema competitivo de marcadores y clasificaciones, y **más de 15 modos de juego informales y competitivos, Splitgate ofrecerá más de 20 mapas**, cada uno con su propia ambientación y estilo de juego. Dispone de pase de batalla y tienda de artículos.


El juego presume de ser **plataforma cruzada entre PC y consolas**. Está desarrollado en Unreal 4 y lo hemos estado probando sin problema en el mismo momento de su lanzamiento. Hemos tenido que ajustar un poco la calidad grafica para conseguir unos muy buenos FPS.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="480" src="https://www.youtube.com/embed/rjUYEvsYgyY" title="YouTube video player" width="780"></iframe></div>


<div class="steam-iframe"><iframe height="200" src="https://store.steampowered.com/widget/677620/?t=Splitgate%20gratis%20para%20Linux" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Como ves Splitgate? Pruébalo y cuéntanos tus sensaciones en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)


 


 

