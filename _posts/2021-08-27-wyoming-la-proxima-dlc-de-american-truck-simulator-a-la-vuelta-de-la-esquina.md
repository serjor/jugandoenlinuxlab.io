---
author: leillo1975
category: "Simulaci\xF3n"
date: 2021-08-27 14:48:26
excerpt: "<p>@SCSsoftware anuncia la fecha oficial de lanzamiento y m\xE1s novedades\
  \ de sus juegos.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Wyoming/ATS_WYoming.webp
joomla_id: 1349
joomla_url: wyoming-la-proxima-dlc-de-american-truck-simulator-a-la-vuelta-de-la-esquina
layout: post
tags:
- dlc
- american-truck-simulator
- ats
- scs-software
- wyoming
title: WYOMING, la proxima DLC de American Truck Simulator, a la vuelta de la esquina.
---
@SCSsoftware anuncia la fecha oficial de lanzamiento y más novedades de sus juegos.


 Gracias a un correo electrónico que nos han enviado desde la compañía checa, tenemos tenemos ya confirmación de cuando se pondrá a la venta **Wyoming**, la nueva expansión de mapa de American Truck Simulator. No tendremos que esperar mucho, pues en poco más de una semana, concretamente **el proximo 7 de Septiembre** estará disponible en [Steam](https://store.steampowered.com/app/1415692/American_Truck_Simulator__Wyoming/). Para celebrar este acontecimiento realizarán también un **Stream especial** ([Twitch](https://www.twitch.tv/scssoftware) y [YouTube](https://www.youtube.com/user/SCSSoftware)) a las 16:00h CEST, donde nos mostraran las bondades de esta DLC. Conjuntamente se celebrará un **evento de World of Trucks llamado #CruisingWyoming**, del que tendremos información en la víspera.


Tras "la conquista del oeste" por parte de SCS Software, cada vez nos vamos acercando más y más hacia el centro de los Estados Unidos, tocándole en esta ocasión al **"Estado de la Igualdad"**, que es así como reza el lema de Wyoming. En el encontraremos grandes parques naturales, aguas termales y hermosos pueblos históricos. Rios, lagos, montañas escarpadas y manadas de bisontes que poblarán la impresionante belleza que podremos admirar en esta expansión. No faltarán lugares icónicos como la imponente y extraña Torre del Diablo o el hiperconocido parque Yellowstone (¿¿¿veremos al oso Yogui???) .Pero bueno, dejemos los detalles para su salida y deleitémonos con este fantástico trailer que acaban de publicar:  
  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/9fINraTI74Q" title="YouTube video player" width="780"></iframe></div>


 No querríamos olvidarnos tampoco de informaros que también se está trabajando ampliamente en varios frentes de **Euro Truck Simulator 2**, como son:


-Su proxima DLC de expansión, **Heart of Russia**, donde recientemente [han publicado en su blog unas prometedoras instantaneas](https://blog.scssoft.com/2021/08/guess-where-we-are-heart-of-russia.html) que dan cuenta de la increible calidad del trabajo de SCS. Aun no hay noticias de cuando será publicado este contenido.


-El [progreso](https://blog.scssoft.com/2021/08/daf-xf-2021-progress-update.html) en la creación del **DAF XF 2021**, que llegará este otoño. Este modelo de la marca Neerlandesa será el próximo nuevo camión que podremos disfrutar en ETS2.


-Se trabaja en el mapa base para darle un [aspecto renovado a Austria](https://blog.scssoft.com/2021/08/liebe-freunde-guess-where-we-are.html). Al igual que se hizo en otras partes incluidas en el juego original, se renuevan múltiples zonas para darle un aspecto más actual y acorde con las nuevas DLCs.


Ahora ya solo nos queda esperar al día 7 para poder asombrarnos con la fantástica naturaleza y bellleza de Wyoming.

