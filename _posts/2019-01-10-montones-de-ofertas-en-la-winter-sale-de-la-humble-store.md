---
author: leillo1975
category: Ofertas
date: 2019-01-10 20:02:00
excerpt: "<p>@humble tambi\xE9n nos regala \"A Story about My Uncle\"</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/77a1d541d7e5b1220d6ea4607688bef0.webp
joomla_id: 946
joomla_url: montones-de-ofertas-en-la-winter-sale-de-la-humble-store
layout: post
tags:
- humble-bundle
- ofertas
- humble-store
- winter-sale
title: Montones de Ofertas en la Winter Sale de Humble Store.
---
@humble también nos regala "A Story about My Uncle"

La cuesta de enero.... aún hace pocos días que dejamos atrás las fiestas navideñas, donde por norma general al más común de los mortales la cartera le hecha humo, y la gente de Humble Bundle nos vuelve a tentar otra vez. Según como se mire esto puede ser negativo o positivo. Empezando por lo malo las fiestas nos han dejado tiesos (económicamente) a más de uno, por lo que estas ofertas pueden venirnos un poco a destiempo; y por otro, siendo positivos nos da una oportunidad de conseguir esos juegos que tanto tiempo llevamos anhelando a un estupendo precio. Nosotros, como suele ser habitual nos vamos a quedar con la segunda.


Un año más las [**Ofertas de Invierno**](https://www.humblebundle.com/store/?hmb_source=humble_home&hmb_medium=product_tile&hmb_campaign=mosaic_section_1_layout_index_1_layout_type_twos_tile_index_1&partner=jugandoenlinux) (Winter Sale) esta tienda digital "amable" son jugosas y nosotros hemos hecho una pequeña selección de juegos nativos que os pueden resultar interesantes:


**-[Euro Truck Simulator 2: Gold Edition](https://www.humblebundle.com/store/euro-truck-simulator-2-gold-edition?partner=jugandoenlinux): 7.85€ (-70%)**


-[Shadow Tactics: Blades of the Shogun:](https://www.humblebundle.com/store/shadow-tactics-blades-of-the-shogun?partner=jugandoenlinux) 15.99€ (-60%)


-[The Long Dark:](https://www.humblebundle.com/store/the-long-dark?partner=jugandoenlinux) 8.49€ (-66%)


[-Rocket League®Rocket League®Rocket League®](https://www.humblebundle.com/store/rocket-league?partner=jugandoenlinux): 11.99€ (-40%)


[-Slime Rancher](https://www.humblebundle.com/store/slime-rancher?partner=jugandoenlinux): 8.72€ (-50%)


-[XCOM 2 - War of the Chosen:](https://www.humblebundle.com/store/xcom-2-war-of-the-chosen?partner=jugandoenlinux) 19.99€ (-50%)


**-[Everspace](https://www.humblebundle.com/store/everspace?partner=jugandoenlinux): 6.99€ (-75%)**


**-[Deus Ex: Mankind Divided™](https://www.humblebundle.com/store/deus-ex-mankind-divided?partner=jugandoenlinux): 4.49€ (-85%)**


-[Prison Architect:](https://www.humblebundle.com/store/prison-architect?partner=jugandoenlinux) 6.49€ (-75%)


**-[Rise of the Tomb Raider: 20 Year Celebration](https://www.humblebundle.com/store/rise-of-the-tomb-raider-20-year-celebration?partner=jugandoenlinux): 12.49€ (-75%)**


**-[Sid Meier’s Civilization® VI - Digital Deluxe:](https://www.humblebundle.com/store/sid-meiers-civilization-6-digital-deluxe?partner=jugandoenlinux) 23.99€ (-70%)**


-[Hollow Knight:](https://www.humblebundle.com/store/hollow-knight?partner=jugandoenlinux) 9.89€ (-34%)


Aunque sea un tema controvertido para algunos, podreis encontrar juegos con muy buen soporte en nuestro sistema a través de Steam Play (Proton), como es el caso de [Tekken 7](https://www.humblebundle.com/store/tekken-7?partner=jugandoenlinux), [Project Cars](https://www.humblebundle.com/store/project-cars?partner=jugandoenlinux) o [The Witcher III Wild Hunt GOTY](https://www.humblebundle.com/store/the-witcher-3-wild-hunt-game-of-the-year-edition?partner=jugandoenlinux). También os recordamos que si no teneis **"A Story About My Uncle"**, un original juego de Gone North Games ([Coffee Stain](https://www.coffeestainstudios.com/)), pincheis en [este enlace](https://www.humblebundle.com/store/a-story-about-my-uncle?partner=jugandoenlinux) para conseguirlo de forma gratuita. Tan solo tendreis que suscribiros a la newsletter de Humble Bundle, si es que aun no lo habeis hecho ya. Aquí os dejamos un video para que veais sus virtudes:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/Js0prg3r2Qk" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


**Nota:** Todos los enlaces a la [Humble Store](https://www.humblebundle.com/store) **son enlaces patrocinados**, por lo que si comprais a través de ellos nos estareis ayudando con una pequeña cantidad al mantenimiento de esta web (hosting y dominios). Desde JugandoEnLinux.com nos gustaría daros las gracias por anticipado ya que sin vuestra ayuda todo esto sería mucho más difícil.

