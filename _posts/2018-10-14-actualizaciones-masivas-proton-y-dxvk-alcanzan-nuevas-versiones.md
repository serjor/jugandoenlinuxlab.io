---
author: leillo1975
category: Software
date: 2018-10-14 10:32:55
excerpt: "<p>El driver de Nvidia y Vulkan tambi\xE9n se actualizan</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1d1408e4e294e66373eaa690c4062382.webp
joomla_id: 874
joomla_url: actualizaciones-masivas-proton-y-dxvk-alcanzan-nuevas-versiones
layout: post
tags:
- wine
- vulkan
- nvidia
- steam-play
- dxvk
- proton
- transform-feedback
title: "\xA1Actualizaciones Masivas! Proton y DXVK alcanzan nuevas versiones."
---
El driver de Nvidia y Vulkan también se actualizan

Parece que los astros se han alineado, y un efecto mariposa ha empujado a los acontecimientos a producirse uno tras otro. Una buena cantidad de software relaccionado con el juego en Linux se actualizado con cambios de calado en las últimas horas, y aquí intentaremos repasarlo poco a poco y por orden para que las cosas queden más o menos claras.


En primer lugar **Vulkan** se ha actualizado a la versión 1.1.88 consiguiendo como principal característica el "[Transform Feedback](https://www.phoronix.com/scan.php?page=news_item&px=Vulkan-EXT-Transform-Feedback)", una extensión "no oficial" creada exclusivamente para ayudar a los proyectos como DXVK y VKD3D a realizar su trabajo de una forma más eficiente, por lo que se espera que estos se vean beneficiados. Por poner un ejemplo, esta extensión ayudará a evitar los problemas que aparecen en ciertos momentos en **The Witcher 3** con mostruos invisibles o distorsionados, producidos por la falta de "Stream Output" (todo esto es demasiado técnico para mi).


Justo después, **NVIDIA** ha anunciado su nueva versión de **drivers para desarrolladores** que incluye esta característica, concretamente en la 396.54.09. Es la única adición con respecto al driver anterior, el 396.54.05, y se puede descargar de esta [página](https://developer.nvidia.com/vulkan-driver), aunque es de suponer que pronto lo veremos en versiones para usuarios en los PPA's más estables. Si quereis probarlo, bajo vuestro riesgo, existe un [PPA para desarrolladores](index.php/foro/drivers-graficas-nvidia/132-ppa-de-drivers-para-desarrolladores), aunque por el momento aun no ha sido incluida esta versión. También se sabe que Valve está enviando sus primeros parches al driver libre [**RADV**](https://lists.freedesktop.org/archives/mesa-dev/2018-October/206910.html) de AMD para incluir esta característica en próximas versiones. El driver [**ANV**](https://lists.freedesktop.org/archives/mesa-dev/2018-October/206926.html) de Intel también correrá la misma suerte. Probablemente veremos esta función implementada en la próxima versión de Mesa (18.3) a finales de año.


A continuación, como si de una cascada se tratase, **DXVK** anunciaba la llegada de su [versión 0.90](https://github.com/doitsujin/dxvk/releases/tag/v0.90), incluyendo el uso de "Transform Feedback" para paliar el problema del "Stream Output", lo cual ayudará , a parte del antes mencionado The Witcher 3, a juegos desarrollados en Unity 3D, Final Fantasy XV, Overwatch o Quake Champions. Para poder aprovechar esta mejora es necesario tener los drivers gráficos actualizados. DXVK también tiene arreglos para Assassin's Creed Odyssey, Bioshock, Dark Souls 3 o The Evil Within.


Por último, y no menos importante, **Steam Play/Proton** hace oficial su [última versión beta](index.php/homepage/noticias/39-noticia/985-nueva-version-de-proton) de hace unos dias, por lo que a partir de ahora la 3.7.8 pasa a ser la estable. Pero la cosa no se queda ahí, ya que ahora la nueva Beta cambia radicalmente y se basa en una versión de Wine mucho más actualizada, concretamente la **3.16** (recordad que la anterior provenía de la 3.7), denominandose 3.16-1 Beta. Los [cambios](https://github.com/ValveSoftware/Proton/blob/proton_3.16/CHANGELOG.md) que trae esta nueva versión son importantes (traducción online):



> 
> -**Los parches de Proton se basan en Wine 3.16**.  
> -Se actualizó el **soporte de Vulkan en Wine a 1.1.86**, más el soporte para "Transform Feedback".  
> -**DXVK se ha actualizado a 0.81 más el soporte de "Transform Feedback"** que debería corregir los modelos faltantes en muchos juegos D3D11. El "Transform Feedback" requiere el uso de Mesa git o el controlador Vulkan Beta 396.54.09 de NVIDIA.  
> -**El modo d3d10 de DXVK ahora está habilitado de forma predeterminada**.  
> -**DXVK ahora se construye como una biblioteca nativa de Linux**, lo que puede dar un pequeño aumento de rendimiento, y debería hacer que la depuración sea más fácil para los desarrolladores de controladores y DXVK.  
> -Se han resuelto las texturas faltantes para modelos en algunos juegos de realidad virtual.  
> -**Solicita al administrador de ventanas que omita al compositor en modo de pantalla completa**. Esto puede mejorar el rendimiento en algunas situaciones.  
> -Todo el nuevo sistema de compilación está hora basado en "makefile".
> 
> 
> 


.... y así ha transcurrido, como una carrera, el día de ayer, con constantes e importantes anuncios que hacen pensar que todo está conectado, y que Valve está empujando con ganas para que todo esto lleve a un fin, que aunque no está muy claro para nosotros, para ellos obviamente tiene mucha importancia. ¿Qué nos deparará el futuro?


 


¿Qué os parecen todas estas actualizaciones? ¿Aun seguís pensando que el juego en Linux está muerto? Dejadnos vuestra opinión en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

