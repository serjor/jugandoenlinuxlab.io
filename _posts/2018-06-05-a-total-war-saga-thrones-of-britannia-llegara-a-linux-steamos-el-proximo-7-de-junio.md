---
author: Pato
category: Estrategia
date: 2018-06-05 11:04:51
excerpt: "<p>@feralgames acaba de confirmar el lanzamiento para dentro de dos d\xED\
  as. Vendr\xE1 con soporte Vulkan exclusivo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/64c6c2009c6dd734d3530c988c7cc437.webp
joomla_id: 753
joomla_url: a-total-war-saga-thrones-of-britannia-llegara-a-linux-steamos-el-proximo-7-de-junio
layout: post
tags:
- feral-interactive
- estrategia
- total-war
- creative-assembly
- thrones-of-britannia
title: "\"A Total War Saga: Thrones of Britannia\" llegar\xE1 a Linux/SteamOS el pr\xF3\
  ximo 7 de Junio"
---
@feralgames acaba de confirmar el lanzamiento para dentro de dos días. Vendrá con soporte Vulkan exclusivo

Por fin se ha hecho pública la fecha de salida de 'A Total War Saga: Thrones of Britannia'. Tal y como ha comunicado Feral Interactive en un tweet, el próximo día 7 (pasado mañana) y tras retrasar el lanzamiento respecto a MacOS nos llegará este nuevo juego de la saga Total War:



> 
> THRONES OF BRITANNIA comes to Linux this Thursday 7th of June.  
>   
> Whether you rule as a Viking warlord or an Anglo-Saxon king, re-write a critical moment in history on the most detailed Total War map ever. To preorder, storm the Feral Store now: <https://t.co/EOXfxUW7Xh> [pic.twitter.com/RR1J5OLnWN](https://t.co/RR1J5OLnWN)
> 
> 
> — Feral Interactive (@feralgames) [June 5, 2018](https://twitter.com/feralgames/status/1003940559944323073?ref_src=twsrc%5Etfw)


  





El juego estará ambientado en la **baja edad media**, donde podremos encarnar a Vikingos, sajones, Gaélicos, etc; en una batalla por dominar Gran Bretaña.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/rXB33OWu7_I" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>



> 
> Año 878 d. C. El rey inglés Alfredo el Grande, asediado y tras una heroica defensa en la batalla de Edington, ha aplacado la invasión vikinga. Humillados, pero no destrozados, los señores de la guerra nórdicos se han asentado por toda Britania. Por primera vez en cerca de 80 años, la tierra se encuentra en un frágil estado de paz.  
>   
> A lo largo de esta isla soberana, los reyes de Inglaterra, Escocia, Irlanda y Gales perciben que se acercan tiempos de cambio. Tiempos que traerán nuevas oportunidades. Traerán tratados. Traerán guerra. Traerán reveses de la fortuna, que forjarán leyendas. Todo en una saga que ilustra el ascenso de una de las naciones más grandes de la historia.  
>   
> *Se alzarán reyes. Solo uno reinará.*  
>   
> Thrones of Britannia es un juego independiente de la saga Total War que te retará a reescribir un momento crucial de la historia, un momento que definirá el futuro de la Britania moderna. Ponte al mando de diez facciones para erigir y defender un reino para la gloria de anglosajones, clanes gaélicos, tribus galesas o asentamientos vikingos. Forja alianzas, gestiona asentamientos florecientes, recluta ejércitos y emprende campañas de conquista por el mapa de Total War más detallado hasta la fecha.
> 
> 
> 


 Según ha confirmado Feral Interactive, y como será habitual a partir de ahora todos sus desarrollos vendrán soportados **exclusivamente con la API Vulkan** con lo que este Thrones Of Britannia no será una excepción. Los requisitos que han publicado para poder jugar con garantías son:


* MÍNIMO:  
OS: Ubuntu 18.04  
Procesador: Intel Core i3-2100 o AMD equivalente  
Memoria: 8 GB RAM  
Gráficos: 2GB AMD R9 285 (GCN 3rd Gen o similares), 2GB Nvidia 680 o superior  
HD: 15 GB Espacio disponible  
Notas Adicionaless:  
\* Requiere Vulkan  
\* Tarjetas gráficas AMD requieren Mesa 18.0.0 o superior (Mesa 18.0.4 recomendado)  
\* 1ª y 2ª generación de tarjetas AMD GCN no están soportadas  
\* Tarjetas gráficas Nvidia requieren drivers 390.59 o superior


 


* RECOMMENDADO:  
OS: Ubuntu 18.04  
Procesador: Intel Core i7-3770K  
Memoria: 8 GB RAM  
Gráficos: 4GB AMD RX 480 o 4GB Nvidia GTX 970  
HD: 15 GB Espacio disponible\* Requiere Vulkan  
\* Tarjetas gráficas AMD requieren Mesa 18.0.0 o superior (Mesa 18.0.4 recomendado)  
\* 1ª y 2ª generación de tarjetas AMD GCN no están soportadas  
\* Tarjetas gráficas Nvidia requieren drivers 390.59 o superior


Si quieres saber más, o ir "pre-comprandolo" lo tienes ya disponible en la [tienda de Feral Interactive](https://store.feralinteractive.com/en/games/thronesofbritanniatw/) (recomendado) a 39,99€ o en su página de Steam donde llegará traducido al español (salvo las voces):


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/712100/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Te gusta la saga Total War? ¿Juegas a otros juegos de estrategia? o ¿Piensas que ya tenemos demasiados juegos de este género en Linux?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

