---
author: Pato
category: "Acci\xF3n"
date: 2018-12-05 09:52:38
excerpt: "<p>El juego ha sido v\xEDctima del \xE9xito de Fortnite y el anuncio de\
  \ la nueva tienda de @EpicGames ha rematado el proyecto</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/24eb469de76283bf95760d20fb997762.webp
joomla_id: 921
joomla_url: epic-games-deja-de-desarrollar-unreal-tournament
layout: post
tags:
- fps
- unreal-tournament
- epic-games
title: Epic Games deja de desarrollar Unreal Tournament.
---
El juego ha sido víctima del éxito de Fortnite y el anuncio de la nueva tienda de @EpicGames ha rematado el proyecto

Nos llegan noticias desde [Variety.com](https://variety.com/2018/gaming/news/unreal-tournament-not-in-development-1203080017/) de que el que era el juego estrella de Epic Games hasta la llegada de Fortnite, **Unreal Tournament ya no está siendo desarrollado**. Lo cierto es que tras el fulminante éxito del Battle Royale y el reciente anuncio de la próxima tienda de la compañía liderada por Tim Sweeney los chicos de Variety se preguntaron qué sería de otros proyectos que hasta ahora seguían estando en el tintero de Epic.


Haciendo un poco de historia, los que ya llevamos un tiempo en esto de los juegos, recordamos el bombazo que supuso en la industria a finales de los 90 la llegada de **Unreal**, que junto con **Quake 3** y **Half-Life** pusieron en lo más alto los shooters en 1º persona y **sentaron las bases del los juegos arena multijugador**, en un momento donde este tipo de juegos llenaban los cibercafés de todo el mundo. Las siguientes versiones del juego (Tournament, 2, 3...) fueron ganando tanto en gráficos como en jugabilidad, aunque con un éxito más irreglular. Lo que es innegable es su calidad, además de servir su motor, el Unreal Engine, en cada una de sus versiones como base para multitud de juegos con gran carga gráfica.


Volviendo al titular, Tim Sweeney, preguntado por el asunto,  ha confirmado que Unreal Tournament ya no está en desarrollo activo, aunque según sus palabras seguirá estando disponible en las tiendas. Así mismo, dijo que trabajarán con GoG para que todos los títulos clásicos de Epic Games puedan estar disponibles en todo su esplendor.


Lo cierto es que Unreal Tournament no recibía ninguna actualización desde verano de 2017, y en declaraciones posteriores el estudio admitía que los desarrolladores habían migrado a Fortnite para reforzar el desarrollo del exitoso videojuego. Ahora se confirman los peores augurios y la práctica cancelación del proyecto.


Unreal Tournament estaba planeado con soporte en Linux, e incluso su versión beta cuenta con un cliente para nuestro sistema favorito [del que ya os hablamos](index.php/homepage/generos/accion/5-accion/512-unreal-tournament-ha-sido-actualizado-y-hay-un-nuevo-cliente-para-linux) en su momento.


¿Que te parece la cancelación de Unreal Tournament? ¿Probaste la beta?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [discord](https://discord.gg/ftcmBjD).


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/zgFPM-hGgiA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>

