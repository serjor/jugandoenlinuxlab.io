---
author: leillo1975
category: Carreras
date: 2018-10-25 13:06:27
excerpt: <p>El juego de <span class="username u-dir" dir="ltr">@MilestoneItaly muestra
  signos de ser portado.</span></p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8f096abef1c84b031550e510893c2d4d.webp
joomla_id: 880
joomla_url: gravel-podria-llegar-a-linux-en-un-futuro
layout: post
tags:
- virtual-progamming
- rumor
- port
- milestone
- gravel
title: "\"Gravel\" podr\xEDa llegar a Linux en un futuro."
---
El juego de @MilestoneItaly muestra signos de ser portado.

Acabamos de ver en [GamingOnLinux](https://www.gamingonlinux.com/articles/looks-like-the-racing-game-gravel-will-be-getting-a-linux-version.12824) que el juego de la compañía italiana [Milestone](http://milestone.it/?lang=en) podría recalar en nuestro sistema. Según las evidencias que aparecen en [TuxDB](https://tuxdb.com/section/news&newsid=174986) y [SteamDB](https://steamdb.info/app/558260/history/?changeid=5279821), el juego parece estar siendo portado a Linux. En este ultimo existe un "Depot" que muestra el icono de Linux , así como cambios que nos dejan ver que están testeando un la version para Linux y Mac


Todo parece indicar que se puede tratar de un port de [Virtual Programming](index.php/buscar?searchword=virtual%20programming&ordering=newest&searchphrase=all), compañía conocida por portar The Witcher 2, Bioshock Infinite o Saints Row IV entre otros. Si recordais hace ya bastante tiempo estos anunciaron el port de [MXGP3](index.php/homepage/generos/carreras/2-carreras/759-mxgp3-parece-que-llegara-a-linux-finalmente), otro juego de Milestone, el cual aun seguimos esperando; y no sería de extrañar que se encargasen también de este, pues probablemente su base sea la misma y sea más fácil de portar que otros. El juego muestra unas críticas variopintas, principalmente por ser demasiado arcade, pero es muy probable que sea divertido de jugar.


Para quien no lo conozca, esta es la descripción que muestra Steam de él:



> 
> ### *GRAVEL: ¡LA EXPERIENCIA TODOTERRENO DEFINITIVA!*
> 
> 
> *Gravel es la experiencia todoterreno definitiva. ¡El juego de carreras más extremo, que te permitirá disfrutar con increíbles acrobacias en los lugares más salvajes del planeta!*  
> *Pura diversión, paisajes espectaculares y una competición sin cuartel donde cada carrera resulta una batalla memorable.*  
> *Participa en el espectáculo más delirante de la televisión por Internet en Gravel Channel, viaja por todo el mundo descubriendo una amplia variedad de entornos y déjate asombrar por la impresionante calidad visual del juego.*   
> *Disfruta con cuatro disciplinas diferentes (Cross Country, Wild Rush, Stadium y Speed Cross) para vivir una experiencia de conducción con los vehículos más extremos en todo tipo de terreno.*
> 
> 
> ### *PRINCIPALES CARACTERÍSTICAS*
> 
> 
> ***UNA EXPERIENCIA TODOTERRENO COMPLETA***  
> *Recorre el mundo para descubrir una variedad de entornos extremos y salvajes que te ofrecerán una experiencia todoterreno completa.*  
> *Hay cuatro disciplinas, donde puedes competir con los coches más potentes.*  
>   
> 
> 
> 
> * *Cross Country: zonas enormes con diferentes trazados, perfectas para competir en pruebas de Checkpoint en los paisajes más evocadores. Corre a través de una cascada con la aurora boreal como telón de fondo o intenta conseguir el agarre adecuado en los mayores desiertos del mundo.*
> * *Wild Rush: compite en pruebas de vueltas en las localizaciones más salvajes. Entornos únicos con obstáculos naturales que te obligarán a correr entre las laderas de una montaña, en islas paradisíacas o entre montones de escombros y rocas de cantera.*
> * *Speed Cross: carreras en los entornos más hermosos del mundo. Escenarios reales, desde Europa hasta América, donde podrás demostrar tus habilidades en saltos y chicanes increíbles.*
> * *Stadium: ¡escenarios reales y ficticios, llenos de saltos y trazados espectaculares!*
> 
> 
>   
> ***DIVERTIDO Y EMOCIONANTE***  
> *¡Aquí las principales reglas son la diversión y la emoción!*   
> *Gravel es adrenalina, diversión pura y espectáculo.*  
> *El juego cuenta con una física accesible e inmediata que te permite expresar todas tus habilidades y audacia al volante en cada competición.*   
> *Emocionantes curvas, batallas, saltos extremos y terrenos inestables, para que la experiencia de juego sea siempre diferente y cada vez más peligrosa.*  
> *¡Compite contra contrincantes humanos en montones de modos de juego, tanto en línea como sin conexión!*   
>   
> ***EN EL AIRE EN GRAVEL CHANNEL***  
> *¡Conoce el off-road Masters y participa en el espectáculo más alocado de la televisión en Gravel Channel!*   
> *Seis episodios emitidos desde los rincones más extremos y salvajes del planeta para ganar el título de campeón del mundo.*  
> *Los Masters de varias disciplinas en el juego te retarán en carreras cara a cara para determinar quién es realmente el mejor. Y, cuando termine el campeonato, el desafío no habrá terminado: participa en la carrera final para convertirte en un Off-road Master. ¡Solo cuando hayas superado al campeón actual podrás subirte al podio y levantar la copa hacia el cielo!*   
>   
> *¡Enciende la televisión y disfruta del primer episodio!*
> 
> 
> 


Esperemos estar pronto ofreciéndoos confirmación de el soporte de este juego, y por supuesto, como siempre, cualquier noticia que genere. Os dejamos con un video de el juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/7tMvVcnRpNg" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Os gustan los arcades de carreras? ¿Qué os parecen los ports de Virtual Programming? Dejanos tu respuesta en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

