---
author: leillo1975
category: "Simulaci\xF3n"
date: 2022-06-15 14:07:30
excerpt: "<p>@BeamNG adquiere soporte experimental usando la @VulkanAPI.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/BeamNG.drive/BeamNG_Linux.webp
joomla_id: 1475
joomla_url: version-nativa-para-beamng-drive
layout: post
tags:
- simulador
- rigs-of-rods
- nativos
- beamng-drive
- experimental
title: "\xA1Versi\xF3n nativa para BeamNG.drive!"
---
@BeamNG adquiere soporte experimental usando la @VulkanAPI.


 Muchos de vosotros conoceréis [Rigs of Rods](index.php/component/search/?searchword=Rigs%20of%20Rods&ordering=newest&searchphrase=all), un juego Open Source del que ya hemos hablado en más de una ocasión en nuestra web. Se trata de un simulador de vehículos centrado en las físicas de conducción y colisiones que permite las cosas más locas. Pues [BeamNG.drive](https://beamng.com/game/) es su continuación comercial, y esto es así por que sus desarrolladores son los originarios del juego libre antes mencionado... En este caso se trata de un **juego mucho más evolucionado y pulido**, que lleva unos cuantos años dando guerra en acceso anticipado. Y es que a pesar de esto último, y de estar en proceso de desarrollo, inacabado; el juego tiene una legión de seguidores (entre los que personalmente me encuentro) que aplauden con las orejas cada vez que esta gente saca una nueva versión. En esta ocasión mucho más, ya que gracias a la recién salida del horno, **BeamNG.drive adquiere soporte nativo experimental**.


Hace algún tiempo se lanzó, también de forma experimental, el **soporte gráfico con Vulkan en Windows** , y desde entonces los propios desarrolladores [han comunicado](https://www.beamng.com/threads/linux-port.72927/page-2) en sus foros en más de una ocasión su idea de crear una versión nativa para nosotros, los Linuxeros. El caso es que gracias a Proton hace tiempo que es posible disfrutar del juego de una forma bastante decente, pero obviamente una versión nativa, aunque sea experimental, demuestra al menos un respeto que se agradece mucho desde la comunidad.


Este soporte experimental ha llegado con la **versión 0.25**, que entre otras [muchísimas novedades](https://beamng.com/game/news/patch/beamng-drive-v0-25/), nos pone por fin en su órbita de desarrollo, tal y como podemos ver en el siguiente [comunicado oficial](https://beamng.com/game/news/announce/experimental-linux-support/):


*Saludos, entusiastas de Linux,*


*Con el lanzamiento de la versión 0.25 incluimos soporte EXPERIMENTAL para BeamNG Linux. Sabemos que esto es algo que muchos de ustedes han querido desde hace tiempo, y parece que ha llegado el momento de dar a nuestros jugadores la oportunidad de experimentar con esto.*


*Dado que se trata de un producto experimental en curso, no ofrecemos asistencia al cliente para BeamNG en Linux. Este producto puede tener fallos, ser inestable o todo lo anterior, pero si eres un entusiasta de Linux te animamos a que lo pruebes.*


*Sus comentarios serán muy valiosos para ayudarnos a conseguir una versión de BeamNG para Linux con el soporte adecuado. En caso de preguntas, [utilice este hilo,](https://www.beamng.com/threads/linux-port-%E2%80%93-feedback-known-issues-and-faq.86422/) pero asegúrese de leer primero las directrices.*


*¡Feliz prueba!*


Desde JugandoEnLinux, a partir de ahora, **os informaremos de todas las novedades, y por supuesto en lo tocante a las mejoras de la versión nativa**. También estaos atentos a nuestras redes sociales de [Twitter](https://mobile.twitter.com/jugandoenlinux) y [Mastodon](https://mastodon.social/web/@jugandoenlinux), porque intentaremos realizar algún directo probando esta versión Linux experimental. Os dejamos con un video que hemos grabado donde podéis ver algunos aspectos del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="440" src="https://www.youtube.com/embed/CwiC1YoTriQ" title="YouTube video player" width="780"></iframe></div>
Podeis haceros con la **versión en Acceso Anticipado de BeamNG.drive** en la tienda de [Humble Bundle](https://www.humblebundle.com/store/beamngdrive?partner=jugandoenlinux) (**patrocinado**) o en [Steam](https://store.steampowered.com/app/284160/BeamNGdrive/).

