---
author: Pato
category: "Simulaci\xF3n"
date: 2018-10-03 17:47:20
excerpt: "<p>Egosoft da prioridad al ping\xFCino por encima de MacOS al usar la API\
  \ Vulkan</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f56335a10d7a21c1fd477cd23e9edb72.webp
joomla_id: 866
joomla_url: x4-foundations-llegaria-a-linux-steamos-el-30-de-noviembre-en-forma-de-beta
layout: post
tags:
- accion
- espacio
- proximamente
- ciencia-ficcion
- simulador
title: "X4: Foundations llegar\xEDa a Linux/SteamOS el 30 de Noviembre en forma de\
  \ beta"
---
Egosoft da prioridad al pingüino por encima de MacOS al usar la API Vulkan

¡Que tiempos para jugar en Linux! Ayer mismo Egosoft, la compañía responsable de la saga de acción y estrategia espacial "X" twiteó a preguntas de un usuario que la próxima entrega de la saga, X4:Foundations tiene como objetivo llegar el próximo 30 de Noviembre tanto a Windows como a Linux en fase Beta, pero que debido a que han cambiado la API gráfica a Vulkan tienen difícil lanzar el juego en sistemas Mac debido al pobre soporte de esta API.



> 
> Our goal for now is Windows and Linux Beta at release. OSX probably not in the short term because Vulkan is not easily supported there.
> 
> 
> — EGOSOFT GmbH (@EGOSOFT) [October 2, 2018](https://twitter.com/EGOSOFT/status/1047123618365431808?ref_src=twsrc%5Etfw)


  





Dicho lo cual, y si cumplen su palabra tendremos un nuevo juego de la saga que llevar a nuestros discos duros.


X4: FOUNDATIONS es la esperada secuela de la serie X que trae nuestra mas sofisticada SIMULACIÓN del Universo. Pilota cualquier nave, EXPLORA el espacio o gestiona un Imperio; COMERCIA, LUCHA, CONSTRUYE Y PIENSA con cuidado, mientras te embarcas en una jornada épica.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/dUcXYxILnF8" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


**Características:**


**Libertad para pilotar todas las naves personalmente:**   
  
X4 te permitirá pilotar todas las naves personalmente. X4 te ofrece una gran variedad de naves que pilotar, desde las pequeñas naves exploradoras hasta las enormes Portanaves, todo puede ser pilotado o desde la cabina o a través de una vista externa.


**Módulos en las Estaciones y mejoras de las Naves:**   
  
Construir estaciones espaciales y fábricas siempre ha sido uno de los pilares de los juegos X.


**El universo X más dinámico de todos los tiempos:**   
  
X4 será el primer juego X que permitirá a nuestras razas y facciones construir y expandir libremente sus imperios.


**Gestión de tu imperio con un mapa de gran alcance:**   
  
Una vez que que tengas más naves, NPCs que trabajan como pilotos, tripulantes o directores de estaciones, el mapa será el método preferido para administrarlo todo.


**El juego X con la economía más detallada:**   
  
Uno de los principales puntos de venta de los juegos X siempre ha sido la simulación de una economía real.


**Investigación y Teleportación:**   
  
Aparte de la opción de cambiar de nave a nave de una manera fluida y de que NPCs controlen tu imperio, hay un nivel más. Una vez que posees una flota más grande, estarás muy interesado en investigar una tecnología en tu Cuartel General: Teleportacion.


En X4, puedes empezar tu aventura con una serie de inicios diferentes y eligiendo varios personajes, cada uno con su propio papel, conjunto de relaciones y diferentes naves y tecnologías para empezar.Pero no importa como empieces, siempre tendrás libertad para cambiar de dirección.


De momento no tenemos datos sobre requisitos de sistema en Linux, pero siempre podemos tener una orientación con los de Windows. Podéis verlos en su [página de Steam](https://store.steampowered.com/app/392160/X4_Foundations/), donde X4: Foundations ya se puede pre-comprar, aunque nuestra recomendación es que esperéis a que el juego esté disponible, al menos en versión beta para Linux/SteamOS.


También podéis visitar su [página web oficial](https://www.egosoft.com/download/index_es.php) donde tenéis toda una vasta cantidad de información sobre el juego y su universo.


¿Qué te parece la llegada de X4: Foundations a Linux? ¿Te gustan los juegos de esta saga?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [discord](https://discord.gg/ftcmBjD).

