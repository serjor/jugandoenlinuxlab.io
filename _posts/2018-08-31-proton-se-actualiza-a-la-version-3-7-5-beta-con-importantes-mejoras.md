---
author: leillo1975
category: Software
date: 2018-08-31 07:32:55
excerpt: "<p>Esta nueva versi\xF3n otorga a Steam Play m\xE1s rendimiento y correcciones</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0aa2526f21d94ec0cf0dfc24eb7661b7.webp
joomla_id: 847
joomla_url: proton-se-actualiza-a-la-version-3-7-5-beta-con-importantes-mejoras
layout: post
tags:
- wine
- valve
- steam-play
- dxvk
- proton
title: "Proton se actualiza a la versi\xF3n 3.7.5 Beta con importantes mejoras."
---
Esta nueva versión otorga a Steam Play más rendimiento y correcciones

Después de todo el [revuelo formado por Valve en los últimos dias](index.php/homepage/generos/software/item/960-steam-play-de-valve-da-un-salto-de-gigante-y-amplia-enormemente-el-catalogo-para-linux-steamos-actualizado) en el mundo gamer para Linux, parece que la cosa no se ha quedado ahí y se sigue trabajando de forma constante en mejorar la ya de por si genial versión de Proton. Para quien no conozca Proton, se trata de un **fork de Wine que integra DXVK** y que permite a Steam ejecutar software/juegos de Windows en nuestros equipos con GNU/Linux. Desde que se lanzó [la comunidad de jugadores ha realizado tests a miles de juegos](https://docs.google.com/spreadsheets/d/1DcZZQ4HL_Ol969UbXJmFG8TzOHNnHoj8Q1f8DIFe8-8/htmlview#) arrojando resultados más que notables en una buena cantidad de ellos, y [facilitando información sobre fallos](https://github.com/ValveSoftware/Proton/issues) para que los desarrolladores intenten corregirlo.


Ayer por la tarde/noche, Proton se ha actualizado una vez más alcanzando la versión 3.7.5 (beta) que incluye diversas correcciones y un mayor rendimiento, lo que siempre es de agradecer. La [lista de cambios](https://github.com/ValveSoftware/Proton/blob/proton_3.7/CHANGELOG.md) está en su página de [github](https://github.com/ValveSoftware/Proton) y son los siguientes:



> 
> -Mejoras de rendimiento para las API de sincronización en escenarios limitados por CPU  
>  -La captura automática del ratón en ventanas de pantalla completa está activada de forma predeterminada.  
>  -Más ratios de pantalla tienen resoluciones más pequeñas disponibles.  
>  -Se ha corregido un error en versiones anteriores de SDL.  
>  -Arreglo para la deriva del cursor del ratón en Deus Ex.  
>  -El directorio de volcado del script de depuración puede configurarse con PROTON_DEBUG_DIR.  
>  -Mejoras adicionales en el enfoque a pantalla completa y compatibilidad con python3.
> 
> 
> 


Como veis los cambios no son moco de pavo y ya estais tardando en correr a probar juegos con esta nueva versión. Os recordamos que [disponemos de un hilo en nuestro foro para poder reportar vuestros tests](index.php/foro/juegos-steam-play/127-tests-de-compatibilidad-de-tus-juegos-en-steam-play) con Steam Play. Por supuesto también podeis opinar lo que querais sobre Steam Play y Proton en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


