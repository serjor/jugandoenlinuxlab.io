---
author: Pato
category: "Simulaci\xF3n"
date: 2019-05-03 14:12:02
excerpt: "<p>El juego de <span class=\"css-76zvg2 css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo\
  \ r-qvutc0\" dir=\"auto\">@RaymondDoerr</span> a\xFAn en acceso anticipado sigue\
  \ en oferta</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9e2da3b69f12c2eed78f14bf191f9223.webp
joomla_id: 1038
joomla_url: rise-to-ruins-crea-tendencia-y-aumenta-exponencialmente-su-base-de-usuarios-tras-entrar-en-oferta
layout: post
tags:
- indie
- estrategia
- simulacion
title: Rise to Ruins crea tendencia y aumenta exponencialmente su base de usuarios
  tras entrar en oferta
---
El juego de @RaymondDoerr aún en acceso anticipado sigue en oferta

Rise to Ruins es de esos casos en los que el boca a boca crea tendencia. El juego que aún está en acceso anticipado desde que se lanzó allá por 2014 entró en oferta hace un par de días y desde entonces solo ha subido como la espuma, y todo gracias a un descuento y artículos como el de gamingonlinux.com.


Tanto es así, que su creador se felicitaba en twitter por el repentino éxito, llegando a superar sus propios records de afluencia de jugadores, ventas (mas de 9.000 juegos vendidos en solo unas horas) y ya es trending en las estadísticas de Steam:



> 
> Rise to Ruins is the Number 1 trending game on [@steamcharts](https://twitter.com/steamcharts?ref_src=twsrc%5Etfw) ? [pic.twitter.com/zByWvo76Hm](https://t.co/zByWvo76Hm)
> 
> 
> — Raymond Doerr (@RaymondDoerr) [May 3, 2019](https://twitter.com/RaymondDoerr/status/1124309623761199106?ref_src=twsrc%5Etfw)


  





*Rise to Ruins es un simulador de aldea desafiante, diseñado para cerrar la brecha entre las complejidades de la simulación de aldea con la simplicidad de la estrategia clásica en tiempo real de los años 90. Inspirado en juegos como Towns, Gnomoria, Banished y Dwarf Fortress, pero distinto.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/N1X6UFFN5cI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Si tienes curiosidad, puedes ver Rise to Ruins [en Steam](https://store.steampowered.com/app/328080/Rise_to_Ruins/) donde sigue con un 66% de descuento en estos momentos, o en su [página web oficial](https://risetoruins.com/).

