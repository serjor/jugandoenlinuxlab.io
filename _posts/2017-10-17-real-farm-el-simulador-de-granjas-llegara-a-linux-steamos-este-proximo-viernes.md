---
author: Pato
category: "Simulaci\xF3n"
date: 2017-10-17 12:07:17
excerpt: <p>El juego ha aparecido en SteamDB y tiene el logo y los requisitos para
  Linux</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/616bcb4070200eba2fbb3aacc645866b.webp
joomla_id: 490
joomla_url: real-farm-el-simulador-de-granjas-llegara-a-linux-steamos-este-proximo-viernes
layout: post
tags:
- simulador
- estrategia
- agricultura
title: "'Real Farm' el simulador de granjas llegar\xE1 a Linux/SteamOS este pr\xF3\
  ximo viernes"
---
El juego ha aparecido en SteamDB y tiene el logo y los requisitos para Linux

Hace tan solo unas horas ha aparecido en [SteamDB](https://steamdb.info/app/573680/) para Linux 'Real Farm', un simulador de granjas creado por Triangle Studios y editado por SOEDESCO, compañía que ya nos ha traído a Linux títulos como '[Rogue Stormers](http://store.steampowered.com/app/299480/Rogue_Stormers/)' o '[AereA](http://store.steampowered.com/app/573660/AereA/)'. Guiado por la curiosidad he estado indagando un poco y parece que el juego llegará a Linux/SteamOS, ya que cuenta con el icono y los requisitos para Linux en su web.


Lo llamativo de este 'Real Farm' es su apuesta por la gestión realista (sin llegar al extremo de otros simuladores donde poco menos que tienes que certificarte en manejo de máquinas especiales para poder jugar). Si bien es cierto que a primera vista parece un título "menor" dentro del subgénero de simulación y gestión de granjas, a falta de "farming Simulator" en Linux este 'Real Farm' bien puede cubrir el hueco.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/CEKyPMXbKyQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


**Sobre el juego:**


*Cálzate las botas, arranca tu tractor y cultiva éxito en Real Farm, el simulador agrícola más envolvente que hay. Progresa de jornalero a experto agrícola en el modo Trayectoria o conviértete en agricultor consolidado en el modo Libre. Explora un increíble mundo abierto que da a vida al medio rural americano en 4K nativo y 60 FPS.*


*Céntrate en cultivos, ganado o ambos y gana dinero o reputación sobre la marcha. Adquiere terreno, gestiona personal y conduce y mantén los vehículos agrícolas más potentes disponibles.*


* Sumérgete en un enorme mundo abierto presentado en un magnífico 4K nativo.
* Progresa de jornalero a agricultor experto en el modo Trayectoria.
* Desempeña el papel de un agricultor consolidado en el modo Libre.
* Siembra cultivos, mantén ganado o ambas acciones, comprando y vendiendo en una economía en tiempo real.
* Empieza a arar rápidamente con controles intuitivos y un completo tutorial.
* Conduce y mantén una gran gama de maquinaria y vehículos agrícolas.


**Requisitos:**


**MÍNIMO:**


+ **SO:** Ubuntu o SteamOS
+ **Procesador:** 2.7 GHz Dual Core
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** Nvidia Geforce GTX 670 o AMD Radeon HD 7970
+ **Almacenamiento:** 2 GB de espacio disponible


**RECOMENDADO:**


+ **SO:** Ubuntu o SteamOS
+ **Procesador:** Intel® Core™ i5 4th Gen / AMD A10 series
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** NVIDIA® GeForce® GTX 950
+ **Almacenamiento:** 2 GB de espacio disponible


El juego estará disponible en español el próximo viernes día 20, con lo que la espera no será larga. Si quereis ver un gameplay, han publicado varios vídeos de beta testing. Os dejo uno en español:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/z-egMQS22pM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Si quereis saber más puedes verlo en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/573680/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


¿Te pondrás el mono de trabajo para cultivar y llevar ganadería?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

