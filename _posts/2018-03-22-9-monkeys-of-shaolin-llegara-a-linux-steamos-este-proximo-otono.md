---
author: Pato
category: "Acci\xF3n"
date: 2018-03-22 17:22:02
excerpt: <p>El juego de Sobaka Studios nos trae una atractiva propuesta para los que
  gustan de los juegos de lucha de los 90</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6af8f580e799444e5a52119018e87a65.webp
joomla_id: 689
joomla_url: 9-monkeys-of-shaolin-llegara-a-linux-steamos-este-proximo-otono
layout: post
tags:
- accion
- indie
- aventura
- proximamente
- beat-em-up
- lucha
title: "'9 Monkeys of Shaolin' llegar\xE1 a Linux/SteamOS este pr\xF3ximo oto\xF1o"
---
El juego de Sobaka Studios nos trae una atractiva propuesta para los que gustan de los juegos de lucha de los 90

¿Te gustan los juegos de tipo "Beat'em up" de la era SNES o Sega? pues estás de enhorabuena, por que este próximo otoño el estudio ruso "Sobaka Studios" nos traerá '9 Monkeys of Shaolin', un juego de acción de estética retro donde tendremos que repartir estopa de lo lindo a derecha e izquierda, tal y como hacíamos en aquellos tiempos.


*"Como un simple pescador chino llamado Wei Cheng tendrás que vengar la muerte de tus amigos y familiares asesinados en un ataque pirata a tu tranquilo pueblo. Nuestro protagonista es realmente duro de pelar, pues conoce los principios de las artes marciales ancestrales solo dominadas por los maestros monjes Shaolin. Prepárate para una desafiante aventura en la China medieval en una lucha sin cuartel contra hordas de enemigos."*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/WCl7qpFxXrA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Tendremos acción y batallas con controles intuitivos y una increíble atmósfera inspirada en las grandes películas de kung-fu de los 70. Entre las características principales, tendremos:


* 3 Estilos de lucha únicos: lucha en tierra, en el aire o utiliza los sellos místicos.
* Cautivadora narrativa en la que descubriremos la historia de este misterioso pescador y sus conocimientos de las artes marciales.
* Estilo visual impactante.
* Extenso sistema de desarrollo del personaje.
* más de 25 escenarios
* Modo cooperativo para 2 jugadores


En cuanto a su disponibilidad, tan solo sabemos que llegará en algún momento de este próximo otoño y que está anunciado sin ninguna duda para nuestro sistema favorito. Aún se desconocen los requisitos.


Si quieres saber más detalles, puedes visitar su [web oficial](http://9monkeysofshaolin.com/) o su página de [Steam](http://store.steampowered.com/app/739080/).


¿Qué te parece '9 Monkeys of Shaolin'? ¿Te gustan los Shoot'em Ups de la vieja escuela?  



Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


