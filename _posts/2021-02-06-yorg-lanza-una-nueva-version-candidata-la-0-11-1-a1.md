---
author: leillo1975
category: Carreras
date: 2021-02-06 19:37:23
excerpt: "<p>El juego #OpenSource desarrollado por <span class=\"css-901oao css-16my406\
  \ r-poiln3 r-bcqeeo r-qvutc0\">@ya2tech</span> - <span class=\"css-901oao css-16my406\
  \ r-poiln3 r-bcqeeo r-qvutc0\">@flaviocalva busca la estabilizaci\xF3n </span></p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/YORG/yorg_0_11_localmulti.webp
joomla_id: 1277
joomla_url: yorg-lanza-una-nueva-version-candidata-la-0-11-1-a1
layout: post
tags:
- open-source
- yorg
- ya2
- flavio-calva
- panda3d
title: "YORG lanza una nueva versi\xF3n, la 0.11.1"
---
El juego #OpenSource desarrollado por @ya2tech - @flaviocalva busca la estabilización 


**ACTUALIZACIÓN 6-3-21:**


Tras un mes después de ser lanzada en versión candidata, ya tenemos aquí la [versión definitiva](https://www.ya2.it/posts/0_11_1.html?utm_source=feedburner&utm_medium=email&utm_campaign=Feed%3A+ya2tech+%28Ya2+%C2%BB+News%29) de esta revisión, en la que como os comentamos en la anterior ocasión se **pone el foco en la estabilización y corrección de errores**. Podeis encontrar los links a la versión definitiva en la sección de [descargas](https://www.ya2.it/pages/yorg.html#download) de su página web, y también en sus páginas correspondientes de [GameJolt](http://gamejolt.com/games/yorg/248156), [itch.io](http://ya2.itch.io/yorg) e [IndieDB](http://www.indiedb.com/games/yorg).


![](https://www.ya2.it/images/yorg/yorg_0_11_a.jpg)


Si quereis podeis [colaborar](https://www.ya2.it/pages/support_us.html#support-page) con este proyecto de diversas formas, entre las que se encuentra un [Patreon](https://www.patreon.com/ya2) desde solo 1€. Ahora solo queda esperar por las novedades que nos pueda ofrecer Flavio Calva de su nuevo proyecto por el que nos tiene en ascuas desde hace meses! ;)

---


**NOTICIA ORIGINAL 6-2-21:**  
Hace un año y medio escribíamos sobre el [lanzamiento de la versión 0.11]({{ "/posts/disfruta-de-super-bombinhas-un-plataformas-libre" | absolute_url }}) de este juego de carreras de coches libre. En aquel momento las novedades se centraban en el soporte de de mandos, con force feedback incluido, la mejora del apartado multijugador, tanto local como online, gráficos de partículas, un modelo de conducción revisado, que hacía mucho más controlables los coches y mejora de la IA, entre otros muchos cambios.


Hoy se lanza en **versión candidata**, una revisión de esta versión, que **pone el foco en la estabilización y corrección de errores** del juego, pues según palabras de [Flavio Calva](https://twitter.com/flaviocalva), su desarrollador, este **se encuentra ya muy cerca de lo planeado cuando se inició este proyecto**. En esta nueva build, la [0.11.1RC](https://www.ya2.it/posts/0_11_1_rc.html),  las principales novedades son:


* nuevas traducciones (gd, es, gl, de)  
* Soporte de joypad (varias mejoras, por ejemplo, correcciones: freno, reaparición, vibración; más opciones de configuración)  
* Mejoras en la interfaz gráfica de usuario (mejor resaltado del menú)  
* pequeñas mejoras (ia, cámara)  * correcciones (uso de CPU, multijugador local, multijugador en línea, servidor en línea, audio, interfaz gráfica de usuario, compilaciones)  
* refactorización (soporte para nuevo panda3d, red)  
* mejoras de desarrollo interno (construcción con herramientas de configuración, eliminadas algunas dependencias)*


Como sabreis, en JugandoEnLinux.com somos parte activa del proceso de testeo del juego, y podemos aseguraros que **el juego se siente mucho mejor ahora que hace un año y medio**, siendo notables las mejoras en el control de los vehículos o el funcionamiento de la cámara, por lo que os recomendamos encarecidamente que lo probeis. Recordad también que lo lanzado es una versión candidata, por lo que **si encontrais cualquier problema se agradecería mucho que lo comunicaseis a Flavio** para que pueda corregirlo (teneis el contacto en esta [página](https://www.ya2.it/pages/about.html#about-page)).


También hay que resaltar el **anuncio de un nuevo juego Open Source desarrollado con Panda3D**, al igual que YORG del que dará nuevos detalles pronto. [Por lo poco que podemos intuir](https://youtu.be/csumm3oLiXk) de él  también parece ser un juego de coches, aunque parece que tendrá un enfoque distinto.


Teneis el enlace para descargar esta nueva versión en la [web del anuncio](https://www.ya2.it/posts/0_11_1_rc.html). Por supuesto os informaremos cuando salga la versión definitiva. Mientras tanto podeis divertiros jugando con YORG tal y como lo hicimos nosotros en su día en este video:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/jFhevc5mXM4" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


¿Habeis jugado ya a YORG? ¿Os gustan este tipo de juegos de carreras? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

