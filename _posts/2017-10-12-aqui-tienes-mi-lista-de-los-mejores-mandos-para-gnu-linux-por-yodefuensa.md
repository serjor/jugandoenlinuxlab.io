---
author: yodefuensa
category: Hardware
date: 2017-10-12 08:00:00
excerpt: <p>Nuestro colaborador nos repasa los que a su juicio son los mejores gamepads
  para nuestro sistema favorito</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8ada8b01a991faf3f98f4a7470382ef6.webp
joomla_id: 469
joomla_url: aqui-tienes-mi-lista-de-los-mejores-mandos-para-gnu-linux-por-yodefuensa
layout: post
title: "Aqu\xED tienes mi lista de los mejores mandos para GNU/Linux - Por Yodefuensa"
---
Nuestro colaborador nos repasa los que a su juicio son los mejores gamepads para nuestro sistema favorito

Sí, los jugadores de PC podemos presumir de jugar con teclado y ratón. Y es la mejor dupla para shooters y juegos de estrategia. Pero un buen plataformas se disfruta con un buen mando. Por eso hoy vengo a presentar mi lista sobre los diferentes mandos que podemos usar en nuestro sistema. Si bien es cierto que la inmensa mayoría van a funcionar es importante saber como esta la compatibilidad con los juegos.


Esto quiere decir que un mando no oficial para la xbox, puede funcionar en Linux. Pero no tiene porque hacerlo como un mando de la xbox. Es decir podríamos tener que mapear cada juego con las acciones para cada botón. Por eso me voy a centrar en los de las grandes marcas y como se encuentra su relación con Linux. Empezamos:


**Xbox 360:**


![xbox360](https://images-na.ssl-images-amazon.com/images/I/91f7fO%2B89IL._SL1500_.jpg)


Nos encontramos ante calidad y compatibilidad. La mayoría de juegos de Steam que tienen compatibilidad con mando están pensados para el mando de Microsoft. Esto quiere decir que además de enchufar y jugar tenemos juegos adaptados a sus controles.


Como características especiales podemos destacar que fue de los primeros en incluir un joystick de corto recorrido para tener mayor precisión.


El lado negativo es que en amazon lo podemos encontrar desde 30€, por suerte es un mando con solera. Y en el mercado de segunda mano se encuentra desde 10-15€. Sin duda una compra recomendada.


**Xbox one:**

![Xbox one](https://images-na.ssl-images-amazon.com/images/I/71tayY-WQEL._SL1500_.jpg)


No se puede negar, cuando se presento era la perfección hecha mando. Solo en investigación y desarrollo Microsoft invirtió 100 millones, esto no tiene porque determinar su calidad final. Pero sin duda estamos ante un producto casi redondo


Por un lado nos encontramos con las ventajas de la compatibilidad ya vista en el anterior mando, y por otro mayor comodidad con un acabado más vistoso.


Si merece la pena comprar este mando para jugar en Linux, es una pregunta complicada, su precio es caro, y con él podremos hacer lo mismo que con el mando de la 360. Por lo tanto aquí depende del bolsillo de cada uno.


Además hay que destacar que si queremos jugar de forma inalámbrica tanto para éste como para el de la 360 hay que comprar su adaptador bluetooth.


**Dualshock 3:**


![dualshock3](http://www.bhphotovideo.com/images/images1000x1000/sony_99004_dualshock_3_wireless_controller_1084343.jpg)


Nos encontramos entre manos ante el diseño más conocido y usado. Sin apenas cambios desde el primero para psx.


Si has hecho la prueba de conectar este mando en Windows deberás buscar drivers para hacerlo funcionar. No tendremos estos problemas en la casa del pingüino. Basta con conectarlo para detectarlo como un mando de ps3. Si bien es cierto que en Steam y demás juegos esta parcheado como si fuera un mando de xbox.


A destacar, el uso del joystick en la parte inferior, y mayor separación en la botonera. Podemos decir que es un mando cómodo pero un poco por detrás del resto. Habría que mencionar que se enchufa mediante un cable mini-usb y no micro-usb, un poco menos extendido hoy en día.


No es una compra recomendada de primera mano, y de segunda mano deberíamos apostar por el mando de la xbox 360.


¿Estamos ante un mando que no merezca la pena? No. Gracias a los nostálgicos este mando tiene su nicho. Usar el emulador pcsx y pcsx2 pierde su encanto desde un mando de Microsoft. Destacar además que la forma en la que esta distribuida las flecha y la separación en la botonera a gusto personal podemos decir que es un mando que se adapta mejor a juegos de lucha.


**Steam Controller:**


![steamcontroller](http://www.globalgeeknews.com/wp-content/uploads/2016/09/Steam-Controller-1024x713.jpg)


El niño raro del patio, pero no por ello decepciona. De primeras podemos decir que es un mando que se sujeta diferente, si bien hasta ahora las “¿piernas? ¿cuernos?” del mando estaban hacía atrás esta vez sobresalen hacía adelante, esto no hace que sea raro o incomodo, solo diferente, sigue siendo cómodo, un poco menos confortable que el mando de la xbox one, pero no se queda descolgado.


Ahora bien que es eso del panel táctil. La mejor manera de definirlo es como un ratón de un portátil. ¿Tú jugarías con el ratón de un portátil? ¿No? ¿Entonces porque el steam controller es tan bueno?


Bien imaginemos que con el ratón de ese portátil que jugamos, podemos configurarlo para moverlo de manera precisa para cada juego. Me explico. Configurar la velocidad del ratón, ademas de si queremos que los bordes se configuren de manera que sea como dejar empujado un joystick o que no se siga el movimiento una vez llegado al final. Si se configura como un joystick puede ser fácil manejar un personaje o una cámara. De la otra manera será más fácil configurar el apuntado preciso.


Ahora bien, cada juego compatible con controlador en Steam funcionará con el Steam Controller. Pero puede necesitar algun pequeño retoque, el resto de juegos pensados para jugar con teclado y ratón, pueden ser configurados mediante perfiles, podremos hacer los nuestros o descargarnos de internet los recomendados. Esto simplifica mucho el proceso pero el querer afinarlo al 100% a nuestro gusto puede llevar tiempo. Eso si ganaremos en jugabilidad muchísimo.


¿Merece la pena? Sí. ¿Es como un mando tradicional? Sí ¿Puede sustituir al teclado y ratón? Si y No podemos jugar al Bioshock, pero juegos como el Left 4 Dead se juegan mejor desde el ratón. Pensando en jugar desde el salón en remoto sin cables, pues si es más cómodo que andar con el teclado y ratón de arriba para abajo.


El aspecto negativo su precio. A parte de que solo es recomendable comprarlo de oferta hay que añadir el coste del envío.


También destacar que por desgracia solo podemos hacerlo funcionar si disponemos de steam. Se puede configurar para cualquier juego o emulador, previamente añadiendo el juego como un juego no de Steam.


**Dualshock 4:**


![dualshock4](https://technabob.com/blog/wp-content/uploads/2013/11/playstation-4-dualshock-4-controller.jpg)


Si te estas preguntando porqué no he puesto el Dualshock 4 después del 3 es por una razón. Sony prometió traer el DS4 para pc sin embargo con el paso del tiempo. Sony no sacaba ningún driver ni aplicación de compatibilidad. Fue de la mano de Steam que nos encontramos ante un soporte mejor de lo que podríamos imaginar en un principio.


Pues se añade la costumización del steam controller al mando de ps4. Hablamos de una total integración con Steam, pudiendo configurar el mando a nuestro gusto.


En lo que sobresale el Steam Controller es que permite adaptar un juego pensado para teclado a uno para mando, con el Dualshock 4 se nos vuelve a abrir este abanico de posibilidades.


La mejor forma de demostrar esto es con un video, me hubiera gustado de primera mano probar que tal funciona, pero no es el caso.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/GIc0kUDH1rU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Aquí podemos ver el mapeado y la integración de la que hablo, por desgracia el video esta en Windows.


El aspecto negativo es su elevado precio y también la poca calidad de la goma en los joystick y es que tiende a desgastarse y romperse, tiene fácil solución y es comprar unos pequeños protectores.


En definitiva salvo el Dualshock 3 todos son buenas compras. Mi recomendación personal es el xbox 360, Steam Controller y Dualshock 4 en indistinto orden. Si bien el mando de la Xbox One es increible no puedo ponerlo por encima de estos tres por no aportar nada nuevo que no haga ya el de la 360.


Mencionar el porqué no he incluido Xbox Elite Controller: es porque para sacarle todo el jugo a este magnífico mando debemos usar una aplicación propia desde la Windows Store.


¿Qué te parece mi lista? ¿añadirías alguno más? ¿cual sería tu orden de preferencia?


Cuéntanoslo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


Este artículo ha sido escrito por un colaborador (Yodefuensa). Si quieres que publiquemos algún artículo puedes enviárnoslo [clicando aquí](index.php/contacto/envianos-tu-articulo), y lo valoraremos.

