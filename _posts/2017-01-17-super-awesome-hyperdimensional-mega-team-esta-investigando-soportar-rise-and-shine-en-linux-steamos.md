---
author: Pato
category: "Acci\xF3n"
date: 2017-01-17 22:59:00
excerpt: "<p>El juego est\xE1 cosechando cr\xEDticas muy positivas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/600eb6612b31632f6c618c9c012d873d.webp
joomla_id: 179
joomla_url: super-awesome-hyperdimensional-mega-team-esta-investigando-soportar-rise-and-shine-en-linux-steamos
layout: post
tags:
- accion
- indie
- plataformas
- steam
title: "Super Awesome Hyperdimensional Mega Team est\xE1 investigando soportar 'Rise\
  \ and Shine' en Linux/SteamOS (ACTUALIZADO)"
---
El juego está cosechando críticas muy positivas

Rise and Shine es de esos juegos que quieres que esté en Linux si o si. Mezcla elementos "run and gun" con características novedosas que hacen del juego algo especial. Juzga por ti mismo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/hXYQ4iunOSo" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 A principios de semana y ante la inminente salida del juego en Steam nos pusimos en contacto con el estudio para tantear la posibilidad de que el juego que comienza a despuntar ya en críticas llegara a Linux, pero el estudio nos respondió que de momento no estaba previsto y solo cuando el juego estuviera ya publicado en las plataformas previstas en un primer momento estudiarían la posibilidad de soportar Linux:



> 
> [@JugandoenLinux](https://twitter.com/JugandoenLinux) ¡Gracias! Por ahora sólo PC y Xbox One, pero según cómo vaya todo lo estudiaremos :)
> 
> 
> — SuperMegaTeam (@SuperMegaTeam) [10 de enero de 2017](https://twitter.com/SuperMegaTeam/status/818827189193936896)



 Sin embargo algo parece haber cambiado, pues en los foros de Steam, como siempre preguntados por la misma posibilidad hoy uno de los desarrolladores ha contestado que están investigando/testeando dar soporte a Linux. Puedes ver la respuesta [en este enlace](http://steamcommunity.com/app/347290/discussions/0/364041776192513286/?tscn=1484339827#c144513248277553264).


Siempre es bueno que un buen juego llegue a linux, y mas si se trata de uno con la calidad que se le nota a este 'Rise and Shine', y más aún siendo un estudio español. Si tienes interés pásate por el hilo y ¡muestra tu apoyo para que este juego llegue a nuestro sistema favorito!


¿Que te parece este 'Rise and Shine'? ¿Te gustaría que llegase a Linux? Muestrale tu apoyo al estudio [en el hilo del foro](http://steamcommunity.com/app/347290/discussions/0/364041776192513286/?tscn=1484339827) de Steam.


Cuéntame lo que piensas en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o en [Discord](https://discord.gg/ftcmBjD).


 


**ACTUALIZACIÓN:** **Se acaba de confirmar el juego para GNU/Linux-SteamOS por parte de sus creadores [*Working on Mac and Linux versions as we speak, guys :)* ] .  Podeis comprobarlo en este [post de los foros del juego en Steam](http://steamcommunity.com/app/347290/discussions/0/364041776192513286/?ctp=3#c144513670982134485). Finalmente podremos disfrutar de este juego en nuestro sistema favorito.**

