---
author: leillo1975
category: Software
date: 2019-11-06 11:48:42
excerpt: <p>Un tweet de @SteamDB nos pone sobre la pista</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3bbd7eba6b043fd8a3819fa2bb9dd9c9.webp
joomla_id: 1128
joomla_url: valve-podria-estar-desarrollando-algo-llamado-steam-cloud-gaming
layout: post
tags:
- steam
- valve
- steam-play
- remote-play-together
- steam-cloud-gaming
title: "Valve podr\xEDa estar  desarrollando algo llamado \u201CSteam Cloud Gaming\u201D\
  ."
---
Un tweet de @SteamDB nos pone sobre la pista

Como se suele decir, "se veía venir", tal y como nuestro colega Pato [escribió](index.php/homepage/editorial/4-editorial/995-y-si-valve-estuviera-pensando-en-el-streaming-opinion) hace más de un año aquí mismo, ya que la tendencia en el negocio de la distribución digital de videojuegos y los ultimos movimientos y servicios que estaba proporcionando Valve a sus usuarios de Steam, así lo parecían estar vaticinando.  Y es que la competencia está marcando el camino, y servicios como Xbox Game Streaming, Playstation Now o [Stadia](index.php/homepage/noticias/39-noticia/1122-stadia-la-nueva-plataforma-de-videojuegos-de-google-ha-sido-presentada-en-la-gdc), cada vez estarán más en voga, por mucho que les duela a algunos. **Valve como lider en su sector no debe quedarse atrás**, y parece que se está poniendo manos a la obra, o así al menos parece según lo visto en un tweet de Steam Database:



> 
> Valve is working on "Steam Cloud Gaming" according to partner site code update. Partners will need to sign an addendum to their terms.  
>   
> Could this be a competitor to [@GoogleStadia](https://twitter.com/GoogleStadia?ref_src=twsrc%5Etfw)?<https://t.co/7AQ9YxCol8>
> 
> 
> — Steam Database (@SteamDB) [November 6, 2019](https://twitter.com/SteamDB/status/1192017645765382144?ref_src=twsrc%5Etfw)



Vale, hay que ser claros, por ahora **todo esto hay que cogerlo con pinzas**, pues **se trata de un rumor** basado en un [cambio detectado en sus condiciones legales](https://github.com/SteamDatabase/SteamTracking/commit/0947d1270f23b2c8aa495807022381ba38ed1964#diff-7641e3f4688e6136d56f495b7259c943) para sus socios. Puede que Valve simplemente esté haciendo esto por si acaso, o puede que se esté trabajando para sea una realidad en un tiempo concreto, lo cual a nuestro juicio es la opción más plausible. La frase que ha provocado todo este revuelo es la siguiente:


*'Warning', 'You must agree to the terms in the Steam Cloud Gaming Addendum before continuing.' (“Atención, Tu debes aceptar los terminos en el anexo de Steam Cloud Gaming antes de continuar”)*


Desde luego tienen los mimbres y la infraestructura necesarias para hacer que esto sea posible, pues **desde hace bastante están trabajando en proyectos relaccionados con el Streaming de juegos**, empezando con “**In Home Streaming**” hace años a través del **Steam Link** (hardware), y más tarde a través de aplicaciones instalables en dispositivos móviles y multimedia. Recientemente han reconvertido este servicio rebautizándolo como **“Remote Play”** permitiéndonos realizar esto, pero a través de internet, y eliminando así una gran barrera. Tan solo hace unos días se presentaba “[Remote Play Together](index.php/homepage/generos/software/12-software/1248-steam-se-actualiza-con-remote-play-together-y-la-remodelacion-de-la-biblioteca)” que nos habilitaba para jugar online con amigos a juegos que solo disponían de multijugador local.... y después está ese gran proyecto de hacer que los juegos de Windows puedan ejecutarse en Linux.... ¿como se llamaba? .... ahhh, [Steam Play/Proton](index.php/buscar?searchword=Proton&ordering=newest&searchphrase=all), ¿os suena?. Este último le permitiría a Valve ejecutar gran parte de su catálogo sin soporte nativo en sus servidores con Linux.


Como veis todos los astros se alinean para que esto sea una realidad, y por supuesto nosotros seguiremos bien atentos a la actualidad para que esteis bien informados. Nos gustaría mucho conocer vuestra opinión sobre este tema, por lo que puedes contárnoslo en los comentarios o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

