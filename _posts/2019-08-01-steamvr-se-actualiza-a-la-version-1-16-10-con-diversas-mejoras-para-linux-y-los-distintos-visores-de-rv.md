---
author: Pato
category: Noticia
date: 2019-08-01 17:17:21
excerpt: <p>Valve sigue trabajando para que la experiencia de la realidad virtual
  sea de primer nivel en Linux</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/82dbf6cae1772ebce3f61a4b34447ba4.webp
joomla_id: 1088
joomla_url: steamvr-se-actualiza-a-la-version-1-16-10-con-diversas-mejoras-para-linux-y-los-distintos-visores-de-rv
layout: post
tags:
- steamvr
- valve-index
title: "SteamVR se actualiza a la versi\xF3n 1.16.10 con diversas mejoras para Linux\
  \ y los distintos visores de RV"
---
Valve sigue trabajando para que la experiencia de la realidad virtual sea de primer nivel en Linux

Novedades respecto a SteamVR, pues se ha actualizado a la versión 1.16.10 con diversas mejoras a todos los niveles, tanto para los visores como las APIs. En concreto para Linux las mejoras publicadas son:


Corregido el índice HMD siempre notifica que hay una actualización de firmware disponible.  
Se corrigió la imposibilidad de reiniciar SteamVR después de que vrserver falla (también conocido como LfMutexUnlockRobust se bloquea).  
Se corrigió vrwebhelper que bloquea el cliente de Steam.  
Se corrigieron los colores 'psicodélicos' en el cliente de Steam causados por salir de SteamVR.  
Se corrigieron los mensajes de 'Reiniciar SteamVR' que salían de SteamVR pero no se reiniciaban.  
Se corrigieron varios bloqueos de vrcompositor.  
Se corrigió 'ioctl (GFEATURE): Broken Pipe' spam en stdout.  
Diálogos básicos ajustados durante el inicio para utilizar el programa zenity del host cuando esté disponible.


También hay mejoras para el nuevo visorde VR de Valve, el Valve Index tanto a nivel de firmware del visor como del firmware de los mandos.


Si quieres ver todas las novedades de la nueva versión, tienes toda la información en e[l post del anuncio en Steam](https://steamcommunity.com/games/250820/announcements/detail/1603763772826636893).

