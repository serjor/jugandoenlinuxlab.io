---
author: Serjor
category: Rol
date: 2017-11-28 23:13:39
excerpt: "<p>El estudio ruso AtomTeam ( <span class=\"username u-dir\" dir=\"ltr\"\
  >@the_atomgame </span>) lanza la versi\xF3n final del juego</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e99cca3cbc010706fcba1d1bd7112857.webp
joomla_id: 555
joomla_url: atom-rpg-post-apocalyptic-indie-game-llega-en-early-access
layout: post
tags:
- rpg
- early-access
- atom-rpg
title: 'ATOM RPG: Post-apocalyptic indie game llega en Early Access (ACTUALIZADO)'
---
El estudio ruso AtomTeam ( @the_atomgame ) lanza la versión final del juego

**ACTUALIZACIÓN 19-12-18:** Nos acaba de llegar el aviso de Steam de que el juego finalmente ha sido publicado en su [versión 1.0](https://steamcommunity.com/games/552620/announcements/detail/1717459369224663416https://steamcommunity.com/games/552620/announcements/detail/1717459369224663416), por lo que se da por "finiquitado" el periodo de Acceso Anticipado en el que ha estado este juego en el último año. Podeis ver el tweet donde se anucia su salida oficialmente:  
  




> 
> Atom RPG is released on Steam! We hope that you will like it :)[#indiedev](https://twitter.com/hashtag/indiedev?src=hash&ref_src=twsrc%5Etfw) [#gamedev](https://twitter.com/hashtag/gamedev?src=hash&ref_src=twsrc%5Etfw) [#Steam](https://twitter.com/hashtag/Steam?src=hash&ref_src=twsrc%5Etfw) [#atomrpg](https://twitter.com/hashtag/atomrpg?src=hash&ref_src=twsrc%5Etfw) [#crpg](https://twitter.com/hashtag/crpg?src=hash&ref_src=twsrc%5Etfw) [#Release](https://twitter.com/hashtag/Release?src=hash&ref_src=twsrc%5Etfw) [#HereWeGo](https://twitter.com/hashtag/HereWeGo?src=hash&ref_src=twsrc%5Etfw)<https://t.co/mfrAIQYmSX> [pic.twitter.com/pzCNx2I1yJ](https://t.co/pzCNx2I1yJ)
> 
> 
> — ATOM RPG - DEV BLOG (@atomrpg) [19 de diciembre de 2018](https://twitter.com/atomrpg/status/1075480051905388544?ref_src=twsrc%5Etfw)



 


 [ATOM RPG](https://atomrpg.com/) ha sido actualizado en infinidad de ocasiones durante todo este tiempo, por lo que gracias a estos parches el juego ha crecido y mejorado considerablemente, pasando a ofrecer las siguientes características:  
  
*-Más de 100 horas de juego*


*-Posibilidad de interactuar con más de 450 NPC's únicos, cada uno con su propio retrato, diálogos ramificados y su propio lugar en la estructura del mundo.*


*-Varios finales y varios caminos para llegar a ellos.*


*-El sistema de ventajas, habilidades y rasgos te permitirá crear cualquier tipo de personaje que puedas imaginar, ya sea un luchador o un intelectual viajero.*


*-Más de 250 misiones únicas de todas las formas y tamaños, desde las más simples hasta las más épicas, desde las parcialmente ocultas hasta las más obvias.*


*-90 ubicaciones diferentes, repartidas en tres mapas globales.*


*-Contiene más de 100 tipos de armas, más de 30 tipos de enemigos e incluso un coche.*


Además el estudio promete mucho más contenido en forma de DLC's y parches gratuitos futuros. Como veis el juego merece mucho la pena, taly como indican las reseñas de Steam, y tiene un precio realmente bueno para un juego que ofrece tanto, pues **tan solo cuesta 14.99€**. Podeis ver comprarlo pulsando en el Widget que está un poco más abajo.




---


**NOTICIA ORIGINAL:** Hoy nos llegaba el aviso vía Steam, y es que después de completar su [kickstarter](https://www.kickstarter.com/projects/atomrpg/atom-rpg), el juego [ATOM RPG](http://store.steampowered.com/app/552620/ATOM_RPG_Postapocalyptic_indie_game/) ha sido lanzado, en Early Access, eso sí.


Este ATOM RPG nos recuerda mucho a Fallout 2 o Wasteland, donde tendremos que sobrevivir en un yermo post-apocalíptico, en el que los combates serán por turnos, con una evolución del personaje que hará que la experiencia sea única, y además podremos llegar a finales diferentes para las misiones que vayamos haciendo.


La verdad es que parece tremendamente interesante, y que nos puede llegar a ofrecer una gran cantidad de horas, pero volvemos a recalcar su condición de juego en Early Access, y además debemos señalar que el juego no está traducido al castellano, ni tan siquiera los subtítulos.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/552620/" style="border: none;" width="646"></iframe></div>


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/GHDneF8t-6o" style="border: none;" width="560"></iframe></div>


 


Y tú, ¿piensas hacerte con este RPG clásico? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

