---
author: leillo1975
category: "An\xE1lisis"
date: 2019-04-16 19:43:13
excerpt: "<p>Vemos al detalle las novedades de esta expansi\xF3n de <span class=\"\
  username u-dir\" dir=\"ltr\"><span class=\"username u-dir\" dir=\"ltr\">@FiraxisGames\
  \ y </span></span><span class=\"username u-dir\" dir=\"ltr\">@AspyrMedia</span></p>\r\
  \n<p>&nbsp;</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/bd1351b408feaa7a7bb52dcdff55aef5.webp
joomla_id: 1012
joomla_url: analisis-civilization-vi-gathering-storm-dlc
layout: post
tags:
- estrategia
- aspyr-media
- dlc
- analisis
- civilization-vi
- firaxis
- expansion
- gathering-storm
title: "An\xE1lisis: Civilization VI - Gathering Storm DLC."
---
Vemos al detalle las novedades de esta expansión de @FiraxisGames y @AspyrMedia


 

Pocos juegos son capaces de tenerte sentado horas y horas, perdiendo completamente la noción del tiempo como lo consigue la saga Civilization. En mi caso he de reconocer que me encantan, aunque en muchas ocasiones no los juego todo lo que debería por miedo a lo dicho, y es que cuando lo hago nunca soy capaz de jugar una horita y listo, sinó que puede pasar una tarde entera.... o una noche. Para hacer este anaĺisis he tenido que hacer un "esfuerzo" y "hacer de tripas corazón" y dedicarle un "poco" de ese escaso tiempo que nos deja la vida diaria.


Como sabreis si sois asiduos de nuestra web, [Civilization VI](index.php/homepage/analisis/tag/Civilization%20VI) es un juego que **genera bastantes noticias** en nuestra página desde la [fecha de su salida](index.php/homepage/generos/estrategia/8-estrategia/270-civilization-vi-confirmado-para-linux) hace poco más de dos años, pasando por sus actualizaciones, o la anterior expansión, **Rise & Fall** aquí [también analizada](index.php/homepage/analisis/20-analisis/829-analisis-civilization-vi-rise-fall-dlc). Y es que **el juego es por méritos propios merecedor de toda nuestra atención**, ya que es de agradecer que un título de esta categoría cuente con soporte en nuestro sistema (recordad que [Civilization V](https://www.humblebundle.com/store/sid-meiers-civilization-v?partner=jugandoenlinux) o [Civilization: Beyond Earth](https://www.humblebundle.com/store/sid-meiers-civilization-beyond-earth?partner=jugandoenlinux) también están disponibles). Esta extensa DLC, como todo el material anterior, está desarrollada por [Firaxis Games](https://firaxis.com/), y traida a nuestro querido Linux gracias a [Aspyr Media](https://www.aspyr.com/).


![CivilizationVI GS Announcement 003](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Civilization_VI/GatheringStormDLC/CivilizationVI_GS_Announcement_003.webp)


*En el juego encontraremos montones de nuevas estructuras, maravillas y recursos*


Empezamos pues a hablar de lo que añade esta expansión al juego original. Los **tres aspectos más importantes** de Gathering Storm son sin duda  **los fenómenos naturales**, **el cambio climático** y **las acciones diplomáticas**. Empezando por lo primero hay que decir que estos le **añaden un componente de impredecibilidad al juego**, lo cual es de agradecer. Entre los fenómenos naturales que pueden irrumpir en el devenir de nuestra civilización podemos encontrar las espectaculares erupciones volcánicas, huracanes, sequias, inundaciones.... Los efectos de estos en algunas situaciones son solventables, como por ejemplo con las inundaciones, pudiendo usar presas para contrarrestarlas. Hay que decir que aunque fastidien bastante, **quizás no son todo lo devastadoras que deberían ser**.


En cuanto al **cambio climático**, este proporciona al juego unos **cambios permanentes en el transcurso del juego**. A medida que progresamos, llegará el momento en que descubramos los usos del carbón y el petroleo en la industria. El aprovechamiento de estos acarreará la formación de gases de efecto invernadero que **harán que la temperatura del planeta suba y aumente el nivel del mar**, haya más sequias o más inundaciones. En nuestra mano está el revertir o controlar esta situación, cambiando las **fuentes de energía (recurso introducido en esta expansión)** por otras menos contaminantes como la energías renovables. También hay que tener en cuenta que tanto **el petroleo como el carbón son finitos,** y si no tomamos cartas en el asunto llegará un momento en que nuestra civilización colapse y no tengamos margen de maniobra para seguir avanzando al carecer de energía.


![mandekalu](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Civilization_VI/GatheringStormDLC/mandekalu.webp)


*En las zonas desérticas existen grandes probabilidades de que se formen tormentas de arena*


Algo llamativo del juego es que estos cambios que se producen por culpa de **los fenómenos naturales o el cambio climático pueden ser aprovechados a nuestro favor si sabemos jugar nuestras cartas**. Por poner ejemplos, las zonas inundables, son más fértiles, al igual que las que se han visto afectadas por un volcan.


El otro gran cambio introducido en el juego afecta al **terreno diplomático**, donde esta DLC implementa mejoras sustanciales. Los **puntos de Favor Diplomático** serán una moneda valiosísima que podremos usar a nuestro favor en las deliberaciones del **Congreso Mundial**, donde podremos canjearlos para influir o imponer resoluciones que nos beneficien. Podremos usar, por ejemplo, **las quejas** que hayamos hecho como arma diplomática para presentar como justa una declaración de guerra ante el resto de Civilizaciones. También hay que comentar que Gathering Storm introduce un **nuevo tipo de victoria, la diplomática**, que conseguiremos cuando alcancemos una serie de hitos que convertirán a nuestros personaje en **Lider Mundial**.


![CivilizationVI GS Announcement 002](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Civilization_VI/GatheringStormDLC/CivilizationVI_GS_Announcement_002.webp)


*La estructura del canal nos permitirá cruzar tierra firme con nuestras unidades marítimas para alcanzar otros mares y océanos.*


Por último y no menos importante, como suele ser normal en cada expansión, esta vendrá acompañada de **una buena cantidad de nuevas civilizaciones**. Entre estas **nuevos actores en el tablero de juego** encontraremos Hungría, el pueblo Maorí, Canadá, los Incas, Mali, Suecia, los Otomanos y Fenicia. Entre los líderes de estas facciones destacará  Leonor, duquesa de Aquitania, que podrá hacerse cargo tanto de Francia como de Inglaterra. Por supuesto cada uno de estos pueblos vendrá con sus personajes, unidades especiales, estructuras únicas, maravillas y habilidades. En este sentido, y teniendo solamente en cuenta todas estas civilizaciones, ya de por si es un muy buen argumento para adquirir esta expansión.


![grandvizier](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Civilization_VI/GatheringStormDLC/grandvizier.webp)


*Por supuesto podremos hacer uso de nuevas civilizaciones y personajes, como Solimán en el imperio otomano.*


Es importante decir que **en los primeros compases, el juego puede parecernos un poco vacio**, al igual  que el título original, algo que a nuestro juicio deberían haber mejorado un poco dotándolo de algunas novedades que hagan más ameno el comienzo de la partida, pues puede parecernos en un primer momento (equivocadamente) que hemos invertido mal nuestro dinero. A excepción de los fenómenos naturales, al principio, no se ven grandes diferencias. Eso si, **a medida que vamos avanzando esto cambiará radicalmente** y comenzaremos a tener percepción de todo lo descrito anteriormente. También se podría decir, que aunque en si no es un dato negativo, la cantidad de variables y opciones hacen muy complejo el poder mantener el control sobre todos los apectos del juego.


![mounties render ertwrg](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Civilization_VI/GatheringStormDLC/mounties_render_ertwrg.webp)


*Cada nueva civilización dispondrá de sus propias unidades exclusivas, como los Montados de Canadá.*


En cuanto a los recursos mínimos para poder disfrutar de esta expansión, hay que decir que son los mismos que el juego base. Por supuesto **el juego multiplataforma está permitido**, ya que como sabeis desde [hace unos meses Aspyr consiguió habilitarlo](index.php/homepage/generos/estrategia/8-estrategia/1049-civilization-vi-estrena-multijugador-multiplataforma). Finalmente hay que decir que aparte de todas las virtudes que tiene esta magnífica expansión, el juego tiene un **mensaje ecologista claro** que debería hacernos reflexionar un poco a como estamos tratando a nuestro planeta. Si lo tratamos mal será nuestro enemigo, si lo tratamos bien nuestro aliado. Gathering Storm es, aunque suene a tópico, una expansión imprescindible para todos los fanáticos de la saga que otorga más profundidad y hace de Civilization VI un juego mucho más completo.


**Podeis comprar Civilization VI: Gathering Storm DLC en la [tienda de Humble Bundle](https://www.humblebundle.com/store/sid-meiers-civilization-vi-gathering-storm?partner=jugandoenlinux)**. Recordad que para poder disfrutarla [necesitais el juego base](https://www.humblebundle.com/store/sid-meiers-civilization-6?partner=jugandoenlinux). También podeis adquirir en el mismo lugar la anterior expansión [Rise And Fall.](https://www.humblebundle.com/store/sid-meiers-civilization-vi-rise-and-fall?partner=jugandoenlinux) 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/zn8eP2MBp-8" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Habies jugado ya con Civilization VI: Gathering Storm? ¿Os gustan los juegos de esta saga? Contestanos en los comentarios, o charla sobre este juego en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

