---
author: leillo1975
category: Entrevistas
date: 2017-01-31 14:45:00
excerpt: "<p>El estudio que est\xE1 desarrollando este arcade responde a nuestras\
  \ preguntas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7d0d25bfcaa1421c2931a4f0387462aa.webp
joomla_id: 201
joomla_url: entrevista-con-trinity-team-bud-spencer-terence-hill-slaps-and-beans
layout: post
tags:
- bud-spencer
- terence-hill
- slaps-and-beans
- trinity-team
- entrevista
title: 'Entrevista con Trinity Team (Bud Spencer & Terence Hill: Slaps and Beans)'
---
El estudio que está desarrollando este arcade responde a nuestras preguntas

 Inaguramos esta categoría de entrevistas haciendo unas cuantas preguntas al estudio que está detrás de [Bud Spencer & Terence Hill: Slaps and Beans](http://www.budspencerofficial.com/slaps_and_beans/). Esperamos que no sea la última y pronto os podamos ofrecer más. [Hace unos días os hablamos](index.php/item/282-bud-spencer-terence-hill-slaps-and-beans-llegara-a-gnu-linux) que el juego estaba siendo desarrollado entre otras plataformas para GNU/Linux-SteamOS. En esta ocasión nos pusimos en contacto con ellos y conseguimos que nos respondieran unas pocas preguntas. En primer lugar me gustaría darle las gracias a **Trinity Team** y **Marco Agricola** por perder un poco de su valioso tiempo con nosotros. Sin más, empezamos.


**-JugandoEnLinux(JEL):** ¿Podríais hacer un pequeño resumen sobre vuestra compañía? (Juegos, pais, número de personas, años de existencia, historia...)


**-Trinity Team (TT):** Trinity Team, como una sociedad, está naciendo ahora con el propósito principal de dar vida a Slaps & Beans (S&B), pero todos trabajamos en la industria del videojuego desde hace muchos años. Somos una empresa de cuatro socios fundada en Italia, y entre los juegos que hemos desarrollado o desarrollamos hay varios en Steam y Consolas.


 ![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SlapsAndBeans/SBmiamiGamePlay.webp)


 


**-JEL:** ¿Que distribución de Linux usais para desarrollar el juego?


**-S&B:** Estamos desarrollando principalmente en Windows usando un motor multiplataforma, y para realizar el testeo, nosotros estamos usando Ubuntu Linux 16.04!


 


**-JEL:** ¿Que motor y herramientas de desarrollo usais en la versión para Linux?


**-S&B:** Usamos principalmente Unity3D, Visual Studio y PaintNet para la programación y motor gráfico.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SlapsAndBeans/SBduneBuggy.webp)


 


**-JEL:** ¿Cómo es el proceso de desarrollo? ¿Es más fácil o difícil de lo esperado el desarrollo para Linux? ¿Sois vosotros también usuarios de GNU/Linux?


**-S&B:** Unity nos permite manejar relatiivamente fácil todas las plataformas (Windows, Linux, Mac, PS4, Xbox One, iOs y Android) empezando del mismo código en C#, reduciendo increiblemente los costes y el tiempo de desarrollo. Y si, dos de nosostros somos usuarios de Linux!


 


**-JEL:** ¿Teneis una estimación de tiempo hasta el lanzamiento en Linux?


**-S&B:** A menos que tengamos problemas serios durante la fase de testeo, el lanzamiento en Steam será simultaneo para Linux, Windows y Mac a finales de 2017.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SlapsAndBeans/SBwesternGamePlay.webp)


 


**-JEL:** ¿Cómo estais viendo el panorama del juego en Linux?


**-S&B:** Estamos siguiendolo con curiosidad e interés desde hace bastantes años desde un punto de vista profesional, especialmente desde el lanzamiento de SteamOS!


 


**-JEL:** ¿Qué tipo de juegos tomais como referencia para Slaps & Beans?


**-S&B:** Cualquier Beat'em'Up creado desde Kung-Fu Master en 1984. Personalmente, yo amo de verdad Golden Axe 2 y Batman Returns de Super Nintendo.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SlapsAndBeans/SBbirraESalsiccia.webp)


 


**-JEL:** Si quereis, podeis decir lo que querais sobre vuestro juego y vuestra compañía


**-S&B:**  Para este juego, queremos crear algo que haga un buen tributo a Bud Spencer y Terence Hill, y conquistar los corazones de sus fans. Por el lado de nuestra compañía, tenemos la esperanza de continuar creando juegos mientras mantengamos la misma pasión que nos ha acompañado en nuestro trabajo desde siempre. Nosotros de verdad creemos en ello, si un juego en primer lugar es divertido para nosotros, no puede fallar en entretener a nuestros jugadores.


**-JEL:** Muchas gracias Trinity Team y Marco Agricola de parte del equipo de JugandoEnLinux.com, y por supuesto de nuestros lectores. Seguiremos con interés cualquier noticia sobre vosotros o vuestro juego. Esperamos poder disfrutarlo pronto.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/jswDLLDWjJ4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Y con esto termina la primera entrevista de JugandoEnLinux. Espero que os haya gustado. También me gustaría decir que las preguntas son más o menos las mismas que haremos en un futuro a otros equipos de desarrollo, por lo que si quereis que incluyamos otras en un futuro hacednoslo saber.


Si quereis comentar cualquier cosa sobre esta entrevista podeis dejar un comentario o usar como siempre nuestro canal de [Telegram](https://t.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD). Un Saludo a todos!


 


GIFS Animados: [Steam](https://steamcommunity.com/sharedfiles/filedetails/?id=781553774)

