---
author: Serjor
category: "Acci\xF3n"
date: 2017-06-24 09:50:11
excerpt: "<p>La cosa se pone seria en Croteam con esta actualizaci\xF3n con soporte\
  \ para Vulkan</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/cfee1df0aef1bf88281266898fc4ff19.webp
joomla_id: 387
joomla_url: serious-sam-3-llega-a-serious-fusion-2017
layout: post
tags:
- beta
- vulkan
- serious-sam
- serious-sam-fusion-2017
title: Serious Sam 3 llega a Serious Fusion 2017
---
La cosa se pone seria en Croteam con esta actualización con soporte para Vulkan

Cómo ya os [comentábamos](index.php/homepage/generos/accion/item/333-croteam-anuncia-que-todos-los-serious-sam-soportaran-linux-steamos), Serious Sam 3: BFE iba a ser portado a esa especie de HUB llamado Serious Fusion 2017, y que entre otras cosas soportaría vulkan y VR. Pues bien, nos enteramos gracias a [phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Serious-Sam-3-BFE-Fusion) de que Serious Sam 3 ya está disponible en modo beta, aunque solo la versión con soporte para vulkan, la parte con VR aún no ha llegado.


Así que ni cortos ni perezosos y en un acto de completo altruismo (o porque simplemente queríamos probar a ver qué tal iba) hemos grabado un pequeño gameplay de esta nueva versión bajo vulkan:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/OXUwpLVMc3k" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


A pesar de ser una beta el juego va muy bien. Sí que es cierto que la parte de la intro puede llegar a dar problemas (a mi no me ha pasado, pero a Leo no le iba), pero cómo se puede saltar una vez empieza el juego, aparentemente no hay ningún problema para poder jugar. También tenemos que decir que en el primer intento que hice tuve bastantes "cosas raras", en ningún momento cuelgues, tirones o algo que se pudiera llamar problemas, pero sí que el juego a veces daba una sensación de "esto no está bien, pero no sé porqué". Puede que fuera por el OBS, realmente no estoy seguro, pero debíamos mencionarlo. No obstante como decíamos, y aunque suene contradictorio, el juego va bien, y siendo una beta es de esperar que de vez en cuando haya algún problema.


Esperemos que a partir de aquí Croteam empiece a sacarle chispas a su nuevo motor, sobretodo a la parte que corre bajo vulkan, y veamos de lo que es realmente capaz.


 


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/564310/2535/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 Y tú, ¿ya te has fusionado seriamente? Cuéntanos qué piensas de esta actualización en los comentarios o en los canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

