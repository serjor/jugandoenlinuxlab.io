---
author: leillo1975
category: Carreras
date: 2018-06-20 10:41:00
excerpt: "<p>El juego libre de <span class=\"username u-dir\" dir=\"ltr\">@ya2tech\
  \ alcanza oficialmente la nueva versi\xF3n</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e3cae036c4e2bea1fb8b65e8b2938a97.webp
joomla_id: 777
joomla_url: yorg-0-9-ya-esta-aqui
layout: post
tags:
- open-source
- yorg
- ya2
- coches
title: "\xA1Yorg 0.9 ya est\xE1 aqu\xED!"
---
El juego libre de @ya2tech alcanza oficialmente la nueva versión

Hace tan solo unos días escribíamos un artículo sobre la publicación de la [primera versión candidata](index.php/homepage/generos/carreras/item/885-el-juego-libre-yorg-acaba-de-publicar-la-version-0-9-rc1). Hoy, poco menos que dos semanas después tenemos ya en nuestras manos oficialmente la nueva [versión 0.9 estable](http://www.ya2.it/articles/yorg-09-has-been-released.html#yorg-09-has-been-released), tras algunas pequeñas correcciones. Como os contamos en el otro artículo, tiene importantes novedades entre las que destacaban la **inclusión de un nuevo circuito en la nieve**, un **nuevo tema musical**, más **traducciones de idiomas** (entre los que se encuentra el Español y el Gallego), la **inclusión del efecto Bloom**, y varias **mejoras en el sistema de conducción**. Pero sin duda la más destacable es la inclusión por primera vez de un **modo multijugador, aun en fase experimental**, donde podremos medirnos con nuestros amigos. Para ello necesitaremos una cuenta XMPP.


Como reiteramos, las novedades son importantes, y nuestra comunidad de JugandoEnLinux estamos deseando probar el modo multijugador para ver quien de nosotros es el mejor jugador. Quien sabe, quizás algún día podamos competir en "La partida del Viernes" y probar sus bondades.


Podeis haceros con este juego multiplataforma en [su página de descargas](http://www.ya2.it/pages/download.html). Os dejamos con un video donde [Ya2.it](http://www.ya2.it/), sus desarrolladores, repasan algunas de sus principales novedades:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/HL2YqnVlwVI" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 Estais tardando en bajarlo y dejarnos vuestra opinión sobre esta nueva versión en nuestros comentarios, o en nuestros grupos de [Telegram](https://t.me/jugandoenlinux) y [Discord](https://discord.gg/fgQubVY).

