---
author: Pato
category: Apt-Get Update
date: 2017-09-29 15:33:21
excerpt: "<p>Volvemos a las buenas costumbres, poniendo al d\xEDa lo que nos hemos\
  \ dejado por el camino</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9abae742a9374d5c2cfee072fa802c4b.webp
joomla_id: 470
joomla_url: apt-get-update-especial-lanzamientos
layout: post
title: Apt-Get Update especial lanzamientos
---
Volvemos a las buenas costumbres, poniendo al día lo que nos hemos dejado por el camino

Llevamos unos días sin poder publicar todo lo que quisiéramos (ya quisiéramos poder dedicarnos a esto al 100%) y ya va siendo hora de ponernos al día... con un Apt-Get de lanzamientos. Comenzamos:


**Warbands: Bushido:**


Warbands:Bushido es un juego de mesa de guerra con cartas, miniaturas, dados (prepárese para el juego de mesa random) y bonitos tableros de combate. Todo esto en un wargame multiplataforma con combates entre jugadores como en los mejores juegos de mesa y con escenarios pintorescos (en desarrollo) en campañas de un jugador.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/jTBWfKG6FxI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/556820/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


**Cook, Serve, Delicious! 2!!**


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/qoP53DTpbmQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/386620/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


 **Citadale: The Legends Trilogy**


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/BSvFW4g4_Bo" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/670410/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


**Morphite**


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/fXY4OMy9IBw" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/661740/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


**Grave Chase**


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/b92_uYIkTLI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/611630/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


**SteamWorld Dig 2**


¡SteamWorld Dig ha vuelto! Cava profundo, hazte con riquezas y desentraña los terrores del mundo subterráneo en esta aventura minera de plataformas, inspirada en el estilo de juego clásico de Metroidvania.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/trLZKer9uPI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/571310/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


**Caveman Warriors**


 Vuelve atrás en el tiempo y desata tu cavernícola interior. Aplasta cabezas en este juego de plataformas cooperativo. Juega solo o con amigos hasta 4 jugadores. Inspirado por juegos como New Super Mario, Joe & Mac, Metal Slug, Castle Crashers y Trine.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Tc_y6AOi9cU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/603420/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

