---
author: Pato
category: Apt-Get Update
date: 2017-03-17 19:56:14
excerpt: <p>Nuevo Viernes, nuevo Apt-Get...</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/eb9b7452cdc806568d2312ea5614a301.webp
joomla_id: 257
joomla_url: apt-get-update-albion-online-fossil-echo-man-o-war-nowhere-prophet-fractured-worlds
layout: post
title: Apt-Get Update Albion Online & Fossil Echo & Man O' War & Nowhere Prophet &
  Fractured Worlds...
---
Nuevo Viernes, nuevo Apt-Get...

De nuevo otra semana se nos va, y tenemos que repasar lo que se nos quedó sin publicar. ¿Comenzamos?


Desde [www.gamingonlinux.com:](http://www.gamingonlinux.com)


- El trailer de Albion Online, el MMO en fase beta que llegará a Linux próximamente impresionó a Liam por su contenido. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/the-trailer-for-the-mmo-albion-online-galahad-update-is-impressive.9295).


- Fossil Echo es un interesante juego de aventuras "sin palabras" que ya está disponible en Linux. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/fossil-echo-an-adventure-game-about-a-boys-wordless-journey-is-now-on-linux.9318).


- El cliente de la tienda itch.io ahora ha sido mejorado para solucionar las dependencias. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/the-open-source-itch-store-client-has-gotten-smarter-about-64bit-and-dependencies-on-linux.9314).


- Man O' War: Corsair Chaos Campaign es un interesante juego que está ya disponible en Linux después de que Liam contactase con los desarrolladores. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/man-o-war-corsair-warhammer-naval-battles-is-now-on-linux-after-i-poked-the-developer.9334).


Desde [www.linuxgameconsortium.com:](http://www.linuxgameconsortium.com)


- Nowhere Prophet es un juego de cartas para un jugador con una pinta excelente y está en campaña en Greenlight. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/nowhere-prophet-roguelike-deck-building-greenlight-50062/).


- Fractured Worlds es la nueva expansión para Victor Vran Overkill edition. Estará disponible durante esta primavera. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/fractured-worlds-announced-victor-vran-overkill-edition-50002/).


Vamos con los vídeos de la semana:


Albion Online:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/94kj8Z7rw4o" width="640"></iframe></div>


 Fossil Echo


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/Mn8h2US0Qfk" width="640"></iframe></div>


 Man O' War: Corsair Chaos Campaign


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/KiF3Nm7s3Ds" width="640"></iframe></div>


 Nowhere Prophet


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/k1E6tIuC0fs" width="640"></iframe></div>


 VV Fractured World


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/V7DeEwDyeKo" width="640"></iframe></div>


 Y esto es todo por esta semana. ¿Qué te ha parecido?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

