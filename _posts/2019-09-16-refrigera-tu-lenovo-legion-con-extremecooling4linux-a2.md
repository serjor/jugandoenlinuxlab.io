---
author: leillo1975
category: Software
date: 2019-09-16 08:13:44
excerpt: "<p>La utilidad, desarrollada por nuestro amigo @OdinTdh, se ha vuelto a\
  \ actualizar. #LenovoLegion .</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/46dab725dcec21832ff6fc55e05fcea7.webp
joomla_id: 1110
joomla_url: refrigera-tu-lenovo-legion-con-extremecooling4linux-a2
layout: post
tags:
- software
- linux
- software-libre
- lenovo
- refrigeracion
- extremecooling4linux
- legion
- lenovo-nerve-sense
- odintdh
title: "Refrigera tu Lenovo Legion con \"ExtremeCooling4Linux\" (ACTUALIZACI\xD3N\
  \ 2)"
---
La utilidad, desarrollada por nuestro amigo @OdinTdh, se ha vuelto a actualizar. #LenovoLegion .


**ACTUALIZADO 8-08-20**: Tenemos nuevas noticias de este utilidad, y es que hace tan solo unas horas se ha vuelto a actualizar a la [versión 0.3](https://gitlab.com/OdinTdh/extremecooling4linux/-/releases/0.3), que incluye **novedades importantes** tales  como:


-**Añadido soporte de línea de comandos**: se admiten parámetros de activación, desactivación y cambio de estado (enable, disable, change-state)  
-Se crearon nuevos **paquetes de AppImage y deb**  
-**Soporte para applets de escritorio** (ver [indicator-extremecooling4linux](https://gitlab.com/OdinTdh/indicator-extremecooling4linux) para más información)


Como veis, el ultimo punto es **nuevo proyecto de @OdinTdh** que nos permite controlar activando y desactivando ExtremeCooling4Linux desde la barra de herramientas gracias a un iconito, del que podeis ver una instantanea justo aquí debajo:  
![Indicador](/https://gitlab.com/OdinTdh/indicator-extremecooling4linux/-/raw/master/indicator-ec4l_screenshot.webp)




---


**ACTUALIZADO 16-07-20**: Llevábamos tiempo sin tener noticia de esta estupenda utilidad para los **portátiles de Lenovo Legion**, pués que sepais que recientemente se ha actualizado a la [versión 0.2](https://gitlab.com/OdinTdh/extremecooling4linux/-/releases/0.2) incluyendo los siguientes cambios:


-Añadida una **nueva pestaña de sistema con datos de la BIOS, modelo de portátil e información de la placa base** (ver screenshot abajo)  
-Eliminación de la dependencia del dmidecode en el código  
-Arregladas advertencias de configuración de fuentes.  
-**Nueva AppImage** publicada en itch.io


![ExtermCooling4Linux02](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ExtremeCooling4Linux/ExtermCooling4Linux02.webp)


Como os comentamos la última vez podeis encontrar el código e información sobre "ExtremeCooling4Linux" en la [web de su proyecto](https://gitlab.com/OdinTdh/extremecooling4linux). También podreis encontrar la [última AppImage en itch.io](https://odintdh.itch.io/extremecooling4linux).




---


**NOTICIA ORIGINAL**:


Supongo que todos conoceis [PyLinuxWheel](index.php/buscar?searchword=Pylinuxwheel&ordering=newest&searchphrase=all), esa fantástica utilidad que al igual que [Oversteer](index.php/buscar?searchword=oversteer&ordering=newest&searchphrase=all&limit=20) nos permite configurar nuestros volantes Logitech en Linux. Pues su creador, **OdinTdh**, uno de los miembros más proactivos y antiguos de nuestra comunidad, ha decido lanzar **ExtremeCooling4Linux**, otra utilidad que responde a  una necesidad personal, pero que estamos seguros que a más de uno le vendrá bien.


Si sois poseedores de uin [**portátil Legion**](https://www.lenovo.com/es/es/legion/), la linea de **Laptops para Juegos** de la marca **Lenovo**, probablemente conozcais que en Windows incluye una tecnología, **Lenovo Nerve **Sense****, que permite enfriar rapidamente nuestro portátil cuando las temperaturas son altas. En linux no podíamos activar este modo, con lo que al llevar un rato jugando a nuestros títulos favoritos (especialmente los más exigentes) el portátil se calentaba de más, algo que como sabeis no es nada bueno para cualquier ordenador. Gracias a **ExtremeCooling4Linux**, podemos activar este modo y de esta forma mantener "fresco" nuestro portátil.


![ExtremeCooling4LinuxVentana](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ExtremeCooling4Linux/ExtremeCooling4LinuxVentana.webp)


La utilidad ha sido creada para funcionar en el **Lenovo Legion Y520**, pero debería funcionar correctamente en otros modelos como el **Lenovo Legion Y720**. Debeis saber que el programa no tiene garantía y si lo usais es bajo vuestra propia responsabilidad, por lo que no debeis intentar usarlo **SOLO** en portátiles que no tengan soporte de "Extreme Cooling"  de Lenovo. Por supuesto, la utilidad es **Open Source** y se puede usar bajo la **licencia GPL3**. El programa se acaba de [actualizar](https://gitlab.com/OdinTdh/extremecooling4linux/-/releases) a la versión 0.11 que tiene las siguientes características:


*-Activación y desactivación el modo de ventilador de enfriamiento extremo en el Lenovo Legion Y520. Debería funcionar en otros portátiles similares de Lenovo Legion, como el Lenovo Legion Y720, pero sólo se ha probado en el modelo Lenovo Y520.*  
*-Traducciones de inglés y español*  
*-Paquete AppImage disponible en <https://odintdh.itch.io/extremecooling4linux>*  
*-Deshabilitar el enfriamiento extremo activo/desactivo sin root cuando ExtremeCooling4Linux se ejecuta desde AppImage sin privilegios de root*  
*-Mejor control cuando un usuario intenta habilitar la refrigeración extrema en un portátil no soportado*


Teneis el código y más información sobre el proyecto en su [página de GitLab](https://gitlab.com/OdinTdh/extremecooling4linux), y podeis descargarla en una cómoda [AppImage](https://odintdh.itch.io/extremecooling4linux) desde Itch.io. Seguiremos informando sobre futuras actualizaciones de este software. Podeis dejar vuestras impresiones  sobre esta apliación en los comentarios de esta noticia  o vuestros mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

