---
author: leillo1975
category: Carreras
date: 2022-11-14 15:52:00
excerpt: "<p>Ya podemos descargar la nueva actualizaci\xF3n de este veterano juego\
  \ libre.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/StuntRally/StuntRally27.webp
joomla_id: 1508
joomla_url: lanzada-la-version-2-7-de-stunt-rally
layout: post
tags:
- open-source
- stunt-rally
- ogre
title: "Lanzada la versi\xF3n 2.7 de Stunt Rally"
---
Ya podemos descargar la nueva actualización de este veterano juego libre.


 Si mal no recuerdo, la última vez que os hablamos de este juego fue cuando hicimos un artículo sobre "[Los mejores juegos de conducción libres]({{ "/posts/los-mejores-juegos-de-coches-libres" | absolute_url }})", hace ya casi 4 años. En aquel momento la versión "vigente" era la 2.6 que ya tiene la friolera de 7 años, y sinceramente un servidor ya creía que el proyecto estaba abandonado, pero de repente hace unos meses todo comenzó a moverse de nuevo y finalmente se ha lanzado hace unos días una novísima versión con montones de cambios.


Para quien no lo conozca, [Stunt Rally](http://stuntrally.tuxfamily.org/) es un videojuego libre (creado por [Crystal Hammer](https://cryham.tuxfamily.org/)) de carreras de coches y más tipos de vehículos,  escrito en C++ , con el motor gráfico [OGRE]({{ "/tags/ogre" | absolute_url }}), y que parte del trabajo realizado en otro conocido juego libre, [VDrift]({{ "/tags/vdrift" | absolute_url }}), Dispone de un **apartado gráfico muy cuidado,** y permite jugar tanto de forma **individual**, como **online** y a **pantalla partida**. Su jugabilidad es completamente **arcade** y seguro que os hará pasar un rato de lo más divertido recorriendo su **infinidad de pistas** de lo más variopinto, encontrando escenarios realistas y otros de absoluta fantasía , con toboganes, rampas y saltos imposibles.


![](https://lh3.googleusercontent.com/pw/AL9nZEUIW-5cFUkOzVhN2HGIZCZJe8kwwEvu94r1Vq4fjYMG2lu_cg5QJFF0ATOmm6zni1WheA0Mf6OjA2il-wP6rC5uZC1lsL7XEtgpLWZsrdyXb_rmWFyy7MYbHXCeUsy8KKbPVa9y7xbEZ7T7TnJ9-5eX=w1540-h939-no?authuser=0.webp)


En esta última versión (2.7) podemos encontrar los siguientes [cambios](https://stuntrally.tuxfamily.org/wiki/doku.php?id=changelog) entre los que **destacan** :


* *34 nuevas pistas, de un total de 202, siendo renovadas 130.*
* *7 nuevos vehículos*
* *Escuela de conducción, con 6 lecciones, repeticiones con subtítulos.*
* *Reestructuración de la antigravedad de las naves espaciales, menos ideal en el modo de simulación normal*
* *Linea de trazada*
* *Modo de Simulación*
* *Posibilidad de rebobinar*
* *Jugabilidad suavizada, con menos parones y frecuencia de actualización más alta*
* *Gráficos actualizados con nuevas rocas, ríos, cascadas, hierba, y mejores partículas de fuego, humo, nubes...*
* *Múltiples cambios en la interfaz, los menúes y el editor.*


Si queréis descargar el juego podéis encontrar su [código fuente](https://github.com/stuntrally/stuntrally/releases/tag/2.7) en la web de su proyecto (ahora en [Github](https://github.com/stuntrally/stuntrally)) , pero también existe una versión en [Flatpak](https://flathub.org/apps/details/org.tuxfamily.StuntRally). Se esperan versiones en [Snap](https://snapcraft.io/stuntrally) en un futuro, así como la actualización del [PPA Xtradeb para Ubuntu](https://xtradeb.net/play/stuntrally/) próximamente. Os dejamos con un video de esta novísima versión:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/84OhGtslLpI" title="YouTube video player" width="780"></iframe></div>


¿Conocíais Stunt Rally? ¿Qué os parece esta propuesta de juego libre? Puedes contarnos tu parecer sobre esta noticia en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

