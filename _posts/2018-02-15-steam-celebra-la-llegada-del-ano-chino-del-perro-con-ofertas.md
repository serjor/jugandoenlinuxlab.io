---
author: Serjor
category: Ofertas
date: 2018-02-15 21:31:23
excerpt: "<p>Nuevo a\xF1o lunar, nuevas ofertas en Steam</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e5a83a7ecd853a3363498d71a1613623.webp
joomla_id: 647
joomla_url: steam-celebra-la-llegada-del-ano-chino-del-perro-con-ofertas
layout: post
tags:
- steam
- ofertas
- ano-chino
title: "Steam celebra la llegada del a\xF1o chino del perro con ofertas"
---
Nuevo año lunar, nuevas ofertas en Steam

A Steam rara vez le faltan los motivos para anunciar algún tipo de oferta, pero que en china estén celebrando año nuevo es un evento que tienen marcado en el calendario y era muy previsible que llegara a lo largo de la semana.


Y así ha sido, nos lo anunciaban en [Vandal](https://vandal.elespanol.com/noticia/1350703776/hoy-comenzaran-las-nuevas-ofertas-de-steam/), y a través del grupo de telegram nos lo confirmaban la misma gente de Vandal:







Y es que hoy se anunciaba en todos lados a bombo y platillo que desde el 15 de febrero hasta el 19 tendríamos llegarían las ofertas para conmemorar el año chino, y hoy a las 19:00, fieles a la cita, la gente de Steam estrenaba la semana de ofertas.


Es cierto que no son tan potentes como las ofertas de invierno, pero siempre es de agradecer (o no, según lo que diga la cartera) tener algún descuento entre temporada para hacerse con esas joyas ocultas tapadas por grandes títulos.


Y tú, ¿qué piensas comprar durante estas ofertas? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

