---
author: leillo1975
category: Carreras
date: 2020-04-16 14:47:39
excerpt: "<p>Su desarrollador <span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x\
  \ r-bcqeeo r-qvutc0\">@Applimazing</span> lo ha anunciado recientemente.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/UltimateRacing2D/UltimateRacing2d.webp
joomla_id: 1210
joomla_url: ultimate-racing-2d-ya-tiene-version-linux-nativa
layout: post
tags:
- retro
- 2d
- racing
- applimazing
- ultimate-racing-2d
title: "Ultimate Racing 2D ya tiene versi\xF3n Linux nativa"
---
Su desarrollador @Applimazing lo ha anunciado recientemente.


 No teníamos noticia de este videojuego, pero gracias al artículo publicado en [LinuxGameConsortium](https://linuxgameconsortium.com/ultimate-racing-2d-native-support-now-available/), hemos sabido que la compañía que lo ha desarrollado, la Neerlandesa [Applimazing](https://www.applimazing.com/), **ha incluido finalmente el soporte a nuestro sistema operativo en la extensa lista de plataformas para las que está disponible** (Windows, Mac, Xbox One, Switch, Android e iOS), lo cual es de agradecer, pues el juego realmente pinta estupendamente. Podíamos ver el anuncio recientemente en twitter:  
  




> 
> Ultimate Racing 2D is now also available on Linux:<https://t.co/j06XuGGJ94>[#Linux](https://twitter.com/hashtag/Linux?src=hash&ref_src=twsrc%5Etfw) [#Linuxgaming](https://twitter.com/hashtag/Linuxgaming?src=hash&ref_src=twsrc%5Etfw) [#Ubuntu](https://twitter.com/hashtag/Ubuntu?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/9Yy00w8pRH](https://t.co/9Yy00w8pRH)
> 
> 
> — Ultimate Racing 2D (@URacing2D) [April 14, 2020](https://twitter.com/URacing2D/status/1250173489266515973?ref_src=twsrc%5Etfw)



El juego presenta la acción con un **aspecto retro ochentero** y **vista cenital**, y tiene un aire al mítico [SuperSprint](https://en.wikipedia.org/wiki/Super_Sprint) o al libre [Dust Racing 2D](https://github.com/juzzlin/DustRacing2D), pero teniendo como característica el **increible número de categorías, coches y pistas** diferentes que podemos disfrutar. En el juego podremos correr desde karts hasta los más rápidos monoplazas, y dispone de un "**modo Carrera**", modo de **Campeonato**, y **multijugador** tanto **online** (¡de hasta 20 jugadores!) como **local**. Las características del juego son las siguientes:


- 35 categorias de carreras  
 - Más de 300 coches diferentes  
 - Más de 45 pistas internacionales  
 - Multijugador en línea hasta 20 jugadores  
- Multijugador local hasta 8 jugadores  
 - Modo carrera, modo campeonato y modo carrera rápida  
 - Hasta 20 coches en la pista  
 - Personalización de equipos  
 - Parada en boxes, calificación y opciones de impulso  
 - Física automovilística realista y efectos de sonido  
 - Divertido juego con emocionantes peleas de IA  
 - Gráficos visuales en 2D sorprendentes  
 - Efectos del clima  
 - Modo espectador  
 - El juego soporta el teclado y controladores


Como veis el juego no puede dejar indifierente a nadie y un servidor ya esta deseando echarle el guante. Si quereis adquirirlo podeis comprarlo en [Steam](https://store.steampowered.com/app/808080/Ultimate_Racing_2D/). Os dejamos con este video que hemos grabado para vosotros:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/iE_qiIKOWUI" width="800"></iframe></div>


¿Qué os parecen los juegos de carreras Retro? ¿Os atrae lo que nos propone este [Ultimate Racing 2D](https://www.ultimateracing2d.com/)? Podeis darnos veustra opinión sobre este juego en los comentarios de este artículo, en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

