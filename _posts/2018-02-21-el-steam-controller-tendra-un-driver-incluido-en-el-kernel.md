---
author: leillo1975
category: Hardware
date: 2018-02-21 08:17:19
excerpt: "<p>Se espera que sea intruducido en pr\xF3ximas versiones.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1a76fc9d78dc2a3abb4431882931feda.webp
joomla_id: 655
joomla_url: el-steam-controller-tendra-un-driver-incluido-en-el-kernel
layout: post
tags:
- steam-controller
- driver
- kernel
title: "El Steam Controller tendr\xE1 un driver incluido en el Kernel (ACTUALIZADO)"
---
Se espera que sea intruducido en próximas versiones.

**ACTUALIZACIÓN 22-3-18**: Un mes después, nos llega de nuevo a través de [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Linux-Steam-Controller-HID-V6), la noticia de que el driver para el Kernel del Steam Controller ya funciona correctamente con el cliente de Steam. Hasta ahora si funcionaba el driver del Kernel, no funcionaba el de Steam, pero ya se ha conseguido que el primero se detenga y use los drivers proporcionados en el espacio de usuario por el cliente de la conocida tienda de Valve. Una vez cerrado Steam, volverá a su posición inicial.


Hasta ahora **solo se ha podido emular la funcionalidad de ratón y teclado** (lizard-mode) , pero con el tiempo se espera que tenga capacidades completas cuando se termine el trabajo de otorgar todas las funciones de este popular mando.


 




---


**NOTICIA ORIGINAL**: Nos llega una noticia de [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Steam-Controller-RE-Kernel), que **a través de ingeniería inversa**, el desarrollador [Rodrigo Rivas Costa](https://github.com/rodrigorc) está creando un [driver](https://github.com/rodrigorc/inputmap) para el Kernel para poder usar directamente el Steam Controller. Como sabreis, hasta ahora era necesario tener arrancado el cliente de Steam para poder difrutar de todas sus funcionalidades. También podíamos hacer uso de [SC-Controller](https://github.com/kozec/sc-controller) si queriamos usar apliaciones externas a Steam.


Gracias a este driver podremos en un futuro usar el conocido gamepad tanto de forma inalámbrica, como a través de un cable USB. Se está desarrollando como un **dispositivo HID** por lo que cualquier aplicación podrá usarlo sin problemas. Estará habilitado el uso de botonoes, ejes, pads, así como la desconexión inalambrica. También es necesario decir que **aun queda por desarrollar** el uso de acelerómetros, giroscopios, la funcionalidad haptica y de vibración, y los LEDS.


El driver está pendiente de revisión en las listas de correo del kernel, pero aun no está solicitada su inclusión en este. Suponemos que hasta que esté más desarrollado e incluya más opciones no se introducirá en algún lanzamiento del núcleo. Tendremos que estar atentos a este desarrollo pues lo que propone e muy interesante. Actualmente muchos juegos libres o no distribuidos por Steam tienen problemas para usar el Steam-Controller, incluso los ejecutados por Wine. A veces hay que hacer verdaderos malabarismos para hacer que funcionen correctamente, lo cual suele desembocar en dejar de usar este controlador.


Como veis, el software libre, tarde o temprano siempre suele dar opciones a los usuarios, y por supuesto este tipo de iniciativas siempre serán bienvenidas por parte de la comunidad más jugona.


¿Sois usuarios de un Steam Controller? ¿Habeis tenido problemas para usar este mando con algún juego? Déjanos tus respuestas en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

