---
author: CansecoGPC
category: Aventuras
date: 2021-10-17 20:25:31
excerpt: "<p>Nueva versi\xF3n de <a href=\"https://twitter.com/ScummVM\" target=\"\
  _blank\" rel=\"noopener\">@ScummVM</a> el interprete de aventuras por excelencia\
  \ libre y de c\xF3digo abierto.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Scummvm/articulojelscummvm25.webp
joomla_id: 1377
joomla_url: scummvm-2-5-20-aniversario-desde-la-version-0-0-1
layout: post
tags:
- software
- scummvm
- interprete
title: ScummVM 2.5, 20 aniversario desde la version 0.0.1
---
Nueva versión de [@ScummVM](https://twitter.com/ScummVM) el interprete de aventuras por excelencia libre y de código abierto.


Nueva versión del interprete de aventuras por excelencia libre y de código abierto, desde la primera versión que Ludvig Strigeus subió a internet en 2001.


**Motores**


10 motores nuevos, entre ellos:


El motor 2.5D, isométrico o falso 3D, ResidualVM integrado, soportando juegos como Grim Fandango, The Longest Journey y Myst 3: Exile


  
AGS 2.5+, No todos están soportados por temas de dependencias con DLLs, lista de juegos compatibles: <https://wiki.scummvm.org/index.php?title=AGS/Games>


**Nuevos juegos soportados**


Little Big Adventure, Red Comrades 1: Save the Galaxy, Red Comrades 2: For the Great Justice  
Transylvania, Crimson Crown, OO-Topos, Glulx interactive fiction games, Private Eye  
Nightlong: Union City Conspiracy, The Journeyman Project 2: Buried in Time  
Crusader: No Remorse, L-ZONE, Spaceship Warlock


![scummvm interfaz](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Scummvm/scummvm-interfaz.webp)


**Otros**:


La interfaz ha sido rediseñada, soporta Unicode y se adapta mejor (HiDPI).  
Gracias a un estudiante de GSOC (Google Summer of Code), se ha agregado soporte para texto a voz para los juegos, Sfinx, Soltys y The Griffon Legend.  
Traducciones al español de Blue Force, Ringworld y Amazon: Guardians of Eden.


**Descarga:**


Disponible en los repositorios de Arch y derivadas, así como [Snap](https://snapcraft.io/scummvm) y [flatpak](https://www.flathub.org/apps/details/org.scummvm.ScummVM) ya están disponibles.


 


Fuente original: <https://www.scummvm.org/news/20211009/>


 


**¿Qué se bebe en Scumm Bar?** Cuéntamelo si te atreves en los canales habituales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)


 

