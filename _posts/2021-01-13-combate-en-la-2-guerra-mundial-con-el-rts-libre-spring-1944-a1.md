---
author: leillo1975
category: Estrategia
date: 2021-01-13 15:15:22
excerpt: "<p>Este s\xE1bado organizmos una partida&nbsp; de <span class=\"css-901oao\
  \ css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\">@Spring19441</span>. Inf\xF3\
  rmate aqu\xED. </p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Spring1944/spring1944.webp
joomla_id: 1268
joomla_url: combate-en-la-2-guerra-mundial-con-el-rts-libre-spring-1944-a1
layout: post
tags:
- rts
- open-source
- software-libre
- estrategia-en-tiempo-real
- springrts
title: "Combate en la 2\xAA Guerra Mundial con el RTS libre Spring:1944 (ACTUALIZADO)"
---
Este sábado organizmos una partida  de @Spring19441. Infórmate aquí. 


**ACTUALIZACIÓN 15-1-21:**


Gracias a la buena acogida que ha tenido el juego en JugandoEnLinux.com, los usuarios de nuestra comunidad, han decidido organizar una partida mañana Sabado, 16 de Enero a las 20:00h CET. Si quieres participar y apuntate en nuestro grupo de [Telegram](https://twitter.com/JugandoenLinux) o nuestra [sala JugandoEnLinux de  Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org?via=matrix.org&via=t2bot.io&via=ggc-project.de), donde también podrás recibir más detalles de como se organizará. Para la comunicación por voz usaremos como siempre [nuestro servidor mumble](http://mumble.jugandoenlinux.com:64738/). ¡Estas invitado!


 




---


  
**NOTICIA ORIGINAL 13-1-21:**


Ciertamente si había un juego que merecía un espacio en esta web era este que hoy nos ocupa, y es que uno de sus desarrolladores es **uno de los miembros de nuestra comunidad**: [Jose Luis Cercós Pita](https://github.com/sanguinariojoe),  **@sanguinariojoe** para los amigos, algo que por supuesto nos llena de orgullo (y satisfacción). No sería para nada justo que siguieramos ignorando este fantástico juego, y mucho menos cuando hay trabajo de un conocido detrás.


Si recordais, hace unos meses os hablábamos de [Beyond All Reason]({{ "/posts/beyond-all-reason-bar-un-rts-libre" | absolute_url }}), **BAR** para los amigos, un juego de estrategia real basado en el conocido [Balanced Annihilation](https://springrts.com/wiki/Balanced_Annihilation), juego que a su vez era un remake del clásico [Total Annihilation](https://es.wikipedia.org/wiki/Total_Annihilation). Pues ahora olvidémonos de la ambientación futurista de estos juegos y vayámonos a la épica **2ª Guerra Mundial**. Sustituyamos los robots, naves espaciales y laseres, por soldados, tanques y munición pesada en la más cruenta guerra que jamás ha conocido el ser humano. El resultado [Spring: 1944](https://spring1944.net/).


![Spring1944 2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Spring1944/Spring1944_2.webp)


Si estais un poco al tanto os habreis dado cuenta que los juegos que estamos mencionando existen gracias al versátil **motor libre** [SpringRTS](https://springrts.com/), que permite **ingentes cantidades de unidades en pantalla**, un potente apartado **multijugador**, **enormes y detallados mapas de juego**, que permiten **deformación** en la partida, multitud de cámaras 3D, y una c**ompleja inteligencia artificial**, entre otras muchas cosas... Pues aprovechando todas estas bondades de este motor, y juntándolo con el talento de los desarrolladores, este [**proyecto Open Source**](https://github.com/spring1944/spring1944) nació por el 2006, este juego que permite **batallas de hasta 8 facciones** (Gran Bretaña, Unión Soviética, Estados Unidos, Alemania, Japón, Italia, Hungría y Suecia) por **tierra, mar y aire**, en multitud de escenarios diferentes, y con hasta **600 unidades diferentes** entre las que escoger.


Entre los **principales cambios que se han producido en el juego ultimamente**, José Luis nos ha explicado que novedades nos podemos encontrar:



-**Nueva mecánica para armas pesadas:** Antes estas, que no formaban parte de vehículos, se construían como un camión desarmado, que tenía que ser desplegado. Ahora los cañones ya no responden a la misma mecánica. En su lugar, estos **se despliegan como unidades con un cierto número de soldados manejándolos**. Por tanto ahora pueden disparar de forma autónoma, sin necesidad de desplegarlos. Es posible "asustar" a la infantería, dejando el cañón temporalmente fuera de servicio. Si toda la tripulación es eliminada, el cañón es susceptible de ser capturado. Los cañones admiten ser desplegados, lo que tiene ventajas y desventajas. Desplegados no pueden ser capturados, sólo destruidos, además algunos quedan camuflados. La desventaja es que se vuelven inmóviles, lo que permite rodearlos, haciéndo de ellos un blanco fácil.



![ArmasPesadas](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Spring1944/ArmasPesadas.webp)


-**Nuevas unidades:** A parte de todos los cañones para todas las facciones, y sus tripulaciones, **se ha añadido un camión antitanque a los húngaros, el "44M Buzogányvető"**. Se trata de un arma antitanque autotransportada, que sólo puede disparar hacia atrás, y se despliega como el lanzacohetes camuflado. No se puede detectar hasta que se está muy cerca.


![Antitanque](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Spring1944/Antitanque.webp)
 
**-Nueva GUI:** Se ha creado una nueva interfaz que **reemplaza a la que traía el juego por defecto**; y a otra alternativa, pero que era muy inestable. Además de ser **más elegante**, **se puede personalizar**, cambiando posición y tamaño de los elementos en pantalla (Incluso cambia automáticamente de disposición vertical a horizontal). Todas las ventanas tienen las mismas carácterísticas que tenían las anteriores, pero se las ha supervitaminado. Por poner un ejemplo, a la ventana de comando se le ha añadido la posibilidad de seleccionar el comando por defecto, que antes era invariablemente el de mover, y ahora es el de luchar si el usuario no lo modifica. Se acabó ver a los nuevos mandando asaltos suicidas constantemente. Tambień podemos advertir que **el chat tiene autocompletado y filtra los mensajes innecesarios**. Además **permite silenciar jugadores y espectadores** (uno por uno).
 
![GUI](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Spring1944/GUI.webp)
 
**-Nueva AI:** Hasta ahora la inteligencia artificial dejaba bastante que desear. Había que decirle de antemano que tenía que hacer, dependiendo de la facción y el mapa. Si no se había configurado nada para el mapa, la AI sólo enviaba unidades a tu base sin parar... Ahora la inteligencia artificial analiza el mapa, y se va adaptando según van cambiando las partes controladas por cada equipo. Se la ha preparado para manejar muchos más tipos de unidades, por ejemplo transportes y unidades que requieren ser desplegadas. También **se ha automatizado la planificación de la base y unidades a construir mediante una Genetic Artificial Neural Network, que se ha entrenado hasta un nível aceptable**. De ésta forma la AI no sólo es muchísimo más flexible, no requiriendo información manual sobre como funciona el juego, sino que **es bastante impredecible** (ya que hay hasta 20 individuos entrenados). En el nivel fácil cualquier jugador puede ganar a la AI, en nivel medio ya tienes que tener cierta habilidad, y en los niveles difícil e imposible puede poner contra las cuerdas a los jugadores más experimentados. Es importante aclarar que por ahora la AI aún no sabe jugar con unidades navales ni aéreas.

 

![AI](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Spring1944/AI.webp)
Para poder disfrutar de este juego debes instalar el [SpringLobby](https://springlobby.springrts.com/), que puedes encontrar en la mayor parte de los repositorios y las tiendas de software de las principales distribuciones. Por ejemplo si quieres instalarlo en **Ubuntu y derivadas**, basta que abras una terminal y teclees:



```
sudo apt-get install springlobby
```

Si usas una distribución **basada de Arch**, como Manjaro o EndeavourOS tendríais que poner lo siguiente:



```
sudo pacman -Sy springlobby
```

Una vez realizado este paso tan solo tendríais que crearos una cuenta , entrar en la lista de Batallas y seleccionar en alguna que vaya precedida por "S:1944". Si necesitas descargar cualquier archivo del juego para empezar a jugar lo hará automáticamente. Como veis no es nada difícil. Teneis una información más detallada sobre este proceso en la [Wiki del juego](https://github.com/spring1944/spring1944/wiki). También os dejamos un pequeño video de como aprender a jugar a Spring: 1944 en dos minutos (?!?!?!):


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/DIhqYgVCVoA" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


Si quieres una guía un poco más detallada échale un ojo a [este otro video](https://youtu.be/6t1GCk4JOoo). ¿Conocíais este juego antes de leer este artículo? ¿Qué os parecen sus últimas novedades? ¿Jugais a algún otro juego con el motor Spring? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

