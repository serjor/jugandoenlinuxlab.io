---
author: Pato
category: Rol
date: 2017-04-21 18:47:51
excerpt: "<p>El juego se actualiza con nuevo motor HD. Tambi\xE9n anuncian la llegada\
  \ del pr\xF3ximo DLC 'Shattered Embrace' para el \xFAltimo cuarto de este a\xF1\
  o 2017</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/390c9d1de2a80a844d0e01ba21c1192e.webp
joomla_id: 297
joomla_url: two-worlds-ii-call-of-the-tenebrae-anuncia-su-lanzamiento-en-linux-el-proximo-dia-25-de-mayo
layout: post
tags:
- accion
- fantasia
- rol
- tercera-persona
- mundo-abierto
title: "'Two Worlds II: Call of the Tenebrae' anuncia su lanzamiento en Linux el pr\xF3\
  ximo d\xEDa 25 de Mayo"
---
El juego se actualiza con nuevo motor HD. También anuncian la llegada del próximo DLC 'Shattered Embrace' para el último cuarto de este año 2017

De nuevo Topware sigue trayendo sus desarrollos a Linux, y eso es muy buena señal, pues parece que le es rentable. Gracias a [gamingonlinux.com](https://www.gamingonlinux.com/articles/two-worlds-ii-to-released-on-linux-in-may-along-with-engine-upgrade-call-of-the-tenebrae-dlc.9545) nos ha llegado esta buena noticia, y es que 'Two Worlds II' es un buen juego de Rol de mundo libre en tercera persona, y de eso en Linux no vamos muy sobrados.


Según el [anuncio oficial](http://steamcommunity.com/games/7520/announcements/detail/1292940021078761291) de su [página de Steam](http://store.steampowered.com/app/7520), 'Call of the Tenebrae' vendrá a Linux el próximo día 25 de Mayo junto con una actualización del motor de 'Two Worlds II' para hacerlo compatible con alta definición y otras mejoras. Para que vayais abriendo boca os dejo el vídeo promocional:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/ehrFJxonQ3Q" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Por otra parte, también han anunciado la llegada de un próximo DLC llamado 'Shattered Embrace' que estará disponible durante el último cuarto de este año 2017, y además un pase de temporada que da acceso a 'Call of the Tenebrae', el DLC 'Shattered Embrace', dos packs de mapas multijugador que aún están por llegar, contenido digital de lujo como bonus items, Art book, El "Two Worlds Compendium", la banda sonora y más. La nueva aventura saldrá por 9,99 dólares (no se sabe en € aún) como DLC, o también lo podrás comprar como juego aparte por 14.99 dólares.


 


Sinopsis de Two Worlds II:



> 
> Un viaje apasionante al pasado de Antaloor.  
> Tras la caída de Aziraal, Dios del Fuego, el Señor Oscuro Gandohar prácticamente ha logrado su objetivo de perturbar el equilibrio de los elementos. La magia oscura se derrama sobre la tierra para llenar el vacío. Siendo consciente de su potencial para controlar totalmente los poderes oscuros, Gandohar comienza a utilizar el poder de la trampa de Aziraal con Kyra, descendiente de los Huérfanos. A pesar de su linaje, Kyra no puede soportar la tensión. Con Antaloor bajo sus garras, Gandohar idea un nuevo plan desde su fortaleza de Oswaroth. Pero aún hay quien se opone al tirano, con la esperanza de inclinar el equilibrio de fuerzas a su favor. La lucha por Antaloor continúa...
> 
> 
> 


Características:



> 
> Un sistema de combate flexible con movimientos de ataque variables, bloqueos y trucos especiales  
> Tecnología de última generación gracias al potente motor GRACE™ y a las herramientas especiales  
> Gran cantidad de minijuegos para tomarse un respiro de la mecánica de juego  
> Una interfaz intuitiva para el sistema de Magia DEMONS™ y el sistema de Alquimia PAPAC™  
> Mercenarios y seres creados mediante la magia que podrán acompañar al jugador  
> Complejo sistema de organización de objetos con elementos movibles y utilizables  
> La herramienta especial CRAFT™ permite la configuración individual de la armadura y las armas  
> Diversos oponentes Jefe, incluyendo estrategias especiales de combate y recompensas únicas  
> El flexible sistema MoSens garantiza movimientos habituales y de combate realistas  
> Diversas formas para moverse como andar, correr, esprintar, cabalgar, nadar, navegar y teletransportarse  
> Fascinante historia de fondo con numerosas escenas cinemáticas  
> Amplias opciones multijugador incluyendo simulación de la configuración
> 
> 
>  
> 
> 
> 


 Aún no se saben los requisitos recomendados para Linux, pero estaremos atentos a las noticias que nos lleguen para informar en cuando se sepan.


¿Qué te parece 'Two Worlds II'? ¿Piensas jugarlo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

