---
author: Pato
category: Software
date: 2019-01-17 11:07:49
excerpt: "<p>El cliente beta sigue mejorando en m\xFAltiples frentes</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5fbbaa5b3f48d64954a6b8e2ae2287a1.webp
joomla_id: 951
joomla_url: nueva-actualizacion-del-cliente-de-steam-beta-con-mejoras-para-linux
layout: post
tags:
- steam
- beta
title: "Nueva actualizaci\xF3n del cliente de Steam Beta con mejoras para Linux"
---
El cliente beta sigue mejorando en múltiples frentes

Estamos viviendo tiempos revueltos respecto a los clientes o tiendas para videojuegos, y Valve no es ajena a que deben mover ficha, es por esto que están acelerando desarrollos y ayer mismo fue lanzada una nueva versión de Steam en la rama beta. Esto en sí no es nada raro, se producen lanzamientos del cliente beta de Steam con cierta asiduidad, sin embargo la de ayer ha llamado nuestra atención debido a que parece enfocada a mejoras en el cliente para Linux.


Citando el anuncio de Steam, incorpora mejoras en múltiples apartados como el soporte para conexión de servidores por ipv6, tamaños de ventana, chat, se ha añadido una opción en el overlay para forzar la salida, se ha añadido soporte para nuevos controles mediante Steaminput, etc.


En cuanto a Linux, las mejoras son sustanciales:


    Se corrigieron las descargas de 0 bytes que se ponen en cola en el inicio para todos los títulos de Steam Play  
    Se corrigió la rueda de ratón incorrecta en desplazamientos  
    Se solucionó un problema por el cual algunos juegos no se detectaban correctamente como si se estuvieran ejecutando a pesar de que algunos de sus procesos aún permanecían en segundo plano  
    Se fija La bandeja del sistema, incluido el menú, a veces incluye demasiadas entradas de juegos recientes  
    Se corrigió el cuadro de diálogo de configuración de forma incorrecta siempre solicitando un reinicio del cliente  
    Se agregaron gnutls 3 al Steam Runtime, que solucionan problemas de conectividad de red en muchos títulos de Steam Play  
    Se solucionó un problema con el host libssl en distribuciones más recientes que rompían algunos títulos  
    Se corrigió un error que podía resultar en la imposibilidad de crear accesos directos para ciertos juegos. Para desbloquear títulos previamente afectados, elimine todos los archivos \* .ico y \* .zip de ~ / .steam / root / steam / games y "Verificar archivos de juegos" para volver a descargar los íconos con el formato correcto  
    Se corrigió un error con las compras en el juego en Big Picture


Puedes ver toda la información en el [anuncio de la beta](https://steamcommunity.com/groups/SteamClientBeta#announcements/detail/1703951108822305086).


Como veis, Valve sigue mejorando el cliente para Linux. ¿Qué te parecen estas mejoras? ¿Crees que Valve debería acelerar su desarrollo para nuestro sistema favorito?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram.](https://telegram.me/jugandoenlinux)

