---
author: Serjor
category: "Exploraci\xF3n"
date: 2018-01-16 19:34:56
excerpt: "<p>Rel\xE1jate en un mundo colorido por explorar</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/99b262cbe486d2d63784c37b0d4354d9.webp
joomla_id: 608
joomla_url: innerspace-ya-disponible-en-steam
layout: post
tags:
- aspyr-media
- innerspace
- polyknight-games
title: InnerSpace ya disponible en Steam
---
Relájate en un mundo colorido por explorar

Puntuales a su cita, la gente de Aspyr ha lanzado hoy día 16 de Enero, InnerSpace, un juego desarrollado por PolyKnight Games, además en un lanzamiento con soporte para GNU/Linux desde el primer día.


El juego nos invita a relajarnos mientras exploramos el entorno y descubrimos la historia del mundo que nos rodea.


Podéis encontrarlo en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/347000/" style="border: 0px none; display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


Y si queréis ver un poco de que va, tenéis el unboxing que hemos hecho en el canal de Twitch, no obstante, y gracias a la clave facilitada por Aspyr, también haremos un análisis del mismo.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/FJLAAx-Y6oE" style="border: 0px none; display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Y vosotros, ¿qué pensáis de este InnerSpace? Dejar vuestros comentarios o pasaros por nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

