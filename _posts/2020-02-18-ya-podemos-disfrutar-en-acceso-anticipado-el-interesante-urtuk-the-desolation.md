---
author: Pato
category: Rol
date: 2020-02-18 17:46:43
excerpt: "<p>El juego de&nbsp;@UrtukTheGame nos ofrece rol y estrategia por turnos\
  \ en mundo abierto</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Urtukthedesolation/Urtukthedesolation.webp
joomla_id: 1173
joomla_url: ya-podemos-disfrutar-en-acceso-anticipado-el-interesante-urtuk-the-desolation
layout: post
tags:
- indie
- rol
- estrategia
- turnos
title: 'Ya podemos disfrutar en acceso anticipado el interesante Urtuk: The Desolation'
---
El juego de @UrtukTheGame nos ofrece rol y estrategia por turnos en mundo abierto


Ya tenemos disponible en acceso anticipado el interesante **Urtuk: The Desolation**, un juego de rol y estrategia por turnos en mundo abierto en el que tendremos que adentrarnos en su universo de fantasía oscura, y en el que el apartado artístico es de lo más interesante ya que todos los recursos gráficos están dibujados a mano, desde personajes, monstruos y objetos, hasta el mapa mundial y los campos de batalla.


*Guía a tu grupo de aventureros a través de las ruinas de un mundo antiguo. Recluta nuevos seguidores, saquea los cadáveres de tus enemigos caídos y haz tu mejor esfuerzo para sobrevivir en este reino duro e implacable*.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/vQoDT8sbjRY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Sus características principales son:


* RPG de supervivencia con enfoque en combate y exploración de mundo abierto en un entorno de fantasía oscura
* Combate tácticamente rico por turnos en mapas grandes, con múltiples factores ambientales y un sistema de clase / habilidad cuidadosamente diseñado
* Campaña de supervivencia generada procesalmente
* Extrae mejoras de personaje directamente de tus enemigos caídos


Si quieres saber más, puedes visitar su [página web oficial](http://www.urtuk.com), o si estás interesado, tienes disponible **Urtuk: The Desolation** en acceso anticipado**con un 50% de descuento** en su [página de Steam](https://store.steampowered.com/app/1181830/Urtuk_The_Desolation/), aunque también lo tienes disponible en [Itch.io](https://madsheepstudios.itch.io/urtuk-the-desolation).


 

