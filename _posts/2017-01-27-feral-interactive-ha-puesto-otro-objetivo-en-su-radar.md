---
author: Pato
category: Software
date: 2017-01-27 18:40:04
excerpt: "<p>Desde que publicaron su \xFAltimo port no hab\xEDan comunicado ninguna\
  \ informaci\xF3n acerca de nuevos desarrollos. \xBFSe te dan bien las adivinanzas?</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/778faba3e3dc8c6c6db24b403da494ae.webp
joomla_id: 199
joomla_url: feral-interactive-ha-puesto-otro-objetivo-en-su-radar
layout: post
tags:
- proximamente
- feral-interactive
title: Feral Interactive ha puesto otro "objetivo" en su radar
---
Desde que publicaron su último port no habían comunicado ninguna información acerca de nuevos desarrollos. ¿Se te dan bien las adivinanzas?

Gracias al artículo de [www.gamingonlinux.com](https://www.gamingonlinux.com/articles/feral-interactive-are-teasing-a-new-linux-port-again.9009) nos llega información de que Feral ha puesto un nuevo objetivo en el radar de "próximos lanzamientos" donde suele poner los desarrollos que están por llegar y que aún no pueden desvelarnos qué títulos son. Es una especie de "juego de adivinanza" donde nos avisan que hay un desarrollo en marcha y nos invitan a "adivinar" qué puede ser.


El caso es que desde que publicaron su último port 'Total War:WARHAMMER' del que [ya os hablamos en Jugando en Linux](index.php/item/109-total-war-warhammer-ya-disponible-en-linux-steamos) no habíamos vuelto a oir nada de los chicos de Feral, hasta hoy. Esta es la imagen que nos dejan:


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Feral/Feralfortdukremlin.webp)


Como puedes ver, se trata de una imagen de la secuencia de introducción de la conocida serie de televisión "Los Simpson" donde podemos ver a Maggie, la hija pequeña de Homer pasando por un lector de código de barras en un supermercado. Además se puede leer en la inscripción: "FORT DU KREMLIN-BICÉTRE"


Puedes verlo tú mismo diréctamente en la web de Feral: <http://www.feralinteractive.com/es/upcoming/>


¿Qué nuevo juego nos traerán los chicos de Feral? ¿te atreves a adivinarlo? ¿Alguna idea?


Cuéntanoslo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

