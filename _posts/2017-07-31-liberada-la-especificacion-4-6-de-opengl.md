---
author: Serjor
category: Software
date: 2017-07-31 20:44:54
excerpt: "<p>Esta veterana API seguir\xE1 dando mucho juego</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/15abde3c81505959f6d2610e3cdfbe51.webp
joomla_id: 424
joomla_url: liberada-la-especificacion-4-6-de-opengl
layout: post
tags:
- opengl
- khronos-group
title: "Liberada la especificaci\xF3n 4.6 de OpenGL"
---
Esta veterana API seguirá dando mucho juego

Leemos en [phoronix](https://www.phoronix.com/scan.php?page=article&item=gl46-spirv-parallel-shad&num=1) que Khronos Group ha liberado la especificación de la versión [4.6 de OpenGL](https://www.khronos.org/news/press/khronos-releases-opengl-4.6-with-spir-v-support).


Esta revisión incluye interesantes extensiones y características. Para empezar ahora es obligatorio soportar SPIR-V, el cuál ya era soportado por Vulkan y OpenCL, así que ahora también tendremos estos *"shaders en binario"* disponibles para OpenGL.


También se incluyen nuevas extensiones que acercan un poco más el desarrollo de aplicaciones bajo OpenGL a esa tierra prometida en la que los drivers gráficos no tengan una sobrecarga extra.


Así mismo se han añadido varias extensiones que permiten una mejor interoperatividad con Vulkan y/o Direct3D.


Os dejamos con la lista de las nuevas extensiones:


* GL_ARB_gl_spirv (soporte para SPIR-V)
* GL_ARB_spirv_extensions (soporte para SPIR-V)
* GL_ARB_indirect_parameters (reducción de la sobrecarga en los drivers)
* GL_ARB_shader_draw_parameters (reducción de la sobrecarga en los drivers)
* GL_ARB_pipeline_statistics_query (funcionalidades de D3D)
* GL_ARB_transform_feedback_overflow_query (funcionalidades de D3D)
* GL_ARB_texture_filter_anisotropic ([Filtrado anisotrópico](https://es.wikipedia.org/wiki/Filtrado_anisotr%C3%B3pico))
* GL_ARB_polygon_offset_clamp (supresión de ["light leaks"](https://en.wikipedia.org/wiki/Light_leak))
* GL_ARB_shader_atomic_counter_ops (mejoras de rendimiento y funcionalidad con los shaders)
* GL_ARB_shader_group_vote (mejoras de rendimiento y funcionalidad con los shaders)
* GL_KHR_no_error (mejora del rendimiento a base de no reportar errores)


Y para mejorar la interoperatividad con Vulkan y D3D:


* GL_EXT_memory_object
* GL_EXT_memory_object_fd
* GL_EXT_memory_object_win32
* GL_EXT_semaphore
* GL_EXT_semaphore_fd
* GL_EXT_semaphore_win32
* GL_EXT_win32_keyed_mutex


Y aunque no son obligatorias para declarar compatibilidad con OpenGL 4.6, también se han presentado las siguientes extensiones:


* GL_KHR_parallel_shader_compile (compilación en paralelo de los shaders)
* WGL_ARB_create_context_no_error (para usar junto con GL_KHR_no_error)
* GXL_ARB_create_context_no_error (para usar junto con GL_KHR_no_error)


Por ahora tan solo NVDIA ha presentado un [driver](https://developer.nvidia.com/opengl-driver) con soporte para OGL4.6, en modo beta eso sí, aunque como indican en la propia presentación de la especificación, esta es la primera versión de OpenGL en la que los drivers libres podrán liberar una versión compatible en tiempo récord, y es que algunas de estas nuevas extensiones ya habían ido siendo introducidas.


Y tú, ¿eres más de Vulkan o de OpenGL? Cuéntanos qué te parecen estos añadidos a esta API en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

