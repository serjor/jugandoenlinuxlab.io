---
author: P_Vader
category: Ofertas
date: 2023-02-10 17:35:00
excerpt: "<p>Aprovecha para probar demos de lo que se est\xE1 desarrollando en el\
  \ mundo indie principalmente.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Next_Fest/next_fest_2023.webp
joomla_id: 1525
joomla_url: next-fest-en-steam
layout: post
tags:
- indie
- steam
- demo
- puzzles
- arcade
- survival
- next-fest
title: Next Fest en Steam
---
Aprovecha para probar demos de lo que se está desarrollando en el mundo indie principalmente.


Desde hace tiempo que sigo con gran interés los festivales de Steam como este **[Next Fest](https://store.steampowered.com/sale/nextfest), donde podemos probar hasta el 13 de febrero muchas demos de los juegos que se vienen**. En mi caso particular me interesan los juegos indies o de pequeños estudios y la gran mayoría de lo que se presenta en estos festivales entran en ese conjunto.


**Recordar que estos juegos están en pleno desarrollo** y pueden contener partes incompletas o sufrir de algún fallo. Aquí van tres buenos ejemplos que he descubierto en este Fest:


###  [Blue Fish Yokohama](https://store.steampowered.com/app/1716500/Blue_Fish_Yokohama/)


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/aujRjRD2lLk" title="YouTube video player" width="720"></iframe></div>


La ambientación y sus puzles son muy buenos. Este relajante juego de rompecabezas en el que eres un pez que anda saltando, chocando, rebotando y chapoteando por un restaurante japonés para zamparse la comida de sus clientes es único.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1716500/" style="border: 0px;" width="646"></iframe></div>


 




---


### [99 Fails](https://store.steampowered.com/app/2164500/99_Fails/)


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/5x2fT3Tso_k" title="YouTube video player" width="720"></iframe></div>


No se como describir este juego. Es de lo mas surrealista que he podido probar, tiene mala leche y encima engancha. Lo tienes que probar para alucinar como me ha pasado a mi. Lo peor de todo es que me ha gustado. Saldrá en Linux aunque la demo la puedes probar sin problema con Steam play/Proton.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/2164500/" style="border: 0px;" width="646"></iframe></div>


 




---


### [The Last Plague: Blight](https://store.steampowered.com/app/1564600/The_Last_Plague_Blight/)


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/ltpV02nKqno" title="YouTube video player" width="720"></iframe></div>


Supervivencia total. Cuida de tu bienestar, enfréntate a innumerables amenazas y supera enormes desafíos en un mundo abierto generado aleatoriamente. Una oscura enfermedad conocida como el Tizón ha llegado a la tierra. Apenas has podido escapar de sus devastadores efectos y te encuentras exhausto, hambriento y solo en el desierto.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1564600/" style="border: 0px;" width="646"></iframe></div>


 




---


¿Vas a probar alguna demo más? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix.](https://matrix.to/#/+jugandoenlinux.com:matrix.org)


 


 

