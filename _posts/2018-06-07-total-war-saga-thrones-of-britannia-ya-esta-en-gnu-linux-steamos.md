---
author: leillo1975
category: Estrategia
date: 2018-06-07 10:47:35
excerpt: <p>@feralgames nos trae un nuevo juego de Creative Assembly</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1fb8d2d7c882fbaba1a2c09d1d9cbe68.webp
joomla_id: 757
joomla_url: total-war-saga-thrones-of-britannia-ya-esta-en-gnu-linux-steamos
layout: post
tags:
- feral
- total-war
- creative-assembly
- thrones-of-britannia
title: "\"Total War Saga: Thrones of Britannia\" ya est\xE1 en GNU-Linux/SteamOS"
---
@feralgames nos trae un nuevo juego de Creative Assembly

 Atención fans de los juegos Total War, acaba de desembarcar en GNU-Linux/SteamOS el nuevo título de la serie gracias a los chicos de **Feral**. Tal y como [os anunciamos los dias pasados](index.php/homepage/generos/estrategia/item/875-a-total-war-saga-thrones-of-britannia-llegara-a-linux-steamos-el-proximo-7-de-junio), han sido puntuales a la cita como buenos británicos, y desde ya el juego está disponible para su compra. El anuncio nos llegaba a través de correo electrónico y como es habitual también por las redes sociales. Aquí teneis el **tweet** donde comentan su lanzamiento:



> 
> Explore and conquer Early Medieval Britain in A Total War Saga: THRONES OF BRITANNIA, now available for Linux.  
>   
> Buy from the Feral Store to kick-start the ascent of one of history’s great nations – <https://t.co/PUzhFZpI4q> [pic.twitter.com/rhME37wZBV](https://t.co/rhME37wZBV)
> 
> 
> — Feral Interactive (@feralgames) [June 7, 2018](https://twitter.com/feralgames/status/1004667676881571840?ref_src=twsrc%5Etfw)



 Si recordais nuestro [anterior artículo](index.php/homepage/generos/estrategia/item/875-a-total-war-saga-thrones-of-britannia-llegara-a-linux-steamos-el-proximo-7-de-junio) sobre este juego,  este funcionará con **Vulkan** y sus requisitos técnicos eran los siguientes:


* **MÍNIMO:**  
OS: Ubuntu 18.04  
Procesador: Intel Core i3-2100 o AMD equivalente  
Memoria: 8 GB RAM  
Gráficos: 2GB AMD R9 285 (GCN 3rd Gen o similares), 2GB Nvidia 680 o superior  
HD: 15 GB Espacio disponible  
Notas Adicionaless:  
\* Requiere Vulkan  
\* Tarjetas gráficas AMD requieren Mesa 18.0.0 o superior (Mesa 18.0.4 recomendado)  
\* 1ª y 2ª generación de tarjetas AMD GCN no están soportadas  
\* Tarjetas gráficas Nvidia requieren drivers 390.59 o superior


* **RECOMMENDADO:**  
OS: Ubuntu 18.04  
Procesador: Intel Core i7-3770K  
Memoria: 8 GB RAM  
Gráficos: 4GB AMD RX 480 o 4GB Nvidia GTX 970  
HD: 15 GB Espacio disponible\* Requiere Vulkan  
\* Tarjetas gráficas AMD requieren Mesa 18.0.0 o superior (Mesa 18.0.4 recomendado)  
\* 1ª y 2ª generación de tarjetas AMD GCN no están soportadas  
\* Tarjetas gráficas Nvidia requieren drivers 390.59 o superior


También nos gustaría anunciaros que proximamente publicaremos un **análisis** de este juego de estategia, y por supuesto realizaremos **videos y Streams**, por lo que os recomiendo estar atentos a nuestra web y cuentas en redes sociales. Podeis adquirir el juego en la [tienda de Feral](https://store.feralinteractive.com/es/mac-linux-games/thronesofbritanniatw/) (**recomendado**), en [Humble Bundle (patrocinado)](https://www.humblebundle.com/store/total-war-saga-thrones-of-britannia?partner=jugandoenlinux)  o en [Steam](https://store.steampowered.com/app/712100/Total_War_Saga_Thrones_of_Britannia/). Os dejamos con un video del juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/rXB33OWu7_I" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>



¿Os gustan los juegos de la serie Total War? ¿Qué os parece la localización y momento histórico de Thrones of Britannia? Cuentanoslo en los comentarios, o en los canales de JugandoEnLinux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

