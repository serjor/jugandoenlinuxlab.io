---
author: Serjor
category: Estrategia
date: 2019-03-30 11:15:18
excerpt: "<p>La editora Kalypso Media no falla a los usuarios del ping\xFCino y nos\
  \ trae esta nueva edici\xF3n de Tropico</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2e982f65432588c32a492f05619f3935.webp
joomla_id: 1010
joomla_url: tropico-6-ya-disponible
layout: post
tags:
- kalypso-media
- tropico
- tropico-6
title: Tropico 6 ya disponible
---
La editora Kalypso Media no falla a los usuarios del pingüino y nos trae esta nueva edición de Tropico

Cosas de abrir Steam, que te enteras de que Tropico 6, el famoso simulador de Dictadores, ya está disponible, y lo mejor con soporte para GNU/Linux desde el primer día, y tal y cómo podemos leer en su descripción:



> 
> ¡El Presidente ha vuelto! Demuestra una vez más tu valía como temible dictador u hombre de estado pacifista en la isla de Tropico y erige el futuro de tu nación durante cuatro eras emblemáticas.
> 
> 
> 


Como novedades principales presenta:


* Por primera vez en esta saga, podrás jugar en enormes archipiélagos. Gobierna varias islas a la vez y enfréntate a los nuevos retos.
* Envía a tus agentes a tierras extranjeras para robar las maravillas del mundo y otros monumentos para tu colección.
* Levanta puentes, construye túneles y transporta a tus ciudadanos y turistas en taxis, autobuses y teleféricos. Tropico 6 ofrece medios de transporte e infraestructuras completamente nuevas.
* Personaliza tu palacio y escoge entre todos los extras disponibles.
* Tropico 6 incluye un sistema de investigación revisado que se centra en los aspectos políticos de ser el mayor dictador del mundo.
* ¡Vuelven los discursos electorales! Dirígete a los ciudadanos y haz promesas que no podrás cumplir.
* Tropico 6 presenta multijugador para hasta 4 jugadores.


Está disponible en Steam y en [Humble Bundle](https://www.humblebundle.com/store/tropico-6?partner=jugandoenlinux) (enlace de afiliado).


Os dejamos con su trailer:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/k7HQqGFwQcA" width="560"></iframe></div>


Y tú, ¿Emularás al famoso dictador? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

