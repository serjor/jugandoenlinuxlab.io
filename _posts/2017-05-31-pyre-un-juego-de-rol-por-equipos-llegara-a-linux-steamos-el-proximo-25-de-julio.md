---
author: Pato
category: Rol
date: 2017-05-31 09:30:15
excerpt: "<p>El juego de Supergiant Games llegar\xE1 con soporte para nuestros sistemas\
  \ desde su lanzamiento</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4739b6c64144f72975550c5e8df1b948.webp
joomla_id: 350
joomla_url: pyre-un-juego-de-rol-por-equipos-llegara-a-linux-steamos-el-proximo-25-de-julio
layout: post
tags:
- accion
- indie
- proximamente
- rol
- precompra
title: "'Pyre' un juego de rol por equipos llegar\xE1 a Linux/SteamOS el pr\xF3ximo\
  \ 25 de Julio"
---
El juego de Supergiant Games llegará con soporte para nuestros sistemas desde su lanzamiento

Buenas noticias para los amantes del buen rol. Gracias a [gamingonlinux.com](https://www.gamingonlinux.com/articles/pyre-from-supergiant-games-is-coming-to-linux-should-be-a-day-1-release.9754) nos enteramos que 'Pyre' [[web oficial](http://www.supergiantgames.com/games/pyre/)] del estudio "Supergiant Games" responsables de memorables juegos como '[Bastion](http://store.steampowered.com/app/107100/Bastion/)' o '[Transistor](http://store.steampowered.com/app/237930/Transistor/)' llegará a Linux, y además debería hacerlo en su lanzamiento el próximo día 25 de Julio. 


Según nos cuenta Liam, la versión del juego para Linux corre a cargo del conocido programador **Ethan Lee**, que ya nos ha traído otros maravillosos ports, por lo que la calidad del mismo está prácticamente garantizada. 


*'Pyre' es un RPG por equipos en el que intentarás conducir a la libertad a una banda de exiliados alcanzando la victoria en las ancestrales competiciones que se libran en un vasto y místico purgatorio. ¿Quién regresará entre laureles y quién pasará el resto de sus días condenado al exilio?*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/FvxfmnrNj2U" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


#### **Características:**


*- Un nuevo mundo de los creadores de Bastion y Transistor*  
*¡Adéntrate en el mundo más vasto e imaginativo jamás concebido por Supergiant! Conoce a un reparto coral de personajes que luchan por recuperar su libertad mientras te abres paso por el místico purgatorio de Downside.*


*- Un sistema de batallas entre grupos de tres repleto de acción*  
*Enfréntate a un variopinto elenco de adversarios en tensos enfrentamientos de alto riesgo en los que cada victoria (¡o derrota!) acerca a los exiliados un poco más a la iluminación. Elige a tres miembros de tu grupo para cada ritual y equípalos con arcanos talismanes y poderosos talentos o "Masteries".*


*- Una historia con múltiples sendas que recorrer y sin fin de la partida*  
*En Pyre, nunca perderás tu progreso. No importa si prevaleces o fracasas: el viaje continúa. El compromiso de Supergiant con la narración interactiva se expresa a través de una historia que cada jugador experimentará de una manera única.*


*- Desafía a un amigo en el modo enfrentamiento*  
*Además de la campaña para un jugador, Pyre ofrece un modo enfrentamiento para dos jugadores en el que podrás desafiar a un amigo (o un rival controlado por IA) a uno de los frenéticos combates rituales del juego. Selecciona tu triunvirato entre más de 20 personajes únicos.*


*-Una suntuosa presentación que te llevará a otro mundo*  
*Una vez más, Pyre valida el galardonado talento del equipo responsable Bastion y Transistor. Desde las vívidas ilustraciones pintadas a mano hasta la evocadora banda sonora original, cada aspecto de la presentación de Pyre te sumerge en su mundo de fantasía.*


*- Controles intuitivos y personalizables*  
*Juega con mando o ratón y teclado usando controles totalmente personalizables adaptados para PC. El juego ofrece un amplio margen para adaptar el nivel de desafío tanto en la campaña como en el modo enfrentamiento.*


Los requisitos mínimos para mover el juego según han publicado en Steam son:


SO: glibc 2.17+, 32/64-bit  
Procesador: Dual Core 3.0ghz  
Memoria: 4 GB de RAM  
Gráficos: Gráficas con soporte OpenGL 4.2+   
Almacenamiento: 7 GB de espacio disponible.


Según se puede leer en su página de "[preguntas frecuentes](http://www.supergiantgames.com/blog/pyre-faq/)" el juego no llegará traducido ya que todos los recursos se centran en conseguir la experiencia de juego que buscan y las traducciones han quedado en un plano secundario. 


Aún así, si quieres saber más o precomprar el juego con un 10% sobre el precio final, lo tienes en su página de Steam, donde estará disponible oficialmente a partir del día 25 de Julio:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/462770/" width="646"></iframe></div>


¿Que te parece este 'Pyre'? ¿Te gustan los juegos de rol de Supergiant Studios? ¿Te gustaría hechar unas partidas multijugador con nosotros?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

