---
author: P_Vader
category: "Acci\xF3n"
date: 2022-01-28 12:56:12
excerpt: "<p>Tras 5 meses desde el inicio de la anterior temporada se han a\xF1adido\
  \ muchas novedades y mejoras.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/splitgate/splitgate_season_1.webp
joomla_id: 1416
joomla_url: splitgate-estrena-la-temporada-beta-1
layout: post
tags:
- multijugador
- gratis
- beta
- fps
- portal
- splitgate
- temporada
- uno
title: Splitgate estrena la Temporada beta 1
---
Tras 5 meses desde el inicio de la anterior temporada se han añadido muchas novedades y mejoras.


Hace 5 meses se lanzó la [Temporada Beta 0]({{ "/posts/splitgate-lanza-la-temporada-0" | absolute_url }}), la primera oficial desde el exitoso lanzamiento de este FPS multijugador gratuito, mezcla de Portal y Halo. Desde entonces ha habido varias pequeñas actualizaciones pero la gran actualización ha llegado con el anuncio de esta nueva Temporada beta 1.


Las **novedades** que se han incluido son:


* Editor de mapas V1 (en fase Beta):
	+ El modo de creación de mapas permite a los jugadores crear sus propios mapas personalizados en un nuevo nivel a modo de plantilla en blanco llamado "Tierra plana".
	+ Colocar una gran variedad de geometrías en el mapa, como rampas, muros, suelos, bloques, accesorios, etc.
	+ Colocar una variedad de superficies de portal como cubos, paredes, techos y suelos.
	+ Colocar lugares de aparición para armas.
	+ Mueve los generadores de objetivos para los modos de juego basados en objetivos como Captura de bandera, Oddball, Dominación y Rey de la Colina.
	+ Guarda, renombra y juega tus mapas en partidas personalizadas.
* Nuevos modos:

	+ Una bandera: Nuevo modo de captura la bandera con una sola bandera. Los equipos se turnan para defender y atacar la bandera cada dos rondas
	+ Evolución: Variación de Showdown. Basado en rondas con una carga de armas establecida, el primero en llegar a 6 puntos gana. La carga mejora en la siguiente ronda si ese equipo pierde.
* Se han añadido carreras de dificultad fácil y media en el modo carrera, con un total de 20 nuevas carreras.


![Editor de mapas](https://assets-global.website-files.com/5c72f0026e5e68e120ecd951/61f33f873e4e75c3c8fdc4f0_Splitgate%20%20Arena%20Warfare%20Screenshot%202022.01.26%20-%2021.02.49.81.png)


**Mapas**


* Rediseño de Foregone Destruction con nuevo arte:
	+ Revisión visual completa de Foregone Destruction.
	+ Se ha ajustado el nivel.
	+ Se han añadido armas, un jetpack y una pistola de portales llamada "Estranged" para complementar a Foregone.
* Nuevo mapa de simulación: Hotel
	+ Nuevo mapa tipo SIM con énfasis en la verticalidad.


![Mapa hotel](https://assets-global.website-files.com/5c72f0026e5e68e120ecd951/61f341503e4e7518de0018d6_6.jpg)
**Temporada**


* Pase de batalla de la Temporada beta 1:

	+ 100 niveles con recompensas gratuitas y premium.
	+ Nuevos personajes, jetpacks, skins, wraps y objetos.
* Nuevas recompensas por recomendar la Temporada beta 1.
* Nuevos retos añadidos.
* Recompensas para los jugadores clasificados en la Temporada Beta 0.


![Recompesas de temporada](https://assets-global.website-files.com/5c72f0026e5e68e120ecd951/61f34258d2d4b602f6bf1ada_Splitgate%20%20Arena%20Warfare%20Screenshot%202022.01.27%20-%2017.08.23.33.png)


**Interfaz gráfica:**


* Se han reorganizado las listas de reproducción rápida para dar cabida a más modos de juego.
* Se ha cambiado el menú principal y los lobbies previos y posteriores a la partida para que muestren Foregone Destruction.
* Se ha eliminado el personaje del jugador y la palanca de la sala de caída.


**Juego:**


* Ajustes de la jugabilidad en la mayoría de los mapas.
* Actualizado todos los muros del portal a una nueva textura/efecto.


**Otros cambios:**


* Splitgate se ha asociado con Prime Gaming para ofrecer a los jugadores skins exclusivos de armas, portales, armaduras y mucho más. Disponible para los [miembros de Prime](https://gaming.amazon.com/loot/splitgate).
* Mejoras en el rendimiento del audio.
* Varias correcciones de errores.


‍<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/ZcxWeiOYHss" title="YouTube video player" width="780"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/677620/?t=Splitgate%20es%20un%20juego%20de%20disparos%20multijugador%20gratuito%2C%20con%20tem%C3%A1tica%20de%20ciencia%20ficci%C3%B3n%2C%20basado%20en%20la%20creaci%C3%B3n%20de%20portales.%20Esta%20mec%C3%A1nica%20supone%20una%20evoluci%C3%B3n%20del%20g%C3%A9nero%20de%20los%20FPS%20y%20te%20ofrecer%C3%A1%20vertiginosos%20combates%20multidimensionales." style="border: 0px none;" width="646"></iframe></div>


¿Qué te parece las novedades para esta nueva temporada? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).
