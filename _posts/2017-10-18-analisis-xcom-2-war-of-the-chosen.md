---
author: leillo1975
category: "An\xE1lisis"
date: 2017-10-18 15:30:36
excerpt: "<p>Repasamos todas las particularidades de esta magn\xEDfica expansi\xF3\
  n.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/cee635145ad7a438ae066c6b737ab964.webp
joomla_id: 468
joomla_url: analisis-xcom-2-war-of-the-chosen
layout: post
tags:
- feral-interactive
- dlc
- xcom-2
- war-of-the-chosen
title: "An\xE1lisis: XCOM 2 - War Of The Chosen."
---
Repasamos todas las particularidades de esta magnífica expansión.

Hace ya un buen puñado de años llegó a mis manos un magnífico juego llamado **UFO Enemy Unknown**. El juego ya llevaba un lustro en el mercado cuando finalmente aterrizó en mi magnífico Pentium III.  La mezcla de estratégia por turnos, gestión y acción me cautivó al momento y rapidamente se convirtió en uno de mis juegos favoritos de todos los tiempos. Hace ya unos años, cuando todo esto de los juegos en Linux volvió a estar en la palestra gracias a  Steam, uno de los primeros juegos que entraron en mi biblioteca, sin ser de Valve, fué **XCOM: Enemy Unknown** ([Feral Store](https://store.feralinteractive.com/en/mac-linux-games/xcomenemyunknowncomplete/), [Steam](http://store.steampowered.com/sub/37429/)) . Venía de la mano de una desconocida compañía (para mi) que hasta ahora se dedicaba a portar juegos de Windows a Mac y ahora se atrevía con Linux. Esa compañía era [Feral Interactive](http://www.feralinteractive.com/es/), y gracias a ellos podemos disfrutar de grandes títulos en nuestro sistema. Antes de que nos olvidemos, nos gustaría agradecerles la copia facilitada para la elaboración de este análisis.


 


Si continuamos poniendoos en antecedentes, muchos de vosotros sabreis que Feral convirtió esta joya de [Firaxis](http://www.firaxis.com/?/games/single/xcom-2) (**XCOM 2** - [Feral Store](https://store.feralinteractive.com/es/mac-linux-games/xcom2/), [Steam](http://store.steampowered.com/app/268500/XCOM_2/)) ya hace mas de un año y medio, y si no recuerdo mal de forma simultanea a Windows. El juego tenia la difícil misión de al menos igualar a su antecesor y lo consigue con creces, dotando al título de más profundidad aun si cabe. En esta ocasión la guerra ha terminado mal para nosotros, pues los "Ancianos" finalmente salieron victoriosos. Los alienígenas se han "aliado" con los humanos y nuestro papel pasa de ser un ejército organizado a convertirnos en grupúsculos de insurgentes diseminados y desorganizados por el planeta. A partir de aquí nuestra labor consiste en **unificar la resistencia** y plantarle cara a Advent mediante una guerra de guerrillas.


 


![Asesina](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisXCOM2WotC/Asesina.webp)


*La Asesina es la Elegida que nos pondrá en aprietos en primer lugar*


 


Pero vamos al lío, pues toca empezar a hablar de esta expansión. **War of the Chosen**, al igual que en su día hizo **Enemy Within** con XCOM, no añade historia a la trama del juego base, si no que la modifica  o superpone, dotando de más profundidad al juego, con montones de nuevas características (facciones, enemigos, armas, mejoras...), que hacen que la adquisición de esta, a pesar de su precio (unos 40€), sea más que justificada teniendo en cuenta la cantidad de novedades presentadas... Y sin más vamos a empezar a detallarlas.


 


La primera con la que nos vamos a encontrar al poco de empezar una nueva campaña es la inclusión de nuevas e interesantes facciones de la resistencia. Por un lado tendríamos a **los Segadores,** francotiradores sigilosos que se dedican a cazar a los aliens como si de un venado se tratase; **los Guerrilleros**, que son Aliens que se han liberado del yugo de Advent extrayendose sus implantes cerebrales y con gran facilidad en el combate; y por último **los Templarios**, que son unidades humanas con un poder Psíquico enorme y hábiles en el cuerpo a cuerpo. Todas estas unidades evolucionan de forma diferente a las convencionales, siendo las capacidades añadidas con los niveles no excluyentes como en las unidades normales (en un mismo nivel podríamos tener varias). Establecer contacto con estas facciones nos va a permitir, además de poder disponer de ellas, suculentas bonificaciones, **operaciones de inflitración encubiertas**, misiones, etc.


 


![Segadora](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisXCOM2WotC/Segadora.webp)


*Los segadores serán la primera facción de la resistencia con la que nos encontraremos*


 


Si hablamos de las unidades convencionales, vamos a encontrar más novedades a parte de las **nuevas estéticas** con las que podremos personalizarlos. La más importante entre todas ellas es la posibilidad de **establecer vinculos de amistad entre personajes**, los cuales se van a hacer más fuertes a medida que estos personajes realicen misiones juntos. Esa afinidad nos va a permitir que un compañero pueda darle una acción adicional al otro durante el combate. Esta ventaja solo se podrá realizar una vez por batalla, pero será mejorada a medida que la amistad sea mayor. Dichos vinculos también podrán ser con miembros de las otras facciones de la resistencia. También **podremos crear carteles de propaganda de la resistencia** que luego veremos por las paredes de los escenarios de los combates haciendo fotos a nuestros soldados.


 


En cuanto a los enemigos tenemos montones de novedades. Existen nuevos tipos de soldados, como **los Purificadores**, que son soldados Advent equipados con un lanzallamas, capaces de atacar a varias unidades al mismo tiempo, y que pueden explotar cuando mueren, algo con lo que tenemos que tener cuidado si tenemos unidades cerca. También encontraremos **Sacerdotes**, unidades con gran poder psionico, directamente ligados a los ancianos, con lo que ello supone. Los **Espectros** podrán crear réplicas de nuestras unidades para complicarnos aun más las cosas.


 


![Guerrillero](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisXCOM2WotC/Guerrillero.webp)


*No todas las facciones de la resistencia son humanas. Los guerrilleros son alienígenas que lucharán a nuestro lado*


 


Además de estas unidades de Advent, aparecerá en escena una nueva "facción" enemiga, **los Perdidos**, que son humanos afectados por la radiación y se han convertido en una especie de Zombies. Pueden atacar tanto a nuestros soldados como  a los de Advent y lo hacen en hordas, por lo que si dejamos que se nos acerquen demasiado pueden ser altamente peligrosos. Además las explosiones producidas por vehiculos en llamas o granadas pueden despertarlos de su letargo, por lo que usar estas últimas no es buena idea para acabar con ellos, ya que solo atraeríamos más. Afortunadamente podemos acabar con ellos con cierta facilidad, ya que son débiles y **podemos encadenar ataques consecutivos disparándoles a la cabeza**, por lo que conviene tener las armas siempre cargadas cuando estos se presentan.


 


Pero si hay algo en lo que destaca esta expansión, y que además le da título a esta, es la **inclusión de los Elegidos** ("the Chosen"), que son 3 alienígenas extremadamente poderosos. Nos enfrentaremos a la **Asesina**, cuya habilidad es mantenerse en las sombras y atacar sin ser vista; el **Cazador**, una especie de francotirador capaz de atacar a nuestras tropas a distancia y equipado también con un gancho; y por último el **Brujo**, cuyos habilidades son ataques los psionicos y la capacidad de invocar espíritus para atacarnos.  Su misión en la tierra es encontrarnos a nosotros y el Avenger (nuestra nave), y para ello no dudarán en atacarnos, secuestrar e interrogar a nuestras tropas. Estos estarán localizados en diversas partes del mapa y tendrán su area de influencia. Por decirlo de alguna manera **son  la contraposición de las facciones rebeldes**. Estos aparecerán cuando menos lo esperemos en las batallas y también influirán en las acciones estratégicas que realizemos en el mapa de misiones. Hasta que podamos eliminarlos tendremos que derrotarlos en más de una ocasión.


 


![Combate](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisXCOM2WotC/Combate.webp)


 


Nuestra base dispondrá de nuevas dependencias como **"El anillo"** o la enfermería. En el primero coordinaremos las operaciones encubiertas relaccionadas con las otras facciones de la resistencia, operaciones en las que tendremos que asignar tanto a soldados, como cientificos, ingenieros o suministros. Completar estas operaciones nos otorgará mejores relaciones con la facción para la que sea el "trabajo", además de importantes ventajas, mejoras, etc. En **la enfermería** podremos curar a nuestros soldados mucho más rápido, e incluso quitar los aspectos negativos de sus estados de ánimo. Además de estas novedades, en la dependencia del Laboratorio, nuestros científicos podrán realizar **investigaciones "inspiradas"** que se nos presentarán en determinados momentos y que gracias a ellas podremos investigar mucho más rápido determinadas tecnologías.


 


En el **apartado técnico** el juego no ofrece nada reseñable con respecto al juego base, siendo los gráficos y el sonido iguales (incluyendo un doblaje al español perfecto). El rendimiento del juego es igual al título que expande, por lo que hablamos de un juego bastante exigente con unas **tasas de frames no muy altas** y que en ciertos momentos (cinemáticas y cargas sobretodo) bajan bastante. Aún así, al tratarse de un juego de estratégia por turnos no vamos a sufrir este inconveniente en ningún momento ni vamos a jugar peor por ello, aunque eso si, no deja de ser molesto. En cuanto a esta inconveniencia, al no poder probar el juego en Windows, no puedo asegurar que sea un defecto del juego en si o un problema de port, aunque apuesto más por esto último, y si es así **no estaría de más que Feral le echase un ojo a la optimización**.


 


![OperacionesEncubiertas AnilloResistencia](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisXCOM2WotC/OperacionesEncubiertas-AnilloResistencia.webp)


*El anillo será una de las nuevas instalaciones, y desde el podremos coordinar las operaciones encubiertas*


 


 En cuanto al **apartado artístico** la expansión sigue lógicamente la misma tónica que el juego base, con las diferencias obvias que ofrecen las novedades de esta DLC. Nos vamos a encontrar el mismo tipo de menús, mapa o diseño de personajes, que en el título original. Las músicas no son una excepción y siguen con la misma tónica. El juego ofrece además novedades en los escenarios de las misiones tácticas, pudiendo encontrar ciudades abandonadas o túneles repletos de perdidos.


 


Si hablamos de la **jugabilidad**, es necesario decir que como es lógico mama del juego original, y si este último ya era complejo por la abrumadora cantidad de opciones y eventos que pueblan el juego, ahora con las novedades de la expansión aun se complica un poco más. Hay que recalcar que **la saga XCOM no es fácil**, por lo que no será ninguna novedad que perdamos batallas, soldados y partidas enteras. Aun así el juego cuenta con 4 niveles de  dificultad (desde recluta hasta leyenda) y bastantes opciones avanzadas de personalización de la partida, entre las que destaca la "**Estrategia Prolongada"**, que hace que el avance del proyecto Avatar sea más lento y tengamos más tiempo nosotros para planificar nuestra estrategia. Disponemos también de **modo Multijugador**, tanto a través de LAN como de internet; un **apartado de MOD's** donde podremos bajar contenidos de Workshop o crear nuevas campañas, elementos del juego, etc. Podemos observar también en el menú principal un **modo Reto** donde encontraremos desafios a la comunidad y donde puntuaremos en un marcador global.


 


![MapaGestion Puente](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisXCOM2WotC/MapaGestion-Puente.webp)


*El mapa del juego está estructurado exactamente igual que el del juego base*


 


Gracias a esta DLC, se ha conseguido otorgar al juego de **mayor profundidad** y realmente se ha conseguido mejorar aun más si cabe a este título. La integración de War of The Chosen tanto en la historia como en el resto de factores del juego se engarza perfectamente sin que notemos ninguna incongruencia. Debemos encomiar también el trabajo de Feral Interactive por ofrecernos casi sin retraso este magnífico añadido. Hay algo que puedo decir con rotundidad, **después de jugar a War of the Chosen, no se nos va a pasar por la cabeza jugar a la campaña original**. Sin duda alguna una compra obligada para todos los que disfrutasteis de XCOM 2 y una magnífica oportunidad de "rejugar" (atención a las comillas) este ya clásico de la estratégia por turnos.


 


Si quereis ver de que va el juego de una forma un poco más gráfica, aquí os dejamos un gameplay que hemos realizado exclusivamente para este análisis:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/Nyal0X8F6Yw" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Si os ha convencido lo que habeis leido en este análisis y quereis comprar XCOM2:  War of the Chosen podeis hacerlo en [**la tienda de Feral (recomendado)**](https://store.feralinteractive.com/es/mac-linux-games/xcom2warofthechosen/) o en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/593380/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Qué os ha parecido el análisis? ¿Habeis jugado ya a XCOM 2? ¿Qué opinión os merece esta expansión? ¿Teneis pensado adquirirla? En JugandoEnLinux apreciamos mucho tus opiniones por lo que si quieres puedes usar los comentarios  o nuestro canal de [Telegram](https://t.me/jugandoenlinux) para dejar tus mensajes.

