---
author: Pato
category: Hardware
date: 2018-05-25 18:42:17
excerpt: "<p>La nueva consola de Atari ya se puede comprar en pre-reserva en su campa\xF1\
  a en Indiegogo!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c5c0a28a3994ca1491828123987c79bf.webp
joomla_id: 748
joomla_url: atari-vcs-abrira-su-campana-de-pre-reservas-el-proximo-miercoles-actualizado
layout: post
tags:
- ataribox
- atari
title: "Atari VCS abrir\xE1 su campa\xF1a de pre-reservas el pr\xF3ximo Mi\xE9rcoles\
  \ (actualizado)"
---
La nueva consola de Atari ya se puede comprar en pre-reserva en su campaña en Indiegogo!

**(actualización 30/05/2018):**


¡Ya está abierta la campaña de financiación de la consola Atari VCS! y por fin tenemos algunos datos más sobre lo que llevará la nueva consola de la mítica compañía...


Lo primero de todo, tal y como ya sabíamos si estás suscrito a las notificaciones de Atari al respecto seguro que habrás recibido un correo con un enlace para poder pre-comprar la **consola Atari VCS Onix** (la del frontal negro) con un 33% de descuento sobre su precio de campaña, **quedándose en 199$ (unos 170€)** más gastos de envío (unos 25€). Esta oferta está disponible para todo el mundo pero es limitada y se acabará si se agotan las 25.000 consolas previstas para esta oferta.


La oferta con el paquete de consola + mando clásico se queda en 229$ también con la limitación de 25.000 consolas.


Si no llegas a tiempo de las ofertas, tendrás la opción de conseguir la consola Atari VCS Onix a 249$, o con mando clásico por 279$.


Por otra parte, las ofertas de la Atari VCS Collector's Edition (la del frontal de madera) **comienzan en 299$** junto al mando clásico.


Pero vamos a lo interesante. ¿Qué características y opciones tiene la Atari VCS?


Está claro que Atari no se anda con tonterías, y ha puesto mucho empeño en hacer atractiva a su consola. A destacar:


+ Juega a juegos clásicos y modernos, con **Atari Vault** por bandera con una colección de más de 100 juegos clásicos pre-cargados en la consola.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/zRdFw7MODc4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


* **Stream de vídeo** mediante Twitch u otros servicios
* **Portea o crea nuevo contenido** y nuevas apps dentro del sandbox Linux integrado
* **Control por voz** para funciones de navegación, online, o acceso a accesorios
* Personaliza tus **listas de juegos** para contenido de todo tipo, y comparte en tus redes sociales favoritas.


En cuanto a estudios y partners, Atari tiene ya confirmados una docena de estudios, y afirma tener más para anunciar próximamente. Además afirma estar trabajando con un **socio tecnológico** que desvelará en breve con el que está uniendo esfuerzos para proveer excitantes y diversos juegos y otro contenido de entretenimiento para Atari VCS. ¿Alguien dijo Valve? ¿O será otra compañía? Se admiten apuestas.


Pasemos al sistema. Tal y como se venía anunciando será un sistema **Linux personalizado sobre la base de un Ubuntu con kernel 4.10**, con **CPU AMD Bristol Ridge A1 y gráfica Radeon R7** (la APU de bajo consumo), 4GB de RAM DDR4 y 32 GB de memoria interna. Todos los detalles a continuación:




|  |  |
| --- | --- |
| **Specification\*** | **Measurement** |
| Unit Dimensions | 14.5" x 5.3" x 1.6" |
| Unit Weight | 3 lbs. |
| Materials | Plastic, Metal, Wood |
| Operating System | Linux OS based on Ubuntu (Linux Kernel 4.10) |
| Compatible Systems | Linux |
| Power | Low TDP architecture - Less Heat & Noise |
| Connections | HDMI 2.0, 2.4/5GHz Wi-Fi, Bluetooth 5.0, Gigabit Ethernet, 4xUSB 3.0 |
| External Inputs | Classic Joystick, Modern Controller, Microphone |
| Storage | 32GB eMMC, external HD, SD card  |
| RAM | 4GB DDR4 RAM |
| Memory | 32GB eMMC |
| CPU | Bristol Ridge A1 |
| GPU | Radeon R7 |
| HDCP Integration | HDCP 2.2 |
| Second Screen (Screencasting) | Yes |
| Cloud Storage | Yes. Additional Service Offering |
| Required Internet Connection | Not for classic gaming but required to access all features  |
| Cross Game Chat | Skype, Discord, etc. |
| Voice Commands | 4-front facing mic array |
| Subscription Needed? | No. Includes cloud and other services. |
| Live Streaming | Yes with Twitch.tv |
| Mouse & Keyboard Support | Yes |


Para no alargarnos más, **Atari tiene planeada una línea de periféricos** para la consola aunque ya avisan que es **compatible con los periféricos de PC** como teclado y ratón, gamepads, cámaras etc, y si todo va como está previsto en su rodamap las primeras unidades se servirán para la primavera del próximo año 2019.


En el momento de escribir estas líneas la campaña ya ha recaudado más de 770.000$. Si piensas pillarla, ¿a qué estás esperando?


Tienes todos los detalles de la campaña [en este enlace](https://www.indiegogo.com/projects/atari-vcs-game-stream-connect-like-never-before-computers-pc/x/18828265#/).


<div class="resp-iframe"><iframe height="445px" src="https://www.indiegogo.com/project/atari-vcs-game-stream-connect-like-never-before-computers-pc/embedded/18828265" width="222px"></iframe></div>


 


Cuéntame qué te parece la nueva consola "Linux" de Atari en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).




---


**(Artículo original):**
Llevamos tiempo esperando noticias de Atari respecto a su campaña de pre-reservas de su próxima consola Atari VCS. Hoy por fin hemos recibido confirmación de que el próximo Miércoles 30 de Mayo abrirán dicha campaña en la plataforma Indiegogo donde se podrá acceder a la pre-compra de la consola con un descuento. Además, se espera que desvelen por fin los detalles del aparato y posiblemente los juegos que traerá de salida, aunque que ya sabemos algunos detalles, a saber:


* Será una consola dedicada a títulos tanto de la propia Atari como de estudios externos, aunque su potencia descarta juegos AAA (en principio).
* Podrá hacer Streaming de servicios multimedia comoHBO, Hulu o Netflix.
* Tendrá un joystick de estilo retro para los juegos de la propia Atari, y un gamepad para juegos más actuales.
* Tendrá dos versiones, una con apariencia más retro con frontal de madera, y una versión mas actual con frontal de plástico, aunque se rumorea que tendrán distintos precios.
* Tendrá un procesador customizado, y aunque en un principio se afirmó que sería de AMD posteriormente surgieron rumores de que estaría al cargo de Intel.
* Tendrá un precio estimado de entre 250 a 300 dólares, aunque habrá que ver en qué se traduce eso en euros.
* Y lo más importante, tendrá como sistema operativo un sistema con Linux personalizado por Atari y abierto, y se rumorea la posibilidad de poder ejecutar Steam en la máquina.


Si quieres saber más detalles, puedes repasar todos los artículos que hemos publicado en Jugando en Linux sobre Atari VCS [en este enlace](index.php/buscar?searchword=ataribox&ordering=newest&searchphrase=all&limit=20), y también puedes visitar [la página de Atari VCS en Indiegogo](https://www.indiegogo.com/projects/atari-vcs-game-stream-connect-like-never-before/coming_soon), aunque aún no hay mucha información.


Os mantendremos informados de lo que se mueva el Miércoles al respecto.

