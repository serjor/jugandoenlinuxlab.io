---
author: Pato
category: Software
date: 2022-12-14 13:53:49
excerpt: "<p>Se trata de un bug que lleva con nosotros varios a\xF1os y que solo afecta\
  \ a los host bajo linux</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/steamlink/steamlink.webp
joomla_id: 1514
joomla_url: valve-soluciona-el-bug-de-sonido-de-remote-play-en-el-ultimo-cliente-beta-de-steam
layout: post
tags:
- steamos
- steam
- steam-link
- steam-play
title: "Valve soluciona el bug de sonido de Remote Play en el \xFAltimo cliente beta\
  \ de Steam"
---
Se trata de un bug que lleva con nosotros varios años y que solo afecta a los host bajo linux


Normalmente no solemos escribir sobre lanzamientos beta, pero esta vez merece una excepción, ya que se trata de un problema que llevamos arrastrando desde hace unos años los que jugamos en Linux.


En concreto afecta a los jugadores bajo Linux y que además utilizan la característica Remote Play de Steam para jugar en remoto desde su PC hacia cualquier dispositivo con la aplicación Steam Link mediante red local o internet.


Si quieres saber de qué va esto de Steam Link puedes verlo en su página web [en este enlace](https://store.steampowered.com/app/353380/Steam_Link/).


Entrando en harina, el problema que teníamos si queríamos utilizar Remote Play desde un PC con Linux es que al poco de comenzar una sesión de juego el sonido comenzaba a presentar distorsiones y ruido, y terminaba por cortar el sonido poco a poco hasta quedar en silencio. La única solución era salir y reiniciar la sesión con lo que la sesión de juego quedaba interrumpida, cosa que en otros sistemas no ocurría. Ahora por fin, Valve ha solucionado el problema en la última actualización del cliente beta de Steam, con las siguientes correcciones:


General


• Nuevo modo Big Picture  
 • Añadido soporte para cambiar el monitor primario en Windows. Ver Configuración -> Pantalla -> Pantalla preferida.  
 • Corregida la escala de superposición al cambiar el tamaño de la ventana del juego


Steam Input:  
 • Añadido soporte para el gamepad Armor-X Pro en modo PS4  
 • Corregido un retraso prolongado al inicio cuando se enchufa el teclado Razer Huntsman Elite  
 • Corregido el controlador Logitech G29 que aparecía como un gamepad en lugar de un volante


Juego remoto  
 • **Corregido el crepitar y la pérdida de audio eventual al transmitir desde Linux**


Linux  
 • Corregido el bloqueo al tomar capturas de pantalla a través de la superposición (overlay)


Puedes acceder a la beta del cliente de Steam entrando en el menú "*Steam - Parámetros - Cuenta*" dentro de la sección "*Participación en Beta*" y clicando en el botón "*Cambiar*".


Tienes toda la información en la página de información de las actualizaciones del cliente de Steam [en este enlace](https://steamcommunity.com/groups/SteamClientBeta/announcements/detail/3627116953542043544).

