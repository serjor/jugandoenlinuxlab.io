---
author: Pato
category: Noticia
date: 2017-10-02 16:18:32
excerpt: "<p>\xA1Se nos viene otra novedad!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7d2898c3630feea92ec1553d16389ff6.webp
joomla_id: 471
joomla_url: el-radar-de-feral-interactive-vuelve-a-detectar-un-nuevo-ofni
layout: post
tags:
- proximamente
- feral-interactive
title: El radar de Feral Interactive vuelve a detectar un nuevo OFNI
---
¡Se nos viene otra novedad!

¡Buenas noticias desde Feral Interactive! hace tan solo un par de horas que la compañía a la que tanto le debemos ha anunciado que el radar ha detectado nueva actividad:



> 
> A dusty screen flickers to life; eerie bleeps fill the air.  
> Mac and Linux signals have popped up on The Feral Radar!<https://t.co/v2d998T2GE> [pic.twitter.com/YQBdiJaoFX](https://t.co/YQBdiJaoFX)
> 
> 
> — Feral Interactive (@feralgames) [October 2, 2017](https://twitter.com/feralgames/status/914856830337257472?ref_src=twsrc%5Etfw)



 Como podéis ver, se trata de un extraño dibujo y la leyenda: "O vain illusion of glory and grandeur"


![feralteaser02102017](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Feral/feralteaser02102017.webp)


¿Qué juego puede ser? ¿Alguna idea? Estamos deseando saber vuestras teorías, ya sea en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

