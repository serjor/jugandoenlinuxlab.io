---
author: Pato
category: "Acci\xF3n"
date: 2017-11-10 15:47:59
excerpt: "<p>M\xE1quinas de lucha futuristas reparti\xE9ndose le\xF1a</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ad50363b9b3c0e7cd5049a5778e00aff.webp
joomla_id: 529
joomla_url: lucha-de-robots-en-clash-of-robots-ya-disponible-en-linux-steamos
layout: post
tags:
- accion
- indie
- arcade
- lucha
title: Lucha de robots en 'Clash of Robots' ya disponible en Linux/SteamOS
---
Máquinas de lucha futuristas repartiéndose leña

Si ayer nos hacíamos eco del lanzamiento de 'Slashers', hoy nos llega otro juego de lucha con la particularidad de que esta vez serán robots los que se den mamporros.


'Clash of Robots' es un juego que nos llega desde Android, y que ahora prueba suerte en compatibles. Se trata de un juego de lucha donde tendrás que enfrentarte a diferentes oponentes, cada uno con sus características y estilo de lucha únicos. 


*"Combina tu inteligencia humana con la fuerza, la resistencia y las tácticas de un robot en este juego futurista robótico. Participar con el enemigo, detectar su debilidad, formular una estrategia y sumergirse en esta lucha y dar su 100% para brillantes recompensas. Únete a las épicas batallas PvP contra rivales de todo el mundo!"*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/hP1h-zp9gQ8" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Tendremos a nuestra disposición diversos modos de juego, como modo torneo, liga o lucha 1vs1 al mejor de 3 combates. 


Si te van los juegos de lucha y quieres echarle un vistazo, lo tienes disponible traducido al español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/651560/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


¿Que te parece este 'Clash of Robots'? ¿Te gustan los juegos de lucha?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

