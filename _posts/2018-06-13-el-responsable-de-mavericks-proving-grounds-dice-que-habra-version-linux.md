---
author: Pato
category: "Acci\xF3n"
date: 2018-06-13 20:39:54
excerpt: "<p>As\xED lo afirm\xF3 hace unos meses el CEO del estudio que est\xE1 desarrollando\
  \ @PlayMavericks</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d0781aab6107ab09663a5b35f9583041.webp
joomla_id: 769
joomla_url: el-responsable-de-mavericks-proving-grounds-dice-que-habra-version-linux
layout: post
tags:
- accion
- proximamente
- battle-royale
- teaser
title: "El responsable de 'Mavericks Proving Grounds' dice que habr\xE1 versi\xF3\
  n Linux"
---
Así lo afirmó hace unos meses el CEO del estudio que está desarrollando @PlayMavericks

Si al igual que nosotros estuviste viendo el PC Gaming Show la noche del pasado martes te darías cuenta de un llamativo trailer sobre un juego de acción en mundo abierto con toques de Battle Royale tan de moda en estos días llamado '**Mavericks**'. Pues bien, nos enteramos por [Gamingonlinux](https://www.gamingonlinux.com/articles/mavericks-is-a-battle-royale-game-with-unique-open-world-mechanics-that-plans-linux-support-with-vulkan.11959) que el juego **tendrá versión para Linux**. Según el artículo de Liam el propio CEO de "**Automation**", el estudio detrás de Mavericks confirmó hace algunos meses en [Reddit](https://www.reddit.com/r/playmavericks/comments/7znpxf/will_there_be_a_linux_version/durl7in/) que el juego tendría versión para Linux.



> 
> *Si, habrá una versión linux de Mavericks (de hecho también utilizamos Linux para todo lo relacionado con el trabajo y servidores, incluyendo físicas y la lógica del juego)!. Desafortunadamente, es poco probable que sea en el lanzamiento, pero la mayoría del trabajo ya está hecho para un soporte nativo en Linux (no hay necesidad de portearlo). El retraso es por que primero debemos asegurarnos que el **renderizado Vulkan** está listo para producción. Está llegando, pero aún no tenemos una fecha concreta. Haremos un anuncio tan pronto como la tengamos :)*
> 
> 
> *James*
> 
> 
> 


¡Grandes noticias para los que buscan más acción! ¡y encima viene "Vulkanizado"!


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/xjYKk0XRYWI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Mavericks será un juego de acción en mundo abierto y persistente y con un modo de juego "Battle Royale" y una narrativa dinámica donde tendremos que defender nuestra facción y tomar decisiones que afectarán a nuestro mundo y la forma en la que jugamos.


Si quieres más información, puedes visitar la [web oficial del juego](https://mavericks.gg/).


¿Qué te parece 'Mavericks'? ¿Piensas que es "otro" Battle Royale? ¿Te gustaría jugarlo en Linux?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

