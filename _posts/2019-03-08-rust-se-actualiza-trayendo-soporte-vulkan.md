---
author: leillo1975
category: "Acci\xF3n"
date: 2019-03-08 14:36:31
excerpt: <p class="ProfileHeaderCard-screenname u-inlineBlock u-dir" dir="ltr"><span
  class="username u-dir" dir="ltr">@FcpnchStds lo incluye en "The Oil Rig Update"</span></p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/884076e32a85fde4d5ab4fe1dc1af0d4.webp
joomla_id: 991
joomla_url: rust-se-actualiza-trayendo-soporte-vulkan
layout: post
tags:
- vulkan
- supervivencia
- actualizacion
- rust
- facepunch
title: '"RUST" se actualiza trayendo soporte Vulkan.'
---
@FcpnchStds lo incluye en "The Oil Rig Update"

Aun recordamos con tristeza cuando a principios del verano pasado la compañía británica [Facepunch](https://facepunch.com/), desarrolladora del también conocido [Garry's Mod](https://gmod.facepunch.com/), quitaba el icono de Linux/SteamOS en Steam de su juego más conocido, [Rust](https://rust.facepunch.com/). El **motor Unity** con el que desarrollan este título les estaba generando problemas que no le permitían dar un soporte oficial adecuado. Por lo que sabemos aún así iban actualizando el juego al mismo tiempo que la versión de Windows. Pero todo cambió más o menos hace un mes cuando lanzaron "[The Air Power update](https://rust.facepunch.com/blog/february-update)", donde **los cambios en Unity les permitieron corregir dichos problemas y mejorar el soporte**.


Ayer presentaban una **nueva actualización**, "[The Oil Rig Update](https://rust.facepunch.com/blog/oilrig-update)", donde entre otras muchas cosas, **incluyen el soporte para aprovechar la potencia de Vulkan en su software**. Como os imaginareis, **hasta ahora el juego usaba OpenGL**, lo cual no le permitía incluir muchas de las optimizaciones que estaban siendo usadas en Windows y podrían romper regularmente los shaders. Para hacer más sencillo su trabajo y poder ofrecer una calidad adecuada a sus jugadores Linuxeros han decidido cambiar a Vulkan (para los usuarios de MacOS usarán Metal). Por esta razón recomiendan que si sois jugadores de Rust actualicéis al máximo vuestros drivers gráficos y si lo ejecutáis desde Windows y queréis probar a ejecutarlo con Vulkan uséis el siguiente **parámetro de lanzamiento** en las propiedades del juego en Steam:


`-force-vulkan`


Por supuesto, a parte del uso de Vulkan, la actualización incluye [montones de novedades](https://rust.facepunch.com/changes/1) entre las que **destacan las siguientes**:


  
-Como indica el nombre, aparece una nueva localización en el mapa, la **plataforma petrolífera**, que para conseguir conquistarla será necesario pertenecer a un equipo.


-El **nuevo proceso de creación de "builds"** ahora es el doble de rápido, permitiendo un desarrollo más cómodo del juego


-El juego ahora **carga los activos un 10% más rápido** en SSD's


-La **excavadora gigante** es otro nuevo punto del mapa, la encotraremos en medio del desierto.


-**Actualización del Oceano**, donde las olas ahora mueven los objetos en el agua e interactúan con el jugador.


-**Se han vuelto a incluir los reflejos** para mejorar la iluminación del mapa. Estos habían sido desactivados en anteriores versiones por que originaban problemas


 


Si aun no teneis este fantástico juego de supervivencia, [podeis comprar Rust en la Humble Store (patrocinado)](https://www.humblebundle.com/store/rust?partner=jugandoenlinux). Aquí teneis un video con el trailer del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/HNavTaDpXXU" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Jugais a Rust? ¿Lo habeis probado ya con Vulkan? ¿Habeis notado mejoría? Responde en los comentarios o charla con nosotros sobre ello en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

