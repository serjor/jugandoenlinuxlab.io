---
author: leillo1975
category: Carreras
date: 2020-02-28 11:56:35
excerpt: "<p><span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\"\
  >@OrontesG anuncia la fecha de lanzamiento del juego en Acceso Anticipado<br /></span></p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DRAG/DRAG_ART.webp
joomla_id: 1180
joomla_url: drag-ya-tiene-pagina-en-steam-y-entra-en-acceso-anticipado-a4
layout: post
tags:
- steam
- acceso-anticipado
- drag
- thorsten-folkers
- orontes-games
title: "DRAG ya tiene p\xE1gina en Steam y entrar\xE1 en Acceso Anticipado (ACTUALIZADO\
  \ 4)"
---
@OrontesG anuncia la fecha de lanzamiento del juego en Acceso Anticipado  



**ACTUALIZADO 2-7-20**: Después de pasarlo en grande intentando bajar tiempos con la demo de DRAG hace tan solo unos días, Orontes Games anuncia que su juego finalmente se hará público a las 19:00 Horas CEST del día 11 de Agosto en Acceso Anticipado, por lo que quien quiera comprar el juego y disfrutarlo, podrá hacerlo en poco más de un mes. De la fecha oficial de la versión definitiva aun no se sabe nada, pero seguro que más de uno está frotándose las manos para echarle el guante a este título, aunque sea en "Early Access". 


Por lo demás aun no tenemos ninguna información nueva relevante, pero como venimos haciendo desde un principio estaremos al tanto de todas las novedades.




---


**ACTUALIZADO 16-6-20**: Con motivo del [Steam Game Festival](https://store.steampowered.com/sale/gamefestival) y una semana de retraso, aunque no por culpa de [Orontes Games](https://twitter.com/orontesg?lang=es), si no de Valve, **está por fin disponible la demo prometida** donde podemos disfrutar por primera vez de los buenos mimbres que presenta este juego. Nosotros ya lo hemos podido probar y hemos de decir que el juego muestra unos gráficos muy buenos, unas físicas cuidadas y una buena jugabilidad, aunque para ser justos una **dificultad acusada**. Podeis descargar su demo en su página en [Steam](https://store.steampowered.com/app/773840/DRAG/).


También hay que comentar que [han decidido regalar el juego a los 10 mejores tiempos](https://store.steampowered.com/newshub/app/773840/view/2235421525039801615) que se hayan registrado en esta demo, lo que os garantizo que es harto difícil. Si quereis probar debeis saber que **teneis hasta el 22 de junio**. Os dejamos con el video que acabamos de grabar probando el juego. Esperamos vuestras opiniones sobre DRAG!


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/76n6n97_SQg" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>




---


**ACTUALIZADO 21-5-20**: Los desarrolladores de este prometedor juego de carreras acaban de [confirmar](https://polycount.com/discussion/comment/2726954#Comment_2726954), que el juego ha sido confirmado para el próximo Steam Game Festival - Edición de Verano, y que gracias a ello **podremos disfrutar de su demo entre los días 9 y 14 de Junio**, por lo que nos instan a seguir (y si queremos poner en nuestra lista de deseos) su [página en Steam](https://store.steampowered.com/app/773840/DRAG/) para ser avisados. Aquí teneis el tweet:  
  




> 
> DRAG has been confirmed for the upcoming[#SteamGameFestival](https://twitter.com/hashtag/SteamGameFestival?src=hash&ref_src=twsrc%5Etfw) Summer Edition!  
>   
> Wishlist and Follow DRAG on [@Steam](https://twitter.com/Steam?ref_src=twsrc%5Etfw)<https://t.co/hskosmuxKL>  
> to play ? the Demo between June 9-14[#indiegame](https://twitter.com/hashtag/indiegame?src=hash&ref_src=twsrc%5Etfw) [#indiedev](https://twitter.com/hashtag/indiedev?src=hash&ref_src=twsrc%5Etfw) [#gamedev](https://twitter.com/hashtag/gamedev?src=hash&ref_src=twsrc%5Etfw) [#indiewatch](https://twitter.com/hashtag/indiewatch?src=hash&ref_src=twsrc%5Etfw) [#games](https://twitter.com/hashtag/games?src=hash&ref_src=twsrc%5Etfw) [#racinggame](https://twitter.com/hashtag/racinggame?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/wlGc9sEOD7](https://t.co/wlGc9sEOD7)
> 
> 
> — Orontes Games (@OrontesG) [May 21, 2020](https://twitter.com/OrontesG/status/1263280356737855488?ref_src=twsrc%5Etfw)



 También aclaran que **las imágenes del primer trailer del juego son 100% reales** y se corresponde con el "**gameplay**" que encontraremos. Las diapositivas, los saltos y las derivaciones no son scripts ni ayudas de conducción, sino puramente físicas. Confirman que el juego será más exigente que un simple arcade y nos emplazan un nuevo trailer proximamente. Seguimos informando...




---


**ACTUALIZADO 6-4-20**: Gracias a lo leido en sus [salas de Discord](https://discordapp.com/invite/hUDxpnN), tenemos bastante información nueva acerca del lanzamiento del juego (tranquilos, todavía no tenemos fecha):


***Título**: DRAG*  
***Género**: Carreras, Simulación, Deportes, Acceso Anticipado*


***Fecha de lanzamiento**: PC (Windows / Linux nativo) - A lo largo de 2020*  
***Precio**: $ 34.99 USD con un 10% de descuento para la primera semana*  
*(Los precios pueden variar según su región y moneda)*


***Modos de juego**:*  
*- **24 desafíos para un jugador** con tablas de clasificación (clasificaciones globales)*  
*- **Calificación multijugador** en línea y carreras de rueda a rueda*  
*- **Multijugador local** en pantalla dividida*  
*- **Contrarreloj** (carrera libre)*


***Caracteristicas**:*  
*- **vehículos totalmente físicos** con resistencia aerodinámica y deslizamiento*  
*- **modelo de daños** que permite carreras competitivas*  
*- **dos vehículos desbloqueables** con diferentes configuraciones de chasis*  
*- **Cámaras en tercera persona** (cerca y lejos)*  
*- **Cámaras en primera persona**, *Vista en cabina* y cámara capó*  
*- **19 logros de Steam***  
*- **Música de sintetizadores** al estilo 80's*


***Arcade o simulación?***


*Los vehículos son simulados completamente físicos y no tienen control de estabilidad ni líneas de carrera magnéticas de ningún tipo.*


***¿Pero necesito un volante para las carreras?***


*No, está diseñado para jugarse con un gamepad o incluso un teclado.*  
*(Se agregará soporte para los volantes en Early Access lo antes posible)*


Como veis, información de lo más interesante que no hace más que aumentar más y más el hype que ya tenemos, que nos es poco. Seguimos informando....




---


**ACTUALIZADO 2-3-20**: Acabamos de recibir una notificación en nuestro correo informándonos de [nuevas respuestas en el post de Polycount](https://polycount.com/discussion/comment/2717600#Comment_2717600), y entre ellas hay **información referente a ciertos aspectos relaccionados con la jugabilidad**. En primer lugar aclara que el juego está **pensado para ser jugado con gamepad o teclado**, pero que durante el periodo de acceso anticipado **intentarán soportar también volantes**.


También es interesante saber que **los jugadores tendrán que usar el rebufo de los demás coches para adelantar**, por lo que esto tendrá que ser usado con cierta estrategia para evitar que vaya en nuestra contra. En cuanto a la lucha con el resto de competidores se han puesto sobre la mesa puntos muy interesantes, como que **será posible echar al resto de jugadores fuera de la pista, lo cual hará que no puedan volver en ciertos escenarios**, pero debemos hacer esto con cierta cautela si no queremos dañar nuestro coche, que podría perder las ruedas. Sobre este ultimo punto, **siempre que tengamos 3 ruedas será posible continuar.**


Habrá un elemento que determinará el desarrollo de las carreras, y es la **vuelta comodín**, algo que vimos con anterioridad en DIRT Rally y DIRT 4 en las carreras de Rallycross. Tendremos la **obligación de pasar una vez por una zona especial del trazado**, por lo que si en la ultima vuelta no lo hemos echo se nos cerrará el camino y tendremos que pasar obligatoriamente por ahí. También lo podremos usar en nuestro favor, por ejemplo si tenemos un competidor aprovechando nuestro rebufo, para poder escapar de él, aunque tendremos que cuidar la incorporación al trazado para no meternos en problemas con el resto.


Como veis, estás informaciones auguran que **será un arcade donde podremos jugar sucio para ganar**, y donde tendremos que cuidarnos de nuestros competidores..... y es que cada vez que nos ensañan algo del juego, más ganas nos entran de jugar. Seguimos informando.


 




---


**NOTICIA ORIGINAL (28-2-20):**


 Como sabeis, en más de una ocasión os hemos hablado de este juego ([1](index.php/component/k2/2-carreras/735-pronto-veremos-drag-un-juego-de-carreras-en-acceso-anticipado), [2](index.php/component/k2/2-carreras/931-los-desarrolladores-de-drag-nos-muestran-nuevos-avances), [3](index.php/component/k2/2-carreras/1058-el-desarrollo-de-drag-continua-con-importantes-novedades), [4](index.php/component/k2/2-carreras/1236-tras-bastante-tiempo-sin-noticias-drag-continua-su-desarrollo)), que **lleva en desarrollo varios años**, y es que no es para menos, pues el aspecto que ha mostrado desde el principio es soberbio y más teniendo en cuenta que tan solo lo desarrollan dos personas desde Alemania, los hermanos "[Thorsten Folkers](https://tfolkers.artstation.com/)" (**Orontes Games**). **Este proyecto, que está construido usando Linux con su propio motor escrito en C++, y usando Python para scripting**, acaba de anunciar en su cuenta de [Polycount](https://polycount.com/discussion/183339/ong-offroad-racer-indie-game-dev-log/p4) que el juego finalmente ya tiene su propia [página en Steam,](https://store.steampowered.com/app/773840/DRAG/) donde **estará disponible en Acceso Aniticipado a lo largo del presente año**. Por lo tanto ya podeis si quereis, añadir el juego a deseados y seguir las noticias que allí comuniquen.


También han publicado un **trailer realizado con el propio motor** donde podemos ver imágenes exclusivas del juego en acción, que no hacen más que aumentar aun más el Hype sobre este juego. Podeis verlo justo aqui debajo:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/_cZfa7lE67g" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 Además de esto también han facilitado [información](https://polycount.com/discussion/183339/ong-offroad-racer-indie-game-dev-log/p4) sobre lo que han estado haciendo desde su última comunicación , encontrándonos con novedades importantes, entre las que destacan las siguientes:


-**Soporte para códecs de video vp9:** han tenido que pasar de usar en el motor el códec de vídeo theora a vp9, debido al nuevo material de vídeo de 4k/60 fps. Se ha usado ffmpeg para codificar a vp9 los videos dentro del juego y los fondos de los menús.


-**Finalizando la escritura del juego:** se está completando el scripting del juego con "tweening y fades" por todas partes y pruebas completas.


-**Todas las pistas tienen ahora follaje y grava en el suelo**


![grava](/https://us.v-cdn.net/5021068/uploads/editor/y8/phpz56vrwhl7.webp)


-**Añadidos detalles a la estructura aérea de la zona C** como escaleras o pasamanos para transmitir la escala de las cosas cuando se construyen este tipo de estructuras de aspecto extraño.


-**Terminada la zona C con los dos últimos objetos:** una grúa está inspirada en una grúa existente en el mar, pero se pretendía algo más dramático para este nivel que se ajustara al estilo del arte. Después del bloqueo, se hizo una pintura de high-poly y low-poly,  y texturizada en Substance Painter. Los paneles del piso usan un material de "tiling" para ahorrar algo de espacio de textura y aumentar la resolución para el resto de la grúa.


![grua](/https://us.v-cdn.net/5021068/uploads/editor/ag/fpiv0yiao2r6.webp)


-**Inicio y fin de la línea de meta de la zona C:** que coincide con el estilo de la grúa. Se reutilizaron partes del módulo de luz de las otras pistas para tener cierta consistencia en los mapas.


**-UI del garaje:** se terminó la interfaz de usuario del garaje con las especificaciones técnicas de los diferentes vehículos y los selectores de color para los colores primarios y secundarios personalizables.


**-Artwork / keyart:** esto también se hizo en el motor. Probablemente ya sehaya visto esto como una miniatura de vídeo.


-**Logo del Motor y el estudio + pantalla animada:** Una de las razones por las que se tuvo que cambiar al códec vp9 fueron los videos como este que se quería a 4k 60 fps también:


![logo orontes](/https://us.v-cdn.net/5021068/uploads/editor/o1/vi2afdmadlt7.webp)


Además de esto hemos estado charlando con los desarrolladores sobre el juego en [discord](https://discord.gg/hUDxpnN) y nos han comentado que intentarán implementar de alguna manera una cámara subjetiva, así como soporte para volantes y FFB, aunque todo esto está aun lejos de ser una realidad. Permaneceremos muy atentos a todas las novedades que podamos ofreceros y continuaremos esperando a ver la primera versión jugable de DRAG.


¿Como os habeis quedado con las novedades de este juego? ¿Qué os parece el desarrollo que están haciendo estos hermanos? Podeis darnos veustra opinión sobre este juego en los comentarios de este artículo, en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

