---
author: Pato
category: Realidad Virtual
date: 2018-03-14 09:55:21
excerpt: "<p>La API se auto-configurar\xE1 adapt\xE1ndose a cada GPU y dispositivo\
  \ VR</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/671fe58af5845f33e7786dd608484c8d.webp
joomla_id: 677
joomla_url: steamvr-beta-actualizado-soporte-para-auto-resolucion-en-camino
layout: post
tags:
- realidad-virtual
- steamvr
- beta
title: "'SteamVR Beta' actualizado: soporte para auto-resoluci\xF3n en camino"
---
La API se auto-configurará adaptándose a cada GPU y dispositivo VR

Ayer tarde nos llegó la notificación de que SteamVR, la API desarrollada por Valve para dar soporte a las aplicaciones VR en su rama beta ha recibido una nueva característica para obtener la mejor experiencia visual de las gráficas actuales, tratando de reducir así el coste de desarrollo y haciéndole algo mas fácil a los desarrolladores su trabajo.


Para lograr esto "tunean" la resolución de las aplicaciones para cada GPU y gafas VR para sacar el rendimiento óptimo de cada uno de estos elementos. El runtime de SteamVR mide el rendimiento de la GPU y le comunica a la aplicación la resolución apropiada para renderizar los gráficos de manera óptima y así logran una configuración adecuada para cada aplicación logrando así una mejor experiencia para los usuarios.


[Según el comunicado](http://steamcommunity.com/games/250820/announcements/detail/1661138371528106606), hay muchos equipos VR que están siendo infra-utilizados y no sacan lo mejor de las aplicaciones VR, por lo que con este cambio ahora serán capaces de obtener mejores resoluciones, y lo mejor de todo es que será de forma automática. No habrá que hacer nada.


Por supuesto, el usuario siempre tendrá la última palabra y la opción de "saltarse" la configuración por defecto de SteamVR, y configurar los parámetros a su gusto.


Por otra parte, aseguran que esta nueva configuración también ayudará a "bajar" el coste de la VR, ya que será aplicable a todos los dispositivos actuales y futuros, con independencia de las especificaciones del PC, analizando siempre las capacidades de la GPU y las gafas que se empleen.


Por otra parte, también afirman que de este modo facilitan también el trabajo de los desarrolladores, ya que a partir de ahora no tendrán que testear las aplicaciones en cada una de las configuraciones y gafas del mercado, si no que podrán centrarse en el desarrollo y dejar a SteamVR el trabajo de "adaptar" las configuraciones gráficas para cada equipo.


Si quieres saber más, puedes visitar [el post del anuncio](http://steamcommunity.com/games/250820/announcements/detail/1661139006826676957) en Steam.


¿Que te parece la evolución de SteamVR? ¿Piensas usar las Vive o Vive Pro en tu equipo Linux ahora o en el futuro?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

