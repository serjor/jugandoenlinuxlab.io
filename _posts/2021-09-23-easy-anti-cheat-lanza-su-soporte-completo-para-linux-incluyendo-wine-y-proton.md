---
author: Pato
category: Software
date: 2021-09-23 19:58:39
excerpt: "<p>El d\xEDa que est\xE1bamos esperando ha llegado, aunque puede que no\
  \ sea todo color de rosa</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Easyanticheat/eac-proton.webp
joomla_id: 1365
joomla_url: easy-anti-cheat-lanza-su-soporte-completo-para-linux-incluyendo-wine-y-proton
layout: post
tags:
- steam
- wine
- proton
- easy-anti-cheat
title: Easy Anti Cheat lanza su soporte completo para Linux, incluyendo Wine y Proton
---
El día que estábamos esperando ha llegado, aunque puede que no sea todo color de rosa


Una nueva bomba ha caído en el mundo del videojuego para Linux. hace pocos minutos nos llegaba notificación desde el [blog de desarrolladores de Epic](https://dev.epicgames.com/en-US/news/) que su ampliamente utilizado sistema anti-trampas, **Easy Anti Cheat** (EAC) comienza a ofrecer **soporte completo para Linux**con el objetivo de dar soporte a la Steam Deck, incluyendo también dentro de nuestro soporte los juegos ejecutados con **Proton y Wine**. Tal y como explican:



*"Epic Online Services existe para conectar a desarrolladores y jugadores en todas las plataformas, incluido el próximo **Steam Deck**, y estamos emocionados de dar otro paso en esa dirección.*


*A principios de este año, los juegos Easy Anti-Cheat para Windows se pusieron a disposición de todos los desarrolladores de forma gratuita. Hoy, ampliamos el soporte a Linux y Mac para desarrolladores que mantienen compilaciones nativas completas de sus juegos para estas plataformas.*


*Para facilitar a los desarrolladores la distribución de sus juegos a través de plataformas de PC, se incluye soporte para las capas de compatibilidad **Wine y Proton en Linux**. **A partir de la última versión del SDK**, **los desarrolladores pueden activar el soporte anti-trampas para Linux a través de Wine o Proton con solo unos pocos clics** en el Portal para desarrolladores de Epic Online Services.*


*Easy Anti-Cheat está disponible de forma gratuita a través de Epic Online Services en todas las plataformas de PC, lo que ayuda a todos los desarrolladores a mantener una experiencia de juego justa y segura para los jugadores en el sistema operativo de su elección."*


Puedes ver el anuncio en el post del blog [en este enlace](https://dev.epicgames.com/en-US/news/epic-online-services-launches-anti-cheat-support-for-linux-mac-and-steam-deck?sessionInvalidated=true).


Si bien es cierto que de algún modo esperábamos un movimiento de este tipo, lo cierto es que lo esperábamos quizás para un poco mas adelante y de otra forma. Tal y como explican, deberán ser los propios desarrolladores los que deberán activar el soporte para Wine/Proton en el SDK, y aunque como explican supondrá solo unos pocos clics, **la cuestión ahora es cuántos estudios desarrolladores darán ese paso para ofrecernos su soporte**. (De ahí que pongamos que "no todo es color de rosa")


En cualquier caso, y como ya hemos demostrado en muchas otras ocasiones si por algo nos distinguimos los usuarios del pingüino es de sabernos hacer escuchar. Quizás ahora sea nuestro turno de "**solicitar de forma educada** a los desarrolladores que no se den por aludidos" de que pueden activar su soporte para que nosotros también podamos jugar.


Por el momento sería de esperar que los propios juegos de Epic que emplean su sistema comenzasen a funcionar bajo Wine/Proton, y esperemos ver llegar títulos como: **Apex Legends, For Honor, Fortnite, Gears 5, Squad, Hunt Showdown, Paladins, Ghost Recon**... por citar solo unos pocos.   
  



¿Qué juegos con soporte EAC esperas poder ejecutar próximamente en Wine/Proton?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

