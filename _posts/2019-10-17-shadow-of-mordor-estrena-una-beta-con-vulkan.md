---
author: leillo1975
category: "Acci\xF3n"
date: 2019-10-17 17:22:14
excerpt: "<p>@feralgames actualiza la API gr\xE1fica en su juego a\xF1os despu\xE9\
  s de su salida</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/22b5b3b0334d4500edf03be1a111fda9.webp
joomla_id: 1121
joomla_url: shadow-of-mordor-estrena-una-beta-con-vulkan
layout: post
tags:
- beta
- feral
- warner-bros
- shadow-of-mordor
- middle-earth
- vukan
title: '"Shadow of Mordor" estrena una beta con Vulkan (ACTUALIZADO)'
---
@feralgames actualiza la API gráfica en su juego años después de su salida

Vaya, menudo sorpresón que me he llevado esta mañana cuando he leido una noticia sobre "**Middle Earth: Shadow of Mordor**" tanto tiempo después de su salida en **julio de 2015**. Y es que "nunca es tarde si la dicha es buena", ya que cuando aun estamos con la resaca del anuncio de "[Shadow of the Tomb Raider](index.php/homepage/generos/accion/5-accion/1242-shadow-of-the-tomb-raider-desembarcara-pronto-en-linux-steamos)" en mente, Feral Interactive se saca este conejo de la chistera. La noticia nos llegaba a través de la siempre genial [GamingOnLinux](https://www.gamingonlinux.com/articles/the-linux-port-of-shadow-of-mordor-from-feral-interactive-has-gained-a-vulkan-beta-a-massive-difference.15226), una de las páginas de referencia para todos los amantes de los juegos en Linux.


Si sois poseedores de este juego, para activar la beta con Vulkan, tan solo teneis que ir a las **Propiedades** del juego, y en la pestaña **BETAS**, desplegar el menu para escoger la beta y seleccionar "**linux_vulkan_beta**". Tras realizar estos pasos el juego se actualizará y podreis disfrutar del juego con esta API.


Gracias a esta **beta**, el juego cambia la API gráfica utilizada en el port, **de OpenGL** (la original) **a Vulkan**, permitiendo un rendimiento que incluso supera el doble de frames por segundo, al menos en el equipo utilizado para ello en las pruebas. Dicho rendimiento incluso es **superior al ofrecido por Steam Play/Proton**, lo cual es un punto a favor de Feral. Nosotros por nuestra parte **intentaremos realizar tests para así sacar nuestros propios resultados** y los publicaremos tanto en esta noticia como en nuestros grupos de [Telegram](https://t.me/jugandoenlinux), como en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org). Recordad que vosotros podeis hacer los propio dejando vuestras pruebas en los **comentarios de esta noticia** o en el [tema dedicado a Benchmarks de nuestros foros.](index.php/foro/foro-general/46-benchmarks-de-tus-videojuegos-en-linux?start=20#443)


Ahora nos hacemos un par de preguntas, ¿será esto un preludio de más juegos de Feral actualizados con Vulkan? ¿Tendrá algo que ver la proximidad del lanzamiento de [Google Stadia](index.php/homepage/noticias/39-noticia/1122-stadia-la-nueva-plataforma-de-videojuegos-de-google-ha-sido-presentada-en-la-gdc)? Creo que estas cuestiones dan para debatir mucho, pero son solo especulaciones sin ninguna base más que Stadia corre sobre Linux y usa Vulkan, y por supuesto necesita catálogo.


Como veis estamos ante una **magnífica oportunidad de sacar del baul a este espectacular juego**, uno de los mejores que tenemos en nuestro catálogo, y que tantas horas de diversión nos ha procurado. A los que no lo tengais, desde JugandoEnLinux.com os recomendamos que os hagais con él sin dudarlo, especialmente si os gustan los juegos de acción en tercera persona con mundo abierto, y/o si sois fans de el universo que creó Tolkien. Podeis comprar "**Middle Earth: Shadow of Mordor**" en la **[Humble Store (enlace patrocinado)](https://www.humblebundle.com/store/middleearth-shadow-of-mordor-game-of-the-year-edition?partner=jugandoenlinux)**. Os dejamos con el trailer del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/9YDJN7MkZMk" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>




---


**ACTUALIZACIÓN:** Tal y como os dijimos antes hemos realizado nuestros propios benchamrks con unos resultados que nos han dejado completamente en shock, pues superan con mucho las espectativas que teníamos previamente. El equipo en el que hemos hecho las pruebas es un **Intel i5-3550** (3.3Ghz x4) con **16GB** DDR3-1600 de RAM y una **Nvidia GTX-1060-6GB** con el **driver 430.50**, y todo ello corriendo sobre **Ubuntu 18.04.3**, y con una resolución de **1920x1080 y gráficos en Ultra:** 




|  |  |  |
| --- | --- | --- |
| **FPS** | **OpenGL Original** | **Vulkan Beta** |
| **Media** | 67.33 | 109.85 |
| **Máximo** | 121.29 | 161.08 |
| **Mínimo** | 39.21 | 71.15 |

