---
author: Pato
category: Software
date: 2021-07-01 08:32:20
excerpt: "<p>El lanzador traer\xE1 mejoras a todos los niveles en su pr\xF3xima versi\xF3\
  n 1.8.0</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HeroicGamesLauncher/Heroicgameslaunch180.webp
joomla_id: 1321
joomla_url: heroic-games-launcher-sigue-evolucionando-y-prepara-nuevas-caracteristicas
layout: post
tags:
- lanzador
- epic-game-store
- heroic-games-launcher
title: "Heroic Games Launcher sigue evolucionando y prepara nuevas caracter\xEDsticas"
---
El lanzador traerá mejoras a todos los niveles en su próxima versión 1.8.0


Hace unos días que fue lanzada la versión 1.7.2 del lanzador **Heroic Games Launcher**, que como sabéis es un proyecto para lanzar juegos de otras plataformas con o sin soporte para Linux, aunque de momento se centra en la (infausta) Epic Game Store.


En concreto, en esa versión las mejoras eran:


* Corregido un error con la configuración de Wine que no se actualizaba cuando solo se tenía instalada una versión de Wine.
* Corregido un error cuando no funcionaba mover el juego a una carpeta con espacios.
* Corregido un error por el cual DXVK no se estaba instalando en una carpeta de prefijo con espacios.
* Corregido un error por el cual al cancelar la instalación, Heroic comenzaba la instalación de todos modos en la ruta incorrecta.
* Arreglado el botón LatestLog no funciona.
* Corregido que Winetricks y Winecfg no funcionaran para los prefijos de proton
* Otras pequeñas correcciones en diferentes configuraciones.


Esta era la última versión planeada antes de saltar a la rama 1.8.x pero los desarrolladores acaban de lanzar una segunda versión candidata al lanzamiento (RC2) de esta rama que ya se está convirtiendo en un lanzamiento mayor del proyecto. En concreto la que será la versión 1.8.0 traerá:


* Añadida la sección Juegos Recientes en la bandeja de iconos y un filtro en la boblioteca.
* Añadida compatibilidad con Discord RPC (excepto Linux AppImages).
* El submenú ahora siempre está visible en la página del juego.
* Es posible actualizar un juego desde la página del juego ahora haciendo clic en la información de actualización en lugar de abrir la configuración como antes.
* Aún no es posible descargar Fortnite y Cyberpunk2077, pero ahora se pueden importar.
* Se corrigió un error cuando un juego necesitaba reparación antes de actualizarse y la información se estropeaba
* Añadidos accesos directos en el menú de escritorio y aplicaciones. De forma predeterminada, siempre se creará después de instalar un nuevo juego y eliminarlo al desinstalarlo.
* Añadido botón para crear accesos directos desde la página del juego.
* Mejoras en los controladores Winetricks y Winecfg y deberían funcionar ahora con proton 6.3+.
* Corregida la carpeta de sincronización de guardado al usar Proton.
* Añadida la variable STEAM_COMPAT_CLIENT_INSTALL_PATH al iniciar un juego con proton, ya que se necesita a partir de las versiones 6.3+ de Proton.
* Registro mejorado cuando se ejecuta desde la terminal
* Otras correcciones de errores menores y mejoras de rendimiento.


Como puedes ver el desarrollo apunta maneras y con un futuro en el que pretenden integrar otras tiendas como Itch.io o GOG puede convertirse en un lanzador a tener muy en cuenta.


Tienes todos los detalles en la [página del proyecto en Github](https://github.com/Heroic-Games-Launcher/HeroicGamesLauncher/releases).


 

