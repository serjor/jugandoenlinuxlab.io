---
author: Serjor
category: Software
date: 2019-08-01 18:51:09
excerpt: <p>Un nuevo launcher con el que tener nuestros juegos bien organizados</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/08ae461f14a3d195f7e52b850155dc3c.webp
joomla_id: 1089
joomla_url: pegasus-frontend-un-launcher-para-emuladores-y-demas-juegos-que-tengamos-en-nuestro-ordenador
layout: post
tags:
- launcher
- pegasus-frontend
title: "Pegasus Frontend, un launcher para emuladores y dem\xE1s juegos que tengamos\
  \ en nuestro ordenador"
---
Un nuevo launcher con el que tener nuestros juegos bien organizados

Hoy en nuestro canal de [Telegram](https://telegram.me/jugandoenlinux) nos hacían eco de un nuevo launcher de videojuegos, [Pegasus Frontend](https://pegasus-frontend.org/), una aplicación open source y multiplataforma para organizar y ejecutar prácticamente cualquier juego que tengamos en nuestros discos duros, ya sean juegos de Steam o GOG, pero sobretodo emuladores, que es donde ponen el mayor esfuerzo.


Cómo os comentábamos, es multiplataforma, estando disponible para GNU/Linux, macOS, Windows y varios sistemas embebidos como Android, RetroPie y algunos más.


Se diferencia de otros launchers como Lutris en que por ejemplo Pegasus no se encargará de descargar los juegos por nosotros, pero en juegos de emuladores hará lo que pueda para traer información del juego y mostrarnos su carátula y datos asociados, lo cuál es de agradecer, porque al menos a mí, siempre me gusta contar con una pequeña reseña y alguna captura.


Os dejamos un vídeo de cómo funciona (en Android):


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/d4kvUZjZ_tI" width="560"></iframe></div>


Y tú, ¿eres de usar diferentes lanzadores de juegos? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://t.me/jugandoenlinux) y [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)

