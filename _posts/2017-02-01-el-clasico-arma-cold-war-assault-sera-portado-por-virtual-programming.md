---
author: leillo1975
category: "Simulaci\xF3n"
date: 2017-02-01 10:50:00
excerpt: "<p>Acabo de leer en <span style=\"color: #000080;\"><a href=\"https://www.gamingonlinux.com/articles/virtual-programming-are-porting-arma-cold-war-assault-and-frog-climbers-to-linux.8982\"\
  \ target=\"_blank\" style=\"color: #000080;\">GamingOnLinux</a></span> la siguiente\
  \ gran noticia, y es que la compa\xF1\xEDa dedicada a portar juegos para GNU/Linux\
  \ y Mac, <span style=\"color: #000080;\"><a href=\"https://www.vpltd.com/\" target=\"\
  _blank\" style=\"color: #000080;\">Virtual Programming</a><span style=\"color: #000000;\"\
  >, est\xE1 trabajando en traernos este gran juego a nuestros PCs proximamente, as\xED\
  \ como <span style=\"color: #000080;\"><a href=\"http://store.steampowered.com/app/485120/\"\
  \ target=\"_blank\" style=\"color: #000080;\">Frog Climbers</a></span>. </span></span></p>\r\
  \n<p><img src=\"images/Articulos/ArmaColdwar1.jpg\" alt=\"\" style=\"display: block;\
  \ margin-left: auto; margin-right: auto;\" /></p>\r\n<p>&nbsp;Para quien no lo conozca,\
  \ se trata de un juego desarrollado por Bohemia Interactive hace ya bastantes a\xF1\
  os (2001), y que probablemente muchos conozcais como el primer Operation Flashpoint.\
  \ Hace unos cuantos a\xF1os, Codemasters se hizo con los derechos del juego, y desde\
  \ entonces Bohemia Interactive continu\xF3 con la saga con otro nombre, ARMA. El\
  \ juego marc\xF3 un antes y un despu\xE9s en el sector, ya que combina a partes\
  \ iguales la acci\xF3n y la simulaci\xF3n. Nos encontramos ante un t\xEDtulo en\
  \ el que cada movimiento puede costarte la vida, donde seguir ordenes (al principio),\
  \ y darlas bien (despu\xE9s) es tremendamente importante, donde un simple tiro mal\
  \ dado te puede matar, o en el mejor de los casos herirte y hacer que tengas que\
  \ pasarte el resto de la misi\xF3n arrastrandote por el enorme campo de batalla.\
  \ En \xE9l se puede usar todo tipo de veh\xEDculos (jeeps, tanques, helic\xF3pteros,\
  \ camiones..) y el arsenal de armas es tremendamente extenso.</p>\r\n<p>&nbsp;</p>\r\
  \n<p>Arma: Cold War Assault incluye lo que en su d\xEDa era la campa\xF1a principal\
  \ y su expansi\xF3n \"Resistance\". No se ha facilitado ninguna estimaci\xF3n de\
  \ cuando estar\xE1 disponible, pero yo ya me estoy frotando las manos deseando poder\
  \ rejugarlo de nuevo. Os dejo el trailer oficial del juego, por favor, vedlo desde\
  \ la perspectiva de que es un juego con m\xE1s de 15 a\xF1os:</p>\r\n<p>&nbsp;</p>\r\
  \n<p><iframe src=\"https://www.youtube.com/embed/a75eG8HG5Fk\" width=\"560\" height=\"\
  315\" style=\"display: block; margin-left: auto; margin-right: auto;\" allowfullscreen=\"\
  allowfullscreen\"></iframe></p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"text-decoration:\
  \ underline;\"></span><strong><span style=\"text-decoration: underline;\">ACTUALIZACI\xD3\
  N:</span> Virtual Programing acaba de anunciar en su cuenta de Twitter que el juego\
  \ entra en oficialmente en Beta, aunque aun no ha aclarado si ser\xE1 p\xFAblica,\
  \ privada o solo para desarrolladores.<br /></strong></p>\r\n<blockquote class=\"\
  twitter-tweet\" data-lang=\"es\">\r\n<p dir=\"ltr\" lang=\"en\">Arma: Cold War Assault\
  \ - Current State: beta <br />Coming to Mac &amp; Linux <a href=\"https://twitter.com/hashtag/linuxgaming?src=hash\"\
  >#linuxgaming</a> <a href=\"https://twitter.com/hashtag/macgaming?src=hash\">#macgaming</a>\
  \ <a href=\"https://twitter.com/hashtag/ARMA?src=hash\">#ARMA</a> <a href=\"https://twitter.com/hashtag/Linux?src=hash\"\
  >#Linux</a> <a href=\"https://twitter.com/hashtag/Mac?src=hash\">#Mac</a><a href=\"\
  https://t.co/WmaaKKHKRN\">https://t.co/WmaaKKHKRN</a> <a href=\"https://t.co/yQ7MOe5XwA\"\
  >pic.twitter.com/yQ7MOe5XwA</a></p>\r\n\u2014 Virtual Programming (@virtualprog)\
  \ <a href=\"https://twitter.com/virtualprog/status/826741844369403904\">1 de febrero\
  \ de 2017</a></blockquote>\r\n<script src=\"//platform.twitter.com/widgets.js\"\
  \ async=\"\" type=\"text/javascript\" charset=\"utf-8\"></script>\r\n<p>&nbsp;</p>\r\
  \n<p>FUENTES: <a href=\"https://www.gamingonlinux.com/articles/virtual-programming-are-porting-arma-cold-war-assault-and-frog-climbers-to-linux.8982\"\
  \ target=\"_blank\">GamingOnLinux</a>, <a href=\"https://www.vpltd.com/\" target=\"\
  _blank\">Virtual Programming</a>, <a href=\"http://store.steampowered.com/app/65790/\"\
  \ target=\"_blank\">Steam</a></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c9e8d9069e929f4898939a62f1adcffd.webp
joomla_id: 187
joomla_url: el-clasico-arma-cold-war-assault-sera-portado-por-virtual-programming
layout: post
tags:
- virtual-progamming
- bohemia-interactive
- arma
title: "El cl\xE1sico \"Arma: Cold War Assault\" ser\xE1 portado por Virtual Programming\
  \ (ACTUALIZADO)"
---
Acabo de leer en [GamingOnLinux](https://www.gamingonlinux.com/articles/virtual-programming-are-porting-arma-cold-war-assault-and-frog-climbers-to-linux.8982) la siguiente gran noticia, y es que la compañía dedicada a portar juegos para GNU/Linux y Mac, [Virtual Programming](https://www.vpltd.com/), está trabajando en traernos este gran juego a nuestros PCs proximamente, así como [Frog Climbers](http://store.steampowered.com/app/485120/). 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ArmaColdwar1.webp)


 Para quien no lo conozca, se trata de un juego desarrollado por Bohemia Interactive hace ya bastantes años (2001), y que probablemente muchos conozcais como el primer Operation Flashpoint. Hace unos cuantos años, Codemasters se hizo con los derechos del juego, y desde entonces Bohemia Interactive continuó con la saga con otro nombre, ARMA. El juego marcó un antes y un después en el sector, ya que combina a partes iguales la acción y la simulación. Nos encontramos ante un título en el que cada movimiento puede costarte la vida, donde seguir ordenes (al principio), y darlas bien (después) es tremendamente importante, donde un simple tiro mal dado te puede matar, o en el mejor de los casos herirte y hacer que tengas que pasarte el resto de la misión arrastrandote por el enorme campo de batalla. En él se puede usar todo tipo de vehículos (jeeps, tanques, helicópteros, camiones..) y el arsenal de armas es tremendamente extenso.


 


Arma: Cold War Assault incluye lo que en su día era la campaña principal y su expansión "Resistance". No se ha facilitado ninguna estimación de cuando estará disponible, pero yo ya me estoy frotando las manos deseando poder rejugarlo de nuevo. Os dejo el trailer oficial del juego, por favor, vedlo desde la perspectiva de que es un juego con más de 15 años:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/a75eG8HG5Fk" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


**ACTUALIZACIÓN: Virtual Programing acaba de anunciar en su cuenta de Twitter que el juego entra en oficialmente en Beta, aunque aun no ha aclarado si será pública, privada o solo para desarrolladores.**



> 
> Arma: Cold War Assault - Current State: beta   
> Coming to Mac & Linux [#linuxgaming](https://twitter.com/hashtag/linuxgaming?src=hash) [#macgaming](https://twitter.com/hashtag/macgaming?src=hash) [#ARMA](https://twitter.com/hashtag/ARMA?src=hash) [#Linux](https://twitter.com/hashtag/Linux?src=hash) [#Mac](https://twitter.com/hashtag/Mac?src=hash)<https://t.co/WmaaKKHKRN> [pic.twitter.com/yQ7MOe5XwA](https://t.co/yQ7MOe5XwA)
> 
> 
> — Virtual Programming (@virtualprog) [1 de febrero de 2017](https://twitter.com/virtualprog/status/826741844369403904)



 


FUENTES: [GamingOnLinux](https://www.gamingonlinux.com/articles/virtual-programming-are-porting-arma-cold-war-assault-and-frog-climbers-to-linux.8982), [Virtual Programming](https://www.vpltd.com/), [Steam](http://store.steampowered.com/app/65790/)

