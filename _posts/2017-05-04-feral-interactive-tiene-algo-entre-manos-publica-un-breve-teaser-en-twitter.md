---
author: Pato
category: Editorial
date: 2017-05-04 11:40:06
excerpt: "<p>La compa\xF1\xEDa sigue sin soltar prenda de en qu\xE9 pueden estar metidos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/37e725efe26e0487bc83287a1c350936.webp
joomla_id: 305
joomla_url: feral-interactive-tiene-algo-entre-manos-publica-un-breve-teaser-en-twitter
layout: post
tags:
- proximamente
- feral-interactive
title: 'Feral Interactive tiene algo entre manos: publica un breve teaser en twitter'
---
La compañía sigue sin soltar prenda de en qué pueden estar metidos

Como ya os desvelamos [hace aproximadamente un mes aquí en Jugando en Linux](index.php/homepage/editorial/item/405-el-radar-de-feral-vuelve-a-detectar-un-nuevo-juego-no-identificado) Feral está trabajando en un nuevo desarrollo, tal y como pudimos ver en su radar. Ahora, vuelven a llamar nuestra atención con un breve y escueto vídeo que han publicado en su cuenta de twitter:



> 
> No comment… any ideas? [pic.twitter.com/u3yIwm329W](https://t.co/u3yIwm329W)
> 
> 
> — Feral Interactive (@feralgames) [May 4, 2017](https://twitter.com/feralgames/status/860088104442638337)



 


Seguimos sin saber de qué se trata, puesto que Feral sigue sin soltar prenda. Viendo el vídeo... ¿se os ocurre alguna idea? Nosotros de momento solo tenemos vagas teorías, pero estaríamos encantados de escuchar vuestras ideas y suposiciones...


¿Que tal si nos lo contais en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)?. Os estamos esperando.


 

