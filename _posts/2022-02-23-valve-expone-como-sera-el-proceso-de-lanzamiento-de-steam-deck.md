---
author: Pato
category: Hardware
date: 2022-02-23 21:28:17
excerpt: "<p>Adem\xE1s,&nbsp;avanza el lanzamiento del Dock oficial para la m\xE1\
  quina</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/Deck-box.webp
joomla_id: 1434
joomla_url: valve-expone-como-sera-el-proceso-de-lanzamiento-de-steam-deck
layout: post
tags:
- steamos
- steam
- valve
- steam-deck
title: "Valve expone c\xF3mo ser\xE1 el proceso de lanzamiento de Steam Deck"
---
Además, avanza el lanzamiento del Dock oficial para la máquina


A solo dos días del lanzamiento de la **Steam Deck** Valve acaba de publicar en su último post cómo será el procedimiento que se pondrá en marcha este próximo Viernes 25:


*"Hola a todos, Steam Deck se lanza en solo dos días! hemos pensado enviaros algunos detalles de lo que se puede esperar el viernes 25 de febrero:*


*A partir de las 10 a.m., hora del Pacifico, enviaremos correos electrónicos al primer lote de titulares de reservas a través de la dirección vinculada a su cuenta de Steam. Esas personas tendrán 72 horas para completar su compra en Steam para el modelo específico que reservaron. Si cancelan o no compran en este plazo, liberaremos su reserva a la siguiente persona en la cola.*


*Casi al mismo tiempo comenzará la cobertura de los aproximadamente 100 medios de comunicación a los que enviamos unidades de revisión temprana, por lo que habrá un montón de contenido interesante y cobertura para que todos lo revisen.*


*También esperamos tener más adelante **el Dock oficial para Steam Deck** en manos de los clientes. No sucederá tan pronto como quisiéramos, pero estamos entusiasmados de hablar más sobre esto pronto y **estamos planeando lanzarlo a finales de la primavera**.*


*Mientras tanto, no podemos esperar para que la gente juegue en Steam Deck, y no podemos esperar para escuchar lo que piensas! Siéntete libre de etiquetar la cuenta oficial de Steam Deck en twitter (@OnDeck) para compartir tu experiencia."*


Puedes ver el post en el blog oficial en Steam [en este enlace.](https://steamcommunity.com/games/1675180/announcements/detail/3113680526234449024)


Por nuestra parte, anunciaros que este mismo **Viernes 25** (pasado mañana) **vamos a realizar un nuevo episodio de nuestro podcast mensual en Jugando en Linux** que podreis seguir a través de "[Twitch](https://www.twitch.tv/jugandoenlinux)". Hablaremos sobre todo lo que ha estado sucediendo en estas últimas semanas en torno al mundillo del videojuego en Linux, por supuesto de la Steam Deck (minuto y resultado? ;-P), y todo lo que salga entre medias.


Te esperamos... ¡¡No te lo pierdas!!

