---
author: Pato
category: Puzzles
date: 2017-12-12 18:15:57
excerpt: <p>Es obra de los creadores de SpaceChem o Infinifactory</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f269038fa4534045b59925e7d5e023e1.webp
joomla_id: 571
joomla_url: opus-magnum-sale-de-acceso-anticipado-y-ya-esta-disponible-en-linux-steamos
layout: post
tags:
- indie
- puzzles
- simulador
title: "'Opus Magnum' sale de acceso anticipado y ya est\xE1 disponible en Linux/SteamOS"
---
Es obra de los creadores de SpaceChem o Infinifactory

'Opus Magnum' es el típico juego que cuando te vas a dar cuenta te ha enganchado irremediablemente. Partiendo de la base de que se trata de un juego de "puzzles" peculiar, Zachtronics ha desarrollado un juego que bebe de sus anteriores desarrollos([Infinifactory](http://store.steampowered.com/app/300570/Infinifactory/), [SpaceChem,](http://store.steampowered.com/app/92800/SpaceChem/) [Shenzen IO](http://store.steampowered.com/app/504210/SHENZHEN_IO/)) para llevar lo que aparentemente es un juego de "crear máquinas" a un nuevo nivel.


*Guiado por el alquimista mas prometedor de su generación, Anateus Vaya acaba de aceptar un puesto como Alquimista jefe de la casa Van Tassen, las mas antigua y rica de las casas de los ancestros. Pero los peligros acechan tras la fachada opulenta de la familia, un alquimista solitario puede no ser capaz de resolver cada problema.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Uj689znjxpg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


En Opus Magnum [[web oficial](http://www.zachtronics.com/opus-magnum/)] *tendrás que dominar la intrincada maquinaria de física del motor de transmutación - la herramienta mas avanzada de los ingenieros alquimistas- y usarla para crear remedios vitales, gemas preciosas, armas mortales y mucho más.*


'Opus Magnum' no está disponible en español, pero si te van los buenos juegos de puzzles y el idioma no es problema para ti, lo tienes disponible en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/558990/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Qué te parece Opus Magnum? ¿Te gustan los juegos de puzzles?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

