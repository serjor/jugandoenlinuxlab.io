---
author: Pato
category: "Acci\xF3n"
date: 2017-02-08 18:36:05
excerpt: "<p>As\xED lo han publicado, incluyendo las versiones de Realidad Virtual.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e529a8dc22bd84a37f6f8ae6b8ce40d3.webp
joomla_id: 211
joomla_url: croteam-anuncia-que-todos-los-serious-sam-soportaran-linux-steamos
layout: post
tags:
- accion
- primera-persona
title: "Croteam anuncia que todos los 'Serious Sam' soportar\xE1n Linux/SteamOS con\
  \ Vulkan"
---
Así lo han publicado, incluyendo las versiones de Realidad Virtual.

Esta mañana nos llegaba la noticia vía twitter de que todos los Serious Sam soportarían Linux/SteamOS en una actualización gratuita denominada "Serious Fusion 2017".



> 
> A free update, Serious Fusion 2017, announced for all Sam owners. Also: SteamOS/Linux/OSX support & more VR games! <https://t.co/RjYotLMtqW> [pic.twitter.com/ICNMg5tXFX](https://t.co/ICNMg5tXFX)
> 
> 
> — Croteam (@Croteam) [8 de febrero de 2017](https://twitter.com/Croteam/status/829244251351429120)



 En un extenso artículo donde hablan de los últimos movimientos del estudio con los juegos de Serious Sam comienzan diciendo que están trabajando en los primeros títulos de la serie de cara a soportar la Realidad Virtual y por lo visto los resultados son prometedores. Pero lo mejor viene un poco más adelante donde dan más noticias:


- La actualización para Serious Sam VR The last Hope "Shanti" ya está terminada y se publicará cuando termine la fase de testeo.


- Habrá un SSVR: The second Encounter y un Serious Sam 3 VR: BFE


- Habrá un The Talos Principle VR


- Serious Sam 4 VR está en desarrollo y hay equipos en el estudio trabajando en nuevos proyectos VR.


- Anuncian la actualización Serious Sam Fusion 2017, tanto para los juegos VR como para los que no lo son y que será gratuita para todos los poseedores de los juegos. Esta actualización se lanzará gradualmente comenzando por "The First Encounter", luego llegará "Second Encounter" y "Serious Sam 3 BFE". Esta actualización traerá soporte cruzado para mods en un Steam Workshop común a todos los juegos así como juego cruzado entre juegos VR y normales, así como otras novedades.


Las características principales que tendrán TODOS los juegos gracias a la actualización Serious Sam Fusion de forma gratuita para sus poseedores son:


\* Todos los juegos soportan SteamOS/Linux


\* Pantalla partida


\* Ejecutables de 64 bits


\* Soporte para la API Vulkan (DirectX 9 se está eliminando de los juegos)


\* Renderizado multihilo


\* Nuevo sistema de guardado mas ligero y rápido


\* Soporte para todos los gamepads incluyendo Steam Controller, PS4 y los viejos mandos Direct input


\* Soporte multimonitor


\* Modo ventana sin bordes


\* Salidas de audio configurables


\* Sonido 3D mejorado


\* Motor de físicas mejorado incluyendo los movimientos del personaje


\* "Streaming" de texturas en todos los juegos


\* Soporte mejorado para mods


\* Muchas otras mejoras y actualizaciónes.


Por último, esta actualización les permitirá mantener los juegos actualizados de forma regular mucho mas facil y frecuentemente.


Tienes el artículo completo [en este enlace](http://www.croteam.com/serious-wednesday-update/) (en inglés).


¿Qué te parecen las novedades de Croteam para el Serio de Sam? ¿Significará esto que los títulos de Realidad Virtual también los tendremos disponibles? ¿Estaremos a punto de recibir noticias sobre la Realidad Virtual en Linux?


Cuéntamelo en los comentarios, en el foro o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

