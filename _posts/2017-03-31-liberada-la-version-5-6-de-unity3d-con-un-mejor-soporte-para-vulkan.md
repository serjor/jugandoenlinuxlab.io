---
author: Serjor
category: Editorial
date: 2017-03-31 16:14:21
excerpt: "<p>Marca tambi\xE9n el fin del ciclo de revisiones para la versi\xF3n 5</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/df1da44862bfbe0dd2169e4d14570593.webp
joomla_id: 273
joomla_url: liberada-la-version-5-6-de-unity3d-con-un-mejor-soporte-para-vulkan
layout: post
tags:
- vulkan
- unity3d
title: "Liberada la versi\xF3n 5.6 de Unity3D, con un mejor soporte para Vulkan"
---
Marca también el fin del ciclo de revisiones para la versión 5

Nos enteramos vía twitter de que la gente de Unity3D [ha liberado la versión 5.6](https://t.co/ExMVswIh0h) de su famoso motor.


 



> 
> Unity 5.6 released. Brings API [#Vulkan](https://twitter.com/hashtag/Vulkan?src=hash) support for Windows, [#Android](https://twitter.com/hashtag/Android?src=hash) and [#Linux](https://twitter.com/hashtag/Linux?src=hash) desktop. <https://t.co/ExMVswIh0h> [#Unity](https://twitter.com/hashtag/Unity?src=hash) [#UnityEngine](https://twitter.com/hashtag/UnityEngine?src=hash)
> 
> 
> — Angry Penguin (@AngryPenguinPL) [31 de marzo de 2017](https://twitter.com/AngryPenguinPL/status/847839009640468480)



 


 Unity3D, es quizás uno de los motores más importantes y utilizados a día de hoy en el desarrollo de videojuegos, sobretodo en estudios independientes, gracias por un lado a que no tenía coste por la licencia, y por otro a su soporte multiplataforma, ya que probablemente sea uno de los motores con la lista de sistemas a los que puede exportar más grande.


Entre otras muchas novedades, como mejoras en el rendimiento, gestión de la luz, o un nuevo sistema de partículas, trae la que quizás más nos interesa a los Linuxeros, y es que esta nueva versión viene con soporte completo para Vulkan, con lo que probablemente dentro de no muchos meses empezaremos a ver juegos que soportan esta API de manera habitual, y con la correspondiente ganancia de rendimiento.


También trae soporte experimental para WebAssembly, una tecnología que promete alcanzar velocidades de ejecución cercanas a los programas nativos, con la promesa de juegos webs de calidad, lo cuál hace que el problema de la multiplataforma se pueda reducir, aunque esto por ahora, son promesas y no hechos.


 


Os dejamos con el último vídeo de la GDC presentado por Unity, donde nos explican todas las características de esta nueva versión:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/YHweZ8dhOJA" width="560"></iframe></div>

