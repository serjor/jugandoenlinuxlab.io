---
author: Pato
category: "Acci\xF3n"
date: 2019-02-01 15:36:33
excerpt: "<p>Adem\xE1s lo ten\xE9is en oferta aunque por poco tiempo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4ff28a9823b64d31d17bd0411c6126bd.webp
joomla_id: 961
joomla_url: the-mean-greens-plastic-warfare-vuelve-a-linux-steamos-y-anade-nuevas-caracteristicas
layout: post
tags:
- accion
- indie
- multijugador
- tercera-persona
title: "The Mean Greens -Plastic Warfare vuelve a Linux/SteamOS y a\xF1ade nuevas\
  \ caracter\xEDsticas"
---
Además lo tenéis en oferta aunque por poco tiempo

La historia de "The Mean Greens - Plastic Warfare" es un tanto caótica. En un principio estuvo disponible en Linux/SteamOS aunque hace unos meses los desarrolladores dejaron nuestra versión atrás con resultado incluso de que quitaron el icono de soporte de SteamOS de la tienda de Steam. Sin embargo, desde [gamingonlinux.com](https://www.gamingonlinux.com/articles/the-mean-greens-plastic-warfare-now-has-official-linux-support-once-again.13454) avisan de que vuelve a estar disponible para nuestro sistema favorito, y con mejoras sustanciales.


The Mean Greens- Plastic Warfare es un juego multijugador en tercera persona donde manejamos a soldaditos de plástico en batallas de hasta 10 jugadores en entornos y mapas únicos.


Repasando la lista de actualizaciones del juego, desde principios del mes de enero el juego ha obtenido mejoras en los mapas, los niveles, la dificultad, soporte de listas de servidores e incluso una opción de lanzar servidores dedicados protegidos por password. Puedes ver la lista completa [en este enlace](https://steamcommunity.com/games/360940/announcements/).


"*Participa en la mayor batalla de la historia de Verdes VS. Bronceados. Lucha entre y contra otros en línea. Salta, dispara y avanza hacia la victoria con un juego de ritmo rápido basado en objetivos. A veces las batallas más grandes son peleadas por los soldados más pequeños."*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/8_M3R6uAX9g" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Características:


     10 modos  
     10 mapas  
     6 armas  
     5v5 batallas multijugador  
     Soporte de controlador completo  
     Chat de voz  
     Chat de texto  
     Fiesta de partido  
     Servidores dedicados


En cuanto a los requisitos, son:


Mínimo:  
         ·SO: kernel 2.6.32 o más reciente y glibc 2.12.2 o más reciente  
         ·Procesador: Intel de cuatro núcleos o AMD, 2.5 GHz o más rápido  
         ·Memoria: 4 GB de RAM  
         ·Gráficos: Tarjeta gráfica compatible con OpenGL 4.1  
         ·Red: Conexión de banda ancha a internet  
         ·Almacenamiento: 14 GB de espacio disponible


The Mean Greens - Plastic Warfare está traducido al español, si quieres saber mas puedes visitar su [página web](http://themeangreens.com/), o si quieres jugarlo lo tienes en [Humble Bundle](https://www.humblebundle.com/store/the-mean-greens-plastic-warfare?partner=jugandoenlinux) (enlace patrocinado) o en su [página de Steam](https://store.steampowered.com/app/360940/The_Mean_Greens__Plastic_Warfare/) donde está disponible con un **90%** de descuento en estos momentos. ¡Corre que quedan pocas horas de oferta!

