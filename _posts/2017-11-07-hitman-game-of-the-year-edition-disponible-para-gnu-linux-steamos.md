---
author: leillo1975
category: "Acci\xF3n"
date: 2017-11-07 14:19:31
excerpt: "<p>Esta edici\xF3n se lanza simultaneamente en Linux, Mac y Windows.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/43bbe23868902043e2660941f5e60894.webp
joomla_id: 518
joomla_url: hitman-game-of-the-year-edition-disponible-para-gnu-linux-steamos
layout: post
tags:
- feral-interactive
- hitman
- io-interactive
- game-of-the-year-edition
- goty
title: '"Hitman - Game Of The Year Edition" disponible para GNU-Linux/SteamOS.'
---
Esta edición se lanza simultaneamente en Linux, Mac y Windows.

Estos chicos de Feral están que no paran. La semana pasada nos traían a nuestros PC's [la versión Vulkanizada de F1 2017](index.php/homepage/generos/carreras/item/638-f1-2017-disponible-para-gnu-linux-steamos), y pocos días después nos vuelven a sorprender y nos traen la versión GOTY del conocido último título del **Agente 47**. A mediados de Abril, nuestro compañero [Serjor](index.php/homepage/analisis/itemlist/user/196-serjor) nos deleitaba con un [estupendo análisis](index.php/homepage/analisis/item/367-analisis-hitman) del juego base, y a primeros de Octubre anunciabamos [nuevos contenidos](index.php/homepage/generos/accion/item/602-octubre-sera-un-mes-interesante-para-los-seguidores-de-hitman). Hace un momento nos llegaba un correo electrónico de Feral anunciando la disponibilidad de este, y también podíamos ver el anuncio en twitter como suele ser habitual:



> 
> Set your crosshairs on HITMAN – Game of the Year Edition for macOS and Linux.  
> Load up our website for details: <https://t.co/ABHHiiAsST> [pic.twitter.com/AxSfA0kUkh](https://t.co/AxSfA0kUkh)
> 
> 
> — Feral Interactive (@feralgames) [November 7, 2017](https://twitter.com/feralgames/status/927879751313903622?ref_src=twsrc%5Etfw)


  





 Como comentamos antes, el juego se lanzará a la vez en los 3 sistemas que soporta Steam (GNU-Linux, Mac y Windows), lo cual es de agradecer tanto a [IO Interactive](https://www.ioi.dk/), la  compañía danesa desarrolladora del juego, como a [Feral Interactive](http://www.feralinteractive.com/es/), los portadores a Linux y Mac. En la versión GOTY del juego, a parte del juego original vamos a encontrar con la posibilidad de jugar **cuatro nuevas misiones del Paciente Cero** (Cuatro misiones nuevas basadas en un complot para desencadenar una pandemia vírica global); así como **Tres nuevos paquetes de misiones de Intensificación:** El Paquete Segador, El Paquete Vaquero y el Paquete Payaso; cada uno incluye su propio traje, misión y una recompensa de asesino a añadir a tu arsenal. Teneis toda la información necesaria en el [minisitio](http://www.feralinteractive.com/es/games/hitman/story/) que Feral ha habilitado para la ocasión.


 


En cuanto a los requisitos técnicos necesarios para poder ejecutar el juego con garantías necesitaremos:


* ***OS:** Ubuntu 16.04, Steam OS 2.0*
	+ *Recomendado: Ubuntu 16.10, Steam OS 2.0*
* ***Procesador:** Intel Core i5-2500K, AMD FX-8350 4 GHz*
	+ *Recomendado: Intel Core i7 3770, 4 GHz*
* ***RAM:** 8 GB*
* ***HDD:** 81 GB*
* ***Gráfica:** 2 GB Nvidia GTX 680 (Driver version 375.26), 2 GB AMD R9 270X (Driver version - Mesa 13.0.3)*
	+ *Recomendado: 4 GB Nvidia GTX 970 (Driver version 375.26), 4 GB AMD Radeon R9 290 (Driver version - Mesa 13.0.3)*
* ***Accesorios:** Teclado, Mouse*
	+ *Recomendado: Game Pad*


*Las siguientes tarjetas no están soportadas: AMD Radeon HD 4xxx Series, AMD Radeon HD 5xxx Series, AMD Radeon HD 6xxx Series, ATI Radeon HD2xxx Series, ATI X1xxx Series, además de todas Intel*


Como veis los requisitos no son muy comedidos, por lo que necesitaremos un PC tirando a actual para poder jugarlo. Si quereis ver el trailer del juego podeis verlo aqu:


 


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/JdAVK62d5y4" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 Si quereis adquirir "HITMAN - Game Of The Year Edition" podeis, como siempre, hacerlo en [**la tienda de Feral (Recomendado)**](https://store.feralinteractive.com/en/mac-linux-games/hitmangoty/), o en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/236870/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


 ¿Conoceis la saga de juegos de Hitman? ¿Os convence esta Edición? Déjanos tus impresiones sobre este lanzamiento en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

