---
author: Pato
category: "Acci\xF3n"
date: 2018-02-13 11:17:10
excerpt: "<p>@feralgames lanzar\xE1 el juego ma\xF1ana mismo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0b242dd5a6f3b60d9c07a5d96a2bc449.webp
joomla_id: 641
joomla_url: rise-of-the-tomb-raider-20-anniversario-llegara-a-gnu-linux-proximamente-de-la-mano-de-feral-interactive
layout: post
tags:
- accion
- proximamente
- feral-interactive
- exploracion
- feral
title: "'Rise of the Tomb Raider: 20\xBA Anniversario' llegar\xE1 a GNU-Linux pr\xF3\
  ximamente de la mano de Feral Interactive (ACTUALIZACI\xD3N 3)"
---
@feralgames lanzará el juego mañana mismo

**ACTUALIZACIÓN 18-4-18:** Acabamos de recibir la noticia por parte de Feral a través de correo electrónico que finalmente este esperadísimo juego estará disponible mañana mismo (19 de Abril). También podemos ver dicho anuncio en twitter:



> 
> Tomorrow, on April 19th, hunt and climb your way to survival in Rise of the Tomb Raider for Linux.  
>   
> Become Lara Croft to tame the frozen forests, crumbling ruins and ancient crypts of this acclaimed action-adventure. Learn more on our minisite: <https://t.co/bwMjpzJgWT> [pic.twitter.com/8v1o7UaQXT](https://t.co/8v1o7UaQXT)
> 
> 
> — Feral Interactive (@feralgames) [April 18, 2018](https://twitter.com/feralgames/status/986554259469733888?ref_src=twsrc%5Etfw)



 En cuanto a los requisitos técnicos, Feral nos comunica que serán los siguientes (copio/pego):


***Minimum:***  
*OS: Ubuntu 17.10*  
*Processor: Intel Core i3-4130T or AMD equivalent*  
*Memory: 8 GB RAM*  
*Graphics: 2GB AMD R9 285 (GCN 3rd Gen and above), 2GB Nvidia GTX 680 or better*  
*Storage: 28 GB available space*


***Recommended:***  
*OS: Ubuntu 17.10*  
*Processor: Intel Core i7-3770K*  
*Memory: 12 GB RAM*  
*Graphics: Nvidia GTX 980Ti*  
*Storage: 28 GB available space*


*Notes:*


* *Requires Vulkan.*
* *Nvidia graphics cards require driver version 396.18 or later. If you need to update your drivers, we recommend using the [Proprietary GPU Drivers PPA](https://launchpad.net/~graphics-drivers/+archive/ubuntu/ppa).*
* *AMD graphics cards require Mesa 17.3.5 or later. If you need to update your drivers, we recommend using the [Padoka Stable PPA](https://launchpad.net/~paulo-miguel-dias/+archive/ubuntu/pkppa).*
	+ *Vega graphics cards require Mesa 18.0 or later.*
	+ *AMD GCN 1st and 2nd generation graphics cards are not supported.*
* *Intel graphics cards are not supported.*
* *Requires an SSE2 capable processor.*


Por supuesto, en cuanto el juego esté disponible os  informaremos. ¡Ya queda menos!




---


**ACTUALIZACIÓN 9-4-18**: Mientras esperamos ansiosos la retrasmisión de mañana, **recibimos via twitter** la confirmación de que "Rise Of Tomb Raider" **llegará durante este mes** a nuestros equipos, justo después de llegar a MacOS, que lo hará el jueves de esta misma semana. También anuncian que **los requisitos técnicos serán desvelados poco antes del lanzamiento**. Esperemos que la espera no sea muy larga y que los problemas que tengan con el port no lo retrasen demasiado. Os dejamos con el tweet del anuncio:



> 
> Lara Croft is returning to Linux in Rise of the Tomb Raider later this month, shortly after macOS.  
>   
> Specs will be announced closer to launch. In the meantime, gear up for adventure with our Linux livestream tomorrow at 6PM BST / 10AM PDT on Twitch – <https://t.co/kbDZOaA0zS> [pic.twitter.com/SzUUECAK9l](https://t.co/SzUUECAK9l)
> 
> 
> — Feral Interactive (@feralgames) [April 9, 2018](https://twitter.com/feralgames/status/983283063814873088?ref_src=twsrc%5Etfw)



 




---


**ACTUALIZACIÓN 6-4-18:** Feral acaba de comunicar via Twitter que el próximo Martes, día 10 de Abril, a las 19:00 hora española peninsular (UTC+2) nos mostraran a todos que tal funciona Tomb Raider en Linux a través de su [canal de Twitch](https://www.twitch.tv/feralinteractive). Este es el tweet del anuncio:



> 
> On Tuesday 10 April at 6PM BST / 10AM PDT, get an early look at the spectacular Rise of the Tomb Raider on Linux.  
>   
> Three Feral explorers will team up with Lara Croft to uncover the secret of immortality. Don't miss this [#FeralPlays](https://twitter.com/hashtag/FeralPlays?src=hash&ref_src=twsrc%5Etfw), follow us on Twitch – <https://t.co/kbDZOaRBrq> [pic.twitter.com/nVVJcitRTM](https://t.co/nVVJcitRTM)
> 
> 
> — Feral Interactive (@feralgames) [April 6, 2018](https://twitter.com/feralgames/status/982190500613570560?ref_src=twsrc%5Etfw)



 En JugandoEnLinux.com estamos ansiosos contando las horas para poder verlo, y seguiremos pendientes de cualquier información nueva que nos llegue sobre este título. Mientras tanto id preparando las palomitas.





---


**NOTICIA ORIGINAL:** ¡Hurra! lo que se rumoreaba al fin se ha confirmado. Uno de los dos "OFNIs" era el 'Rise of the Tomb Raider: 20º Aniversario'. La llegada del título de Square Enix y Cristal Dynamics a nuestro sistema favorito era poco menos que un secreto a voces.


Según el comunicado que podéis ver [en este enlace](http://www.feralinteractive.com/en/news/849/), Feral Interactive anuncia:


*Esta primavera, embárcate en la dramática secuela de acción-aventura de Tomb Raider.*


*En Rise of the Tomb Raider, juega como Lara Croft y explora las regiones más antiguas y remotas del mundo para descubrir el secreto de la inmortalidad. Domina el combate de Lara, buscando habilidades para sobrevivir en entornos bellos pero letales mientras te mantienes un paso por delante de Trinity, una organización secreta global.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/sr2jcR4ovPw" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Ya está disponible el mini-site que han preparado para promocionar el juego y que podéis visitar [en este enlace](https://www.feralinteractive.com/es/games/riseofthetombraider/about/), y donde se puede leer entre las características del juego que este 'Rise of the Tomb Raider' **Vendrá con soporte en Vulkan**.  Esperamos tenerlo pronto disponible tanto en su página de steam como en la Tienda Feral, donde siempre es recomendable comprar sus ports para apoyar mejor al estudio.


¿Que os parece este 'Rise of the Tomb Raider? ¿Te embarcarás en esta nueva aventura de Lara Croft? ¿Jugaste al primer título?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

