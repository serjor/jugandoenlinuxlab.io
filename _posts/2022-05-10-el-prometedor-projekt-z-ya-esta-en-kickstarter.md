---
author: leillo1975
category: "Acci\xF3n"
date: 2022-05-10 13:50:21
excerpt: "<p>El FPS de <span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\"\
  >@314arts</span> ha comenzado hace unos d\xEDas su campa\xF1a de financiaci\xF3\
  n.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Projekt_Z/ProjektZLinux.webp
joomla_id: 1464
joomla_url: el-prometedor-projekt-z-ya-esta-en-kickstarter
layout: post
tags:
- kickstarter
- fps
- unity3d
- 314-arts
- projekt-z
title: "El prometedor Projekt Z ya est\xE1 en Kickstarter"
---
El FPS de @314arts ha comenzado hace unos días su campaña de financiación.


 Ciertamente nos sorprende no haber hablado antes de este juego, pues la verdad es que llevamos un tiempo siguiéndole la pista desde que hace unos meses publicasen algunos [videos impresionantes](https://www.youtube.com/channel/UCE1-zmz0HSGb_CdUWRQFgjA) sobre el desarrollo del juego, y mucho más teniendo en cuenta que han prometido el soporte nativo para nuestro sistema desde el principio.


Hace unos días el proyecto llegaba definitivamente a **Kickstarter** donde intentará alcanzar la **meta de financiación de 100.000€**. A partir de ahí la idea es ir avanzando poco a poco en el desarrollo hasta llegar a la versión final del juego que se espera para **finales de 2024**, pasando antes por una versión Alpha privada, y Betas Privada y Pública. Para conseguir estos objetivos hay diferentes tipos de aportaciones que van desde los 5 a los 4000€, donde logicamente según invirtamos, tendremos derecho a más o menos cosas relaccionadas con el juego. **Si nuestra intención es acceder a la beta, tendremos que aportar como mínimo 25€**. Si quereis más información sobre esta campaña os recomendamos que accedais a este [link de Kickstarter](https://www.kickstarter.com/projects/314artsohg/projekt-z-ww2-zombie-coop-fps).


![](https://ksr-ugc.imgix.net/assets/036/938/922/7bef47112c39366bbf9cd68a04bff291_original.gif?ixlib=rb-4.0.2&w=680&fit=max&v=1649558180&gif-q=50&q=92&s=d18b6b1bd8c7c06f139804bf93d80454)


Para quien no lo conozca aun [Projekt Z](https://www.projektzgame.com/) es un **juego de acción en primera persona** creado con **Unity3D** por la compañía alemana [314 Arts](https://www.314-arts.com/), y que hace gala de mezclar elementos tan manidos como la **segunda guerra mundial y la temática zombie**, algo que ya habíamos visto en otros conocidos juegos. En este caso, **el juego será Free2Play**, por lo que en un principio no habrá que desembolsar un solo euro para jugar, pero en cual habrá que pagar por ciertas características como en otros títulos de su género en forma de DLCs y pases de temporada. Eso si, **podremos terminar el juego sin gastar "un duro"**. Esta es su descripción oficial en su [pagina de Steam](https://store.steampowered.com/app/763310/Projekt_Z/):


*Projekt Z es un juego de disparos en primera persona gratuito ambientado en un escenario de zombis de la Segunda Guerra Mundial en una isla secreta alemana. El juego se centra en el "Projekt Z", un proyecto dirigido por los nazis para convertir a los Zombis, que han sido descubiertos en la isla con anterioridad, en armas para cambiar la guerra a favor de la Alemania nazi. En la isla, descubrirás varios proyectos secretos nazis relacionados y no relacionados con el "Projekt Z".*


* ***Modo de juego principal de Projekt Z***


*Tienes el HUB, aquí puedes fabricar armas, munición y otros consumibles. También podrás construir cosas dentro del hub (similar a la construcción de asentamientos de Fallout 4). Desde el hub, se inician las misiones. Las misiones pertenecen a un arco argumental y pueden influirse mutuamente, dependiendo del orden en que las hagas. Un ejemplo: una misión en la que limpies el sistema de alcantarillado de la isla hará que en otras misiones aparezcan diferentes tipos de zombis que, de otro modo, estarían atrapados en el sistema de alcantarillado.*


*Los arcos argumentales tienen un principio y un final y no deben alargarse indefinidamente. Al fin y al cabo, las mejores historias son las que se planificaron con un final definitivo en mente.*


*Durante las misiones, puedes encontrar objetos, materiales de construcción (para el hub) y reclutar personal para tu hub.
Las misiones se pueden repetir para recoger más materiales/botín.*


* ***Modo Supervivencia de Olas de Projekt Z***


*El modo Wave Survival no tiene nada que ver con el modo principal del juego. En este modo, tienes que luchar por tu vida, matar interminables oleadas de zombis y mejorar tus armas en tu mesa de trabajo.
Sube de nivel a tu personaje en cada ronda con drogas como la heroína, el alcohol o la cocaína.*


Como podeis ver el juego es claramente no apto para todos los publicos ni para los politicamente correctos... Desde JugandoEnLinux.com estaremos (ahora si) atentos a las actualizaciones importantes de este proyecto. Mientras tanto os dejamos con el trailer creado para celebrar la llegada a Kickstarter:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/w1eTeRjiacA" style="display: block; margin-left: auto; margin-right: auto;" title="YouTube video player" width="780"></iframe></div>


 

