---
author: leillo1975
category: "Acci\xF3n"
date: 2020-01-27 11:47:37
excerpt: "<p>@3DRealms nos traer\xE1 este cl\xE1sico remasterizado.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/KingpinReloaded/KingpinReloaded.webp
joomla_id: 1155
joomla_url: kingpin-reloaded-llegara-a-linux
layout: post
tags:
- remasterizacion
- 3d-realms
- interplay
- quake-2
- slipgate-ironworks
- kingpin
title: "Kingpin: Reloaded llegar\xE1 a Linux. "
---
@3DRealms nos traerá este clásico remasterizado.


Una vez más, gracias a [GamingOnLinux](https://www.gamingonlinux.com/articles/get-ready-to-live-a-life-of-crime-with-kingpin-reloaded-announced-by-3d-realms-will-be-coming-to-linux.15852), nos enteramos de que este **mítico juego de finales de los 90**, construido sobre el trillado motor de **Quake II** y que tanto disfruté en su día, será actualizado para adaptarlo mejor a los tiempos que corren. Este título que fué desarrollado por **Xatrix Entertaiment** ([Gray Matter Interactive](https://en.wikipedia.org/wiki/Gray_Matter_Interactive)) y editado por la conocidisima [Interplay](https://en.wikipedia.org/wiki/Interplay_Entertainment), mostraba una **violencia inusitada** que podíamos percibir ya no solo en las imágenes y actos, sino también en el lenguaje.


Los encargados de actualizar el juego son [Slipgate Ironworks](https://slipgate-studios.com/), compañía que recientemente se ha encargado del port a consolas de [Wrath: Aeon of Ruin](index.php/listado-de-categorias/accion/990-wrath-aeon-of-ruin-un-nuevo-fps-retro-llegara-a-linux), juego del que aquí hablamos hace unos meses. Entre las novedades que encontraremos en [Kingpin: Reloaded](https://3drealms.com/catalog/kingpin-reloaded_171/) con respecto al original, encontraremos:


*-Resolución 4K*


*-Soporte para monitores UltraWide*


*-Gráficos actualizados en el modo mejorado.*


*-Compatibilidad con mandos.*


*-Nuevos sistemas de búsqueda y conversación.*


*-Modo No-Violencia*


"**Kingpin: Life of Crime**" marcó un antes y un después en los shooters, tan de moda en aquella eṕoca, ya no solo por su estética **"Art Decó"**, sinó también por su cuidada y original jugabilidad, mostrando mecánicas que no eran habituales en los shooters 3D de la eṕoca, como la **posibilidad comprar armas** con el dinero que vayamos recogiendo en el **Pawn-O-Matic**, de poder **modificarlas** a nuestro gusto, **interactuar con otros personajes** del juego obteniendo distintas respuestas según sea nuestra actitud o **contratando matones** para que formen parte de nuestra banda y nos ayuden en el juego. También había que destacar la **introducción de las zonas e intensidades de daño**,  lo cual permitía que si heríamos a un enemigo en determinadas **zonas del cuerpo**, quedase más o menos tocado, e incluso presentase **heridas** más impactantes visualmente hablando y fuesen dejando **rastros de sangre** que nos permitirían dar con ellos. Como veis el juego estaba lleno de lindezas...


El juego estará disponible en el segundo cuarto de este año por lo que lo veremos en tan solo unos meses, y ya tiene su [web en Steam](https://store.steampowered.com/app/1224700/Kingpin_Reloaded/) , donde podeis añadirlo si quereis a deseados para mostrar vuestro interés como Linuxeros. Os dejamos con el trailer de presentación del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/bIZncEjZoig" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


¿Jugasteis al "Kingpin: Life of Crime" original? ¿Qué opinión os merecen estas remasterizaciones de juegos ya lanzados? Puedes contarnos lo que quieras sobre esta noticia en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

