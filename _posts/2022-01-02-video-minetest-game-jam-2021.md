---
author: P_Vader
category: "V\xEDdeos JEL"
date: 2022-01-02 19:45:20
excerpt: "<p><span class=\"style-scope yt-formatted-string\" dir=\"auto\">Probamos\
  \ los minijuegos ganadores de la Minstest Game Jam 2021</span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Minetest/minetest_game_jam_2021.webp
joomla_id: 1405
joomla_url: video-minetest-game-jam-2021
layout: post
tags:
- minetest
- jam
- '2021'
title: "V\xCDDEO: Minetest Game Jam 2021"
---
Probamos los minijuegos ganadores de la Minstest Game Jam 2021



<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/Ir9GrkaWCUM" title="YouTube video player" width="780"></iframe></div>

