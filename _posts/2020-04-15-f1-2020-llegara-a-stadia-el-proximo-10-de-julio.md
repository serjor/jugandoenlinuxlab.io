---
author: Pato
category: "Simulaci\xF3n"
date: 2020-04-15 18:09:04
excerpt: "<p>El pr\xF3ximo juego de Codemasters y Koch Media se podr\xE1 jugar en\
  \ Linux a trav\xE9s de dicha plataforma</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/F12020stadia.webp
joomla_id: 1208
joomla_url: f1-2020-llegara-a-stadia-el-proximo-10-de-julio
layout: post
title: "F1 2020 llegar\xE1 a Stadia el pr\xF3ximo 10 de Julio"
---
El próximo juego de Codemasters y Koch Media se podrá jugar en Linux a través de dicha plataforma


 


Para todos los que esperan una nueva entrega de la saga de Fórmula 1 en nuestro sistema favorito, tenemos buenas noticias, o según se mire puede que con sabor agridulce. Esta misma tarde nos hemos enterado a través de la cuenta oficial de Koch Media de que el juego de F1 2020 llegará a Stadia el próximo día 10 de Julio:



> 
> Be the 11th team in the biggest F1® game to date. Introducing F1® 2020.   
>   
> Coming to PS4, Xbox One, PC and Google Stadia July 10 2020. ?[#F12020](https://twitter.com/hashtag/F12020?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/phkZSnYHnj](https://t.co/phkZSnYHnj)
> 
> 
> — Koch Media UK (@KochMediaUK) [April 15, 2020](https://twitter.com/KochMediaUK/status/1250408534602366977?ref_src=twsrc%5Etfw)



¿Por qué decimos que puede suponer un sabor agridulce? Pues por que ciertamente **por fin podremos disfrutar de un juego de F1 en Linux el mismo día de lanzamiento**, pero la parte no tan buena es que por ahora, y por lo que hemos podido comprobar, **no tenemos soporte para los volantes** a través de nuestros navegadores basados en Chromium.


Por lo que sabemos los volantes de Logitech, únicos que funcionan con todas las garantías en sistemas Linux no son detectados por Stadia a través de Chrome, Chromium o Vivaldi, citando los que hemos podido probar por nosotros mismos.


Así pues, y a la espera de saber si Codemasters o Feral Interactive harán una versión nativa para Linux en Steam de momento podremos jugar a F1 2020 pero con los mandos soportados por Stadia, que según hemos comprobado son los mandos de Xbox o los compatibles con estos.


¿Qué te parece la llegada de títulos triple A a Linux a través de Stadia? ¿Les darás una oportunidad?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

