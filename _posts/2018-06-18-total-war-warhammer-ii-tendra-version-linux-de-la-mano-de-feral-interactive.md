---
author: Pato
category: Estrategia
date: 2018-06-18 11:05:14
excerpt: "<p>@feralgames el estudio al que tanto le debemos lo ha anunciado en un\
  \ tweet y ya est\xE1 disponible el v\xEDdeo promocional</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c54cd01619725a4dabebc35be504cd7b.webp
joomla_id: 773
joomla_url: total-war-warhammer-ii-tendra-version-linux-de-la-mano-de-feral-interactive
layout: post
tags:
- accion
- proximamente
- feral-interactive
- estrategia
title: "Total War: WARHAMMER II tendr\xE1 versi\xF3n Linux de la mano de Feral Interactive"
---
@feralgames el estudio al que tanto le debemos lo ha anunciado en un tweet y ya está disponible el vídeo promocional

¡Por fin! tenemos la confirmación de que el juego que **Feral Interactive** tenía entre manos y que la semana pasada divisamos como un nuevo OFNI no es otro que '**Total War: WARHAMMER II**'. Parece ser que la asociación del estudio británico con Sega y Creative Assembly sigue con buen pie, pues ya son unos cuantos títulos de estos estudios que nos llegan gracias a Feral. Gracias a un tweet nos han desvelado el próximo título que nos llegará "durante este año" con fecha aún por determinar, aunque conociendo a Feral no tendremos que esperar mucho para tenerlo en nuestros PCs.



> 
> It’s the news you’ve been seeking for so long… Total War: WARHAMMER II comes to macOS and Linux later this year!  
>   
> For your first eye-widening look at the spellbinding campaigns and combat of WARHAMMER II, watch the trailer on YouTube: <https://t.co/96HN4fEYsb> [pic.twitter.com/LCkqWIYBDc](https://t.co/LCkqWIYBDc)
> 
> 
> — Feral Interactive (@feralgames) [June 18, 2018](https://twitter.com/feralgames/status/1008650958455345152?ref_src=twsrc%5Etfw)



 


Por otra parte, también lo han anunciado [en un nuevo artículo](https://www.feralinteractive.com/en/news/875/) en su web oficial.


En cuanto a 'Total War: WARHAMMER II', se trata de la nueva entrega lanzada en Septiembre de esta saga de la que [ya tenemos](https://store.steampowered.com/search/?snr=1_5_9__12&term=total+war) un buen puñado de juegos.


#### Defiende tu mundo. Destruye el suyo.


*Total War™: WARHAMMER® II es un juego de estrategia de proporciones épicas. Elige de entre cuatro exclusivas facciones y decide cómo librar la guerra: lanzando una campaña de conquista para salvar este enorme y fantástico mundo o destruirlo.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/WeDQQBwxRTU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 **Total War™: WARHAMMER® II** posee dos partes diferenciadas: una de ellas es la campaña de mundo abierto por turnos y la segunda consiste en batallas tácticas a tiempo real, ambientadas en los fantásticos paisajes del Nuevo Mundo.


Si quieres saber más detalles, puedes visitar la [miniweb](https://www.feralinteractive.com/en/games/warhammer2tw/) que ya han creado para el juego en su propia página como ya es habitual y donde irán añadiendo más contenido en próximas fechas.


¿Qué te parece este Total War: WARHAMMER II? ¿Te gusta el universo Total War? ¿Juegas a juegos de estrategia de Creative Assembly?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

