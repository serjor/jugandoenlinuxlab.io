---
author: Serjor
category: Sandbox
date: 2021-02-19 15:21:51
excerpt: "<p>Un punto de encuentro m\xE1s para nuestra comunidad</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/Valheim900.webp
joomla_id: 1279
joomla_url: servidor-dedicado-de-valheim-oficial-de-jugandoenlinux-com
layout: post
tags:
- valheim
- servidor-dedicado
title: Servidor dedicado oficial de jugandoenlinux.com para Valheim
---
Un punto de encuentro más para nuestra comunidad


No creo que nadie descubra gracias a nosotros que Valheim ha sido toda una revolución que ha sorprendido a propios y extraños, ya que hace unos días os decíamos que había llegado a su millón de usuarios, prácticamente enseguida tuvimos que corregirnos para hablar de sus dos millones, y resulta que leemos en [GOL](https://www.gamingonlinux.com/2021/02/theres-no-stopping-the-viking-invasion-as-valheim-hits-3-million-sales) que ya ha llegado a los tres millones. Y aunque no os decimos nada que no sepáis ya, sí que esperamos que gracias a nosotros podáis disfrutar de este juego que ahora mismo goza de un momento imparable, también entre los linuxeros, con gente de la comunidad de jugandoenlinux a través del servidor dedicado que hemos creado para el juego.


Desafortunadamente, y debido a que debe haber algún tipo de ¿bug? ¿problema? ¿parámetro de configuración desconocido? El encontrar listado el servidor desde el propio juego está solamente al alcance de unos pocos elegidos por Thor, así que para el resto de los mortales os traemos un tutorial rápido de cómo añadir el nuestro (tuyo también querido lector) en Steam para poder acceder a él.


En primer lugar en el menú "Ver" escogemos "Servidores":


![tuto server valheim 1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ValheimServer/tuto_server_valheim_1.webp)


Cuando se abra la ventana de servidores vamos a la pestaña "Favoritos" y pulsamos sobre "Añadir servidor":


![tuto server valheim 2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ValheimServer/tuto_server_valheim_2.webp)


En el popup que aparecerá añadimos la dirección del servidor y por último le damos a "Añadir servidor a favoritos":


![tuto server valheim 3](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ValheimServer/tuto_server_valheim_3.webp)


Con lo que ya debería aparecernos en la lista de servidores favoritos:


![tuto server valheim 4](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ValheimServer/tuto_server_valheim_4.webp)


La dirección del servidor es <valheim.jugandoenlinux.com:2457> y para evitar abusos está protegido por contraseña.


La contraseña inicial es bastante fácil de deducir, pero si en el futuro decidimos cambiarla de manera periódica o por si no atinas y quieres preguntar por ella, puedes hacerlo en el canal [#a_jugar:matrix.org](https://matrix.to/#/#a_jugar:matrix.org?via=matrix.org&via=t2bot.io&via=matrix.allmende.io) de Matrix, donde además os animamos a que organicéis quedadas para acabar con ese troll que te mira mal.


Esperamos que os guste la iniciativa, y sobre todo que lo uséis, pero que lo uséis correctamente. Es el primer servidor dedicado que ponemos disponible de manera oficial, y aún no sabemos muy bien cómo evolucionará, si habrá problemas entre usuarios y cómo moderarlo, con lo que ante cualquier incidencia o conflicto os pedimos por un lado paciencia y por otro un reporte con evidencias, por si tenemos que tomar alguna medida, que sea con datos suficientes.


Os dejamos con un vídeo de Leillo haciendo el vikingo:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/mH9g9o_8ofo" width="560"></iframe></div>


Lo dicho, ¡a jugar!


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/892970/" style="border: 0px;" width="646"></iframe></div>

