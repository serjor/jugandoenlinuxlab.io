---
author: Pato
category: "Acci\xF3n"
date: 2018-08-24 20:15:08
excerpt: "<p>El juego que ha llegado a los que aportaron al proyecto en Linux parece\
  \ que est\xE1 como m\xEDnimo bastante verde</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8181cbd90df5a1571a01396c2c96d8d0.webp
joomla_id: 843
joomla_url: crazy-justice-ya-esta-disponible-pero-solo-para-los-backers
layout: post
tags:
- accion
- indie
- acceso-anticipado
- battle-royale
- crazy-justice
title: "Crazy Justice ya est\xE1 disponible, pero solo para los \"Backers\""
---
El juego que ha llegado a los que aportaron al proyecto en Linux parece que está como mínimo bastante verde

Buenas y malas noticias para los que están esperando un Battle Royale en condiciones en Linux/SteamOS. Hoy era el día para el acceso anticipado, tal y como os contamos en una noticia anterior, pero al parecer Black Riddles Studio solo ha enviado claves a los que aportaron dinero a la campaña de financiación colectiva.


De todos modos, y [según los reportes](https://www.gamingonlinux.com/articles/despite-promising-an-early-access-release-crazy-justice-is-out-only-for-backers-for-now-and-its-rougher-than-expected.12417) de algunos "afortunados" que han recibido una clave, el juego ha llegado en un estado cuando menos de "beta temprana" pues reportan fallos en texturas, caídas de frames y algunos otros problemas típicos de un producto sin pulir.


Por otro lado, **hay quien afirma que la versión Windows del juego parece estar algo mas pulida** a tenor de los vídeos que ya han comenzado a circular por la red.


Estaremos atentos a las novedades de cara al 31 de Agosto, fecha en la que aparece como lanzamiento oficial en SteamDB.

