---
author: Pato
category: Aventuras
date: 2017-03-30 16:27:26
excerpt: <p>El juego es obra del estudio de Ron Gilbert</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/97a787f8d6fb66aaef15fa858aa433ea.webp
joomla_id: 269
joomla_url: thimbleweed-park-ya-disponible
layout: post
tags:
- indie
- aventura
- pixel
- point-and-clic
title: '''Thimbleweed Park'' ya disponible en Linux/SteamOS'
---
El juego es obra del estudio de Ron Gilbert

'Thimbleweed Park' es de esos juegos que apuntan a clásico nada mas salir. Desarrollado por "Terrible Toybox" quien realmente está detrás de este desarrollo es nada menos que Ron Gilbert, el padre de juegos como Monkey Island o Maniac Mansion.


'Thimbleweed Park' [[web oficial](https://thimbleweedpark.com/)] culminó con éxito una campaña en Kickstarter y ahora nos llega esta aventura de corte retro y píxeles por doquier y con personajes y situaciones de lo más... divertido.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/6HcoXpDNwBA" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Sinopsis del juego:


*Thimbleweed Park te da la bienvenida. Población: 80 lunáticos.*


*Un hotel paranormal, un circo abandonado, una fábrica de almohadas incendiada, un cadáver pixelándose bajo el puente, inodoros que funcionan con tubos de vacío... Nunca has estado en un sitio como este.*


*Por diversos motivos, cinco personas se encuentran al mismo tiempo en este pueblo arruinado y abandonado. Todavía no lo saben, pero todos tienen un profundo vínculo en común. Y alguien los observa.*


Características:


*De Ron Gilbert y Gary Winnick, creadores de Monkey Island y Maniac Mansion.*  
*Una historia de crimen y misterio ambientada en 1987.*  
*Cinco personajes jugables que pueden trabajar en equipo... o sacarse de quicio.*  
*¡No es un simulador de andar!*  
*Ingeniosos puzles entrelazados con una trama llena de sorpresas que no te dejará indiferente.*  
*Un mundo enorme y extraño que puedes explorar a tu ritmo.*  
*Un chiste cada dos minutos... ¡garantizado!\**  
*Dos modos de juego, casual y difícil, de dificultad variada.*


Si te gustan las aventuras divertidas con situaciones de lo más hilarantes, desde luego no puedes dejarlo escapar. 


Tienes 'Thimbleweed Park' disponible con textos en español en su página de Steam:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/569860/" width="646"></iframe></div>


 

