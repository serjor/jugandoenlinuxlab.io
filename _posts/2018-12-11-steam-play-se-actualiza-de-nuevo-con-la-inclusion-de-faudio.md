---
author: leillo1975
category: Software
date: 2018-12-11 20:30:28
excerpt: "<p>Proton alcanza la versi\xF3n 3.16-5</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1789963c93f14377406204a5c197f3cc.webp
joomla_id: 929
joomla_url: steam-play-se-actualiza-de-nuevo-con-la-inclusion-de-faudio
layout: post
tags:
- beta
- valve
- ethan-lee
- steam-play
- dxvk
- proton
- faudio
title: "Steam Play se actualiza de nuevo con la inclusi\xF3n de FAudio."
---
Proton alcanza la versión 3.16-5

Todo el mundo lo llevaba esperando desde que se [anunció](index.php/homepage/noticias/39-noticia/999-ethan-lee-pone-en-modo-mantenimiento-sus-otros-desarrollos-y-comienza-a-trabajar-en-proton) que [Ethan Lee](index.php/buscar?searchword=ethan%20lee&ordering=newest&searchphrase=all) se ponía a trabajar para Valve para implementar FAudio. Finalmente hoy ha llegado al fin en una nueva beta de Proton, concretamente la 3.16-5. La [lista de cambios](https://github.com/ValveSoftware/Proton/wiki/Changelog#available-in-proton-316-beta) es imporante como podeis ver a continuación:



> 
> -Implementación completamente nueva de XAudio2, utilizando el proyecto **FAudio**. Esto debería proporcionar una calidad de audio mucho mayor para los juegos que utilizan XAudio2. En particular, ahora soporta las funciones de mezcla de volumen y procesamiento de efectos de audio que faltaban en la antigua implementación.  
>  -**Mejor soporte para navegadores web** incrustados basados en Chromium. Los juegos y lanzadores que contienen un navegador web integrado ahora pueden funcionar mejor.  
>  -Se actualiza a **DXVK 0,93**. También **incluye un arreglo para algunos juegos de Unity VR** cuando se ejecutan con DXVK, como SUPERHOT VR y Job Simulator.  
>  -Mejoras de compatibilidad para cambiar rutas entre Linux y Windows usando la API de Steam. Entre otras cosas, esto debería arreglar las imágenes de iconos de Steam Controller en ciertos juegos.  
>  -Correcciones menores para usuarios de PulseAudio con cierto hardware.  
>  -Para los desarrolladores, soporte opcional para Vagrant, que debería facilitar el proceso de creación de builds de VM en Proton.
> 
> 
> 


También hay que decir que se ha actualizado la lista de títulos que entran en la "Whitelist" oficial de Valve, segun podemos ver en [SteamDB](https://steamdb.info/app/891390/history/):


* *AstroPop Deluxe*
* *Stronghold HD*
* *Puzzle Agent 2*
* *BEEP*
* *Botanicula*
* *Castlevania: Lords of Shadow – Ultimate Edition*
* *Solstice*
* *20XX*
* *Crash Dive*
* *Football Tactics & Glory*
* *Mushihimesama*
* *Apocalipsis*
* *Long Gone Days*
* *Rusty Lake: Roots*
* *SYMMETRY*
* *First Strike: Final Hour*
* *Subsurface Circular*
* *Avernum 3: Ruined World*
* *Tactical Monsters Rumble Arena*
* *A Case of Distrust*
* *Rusty Lake Paradise*
* *Dragon Cliff*
* *Infinos Gaiden*
* *Meteor 60 Seconds!*
* *Objects in space*
* *O.C.D. - On Completeness & Dissonance*
* *Anime Dress Up*
* *Mad Tower Tycoon*


Como veis poco a poco, este proyecto avanza más y más, llegando hasta donde muchos nunca pensamos que podríamos llegar a ver. Continuaremos informando....

