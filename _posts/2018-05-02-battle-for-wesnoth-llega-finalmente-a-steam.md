---
author: leillo1975
category: Estrategia
date: 2018-05-02 07:58:32
excerpt: "<p>Adem\xE1s se actualiza con importantes novedades.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6648f45035a47efdafeee4d3f3f056e4.webp
joomla_id: 723
joomla_url: battle-for-wesnoth-llega-finalmente-a-steam
layout: post
tags:
- steam
- open-source
- estrategia-por-turnos
- battle-for-wesnoth
title: '"Battle for Wesnoth" llega finalmente a Steam.'
---
Además se actualiza con importantes novedades.

Gracias a un miembro de nuestro grupo de Telegram, **@franciscot**, hemos sabido que este conocido juego de **código libre**, "[Battle for Wesnoth](https://www.wesnoth.org/)" ha desembarcado finalmente en **Steam**, siguiendo los pasos de otros como el reciente [Zero-K](https://store.steampowered.com/app/334920/ZeroK/), del que seguimos esperando versión para Linux, o [SuperTuxKart](index.php/homepage/generos/carreras/item/634-super-tux-kart-lanza-una-actualizacion-especial-para-halloween) que lo hará proximamente. El juego además se ha actualizado a la **versión 1.14** que incluye importantes [mejoras](https://www.wesnoth.org/start/1.14/) entre las que destacan:


- Una nueva campaña de un jugador


- Una renovación de la pantalla de multijugador


- Un organizador de añadidos y mods


- Mejoras en el motor gráfico


- Nuevas animaciones y gráficos de unidades


 


"Battle for Wesnoth" es, para quien no lo conozca, un juego de **estrategia por turnos** a la vieja usanza, que recuerda en gran medida a clásicos como  las sagas de [Heroes of Might & Magic](https://es.wikipedia.org/wiki/Heroes_of_Might_and_Magic), [Disciples](https://en.wikipedia.org/wiki/Disciples:_Sacred_Lands) o [Warlords](https://en.wikipedia.org/wiki/Warlords_(game_series)). Este veterano título, que lleva ya casi 15 años entre nosotros, es uno de los mejores juegos Open Source de los que disponemos y tiene un **innegable y dedidicado trabajo detrás** que conviene no despreciar. Por supuesto es un juego muy recomendado si nos gusta este género. Las descripción oficial del juego es la siguiente:



> 
> La Batalla for Wesnoth es un juego de estrategia basado en turnos de código abierto con un tema de alta fantasía. ¡De los llanos de Weldyn hasta los bosques de Wesmere, de las minas de Knalga a las altas cumbres de las Montañas Profundas, tome parte en docenas de aventuras a través de siglos de historia! Juegue el papel de un joven príncipe en su huida de los señores liches a un nuevo hogar a través del océano... Adéntrese en las oscuridades más profundas de la tierra para forjar una gema de fuego ardiente... Viaje a través del continente en una osada campaña para reclamar su trono legítimo de una reina usurpadora... Tome el lugar de un joven mago, su aldea bajo asedio por una invasión de orcos, en su búsqueda de venganza e inmortalidad... Combata las hordas de no muertos desatadas por un vil nigromante en curso a su capital... Lidere un grupo intrépido de sobrevivientes a través de las arenas ardientes a una confrontación final con un enemigo jamás antes visto... La opción es suya.  
>   
>  Luego, ¡embárquese en nuevas batallas en línea! ¡Escoja de entre siete facciones completamente balanceadas y luche contra sus amigos en más de 50 mapas multijugador para grupos de diversos tamaños!  
>   
>  ¡Explore cientos de campañas, mapas y facciones creados por la comunidad tanto para modos de un solo jugador y multijugador, y ponga a prueba sus habilidades para crear su propio contenido! Wesnoth incluye su propio editor de mapas, un motor de juego altamente modificable, un lenguaje de programación simple e intuitivo, y una amistosa comunidad de creadores llenos de dedicación. ¡Nadie se imagina qué aventuras hará realidad!
> 
> 
> 


El juego además puede ser disfrutado en cualquier PC sin problemas debido a sus **bajos requisitos**:


* **SO:** Ubuntu 16.04 o compatible
* **Procesador:** Doble núcleo 2.0 GHz o mejor
* **Memoria:** 2 GB de RAM
* **Almacenamiento:** 800 MB de espacio disponible


Con motivo de esta nueva versión, podemos deleitarnos viendo este trailer donde se nos detallan las características de este juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/xDDKImYOIvA" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 Si quereis jugar a esta ultima versión de "Battle for Wesnoth" podeis descargarlo completamente gratis desde su [página web](https://www.wesnoth.org/) o en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/599390/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Qué opinión os merece que juegos de Código Libre sean lanzados en una plataforma privativa como Steam? ¿Habeis jugado ya a "Battle for Wesnoth"? Déjanos tus impresiones en los comentarios o en nuestros grupos de [Telegram](https://t.me/jugandoenlinux) y [Discord](https://discord.gg/fgQubVY).

