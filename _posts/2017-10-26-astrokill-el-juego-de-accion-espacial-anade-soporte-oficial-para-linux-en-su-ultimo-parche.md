---
author: Pato
category: "Acci\xF3n"
date: 2017-10-26 15:48:04
excerpt: "<p>El juego a\xFAn est\xE1 en acceso anticipado pero los desarrolladores\
  \ consideran la versi\xF3n de Linux suficientemente madura</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/738d4cfca208fd24c93e01c555c7f6b6.webp
joomla_id: 505
joomla_url: astrokill-el-juego-de-accion-espacial-anade-soporte-oficial-para-linux-en-su-ultimo-parche
layout: post
tags:
- accion
- indie
- acceso-anticipado
title: "'Astrokill' el juego de acci\xF3n espacial a\xF1ade soporte oficial para Linux\
  \ en su \xFAltimo parche"
---
El juego aún está en acceso anticipado pero los desarrolladores consideran la versión de Linux suficientemente madura

Los desarrolladores de 'Astrokill' lanzaron el pasado día 23 el último parche de su juego (el 0.9.2) y en el que según [su anuncio oficial](http://steamcommunity.com/games/376880/announcements/detail/2517935604739652299) ya consideran oficial el soporte en Linux. 


Anteriormente, y tal como nos anunció Serjor [en un primer artículo](index.php/homepage/generos/accion/item/554-los-desarrolladores-de-astrokill-buscan-betatesters-para-la-version-de-gnu-linux), los desarrolladores estuvieron buscando betatesters para poder sacar adelante su versión para nuestro sistema favorito, y tras unos meses ya tenemos nuestra versión. 


De todos modos, el juego sigue en fase de acceso anticipado y esperan tener una versión definitiva de lanzamiento en próximas fechas.


'Astrokill' es un shooter espacial moderno con claras influencias de juegos "old school" como "TIE Fighter" o "Freespace 2". Los jugadores combatirán entre facciones en naves de combate, intercepción, asalto, médicalizadas etc..  en perspectiva de primera o tercera persona.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Mn7iUPN458E" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Los requisitos para mover el juego son:


**MÍNIMO:**


+ **SO:** Linux o SteamOS
+ **Procesador:** 2.8 GHz
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** GeForce GTX 660 o AMD Radeon HD 6800
+ **Almacenamiento:** 5 GB de espacio disponible


**RECOMENDADO:**


+ **SO:** Linux o SteamOS
+ **Procesador:** 3.2 GHz
+ **Memoria:** 16 GB de RAM
+ **Gráficos:** GeForce GTX 1050 o AMD Radeon RX 460
+ **Almacenamiento:** 5 GB de espacio disponible


'Astrokill' no está disponible en español, pero si no es problema y quieres pegar unos tiros con tus cañones láser, lo tienes disponible en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/376880/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

