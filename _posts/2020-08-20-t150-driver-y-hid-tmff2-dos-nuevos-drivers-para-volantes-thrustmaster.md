---
author: leillo1975
category: Software
date: 2020-08-20 17:59:17
excerpt: "<p>Ya es posible el Force Feedback en los volantes T150 y T300 de <span\
  \ class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\">@TMThrustmaster.\
  \ </span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/thrustmaster/Thrustmaster_drivers.webp
joomla_id: 1249
joomla_url: t150-driver-y-hid-tmff2-dos-nuevos-drivers-para-volantes-thrustmaster
layout: post
tags:
- drivers
- force-feedback
- thrustmaster
- modulo
- t150
- t300-rs
- t150-driver
- hid-tmff2
- tmdrv
title: t150_driver y hid-tmff2, dos nuevos drivers para volantes Thrustmaster
---
Ya es posible el Force Feedback en los volantes T150 y T300 de @TMThrustmaster. 


 Una vez más la comunidad es capaz de suplir el trabajo que tendrían que hacer las compañías, y al igual que en su día el tesón de unos usuarios hartos de esperar por el soporte que no llegaba a sus preciados volantes Logitech (drivers del kernel y [new-lg4ff]({{ "/posts/new-lg4ff-un-nuevo-driver-mucho-mas-completo-para-nuestros-volantes-logitech" | absolute_url }})); llega por fin la posibilidad de usar este tipo de periféricos de la conocidísima marca **Thrustmaster**. Vale, es cierto que **hace años que existe un driver que permite ciertas funcionalidades** en algunos modelos como es [tmdrv](https://gitlab.com/her0/tmdrv), pero estas se limitan a poco más que reconocer botones y ejes, por lo que aunque es posible su uso, **se pierde algo importantísimo como son las sensaciones que producen los tan imprescindibles efectos de Force Feedback**.


Yendo al grano vamos a comenzar con el volante más económico de los que aquí hablamos, el [T150 (t150_driver)](https://github.com/scarburato/t150_driver), que está desarrollado por [@scarburato](https://github.com/scarburato), y que a parte de permitir inicializar ejes y botones, tiene muchas más características interesantísimas como cambiar los **grados entre 270 y 1080º**, ajustar la **fuerza de retorno entre 0 y 100%**, ver la **versión de firmware**; y lo que es más importante, **activar algunos de los efectos de force feedback** como son **constant, periodic, damper o spring**, así como poder ajustar la **ganancia** o modificar la **escala global de force feedback entre 0 y 100%**. Por supuesto quedan aun algunas cosas por implementar, como son el resto de efectos de Force Feedback, la actualización del Firmware, o la lectura de los ajustes del volante.


 Continuamos ahora con el driver desarrollado para el **mejor volante en relación calidad precio de la marca**, y por supuesto un éxito de ventas, como es el [T300RS (hid-tmff2)](https://github.com/Kimplul/hid-tmff2), que en esta ocasión corre a cargo de [@Kimplul](https://github.com/Kimplul). Este **módulo del kernel** está actualmente en un **estado jugable**, tal y como podemos ver en la página del proyecto soporta **modificación del rango de grados**, así como el ajuste de la **ganancia**, el **autocentrado** y la **mayoría de los efectos de force feedback**. Además el creador del driver comenta que ha hecho una serie de **mejoras en la actualización dinámica de los efectos**, y aunque todavía está lejos de ser perfecta, la experiencia va mejorando poco a poco. Existen algunos inconvenientes, como pueden ser la falta de actualización dinámica de los efectos de rampa y en algunos juegos el mapeo inconsistente de los pedales, lo que significa que todos los pedales deben ser detectados en los juegos, pero pueden ser mapeados incorrectamente.


Personalmente pienso que estos proyectos, aunque son muy prometedores y debemos agradecer el esfuerzo a sus desarrolladores, **deberían fusionarse en un driver genérico que trate de englobar el mayor número posible de dispositivos de la marca**, al igual que en el caso de Logitech con por ejemplo new-lg4ff. De esta forma, además de aunar esfuerzos, sería mucho más cómodo para el usuario, además de facilitar las cosas para una posible y deseable inclusión en kernel.


Como veis, gracias a estos dos nuevos desarrollos, todos aquellos que amamos los juegos de conducción y el simracing estamos de enhorabuena, pues **estos proyectos nos permiten ampliar la oferta de dispositivos que podemos usar** para disfrutar de este tipo de títulos. Ahora solo queda que las desarrolladoras de se animen a portarlos a nuestro sistema para poder ser jugados de forma nativa y no tener que tirar tanto de Steam Play/Proton.


Queda por decir que **ambos proyectos son experimentales** y les queda bastante trabajo por delante, por lo que **sus creadores agradecerán todo el feedback que podais darle sobre su funcionamiento** para que con el tiempo vayan mejorando. Por supuesto, **en JugandoEnLinux.com nos encantaría que si algúno de vosotros tiene alguno de estos volantes, los pruebe y nos diga que tal le ha ido**, si la experiencia es buena, o por ejemplo si le ha funcionado bien en tal o cual juego. Para ello podeis dejarnos vuestras impresiones sobre estos drivers en los comentarios, o en mensajes en la sala "[Zona Racing](https://matrix.to/#/!zkLPJYaRgUPeCoUtzk:matrix.org?via=matrix.org&via=t2bot.io&via=privacytools.io)" de nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), y por supuesto en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

