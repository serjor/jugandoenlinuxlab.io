---
author: leillo1975
category: "Exploraci\xF3n"
date: 2019-08-21 08:27:15
excerpt: "<div class=\"css-1dbjc4n r-15d164r r-1g94qm0\">\r\n<div class=\"css-1dbjc4n\
  \ r-1wbh5a2 r-dnmrzs\" role=\"presentation\" aria-haspopup=\"false\">\r\n<div class=\"\
  css-1dbjc4n r-1wbh5a2 r-dnmrzs\">\r\n<div class=\"css-1dbjc4n r-18u37iz r-1wbh5a2\"\
  >\r\n<div class=\"css-1dbjc4n r-1adg3ll r-15d164r\"><span class=\"css-901oao css-16my406\
  \ r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\">El juego de @Rockfishgames entra en Acceso\
  \ Anticipado<br /></span></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/85eaadb12c96831e2dd8e8d0ba75e08b.webp
joomla_id: 1101
joomla_url: habra-everspace-2-y-tendra-soporte-para-linux-a4
layout: post
tags:
- acceso-anticipado
- exploracion
- mundo-abierto
- everspace
- rockfish-games
- everspace-2
title: "Habr\xE1 \"EVERSPACE 2\" y tendr\xE1 soporte para Linux (ACTUALIZADO 4)."
---




El juego de @Rockfishgames entra en Acceso Anticipado  





****ACTUALIZACIÓN 19-1-21:**** Como podeis ver hace muuucho tiempo que no hablábamos de este juego, y todo hay que decirlo, de forma totalmente injusta, pues el desarrollo y las noticias no se han detenido nunca por parte de la gente de [Rockfish Games](https://rockfishgames.com/), el estudio alemán que está detrás del juego, Pero obviamente con una noticia como nos ocupa sería casi delito no darle repercusión en nuestra web, y es que finalmente el juego ya se encuentra en "Early Access" desde ayer, y ya es posible jugarlo. El anuncio nos llegaba el Lunes por la tarde a través de nuestro correo electrónico.


Según podemos ver en la [noticia que publicaban en Steam](https://store.steampowered.com/app/1128920/EVERSPACE_2/), el juego se puede disfrutar, repetimos, en Acceso Anticipado, desde ayer a a las 7 de la tarde (CET). con un precio de salida de 37.99€. Este precio será más alto en el lanzamiento de la versión final del juego , que tendrá lugar en la primera mitad del próximo año.  La versión inicial del juego tendrá unas 25 horas iniciales de juego, de las cuales por el momento la mitad están dobladas al inglés con actores profesionales. En la versión completa podremos ver el título traducido a más idiomas. Durante ese tiempo podrás irte familiarizando con las mecánicas de combate espacial, exploración, minería, resolución de puzles, viajes, comercio, itemización, artesanía, personalización de naves, ventajas para el jugador y sus acompañantes. Pasarás todo ese tiempo en cinco subclases diferentes de naves para jugadores.


Además de los dos sistemas estelares que los pilotos podrán explorar en Early Access en el lanzamiento, planean tener de cuatro a seis más, así como varias subclases de naves de jugadores adicionales más allá del Interceptor, Centinela, Atacante, Cañonera y Explorador en el lanzamiento, que se irán añadiendo gradualmente cada trimestre. En el juego final, los jugadores también podrán disfrutar del doble de contenido de la historia, además de muchas más actividades finales.


No os olvideis de comentar que os parece este juego. Os dejamos con el video del lanzamiento del juego en Acceso Anticipado:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/jeBXVhuURKI" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


 


 


 




---


**ACTUALIZACIÓN 5-11-19:** Finalmente lo han logrado. Los chicos de Rockfish Games [anunciaron ayer mismo](https://www.kickstarter.com/projects/rockfishgames/everspace-2/posts/2673270) que el objetivo de conseguir **450.000€** para financiar la segunda parte de Everspace ha sido superado, siendo en este momento la cantidad recaudada de 503.478€, por lo que ahora ya pueden respirar tranquilos y preocuparse unicamente de desarrollar el mejor videojuego posible. A pesar de que todo parecía augurar que no conseguirían la cantidad demandada, finalmente lo han conseguido.


El equipo de desarrollo se plantea ahora **su próxima meta que es crear "Las grietas de los Antiguos"**, una zona especial a la que tendremos acceso cerca del final del juego y donde tras adentrarnos es estas anomalías espacio temporales podremos navegar por zonas peligrosísimas con implacables enemigos, pero que a cambio nos ofrecen suculentos botines. Si morimos tras adentrarnos en esta grieta, seremos expulsados de ella a la zona anterior, pero habiendo conseguido a cambio de un gran botin y bonificación en función de cuanto hayamos conseguido adentrarnos en esta zona desconocida del espacio. Os dejamos con uno de los últimos gameplays del juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://player.twitch.tv/?autoplay=false&amp;video=v503503786" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**ACTUALIZACIÓN 15-10-19:** *Uwe Wütherich*, director de Rockfish Games, el estudio responsable de Everspace 2 ha querído darnos más datos sobre el juego. En primer lugar nos ha anunciado y agradecido que **han superado la barrera de los 200.000€**, (ahora mismo 238.620€), por lo que la meta de 450.000€ está a medio camino. También nos ha aclarado que EVERSPACE 2 será un **juego de rol de mundo abierto** con una progresión persistente del jugador, la narración será muy diferente esta vez con respecto a la primera parte. En este video nos explica ciertos aspectos muy interesantes del juego:  



<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/uA8YM8dGDdc" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Como veis, la historia comenzará en la zona desmilitarizada (DMZ) del espacio, donde seremos un clon de Adam Roslin, el protagonista de Everspace 1, donde vivirá al margen de la ley hasta que es atrapado por unos piratas espaciales para cobrar la recompensa que hay por él. Durante la aventura conocerá a más personajes que se unirán a él en la aventura. En el video se vé que habrá cinemáticas con un estilo comic semejante al de la primera parte, pero en una cantidad mucho más grande.


Prometen más información proximamente, por lo que seguiremos atentos para manteneros bien informados.




---


**ACTUALIZACIÓN 2-10-19:** Hace más o menos un par de horas nos llegaba un correo por parte de **Rockfish Games** de que ya se puede colaborar en la financiación de la continuación de su aclamado EVERSPACE , pues desde hace un rato está disponible en su página de [Kickstarter](https://www.kickstarter.com/projects/rockfishgames/everspace-2). En ella se marcan como objetivo la **cantidad de 450.000€,** que como veis es una cantidad bastante ambiciosa. En este momento llevan algo más de 17.000€ y quedan 33 dias por delante. Si quereis colaborar o curiosear un poco por los detalles de esta campaña no dudeis en [pasaros por Kickstarter.](https://www.kickstarter.com/projects/rockfishgames/everspace-2)


![Everspace 2](/https://ksr-ugc.imgix.net/assets/026/676/196/9b9ffd6c8b964eaeaeffc3b22d749376_original.gif?ixlib=rb-2.1.webp)


 




---


**NOTICIA ORIGINAL:**


Hace ya casi dos años que nuestro compañero @Serjor nos descubría a muchos este juego con su fenomenal [análisis](index.php/homepage/analisis/20-analisis/605-analisis-everspace). El juego, aunque tardó en tener [soporte oficial](index.php/homepage/generos/accion/5-accion/851-el-genial-everspace-ya-es-oficialmente-un-juego-para-gnu-linux-steamos), disponía de una versión en pruebas para nuestro sistema que funcionaba bastante bien y nos permitía disfrutarlo plenamente. En el descubríamos un fantastico "Roguelike" ambientado en el espacio donde en cada "salto" llegábamos a un cuadrante del espacio diferente generado proceduralmente, y en el que teníamos que adquirir mejoras para nuestra nave, recolectando para ello recursos y comerciando, todo ello aderezado con una magnífica ambientación y unos gráficos en 3D sobresalientes. Sin duda el juego fué merecedor para nosotros de estar entre [los mejores juegos de 2017](index.php/homepage/editorial/4-editorial/713-los-mejores-de-2017).


Pero volviendo al tema que nos ocupa, recientemente hemos visto en los [foros de Steam](https://steamcommunity.com/app/1128920/discussions/0/3647273545692781907/), que gracias a una pregunta de [LinuxGameConsortium](https://linuxgameconsortium.com/), la continuación de este juego tendrá soporte para Linux/SteamOS, por lo que podremos jugar a Everspace 2 en nuestros ordenadores en un par de años (2021). pasando primero por un periodo de Acceso Anticipado. Lo que no se ha confirmado es si en este "Early Access" habrá una versión para nuestro sistema. A parte de esto, por lo que hemos podido saber, esta segunda parte está siendo desarrollada con el motor Unreal, al igual que su predecesora, lo cual es una garantía de calidad gráfica.


El juego estará mucho más centrado en la exploración espacial tal y como podemos ver en la descripción del juego que podemos encontrar en su [página web](https://www.everspace.game/):


*EVERSPACE™ 2 es un juego de disparos espaciales rápido para un jugador con exploración profunda, toneladas de botín y elementos clásicos de RPG. Experimenta una emocionante historia ambientada en un mundo abierto, vívido y artesanal, lleno de secretos y peligros en tu viaje para convertirte en un ser humano después de todo.*


Poca más información más os podemos ofrecer por ahora, pero permaneceremos atentos para ofreceros cualquier novedad reseñable, por lo que os recomendamos que permanezcais a atentos a las noticias de nuestra página y a nuestras cuentas en R.R.S.S. ([Mastodon](https://mastodon.social/@jugandoenlinux) y [Twitter](https://twitter.com/JugandoenLinux)). Mientras tanto, para que se os vaya abriendo el apetito os dejamos con el trailer del anuncio:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/I-LNiZxsY7s" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Si quereis haceros con la primera parte podeis adquirirla en la tienda de [Humble Bundle](https://www.humblebundle.com/store/everspace?partner=jugandoenlinux). ¿Habeis jugado ya a Everspace? ¿Qué os ha parecido la primera parte de este juego de Rockfish Games? ¿Qué espectativas teneis para su secuela? Cuéntanoslo en los comentarios o deja tus mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

