---
author: Serjor
category: Estrategia
date: 2017-12-19 22:00:46
excerpt: <p>Nuevos mapas, unidades y skins</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9e7fc301c24011e6d40ec6dd798bf290.webp
joomla_id: 580
joomla_url: warhammer-40-000-dawn-of-war-iii-se-actualiza
layout: post
tags:
- feral-interactive
- feral
- warhammer
- dawn-of-war
title: 'Warhammer 40,000: Dawn of War III se actualiza'
---
Nuevos mapas, unidades y skins

Nos llegaba hoy un aviso de la gente de Feral Interactive, sobre la nueva actualización del juego Warhammer 40000: Dawn of War III, y es que en esta [actualización](http://www.feralinteractive.com/es/news/838/) han incluido:


* Nuevos mapas para el modo multijugador
* Nuevas unidades de élite
* Nuevos skins


Quitando el tema de los skins, que nunca están de más los cambios estéticos, y más si son gratuitos, los nuevos mapas multijugador y las nuevas unidades pueden dar mucho juego, así que ya sabéis, si tenéis el juego, aseguraros de que se os actualiza para disfrutar de estas novedades.


 


Y tú, ¿ya has probado las novedades? Cuéntanos qué te parecen en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

