---
author: Pato
category: Rol
date: 2017-04-06 19:10:26
excerpt: <p>El juego tiene disponible una demo para Linux</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/bbdd9bab523659f72e79235cef0a8565.webp
joomla_id: 280
joomla_url: hellpoint-un-llamativo-rpg-espacial-con-tematica-oscura-esta-en-campana-en-kickstarter
layout: post
tags:
- indie
- multijugador
- rol
- kickstarter
title: "'Hellpoint' un llamativo RPG espacial con tem\xE1tica \"oscura\" est\xE1 en\
  \ campa\xF1a en Kickstarter"
---
El juego tiene disponible una demo para Linux

'Hellpoint' [[web oficial](http://www.cradlegames.com/index.php/hellpoint)] es de esos juegos que no pasan desapercibidos. El estudio indie canadiense "Cradle Games" compuesto por algunos veteranos de la industria que han participado en proyectos como algunos Assassin's Creed, Call of Duty, Prince of Persia o Skylanders nos presenta un juego de rol y acción ambientado en una nave espacial llamada Irid Novo que se encuentra orbitando alrededor de un agujero negro supermasivo y donde se ha desencadenado un "cataclismo" llamado The Merge. 


![merge](https://ksr-ugc.imgix.net/assets/016/124/554/5808b341ac7e182a3baa4944d39dee80_original.png)


"*En tan solo una fracción de segundo todo ser vivo perdió la cordura y la memoria, y sus cuerpos fueron invadidos por versiones de ellos mismos provenientes de otras dimensiones. El accidente también atrajo a otros seres de inmenso poder que deberían estar solos en otras dimensiones.*"


Con esta premisa, quién no se apunta a saber que ha podido pasar?


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/WHy9rW9zQBw" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


En 'Hellpoint' jugaremos como un personaje sin nombre que ha sido impreso en 3D por "The Autority" con la orden de terminar lo que quedó sin hacer, y cuyas intenciones estarán determinadas por tus propias decisiones. Dado su desarrollo y ambientación, no ha faltado quien comparase el título con un "Dark Souls" ambientado en el espacio.


En cuanto a su desarrollo, la nave orbitará en tiempo real alrededor del agujero negro y su posición en el espacio determinará el comportamiento de los habitantes de la nave. Dependiendo del momento de la órbita los enemigos de ciertos niveles se volverán locos o quedarán paralizados por el miedo, otros seres deambularán por los corredores o se desencadenarán distintos eventos.


![merge2](https://ksr-ugc.imgix.net/assets/016/132/324/feadff1ff639181b74823d2b7677bdbf_original.jpg)


En cuanto a las novedades, 'Hellpoint' contará con un modo de juego multijugador a pantalla partida con modo cooperativo o jugador contra jugador. Además, un amigo podrá unirse a la partida o abandonarla cuando desee, compartiendo los logros que se vayan obteniendo y guardando el avance para mas adelante. Respecto al combate, será una mezcla de estrategia y lucha visceral donde encontrar y manejar las armas que vayamos consiguiendo será vital para poder avanzar. Muy al estilo "Dark Souls" vamos.


Si te atrae lo que ves, además ¡lo puedes probar!. Si, Cradle Games ha publicado [**una demo del juego en Linux**](http://www.cradlegames.com/Demo/Hellpoint_Demo_Linux_v016b.zip) que puedes descargar desde su  web de Kickstarter, y donde por solo 16,80€ (la propuesta más baja) podreis aportar al proyecto y conseguir el juego en formato digital en Steam o en GOG sin DRM:


<div class="resp-iframe"><iframe height="360" src="https://www.kickstarter.com/projects/hellpoint/hellpoint-a-dark-sci-fi-rpg/widget/video.html" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


¿Qué te parece la propuesta de 'Hellpoint'? ¿Piensas aportar? ¿Has probado la demo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

