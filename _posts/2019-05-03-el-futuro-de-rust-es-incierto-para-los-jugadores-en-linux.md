---
author: Pato
category: "Acci\xF3n"
date: 2019-05-03 14:39:35
excerpt: <p>El soporte de software de terceros puede determinar el futuro del juego
  para nuestro sistema</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/af08ba4bce655d5aaad3672b255643fe.webp
joomla_id: 1039
joomla_url: el-futuro-de-rust-es-incierto-para-los-jugadores-en-linux
layout: post
tags:
- accion
- multijugador
- estrategia
- survival
title: El futuro de Rust es incierto para los jugadores en Linux
---
El soporte de software de terceros puede determinar el futuro del juego para nuestro sistema

Según leemos en gamingonlinux.com la [última actualización en el blog de Rust](https://rust.facepunch.com/blog/smol-update), el ya veterano juego de supervivencia que tan buenos momentos dio a los jugadores de Linux en el pasado trae malas noticias para los que aun lo juegan en nuestro sistema favorito.


Según se puede leer en la entrada del blog:


*"Esta actualización trae un nuevo conjunto de correcciones para el cliente de Linux que debería resolver algunos de los problemas que se han reportado. Desafortunadamente, también tengo que aprovechar este momento para abordar el futuro de Rust en Linux. Actualmente estamos debatiendo internamente si finalizará o no la compatibilidad con Linux en un futuro próximo. Hay muchas razones para esto, pero el mayor problema ahora es el estado problemático del soporte de Linux por parte de terceros. Cualquier software que sea compatible con Linux se enfrenta al mismo problema de poner mucho esfuerzo para una base de clientes extremadamente pequeña, por lo que simpatizamos con las decisiones que nuestros socios han estado tomando. Desafortunadamente, esto significa que seguimos encontrando problemas con Rust en Linux que no podemos resolver directamente y necesitamos esperar a las correcciones, que pueden demorar meses o, en algunos casos, nunca materializarse. Aún no hemos tomado la decisión de continuar o no con el soporte de Linux, pero quisimos comunicar este proceso con anticipación para que la comunidad lo sepa."*


Rust es un juego de supervivencia en el que empiezas desnudo y solo en una misteriosa isla, y en la que tendrás que buscarte la vida recolectando materiales y construyendo para sobrevivir al resto de habitantes y criaturas que pueblan a tu alrededor.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/HNavTaDpXXU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Es cierto que el juego siempre ha pasado por altibajos en cuanto a soporte en Linux, ya que hace meses que dejó de aparecer el logo de soporte para Linux/SteamOS, y cada actualización era una lotería pudiendo dejar de funcionar en cualquier momento o tener problemas de toda índole. Pero al menos, hasta ahora el soporte se había mantenido aunque fuera de forma "extraoficial".


Ya veremos en que queda todo, ya que perder el soporte de un juego por no tener soporte de un software de terceros está comenzando a ser una tendencia preocupante para los que jugamos en Linux.


Si quieres saber mas sobre Rust, puedes visitar su [página web oficial](https://rust.facepunch.com/) o su [página de Steam](https://store.steampowered.com/app/252490?snr=2_groupannouncements_detail__apphubheader) donde aunque no se muestra el icono de Linux/SteamOS aún se puede comprar y jugar en Linux, aunque no sabemos por cuanto tiempo.


¿Que pensáis sobre Rust? ¿Creeis que será una gran pérdida para el juego en Linux?


Cuéntamelo en los comentarios o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux).

