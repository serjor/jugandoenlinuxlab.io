---
author: Pato
category: "Acci\xF3n"
date: 2018-05-10 17:46:40
excerpt: "<p>Se trata de la secuela del primer ANIMA, aunque no estar\xE1 disponible\
  \ hasta despu\xE9s de su lanzamiento en otros sistemas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8212207f146778f83156ffc204ca380c.webp
joomla_id: 731
joomla_url: anima-gate-of-memories-the-nameless-chronicles-tambien-llegara-a-linux-steamos
layout: post
tags:
- accion
- aventura
- proximamente
- rol
title: "'ANIMA GATE OF MEMORIES: THE NAMELESS CHRONICLES' tambi\xE9n llegar\xE1 a\
  \ Linux/SteamOS"
---
Se trata de la secuela del primer ANIMA, aunque no estará disponible hasta después de su lanzamiento en otros sistemas

Hace tiempo que seguimos los pasos de "Anima Project Studios", creadores de uno de los pocos juegos decentes de género JRPG de acción 3D en tercera persona que disponemos en Linux: '[Anima Gate Of Memories](https://store.steampowered.com/app/380750/Anima_Gate_of_Memories/)' del que os haremos una review dentro de poco. Hace bien poco nos han llegado noticias de que estaban preparando un nuevo juego basado en el universo Anima titulado **Anima The Nameless Chronicles**, y curioseando hemos encontrado su página en Steam donde están preparando su lanzamiento, pero no aparecen los requisitos para Linux/SteamOS y ni cortos ni perezosos les hemos preguntado por su intención de lanzar el nuevo juego para nuestro sistema favorito, a lo que nos han contestado:


"*Si, también apuntamos hacia una versión Linux, pero (Siempre hay un PERO ^_^¡) no estará lista para el lanzamiento de día 1. Lleva mucho tiempo hacer una buena conversión / port, por lo que la añadiremos en un futuro cercano (si todo va bien, 2 a 4 meses)"*


[Aquí](https://steamcommunity.com/app/850060/discussions/0/1696046342875256537/#c1696046976475812697) puedes ver la respuesta en los foros de Steam.


"*Anima The Nameless Chronicles*  *es un juego ARPG en tercera persona que cuenta la historia de un hombre sin nombre, un inmortal condenado a caminar por el mundo hasta el fin de los tiempos.*


*Cuando las tinieblas de su pasado se vuelven a levantar, se verá obligado a tomar parte en el conflicto en el que la misma existencia está en cuestión*".


Por lo visto, según otro comentario en el foro esta vez no tendremos una protagonista femenina como caracter principal, aunque nos emplazan al vídeo promocional que debería hacerse público en unos días.


Eso si, ya podéis ver [en su página de Steam](https://store.steampowered.com/app/850060/ANIMA_GATE_OF_MEMORIES_THE_NAMELESS_CHRONICLES/) algunas imágenes y gifs animados del juego para ver algunos movimientos, y donde apuntan **el lanzamiento oficial a Junio de este mismo año, con un lanzamiento posterior para Linux/SteamOS** como ya hemos comentado.


Seguiremos atentos a las novedades sobre este Anima The Nameless Chronicles.

