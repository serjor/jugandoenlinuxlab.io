---
author: Pato
category: Editorial
date: 2018-04-04 07:50:06
excerpt: "<p>Afirman adem\xE1s tener nuevas iniciativas en Linux aunque no las desvelan</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3f51ec9d49bc461803b454a4875d5aa8.webp
joomla_id: 703
joomla_url: valve-reafirma-su-compromiso-con-linux-y-steamos
layout: post
tags:
- steamos
- valve
- linux
title: Valve reafirma su compromiso con Linux y SteamOS
---
Afirman además tener nuevas iniciativas en Linux aunque no las desvelan

Ante la polvareda que se estaba levantando en los medios tras la aparente retirada de la sección de las Steam Machines de la portada de Steam, (la sección sigue activa y se puede visitar sin problemas) Valve ha salido al paso en un comunicado por boca de uno de los máximos responsables de los desarrollos para Linux/SteamOS de la empresa con sede en Washington.


En concreto, Pierre-Loup Griffais ha publicado un post en el foro de desarrollo de Steam para Linux explicando la situación actual del desarrollo en Linux/SteamOS y los motivos que han llevado a la empresa a retirar de la portada la sección de las Steam Machines. Reproducimos el mensaje traducido íntegro, pues pensamos que es de importancia:



> 
> *Nos hemos dado cuenta de que con motivo de una limpieza rutinaria en la navegación de la tienda de Steam se ha pasado a una historia sobre la retirada de las Steam Machines. Esa sección de la tienda aún está disponible, pero fue retirada de la barra principal de navegación basándonos en el tráfico de usuarios. Dado que este cambio ha levantado mucho interés, hemos pensado que tiene sentido abordar algunos de los puntos que hemos visto que la gente ha ido sacando en consecuencia.*
> 
> 
> *Mientras que es cierto que las Steam Machines no están ciertamente volando de las estanterías, nuestras razones para luchar por una plataforma de juego competitiva y abierta no han cambiado significativamente. Seguimos trabajando duro para hacer de los sistemas operativos Linux un gran lugar para los juegos y las aplicaciones. Creemos que, en última instancia, resultará en una mejor experiencia tanto para los desarrolladores como para los clientes, incluidos los que no están en Steam.  
>   
> A través de la iniciativa Steam Machine, hemos aprendido bastante sobre el estado del ecosistema Linux para los desarrolladores de juegos. Hemos recibido muchos comentarios y nos hemos centrado en abordar las deficiencias que observamos. Creemos que una parte importante de ese esfuerzo es nuestra continua inversión en hacer de Vulkan una API de gráficos competitiva y bien soportada, así como asegurarnos de que tiene un soporte de primera clase en plataformas Linux.*
> 
> 
> *Recientemente anunciamos la disponibilidad de Vulkan para macOS e iOS, que se suma a la disponibilidad existente para Windows y Linux. También lanzamos Steam Shader Pre-Caching, que permitirá a los usuarios de aplicaciones basadas en Vulkan omitir la compilación de shaders en su máquina local, mejorando significativamente los tiempos de carga iniciales y reduciendo el tiempo de ejecución total en comparación con otras API. Hablaremos más sobre el Shader Pre-Caching en los próximos meses a medida que el sistema madure.*
> 
> 
> *Al mismo tiempo, continuamos invirtiendo importantes recursos en el apoyo al ecosistema de Vulkan, las herramientas y los esfuerzos en los drivers. **También tenemos otras iniciativas de Linux de las que aún no estamos listos para hablar; SteamOS seguirá siendo nuestro medio para ofrecer estas mejoras a nuestros clientes, y creemos que en última instancia beneficiarán al ecosistema Linux en general.***Puedes ver el mensaje original [en este enlace](http://steamcommunity.com/app/221410/discussions/0/1696043806550421224/).
> 
> 
> 


Como ya dijimos [en su momento](index.php/homepage/editorial/item/626-esta-creciendo-lo-suficiente-el-juego-en-linux), Valve sigue necesitando un sistema que le garantice seguir adelante pase lo que pase a medio o largo plazo. Nunca es bueno depender de un solo sistema ni un solo proveedor, aunque si que es cierto que aún quedan muchos pasos por dar antes de que Linux se convierta en un sistema de "masas" para jugar. Sin embargo la afirmación de que las razones para luchar por una plataforma libre y abierta siguen estando presentes para Valve es una gran noticia. Esperamos ver las novedades de lo que tienen en la recámara para nuestro sistema favorito.


Y tu ¿Que piensas de la evolución de SteamOS y las Steam Machines? ¿Piensas que Valve va por buen camino en su apuesta por Linux y Vulkan?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

