---
author: Serjor
category: Estrategia
date: 2019-10-18 14:52:17
excerpt: "<p>Los erizos peleones llegan a su primera versi\xF3n estable</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f0045bbd92708e1770925e94b8398442.webp
joomla_id: 1123
joomla_url: hedgewars-llega-a-la-version-1-0
layout: post
tags:
- open-source
- codigo-abierto
- hedgewars
title: "HedgeWars llega a la versi\xF3n 1.0"
---
Los erizos peleones llegan a su primera versión estable

Vía [LinuxGameCast](https://linuxgamecast.com/2019/10/hedgewars-1-0-linux-multiplayer/) nos enteramos de que el juego open source Hedgewars ha [llegado](https://www.hedgewars.org/node/9014) a la versión 1.0.


Para los que no os suene este juego, Hedgewars es un clon open source del famoso juego Worms, aunque en esta ocasión en vez de controlar unos gusanos con ansias de dar guerra de la forma más hilarante posible, usaremos una armada de ~~Sonics~~ erizos con las mismas ganas de peleas divertidas.


La [lista de cambios](https://hg.hedgewars.org/hedgewars/raw-file/1.0.0-release/ChangeLog.txt) entre la versión 0.9.25 anterior, de la que ya os [hablamos](index.php/homepage/generos/accion/5-accion/1042-hedgewars-alcanza-la-version-0-9-25) hace casi un año, y la 1.0 es bastante larga, como para anotarla en un artículo, lo cuál demuestra que el objetivo de la versión 1.0 es llegar con la mayor calidad posible, tanto en corrección de errores, como opciones y jugabilidad.


Os dejamos un un trailer del juego, que a pesar de ser de una versión anterior, merece la pena verlo, ni que sea para hacernos una idea del humor bélico de este juego.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/u1Y_Pw_TtFo" width="560"></iframe></div>


Y tú, ¿ya has probado esta versión 1.0 de Hedgewars? Cuéntanos qué te ha parecido en los comentarios o en nuestros canales de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux)

