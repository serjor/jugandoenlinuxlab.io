---
author: Pato
category: Apt-Get Update
date: 2017-03-31 17:28:02
excerpt: <p>De nuevo llegamos a Viernes, y de nuevo toca repaso semanal</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/18bdd04e288fc232234be2fb5ea8bf38.webp
joomla_id: 274
joomla_url: apt-get-update-beat-cop-planetscape-torment-88-heroes-meganoid
layout: post
title: Apt-Get Update Beat Cop & Planetscape Torment & 88 Heroes & Meganoid ...
---
De nuevo llegamos a Viernes, y de nuevo toca repaso semanal

Volvemos a las andadas. Vamos a repasar lo que dió de si la semana. ¡Comenzamos!


Desde [www.gamingonlinux.com](http://www.gamingonlinux.com):


- 'Planetscape Torment: Enhaced Edition' Ha sido anunciado con soporte en Linux. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/planescape-torment-enhanced-edition-officially-announced-with-linux-support.9407).


- 'Mantis Burn Racing' es un nuevo juego de carreras arcade en vista cenital con una pinta excelente, y los desarrolladores estarían dispuestos a portarlo si hay suficiente interés. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/mantis-burn-racing-an-awesome-looking-top-down-racer-could-come-to-linux-with-enough-support.9416) y mostrar tu interés [en este otro](http://steamcommunity.com/app/446100/discussions/0/135512625250997449/?tscn=1490694477#c135512625254972522).


- Virtual Programming ha publicado 'Putty Squad' para Linux, y Liam nos lo repasa [en este enalce](https://www.gamingonlinux.com/articles/putty-squad-from-virtual-programming-officially-released-for-linux-some-thoughts.9420).


Desde [www.linuxgameconsortium.com](http://www.linuxgameconsortium.com):


- 'Beat Cop' un juego de aventuras no lineal y diseñado en píxeles ya está disponible en Linux. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/beat-cop-officially-available-now-steam-gog-humble-50995/).


- 'Hollow Knight' un clásico plataformas lateral está ya siendo testeado para su salida en Linux. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/hollow-knight-2d-platformer-testing-linux-release-50710/).


- '88 Heroes' un plataformas de acción ya está disponible también para Linux. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/88-heroes-release-now-available-steam-finally-50746/).


- Warlocks 2: God Slayers es un interesante Hack & Slash que está buscando apoyos en Greenlight. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/warlocks-2-god-slayers-hack-n-slash-now-on-greenlight-50676/).


- 'Meganoid' es otro plataformas con un buen nivel de dificultad que ya está disponible para Linux. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/meganoid-challenging-roguelike-platformer-now-steam-51032/).


Vamos con los vídeos de la semana:


Planetscape Torment


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/G2wXLCIpFRg" width="640"></iframe></div>


 Mantis Burn Racing


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/IaGQUfpo1gA" width="640"></iframe></div>


 88 Heroes


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/_iqRmvypAiw" width="640"></iframe></div>


 Warlocks 2: God Slayers


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/Kn74LeEj_gk" width="640"></iframe></div>


 Beat Cop


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/ldbkBDzC2m4" width="640"></iframe></div>


 Meganoid


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/643dHvxO4RY" width="640"></iframe></div>


 


Esto es todo por esta semana. ¿Qué te ha parecido?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD). 

