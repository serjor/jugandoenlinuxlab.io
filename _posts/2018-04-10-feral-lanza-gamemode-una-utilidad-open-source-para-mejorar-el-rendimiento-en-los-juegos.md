---
author: leillo1975
category: Software
date: 2018-04-10 13:06:35
excerpt: "<p>@feralgames lo integrar\xE1 en \"Rise of Tomb Raider\"</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e8dcebfd55c9d5d7bbfc0a0f8263a6f3.webp
joomla_id: 708
joomla_url: feral-lanza-gamemode-una-utilidad-open-source-para-mejorar-el-rendimiento-en-los-juegos
layout: post
tags:
- feral-interactive
- open-source
- gamemode
- utilidad
title: Feral lanza "GameMode", una utilidad open source para mejorar el rendimiento
  en los juegos
---
@feralgames lo integrará en "Rise of Tomb Raider"

Esta mañana recibíamos un correo electrónico por parte del departamento de prensa de [Feral Interactive](http://www.feralinteractive.com/es/), y nada más empezar a leerlo ya se nos hicieron los ojos chiribitas.  La conocida compañía británica de ports **acaba de liberar una herramienta que permite optimizar el uso de nuestro equipo para la ejecución de juegos**.  Buscando un poco más de información sobre la herramienta que la facilitada por Feral, encontramos una pequeña entrevista realizada por [GamingOnLinux](https://www.gamingonlinux.com/articles/feral-interactive-have-released-an-open-source-tool-thatll-help-get-the-most-performance-out-of-linux-games.11555) a sus desarrolladores y de ella pudimos aclarar un poco más sobre ella.


"GameMode", es un **conjunto de demonio (servicio) y librerias** que lo que hacen basicamente es **cambiar el gobernador del kernel para conseguir el máximo rendimiento** de este este cuando ejecutamos un juego. Por defecto los sistemas vienen prefijados a "Ondemand" o "Powersave", lo que puede provocar cierta lentitud e inestabilidad en el rendimiento. Si nosotros queremos asegurarnos de que nuestro juego aproveche el máximo potencial que puede ofrecernos nuestro sistema y sea lo más estable posible, este gobernador debería estar en "Performance", lo cual hará que nuestro equipo consuma más energía si lo mantenemos permanentemente así. Además debemos darle privilegios de superusuario a estas acciones lo cual entraña siempre un riesgo. Si queremos saber que gobernador estamos usando en cualquier momento podemos usar este comando:


*`cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor`*


Gracias a esta herramienta no es necesario modificar este gobernador constantemente para adecuarnos a la situación que requiera, si no que **el modo de máximo rendimiento se ejecutará automáticamente** cuando arranquemos un juego, y se desactivará al dejar de usar este. Si queremos instalar esta herramienta, por cierto **Open Source** (BSD3),  tan solo tenemos que ir a su página de [GitHub](https://github.com/FeralInteractive/gamemode) y seguir las instrucciones facilitadas.


Para los próximos juegos que lanzará Feral, como el esperado "**Rise of Tomb Raider", vendrá incluido**, por lo que se espera que se ejecute automáticamente si está instalado. Pero también podremos hacer uso de él en otros juegos. Para ello simplemente debemos arrancar el juego con la siguiente orden:


`LD_PRELOAD=/usr/\$LIB/libgamemodeauto.so ./game`


Si nuestro juego está en Steam tendremos que configurar sus parámetros de arranque tal que así:


``LD_PRELOAD=$LD_PRELOAD:/usr/\$LIB/libgamemodeauto.so %command%``


Como podeis observar, una vez instalado, su uso es muy sencillo. Ahora solo tenemos que ver la ventaja que nos va suponer el usarlo. Si es cierto lo que promete podemos estar ante un antes y un después en los juegos para Linux. A veces simplemente un 10% más puede marcar la diferencia. También habrá que seguir el proyecto para ver como evoluciona con el tiempo y si poco a poco va añadiendo nuevas caracteristicas y funcionalidades, especialmente gracias a las aportaciones de la comunidad. Como apunte, estaría bien que por parte, al menos de Feral Interactive, incluyeran el GameMode en su catálogo de juegos actualizándolos, para así evitar el tener que andar tocando los parámetros de arranque.


Hemos realizado algunas pruebas de juegos y hemos obtenido los siguientes resultados:




  |  | **Company of Heroes 2** | **DIRT Rally** | **Shadow Of Mordor** |
|  | **Normal** | **Con GameMode** | **Normal** | **Con GameMode** | **Normal** | **Con GameMode** |
| **Mínimo** | 25,9 | *13,74* | 53,93 | *62,65* | 39,53 | *39* |
| **Máximo** | 53,31 | *56,66* | 94,27 | *93,97* | 79,06 | *82,19* |
| **Media** | 35,27 | *34,39 (-2.49%)* | 68,78 | *77,53 (+12.72%)* | 55,92 | *58,09 (+3.88%)* |


 




  |  | **F1 2015** | **F1 2017** | **DIRT Showdown** |
|  | **Normal** | **Con GameMode** | **Normal** | **Con GameMode** | **Normal** | **Con GameMode** |
| **Mínimo** | 29,84 | *38,03* | 35 | *36* | 56,96 | *37,42* |
| **Máximo** | 60,59 | *70,95* | 49 | *75* |  |  |
| **Media** | 46,49 | *60,41 (+29.94)* | 41 | *52 (+26.82%)* | 65,78 | *58,71 (-10.74%)* |


Como podeis ver, los juegos que comparten el motor EGO de Codemasters, a excepción DIRT Showdown, que fué portado por Virtual Programming, ganan bastantes frames por segundo. En cambio en el resto las diferencias no parecen significativas. Iremos realizando más pruebas con otros juegos que iremos publicando.


Otra cosa que es importante decir es que GameMode no solo es útil en juegos, sinó que puede ayudar bastante en apliaciones que requieran gran cantidad de recursos del ordenador. Por poner un ejemplo, un editor de video se vería muy beneficiado de este software tal y como podemos ver en este video:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/xeCVfmtDDSY" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Antes de que se nos olvide, **es agradable ver como empresas como Feral colaboran con la comunidad del Software Libre** aportando su granito de arena para que todo esto tire para adelante. Como sabeis, a parte de este nuevo proyecto, Feral lleva tiempo colaborando con el desarrollo de Mesa y los drivers libres tanto de OpenGL. como Vulkan. Cierto es que redunda en su propio beneficio, pero también en el de todos, y eso es algo que es importante reconocer.


¿Qué os parece "GameMode"? ¿Ya lo habeis probado? ¿Habeis notado diferencia? Podeis dejar vuestras impresiones y pruebas en los comentarios o usar nuestros grupos de [Telegram](https://t.me/jugandoenlinux) y [Discord](https://discord.gg/SUEPH).


 


 

