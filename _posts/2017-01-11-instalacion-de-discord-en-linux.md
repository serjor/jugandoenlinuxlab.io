---
author: leillo1975
category: Software
date: 2017-01-11 08:54:00
excerpt: "<p>La popular aplicaci\xF3n VoIP para jugadores dispone de una versi\xF3\
  n experimental para GNU/Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2e2c1711fe12b24ae23d95c35bfd21c2.webp
joomla_id: 167
joomla_url: instalacion-de-discord-en-linux
layout: post
tags:
- discord
- chat
- voip
title: "Instalaci\xF3n de Discord en Linux (ACTUALIZADO)"
---
La popular aplicación VoIP para jugadores dispone de una versión experimental para GNU/Linux

Dando una vuelta por mis blogs favoritos como suelo hacer a diario, me encontré un artículo en [DESDELINUX](HTTP://BLOG.DESDELINUX.NET/INSTALAR-DISCORD-LINUX/) (por cierto, Blog muy recomendado) sobre un programa que para todos los que disfrutan de los juegos multijugador será de gran utilidad, ya que la comunicación es algo tremendamente importante. Existen diferentes aplicaciones que tienen como misión facilitarnos esa tarea. Una de las que más está creciendo es DISCORD, que poco a poco se está haciendo con una buena comunidad de usuarios debido a lo bien implementada que está, además de ser totalmente **gratuita y multiplataforma**. Discord posee cliente para Window$, MacOS, IOS, Android y navegadores; y direis, ¿y Linux?. Pues para GNU/Linux existe una **versión en desarrollo y experimental** (Discord Canary) sin soporte pero por lo que he probado es bastante funcional.


 


En este artículo os voy a enseñar a instalarlo en **Debian, Ubuntu y derivadas**. Si quereis instalarlo en otros sistemas podeis consultar el [artículo original de DESDELINUX](http://blog.desdelinux.net/instalar-discord-linux/). Simplemente pinchad en este **[enlace](https://discordapp.com/api/download/canary?platform=linux)**. Esto os descargará un archivo .deb que luego podreis instalar con vuestro gestor de paquetes favorito o abriendo un terminal en la carpeta donde está el archivo y ejecutando **"sudo dpkg -i *nombredelpaquete.deb*"**.


 


Si sois usuarios de esta aplicación seguro que os interesa echarle un ojo al menos para probar si funciona correctamente. Estaría bien que dejasesis vuestras impresiones en los comentarios. Otra cosa, si quereis entrar en el **Servidor de Discord de JugandoEnLinux.com** entrad en el siguiente enlace:


 


**<https://discord.gg/fgQubVY>**


 


 


Saludos!


 


**ACTUALIZACIÓN:** En el dia de hoy (11-1-17) se ha anunciado la liberación oficial de una versión de pruebas (Public Test Build) para Linux.



> 
> Stoked to announce our super sick app for LINUX. Chris was massaging this for ages but it's like super sick now <https://t.co/hQtQpZO95c> [pic.twitter.com/lVyDkBD3cN](https://t.co/lVyDkBD3cN)
> 
> 
> — Discord (@discordapp) [January 11, 2017](https://twitter.com/discordapp/status/818978061903241218)



 


A partir de ahora se podrá descargar de forma normal desde la **[sección de descargas de la página oficial](https://discordapp.com/download)**


 


 

