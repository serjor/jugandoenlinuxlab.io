---
author: Pato
category: Noticia
date: 2019-12-11 13:28:46
excerpt: "<p>Philip Rebohle dice que el soporte de su software se ha convertido en\
  \ una pesadilla</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DXVK-valve-Proton.webp
joomla_id: 1138
joomla_url: dxvk-entra-en-mantenimiento-a-partir-de-ahora-solo-obtendra-parches
layout: post
title: "DXVK entra en mantenimiento. A partir de ahora solo obtendr\xE1 parches"
---
Philip Rebohle dice que el soporte de su software se ha convertido en una pesadilla


Hoy nos encontramos con algo inesperado. Según nos llega desde [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=DXVK-Possible-Maintenance-Mode), el líder del proyecto **DXVK** que tantas alegrías nos ha traído al permitirnos jugar a juegos de Windows en nuestro sistema favorito a través de Wine/Proton y que además está patrocinado por la misma Valve ha anunciado que DXVK tan solo recibirá actualizaciones de mantenimiento y corrección de errores, por lo que según sus palabras ya **no recibirá ninguna nueva característica**.


El motivo que esgrime Rebohle para tomar esta decisión es que el código de DXVK *se ha convertido en una pesadilla de mantenimiento fragil, poco confiable y frustrante. La mayoría de las versiones 1.4.x introdujeron regresiones importantes que no puedo reproducir y, por lo tanto, no puedo depurar y arreglar ... Hacer cualquier tipo de actividad el desarrollo con este código desordenado y roto solo empeoraría esto [..]. Lo único que todavía planeo hacer es implementar algunas extensiones Vulkan útiles y eventualmente fusionar D9VK, el resto será solo corrección de errores*.


Podéis leer el comentario [en este enlace](https://github.com/doitsujin/dxvk/pull/1264#issuecomment-564253190).


Esperemos que esto no suponga un retroceso en cuanto a las posibilidades de juego en Linux, y que sea tan solo que Philip necesita un descanso, por otra parte merecido como apuntan desde varios hilos en reddit y otras redes.


Sea como sea, lo cierto es que de momento aún tenemos DXVK para rato, y además Codeweavers sigue desarrollando su propia capa **VKD3D** que será implementada en Wine y en la que el propio Rebohle [también está contribuyendo](https://www.phoronix.com/scan.php?page=news_item&px=DXVK-Philip-More-For-VKD3D).


Veremos en que acaba este proyecto.

