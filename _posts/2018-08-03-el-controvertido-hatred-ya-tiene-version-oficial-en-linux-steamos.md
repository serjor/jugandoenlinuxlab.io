---
author: Pato
category: "Acci\xF3n"
date: 2018-08-03 10:32:47
excerpt: <p>El juego de @DestCreat_Team hace gala de una violencia muy cruda gracias
  a la cual tuvo problemas en Steam</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a8d0ef5be4cf3931cfa015ddb11dde9d.webp
joomla_id: 817
joomla_url: el-controvertido-hatred-ya-tiene-version-oficial-en-linux-steamos
layout: post
tags:
- accion
- gore
- violento
title: "El controvertido 'Hatred' ya tiene versi\xF3n oficial en Linux/SteamOS"
---
El juego de @DestCreat_Team hace gala de una violencia muy cruda gracias a la cual tuvo problemas en Steam

Nos hacemos eco de la noticia hecha pública por [gamingonlinux.com](https://www.gamingonlinux.com/articles/violent-twin-stick-shooter-hatred-is-now-officially-out-for-linux.12268) de que '**Hatred**' [[web oficial](http://www.hatredgame.com/)], el polémico juego del estudio **Destructive Creations** lanzado en el 2015 y que tuvo problemas al ser retirado por Valve de Steam Greenlight por su violencia explicita, cruda (y según algunos gratuita) del que hace gala.


Tras su lanzamiento, el estudio lanzó una rama beta con la versión Linux del juego, pero ahora ya podemos disfrutar de una versión oficial. Eso sí, no es un juego para todos los estómagos precisamente.


Sinopsis:


*El odio llena todo tu cuerpo. Estás enfermo y cansado de la existencia inútil de la humanidad. Lo único que importa es tu arma y el puro Armageddon que quieres desatar.  
  
Saldrás a cazar, y limpiarás las afueras de Nueva York de todos los humanos con sangre fría. Dispararás, herirás, matarás y morirás. No hay reglas, no hay compasión ni piedad, no tiene sentido regresar. Ahora eres el señor de la vida y la muerte, y tienes el control total sobre la vida de la escoria humana sin valor.*


Advertimos que el vídeo es muy violento y no apto para almas sensibles. Míralo bajo tu propia responsabilidad:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/ytdEYapPXdY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


En cuanto a los requisitos mínimos, son:


MÍNIMO:  
SO: Ubuntu, Arch, Debian, Ubuntu Distro equivalente 64Bits  
Procesador: 2.6 GHz Intel® Core™ i5-750 o 3.2 GHz AMD Phenom™ II X4 955  
Memoria: 4 GB de RAM  
Gráficos: NVIDIA GeForce GTX 460 o AMD Radeon HD5850 (1 GB VRAM)  
Almacenamiento: 5 GB de espacio disponible  
  



Puedes encontrar 'Hatred' en su [página de Steam](https://store.steampowered.com/app/341940/).

