---
author: P_Vader
category: "V\xEDdeos JEL"
date: 2021-09-18 19:45:01
excerpt: "<p>Aprovechando que est\xE1 unos d\xEDas gratis en Steam, probamos el sucesor\
  \ de Shadows Tactics.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DesperadosIII/desperadosIII_1.webp
joomla_id: 1362
joomla_url: video-probamos-desperados-iii
layout: post
tags:
- sigilo
- tactico
- western
title: "V\xCDDEO: Probamos Desperados III"
---
Aprovechando que está unos días gratis en Steam, probamos el sucesor de Shadows Tactics.


Aprovechando que está unos días gratis en [Steam](https://store.steampowered.com/app/610370/Desperados_III/), probamos Desperados III:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/joCfvrJ_biQ" title="YouTube video player" width="560"></iframe></div>


 También puedes comprarlo en [Humble Bundle](https://www.humblebundle.com/store/agecheck/desperados-iii?partner=jugandoenlinux) (patrocinado)


 


 

