---
author: leillo1975
category: Deportes
date: 2017-03-23 21:16:34
excerpt: "<p>Aprovechad esta magnifica ocasi\xF3n para disfrutar de genial juego de\
  \ gesti\xF3n.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/011e88ef4a8328e08be9d913808b8290.webp
joomla_id: 262
joomla_url: football-manager-se-puede-jugar-gratis-este-fin-de-semana
layout: post
tags:
- football-manager
- sega
- sports-interactive
- free-weekend
title: Football Manager se puede jugar gratis este fin de semana
---
Aprovechad esta magnifica ocasión para disfrutar de genial juego de gestión.

Parece que SEGA está dispuesta a que conozcamos algunos de sus juegos este fin de semana. Hace unas horas informábamos de que Motorsport Manager se podía jugar gratis durante esta semana, y ahora nos acabamos de enterar por [Steam](http://steamcommunity.com/games/482730/announcements/detail/486793062806392047) y por Twitter que podremos hacer lo mismo con Football Manager:


 



> 
> [#FM17](https://twitter.com/hashtag/FM17?src=hash) is completely FREE TO PLAY this weekend on Steam! Full details – <https://t.co/JorThuHubG> [pic.twitter.com/jhVL926QDN](https://t.co/jhVL926QDN)
> 
> 
> — Football Manager (@FootballManager) [23 de marzo de 2017](https://twitter.com/FootballManager/status/844957217543598081)



 


 Si estais interesados en probar este título sabed que está disponible desde hoy a las 18:00 hasta el domingo a las 21:00 (Horario de España). Comentar que si os gusta también tendreis la oportunidad de compralo a mitad de precio (27.49€). Para los que no conozcan este juego, se trata de un software desarrollado por  [Sports Interactive](http://www.sigames.com/) y en él podreis gestionar un club de fútbol, desde los fichajes de jugadores, cuerpo técnico, mejora de instalaciones, publicidad, etc.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/nUXV5RI3gm8" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Si quereis descargar y/o comprar este título pasaos por Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/482730/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Descargarás Football Manager? ¿Ya lo tienes? ¿Qué te parece el juego? Puedes responder a estas preguntas o escribir lo que quieras en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

