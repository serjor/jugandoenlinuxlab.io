---
author: Pato
category: Plataformas
date: 2017-06-10 16:12:39
excerpt: "<p>Koji Igarashi publica tambi\xE9n una interesante entrevista con un \"\
  misterioso\" director t\xE9cnico donde puede verse m\xE1s contenido</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f2db05517411d6eb0e1fc32654b32d49.webp
joomla_id: 368
joomla_url: nuevo-trailer-de-bloodstained-ritual-of-the-night-para-el-e3
layout: post
tags:
- accion
- plataformas
- proximamente
title: 'Nuevo trailer de Bloodstained: Ritual of the Night para el E3'
---
Koji Igarashi publica también una interesante entrevista con un "misterioso" director técnico donde puede verse más contenido

A estas alturas creo que todos saben de mi debilidad por los títulos de la saga Castlevania. Siendo así, cuando Koji Igarashi lanzó su [Kickstarter](https://www.kickstarter.com/projects/iga/bloodstained-ritual-of-the-night) para financiar 'Bloodstained: Ritual of the Night' no me pude resistir. Además, anunciaron su lanzamiento para Linux... ¿Qué más se podía pedir?


Así que si, soy "backer" de 'Bloodstained' un juego que promete recuperar la esencia de aquel Castlevania: Simphony of the Night con el propio padre de la "criatura" a la cabeza. No en vano al estilo se le llama "Igavania".


Este fin de semana con la inminente llegada del E3 la editora 505 Games ha publicado un trailer para la ocasión, donde se pueden ver algunas escenas de la jugabilidad:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/kTVYb6xeUcU" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Y por otra parte, el propio Koji Igarashi en su última actualización respecto al desarrollo del juego ha publicado una entrevista hecha por él mismo en persona a un "misterioso" director técnico al que han llamado DICO, y durante la cual se pueden ver más escenas del juego en acción:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/m20wmdMC6ck" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


¿Qué te parece 'Bloodstained: Ritual of the Night'?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


Yo por mi parte estoy ansioso de que llegue el momento de echarle el pad.


 

