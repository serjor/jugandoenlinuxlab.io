---
author: leillo1975
category: "Simulaci\xF3n"
date: 2019-02-13 08:49:08
excerpt: "<p class=\"ProfileHeaderCard-screenname u-inlineBlock u-dir\" dir=\"ltr\"\
  ><span class=\"username u-dir\" dir=\"ltr\">Su creador, </span><span class=\"username\
  \ u-dir\" dir=\"ltr\">@JonDadley</span> <span class=\"username u-dir\" dir=\"ltr\"\
  ><b class=\"u-linkComplex-target\">, </b></span><span class=\"username u-dir\" dir=\"\
  ltr\">investigar\xE1 la posibilidad de portarlo.</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/52568c24e240b502dc9c33c846cdefde.webp
joomla_id: 968
joomla_url: transmission-un-simulador-ambientado-en-los-80-podria-llegar-a-linux
layout: post
tags:
- ets2
- transmission
- jon-dadley
- sea-green-games
- unity
title: "\"Transmission\", un simulador ambientado en los 80, podr\xEDa llegar a Linux"
---
Su creador, @JonDadley **,** investigará la posibilidad de portarlo.

Ayer, dándome el garbeo diario por esa magnífica web que es [RaceDepartment](https://www.racedepartment.com/threads/introducing-transmission-the-euro-truck-sim-for-cars-in-the-80s.164768/) , vi una noticia que inmediatamente capto mi atención. Se trata de "[Transmission](http://seagreengames.com/transmission)", un simulador al estilo del fantástico **Euro Truck Simulator 2**, pero en el que **conduciremos coches de los 80** para llevar paquetes de mensajeria a través de la noche. Por supuesto empezamos a buscar información sobre el juego y nos pusimos en contacto con su creador, [Jon Dadley](https://twitter.com/jondadley), de la británica [Sea Green Games](http://seagreengames.com/),  Enseguida obtuvimos respuesta a nuestras preguntas tal y como podeis ver en el **siguiente tweet**:



> 
> Yes I’m a huge fan of Euro Truck Simulator 2. I use Unity which I understand has good Linux support - I will have to investigate how good the engine support is and what work I will need to port the game to Linux.
> 
> 
> — Jon Dadley (@JonDadley) [12 de febrero de 2019](https://twitter.com/JonDadley/status/1095471095212359683?ref_src=twsrc%5Etfw)



 Como podeis ver, aunque solo sea una posibilidad, esta existe, y más teniendo en cuenta que **el juego está siendo desarrollado con Unity3D**, lo que facilita muchísimo las cosas. Pero también hay que ser francos, **se trata de un proyecto enorme llevado a cabo por una sola persona**, por lo que sería comprensible que al final no se pudiese llevar a cabo el dar soporte a nuestro sistema.


El juego, que se espera para el primer cuarto del año que viene, consta de las **siguientes características**:  
  



**Acerca de la transmisión**


Despierta a medianoche, sintoniza la radio en tu emisora favorita y conduce. Como mensajero de poca monta, conocerás a un elenco de personajes inusuales, llevarás sus paquetes por toda la región y aprenderás sus historias. Entregue los paquetes durante la noche hasta que las largas sombras del amanecer le llamen de vuelta a su cama.


Su objetivo es recoger los paquetes de los lugares y entregarlos a tiempo. Estamos en 1986, por lo que no hay navegador por satélite, dependerá de su mapa y de las señales de las calles para conocer las mejores rutas. Las entregas tardías significan menos dinero.


Al final de cada noche, usted regresa a su garaje. Completar las entregas y ganar dinero abre nuevos garajes y coches, permitiéndole viajar más lejos, completar entregas más grandes y construir un imperio de entrega.


  
**Características**


-Explora un mundo abierto de autopistas abiertas y líneas del horizonte brillantes  
 -Entrega paquetes para un elenco de personajes en misiones de cuentos o en misiones ilimitadas generadas por procedimientos.  
 -Actualica su coche de "fixer-upper" a una máquina "musculosa"  
 -Desbloquea nuevos garajes para realizar trabajos más largos y explorar más del mundo.  
 -Contrata y administra a los conductores para construir un imperio de negocios  
 -Transmite radio, MP3 y podcasts en el equipo de música de tu coche  
 -Fotografía tu viaje con una amplia gama de lentes, filtros y efectos  
 -Desactiva las misiones y los límites de tiempo con el modo Endless Night  
 -Deja que el coche conduzca con la función de control de crucero, para cuando sólo quieras disfrutar de las sensaciones.


Como ves un juego de lo más interesante y prometedor que le pone los dientes largos a cualquiera. Esperemos que finalmente lo podamos disfrutar en Linux. Personalmente he abierto un [tema en los foros de Steam](https://steamcommunity.com/app/509950/discussions/0/1781640738622206703/) preguntando sobre el soporte por si quereis mostrar vuestro interés. También es posible añadir el juego a vuestra lista de desados en Steam para que nuestro sistema salga en las estadisticas:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/509950/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


Cerramos este artículo con el video de adelanto del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/IhSDiBulrJM" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Estais alucinando con "Transmission"? ¿Qué os parece la ambientación y temática? Contesta en los comentarios o charla sobre el juego en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

