---
author: leillo1975
category: "Acci\xF3n"
date: 2018-03-28 18:29:23
excerpt: <p>Tenemos fecha oficial de lanzamiento.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/12ac80b6fdda073e46f27e2999f2dbe7.webp
joomla_id: 694
joomla_url: fox-n-forests-llegara-esta-primavera-a-nuestros-pc-s
layout: post
tags:
- retro
- fox-n-forests
- eurovideo
- bonus-level-entertaiment
title: "\"Fox n Forests\" llegar\xE1 esta primavera a nuestros PC's (ACTUALIZADO)"
---
Tenemos fecha oficial de lanzamiento.

**ACTUALIZACIÓN 24-4-18:** Acabamos de recibir la confirmación (via mail) por parte de [@**Stride_PR**](https://twitter.com/Stride_PR) (dpto. prensa) que finalmente el juego llegará a nuestra pantallas el proximo **17 de Mayo**, cumpliendo así el plazo marcado con anterioridad. Además nos han dejado un nuevo video para amenizarnos la corta espera que muestra la jugabilidad de este colorista juego retro:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/OR-VuDe1_nQ" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**NOTICIA ORIGINAL:** Acabamos de conocer por comunicación directa de [StridePR](http://www.stridepr.com/), que la desarrolladora alemana [Bonus Level Entertaiment](http://bonuslevelentertainment.com/) ,fundada por Rupert Ochsner and Holger Kuchling (anteriormente han trabajado en la saga Saints Row, Dead Island, Risen, Sacred y Metro); y la editora [EuroVideo](https://www.eurovideo.de/) ([The Dwarves](index.php/homepage/analisis/item/247-analisis-the-dwarves) o [Everspace](index.php/homepage/analisis/item/605-analisis-everspace)) están preparando la salida al mercado de "[Fox n Forests](http://foxnforests.com/)", un videojuego retro de acción en 2D que nos transporta a la época dorada de los 16 bits.


Tras una exitosa **campaña de Crowfounding** en [Kicktarter](https://www.kickstarter.com/projects/foxnforests/fox-n-forests), donde consiguieron sus pretensiones económicas para poder llevar a cabo el juego, y ser aprobados por Steam Greenlight; Fox n Forests nos transportará al pasado con sus **gráficos 16 bit**, el **scroll lateral Parallax**, la **música chiptune**, el "**mode 7**", o sus **códigos de trucos**. Combinará elementos de habilidad con rol, pudiendo disparar, cortar o saltar, así como buscar, recolectar y actualizar.


En el juego encarnaremos a **Rick the Fox** (como me recuerda esto a [Titus the Fox](https://youtu.be/cUxqvfqoBDw)....), un zorro que se enfrentará a una fuerza malvada que está jugando con las estaciones en los bosques de Maná, creando criaturas malvadas mitad animal, mitad planta y conspirando para crear una misteriosa e inquietante quinta estación. Para ello contará con su espada y su ballesta mágica, además del poder de cambiar de estación (modifica el tiempo, las plantas y los animales) para poder avanzar en la aventura.


Como veis argumentos interesantes no le faltan, y es perfecto para todos aquellos que amamos esos títulos que hace muchos años nos hicieron engancharnos a esto. El juego **se podrá adquirir esta primavera** en Steam, y sin DRM (suponemos que en GOG) por 19.99€ . Además de en Linux, se lanzará para Windows, Mac, PS4, XBOXOne y Switch. Os informaremos de su salida o cualquier información relaccionada en cuanto tengamos conocimiento de ella. Os dejamos con el último video que han publicado:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/tjyO0OzaZJA" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Os gustan los juegos retro? ¿Qué os parece la historia? Déjadnos vuestra opinión en los comentarios o charlad sobre él en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

