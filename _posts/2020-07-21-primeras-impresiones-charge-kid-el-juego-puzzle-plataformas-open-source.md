---
author: yodefuensa
category: Puzzles
date: 2020-07-21 07:54:44
excerpt: "<p>Nuestro colaborador Yodefuensa nos desvela otro de sus \xFAltimos descubrimientos</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ChargeKid/chargekid.webp
joomla_id: 1243
joomla_url: primeras-impresiones-charge-kid-el-juego-puzzle-plataformas-open-source
layout: post
title: 'Primeras impresiones: Charge Kid, el juego puzzle-plataformas Open Source'
---
Nuestro colaborador Yodefuensa nos desvela otro de sus últimos descubrimientos


 


Había leído en Reddit sobre este juego y desde que me enteré que era **Open Source** tuvo toda mi atención. 


Según nos indican Charge Kid es un juego donde cada salto es un rompecabezas. Puedes compararlo con Portal, donde debes encontrar usos inteligentes de la mecánica y la física para "resolver" el como llegar a la ubicación deseada. Tienes recursos limitados para hacer el salto de forma comparable a Celeste. Pero en lugar de centrarse en el plataformas de precisión, el juego se centra en el aspecto del rompecabezas. La parte difícil es descubrir cómo usar sus recursos para hacer cada salto en lugar de lograr la ejecución. Pero no te dejes engañar; **la ejecución también es difí­cil**, lo que significa que encontrar cada solución ¡Es aún más complicado!.


Y créeme que NO exageran cuando dicen que **es difícil**. Nos encontramos con unas **mecánicas sencillas** que consiguen retorcerse hasta cotas enfermizas. Cada nivel se vuelve un autentico desafío capaz de acabar con la paciencia de cualquiera. Pues a veces creemos tener la solución y hasta que no la ejecutamos como que queremos, no nos damos cuenta de que esa no es la manera. Charge Kid es sin duda uno de los juegos más difícil que he probado en los últimos años. 


Podemos encontrar Charge kid en [Steam](https://store.steampowered.com/app/1334300/Charge_Kid/) por el módico precio de 1.59€ con lo que los desarrolladores verán compensado su esfuerzo y tu a cambio tendrás acceso a los logros de steam. Por el contrario si quieres obtener la versión FREE, Charge kid ha sido creado con Godot 3.2. por lo que bastaría con descargarse el motor gráfico y los archivos del juego para conseguir una build funcional. Yo lo hice y me aseguré de duplicar las plataformas en algunos niveles. 


En definitiva estamos ante un buen juego, un estilo gráfico limpio, sin muchas florituras y una música que acompaña que no pretende destacar. Para mi es un juego que merece ser comprado y que por otro lado os animo que os descarguéis gratis para que husmeéis en su [código fuente](https://gitlab.com/uspgamedev/charge_kid) y aprendáis como funciona.


¿Qué te parece Charge Kid? ¿Le darás una oportunidad?


Cuéntamelo en los comentarios o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux). 

