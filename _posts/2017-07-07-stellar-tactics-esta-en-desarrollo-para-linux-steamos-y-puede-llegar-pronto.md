---
author: Pato
category: Rol
date: 2017-07-07 17:40:00
excerpt: "<p>El estudio ha confirmado en Twitter que est\xE1n trabajando en un port</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/eb800d55c6f0176e166b1567b2249535.webp
joomla_id: 401
joomla_url: stellar-tactics-esta-en-desarrollo-para-linux-steamos-y-puede-llegar-pronto
layout: post
tags:
- espacio
- proximamente
- rol
- exploracion
title: "'Stellar Tactics' est\xE1 en desarrollo para Linux/SteamOS y puede llegar\
  \ pronto"
---
El estudio ha confirmado en Twitter que están trabajando en un port

Gracias a [gamingonlinux.com](https://www.gamingonlinux.com/articles/stellar-tactics-the-space-exploration-rpg-with-classless-character-progression-is-getting-closer-to-linux.9953) nos llegan noticias de que Stellar Tactics [[Steam](http://store.steampowered.com/app/465490/Stellar_Tactics/)] está en desarrollo para Linux/SteamOS y además parece que su lanzamiento para nuestros sistemas está relativamente cerca.


Un usuario divisó movimientos en SteamDB sobre un "depot" para Linux del juego y preguntó en Twitter al estudio si había un port en camino, a lo que le contestaron:



> 
> Running perfectly with one or two small issues. Needs full testing though. Not sure when it will be done.
> 
> 
> — Stellar Tactics (@StellarTactics) [5 de julio de 2017](https://twitter.com/StellarTactics/status/882656762381381634)



Stellar Tactics es un RPG táctico por turnos en vista isométrica con elementos de exploración espacial que aún está en acceso anticipado, en el que tendremos que explorar el universo, abordar naves enemigas, establecer rutas comerciales y descubrir zonas en las que buscar equipamiento para ir mejorando, según avanzamos en un universo que se genera de manera procedural. Según cuentan los desarrolladores el juego está planeado para que termine con un final donde las historias serán ampliadas de manera episódica con contenido tanto de Maverick Games, la desarrolladora, como, ocasionalmente, de la comunidad.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/kTnWDuUNuLs" width="560"></iframe></div>


 


La vedad es que tiene muy buena pinta, así que esperemos que no tarde mucho en llegar a nuestro sistema preferido.


 


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/465490/" width="646"></iframe></div>


 


¿Te animarás a explorar el universo? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

