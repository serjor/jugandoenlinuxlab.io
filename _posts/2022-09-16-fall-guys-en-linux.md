---
author: P_Vader
category: "V\xEDdeos JEL"
date: 2022-09-16 17:04:31
excerpt: "<p>Hemos probado Fall Guys desde la Epic Store con Lutris.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/FallGuys/fall_guys_season1.webp
joomla_id: 1478
joomla_url: fall-guys-en-linux
layout: post
title: Fall Guys en Linux (Actualizado)
---
Hemos probado Fall Guys desde la Epic Store con Lutris.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/JxW44kLpzfQ" title="YouTube video player" width="720"></iframe></div>


 ACTUALIZACIÓN 16/9/2022:


Se puede jugar también con [Heroic Launcher]({{ "/tags/heroic-games-launcher" | absolute_url }}) siguiendo esta guía:


<https://github.com/Heroic-Games-Launcher/HeroicGamesLauncher/wiki/Fall-Guys>


En la segunda temporada parece que falla al iniciar. Se soluciona entrando en la configuración del propio juego en Heroic, en el apartado "**`Otros`**" e introduciendo en "**`Seleccione un EXE alternativo`**": y pones la ruta al archivo `FallGuys_client_game.exe`, en mi caso sería: **`~/Games/Heroic/FallGuys/FallGuys_client_game.exe`**


 




---


LUTRIS (25-06-2022):


1. Hemos usado Lutris 0.5.10.1 para instalar la tienda de Epic y Fall Guys. En la configuración de lutris asegúrate antes de tener activado el ejecutor (runner) Wine y la versión Lutris-GE-Proton-7.16.
2. Botón derecho en la carátula del juego \ pincha en Configurar \ Usamos el ejecutor (runner) wine version Lutris-GE-Proton-7.16, también DXVK 1.10.1 (sin esta versión no funciona los servicios de Epic para invitar amigos) y tenemos activado "Anti trampas Easy".
3. Copiamos el archivo   
`~/Games/epic-games-store/drive_c/Program Files/Epic Games/FallGuys/EasyAntiCheat/easyanticheat_x64.so`   
en:  
`~/Games/epic-games-store/drive_c/Program Files/Epic Games/FallGuys/FallGuys_client_game_Data/Plugins/x86_64/`
4. Editamos el archivo:   
`~/Games/epic-games-store/drive_c/Program Files/Epic Games/FallGuys/FallGuys_client.ini`  
Cambiando la primera linea por:  
`TargetApplicationPath=FallGuys_client_game.exe`
5. Por último quizás es necesario tener instalado en Steam -> Biblioteca -> Herramientas -> Proton Easyanticheat Runtime.


 


Nota: La ruta de instalación `~/Games/epic-games-store/` es la que hace por defecto Lutris, si lo has instalado en otra carpeta tendrás que cambiarlo.


Nota2: Al actualizar es posible que tengas que repetir los pasos.


**Si tienes alguna duda, consulta nuestro canal de Matrix para juegos Wine/Proton:** 


**[https://matrix.to/#/#wine_proton-jugandoenlinux:matrix.org](https://matrix.to/#/#wine_proton-jugandoenlinux)**


 


Fuente: <https://www.reddit.com/r/linux_gaming/comments/vi1mjr/how_to_play_fall_guys_using_egs_and_lutris/>


 

