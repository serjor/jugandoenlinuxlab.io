---
author: leillo1975
category: Carreras
date: 2018-05-15 14:14:17
excerpt: <p>Interesante juego de carreras Open Source</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/10dc0c0d83cf5ec5b13a99f94232defb.webp
joomla_id: 735
joomla_url: conoceis-dust-racing-2d
layout: post
tags:
- carreras
- open-source
- dust-racing-2d
- coches
- codigo-abierto
title: "\xBFConoceis Dust Racing 2D?"
---
Interesante juego de carreras Open Source

No se si a muchos le sonará, pero yo no he tenido noticia de el hasta que nuestro colega @franciscot nos dejó un [video](https://youtu.be/3hrIzzItggQ) en nuestro canal de Telegram. Poco después empecé a ver interesantes artículos sobre él en [Maslinux.es](http://maslinux.es/dust-racing-2d-juego-de-carreras-de-coches-de-codigo-abierto-escrito-en-qt-y-opengl/) y [Ubunlog](https://ubunlog.com/dust-racing-2d-carreras/). Pienso que no sería justo que tratándose de una web de juegos para GNU-Linux, no hablásemos de este título.


[Dust Racing 2D](https://github.com/juzzlin/DustRacing2D) es un juego de código abierto (GPL-3) en dos dimensiones realizado en QT y con librerias OpenGL. La temática, es como dice su nombre, las carreras de coches desde un punto de vista cenital, permitiendo jugar a uno o dos jugadores a competiciones en diferentes circuitos, pudiendo escoger entre tres niveles de difcultad. El juego dispone de varios modos entre los que se encuentran Carrera, Contrarreloj y Duelo.


Este software aparentemente simple, dispone de unas físicas bastante curradas que hacen que los coches derrapen, choquen de forma convincente, dejen frenadas... Además es necesario cumplir las normas, ya que si nos pasamos de listos y atajamos demasiado nos sancionarán. También será necesario cambiar las ruedas cuando llevemos unas cuentas vueltas entrando en boxes. En cuanto a la jugabilidad, es completamente intuitivo y no te costará nada hacerte con los mandos, aunque ello no quiere decir que sea fácil ganar, ya que tendremos que habituarnos a la forma de dirigir los coches.


A nivel técnico disfruta de unos gráficos no muy llamativos pero que cumplen su cometido perfectamente, y que combinan el uso del 2D con el 3D (los coches). Los detalles de los rastros de humo en los derrapes, al pisar tierra, etc están muy trabajados. En cuanto al sonido, el ruido del motor, los choques, frenazos, derrapes... están muy bien implementado. La información en pantalla se muestra de forma austera pero efectiva. A nivel general se puede decir que es un producto muy sencillo, pero muy bien presentado, y que resulta muy fácil de disfrutar.


Dust Racing 2D se puede encontrar en su [página de Github](https://github.com/juzzlin/DustRacing2D) y en la mayoria de los repositorios de las distribuciones más importantes, por lo que no será difícil conseguirlo. Aquí te dejamos un video para que te hagas una idea más fidedigna de lo que te vas a encontrar:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/3hrIzzItggQ" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Conocías ya Dust Racing 2D? ¿Qué te parece este desarrollo Open Source? Déjame tus opiniones en los comentarios, o en nuestros grupos de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

