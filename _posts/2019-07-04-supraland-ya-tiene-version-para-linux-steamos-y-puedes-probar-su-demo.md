---
author: Pato
category: "Exploraci\xF3n"
date: 2019-07-04 17:36:23
excerpt: "<p>Juega a una mezcla de g\xE9neros en este nuevo sandbox</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/36778fed172d9c8502d2d42dc025835b.webp
joomla_id: 1072
joomla_url: supraland-ya-tiene-version-para-linux-steamos-y-puedes-probar-su-demo
layout: post
tags:
- indie
- aventura
- puzzles
- exploracion
title: "Supraland ya tiene versi\xF3n para Linux/SteamOS, y puedes probar su demo"
---
Juega a una mezcla de géneros en este nuevo sandbox

Un nuevo juego ha llamado nuestra atención pues acaba de anunciar su soporte para Linux. Se llama Supraland y como indica su descripción se trata de una mezcla de géneros dentro de un mundo sandbox:


*Explora, encuentra mejoras ocultas, resuelve puzzles, derrota a monstruos, encuentra nuevas habilidades que te ayudarán a llegar a nuevos lugares.*


*Supraland es un Metroidvania de Puzzles en Primera Persona. Las fuentes principales de inspiración son Zelda, Metroid y Portal.*  
*Supraland asume que eres inteligente y no interferirá con tu autonomía. La historia es mínima, suficiente para darte un objetivo a seguir, y después dejarte libre.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/qFsXKSAHhVU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Por lo poco que sabemos, el juego es obra de "Supra Games" que al parecer y como pasa con otros proyectos índie es cosa de una sola persona, lo cual tiene aún mas mérito.


**Características:**


- **Metroidvania** -


Exploras un gran mundo interconectado en el que la mayoría de caminos, al principio, son inaccesibles hasta que encuentres nuevas habilidades para superar esos límites. Una piedra angular del diseño de Supraland fue crear habilidades que continuarán sorprendiéndote por la cantidad de usos que tienen. No quería dar habilidades genéricas de metroidvania que solo tienen un uso. Si combinas tus habilidades las posibilidades son incluso mayores.


- **Exploración** -


La mayoría del juego se trata de explorar e intentar encontrar secretos. A veces pensarás que estás a punto de salirte de los límites del mapa y superar al diseñador de niveles, pero justo ahí encontrarás un cofre esperándote con una mejora muy gratificante.


- **Diseño de puzzles** -


Contra más profundo te adentres en el juego, más te enfrentarás a puzzles tan creativos que son completamente únicos. Tipos de puzzles que ya conozcía de otros juegos fueron rechazados de inmediato en el proceso de diseño. Y es importante para mí que una vez que entiendas la idea detrás de un puzzle, puedas resolverlo inmediatamente en lugar de tener que pasar por una ejecución incómoda y frustrante.


- **Combate** -


La mecánica de lucha está inspirada en los juegos de disparos de la vieja escuela, donde puedes atacar y saltar a gran velocidad y nunca tendrás que esperar a que se recarguen tus armas.


**En cuanto a los requisitos**, son:


**Mínimo:**  

+ Pprocesador y un sistema operativo de 64 bits
+ **Procesador:** Intel Core2Duo 2.66GHz
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** GTX 780
+ **Almacenamiento:** 3 GB de espacio disponible


**Recomendado:**  

+ Procesador y un sistema operativo de 64 bits
+ **SO:** Ubuntu 18.04/Steam OS
+ **Procesador:** Intel Core i5 6600 o AMD Ryzen 1600x
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** NVIDIA Geforce GTX 1060, 3 GB (Legacy: NVIDIA Geforce GTX 780, 4GB) o AMD RX 580, 4 GB (Legacy: AMD R9 290X, 4GB )
+ **Almacenamiento:** 3 GB de espacio disponible


Si te atrae este tipo de juegos y quieres probarlo, tienes disponible una demo en su [página web de Steam](https://store.steampowered.com/app/813630/Supraland/) donde además puedes comprarlo ahora mismo **con un 33% de descuento** por las rebajas de verano.


¿Qué te parece Supraland? ¿Te gustan los juegos con este tipo de mezcla de géneros?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).




Gracias a **@Lordgault** por el apunte ;-)

