---
author: Son Link
category: "Exploraci\xF3n"
date: 2022-02-02 16:21:57
excerpt: "<p>Aprovechamos para adelantaros algunas im\xE1genes espectaculares de la\
  \ pr\xF3xima gran actualizaci\xF3n.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/1anno/valhemin-1anno-1.webp
joomla_id: 1421
joomla_url: 1-ano-de-valheim
layout: post
tags:
- acceso-anticipado
- valheim
title: "Un a\xF1o de Valheim"
---
Aprovechamos para adelantaros algunas imágenes espectaculares de la próxima gran actualización.


Hace un año **Valheim** salió a venta. Un juego que en poco tiempo pulverizo varios récords, y todo esto saliendo como juego en Acceso Anticipado, que aun lo esta y aun le queda camino para la versión 1,0, pero durante este tiempo no han parado de trabajar, sacando nuevas versiones, la primera de las grandes actualizaciones [**Heart & Home**]({{ "/posts/hearth-home-la-primera-gran-actualizacion-de-valheim" | absolute_url }}), y con la siguiente, **Mountain** cada vez más cerca y la cual traerá entre otras novedades cuevas a explorar en las Montañas, si bien de momento solo han mostrado algunas capturas y arte conceptual que os mostramos a continuación:


![valhemin 1anno 1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/1anno/valhemin-1anno-1.webp)


![valhemin 1anno 2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/1anno/valhemin-1anno-2.webp)


 


Durante las vacaciones de invierno [realizarón un concurso](https://twitter.com/Valheimgame/status/1483077076383059968) para buscar la mejor construcción y cuyo ganador fue el siguiente, la cual es espectacular.


![valhemin 1anno 3](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/1anno/valhemin-1anno-3.webp) 


Por ultimo, y para irnos poniendo los dientes largos, han hablado un poco de la siguiente actualización y que afectara al bioma **Misthland**, uno de los biomas del juego que a día de hoy no tiene nada, salvo rocas y arboles. Eso si, aun tendremos que esperar mucho para ello, ya que actualmente aun están pensando en los nuevos materiales de construcción, enemigos y mecánicas, etc, eso si, nos muestra uno de los nuevos enemigos que nos podríamos encontrar (como si no tuviéramos ya bastante con los Deathsquitos)


 ![valhemin 1anno 4](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/1anno/valhemin-1anno-4.webp)


Y esto es todo de momento. **Solo recordar que tenemos un servidor al que podéis uniros. Tenéis más información en el [foro](index.php/foro/valheim)**


Hasta la próxima, y que Odin os proteja.


Fuente: <https://store.steampowered.com/news/app/892970/view/3124937623694535167>


 


 

