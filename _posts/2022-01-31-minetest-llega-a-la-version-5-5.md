---
author: P_Vader
category: "Exploraci\xF3n"
date: 2022-01-31 10:45:41
excerpt: "<p>Esta nueva versi\xF3n trae importantes novedades de cara al futuro.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Minetest/minetest_screenshot_900px.webp
joomla_id: 1418
joomla_url: minetest-llega-a-la-version-5-5
layout: post
tags:
- mundo-abierto
- software-libre
- minetest
- voxel
title: "Minetest llega a la versi\xF3n 5.5"
---
Esta nueva versión trae importantes novedades de cara al futuro.


Tras la ajetreada temporada que han tenido estos últimos meses desarrollando y preparando la [Minetest Game Jam 2021](index.php/42-videos-jel/1405-video-minetest-game-jam-2021), llega el turno de una nueva actualización de este mítico juego software libre inspirado en Minecraft.


En esta ocasión las novedades más bien preparan al juego para futuros cambios importantes que llegarán.


Entre las novedades podemos destacar:


* **Han iniciado su propia bifurcación del motor de renderización Irrlicht,** llamado [IrrlichtMt](https://github.com/minetest/irrlicht), que permitirá corregir muchos errores bastante antiguos como puede ser la introducción de texto en el juego, la imposibilidad de habilitar un modo de pantalla completa, la compatibilidad con macos o mejoras gráficas.  Muchos de estos fallos son bastante conocidos y de difícil arreglo dado que el motor original no se actualiza desde el año 2016.
* Ahora la compresión de mapas es mucho más eficiente utilizando el formato de compresión Zstd. De de esta manera se ha conseguido un aumento en l**a generación de mapas de entre un 20 a 30% más rápida.**
* **Se ha trabajado para implementar sombras dinámicas** las cuales se venía trabajando durante bastantes meses, **pero no estaban listas para incluirse en esta versión**. Este va a ser un añadido bastante espectacular para el juego y que esperemos que incluyan en la siguiente versión.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/NuNXqY5J_4w" title="YouTube video player" width="780"></iframe></div>


* Como siempre se han hecho [muchos arreglo](https://dev.minetest.net/Changelog#5.4.0_.E2.86.92_5.5.0) y correcciones de fallos.


**Una nota importante:**



> 
>   
> "Esta no es la primera vez, pero para evitar sorpresas, queremos señalar que una vez que haya cargado un mapa en Minetest 5.5, no podrá cargarlo en versiones anteriores. Haz copias de seguridad primero. (La compatibilidad cliente-servidor no se ve afectada por esto)."
> 
> 
> 


¿Que te parece Minetest? ¿Lo has jugado alguna vez? Cuentanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 


 

