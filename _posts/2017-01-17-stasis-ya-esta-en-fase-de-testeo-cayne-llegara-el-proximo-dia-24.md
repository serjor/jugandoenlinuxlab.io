---
author: Pato
category: Aventuras
date: 2017-01-17 12:55:32
excerpt: "<p>Son aventuras gr\xE1ficas de terror tipo \"point and click\" en vista\
  \ isom\xE9trica y un cuidado aspecto visual</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3f670763861139bf2201e7bc224257d4.webp
joomla_id: 181
joomla_url: stasis-ya-esta-en-fase-de-testeo-cayne-llegara-el-proximo-dia-24
layout: post
tags:
- proximamente
- steam
- gratis
- aventura-grafica
title: "'STASIS' ya est\xE1 en fase de testeo, 'CAYNE' llegar\xE1 el pr\xF3ximo d\xED\
  a 24"
---
Son aventuras gráficas de terror tipo "point and click" en vista isométrica y un cuidado aspecto visual

El estudio "The Brotherhood" comienza a darnos alegrías. Desde hace tiempo sigo a este estudio por lo llamativo y cuidado de sus trabajos.


Centrados en temáticas Sci-Fi y terror tienen en marcha unas aventuras gráficas de tipo "point and click" bastante interesantes. Desde un punto de vista muy enfocado en estas dos premisas ya lanzaron hace algún tiempo STASIS [[web oficial](http://www.stasisgame.com/)], un galardonado juego que tal y como [comentan en su foro](http://steamcommunity.com/app/380150/discussions/0/541907867777059868/?ctp=16#c144513524088784010) de Steam ya está en fase de testeo para Linux y nos llegará próximamente en una fecha aún por determinar. A continuación te dejo un vídeo donde puedes ver el concepto del juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/b5vNTXmV2ks" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Por otra parte el estudio nos anuncia la llegada de 'CAYNE' [[web oficial](http://www.playcayne.com/)], esta vez con versión Linux tal y como [anuncian en su foro de Steam](http://steamcommunity.com/games/380150/announcements/detail/269488042138543366) para el próximo 24 de Enero ¡y además de forma gratuita!. Se trataría de una continuación de STASIS y se desarrollará dentro del universo distóptico de este último. La escueta descripción es muy sugerente: "Hadley se despierta en una instalación. Está embarazada de 9 meses, y ellos quieren a su bebé..."


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/9f99QQf4l_w" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Podrás descargar CAYNE el próximo día 24 desde su página web o en Steam donde está disponible en español, y si te gusta el juego podrás apoyarlo adquiriendo la versión digital "deluxe" que incluye la banda sonora, fondos de pantalla y otros artículos:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/532840/" width="646"></iframe></div>


CAYNE será la precuela del juego 'BEAUTIFUL DESOLATION' [[web oficial](http://www.desolationgame.com/)] que vendrá próximamente y del que nos anuncian su próxima campaña en Kickstarter precísamente para el mismo día de lanzamiento de CAYNE, y de la que estaremos atentos. Además nos dejan su trailer:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/1IkbdiJCrwo" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 ¿Qué te parecen estas aventuras gráficas? ¿jugarás a este CAYNE?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

