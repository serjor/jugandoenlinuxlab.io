---
author: P_Vader
category: Editorial
date: 2022-12-29 17:55:01
excerpt: "<p>En este segundo especial, continuamos d\xE1ndoos a conocer los juegos\
  \ favoritos del 2022 de Jugando en Linux.&nbsp;</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/NuestrosJuegos2022/Valheim2022.webp
joomla_id: 1518
joomla_url: nuestros-juegos-de-2022-segunda-parte
layout: post
tags:
- steam
- goty
- celeste
- valheim
- veloren
- steam-deck
- stardew-valley
- horizon-zero-dawn
- selaco
title: Nuestros juegos de 2022 (segunda parte)
---
En este segundo especial, continuamos dándoos a conocer los juegos favoritos del 2022 de Jugando en Linux. 


Son Link
--------


A lo largo de este año he jugado a varios juegos, tanto libres como no libres, es por eso que yo he decidido mencionar al juego que más he jugado de cada tipo:


En primer lugar, y como me paso en 2021, al que más he jugado ha sido **Valheim**, ademas en un segundo año de vida marcado por 2 grandes actualizaciones: **Cuevas Heladas** y **Minstlands**. Explorar el vasto mundo, mejorar tus habilidades y equipación, es una gozada, sobre todo si lo haces con amigos y/o familiares. Y al paso que va, lo mismo repite para lo más jugado del 2023.


Feliz Yule y Feliz Navidad


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/NuestrosJuegos2022/Valheim2022.webp)


En cuanto a juegos de código abierto este año ha sido el de Veloren, un MMORPG con un basto mundo por recorrer, y al igual que en Valheim, tendremos que recolectar materiales para mejorar nuestra equipación, así como mejores alimentos. Podemos unirnos al servidor oficial o montar el nuestro propio, como mostré en este directo:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/lgXrYkEbaYY" title="Probamos un servidor de Veloren en una Raspberry Pi 4" width="720"></iframe></div>


En cuanto al año que entra, por el momento el juego que más espero es [Selaco](https://store.steampowered.com/app/1592280/Selaco/), un FPS que usa el motor de código abierto GZDoom, aunque con varios cambios que apenas notas que estas usando el motor de los primeros Doom.


 


P_Vader
--------


Con la llegada de la Steam Deck a mis manos he tenido un cambio de hábitos este año. Al ser una "consola" que puedes ponerte a jugar cómodamente en cualquier sitio, tiendo a iniciar esos juegos que retomas rápidamente. Por ejemplo **volví a rengancharme a Stardew Valley** ([un clásico en lo mas jugado en la Deck](https://twitter.com/OnDeck/status/1598422797826813955?s=20&t=87W6tRivFl_lMbaCVOUdKA)) y como el que no quiere la cosa llevo mas de 95 horas. Es mi juego preferido para antes de irme felizmente a la cama.  
![stardew valley](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/NuestrosJuegos2022/stardew_valley.webp)


 **Celeste con 3284 muertes y casi 13 horas** hasta la fecha es un buen ejemplo de juego que estoy exprimiendo mucho este año con la deck. Me lo tomo con mucha paciencia y lo juego durante ratos cortos. Enciendes la consola y en pocos segundos ya lo estás jugando, para evitar la frustración en el momento que me atranco lo dejo. El día que lo acabe la voy a echar de menos.  
![celeste](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/NuestrosJuegos2022/celeste.webp)


  
El boom para mi este año ha llegado tardío, con un juego que me regalaron estas navidades para exprimir la Deck. Se trata de **Horizon Zero Dawn**. **Lo voy a incluir en mis juegos de 2022 ya que en los escasos días que ando disfrutándolo llevo la friolera de casi 11 horas**. Se me ha pasado el tiempo volando literalmente. Un gran juego en el que la cosola de Valve rinde bien sin dificultad, es mas he subido algunos ajustes gráficos a niveles altos incluso ultra manteniendo una fluidez estable. En este caso he optado por el famoso refresco de pantalla "[golden 40](https://youtu.be/DA2EqFqIPM4?t=198)".  
![horizon zero dawn](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/NuestrosJuegos2022/horizon_zero_dawn.webp)


  
Lo que **espero para 2023** son dos candidatas a joyas. No se si saldrán el próximo año, pero aquí las dejo:  
- El siguiente juego de [ConcernedAp](https://twitter.com/concernedape):  **[Haunted Chocolatier](https://www.hauntedchocolatier.net/)**  
- El eterno esperado: **[Hollow Knight Silksong](https://www.hollowknightsilksong.com/)**


 


¿Qué te parecen los videojuegos que han escogido nuestros colaboradores? Cuéntanos cuáles son los videojuegos a los que más caña has dado durante este 2022 en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

