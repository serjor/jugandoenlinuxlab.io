---
author: leillo1975
category: Arcade
date: 2017-07-18 04:00:00
excerpt: <p>Cumpliendo con lo prometido ya disponemos de este juego</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d4b22e8d4b1fb6349c7d95bc5629e3c0.webp
joomla_id: 411
joomla_url: lanzado-wonder-boy-the-dragon-s-trap
layout: post
tags:
- wonder-boy
- the-dragon-s-trap
- ethan-lee
- dotemu
- lizardcube
title: "Lanzado Wonder Boy: The Dragon's Trap (Actualizaci\xF3n, el juego incluye\
  \ herramientas para modders)"
---
Cumpliendo con lo prometido ya disponemos de este juego

Hace poco más de un mes [os informábamos](index.php/homepage/generos/plataformas/item/499-wonder-boy-the-dragon-s-trap-esta-siendo-portado-a-linux-steamos) que se estaba portando **Wonder Boy: The Dragon's Trap** a GNU-Linux/SteamOS. Hace una semana actualizábamos esta información con la inminente llegada en el día de hoy de tan singular juego. Editado por [DotEmu](http://www.dotemu.com/) y desarrollado por [Lizardcube](http://www.lizardcube.com/), nos llega este **remake del héroe de los 8bits** que tanta diversión nos proporcionó en su día.


 



> 
> Wonder Boy: The Dragon's Trap is now available on Steam for MAC and LINUX players! <https://t.co/Wvd4O4Z6GZ> (ported by [@flibitijibibo](https://twitter.com/flibitijibibo)) [pic.twitter.com/dAsKH6EVNu](https://t.co/dAsKH6EVNu)
> 
> 
> — Lizardcube (@lizardcube) [July 18, 2017](https://twitter.com/lizardcube/status/887246789966876673)



 


La conversión a nuestros sistemas llega de la mano del conocido [Ethan Lee](https://twitter.com/flibitijibibo), que tiene su curriculum [innumerables ports](http://www.flibitijibibo.com/index.php?page=Portfolio/Ports) de juegos de estilo retro, tan conocidos como **Capsized, Rogue Legacy, FEZ, Reus o Dust, An Elysian Tale**. Contar con él para tan esperado título es una garantía y seguro que su trabajo ha sido excelente como ha demostrado con su buen hacer en el pasado.


 


Wonder Boy: The Dragon's Trap es un **arcade de plataformas** de estilo ochentero, pero con gráficos renovados y esta es la **descripción oficial** que podemos ver en Steam:


 



> 
> *Mejorado con preciosas animaciones dibujadas a mano y un paisaje sonoro reorquestado, el clásico de culto vuelve con su mezcla única de exploración, acción y aventura.*  
>    
>  *Convertido en monstruo medio humano y medio lagarto por la maldición del Mekadragón, debes buscar la cura. Solo recobrarás tu forma humana encontrando la Cruz de la Salamandra, un objeto mágico que puede eliminar maldiciones...*  
>    
>  *Recorre lugares enormes e interconectados, habitados por monstruos gruñones y dragones exóticos. Al matarlos, la maldición empeora y te transforma en distintos animales...*  
>    
>  *Juega con el clásico personaje humano ¡o la esperada coprotagonista humana!*  
>  *Juega con el hombre lagarto, el hombre ratón, el hombre piraña, el hombre león, el hombre halcón. ¡Usa sus habilidades para explorar los secretos de Monster Land!*  
>  *Cambia entre gráficos y sonido modernos y de 8 bits cuando quieras, ¡incluso en plena partida!*  
>  *Disfruta de 3 niveles de dificultad apropiados para jugadores ocasionales y expertos, amigos y familias.*
> 
> 
> 


 


Los requisitos mínimos, como podeis ver son comedidos, como se espera de un juego de estas características, por lo que la gran mayoría de vosotros no tendreis casi seguro ningún problema para poder disfrutarlo:


 


**Mínimo:**  

+ SO: glibc 2.17+, 32/64-bit
+ Procesador: Intel(R) Core(TM) i3-4030U CPU @1.90GHZ (4 CPUs)
+ Memoria: 4 GB de RAM
+ Gráficos: OpenGL 3.2 Core Profile support, Intel HD 4000-5000 series (Intel HD 3000 series unsupported)
+ Almacenamiento: 1100 MB de espacio disponible
+ Notas adicionales: SDL_GameController devices fully supported.



**Recomendado:**  

+ SO: glibc 2.17+, 32/64-bit
+ Procesador: Intel® Core™ i3-7100 CPU @3.9 GHz(4CPUs), ~3.9GHz
+ Memoria: 8 GB de RAM
+ Gráficos: NVIDIA GeForce GTX 760
+ Almacenamiento: 1100 MB de espacio disponible
+ Notas adicionales: SDL_GameController devices fully supported.



 


También nos gustaría anunciaros que **pronto publicaremos un completo análisis de este juego**, por lo que os aconsejo a que esteis antentos a nuestras noticias en los próximos días. Os dejo con un video que acaba de publicar DotEmu anunciando la llegada a Linux del juego:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/y9MvS4pVVZc" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Actualización: Podemos leer en [GOL](https://www.gamingonlinux.com/articles/wonder-boy-the-dragons-trap-released-for-linux-tools-included-to-mod-the-game-too.10007) que junto con el juego se incluye también las herramientas para poder crear modificaciones del mismo, simplemente hay que guardar el fichero Settings.cfg con el siguiente flag: "Editor=true"


 


También hemos grabado un video donde "desempaquetamos" este juego y jugamos sus primeros compases. Podeis verlo justo aquí debajo:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/15sm6OnWF9c" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div> 


 


Si quereis haceros con este renovado clásico, podeis comprarlo por tan solo **19.99€ en Steam**


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/543260/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Qué os parece Wonder Boy: Dragon's Trap? ¿Habeis jugado antes a alguno de los juegos de la saga? Dejanos tus impresiones en los comentarios o en nuestro canal de [Telegram](https://telegram.me/jugandoenlinux).

