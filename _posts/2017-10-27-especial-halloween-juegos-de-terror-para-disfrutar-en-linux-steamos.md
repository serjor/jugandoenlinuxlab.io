---
author: Jugando en Linux
category: Terror
date: 2017-10-27 17:00:00
excerpt: <p>En Jugando en Linux os traemos una lista de juegos de terror interesantes
  para "disfrutar" durante estas fechas</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2cf3de1421350014af841348e8a1b0fb.webp
joomla_id: 502
joomla_url: especial-halloween-juegos-de-terror-para-disfrutar-en-linux-steamos
layout: post
tags:
- terror
- halloween
title: 'Especial Halloween: Juegos de terror para disfrutar en Linux/SteamOS'
---
En Jugando en Linux os traemos una lista de juegos de terror interesantes para "disfrutar" durante estas fechas

Aquí en Jugando en Linux hemos pensado que aprovechando además las rebajas de Steam y otras tiendas como GOG (que también ha comenzado sus rebajas de Halloween) vamos a hacer una pequeña lista de juegos de temática de terror o "gore" que estén disponibles en Linux/SteamOS y merezcan la pena para echar unas partidas y pasar un "mal" rato. Comenzamos:


**Outlast**


Métete en un asilo abandonado y descubre sus terribles secretos... si te atreves en este "survival horror" de la mano de algunos de los veteranos en el género. 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/2GPf3MdVOKI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Outlast está en español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/238320/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


**Amnesia:The dark descent & A machine for Pigs**


Una mansión abandonada, despiertas sin apenas recuerdos y los extraños ruidos que salen de los pasillos no te dejan pegar ojo. ¿Qué mas se puede pedir?


La saga Amnesia fue la pionera en llevar los juegos "survival horror" a un nuevo nivel, donde no puedes defenderte y lo único que puedes hacer es correr y esconderte. Ambientado con una acertadísima iluminación y aderezado todo con un sonido que quita el hipo (muy recomendable usar cascos para jugar) hay pocos que puedan jugar a un Amnesia con la luz apagada sin pegar un bote de la silla.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/JEHPwAvrc_U" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/4CagMNLxa9M" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 La saga Amnesia está disponible en español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/57300/6098/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/57300/6098/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


**SOMA**


De los creadores del primer Amnesia nos llegó en 2015 SOMA. El entorno cambia en un videojuego de terror y ciencia ficción ambientado bajo las olas del océano Atlántico. Lucha por sobrevivir en un mundo hostil que te hará cuestionar tu propia existencia.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/cO8PLDh8UbE" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 SOMA está disponible en español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/282140/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


**Alien: Isolation**


Pocas cosas hay más terroríficas que encontrarte a solas en una nave frente a un Xenomorfo mientras tu detector de movimiento se vuelve loco emitiendo sonidos alarmantes. Que se lo digan a Ripley en este magnífico juego de Creative Assembly que nos trajo Feral Interactive. Dentro de la estación espacial Sebastopol (o aún mejor, ¡en la Nostromo!) tendrás que intentar sobrevivir frente a una tripulación enloquecida y un Xenomorfo con un cociente intelectual y una mala baba (literal) sin precedentes.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/mAgs5Jm3URc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/pu_rWH5lS3w" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


 En el espacio nadie puede oir tus gritos. Alien: Isolation está disponible en español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/214490/51156/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


 **Layers of Fear**


 Otro ejemplo de cómo una mansión puede desatar la locura. Gracias a Aspyr media nos llegó esta excelente obra de terror psicológico. ¿Te atreves a participar en la creación de una auténtica obra maestra del miedo? Layers of Fear es un psicodélico juego de terror en primera persona que se centra en el argumento y la exploración. Sumérgete en la mente de un pintor lunático y descubre el secreto de su locura, mientras exploras una enorme mansión victoriana en constante cambio. Desvela las visiones, miedos y horrores que han invadido al pintor y termina la obra maestra que tanto tiempo ha luchado por crear. 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/mOBsKNLtQ3g" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Layers of Fear está disponible en español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/391720/74835/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 **White Noise 2**


Salimos un momento de la experiencia para un jugador y nos vamos a un juego de terror multijugador 4 contra 1 (o más concretamente 1 contra 4). White Noise 2 es la secuela del exitoso White Noise Online. Forma parte del equipo de investigadores, ¡o controla a la criatura y devóralos a todos! White Noise 2 ofrece una experiencia de terror asimétrico 4vs1 que no te dejará indiferente.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/X2yeJ_0C_2E" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


White Noise 2 estádisponible en español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/503350/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


**Dead Island**


Techland y Deep Silver nos trajeron el año pasado una bonita aventura en una isla paradisiaca. Lástima que estuviera infestada de un brote zombie, así que tendremos que liarnos la manta a la cabeza y tratar de sobrevivir en esta "horrible" pesadilla de la que no es posible escapar.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/zsJojcfKxGM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Dead Island está disponible en español:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/383150/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


**>Observer_**


Volvemos con Aspyr Media y este recentísimo juego de los creadores de Layers of Fear que nos acaba de llegar a Linux, [tal y como os anunciamos](index.php/homepage/generos/aventuras/item/625-lanzado-al-fin-observer-en-gnu-linux-steamos). ¿Qué harías si te haquearan los miedos? Estamos en el año 2084. Tu nombre es Daniel Lazarski y eres un detective neural de élite conocido como Observer; tu objetivo es haquear e invadir la mente de los sospechosos. Para recopilar pruebas, deberás revivir sus miedos más siniestros y, finalmente, enfrentarte a los tuyos.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/avhOiKca2KM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


>Observer_ está disponible en español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/514900/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


**Darkwood**


Cambiamos de tercio y pasamos de las 3D a un juego en vista cenital, pero que a mas de uno le ha hecho ponerse cardíaco. Acecha y explora el entorno cambiante del bosque mientras buscas recursos, escóndete en tu agujero cuando caiga la noche y reza para que llegue la luz del día.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/3S3tmWfFACQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Darkwood no está disponible en español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/274520/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


**Dying Light**


Volvemos de nuevo con Techland donde le dan una nueva vuelta de tuerca al genero de supervivencia en un apocalipsis zombie. En esta ocasión te encontrarás encerrado en una ciudad devastada por una epidemia que vuelve a sus habitantes sedientos de carne. Podrás hacerle frente con armas de todo tipo o esquivarlos usando tus habilidades de Parkour. 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/7VkS3beywvk" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Dying Light está disponible en completamente en castellano:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/239140/88801/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


Sabemos que hay muchos más, pero como no podemos ponerlos todos, ¿que tal si nos dices qué te parece nuestra lista? ¿Añadirías alguno? ¿Quitarías algún otro?


Cuéntanoslo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

