---
author: Pato
category: "Acci\xF3n"
date: 2018-06-22 14:15:51
excerpt: <p>El juego es obra de un solo desarrollador @DevRudoy</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/55a9ec53054b140fa3784d6b9508fcf5.webp
joomla_id: 784
joomla_url: heat-guardian-tendra-version-para-linux-steamos
layout: post
tags:
- accion
- indie
- proximamente
- terror
title: "'Heat Guardian' tendr\xE1 versi\xF3n para Linux/SteamOS"
---
El juego es obra de un solo desarrollador @DevRudoy

Nos hacemos eco de otro de esos juegos que a veces pasan desapercibidos pero luego pueden ser pequeñas joyas. Gracias a [linuxgameconsortium.com](https://linuxgameconsortium.com/linux-gaming-news/heat-guardian-linux-port-coming-soon-67673/) nos enteramos de la existencia de '**Heat Guardian**', un juego de acción en vista cenital ambientado en un mundo post-apocalíptico en el que una guerra ha sumido al mundo en un invierno perpetuo en el que la mayoría de los humanos han perecido por el frío. En este contexto tendremos que tratar de sobrevivir a las duras condiciones ambientales y a todos los seres que intentarán acabar con nosotros.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/8fPApTf13-k" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


*Heat Guardian - es un juego hardcore de disparos con elementos de supervivencia, horror y sigilo en el mundo frío post-apocalíptico. El frío mató la mayor parte de la vida en la Tierra. La civilización está destruida. Los restos de la humanidad sobreviven con gran dificultad, el entorno es hostil, los recursos son limitados, y alrededor andan mutantes agresivos.*


*Tu tarea es sobrevivir y encontrar el legendario refugio tras las paredes donde hay posibilidad de una vida normal.*


'Heat Guardian' es de esos raros pero encomiables ejemplos de videojuego desarrollado por **un solo desarrollador** desde Ucrania, Denis Rudoy lo que le da aún mas mérito si cabe.


En cuanto al juego, por su perspectiva y movimientos, me recuerda poderosamente al también notable y espeluznante '[Darkwood](https://steamcommunity.com/app/274520)', aunque aquél era mas terror que acción. Según notifican en el artículo, el desarrollador dice que la versión para Linux está ya siendo desarrollada y podemos esperarla en los próximas semanas. Estaremos atentos a las novedades de este 'Heat Guardian'.


Si quieres saber más, puedes visitar la [página web oficial](http://heatguardiangame.com/) del juego.


¿Que te parece 'Heat Guardian'? ¿Te gustan los juegos de acción en vista cenital?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

