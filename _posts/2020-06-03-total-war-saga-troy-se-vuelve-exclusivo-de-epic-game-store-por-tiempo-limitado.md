---
author: Pato
category: Estrategia
date: 2020-06-03 20:33:21
excerpt: "<p>La versi\xF3n para Linux supuestamente a cargo de Feral Interactive,\
  \ de momento sin comentarios</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/TotalWarTroy/troy-totalwar.webp
joomla_id: 1229
joomla_url: total-war-saga-troy-se-vuelve-exclusivo-de-epic-game-store-por-tiempo-limitado
layout: post
tags:
- rts
- feral-interactive
- feral
- total-war
- troy
title: 'Total War Saga: TROY se vuelve exclusivo de Epic Game Store por tiempo limitado'
---
La versión para Linux supuestamente a cargo de Feral Interactive, de momento sin comentarios


**Sega y Creative Assembly** hicieron ayer un movimiento cuanto menos inusual al anunciar que su próximo título de estrategia "**Total War Saga: TROY**" **va a ser exclusivo de la tienda Epic Game Store,** cuyo lanzamiento está fijado para el próximo día 13 de Agosto y **además gratis durante las primeras 24 horas de estreno**, dejando luego un año completo de exclusividad con dicha tienda para luego llegar a Steam ya bien entrado el 2021. Bueno, quizás no sea un movimiento tan sorpresivo si tal y como se cuenta [en el propio blog del juego](https://www.totalwar.com/blog/a-total-war-saga-troy-on-epic-games-store/) **Epic ha sacado la billetera para "comprar" la exclusiva y la gratuidad con un buen montón de dinero y así pagar todas las copias que los jugadores reclamen**. Por supuesto, el estudio dice como es obvio que todo es "por el bien de la comunidad de jugadores y el futuro de la franquicia".


Si bien hasta ahora todos los títulos "Total War" nos han ido llegando gracias al acuerdo que **Feral Interactive** tiene con estas compañías, es bien sabido que la tienda de Epic no ofrece soporte para juegos nativos en Linux, y ni siquiera su tienda tiene un cliente que pueda gestionar los juegos en nuestro sistema favorito.


De este modo, la versión nativa del juego que [Feral nos tenía anunciada](index.php/component/k2/1-blog-principal/estrategia/1234-troy-un-nuevo-juego-de-la-saga-total-war-tendra-version-nativa-para-gnu-linux) de momento queda en el aire a la espera de saber si cuando termine la exclusividad con Epic tendremos la suerte de poder jugar el nuevo juego de la franquicia Total War, eso sí sin la suerte de que nos regalen el juego.


Lo que sí parece cierto es que la jugada no ha debido sentar nada bien en las oficinas de Feral, pues al ser preguntados por lo sucedido, han despachado un escueto tweet diciendo "no tenemos nada que decir respecto a Total War Saga: TROY".



> 
> We currently have nothing we can share regarding A Total War Saga: TROY on macOS or Linux.
> 
> 
> — Feral Interactive (@feralgames) [June 2, 2020](https://twitter.com/feralgames/status/1267844880551862273?ref_src=twsrc%5Etfw)


  





También es cierto que cabe una (remota) posibilidad de que en una acción sin precedentes la versión Linux se estrene en la tienda de Feral directamente, con descarga desde sus propios servidores... pero sería una sorpresa mayúscula que eso sucediera.


Sea como sea, hasta 2021 es muy probable que no tengamos una versión nativa del juego, y eso será cuando llegue a Steam, si es que al final Feral nos la trae.


Puedes leer el anuncio y los detalles en [este post oficial](https://www.totalwar.com/games/troy/troy-faq/).


¿Que piensas del movimiento de Sega y Creative Assembly? ¿Piensas o pensabas jugar al nuevo juego de la franquicia Total War? ¿Que opinas sobre las exclusivas de Epic y su impacto en un juego que en teoría nos iba a llegar de forma nativa?


Cuéntamelo en los comentarios o en nuestros canales de [Telegram](https://t.me/jugandoenlinux "https://t.me/jugandoenlinux") y [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org "https://matrix.to/#/+jugandoenlinux.com:matrix.org")


 


 

