---
author: Son Link
category: Estrategia
date: 2021-08-08 13:37:00
excerpt: "<p>El desarrollo del juego de estrategia software libre&nbsp;<span class=\"\
  css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">@play0ad</span> avanza para\
  \ actualizarse mediante nuevos a\xF1adidos</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/0ad/0ad_25.webp
joomla_id: 1331
joomla_url: lanzado-0-a-d-alpha-25-yauna
layout: post
tags:
- indie
- rts
- estrategia
- 0ad
title: "Lanzado 0 A.D. Alpha 25: Yaun\u0101"
---
El desarrollo del juego de estrategia software libre @play0ad avanza para actualizarse mediante nuevos añadidos


Tras varios meses de desarrollo, y un retraso de casi un mes respecto a las fechas iniciales (entre el 20 y 30 de junio), ha sido liberada la nueva alpha 25 de 0 A.D., **Yaunā**, también llamada Jonia en la Antigua Grecia y que era una región situada en las actuales Grecia y Turquía.


Esta nueva versión, además de corrección de fallos, así como ampliar algunas características y ampliar funcionalidades, trae estas novedades:


* **Soporte para campañas de un solo jugador**: Los modders pueden crear campañas para un solo jugador, si bien esta versión trae lo básico para futuras versiones se irá ampliando y añadiendo campañas propias.
* Mejora de la capacidad de respuesta del multijugador: Una mejora técnica que los jugadores pueden "sentir" más que ver, es otro paso de **mejora progresiva para suavizar el juego multijugador** y mejorar la búsqueda de rutas de las unidades. Antes los comandos en el multijugador se procesaban dos veces por segundo, ahora serán cinco veces como en el modo individual.
* Restructuración de pedidos – Puedes crear una cola infinita de pedidos y cambiarla sobre la marcha.


* Cambios de equilibrio en curso: Numerosos cambios en el equilibrio del juego mediante micro ajustes en varias civilizaciones, edificios y unidades.
* **18 mapas nuevos** de escaramuza.
* Opciones graficas extendidas.


* Varios cambios en la interfaz de juego: Arriba se ha cambiado la posición de la cantidad de ciudadanos que están recolectando cada recursos, así como un pequeño aumento de tamaño, lo que facilita mucho la visibilidad. El otro cambio situado en la esquina superior izquierda del mini-mapa **es el nuevo botón , el cual nos permite enviar avisos nuestros aliados**, como por donde nos atacan o por que lugar iniciar la ofensiva.
* Correcciones de errores, actualización de traducciones y otras mejoras.


 


![oad25alpha](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/0ad/oad25alpha.webp)


**Otra gran novedad es que se han mejorado los biomas del juego**, lo que viene a ser los entornos que te puedes encontrar por el mundo, formados por los animales, arboles, etc, gracias al trabajo del modder *wowgetoffyourcellphone* y su colaborador *maroder*, que ha ido añadiendo nuevas texturas en su mod, y que se han ido incorporando al juego. Además **los terrenos tendrán una resolución uniforme de 2k**, incluyendo mapas normales y especulares. Antes los terrenos tenían distintas resoluciones y pocos tenían mapas normales o especulares. Por otro lado los nuevos terrenos están libres de derechos, gracias a que están bajo la licencia CC0. Para esta nueva alpha se han añadido nuevos, otros han sido cambiados y **para las próximas versiones todos los biomas antiguos habrán sido reemplazados por unos nuevos**.


 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="440" src="https://www.youtube.com/embed/D3vxXZygHIk" title="YouTube video player" width="780"></iframe></div>


 


Puedes ver los cambios de la versión en la [wiki de actualización del juego](https://trac.wildfiregames.com/wiki/Alpha25#Art) y en su [web oficial](https://play0ad.com/sneak-peak-of-0-a-d-alpha-25/). ¿Que te parece esta nueva version de 0AD? Cuentanoslos en nuestro canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 


 

