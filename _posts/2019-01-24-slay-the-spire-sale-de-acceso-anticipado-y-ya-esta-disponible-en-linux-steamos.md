---
author: Pato
category: Estrategia
date: 2019-01-24 16:30:34
excerpt: "<p>El juego de @MegaCrit llega con una valoraci\xF3n excelente</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5cf8d68adfbc926b964baa0e9f5b8056.webp
joomla_id: 957
joomla_url: slay-the-spire-sale-de-acceso-anticipado-y-ya-esta-disponible-en-linux-steamos
layout: post
tags:
- estrategia
- roguelike
- cartas
title: "Slay the Spire sale de acceso anticipado y ya est\xE1 disponible en Linux/SteamOS"
---
El juego de @MegaCrit llega con una valoración excelente

Tras el [aparente fiasco](https://www.gamingonlinux.com/articles/valves-card-game-artifact-seems-to-be-dying-off-and-fairly-quickly-too.13413) que está viviendo el juego de cartas de Valve "Artifact" ahora nos llega '**Slay the Spire**', un juego de construcción de mazos con un enfoque distinto y enfocado en el juego para un solo usuario y que a tenor de las valoraciones de los usuarios en Steam parece haber dado con la tecla.


Hablamos de un juego que fusiona los juegos de cartas con elementos roguelike para, según las palabras de Mega Crit *crear el mejor juego de cartas para un solo jugador que pudiéramos. Crea un mazo único, encuentra criaturas extrañas, descubre reliquias de inmenso poder, Slay the Spire!*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/9SZUtyYSOjQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Entre las características destacadas se incluye la construcción dinámica de mazos, diseños dinámicos y poderosas reliquias. El juego que acaba de salir de acceso anticipado llega con:


* Tres personajes principales, cada uno de los cuales tiene su propio set de cartas..
* Más de 250 tarjetas totalmente implementadas.
* Más de 150 diferentes objetos a encontrar.
* Más de 50 encuentros de combate únicos.
* Más de 50 eventos misteriosos que pueden ayudarte o dañarte.
* Las Partidas Diarias te permiten compararte con todos los demás jugadores del mundo.
* El Modo Personalizado te permite mezclar y hacer coincidir varios modificadores de partidas locas.


Si quieres saber mas sobre Slay the Spire, puedes visitar su [página web oficial](https://www.megacrit.com/), su [página de Humble Bundle](https://www.humblebundle.com/store/slay-the-spire?partner=jugandoenlinux) (enlace patrocinado) o su [página de steam](https://store.steampowered.com/app/646570/Slay_the_Spire/).


¿Que te parecen los juegos de mazos? dado el fracaso de Artifact, ¿Le darás una oportunidad a Slay the Spire?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux)

