---
author: Pato
category: "Acci\xF3n"
date: 2018-02-13 17:49:10
excerpt: <p>Aparte de las noticias de hoy, tenemos unos cuantos lanzamientos destacables</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4fbac1ae9814b22f440e824a6b165325.webp
joomla_id: 644
joomla_url: ronda-de-lanzamientos-dead-maze-the-fall-part-2-unbound-y-crossing-souls-ya-disponibles-en-linux-steamos
layout: post
tags:
- accion
- indie
- aventura
title: "\xA1Ronda de lanzamientos! 'Dead Maze', 'The Fall Part 2: Unbound' y 'Crossing\
  \ Souls' ya disponibles en Linux/SteamOS"
---
Aparte de las noticias de hoy, tenemos unos cuantos lanzamientos destacables

Aparte de las noticias destacables de hoy, tenemos unos cuantos lanzamientos que no queremos pasar por alto. Vamos a repasarlos:


#### Dead Maze


![DeadMaze](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DeadMaze.webp)


Dead Maze es un juego "free to play" multijugador masivo en 2D con vista isométrica, ambientado en un mundo contemporáneo destruido, repleto de zombies en donde podrás ayudar a los otros jugadores para que tengan acceso a un mejor futuro. Coopera para restaurar la civilización, mejora tu propio campamento, y desvela la oscura narrativa del apocalipsis. Para sobrevivir, tendrás que trabajar en conjunto para explorar lo que queda del mundo luego del apocalipsis zombie, monitorerar tu vida y tu hambre, recoger recursos, y fabricar el mayor número de suministros salvavidas posibles.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/skjSGHt5Hmg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Lo tienes disponible en español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/667890/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


####  The Fall Part2: Unbound


![TheFallApart](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/TheFallApart.webp)


Secuela del premiado 'The Fall': *Eres una Inteligencia Artificial que se ha liberado de sus cadenas. Ahora, un humano “Manipulador” te ha atacado desde la red global y te ha infectado con un [ERROR: EDITADO]. Crea una nueva regla: Salvarte a ti misma. Cázalos. Elimínalos. Haz lo que sea. Usa a quien sea. Libérate.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/DQVCVPYwrZ0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Lo tienes disponible en español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/510490/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


####  Crossing Souls


![CrossingSouls](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/CrossingSouls.webp)


Editado por Devolver Digital (que suelen traernos casi todo lo que sacan a nuestro sistema favorito) nos llega esta aventura con estética ochentera "comic-pixel" de influencias en los grandes blockbusters de entonces como Los Goonies, Gremlins, Regreso al Futuro y muchas mas.


*Es 1986 en California. Un grupo de amigos descubre una misteriosa piedra rosa que permite viajar entre dos reinos. La pandilla vivirá el verano de sus vidas en una aventura que los involucrará en una conspiración del gobierno. Controla a cinco chavales con habilidades especiales mientras luchas y resuelves rompecabezas para salvar a sus familias y al mundo entero.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/f-6S9wNRd-Y" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Lo tienes disponible en español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/331690/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Que te parecen estos lanzamientos? ¿Te interesa alguno en especial?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

