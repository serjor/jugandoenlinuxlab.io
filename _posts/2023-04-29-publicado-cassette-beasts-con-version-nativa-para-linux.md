---
author: Odin
category: Rol
date: 2023-04-29 13:49:00
excerpt: "<p>Los desarrolladores indie <a href=\"https://bytten-studio.com\" target=\"\
  _blank\" rel=\"noopener noreferrer\">Bytten Studio</a> han publicado por fin este\
  \ magnifico <strong>JRPG </strong> de mundo abierto con sabor a retro.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/CassetteBeasts/CassetteBeasts_keyart_16_9.webp
joomla_id: 1533
joomla_url: publicado-cassette-beasts-con-version-nativa-para-linux
layout: post
tags:
- rol
- jrpg
- godot
title: "Publicado Cassette Beasts con versi\xF3n nativa para Linux."
---
Los desarrolladores indie [Bytten Studio](https://bytten-studio.com) han publicado por fin este magnifico **JRPG**  de mundo abierto con sabor a retro.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/_OLST_Fw5Ms" title="YouTube video player" width="720"></iframe></div>


Desde que en **JEL** probamos la **demo** que lanzaron durante el **NextFest** nos habíamos quedado con ganas de poder disfrutar al completo de este prometedor juego **indie**.


Y es que había razones de sobra para que nos llamara la atención. La primera, su peculiar estilo gráfico, siendo en **3D** pero imitanto la perspectiva clásica de los juegos de rol de **16 bits**. La segunda, la jugabilidad estilo **Pokemon** pero con el giro de tuerca de que los propios personajes son los que se convierten en sus criaturas.


Además de esas dos razones, el juego muestra otras cualidades muy positivas como por ejemplo la **historia**, de la que se tiene una pequeña introducción en la demo, y que nos irá desgranando poco a poco tanto los personajes y la misteriosa isla donde viven. También hay que destacar que está traducido a múltiples idiomas entre los que se incluye el **español** cosa que hay que agradecer y que va a facilitar el poder seguir los diálogos y conversaciones de los protagonistas.


Desde el punto de vista técnico, estamos de enhorabuena, ya que tiene versión **nativa**, está **verificado** para **Steam Deck**, y se ha desarrollado mediante el motor de código abierto **Godot Engine**. Desde **JEL** os recomendamos que echéis un vistazo a la [entrevista](https://godotengine.org/article/godot-showcase-cassette-beasts/ "Godot Showcase Cassette Beasts") que les hicieron el equipo de Godot a [Bytten Studio](https://bytten-studio.com), para que podáis ver como ha sido su desarrollo. A nosotros nos ha interesado saber que son **usuarios** de **Linux**, lo que seguramente sea una de las principales razones por las que tenemos versión nativa.


En definitiva, os recomendamos probar la demo y si os gusta podéis comprar el videojuego completo con una **rebaja del 10 por ciento**, hasta el **3 de Mayo**, en **Steam**.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1321440/" width="646"></iframe></div>


¿Qué te parece **Cassette Beasts**? ¿Lo has probado? Cuéntanos como siempre tu opinión sobre este título tan particular en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

