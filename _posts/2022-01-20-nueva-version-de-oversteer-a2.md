---
author: leillo1975
category: Software
date: 2022-01-20 12:00:59
excerpt: "<p>La utilidad de configuraci\xF3n de volantes de <span class=\"css-901oao\
  \ css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">@bernat_arlandis</span> llega intersantes\
  \ novedades.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Oversteer/Oversteer.webp
joomla_id: 1294
joomla_url: nueva-version-de-oversteer-a2
layout: post
tags:
- logitech
- oversteer
- new-lg4ff
- bernat
- thrustmaster
- t150
- t300-rs
- fanatec
title: "Nueva versi\xF3n de Oversteer, la 0.7.0"
---
La utilidad de configuración de volantes de @bernat_arlandis llega intersantes novedades.


**ACTUALIZACIÓN 20-1-22 (0.7.0):** Nuestro amigo Bernat nos acaba de comunicar la disponibilidad de una nueva versión de su completo software para la configuración de volantes, que viene con los siguientes cambios con respecto a la anterior versión:


-Se ha añadido a la lista de volantes compatibles el Logitech G923, el Thrustmaster T150 y el T500RS .  
-Añadidas traducciones al alemán, finlandés y turco.  
 -Instalación de las reglas udev por defecto en lugares sensibles.  
 -Varias correcciones de errores.


Si quereis descargar esta nueva **versión 0.7.0** podeis descargar desde [aquí](https://github.com/berarma/oversteer/releases/tag/0.7.0). Ya sabeis, si sois usuarios de esta fantástica utilidad, toca actualizar.


 




---


**NOTICIA ORIGINAL 26-3-21 (0.6.0):** Hacía tiempo que no hablábamos de esta aplicación imprescindible para todos aquellos que disfrutamos de conducir virtualmente con nuestros volantes, pero eso no quiere decir para nada que su creador, @Bernat (creador también de [new-lg4ff]({{ "/tags/new-lg4ff" | absolute_url }}), el driver para volantes Logitech) haya estado mirando para las musarañas, pues **esta nueva versión viene hasta los topes de novedades**. Si sigués su [proyecto en Github](https://github.com/berarma/oversteer) sabreis de lo que estoy hablando. Para que veais de lo que os hablo aquí os dejo la lista de cambios de esta [versión 0.60](https://github.com/berarma/oversteer/releases/tag/0.6.0):


* Nueva **gestión mejorada de los perfiles**.
* Nuevas **funciones para ejecutarse como complemento** de otra aplicación/juegos. Se tiene que configurar el lanzador del juego para que arranque Oversteer en su lugar y lance el juego.
* Añadidas **pruebas de rendimiento** del volante en una nueva pestaña.
* El rango máximo del volante se adapta al modelo.
* Se pueden usar varios **botones simultáneos para activar/desactivar la función de cambiar el rango** de rotación desde el volante evitando así inutilizar un botón.
* Compatibilidad mejorada en la carga/guardado de perfiles.
* Nueva **función para centrar** el volante.
* Añadidos volantes **Thrustmaster** T300RS, **FANATEC** CSL Elite, FANATEC ClubSport V2/2.5 y FANATEC Podium DD1. Necesitan controladores que están en desarrollo. Ver [README](https://github.com/berarma/oversteer/blob/master/README.md).
* Añadidos volantes **Logitech Wingman FG y Wingman FFG**.
* **Cambios en la interfaz de usuario** para hacerla más compacta y reorganización de algunos controles.
* Solucionados problemas en la detección de dispositivos y sus características.
* **Solucionados problemas usando la aplicación en los lanzadores de Steam.**
* Solucionados problemas en el posicionamiento de ventanas modales.
* Solucionados múltiples errores en la gestión de los ajustes.
* Arreglada la lectura de los **pedales para Logitech Momo**.


Como veis los cambios se centran principalmente en el soporte a nuevos modelos de volantes ([T150, T300RS]({{ "/posts/t150-driver-y-hid-tmff2-dos-nuevos-drivers-para-volantes-thrustmaster" | absolute_url }}), [Fanatec](https://github.com/gotzl/hid-fanatecff) ...), sección de pruebas de rendimiento y muchas mejoras en la interfaz. Ahora solo queda que le saqueis partido usándolo con vuestros juegos favoritos. Nos vemos [en las carreras](https://matrix.to/#/#zona_racing-jugandoenlinux:matrix.org?via=matrix.org&via=t2bot.io&via=hope.net)....

