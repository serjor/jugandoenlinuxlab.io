---
author: Serjor
category: "An\xE1lisis"
date: 2020-01-06 18:38:47
excerpt: "<p>Os traemos unas primeras impresiones de Shadow of The Tomb Raider</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ShadowOfTombRaider/shadow_of_the_tomb_raider.webp
joomla_id: 1146
joomla_url: primeras-impresiones-de-shadow-of-the-tomb-raider
layout: post
tags:
- analisis
- shadow-of-the-tomb-raider
- primeras-impresiones
title: Primeras impresiones de... Shadow Of The Tomb Raider
---
Os traemos unas primeras impresiones de Shadow of The Tomb Raider


*Antes de nada, queremos agradecer a [Feral Interactive](http://www.feralinteractive.com/es/) el habernos facilitado una clave para poder probar el juego.*


Por dar algo de contexto, el port nativo de Shadow Of The Tomb Raider ha sido uno de los más esperados por la comunidad linuxera desde que este fue anunciado. Muchos dudaban realmente de que al final se hiciera, porque gracias a protón, la versión para Windows funcionaba en GNU/Linux muy bien, así que el esfuerzo por traer una versión nativa, a estas alturas, es más que encomiable, digna de mención, y por supuesto, hay que valorar adecuadamente.


Este port además, se veía también como un hito en el desarrollo de ports nativos, y es que como comentábamos en el párrafo anterior, la versión de Windows funciona bien en GNU/Linux, con lo que se corría el riesgo de llegar tarde y llegar mal, con lo que podría convertirse en el primero de los últimos juegos a ser portados de manera nativa si la jugada no salía bien. No sabemos si el port ha llegado tarde, porque eso es algo que Feral tiene que valorar a la hora de hacer balance económico, pero ¿ha llegado mal?


Por ser justos con el juego, no puedo responder completamente a esta respuesta, ya que como reza el título, esta entrada pretende ser unas primeras impresiones después de unas horas de juego y sin haberlo terminado, no un análisis como tal, pero me temo, y hasta cierto punto, no ha llegado todo lo bien que debiera, y me explico.


En las pocas horas que llevo (unas ocho según Steam), me he encontrado ya con algunos problemas. Por un lado, el problema que me encontré mientras grababa el unboxing del juego para nuestro canal de YouTube, que podéis ver [aquí](https://www.youtube.com/watch?v=DajAGniweYo). Al final del mismo se puede ver cómo cuando intento acceder al menú de configuración del Steam Controller, el juego se cierra. Es cierto que no es un problema grave, y no puedo achacarlo única y exclusivamente al juego, pero el juego no se debería cerrar inesperadamente.


Cierres inesperados a parte, en las ocho horas que llevo me he encontrado problemas de renderizado, como problemas con las luces, en concreto, al entrar en cuevas Lara enciende una linterna de manera automática, pero en mi caso, no verse el haz de luz. También en una ocasión el juego renderizó dos Jonah (un personaje que nos acompaña a lo largo de nuestra aventura), el que interactuaba con nosotros y otro que simplemente estaba en medio del escenario sin moverse.


Es cierto que estos últimos fallos podrían venir del código original, y simplemente reproducirse del mismo modo que si jugara en Windows, y que se solucionan cerrando y abriendo el juego, pero sin ser graves, son molestos.


Como decía, es lo que me he encontrado en las ocho primeras horas, y puede que no me vuelva a encontrar más, pero hay que decirlo.


Pero por lo demás, técnicamente, el juego funciona a las mil maravillas. Cuando todo va bien, el juego luce de manera espectacular, las animaciones están muy bien (se nota la mano de Square Enix detrás), la inteligencia artificial, sin ser de lejos la mejor del mundo, cumple, y el control del personaje con el mando es más que correcto.


Y si en el apartado técnico no hay quejas (al margen de los problemas comentados), en todo lo demás, sí. A ver si soy capaz de explicarme sin confundir a nadie. El juego es un sucesor más que digno de los anteriores Tomb Raiders, mantiene su esencia y seguimos buscando recursos y tesoros, cazando, resolviendo puzzles, enfrentándonos a enemigos o mejorando nuestras habilidades según ganamos experiencia como hacíamos en las anteriores entregas, y además, han introducido mecánicas nuevas durante el combate para poder aprovechar el entorno y atacar usando el sigilo, cosa que los fans de los juegos de sigilo agradecemos completamente.


El caso es que si bien cada aspecto del juego está cuidado y por separado funciona, en el conjunto no lo hace, y esto lo convierte, y aquí sí es una opinión completamente personal y **subjetiva**, en un juego anodino, un tanto insulso que no pide ser jugado. La historia no interesa, los puzzles son más de lo mismo, en los combates, si te descubren, acaban siendo iguales que en las entregas anteriores, y el reto solamente está en hacer esa parte sin que te vean. Del mismo modo, andar por la jungla es aburrido, y buscar recursos un tedio. En definitiva, una suma de pequeños *no*, que de manera aislada pasarían desapercibidos, pero que al final suman demasiado como para que el juego (***me***) enganche.


Sé que no es justo no haberme acabado el juego y decir que me aburre, pero el que me aburra es lo que hace que no tenga ganas de acabarme el juego. De todos modos, si te gustan los Tomb Raider, no hagas caso de mis palabras. El juego está valorado como "muy positivo" en Steam, y mirando en páginas como Metacritic y Opencritic vemos que no es un juego de 10, pero sí notable, así que seré yo, o será que según avanza el juego la cosa mejora.


Y si queréis ver cómo se mueve, os dejo un gameplay de Leillo1975, quién retomó la aventura donde yo lo dejé:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/_F84IlhU9I4" width="560"></iframe></div>


Y tú, ¿qué opinas de este Shadow of The Tomb Raider? Cuéntanos tus impresiones en nuestros canales de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux)

