---
author: leillo1975
category: Rol
date: 2022-02-03 10:27:02
excerpt: "<p>Sus creadores, el estudio <span class=\"css-901oao css-16my406 r-poiln3\
  \ r-bcqeeo r-qvutc0\">@BrytenwaldaTeam</span>, acaban de lanzarlo.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/BeyondMankind/BeyondMankindLinux.webp
joomla_id: 1422
joomla_url: beyond-mankind-llega-finalmente-a-gnu-linux-de-forma-nativa
layout: post
tags:
- rpg
- postapocaliptico
- nativos
- beyond-mankind
title: Beyond Mankind llega finalmente a GNU-Linux con soporte nativo
---
Sus creadores, el estudio @BrytenwaldaTeam, acaban de lanzarlo.


 La verdad es que le teníamos echado el ojo a este juego, y personalmente debo reconocer que  estaba en mi lista de deseados desde hace tiempo, Y es que por norma general estos juegos de **Rol-Acción con temática futurista y postapocaliptica** me gustan bastante. El caso es que a finales del pasado verano se lanzó con soporte para Windows, quedando pendiente la versión nativa de nuestro sistema, y **tras algunos parches que han mejorado el juego sustancialmente y en todos los aspectos**, finalmente ya podremos disfrutarlo como se merece. Aquí os dejamos el tweet que anunciaba la inminente llegada del port a nuestro sistema:



> 
> Beyond Mankind, our scifi postapocalyptic indie Action [#RPG](https://twitter.com/hashtag/RPG?src=hash&ref_src=twsrc%5Etfw) is coming out TOMORROW on [#Linux](https://twitter.com/hashtag/Linux?src=hash&ref_src=twsrc%5Etfw)! 🚀 [#indiedevhour](https://twitter.com/hashtag/indiedevhour?src=hash&ref_src=twsrc%5Etfw) [#indiedev](https://twitter.com/hashtag/indiedev?src=hash&ref_src=twsrc%5Etfw) [#indiegame](https://twitter.com/hashtag/indiegame?src=hash&ref_src=twsrc%5Etfw)  
>   
> Go check it out for [#WishlistWednesday](https://twitter.com/hashtag/WishlistWednesday?src=hash&ref_src=twsrc%5Etfw)! <https://t.co/LE43upjLLA> [pic.twitter.com/WlJjq74XWy](https://t.co/WlJjq74XWy)
> 
> 
> — BEYOND MANKIND 🚀 - 30% OFF! (@BrytenwaldaTeam) [February 2, 2022](https://twitter.com/BrytenwaldaTeam/status/1488912505476993024?ref_src=twsrc%5Etfw)


 Para quien no conozca a sus desarrolladores, [Brytenwalda Studios](https://www.brytenwaldastudios.com/) es una **compañía internacional ubicada en nuestro pais**, concretamente en **Santander**, pero tiene miembros que trabajan desde distintos paises como Estados Unidos o Dinamarca. Fue fundada en 2014 por siete socios que habían desarrollado como un hobby el conocido **mod para Mount&Blade: Warband: Brytenwalda**. También desarrollaron con el mismo motor el exitoso **DLC Viking Conquest**. Es este Beyond Mankind su proyecto más ambicioso hasta la fecha.


Como antes os comentamos, [Beyond Mankind](https://www.brytenwaldastudios.com/?games=14) es un **RPG de acción postapocalíptico y de ciencia ficción** que homenajea a los clásicos del género de las últimas décadas, y combina **mecánicas clásicas de rol con elementos de supervivencia y un complejo sistema de diálogo**. Esta es la descripción oficial del juego:


*Beyond Mankind es un RPG de acción que rinde homenaje a los clásicos de principios de los 2000, y que muestra una narrativa madura y cuidada, poniendo al jugador frente a dilemas existenciales, los mismos a los que se enfrenta la humanidad en su conjunto.  
Año 2121, la Tierra está en manos de una nueva raza humana modificada. Tú, con recuerdos implantados de un mundo ya muerto, estás en el centro de un plan para recuperar la Tierra.*


*Crea tu personaje y embárcate en una misión para ayudar a la civilización humana, que actualmente se encuentra exiliada fuera del planeta. Serás parte de H.O.P.E., una unidad militar de élite a la vanguardia de la guerra para recuperar la Tierra. Empezarás bajo las ruinas de lo que una vez fue la ciudad de Avalon en la isla de Santa Catalina. Desde ahí, te embarcarás en un viaje épico que pondrá a prueba tanto tu carácter como tu habilidad.*


*A medida que juegues, te encontrarás desafíos inesperados con soluciones sorprendentes. La interacción social en el juego es profunda, y desarrollarás alianzas, lealtades e incluso romances con otros personajes en base a tu personalidad. Todo esto mientras cubres tus necesidades básicas para no pasar hambre, sed ni frío, y monitorizas tu estado psicológico.*


![BM01](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/BeyondMankind/BM01.webp)


 Si quereis disfrutar de este juego **vuestro equipo debe cumplir al menos estos requisitos**, que como veis no son muy exigentes:



+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** 64-bit Ubuntu 12.04 or 14.04 or Steam OS
+ **Procesador:** Intel Corei5-6500 / AMD Ryzen 5 PRO 2400G
+ **Gráficos:** NVIDIA® GeForce® GTX 660 / AMD Radeon HD 7800-serie
+ **Almacenamiento:** 22 MB de espacio disponible
+ **Notas adicionales:** NVIDIA® GeForce® GTX 660 / AMD Radeon HD 7800-serie


 Como podeis ver abajo en el widget de Steam tiene además un precio genial, y aun encima ahora mismo está rebajado un 30%, por lo que cuesta menos de 12€. ¡Una ocasión excepcional!



<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1301590/" width="646"></iframe></div>


  Nosotros por nuestra parte y gracias a la colaboración del estudio, intentaremos realizar a lo largo de este fin de semana un directo para que lo veamos todos más de cerca, y poder recibir mientras vuestro feedback sobre él. Permaneced atentos a nuestras redes sociales en [Twitter](https://mobile.twitter.com/jugandoenlinux) y [Mastodon](https://mastodon.social/@jugandoenlinux) para la fecha y la hora de esta retransmisión. Os dejamos con el trailer recien lanzado del lanzamiento en nuestro sistema:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/UAeYelMAzeY" title="YouTube video player" width="780"></iframe></div>


¿Qué os parece esta propuesta de Brytenwalda Studios? ¿Os gustan este tipo de juegos de Rol y Acción? Cuentanoslo en nuestro grupo de JugandoEnLinux en [Telegram](https://t.me/jugandoenlinux), o nuestras salas de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org).

