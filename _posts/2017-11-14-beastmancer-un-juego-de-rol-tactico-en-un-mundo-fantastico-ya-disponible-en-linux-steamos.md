---
author: Pato
category: Rol
date: 2017-11-14 19:18:00
excerpt: "<p>A falta de Pokemons, captura monstruos y l\xE1nzalos a luchar en combates\
  \ por turnos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/668fe285e7b87ea59a0ba9aff307fa0b.webp
joomla_id: 528
joomla_url: beastmancer-un-juego-de-rol-tactico-en-un-mundo-fantastico-ya-disponible-en-linux-steamos
layout: post
tags:
- indie
- rol
- estrategia
- turnos
- tactico
title: "'Beastmancer' un juego de rol t\xE1ctico en un mundo fant\xE1stico ya disponible\
  \ en Linux/SteamOS"
---
A falta de Pokemons, captura monstruos y lánzalos a luchar en combates por turnos

Normalmente hay tipos de juegos que son raros de ver, sobre todo por que suelen ser desarrollos para otras plataformas, pero hoy recogemos el testimonio de [Liam en gamingonlinux](https://www.gamingonlinux.com/articles/beastmancer-a-monster-taming-tactical-rpg-is-now-on-linux.10686) sobre este 'Beastmancer'.


Se trata de un juego que recuerda mucho a los "Pokemon", donde tu misión como... ¿personaje? es básicamente buscarte uno o varios monstruos "mascota" y luchar contra otros monstruos para atraparlos también.


*"Beastmancer [[web oficial](http://www.autarcadev.com/#beast)] es un RPG táctico de domador de mónstruos. Juega como un aventurero en una tierra de fantasía, captura monstruos y úsalos para luchar en combates por turnos.*


*Has sido invitado a un misterioso torneo de Beastmancers en un remoto archipiélago con la promesa de riquezas y gloria. Lucha contra enemigos poderosos, captura monstruos y descubre el secreto detrás del torneo."*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Cm2i9jhOSbk" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Detrás de 'Beastmancer' está una sola persona. Un desarrollador que se hace llamar "[Autarca](http://www.autarcadev.com/)" y que ya nos trajo hace un tiempo 'Nomad Fleet' [[Steam](http://store.steampowered.com/app/371010/Nomad_Fleet/)]. Investigando un poco más, el autor se llama Jorge y es Chileno. Desde luego, siendo obra de una sola persona, el desarrollo de semejante juego es encomiable.


El juego salió para Linux/SteamOS el pasado día 1, pero según lo que cuenta Liam no está exento de algunos bugs y falta por pulir algunos aspectos del juego, pero si te llama la atención y quieres darle un vistazo, lo tienes disponible en su página de Steam. Eso sí, no está traducido al español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/562250/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


¿Qué te parece Beastmancer? ¿Los Pokemon te saben a poco y prefieres capturar otro tipo de bestias? 


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

