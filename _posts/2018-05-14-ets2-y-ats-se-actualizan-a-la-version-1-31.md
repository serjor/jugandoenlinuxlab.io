---
author: leillo1975
category: "Simulaci\xF3n"
date: 2018-05-14 18:53:26
excerpt: <p><span class="account-inline txt-ellipsis"><span class="username txt-mute">@SCSsoftware
  actualiza sus juegos con importantes novedades</span></span></p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b98eb4e6d4e5af022817653939abd5f0.webp
joomla_id: 733
joomla_url: ets2-y-ats-se-actualizan-a-la-version-1-31
layout: post
tags:
- ets2
- american-truck-simulator
- ats
- actualizacion
- euro-truck-simulator-2
title: "ETS2 y ATS se actualizan a la versi\xF3n 1.31."
---
@SCSsoftware actualiza sus juegos con importantes novedades

Tras casi un mes de [beta pública](https://steamcommunity.com/app/227300/discussions/0/412447331651632209/?ctp=6#c3211505894135229612) y bastantes cambios durante este periodo, finalmente la compañá checa responsable de Euro Truck Simulator 2 y American Truck Simulator publica hoy la versión definitiva de esta [actualizacíon](http://blog.scssoft.com/2018/05/ats-and-ets2-update-131-out-now.html). El anuncio nos llegaba esta misma tarde a través de Twitter:



> 
> Update 1.31 Out Now! <https://t.co/Ira5Rt7Jzv> [pic.twitter.com/lxKJ8Hp9zG](https://t.co/lxKJ8Hp9zG)
> 
> 
> — SCS Software (@SCSsoftware) [May 14, 2018](https://twitter.com/SCSsoftware/status/996053415158599680?ref_src=twsrc%5Etfw)



 La versión 1.31 de **ambos juegos** trae importantes novedades que son las siguientes:


 -Mangueras espirales eléctricas y de aire entre el camión y la gabarra.


 -Render de los espejos mucho más realista basado en el campo de visión de la posición de la cabeza


 


**Euro Truck Simulator 2** añade lo siguiente:


 -Eventos en carretera (controles policiales, accidentes, incendios, obras)


 -Chasis 8x4 de Scania R y S


 -Nuevas rutas para la [DLC de transportes especiales](https://youtu.be/3mGYjc67buk).


 -Piezas y pinturas exclusivas "[Mighty Griffin](http://blog.scssoft.com/2018/03/mighty-griffin-additions-under.html)" para los Scania S & R


 


**American Truck Simulator** nos trae:


 -Nueva carretera por el parque [Yosemite](http://blog.scssoft.com/2018/03/tioga-pass.html) (CA-120)


 -Nuevos Trailers (con volquetes inferiores)


 


Si quereis ver las novedades que tiene esta actualización podeis ver los siguientes videos que grabamos hace unas semanas:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/3gzggw7Y92Y" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/BeEUjMfcSP4" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Habeis probado ya las novedades de estas actualizaciones? ¿Qué os parecen los cambios que han realizado? Déjanos tu opinión en los comentarios o en nuestros grupos de [Telegram](https://t.me/jugandoenlinux) y [Discord](https://discord.gg/VZGNYP).

