---
author: leillo1975
category: Software
date: 2018-01-30 09:58:02
excerpt: "<p>Tras varias candidatas finalmente ha llegado a la versi\xF3n final.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6febd4c4384e8f172d72aaeb470b230f.webp
joomla_id: 623
joomla_url: el-motor-de-juegos-godot-llega-finalmente-a-su-version-3-0
layout: post
tags:
- multiplataforma
- motor-grafico
- godot
- desarrollo
- engine
title: "El motor de juegos GODOT llega finalmente a su versi\xF3n 3.0."
---
Tras varias candidatas finalmente ha llegado a la versión final.

Si hay un desarrollo que está acaparando ultimamente la atención de los desarrolladores ese es [Godot](https://godotengine.org/). Como muchos sabreis, se trata de un **IDE** que permite a los desarrolladores  crear videojuegos con un conjunto de herramientas unificadas, con la particularidad de que se trata de **Software Libre** (licencia MIT) **Multiplataforma** (GNU-Linux, Windows y MacOS, FreeBSD...), con lo que eso obviamente implica. En el día de ayer (29 de Enero de 2018) se ha lanzado la versión 3.0 definitiva tras 18 meses de trabajo.


![GodotJuego2 copiar](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/GODOT/GodotJuego2_copiar.webp)


Como muchos de vosotros sabreis, Godot es un **motor gráfico para crear videojuegos 2D y 3D** que permite exportar proyectos a multiples plataformas tanto móviles (Android e iOS), como de Escritorio (Windows, MacOS, Linux, BSD), web y Consolas. Entre sus principales ventajas está, a parte de la antes mencionada licencia libre, es el poder trabajar con **GDScript**, un lenguaje propio basado en Python, con lo que eso implica en facilidad de uso; el **uso de nodos** tanto predefinidos como la posibilidad de crearlos; creación e integración de herramientas propias; lenguaje de nivel alto y autocompletado de código, y **multitud de efectos gráficos** como postprocesado, HDR, shaders. Probablemente haya muchisimas ventajas más, pero reconozco que soy un completo neófito en este tema y prefiero no escribir de lo que no se, por lo que pido que si alguno de vosotros está al tanto de el desarrollo y uso de este IDE nos deje más información en los comentarios.


Con respecto a esta ultima versión, la 3.0, sus principales [novedades](https://godotengine.org/article/godot-3-0-released) con respecto a la anterior son:


-Nuevo **renderizador 3D** basado en Físicas, lo que mejora considerablemente con respecto al anterior.


-**BSDF de principios completo de Disney**, lo que implica que es el primer motor en ofrecer el rango completo para rendering basado en físicas.


-**Medio- y Post-Procesado**: Hay un nuevo mapeador de tonos con soporte para HDR, múltiples curvas estándar y exposición automática, reflejos, humo, profundidad de campo, y una poderosa implementación de SSAO.  



-**Materiales y Shaders**: el editor de materiales de la versión 2.1 fué eliminado por culpa de la compatibilidad, pero volverá en la versión 3.1. Mientras, se puede usar un material predeterminado extremadamente poderoso  (que admite texturas detalladas, mapeo triplanar y otras características) y un lenguaje de sombreado fácil de usar.


-**Partículas procesadas por GPU**, lo que permite millones de partículas por frame y efectos impresionantes.


-**Soporte para Mono y C#**: Debido a la demanda popular , ahora es posible crear un script completo con C# en Godot, aunque para ello será necesario usar una build especialmente diseñada para ello.


-**Visual Scripting**: Godot permite usar un lenguaje de script visual, usando cajas y conexiones. Esto es espacilmente útil para gente con pocos o nulos conocimientos de programación, así como artistas, diseñadores, etc.


![GodotRender copiar](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/GODOT/GodotRender_copiar.webp)


Existen multitud de novedades más como la inclusión de un **nuevo motor de sonido,** mejoras en la **liluminación global,** mejoras en el **GDScript**, soporte de **Realidad Virtual**, nueva API de **multiplayer en red**, y muchas más. Como veis, la nueva versión supone un salto enorme con respecto a la anterior versión, la 2.1, y permite al desarrollador muchas más posiblidades que antes. Godot 3.0 ha venido haciendo ruido desde el inicio de su desarrollo, y mucha gente le augura un gran futuro rivalizando incluso con el poderoso Unity3D, sobre todo en proyectos de bajo presupuesto. Su nulo coste, su documentación y comunidad, y por supuesto las inequívocas ventajas que ofrece que su código sea libre son sus bazas principales para ello.


Si quereis descargar Godot 3.0 podeis hacerlo desde su [página oficial](https://godotengine.org/download/linux) o incluso desde [Steam](http://store.steampowered.com/app/404790/Godot_Engine/), aunque en este último aún no han actualzado a esta nueva versión. Podeis ver un video con sus principales novedades justo aquí debajo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/XptlVErsL-o" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 ¿Sois desarrolladores de videojuegos o habeis hecho vuestros pinitos?¿Qué os parece lo que propone Godot? Deja tus respuestas en los comentarios, o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

