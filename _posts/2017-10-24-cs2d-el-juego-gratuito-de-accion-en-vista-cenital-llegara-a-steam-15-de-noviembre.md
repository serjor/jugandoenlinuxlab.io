---
author: Pato
category: "Acci\xF3n"
date: 2017-10-24 08:20:10
excerpt: "<p>La acci\xF3n de Counter Strike en 2D, disponible ya en su propia web</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d4ece76a0fc75091c09eca5036691827.webp
joomla_id: 501
joomla_url: cs2d-el-juego-gratuito-de-accion-en-vista-cenital-llegara-a-steam-15-de-noviembre
layout: post
tags:
- accion
- indie
- casual
- gratis
title: "'CS2D' el juego gratuito de acci\xF3n en vista cenital llegar\xE1 a Steam\
  \ el 15 de Noviembre"
---
La acción de Counter Strike en 2D, disponible ya en su propia web

'CS2D' es, como su nombre indica un "clon" de Counter Strike pero pasado a las 2 dimensiones y además totalmente gratuito. Y cuando decimos totalmente es totalmente. Según los propios desarrolladores no hay manera de gastar dinero en este juego. ¡Todo sea por la diversión!


'CS2D' es un juego de acción multiplataforma que basa su jugabilidad en su acción multijugador en línea (o local con bots) unos gráficos en vista cenital, con movimientos rápidos, múltiples modos de juego, el armamento se basa en el de Counter Strike 1.6 y soporta scripts, modificaciones y trae hasta un editor de niveles.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/0U5KmjuB2WU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


'CS2D' llegará a Steam el próximo día 15 de Noviembre tal y como han anunciado [en su foro](http://store.steampowered.com/app/666220/CS2D/?utm_source=SteamDB&utm_medium=SteamDB&utm_campaign=SteamDB%20App%20Page), sin embargo si queremos jugarlo ya, está disponible para su descarga [en su página web](http://www.cs2d.com/) donde tenemos la posibilidad de descargar incluso un servidor dedicado.


¿Qué te parece CS2D? ¿Hacen unas partidas online?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

