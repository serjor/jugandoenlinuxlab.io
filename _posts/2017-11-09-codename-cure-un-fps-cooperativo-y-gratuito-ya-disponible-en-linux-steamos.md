---
author: Pato
category: "Acci\xF3n"
date: 2017-11-09 16:26:47
excerpt: <p>Lucha solo o en cooperativo contra zombies</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0776cd174c6a8feb115a5ad1770aa06a.webp
joomla_id: 525
joomla_url: codename-cure-un-fps-cooperativo-y-gratuito-ya-disponible-en-linux-steamos
layout: post
title: '''Codename CURE'' un FPS cooperativo y gratuito ya disponible en Linux/SteamOS'
---
Lucha solo o en cooperativo contra zombies

El pasado día 31 de octubre salió a la venta este 'Codename CURE', un juego desarrollado con el motor Source en el que tendremos que enfrentarnos a hordas de zombies junto con hasta otros cuatro compañeros de equipo en misiones de supervivencia o por objetivos.


Puedes jugar a CURE en cooperativo online o solo junto a otros bots. El juego se adapta en todo momento al número de participantes, y lo mejor de todo ¡es que puedes jugarlo gratis!


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/2AqEW4eDjhY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Si quieres probar este 'Codename CURE' lo tienes para descargar traducido al español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/355180/" style='background-image: url("../img/iframe.png"); display: block; margin-left: auto; margin-right: auto;' width="646"></iframe></div>


¿Piensas echar alguna partida online con amigos a este 'Codename CURE? ¿Te van los juegos de matar zombies?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

