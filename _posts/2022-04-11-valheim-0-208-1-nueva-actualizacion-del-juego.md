---
author: Son Link
category: "Exploraci\xF3n"
date: 2022-04-11 13:49:14
excerpt: "<p>La actualizaci\xF3n del juego de Irongate <span class=\"css-901oao css-16my406\
  \ r-poiln3 r-bcqeeo r-qvutc0\">,&nbsp;<span class=\"css-901oao css-16my406 r-poiln3\
  \ r-bcqeeo r-qvutc0\">@Valheimgame</span>,&nbsp; acaba de ser lanzada.</span></p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/valheim.webp
joomla_id: 1460
joomla_url: valheim-0-208-1-nueva-actualizacion-del-juego
layout: post
tags:
- actualizacion
- valheim
title: "Nueva versi\xF3n de Valheim, la 0.208.1"
---
La actualización del juego de Irongate , @Valheimgame,  acaba de ser lanzada.


No hace mucho que se publico la [segunda gran actualización]({{ "/posts/valheim-0-207-20-descogenlando-las-cuevas-heladas" | absolute_url }}) de **Valheim**, pero hay a sido liberada una nueva versión menor mientras esperamos esa gran tercera actualización.


##### NOTAS DE LA REVISIÓN


* Se ha corregido un error por el que los trolls no se escalonaban correctamente a veces.
* Se ha corregido la leyenda de las teclas del mapa y se ha eliminado el icono del mapa del gamepad ajustado a LB.
* Varias pequeñas correcciones en las salas de la Cueva de la Escarcha
* Se ha corregido un error relacionado con la pausa después de estar en el modo de depuración y cambiar de servidor
* La función de actualización ahora recordará el objeto seleccionado cuando se actualice
* Se ha corregido un error por el que el número de la pila dividida a veces no era visible en resoluciones altas.
* Se ha corregido un error por el que a veces las fundiciones no utilizaban la cantidad correcta de carbón.
* Se ha corregido que algunos objetos no queden cubiertos por la nieve.
* El marcador de selección ahora es más visible en fondos claros cuando se utiliza el gamepad.
* Se ha añadido el comando de consola 'resetsharedmap' y varias mejoras en los devcommands
* Al comprar objetos al comerciante usando el gamepad ya no se compra dos veces, además de algunos ajustes en la interfaz
* Sugerencias de teclado y gamepad en la pantalla de inventario
* Al recoger tu lápida ahora intentará apilar objetos.
* Se ha solucionado un problema por el que las lápidas a veces no se podían alcanzar en los árboles del pantano.
