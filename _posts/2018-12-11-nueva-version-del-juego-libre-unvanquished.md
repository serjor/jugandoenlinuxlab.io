---
author: leillo1975
category: Estrategia
date: 2018-12-11 09:05:24
excerpt: "<p class=\"ProfileHeaderCard-screenname u-inlineBlock u-dir\" dir=\"ltr\"\
  ><span class=\"username u-dir\" dir=\"ltr\">@</span><span class=\"username u-dir\"\
  \ dir=\"ltr\">UNVofficial </span><a class=\"ProfileHeaderCard-screennameLink u-linkComplex\
  \ js-nav\" href=\"https://twitter.com/UNVofficial\"><span class=\"username u-dir\"\
  \ dir=\"ltr\"> </span></a>lanza la Alpha 51 tras m\xE1s de dos a\xF1os.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f50e2b492317c6f75474015f1cb554f0.webp
joomla_id: 928
joomla_url: nueva-version-del-juego-libre-unvanquished
layout: post
tags:
- accion
- estrategia
- open-source
- ioquake
- unvanquished
title: "Nueva versi\xF3n del juego libre Unvanquished."
---
@UNVofficial lanza la Alpha 51 tras más de dos años.

Otra vez tenemos una nueva versión de un juego libre encima de la mesa, y en esta ocasión le toca a "Unvanquished", un juego de **Acción mezclada con Estrategia en Tiempo Real** al estilo de Natural Selection, donde podremos jugar por equipos y que enfrenta a soldados humanos tecnológicamente avanzados contra hordas de extraterrestres altamente adaptables. Como os imaginareis la experiencia jugando en uno u otro equipo es completamente diferente, lo que le da un atractivo especial. Los humanos basarán su ataque en la potencia de fuego, mientras que los Alienígenas utilizarán sus rápidos movimientos y el sigilo para tomar ventaja. El objetivo final del juego es destruir las base enemiga y para ello p**odremos construir estructuras** que nos ayudarán tanto a atacar como a defender.


Este juego Open Source es un fork del conocido y también libre [Tremulous](http://www.tremulous.net/), y está desarrollado a partir del **motor Daemon**, una versión altamente modificada del archiconocido [ioquake3](https://ioquake3.org/), y que presenta mayores posibilidades y caracteristicas más avanzadas que este último. Aquí debajo teneis un gráfico de la evolución de este engine gráfico:


![Daemon Engine history.svg](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Unvanquished/Daemon_Engine_history.svg.webp)


Con respecto a esta nueva versión ([Alpha 51](https://www.unvanquished.net/?p=1278)), los desarrolladores han comentado que a pesar de la falta de noticias **el proyecto no estuvo parado ni mucho menos**, asegurando que incluso **la primera beta del juego podría estar ya muy cerca**. También comentan que a pesar de ser una alpha, se trata de una plataforma muy robusta y un motor muy confiable. Entre las novedades más importantes podemos encontrar las siguientes:



> 
> -Nuevo launcher que permite la actualización automática del juego antes de empezar a jugar, además de mostrar las últimas noticias sobre Unvanquished.
> 
> 
> -Los movimientos del jugador han sido modificados, permitiendo apoyarse en las paredes para alcanzar zonas más altas, se ha cambiado el deslizamiento y los humanos ya no pueden esquivar.
> 
> 
> -Se ha implementado un indicador de daño que nos informa da una respuesta visual de la efectividad de nuestras armas, de forma que sepamos cual es la mejor opción dependiendo de la situación.
> 
> 
> -Se ha limpiado mucho código y recursos del juego, quitando las cosas que estaban de más y los duplicados. Se hicieron correcciones en el conjunto de texturas y se redujo mucho el número de archivos, pasando a ser el tamaño del juego de 737 a 488 MB.
> 
> 
> -Se han estrenado nuevos modelos, como un traje de combate o el nuevo "dragon". También se ha repintado la mano que vemos sujetando las armas en primera persona.
> 
> 
> -Se han modificado gran cantidad de sonidos del juego
> 
> 
> -Arreglos varios en los mapas del juego, corrigiendo errores comunes y otros problemas que permitían que los jugadores tomaran ventaja de ellos.
> 
> 
> -Se han hecho arreglos en los bots, permitiendo que estos rellenen los servidores vacios, y se eliminen cuando entre un jugador humano al servidor.
> 
> 
> 


Existen muchos cambios más, pero sería difícil y tedioso enumerarlos, por lo que si quieres conocer más sobre esta actualización es mejor que lo consultes en el siguiente [link](https://www.unvanquished.net/?p=1278). Puedes descargar Unvanquished desde su página de [descargas](https://www.unvanquished.net/?page_id=318). Os dejamos con un gameplay para que podais haceros una idea más exacta de como es el juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/hfAaXZFXDGU" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


En JugandoEnLinux nos encantaría poder jugar unas cuantas partidas con nuestra comunidad a este fantastico juego. ¿Os apuntais? Muestra tu interés o danos tu opinión sobre el juego en los comentarios, o chatea en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

