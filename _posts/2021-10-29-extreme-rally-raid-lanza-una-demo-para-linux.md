---
author: leillo1975
category: "Simulaci\xF3n"
date: 2021-10-29 18:45:28
excerpt: "<p>Este juego en desarrollo de <span class=\"css-901oao css-16my406 r-poiln3\
  \ r-bcqeeo r-qvutc0\">@PuntoSimu se encuentra en estado Alpha.&nbsp;</span></p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ExtremeRallyRaid/ERR.webp
joomla_id: 1384
joomla_url: extreme-rally-raid-lanza-una-demo-para-linux
layout: post
tags:
- demo
- unity3d
- simulacion
- extreme-rally-raid
title: Extreme Rally Raid lanza una demo para Linux
---
Este juego en desarrollo de @PuntoSimu se encuentra en estado Alpha. 


Todos los que me conoceis un poco sabeis que soy un apasionado del Simracing, y desde hace algún tiempo conozco el trabajo de [PuntoSimu](http://www.puntosimu.com.ar/), ya que **han creado multitud de mods de muchos circuitos sudamericanos**. Llegué hasta ellos buscando información sobre el circuito de Interlagos para nuestro querido [Speed Dreams]({{ "/tags/speed-dreams" | absolute_url }}), y finalmente descubrí que la cosa no se quedaba solo ahí, ya que son autores de trabajos para principalmente rFactor, pero también para otros títulos como Assetto Corsa o GT Legends. El caso es que finalmente conocí su canal de [Youtube](https://www.youtube.com/channel/UCjgZu8132YQ9_mQ63vCtmbw), y... sorpresa! descubrí que estaban desarrollando un intersante juego sobre **Rallyes de tipo Raid**, algo poco habitual en el mundo de los juegos con honrosas excepciones como [Dakar 18]({{ "/tags/dakar-18" | absolute_url }}), aquel juego que finalmente no pudimos disfrutar en nuestro sistema.


Ni corto ni perezoso contacté con ellos preguntando si tendría soporte para nuestro sistema, y finalmente estamos aquí hablando de su juego que [acaba de estrenar demo nativa](https://puntosimu.itch.io/extreme-rally-raid/devlog/308613/demo-for-linux-released) para que todos podamos probar su propuesta. El título, que como os decimos **se encuentra en un estado temprano de su desarrollo**, está siendo creado con el famoso motor Unity, presenta **gráficos en 3D** y está enfocado al terreno de la **simulación**, ofreciendo carreras extremas, etapas largas y duras, y en entornos naturales con una menor intervención humana. **Las características implementará son las siguientes:**


* Mapas del mundo real: escenarios de localizaciones reales con elevación precisa y escala real.
* Modelos 3D originales, inspirados en coches reales.
* Conduce con un roadbook como los pilotos profesionales de rally.
* Física realista de los coches.
* Conductores con IA.
* Etapas de diferente duración, más cortas y más grandes, desde unos minutos hasta una hora o más de carrera.
* Clima y tiempo dinámicos.
* Daños visuales y mecánicos.
* Múltiples mandos permitidos: volante, teclado, ratón, gamepad, joystick.
* Diferentes tipos de carreras: Raid extremo, Raid de novatos, Shakedown, Desafíos.


El equipo de Extreme Rally Raid, tras el lanzamiento de esta demo, planea lanzar su juego en **Acceso Anticipado** durante el primer simestre del próximo año, y la versión final en la primera mitad de 2023. Podeis ver su hoja de ruta en la siguiente infografía:



![Hoja de Ruta](https://img.itch.zone/aW1nLzY4NjI4ODIuanBn/original/I1vEpy.jpg)


Desde aquí también nos complace comunicaros que **estamos tomando parte en el testeo de la versión de Linux** y que os convidamos a que probeis esta demo en vuestras repectivas distribuciones, comunicándonos a nosotros o al equipo de desarrollo, los problemas o sugerencias que tengais. Podeis usar para ello si quereis los comentarios de este artículo, nuestro grupo de JugandoEnLinux en [Telegram](https://t.me/jugandoenlinux), o nuestras salas de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org).


Si quereis descargaros la demo y ver más información sobre este juego podeis hacerlo en su página de [Itch.io](https://puntosimu.itch.io/extreme-rally-raid). Os dejamos ahora con el **gameplay de presentación** de la demo que han preparado en PuntoSimu:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/8RN5wwlwaww" title="YouTube video player" width="780"></iframe></div>

