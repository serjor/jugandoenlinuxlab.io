---
author: leillo1975
category: Estrategia
date: 2021-05-27 10:41:27
excerpt: "<p>Este juego #OpenSource es a su vez un fork del antiguo C-Evo.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/C-Evo/C-Evo.webp
joomla_id: 1311
joomla_url: descubre-c-evo-new-horizons-un-juego-inspirado-en-civilization-ii
layout: post
tags:
- civilization
- open-source
- c-evo
- new-horizons
- lazarus
- fork
title: 'Descubre C-Evo: New Horizons, un juego inspirado en Civilization II'
---
Este juego #OpenSource es a su vez un fork del antiguo C-Evo.


 Quizá algunos de los aquí presentes comenzaron en esto de los juegos con clásicos como [Civilization II.](https://es.wikipedia.org/wiki/Civilization_II) Si es así amigo, como ves ya ha pasado demasiado tiempo, porque el juego original es de 1998. Pero eso para muchos de nosotros no es un impedimento, pues los buenos juegos nunca dejan de gustar a pesar de que es más que evidente el paso de los años cuando los retomamos, especialmente si los comparamos graficamente con los juegos más actuales.


Como sabeis la saga Civilization es una de las más lonjevas y exitosas de la historia de los videojuegos, y por supesto es influencia para muchos otros títulos de la estrategia por turnos. Si bien es cierto que hace años conozco la existencia de [FreCiv](http://www.freeciv.org/) y [FreCol](http://www.freecol.org/), de este proyecto no tenía conocimiento hasta hace un momento, al descubrirlo en [Reddit](https://www.reddit.com/r/linux_gaming/comments/nm3ewx/cevo_a_fanmade_game_inspired_by_civilization_ii/). Inmediatamente me he puesto a buscar información sobre él y esto es lo que he encontrado.


"[C-Evo: New Horizons](https://app.zdechov.net/c-evo/wiki)" es un **juego Open Source de estrategia por turnos, escrito en Lazarus, que es fork de el descontinuado [C-EVO](http://www.c-evo.org/index.html)**, juego que por cierto solo tenía versión para Windows. Su creador, Chronos, ha tomado la última versión del código original y la ha portado añadiendo las siguientes características:


*-Todo desde el juego original C-evo 1.2.0*  
 *-Plataformas soportadas: Windows y Linux*  
 *-Arquitecturas soportadas: 32 bits y 64 bits x86*  
 *-Localizaciones incluidas: Checo, alemán, italiano, ruso, chino simplificado y tradicional*  
 *-Archivos gráficos convertidos de BMP a PNG*  
 *-Archivos de texto del juego .txt convertidos a la codificación UTF-8*  
 *-Eliminada la aplicación de configuración externa escrita en C#/.NET. Utilizada la interfaz de configuración del juego.*  
 *-Utilizado el último Delphi StdAI disponible.*  
 *-Añadidos scripts de instalación para Windows, Ubuntu/Debian y Fedora/RHEL/Centos Linux.*  
 *-Componentes en tiempo de diseño convertidos a paquete Lazarus (cevocomponents.lpk)*  
 *-Enlaces de teclas configurables por el usuario*  
 *-Mapa ampliable en tres niveles mediante la rueda del ratón*


Podeis descargar este juego desde su [página web](https://app.zdechov.net/c-evo/wiki), donde podreis encontrar el código fuente, además de versiones para Ubuntu y un enlace a un paquete [Snap](https://snapcraft.io/c-evo).


¿Fuisteis o sois jugadores de Civilization II? ¿Qué os parecen esta clase de proyectos? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://t.me/jugandoenlinux) o nuestras salas de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


