---
author: leillo1975
category: Software
date: 2017-10-30 12:09:27
excerpt: "<p>La primera versi\xF3n candidata llegar\xE1 a primeros de diciembre.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f7644b56cd0ae537784e23b64857cf80.webp
joomla_id: 513
joomla_url: la-version-3-0-de-wine-se-espera-para-finales-de-ano-con-direct3d-11
layout: post
tags:
- wine
- opengl
- directx
- windows
- alexandre-julliard
title: "La versi\xF3n 3.0 de Wine se espera para finales de a\xF1o con Direct3D 11\
  \ (ACTUALIZACI\xD3N)"
---
La primera versión candidata llegará a primeros de diciembre.

**ACTUALIZACIÓN 29-11-17:** [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Wine-3.0-Freeze-Next-Week) ha publicado ayer que a principios del mes que viene, **alrededor del 8 de Diciembre**, se publicará la primera Versión Candidata (**RC1)** de Wine 3.0, y que irán lanzando sucesivas actualizaciones durante el mes hasta llegar a **la versión final a mediados del mes de Enero** si todo va según lo esperado. Alexandre Julliard ha comentado que la semana que viene procederán al congelamiento del código fuente de este conocido wrapper para dedicarse a pulir bugs y optimizar. Esperemos que todo vaya como la seda y pronto estemos disfrutando de este fantastico software que en tantas ocasiones nos ha sacado de más de un apuro.


En JugandoEnLinux.com iremos actualizando este artículo a medida que se vayan publicando noticias relevantes relativas a este desarrollo.


 




---


**NOTICIA ORIGINAL:** Acabamos de leer en **[GamingOnLinux](https://www.gamingonlinux.com/articles/wine-30-expected-this-year-with-direct3d-11-roadmap-for-future-releases-includes-opengl-core-contexts.10630?utm_content=buffer33018&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)** que Alexandre Julliard, fundador de Wine ha realizado un discurso de apertura en el WineConf 2017, donde ha adelantado bastantes cosas importantes sobre el desarrollo del conocido wrapper.  En el se ha hablado del futuro más cercano del proyecto, así como los planes que tienen para el medio y largo plazo.


 


En cuanto a lo que incluirá la versión 3.0, que se espera para finales de este año, **se dará soporte a Direct3D 11**, así como a su flujo de comandos, el driver de Android, "tuberías" de modo mensaje y se subirá la versión del sistema a Windows 7. La hoja de ruta para las futuras versiones pasará por implementar los **contextos del núcleo OpenGL**, el empaquetado para Android, el congelamiento del código (code freeze) pronto; y **empezar con el soporte a Direct3D 12 y Vulkan**.  Se está trabajando también en dar **soporte al driver de Wayland**. Si quereis ver la charla podeis hacerlo en este video:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/AokFgDSLMWU" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Como veis el futuro de Wine? ¿Sois usuarios habituales? Contesta en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

