---
author: leillo1975
category: "Acci\xF3n"
date: 2017-06-02 13:36:02
excerpt: "<p>Los desarrolladores han iniciado una campa\xF1a en Kickstarter</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/937f929d420ab20a5c9d96a1ab9e2021.webp
joomla_id: 356
joomla_url: breaking-point-busca-financiacion-y-tendria-soporte-para-gnu-linux-steamos
layout: post
tags:
- supervivencia
- kickstarter
- mundo-abierto
- breaking-point
- arma-iii
- mod
title: "Breaking Point busca financiaci\xF3n y tendr\xEDa soporte para GNU-Linux/SteamOS"
---
Los desarrolladores han iniciado una campaña en Kickstarter

Según acabo de leer en [GamingOnLinux](https://www.gamingonlinux.com/articles/breaking-point-an-arma-3-mod-is-moving-to-unreal-engine-4-with-kickstarter-linux-vulkan-planned.9763), el conocido [mod "Breaking Point - The Zombie Infection"](http://www.thezombieinfection.com/) se independiza de Arma III y está buscando fondos mediante una campaña de [KickStarter](https://www.kickstarter.com/projects/alderongames/breaking-point/description), donde intentará alcanzar la cifra de 403.000 $ Australianos (unos 265.000€). Alderon Games, que así se llama la compañía Australiana, lleva recaudados unos 123.000 AU$, quedándole aun 19 dias por delante.


 


Se trata de un juego multiplayer online de mundo abierto (sandbox) y muerte permanente, donde primarán la supervivencia, el realismo y las tácticas militares. En el nos encontraremos en un mundo devastado por un virus decadas antes, y donde deberemos combatir tanto a infectados (zombies) como a otros clanes que luchan por controlar los limitados recursos que quedan. Empezaremos en este mundo completamente solos y sin ningún tipo de arma o recurso, por lo que rapidamente tendremos que buscar elementos en el juego que nos permitan sobrevivir y defendernos. Para ello, podremos ingresar en las diferentes facciones que pueblan el juego, pudiendo hacernos con los recursos de nuestros enemigos. Habrá eventos dinámicos aleatorios durante el juego que lo harán mucho más imprevisible. También se ha hecho especial énfasis en una balística realista, en la Inteligencia Artificial de los infectados; o en la variedad y calidad del entorno, que es cambiante. Podremos encontrar vehiculos con los que movernos por el mapa.


 


![breaking](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/BreakingPoint02.webp)


 


El juego estará desarrollado con Unreal Engine 4, ya que el estudio ha aclarado que no tienen acceso al código del motor de Arma II, y con el primero tienen total libertad de crear todo cuanto quieran y como quieran para llevar al juego en la dirección que ellos desean. Desde luego las imágenes que podemos ver en los videos de presentación muestran unos entornos espectaculares. Además los desarrolladores han aclarado que el juego contará con soporte para Mods.


 


Pero vamos a lo que más nos importa, y es que el juego como se aprecia en Kickstarter contará con una versión para nuestro sistema que necesitará de Ubuntu 16.04 64bits o similar, un procesador de cuatro núcleos a 2.5GHZ, 6GB de RAM y una gráfica Nvidia o AMD de serie media con soporte para OpenGL 4. El juego tendrá además la opción de ejecutarse con Vulkan. Podrá adquirirse en un futuro en Steam y los que contribuyan a la campaña de Kickstarter tendrán acceso a las versiones Beta del juego, así como contribuir con su feedback al desarrollo de este. El juego se espera para verano del año que viene, estando disponible para los usuarios de la Beta unos meses antes. Podeis encontrar más información en la [página web oficial](http://www.playbreakingpoint.com/).


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/uvnumlVg75Q" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


¿Que os parece este proyecto? ¿Creeis que conseguirán el objetivo de financiación? ¿Os interesaría colaborar con el? Dejanos tus respuestas o lo que quieras en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

