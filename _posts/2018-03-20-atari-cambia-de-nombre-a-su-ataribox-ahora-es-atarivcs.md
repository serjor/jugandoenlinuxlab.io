---
author: Pato
category: Hardware
date: 2018-03-20 18:22:26
excerpt: <p>Un usuario ha desvelado la posible lista de juegos</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b3404be4ce8e34f9427382fa15263dea.webp
joomla_id: 684
joomla_url: atari-cambia-de-nombre-a-su-ataribox-ahora-es-atarivcs
layout: post
tags:
- proximamente
- ataribox
- atari
- gdc
title: 'Atari cambia de nombre a su Ataribox: ahora es AtariVCS (ACTUALIZADO)'
---
Un usuario ha desvelado la posible lista de juegos

**ACTUALIZACIÓN 10-4-18**: [Yodefuensa](index.php/homepage/analisis/itemlist/user/210-yodefuensa), uno de los usuarios más activos de nuestra comunidad, nos acaba de pasar un enlace con "información" sobre parte de la **"posible" lista de juegos** que podría traer esta "consola". Como veis va todo muy entrecomillado porque la noticia, que viene de [Vandal](https://vandal.elespanol.com/noticia/1350706031/indican-cuales-podrian-ser-los-juegos-para-atari-vcs/), no confirma nada. Como suelo decir, esta información es cogida con pinzas y no hay nada oficial, pero un usuario de Twitter llamado [@pixelpar](https://twitter.com/pixelpar?ref_src=twsrc%5Etfw&ref_url=https%3A%2F%2Fvandal.elespanol.com%2Fnoticia%2F1350706031%2Findican-cuales-podrian-ser-los-juegos-para-atari-vcs%2F) ha dejado este tweet:



> 
> The following games have been rated for the Atari 2600 (it'll be Atari VCS).  
>   
> Expect a game reveal, release date, and preorder date very soon.  
>   
> • XEVIOUS  
> • STAMPEDE  
> • RIVER RAID  
> • PRESSURE COOKER  
> • KABOOM!  
> • PAC-MAN  
> • GALAXIAN  
> • H.E.R.O  
> • DIG DUG [pic.twitter.com/PhuBgxOCBx](https://t.co/PhuBgxOCBx)
> 
> 
> — Pixelpar (@pixelpar) [9 de abril de 2018](https://twitter.com/pixelpar/status/983294067965612034?ref_src=twsrc%5Etfw)



 La info parece bastante plausible, pero hay que tomar cautelas. 




---


**NOTICIA ORIGINAL**: Tras el [fallido intento de financiación](index.php/homepage/hardware/item/691-la-ataribox-estara-disponible-para-compra-anticipada-el-proximo-jueves-dia-14) en IndieGoGo del que ya hablamos en su momento, ahora Atari ha vuelto a dar señales de vida aprovechando su presencia en la Game Developers Conference (GDC). En concreto, nos llegaba la noticia vía Facebook de que ahora su Ataribox se llama AtariVCS:


<div class="resp-iframe"><iframe height="416" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FatariVCS%2Fposts%2F427883194316269%3A0&amp;width=500" style="border: none; overflow: hidden;" width="500"></iframe></div>


 Indagando un poco más, se trata de la misma Ataribox a la que han cambiado el nombre para deshacerse de cualquier referencia a "Xbox" y presentarla como un sistema de video-computadora para videojuegos y entretenimiento digital. La empresa afirma que la consola [será capaz de reproducir contenido](index.php/homepage/hardware/item/613-ataribox-podra-reproducir-contenido-de-servicios-de-streaming-como-hulu-hbo-go-o-netflix) de conocidas plataformas multimedia así como reproducir videojuegos tanto clásicos de la propia firma como actuales de otras plataformas.


#### Classic Controller y Modern Controller


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/Ataripad.webp)


Además de este anuncio, Atari ha presentado el que será su "gamepad" para los juegos más actuales, [aparte del que ya sabemos](index.php/homepage/hardware/item/675-atari-muestra-las-primeras-imagenes-del-joystick-de-la-ataribox) de corte más clásico. Se trata de un mando de inspiración en el de la consola XBox en forma de media luna y con la ya clásica distribución de pad analógico, cruceta y botonera XABY. Queda por ver si también contará con los típicos gatillos y botones frontales.


 Por otra parte, Atari parece estar involucrando a desarrolladores y buscando apoyos dentro de la industria en esta GDC de cara al lanzamiento de esta AtariVCS, y nos emplaza al mes de abril donde nos darán la fecha para poder hacer la reserva de la consola.


De las especificaciones finales poco se sabe aún, salvo que contará con Linux como sistema operativo y aunque anteriormente se habló de un procesador AMD ahora parece sonar con fuerza "un procesador basado en los de Intel", y el rango de precios que se rumorean para la consola está entre los 250 y 300 dólares.


Estaremos al tanto para ver si ofrecen nuevas informaciones de cara al lanzamiento oficial de las reservas.


¿Que te parecen las novedades de AtariVCS? ¿Te gustaría hacerte con una? ¿Piensas que ayudará a expandir el juego en los sistemas Linux?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).



Fuentes: [VentureBeat](https://venturebeat.com/2018/03/19/atari-unveils-atari-vcs-its-first-home-game-console-in-9-years/), [AtariVCS](http://ataribox.com/)

