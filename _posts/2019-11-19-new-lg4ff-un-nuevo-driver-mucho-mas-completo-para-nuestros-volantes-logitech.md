---
author: leillo1975
category: Software
date: 2019-11-19 19:23:23
excerpt: "<p>Su creador, @bernat_arlandis, acaba de actualizar el driver a la versi\xF3\
  n 0.2</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/g29.webp
joomla_id: 1131
joomla_url: new-lg4ff-un-nuevo-driver-mucho-mas-completo-para-nuestros-volantes-logitech
layout: post
tags:
- ets2
- ats
- logitech
- force-feedback
- ffb
- ffbtools
- new-lg4ff
title: "\u201Cnew-lg4ff\u201D, un nuevo driver mucho m\xE1s completo para nuestros\
  \ volantes Logitech (ACTUALIZADO)"
---
Su creador, @bernat_arlandis, acaba de actualizar el driver a la versión 0.2


**ACTUALIZACIÓN 25-12-19**: Como regalo extra de navidad, el desarrollador de este driver avanzado para volantes Logitech, [@bernat_arlandis](https://twitter.com/bernat_arlandis), acaba de lanzar una [nueva "release"](https://github.com/berarma/new-lg4ff/releases/tag/0.2) llamada oportunamente "**Merry Christmas**"con importantes novedades que resumidas son una mejor configurabilidad de la ganancia global y autocentrado, resolución (presumiblemente) de los problemas de saturación de la cola de comandos del dispositivo, aumento de la precisión gracias al uso del temporizador de alta resolución y el uso de latencias óptimas en cualquier hardware, La lista complenta sería la siguiente:


*- Entradas SYSFS para ganancia y autocentrado (lectura/escritura).*  
*- Ganancia global y por aplicación. Las aplicaciones no sobreescriben la ganancia global configurada..*  
*- Arreglado un error de sincronismo que podía aumentar la latencia del FFB.*  
*- Se ha anulado una cola de paquetes URB en el kernel que producía latencias y errores "output queue full".*  
*- Añadidos modos del temporizador que ajustan dinámicamente el periodo según las condiciones para minimizar las latencias.*  
*- Añadido temporizador de alta resolución para mejorar la precisión del FFB.*  
*- El periodo de actualización del FFB por defecto ha bajado de 4 a 2 ms.  
- La ganancia ahora también se aplica a los efectos condicionales. Solo se aplicaba a los que no eran condicionales.*


Como veis es bastante interesante para todos los usuarios de este driver que actualiceis lo antes posible. Si encontrais cualquier tipo de fallo, o veis que se pueda implementar algún tipo de mejora, no olvideis que podeis dejar un mensaje en las "[issues](https://github.com/berarma/new-lg4ff/issues)" de la [página del proyecto](https://github.com/berarma/new-lg4ff).




---


**NOTICIA ORIGINAL (19-11-19):**


Probablemente muchos de vosotros sabreis que es posible utilizar ciertos modelos volantes para jugar nuestros juegos de conducción favoritos en Linux, y entre ellos los que mejor soporte tienen actualmente están los de la conocida marca Logitech, que es la única que además de mapear ejes y botones también tiene soporte para Force Feedback.


Este soporte, que nos ha hecho disfrutar en gran medida de los juegos de coches, estaba incompleto y solo implementaba la fuerza constante, el efecto más común de Force Feedback, presente en la mayoría de los juegos. Esta situación provocaba que en algunos juegos no funcionase esta característica, especialmente en ETS2/ATS o muchos juegos con Wine/Proton.


Desde entonces varios desarrolladores habían intentado incluir más efectos en el driver, consiguiendo avances en este sentido, pero encontrando problemas que les hicieron parar el desarrollo. Debido a esto, un miembro de nuestra comunidad, **@Bernat** , que muchos conocereis por ser el creador de [Oversteer](https://github.com/berarma/oversteer), decidió retomar el trabajo ya hecho y darle su propio enfoque. Cansado de ver como el soporte de su volante no estaba a la par con el de Windows, decidió ponerse manos a la obra para intentar facilitar a los desarrolladores portar más juegos de conducción a Linux.


Previamente al desarrollo del driver, creo una utilidad llamada [FFBTools](https://github.com/berarma/ffbtools), que permitía recoger los logs del Force Feedback en los volantes Logitech, además de corregir algunos fallos presentes en algunos juegos con Wine/Proton, juegos tales como [Project Cars 2](https://github.com/ValveSoftware/Proton/issues/908#issuecomment-543995804) o [Dirt Rally 2](https://github.com/ValveSoftware/Proton/issues/2366#issuecomment-543999619). Con ella recogió información valiosa que le sirvió para entender mejor el funcionamiento del FFB en los juegos, para luego usarla en el nuevo driver.


Durante semanas de arduo trabajo, multitud de horas de programación y testeo, se ha llegado a una versión bastante completa y funcional del nuevo controlador, el cual, además de la fuerza constante, incluye lo siguiente:


*-Soporte para la mayoría de los efectos (excepto inercia) definidos en la [API FF de Linux](https://www.kernel.org/doc/html/latest/input/ff.html). (Constante, Periódica, Spring, Friction, Damper, Rumble...)*


*-Operaciones asíncronas con manejo de efectos en tiempo real.*


*-Limita la velocidad de las transferencias de datos al dispositivo con cierta latencia.*


*-Combina acelerador y embrague (pensado para simuladores de vuelo)*


El nuevo controlador ha sido testeado con multitud de videojuegos, consiguiendo que nativos como ETS2 o ATS, funcione el Force Feedback sin necesidad de ningún plugin; y que juegos que necesitan de Wine o Proton para ser ejecutados adquieran esta preciada característica con unos resultados semejantes a su soporte en Windows.


En JugandoEnLinux.com hemos estado ayudando en el proceso de testeo del driver en sus diferentes etapas y podemos constatar que los siguintes juegos tienen ahora soporte:


- Euro Truck Simulator 2 (nativo, sin plugin)


- American Truck Sumulator (nativo, sin plugin)


- RACE 07 y todas sus expansiones (Wine/Proton)


- Automobilista (Wine/Proton)


- rFactor 1 (Wine/Proton)


- rFactor 2 (Wine/Proton)


- KartKraft (Wine/Proton)


El driver **solo ha podido ser testeado en el Logitech G29**, pero está diseñado para que funcione también en los volantes de la marca con soporte en GNU/Linux, por lo que @Bernat, **su autor os agradecería que lo probaseis y le dejaseis feedback en los “[issues](https://github.com/berarma/new-lg4ff/issues)”.**


La instalación requiere compilar el código, pero el proceso es bastante sencillo y teneis información de como hacerlo en la [página del proyecto](https://github.com/berarma/new-lg4ff). En un futuro se intentará incluir este código en el kernel al igual que el controlador actual o “empaquetarlo” para un uso más sencillo. ¡Ahora solo os queda enchufar vuestro volante y disfrutar como enanos de vuestros juegos de coches favoritos!


¿Sois usuarios de volantes Logitech? ¿Qué os parece este nuevo trabajo de @Bernat? Cuéntanoslo en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

