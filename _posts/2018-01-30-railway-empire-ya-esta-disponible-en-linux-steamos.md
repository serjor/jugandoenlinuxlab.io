---
author: Pato
category: Estrategia
date: 2018-01-30 18:37:10
excerpt: "<p>Convi\xE9rtete en un magnate del transporte del ferrocarril</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/346cfc4cda7a853e9a165b7df6f7b442.webp
joomla_id: 627
joomla_url: railway-empire-ya-esta-disponible-en-linux-steamos
layout: post
tags:
- estrategia
- simulacion
- gestion
title: "'Railway Empire' ya est\xE1 disponible en Linux/SteamOS"
---
Conviértete en un magnate del transporte del ferrocarril

Hace un tiempo ya [anunciamos](index.php/homepage/generos/estrategia/item/680-railway-empire-partira-de-la-estacion-linux-steamos-el-proximo-26-de-enero) que Railway Empire' llegaría a Linux/&SteamOS de la mano de Kalypso Media. ¡Pues ya lo tenemos aquí!


*En **Railway Empire** crearás una compleja y **extensa red de ferrocarril**, comprarás más de **40 trenes diferentes**, modelados con extraordinario detalle, y adquirirás o **construirás estaciones de ferrocarril, edificios de mantenimiento, fábricas y atracciones turísticas** para mantener tu red de transporte por delante de la competencia. Si quieres que el servicio de trenes sea eficaz, también deberás contratar y supervisar a trabajadores, así como desarrollar más de **300 tecnologías**, como mejorar los mecanismos o los propios trenes, crear infraestructuras o servicios avanzados en el lugar de trabajo, etc., todo ello mientras progresas a través de cinco eras de innovaciones tecnológicas.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Df3IcT5kKVc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Al parecer la versión Linux tuvo algunos problemas de lanzamiento que ya han podido solventar tal y como puede leerse en la [última actualización](http://steamcommunity.com/games/503940/announcements/detail/1651001473442850370).


En cuanto a los requisitos, son:


**Mínimo:**  

+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** Ubuntu 16.04.3 LTS + SteamOS (última versión)
+ **Procesador:** Intel Core i5 750 @ 2.6 GHz o AMD Phenom II X4 @ 3.2 GHz
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** nVidia GeForce GTX460 o AMD Radeon HD5870 (2048MB VRAM con Shader Model 5.0)
+ **Almacenamiento:** 7 GB de espacio disponible
+ **Tarjeta de sonido:** Compatible con los últimos drivers


**Recomendado:**  

+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** Ubuntu 16.04.3 LTS + SteamOS (última versión)
+ **Procesador:** Intel Core i5 2400s @ 2.5 GHz o AMD FX 4100 @ 3.6
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** nVidia GeForce GTX 680 o AMD Radeon HD7970 o superior (2048MB VRAM o mas, con Shader Model 5.0)
+ **Almacenamiento:** 7 GB de espacio disponible
+ **Tarjeta de sonido:** Compatible con los últimos drivers


Si te gusta la gestión y la construcción de tu imperio de Ferrocarriles, tienes 'Raiway Empire' en su página de Steam completamente en español:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/503940/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>
