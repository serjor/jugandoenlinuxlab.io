---
author: Pato
category: Estrategia
date: 2020-03-07 00:03:57
excerpt: "<p>@feralgames nos anuncia que YA lo tenemos disponible</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/TotalWarThreeKingdoms/TotalWarTKAWorldBetrayed.webp
joomla_id: 1185
joomla_url: total-war-three-kingdoms-a-world-betrayed-llegara-a-linux-poco-despues-de-su-lanzamiento-actualizado
layout: post
tags:
- feral-interactive
- estrategia
- total-war
title: "Total War: THREE KINGDOMS - A World Betrayed llegar\xE1 a Linux poco despu\xE9\
  s de su lanzamiento (ACTUALIZADO)"
---
@feralgames nos anuncia que YA lo tenemos disponible


Tenemos noticias sobre **Total War: THREE KINGDOMS - A World Betrayed**, y es que Feral nos anuncia que ya lo tenemos disponible para nuestro sistema favorito:



> 
> Total War: THREE KINGDOMS – A World Betrayed Chapter Pack is out now for macOS and Linux.  
>   
> Buy from –  
>   
> Feral Store: <https://t.co/HYS51SuFE8>   
> Steam: <https://t.co/7gfDVkBLRd>   
>   
> The free-LC warlord White Tiger Yan is also available via Steam: <https://t.co/08cmuFptG6> [pic.twitter.com/gtMI9jTjE3](https://t.co/gtMI9jTjE3)
> 
> 
> — Feral Interactive (@feralgames) [April 9, 2020](https://twitter.com/feralgames/status/1248267556567351299?ref_src=twsrc%5Etfw)


  





Como podéis ver, también tenemos disponible el *free-LC warlord White Tiger Yan.*Podemos encontrar A World Betrayed en la [tienda online de Feral Interactive](https://store.feralinteractive.com/es/mac-linux-games/threekingdomstw/) (preferible para que ellos se lleven las mayores ganancias) o [en Steam](https://store.steampowered.com/app/1209110/Total_War_THREE_KINGDOMS__A_World_Betrayed/), al igual que [Warlord White Tiger Yan](https://store.steampowered.com/app/1244640/Total_War_THREE_KINGDOMS__White_Tiger_Yan/).




---


Vamos con un atraso anuncio que hace unos días se nos pasó totalmente. Y es que [Feral Interactive](https://www.feralinteractive.com/es/news/?platform=linux) nos anunció que nos traerá la nueva expansión **Total War: THREE KINGDOMS - A World Betrayed** poco después de su lanzamiento en Windows el cual está previsto para el próximo 19 de Marzo:



> 
> Total War: THREE KINGDOMS – A World Betrayed pledges itself to macOS and Linux shortly after Windows. ? <https://t.co/k3aqRaQAh6>
> 
> 
> — Feral Interactive (@feralgames) [March 5, 2020](https://twitter.com/feralgames/status/1235582264051937280?ref_src=twsrc%5Etfw)


  





En esta expansión, Creative Assembly y Sega nos traerán:


* Dos nuevas facciones dirigidas por dos de los personajes más populares, Lü Bu y Sun Ce
* 13 facciones jugables
* Nuevas unidades únicas para el campo de batalla
* Nuevas mecánicas de facción
* Nuevos eventos y misiones de historia
* Nuevos personajes legendarios
* Nuevos eventos de historia
* Una nueva fecha de inicio: 194 d. C.
* Mecánicas únicas de facción
* Eventos de historia únicos
* nuevos héroes que regresan:
* + Cao Cao
	+ Liu Bei
	+ Yuan Shao
	+ Yuan Shu
	+ Liu Biao
	+ Zhang Yan
	+ Zheng Jiang
	+ Gongsun Zan
	+ Kong Rong
	+ Ma Teng


Si quieres saber más o estás interesado en la reserva, puedes visitar su [página web de Steam](https://store.steampowered.com/app/1209110/Total_War_THREE_KINGDOMS__A_World_Betrayed/) donde **lo puedes reservar con un 10% de descuento** o esperar al lanzamiento de la web del juego dentro de la **tienda de Feral** y así apoyarlos a ellos directamente. Tu decides.


 

