---
author: Pato
category: Noticia
date: 2022-02-25 17:40:06
excerpt: "<p>La compa\xF1\xEDa&nbsp;<span class=\"account-inline txt-ellipsis\" style=\"\
  caret-color: auto;\"><span class=\"username txt-mute\">@THQNordic&nbsp;</span></span><span\
  \ style=\"caret-color: auto;\">nos muestra los juegos que pueden ejecutarse en la\
  \ m\xE1quina de Valve</span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/THQnordic.webp
joomla_id: 1438
joomla_url: thq-nordic-da-la-bienvenida-a-la-steam-deck-con-un-video-promocional
layout: post
tags:
- steam
- thq-nordic
- steam-deck
title: "THQ Nordic da la bienvenida a la Steam Deck con un v\xEDdeo promocional"
---
La compañía @THQNordic nos muestra los juegos que pueden ejecutarse en la máquina de Valve


Hoy es el día de Steam Deck, y se nota. Nada menos que THQ Nordic se ha descolgado con un vídeo en el que dan la bienvenida a Steam Deck y muestran los juegos que se pueden ejecutar en la máquina de Valve. Entre ellos, los títulos verificados:


* Darksiders II Deathinitive Edition: <https://thqn.net/ds2-steam>
* Darksiders Genesis: <https://thqn.net/dsg-steam>
* Desperados III: <https://thqn.net/d3-steam>
* This Is the Police 2: <https://thqn.net/titp2-steam>


Además de un buen número de títulos jugables. Aquí tenéis el vídeo:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/1DFenHOrqbo" title="YouTube video player" width="720"></iframe></div>


En otro orden de cosas, queremos recordarte que tienes una cita con nosotros esta noche a las 22:30 GMT+1 en nuestro podcast de Jugando en Linux donde hablaremos de videojuegos, noticias de videojuegos, novedades de videojuegos... 

Si, tambien de Steam Deck! ;-P

Os esperamos aquí!: [Twitch](https://www.twitch.tv/jugandoenlinux)

