---
author: Pato
category: Plataformas
date: 2017-03-16 18:24:08
excerpt: <p>El juego ofrece retos tanto para un jugador como para multijugador local</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/83a9554230ed6e6cccfe522a2a4bd53e.webp
joomla_id: 251
joomla_url: moribund-un-juego-de-accion-y-plataformas-en-mundo-apocaliptico-ya-disponible-en-linux-steamos
layout: post
tags:
- accion
- indie
- plataformas
- steam
title: "'Moribund' un juego de acci\xF3n y plataformas en mundo apocal\xEDptico ya\
  \ disponible en Linux/SteamOS"
---
El juego ofrece retos tanto para un jugador como para multijugador local

Gracias a [www.linuxgameconsortium.com](https://linuxgameconsortium.com/linux-gaming-news/moribund-post-apocalyptic-action-platformer-launchers-steam-49953/) nos enteramos de la llegada hoy mismo de este 'Moribund', un juego de acción y plataformas bastante llamativo con la premisa de los juegos "fáciles de jugar, dificiles de dominar" y donde se puede jugar solo o en compañía. Hasta 4 jugadores en multijugador local podrán dedicarse a superar los distintos niveles y retos que este juego propone.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/wWS-OvOntPo" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 En 'Moribund' tendrás 8 personajes para elegir con caractarísticas y armamento própio. Cada arma del juego tiene dos modos de disparo. El primer modo inmoviliza a tu enemigo y mientras está paralizado puedes cargar tu segundo modo, que tendrás que disparar sobre tu paralizado oponente para destruirlo. Si a esto le añadimos un ritmo endiablado, amplificadores y trampas la diversión está servida.


Si te va el ir solo, tienes 80 niveles, cada uno con su entorno y mecánicas propias. Si tienes compañía podreis disfrutar con dos modos de juego multijugador, todos contra todos y combate por equipos para una diversión de lo más salvaje, con desmembramientos y muertes de lo mas hilarante.


Si te gusta este 'Moribund', lo tienes disponible en su página de Steam, eso sí, no está traducido al español. Pero si no es problema para ti, lo tienes con un 15% de descuento por su lanzamiento:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/500790/" width="646"></iframe></div>


 

