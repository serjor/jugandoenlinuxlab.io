---
author: Serjor
category: Estrategia
date: 2018-11-12 18:21:54
excerpt: <p>@feralgames acaba de publicar la fecha de salida</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0ca34323b100a0e16065d211a598d1fe.webp
joomla_id: 899
joomla_url: feral-interactive-anuncia-los-requisitos-para-el-proximo-total-war-warhammer-ii
layout: post
tags:
- feral-interactive
- feral
- warhammer
- total-war
- requisitos
title: "Feral Interactive anuncia los requisitos para el pr\xF3ximo Total War: WARHAMMER\
  \ II (ACTUALIZADO)"
---
@feralgames acaba de publicar la fecha de salida

**ACTUALIZACIÓN 16-11-18**: No tendremos que esperar mucho para disfrutar de este expléndido juego pues en unos pocos días lo tendremos disponible en nuestras linuxeras máquinas. Concretamente podremos comprarlo y jugarlo el próximo **martes 20 de Noviembre**. Aquí os dejamos el tweet de Feral que lo anuncia:  
  




> 
> Total War: WARHAMMER II explodes onto macOS and Linux on November 20th.  
>   
> Next week, defend or destroy the New World in the sequel to Creative Assembly's award-winning WARHAMMER.  
>   
> Storm the Feral Store now to secure your pre-order: <https://t.co/9dvPYvtERx> [pic.twitter.com/Nb5vxsxYVM](https://t.co/Nb5vxsxYVM)
> 
> 
> — Feral Interactive (@feralgames) [November 16, 2018](https://twitter.com/feralgames/status/1063466630691340294?ref_src=twsrc%5Etfw)







Por supuesto, en cuanto esté disponible os lo anunciaremos en nuestra web. Permaneced atentos.....




---


**NOTICIA ORIGINAL:** Gracias a [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Total-War-Warhammer-2-Linux-Req) nos enteramos de que Feral Interactive [ha anunciado](http://www.feralinteractive.com/en/news/919/) los requisitos para ejecutar el port de Total War: WARHAMMER II, tanto en GNU/Linux como en macOS.


En el sistema bueno los requisitos mínimos son los siguientes:


* Procesador 3.4 GHz Intel Core i3-4130
* 6GB RAM
* Nvidia GTX 680 de 2GB o AMD R9 285 (GCN tercera generación) de 2GB


Cómo requisitos recomendados:


* 3.4 GHz Intel Core i7-4770
* 8GB RAM
* Nvidia GTX 970 o AMD RX 480 de 4GB en ambos casos


La distribución que soportan de manera oficial es Ubuntu 18.04 64-bit, y en el apartado de drivers para NVIDIA recomiendan los drivers 396.54 o superiores, y para AMD la versión 18.1.5 de MESA o superior.


Os dejamos con su trailer para ir calentando motores antes de su salida. Si queréis más información sobre el juego, podéis visitar su [miniweb](http://www.feralinteractive.com/es/games/warhammer2tw/).


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/WeDQQBwxRTU" width="560"></iframe></div>


Y tú, ¿ya tienes ganas de que salga este Total War? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

