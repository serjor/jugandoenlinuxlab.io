---
author: leillo1975
category: "Simulaci\xF3n"
date: 2019-11-07 18:00:15
excerpt: "<p>@SCSsoftware expande de nuevo su mapa de USA. #LinuxGaming</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a111f23c16ea3f29e72a482eced3b5eb.webp
joomla_id: 1129
joomla_url: lanzada-la-dlc-utah-para-american-truck-simulator
layout: post
tags:
- dlc
- american-truck-simulator
- scs-software
- utah
title: "Lanzada la DLC \u201CUTAH\u201D para American Truck Simulator."
---
@SCSsoftware expande de nuevo su mapa de USA. #LinuxGaming


Como es habitual, la empresa Checa SCS Software, [ha cumplido una vez más su palabra](index.php/homepage/generos/simulacion/14-simulacion/1195-utah-sera-la-nueva-expansion-de-american-truck-simulator) y como es habitual nos trae esta **nueva expansión de mapa** para añadir un estado más del enorme territorio que son los EEUU. El anuncio lo recibíamos una vez más [a través de su blog](https://blog.scssoft.com/2019/11/utah-released.html) y su cuenta de twitter:



> 
> Utah has arrived for American Truck Simulator! ?   
>   
> You can now deliver to this beautiful state for yourself ?   
>   
> Are you ready to drive? Check out Utah on steam here ? <https://t.co/zCeIUcn9m6> [pic.twitter.com/pInumou3LS](https://t.co/pInumou3LS)
> 
> 
> — SCS Software (@SCSsoftware) [November 7, 2019](https://twitter.com/SCSsoftware/status/1192506799515148289?ref_src=twsrc%5Etfw)


  





 Para preparar este lanzamiento han estado trabajando recientemente en la [actualización 1.36](https://blog.scssoft.com/2019/11/american-truck-simulator-update-136.html), que tiene importantisimas mejoras, como viene siendo habitual, entre las que destaca la **capacidad del juego de ver nuestros camiones, traileres y conductores en el mapa**; mejoras en la señalización en el juego base y expansiones, algunas **ciudades mejoradas**, limites de velocidad actualizados, nuevos remolques, retoques en el Anti-aliasing...


![Utah Road map big](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Utah/Utah_Road_map_big.webp)


En esta ocasión han vuelto a **territorios más áridos** como los que vimos en las primeras expansiones del juego (Nevada, Arizona y [Nuevo México](index.php/homepage/analisis/20-analisis/656-analisis-american-truck-simulator-dlc-new-mexico)), dejando de lado los verdes bosques de las dos últimas, [Oregon](index.php/homepage/analisis/20-analisis/998-analisis-american-truck-simulator-dlc-oregon) y [Washington](index.php/homepage/analisis/20-analisis/1196-analisis-american-truck-simulator-washington-dlc). Pero eso no quiere decir que nos den más de lo mismo, pues **encontraremos montones de novedades en “Utah”** entre las que destacan las siguientes:


*-3.500 millas de red de carreteras*


*-10 ciudades principales (Salt Lake City, St. George, Moab...)*


*-Nuevas canteras y minas, incluida la mayor excavación a cielo abierto Mina de cobre Kennecott*


*-Expansión de la industria petrolera (yacimientos mineros, sitios de almacenamiento de petróleo)*


*-Mejora de la cadena de producción agrícola (tiendas rurales, fábricas de piensos)*


*-Lugares famosos: Great Salt Lake, Monument Valley, Virgin River Canyon*


*-Más de 260 monumentos naturales y artificiales reconocibles*


*-12 conocidas paradas de camiones*


*-Nuevo y mejorado proceso de creación del paisaje*


*-Logros de Utah para desbloquear*


Pues terminada la espera, ahora solo tenemos que agarrarnos al volante de nuestro camión y recorrer las carreteras de este estado en nuestro sistema favorito. Os dejamos con el video del lanzamiento:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/zYPfiT4-vj4" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Podeis comprar American Truck Simulator: Utah DLC en [Steam](https://store.steampowered.com/app/1104880/American_Truck_Simulator__Utah/) por un precio que no está nada mal, **11.99€**.También nos gustaría emplazaros a que esteis atentos a nuestras redes sociales ([Twitter](https://twitter.com/JugandoenLinux) y [Mastodon](https://mastodon.social/@jugandoenlinux)) pues como viene siendo habitual realizaremos una serie de Streams especiales a través de nuestros canales de [Youtube](https://www.youtube.com/c/jugandoenlinuxcom) y [Twitch](https://www.twitch.tv/jugandoenlinux) para que nos acompañeis mientras recorremos las carreteras de esta DLC. También podreis disponer de un completo análisis unas semanas más tarde aquí mismo, en JugandoEnLinux.com. Nos gustaría agradecer a SCS Software su colaboración facilitándonos una copia de "Utah" para poder crear estos contenidos.


¿Qué opinión te merece este nuevo territorio? ¿Te han gustado las expansiones anteriores? Cuéntanoslo en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

