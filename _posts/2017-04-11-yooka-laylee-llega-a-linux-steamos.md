---
author: Pato
category: Plataformas
date: 2017-04-11 18:14:31
excerpt: "<p>El juego de Playtonic se ha hecho esperar, pero ya lo tenemos aqu\xED\
  \ para disfrutarlo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/73c564de315ae81db9aaa50a11f02581.webp
joomla_id: 285
joomla_url: yooka-laylee-llega-a-linux-steamos
layout: post
tags:
- indie
- plataformas
- aventura
- 3d
title: "'Yooka Laylee' llega a Linux/SteamOS hoy, d\xEDa de salida"
---
El juego de Playtonic se ha hecho esperar, pero ya lo tenemos aquí para disfrutarlo

¡Por fin! tras una larga espera desde que Playtonic lanzase su exitoso Kickstarter hace ya más de un año, ya tenemos con nosotros 'Yooka Laylee' [[web oficial](http://www.playtonicgames.com/games/yooka-laylee/)]. ¿Qué podemos esperar de 'Yooka Laylee'?


Pues un juego de plataformas y aventuras en 3D con la esencia de Rare con objetivos que cumplir, misiones que realizar y unos cuantos niveles para disfrutar saltando, nadando, volando...


Jugaremos con una pareja de animales; un lagarto y una murciélago (Yooka y Laylee) combinando las habilidades de ambos para ir superando los obstáculos y los niveles. ¡Incluso se puede jugar en cooperativo con un amigo llevando cada uno a un personaje!


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/oqmF8IgxtJ0" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


**Sinopsis de Steam:**



> 
>  
> 
> 
> ¡Yooka-Laylee es un nuevo juego de plataformas de mundo abierto de los veteranos del género Playtonic!
> 
> 
> Explora mundos vastos y preciosos, conoce (y vence) a un reparto inolvidable de personajes y acumula un montón de brillantes coleccionables mientras la pareja de Yooka (el verde) y Laylee (la murciélago ocurrente de nariz grande) se embarcan en una aventura épica para acabar con el malvado empresario Capital B y su retorcidos planes para absorber todos los libros del mundo... ¡Y convertirlos en beneficio puro!
> 
> 
> Con su arsenal de movimientos especiales, nuestros héroes se enfrentarán a una gran variedad de puzles y plataformas en su búsqueda de Pagies, la recompensa dorada que sirve para desbloquear y expandir nuevos mundos alucinantes, todos ellos repletos de personajes rocambolescos, jefes descomunales, desafíos de minas, juegos arcade, preguntas, juegos multijugador... ¡y mucho más!
> 
> 
> 


 **Características principales:**



> 
>  PRESENTAMOS A NUESTRA NUEVA PAREJA DE COLEGAS: Yooka y Laylee disponen de un arsenal de habilidades impresionante diseñado para divertirse en plataformas y desbloqueadas con libertad gracias al vendedor serpentil Trowzer.
> 
> 
> FORJA TU PROPIO CAMINO: Compra y desbloquea movimientos con libertad, elige cuáles de tus mundos favoritos expandir con zonas de juego todavía más grandes y complejas, y equipa tónicos modificadores de juego para personalizar tu estilo de juego.
> 
> 
> UN JUEGO DE COLECCIONABLES PARA UNA ÉPOCA MODERNA: ¡Busca un conjunto de coleccionables brillantes con progresión de jugabilidad como característica central (y otros materiales más valiosos), como la Energía mariposa, de doble acción, o los engañosos Ghost Writers!
> 
> 
> UN REPARTO DE ESPANTO: Conoce (o derrota) un reparto enorme de personajes memorables que cobran vida gracias al equipo de arte y audio que crearon juegos de plataformas legendarios, y que están destinados a perdurar en aventuras futuras como parte del universo Playtonic. Puedes esperarte toda clase de gruñidos, chirridos y graznidos.
> 
> 
> UNA BANDA SONORA DE ENSUEÑO: ¡David Wise (Donkey Kong Country) y Grant Kirkhope (Banjo-Kazooie) se unen para crear una melodía maestra! En serio, límpiate los oídos... que les espera una buena sorpresa.
> 
> 
> JUEGA ACOMPAÑADO: ¡Crea tu propia pareja de colegas y juega a Yooka-Laylee en modo cooperativo! Un segundo jugador puede tomar el control de un personaje cooperativo único y ayudar a Yooka y Laylee aturdiendo enemigos y ayudándoles con los desafíos más complicados. No hace falta ir en la espalda del otro.
> 
> 
> Y MUCHO MÁS: ¡Descubre batallas únicas contra jefes, desafíos de carritos mineros, juegos arcade retrotásticos, juegos de preguntas y más de 8 juegos multijugador únicos!
> 
> 
> 


 Con estas premisas 'Yooka Laylee' viene a cubrir un hueco que en Linux hasta ahora no teníamos cubierto: el de plataformas 3D, y además con un juego al parecer de calidad. Y digo al parecer por que dispongo de una clave y ¡próximamente le daré un repaso para contaros qué tal es! ¡Estar atentos a la web o nuestras redes sociales!


En cuanto a los requisitos para mover este título en Linux, son estos:


SO: Ubuntu 14.04 o superior, o SteamOS  
Procesador: Intel i5-2500 3.3GHz  
Memoria: 8 GB de RAM  
Gráficos: NVidia GTS450 / AMD Radeon 6850HD  
Almacenamiento: 9 GB de espacio disponible  
Notas adicionales: Un gamepad es ALTAMENTE RECOMENDADO para jugar este juego.


Si ya estáis impacientes por echarle el guante a 'Yooka Laylee', ya podéis comprarlo traducido al español en su web de Steam en su versión normal:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/360830/64503/" width="646"></iframe></div>


 


O en su versión digital deluxe con su banda sonora y libro de arte:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/360830/141967/" width="646"></iframe></div>


¿Qué te parece 'Yooka Laylee'? ¿Piensas jugarlo? ¿Te gustan los títulos de Rare?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD). Ah! y no olvides que tenemos una review del juego pendiente!

