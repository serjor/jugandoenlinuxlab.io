---
author: leillo1975
category: Hardware
date: 2017-03-29 07:54:04
excerpt: "<p>Ubuntu da soporte de forma oficial a lo \xFAltimo en controladores libres</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/bebada99aaa9847746eea59472544575.webp
joomla_id: 267
joomla_url: tu-grafica-amd-o-intel-bien-actualizada-oficialmente-en-ubuntu
layout: post
tags:
- amd
- intel
- mesa
- ppa
- xswat
- oficial
title: "Tu gr\xE1fica AMD o Intel bien actualizada oficialmente en Ubuntu"
---
Ubuntu da soporte de forma oficial a lo último en controladores libres

Hace un mes y medio [dábamos cuenta en esta web](index.php/homepage/hardware/item/340-lanzado-oficialmente-mesa-17-0-0) del lanzamiento definitivo de la última versión de Mesa, lo que venía a traer muy importantes mejoras a todos los usuarios de drivers libres, por ende a los de AMD e Intel, principalmente. Bien es sabido que el Stack gráfico de Ubuntu normalmente se actualiza entre versiones, por lo que habría que esperar a una versión más avanzada del sistema  para poder disfrutarlo, concretamente la 17.04, que saldrá en unas semanas. Esto puede representar un problema para los usuarios de la versión LTS, que son muchos. Hasta ahora, para instalar la última Mesa en las versiones actuales de Ubuntu era necesario usar PPA's no oficiales como los que comentábamos en la [sección de tutoriales del foro](index.php/foro/tutoriales/13-instalar-ultima-version-de-mesa-y-de-drivers-libres-amd-intel-nouveu-padoka).


 


Desde hace algún tiempo, **se venía solicitando a Ubuntu que crease un repositorio oficlal** para poder actualizar estos drivers, como en su día hicieron con los [propietarios de Nvidia](https://launchpad.net/~graphics-drivers/+archive/ubuntu/ppa). A esta demanda se sumaba la gente de [Feral Interactive](http://www.feralinteractive.com/es/), que obviamente tiene un interés muy grande en incorporar siempre las últimas mejoras para un mejor rendimiento de sus juegos. Finalmente, parece que esta presión ha tenido recompensa y como leía recientemente en [MuyLinux](http://www.muylinux.com/2017/03/28/ubuntu-ppa-mesa), Ubuntu ha publicado un repositorio para dar soporte actualizado a Mesa. La ventaja de este con respecto a otros no oficiales es que está más controlado y es más estable, por lo que representa un menor riesgo para los usuarios. Aún así es importante destacar que, aunque más seguro que los anteriores, el uso de este repositorio va a cuenta y riesgo de sus usuarios.


 


Si quereis instalarlo, en primer lugar os recomiendo desinstalar el anterior ppa, **si es que instalasteis alguno con este fin antes**. Como ejemplo os dejo como desinstalar él que antes comentaba del foro (padoka PPA):


 



> 
> sudo ppa-purge ppa:paulo-miguel-dias/mesa
> 
> 
> 


Ahora instalaremos el PPA oficial con los siguientes comandos:


 



> 
> sudo add-apt-repository ppa:ubuntu-x-swat/updates  
> sudo apt update  
> sudo apt dist-upgrade
> 
> 
> 


Como veis, los usuarios de gráficas AMD e Intel cada vez tienen mejor soporte para su Hardware, algo que es muy de agradecer a los desarrolladores. También, gracias al desarrollo de Mesa y este tipo de iniciativas, los que ahora tenemos un hardware distinto (nvidia) también podemos plantearnos con más seriedad esta alternativa, abriendo el abanico de posibilidades a la hora de escoger tarjeta gráfica, ya que como sabeis, hasta hace poco tener AMD para jugar era una quimera.


 


Podeis comentar cuales son vuestras impresiones, opiniones sobre la inclusión de este nuevo PPA. No olvideis que también disponemos de nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD) donde os estamos esperando.


 


FUENTES: [MuyLinux](http://www.muylinux.com/2017/03/28/ubuntu-ppa-mesa) ,  [OMGUbuntu](http://www.omgubuntu.co.uk/2017/03/easy-way-install-mesa-17-0-2-ubuntu-16-04-lts)


 

