---
author: leillo1975
category: Carreras
date: 2017-01-12 19:16:29
excerpt: <p>Si no tienes este estupendo juego, esta es la oportunidad de conseguirlo
  gratis</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0d422469a7bfe49699e19d8d898530d7.webp
joomla_id: 175
joomla_url: humble-bundle-esta-regalando-dirt-showdown-corred-insensatos
layout: post
tags:
- humble-bundle
- gratis
- dirt-showdown
- virtual-progamming
- codemasters
title: "Humble Bundle est\xE1 regalando Dirt Showdown, \xA1Corred insensatos!"
---
Si no tienes este estupendo juego, esta es la oportunidad de conseguirlo gratis

Lo que os dije, ¡Corred insensatos!. Tanto si lo teneis como si no (es un buen regalo), podeis haceros con este juego completamente gratis. Para ello solo teneis que tener una cuenta en Humble Bundle (que por cierto está en pleno [Winter Sale](https://www.humblebundle.com/store) con grandes descuentos en juegos) y completar el proceso de "compra", y automáticamente os será entregada una clave de Steam completamente gratuita. Para ello simplemente pinchad **[este enlace](https://www.humblebundle.com/store/dirt-showdown-free-game)**


 


 ![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DirtShowFreeHumble2.webp)


 


Como sabeis, desde hace ya algún tiempo este juego de [Codemasters](http://www.codemasters.com/) está disponible en para GNU/Linux gracias al trabajo de [Virtual Programming](https://www.vpltd.com/) y su wrapper "eON". Decir que el juego funciona realmente bien, con una buena tasa de frames, y lo que es mejor, es tremendamente divertido, incluso en modo multijugador. Se trata de un titulo de conducción totalmente arcade donde tendremos diversos modos de juego, entre los que podremos encontrar pruebas tipo jimkana (o como se escriba), carreras, destrucción, etc... con coches tanto de calle como de lo más variopinto.


Por cierto, ¿que tal unas carreritas?

