---
author: Pato
category: Apt-Get Update
date: 2017-03-24 20:47:04
excerpt: <p>Otra semana se nos escapa. y volvemos al repaso habitual</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/65672688a4f4c8a57ea38e20056bdce1.webp
joomla_id: 264
joomla_url: apt-get-update-eitr-jack-orlando-kova-overload-aurion
layout: post
title: Apt-Get Update EITR & Jack Orlando & Kova & Overload & Aurion...
---
Otra semana se nos escapa. y volvemos al repaso habitual

Otra semana se nos va, esta vez sin mucho contenido, pero es que la cosa ha estado bastante tranquila. De todos modos, siempre hay cosas que repasar. Comenzamos


Desde [www.gamingonlinux.com](http://www.gamingonlinux.com):


- EITR es un juego de Rol y acción con un apartado de combate notable. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/eitr-an-action-rpg-built-around-exceptional-combat-with-fantastic-visuals.9350).


- Desde Valve siguen dándole a la tecla y han parcheado 'DOTA 2' para optimizarlo en los procesadores Rizen. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/dota-2-patched-for-amd-ryzen.9353).


- 'Jack Orlando: Director's Cut' es otro juego de Topware Interactive que recibe port a través de Wine para Linux. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/jack-orlando-directors-cut-now-has-a-wine-port-on.9355).


- Otro juego que nos llega es 'The Wispered World Special Edition'. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/the-whispered-world-special-edition-is-now-available-for-linux.9364).


Desde [www.linuxgameconsortium.com](http://www.linuxgameconsortium.com):


- 'Kova' un interesante juego mezcla de RPG y metroidvania está en Greenlight y próximamente en crowdfunding con versión para Linux. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/kova-metroidvania-style-rpg-now-greenlight-50318/).


- 'Overload' es un juego de disparos en 3D y 6 ejes ya disponible en linux y para gafas 3D. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/overload-much-available-steam-linux-50346/).


- 'Aurion' es un RPG de acción en 2D que está oficialmente confirmado que llegará muy pronto a Linux. Puedes verlo en este enlace.


 


Vamos con los vídeos de la semana:


EITR


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/z4EWdcoSpAo" width="640"></iframe></div>


 


The whispered World


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/vYoMYBdOkwA" width="640"></iframe></div>


 


Kova


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/EYaVfvq2y7Y" width="640"></iframe></div>


 


Overload


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/l6rhz_I8KWE" width="640"></iframe></div>


 


Aurion


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/TeL-sCQSn_0" width="640"></iframe></div>


 


Y hasta aquí este Apt-Get de esta semana. ¿Qué te ha parecido?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


La semana que viene mucho más.

