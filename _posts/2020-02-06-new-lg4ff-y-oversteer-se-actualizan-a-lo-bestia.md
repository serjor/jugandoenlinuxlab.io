---
author: leillo1975
category: Software
date: 2020-02-06 20:53:50
excerpt: "<p><span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\"\
  >@bernat_arlandis</span> a\xF1ade novedades a su driver y aplicaci\xF3n</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Oversteer/new-lg4ff-Oversteer.webp
joomla_id: 1164
joomla_url: new-lg4ff-y-oversteer-se-actualizan-a-lo-bestia
layout: post
tags:
- drivers
- logitech
- volantes
- oversteer
- new-lg4ff
- bernat
title: new-lg4ff y Oversteer se actualizan de nuevo
---
@bernat_arlandis añade novedades a su driver y aplicación


**ACTUALIZADO 17-3-20**: Bernat ([@berarma](https://github.com/berarma)) nos acaba de avisar de que ha hecho unos pequeños ajustes y correcciones tanto en su driver como en su aplicación. En **new-lg4ff** alcanzamos la [versión 0.3.1](https://github.com/berarma/new-lg4ff/releases/tag/0.3.1) y podemos encontrar los siguientes cambios:


*-Se corrigieron problemas de compilación para algunas configuraciones.  
-Se corrigieron algunas rarezas en los efectos al descargar efectos condicionales al dispositivo solo cuando se usaban.  
-Para todos los efectos al descargar el módulo.*


Por otro lado **Oversteer** llega a la [versión 0.4.1](https://github.com/berarma/oversteer/releases/tag/0.4.1) con las siguientes correcciones:


*-Se corrigieron los perfiles de carga de CLI.*  
 *-Correcciones a los archivos de integración de escritorio.*  
 *-README revisado.*


En esta ocasión no encontramos novedades, pero sí cambios en el código que nos permitirán disfrutar de nuestros volantes con menos errores.


 




---


**NOTICIA ORIGINAL**: En los últimos meses  el soporte de volantes Logitech en nuestro sistema ha crecido exponencialmente, y en gran parte se lo debemos a un miembro de nuestra comunidad, **Bernat** ([@berarma](https://github.com/berarma)), desarrollador de la aplicación [Oversteer](index.php/component/search/?searchword=oversteer&searchphrase=all&Itemid=828), que nos permite **configurar este tipo de dispositivos**, y más cerca en el tiempo, de "[new-lg4ff]({{ "/posts/new-lg4ff-un-nuevo-driver-mucho-mas-completo-para-nuestros-volantes-logitech" | absolute_url }})", una nueva versión **mucho más avanzada del driver** que podemos encontrar por defecto en el kernel. Gracias al trabajo de este usuario podemos disfrutar de un soporte ya muy cercano al que tendríamos en Windows, y con estas nuevas versiones, en algunos aspectos incluso superior.


Y es que driver y aplicación vienen en esta ocasión de la mano, pues **uno complementa al otro**, ya que para exprimir al máximo las nuevas características del nuevo driver, es necesario una aplicación de configuración que esté a la altura y que permita tocar todo aquello que podamos modificar. Por lo tanto, en primer lugar vamos a hablar del driver, que es el que provoca la mayor parte de los cambios en la aplicación. **New-lg4ff** alcanza la [versión 0.3](https://github.com/berarma/new-lg4ff/releases/tag/0.3) , apodada "**Bye bad clipping**" y trae como novedades:


- **Balance de efectos condicionales configurable**. Se pueden ajustar los niveles de efectos de muelle, amortiguamiento y fricción para obtener un FF más equilibrado.  
- Los **niveles predeterminados para estos efectos**, que antes estaban al máximo, **se han reducido para que sean similares a los que usa el driver de Windows** y así tener de salida unos resultados similares en los juegos.  
- En los **volantes que tienen leds**, estos se pueden usar para **monitorizar la fuerza de los efectos aplicados** y detectar la saturación (clipping) de los efectos.  
- La **monitorización de la fuerza de los efectos aplicados** se puede realizar desde las aplicaciones usando una entrada **SYSFS**.


A la par **Oversteer** llega a la [versión 0.4](https://github.com/berarma/oversteer/releases/tag/0.4) ("**Catching up"**), y muestra un avance brutal en características, permitiendo cosas que incluso no podemos realizar en Windows. El detalle de estas novedades podeis verlo a continuación:


- **Los elementos de la interfaz de usuario han sido reorganizados** para más claridad y un estilo más moderno.  
- **Los controles de la interfaz que no están disponibles quedan desactivados.**  
- Añadido el **icono de la aplicación**.  
- **Pruebas del volante integradas en la aplicación**. Ya no depende de aplicaciones externas.  
- **El rango de giro se puede modificar usando los botones del volante**. Estos botones **se pueden configurar**.  
- Función de **ventana superpuesta para mostrar el rango** de giro seleccionado.  
- **Cambios de modo de emulación más rápidos**.  
Además, **si usamos el módulo [new-lg4ff](https://github.com/berarma/new-lg4ff)** tendremos las siguientes funciones:  
- **Combinar acelerador y embrague en un mismo eje**, útil para simuladores de vuelo.  
- **Ajustar nivel de efectos condicionales por separado**: muelle, amortiguamiento y fricción.  
- **Monitorización de la fuerza de effectos aplicada usando los leds y/o en la ventana superpuesta**.  
- **Control de la ganancia total de los efectos en combinación con la ganancia establecida por las aplicaciones**.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Oversteer/Oversteer04.webp)


Como veis, Bernat no ha estado perdiendo el tiempo, y gracias a él tenemos un soporte superior en la mayoría de los volantes de esta marca. Este aspecto, como veis ya está cubierto, **ahora solo falta que los desarrolladores del kernel se tomen esto en serio e incluyan este driver que ahora es un módulo, dentro del núcleo**, sustituyendo al actual; y por supuesto que los desarrolladores y porters de juegos tomen nota de estos avances a la hora de traernos sus juegos de conducción a nuestro sistema. ¿A qué esperan?


Podeis descargar [new-lg4ff](https://github.com/berarma/new-lg4ff) y [Oversteer](https://github.com/berarma/oversteer) de sus respectivas **páginas de proyecto**, donde encontrareis las instrucciones para su instalación y funcionamiento. Y vosotros, ¿habeis probado ya este driver y aplicación? ¿Qué os parece el trabajo de Bernat? ¿Teneis un volante Logitech? Puedes contarnos lo que quieras sobre esta noticia en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

