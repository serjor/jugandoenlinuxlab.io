---
author: Pato
category: "Simulaci\xF3n"
date: 2019-01-17 18:08:41
excerpt: <p>Construye tu propia base espacial y gestionala en clave indie en el juego
  de @VoxGamesAU</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f66da0157e88ccf3deb2fbdbfc220fe0.webp
joomla_id: 952
joomla_url: meeple-station-ya-esta-disponible-en-linux-steamos-en-acceso-temprano
layout: post
tags:
- indie
- espacio
- acceso-anticipado
- simulador
- estrategia
title: "Meeple Station ya est\xE1 disponible en Linux/SteamOS en acceso temprano"
---
Construye tu propia base espacial y gestionala en clave indie en el juego de @VoxGamesAU

Meeple Station es un juego de construcción de bases espaciales bastante curioso donde podemos crear y gestionar nuestra base espacial y donde podremos manejar diversos roles. Hace tan solo unas horas nos han llegado [noticias](https://steamcommunity.com/games/900010/announcements/detail/1706202908638604031) de que el juego ha sido lanzado en acceso temprano y tiene versión para Linux/SteamOS.


*"¿Alguna vez has querido construir tu propia estación espacial? ¿Intercambiar bienes y cuidar de los inhabitantes? ¡Explora galaxias distantes y toma misiones audaces mientras tratas de mantener a tus meeple con vida! Meeple Station es un simulador de estación espacial con final abierto en las venas de Rimworld que tiene éxito haciendo justo eso. Vive una aventura sin fin mientras tratas de hacer lo mejor de tu destartalada estación."*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/Nqq3bgKLQsw" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Dentro del juego podremos dedicarnos al comercio, la minería o la investigación entre otras cosas para crear la estación espacial de nuestros sueños, mantener vivos a nuestros "meeples" y visitar territorios inexplorados de la galaxia. Si estás interesado en la propuesta, puedes conseguir 'Meeple Station' en acceso temprano con un descuento del 15% por el lanzamiento.


Tienes toda la información en su [página web oficial](https://meeplestation.com/), en [Steam](https://store.steampowered.com/app/900010/Meeple_Station/) o en [itch.io](https://voxgames.itch.io/meeple-station).


¿Que te parece Meeple Station? ¿Haces una estación espacial?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [discord](https://discord.gg/ftcmBjD).

