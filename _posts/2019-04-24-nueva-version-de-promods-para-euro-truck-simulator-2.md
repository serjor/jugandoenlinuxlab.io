---
author: leillo1975
category: "Simulaci\xF3n"
date: 2019-04-24 07:15:38
excerpt: "<p class=\"ProfileHeaderCard-screenname u-inlineBlock u-dir\" dir=\"ltr\"\
  ><span class=\"username u-dir\" dir=\"ltr\">@ProModsETS2 tambi\xE9n anuncia expansiones\
  \ para American Truck Simulator</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/dcca011eac737955750c5f2f4e56b627.webp
joomla_id: 1028
joomla_url: nueva-version-de-promods-para-euro-truck-simulator-2
layout: post
tags:
- ets2
- american-truck-simulator
- ats
- euro-truck-simulator-2
- mod
- promods
- canada
title: "Nueva versi\xF3n de ProMods para Euro Truck Simulator 2"
---
@ProModsETS2 también anuncia expansiones para American Truck Simulator

No ha pasado ni un año desde la última actualización de ProMods, [la 2.30](index.php/homepage/generos/simulacion/14-simulacion/966-el-fantastico-promods-de-euro-truck-simulator-2-alcanza-la-version-2-30), para que los fans de sin duda el mejor mod de los muchos que tiene **Euro Truck Simulator 2** vuelva a la carga. Como suele ser habitual la lista de novedades es muy jugosa y todos los camioneros virtuales ya están deseando incarle el diente para poder recorrer los Kilómetros y Kilómetros de nuevas carreteras. En esta ocasión recibiamos la noticia a través de email esta misma noche sin noticia de ello en las redes sociales.


En primer lugar debeis saber que **necesitais la versión 1.34** de ETS2 (la última) y además de **todas las expansiones de mapa anteriores**, también la DLC "[Beyond The Baltic Sea](https://www.humblebundle.com/store/euro-truck-simulator-2-beyond-the-baltic-sea?partner=jugandoenlinux)" que recientemente [analizamos en nuestra web](index.php/homepage/analisis/20-analisis/1067-analisis-euro-truck-simulator-2-beyond-the-baltic-sea-dlc). Las novedades que vais a encontrar son las siguientes:


**Croacia**: Ploče  
**Chequia**: Olomouc y la red de autopistas  
**Finlandia** Kittilä  
**Alemania**: Reconstruido: Flensburg, Burg auf Fehmarn  
**Grecia**: Αλεξανδρούπολη (Alexandroupoli), Καβάλα (Kavala)  
**Hungría**: Vásárosnamény. Reconstruido: Debrecen  
**Letonia**: Mejorada: Rēzekne  
**Macedonia**: Прилеп (Prilep)  
**Polonia**: Reconstruido: Cieszyn, Grudziądz. Mejorada: Augustów, A1 (Gorzyce), A4, A8, S8, DK8, (Wrocław), DK16, DK61, DK74 (Zamość), S8 (Ostrów Maz. - Białystok), S52 y DK1 (Bielsko-Biała)  
**Rusia**: Гусев (Gusev)  
**Serbia**: Лесковац (Leskovac), Врање (Vranje). Mejorado: Београд (Beograd)  
**Eslovaquia**: Trnava  
**Suecia**: Ljugarn, Visby  
**Reino Unido** (Inglaterra): Hawes. Reconstruido: Chelmsford, Felixstowe, M6 y A1(M)  
**Reino Unido** (Escocia): Stornoway, Tarbert  
**Reino Unido** (Gales): Mejorada: Aberystwyth  
**General**: Muchas correcciones de errores y ajustes frente a la versión 2.33


Como veis el trabajo ha sido arduo desde la última expansión. Os dejamos con un video de Squirrel probándola hace unos días:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/hYP0YX-8Xxg" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 En otro orden de cosas y no menos importante, la gente de [MandelSoft](https://www.youtube.com/user/MandelSoft) ha anunciado en su [blog de ProMods](https://blog.promods.net/2019/04/promods-ats-a-new-world-is-born/) que **están trabajando en expandir American Truck Simulator hacia el Norte** para poder incluir **Canada** en lo que será su nuevo proyecto. No hay fechas de lanzamiento ni nada por el estilo, pero aprovechando que SCS Soft se ha puesto manos a la obra con el estado de [Washington](index.php/homepage/generos/simulacion/14-simulacion/1081-washington-sera-la-proxima-expansion-de-american-truck-simulator), han comenzado a diseñar **Vancouver** y el territorio que se encuentra al noreste de esta ciudad para recrear el estado de la [Columbia Británica](https://es.wikipedia.org/wiki/Columbia_Brit%C3%A1nica). Por ahora poco más podemos decir, pero os dejamos con un video publicado hace unas semanas donde lo dejaban caer:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/075YzZa1nIQ" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 ![ProMods ATS 1.0](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/Promods/ProMods-ATS-1.0.webp)


Seguiremos muy de cerca este nuevo proyecto y os iremos informando según vayamos teniendo novedades. Por ahora si quereis podeis ir haciéndoos Promods 2.40 desde su página de [descargas](https://promods.net/setup.php). Nosotros os ofreceremos muy pronto un Stream probándo esta nueva versión, por lo que permaneced atentos a nuestras cuentas de [Twitter](https://twitter.com/JugandoenLinux) y [Mastodon](https://mastodon.social/@jugandoenlinux).

