---
author: Pato
category: Noticia
date: 2019-07-31 07:57:04
excerpt: "<p>El proyecto patrocinado por Valve mejorar\xE1 el soporte de los escritorios\
  \ Linux en las gafas de realidad virtual</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e4710ea0caa54c5d561a4702a971c28e.webp
joomla_id: 1084
joomla_url: collabora-anuncia-xrdesktop-un-proyecto-open-source-para-renderizar-el-escritorio-en-la-realidad-virtual
layout: post
title: Collabora anuncia xrdesktop, un proyecto open-source para renderizar el escritorio
  en la realidad virtual
---
El proyecto patrocinado por Valve mejorará el soporte de los escritorios Linux en las gafas de realidad virtual

Vía [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Xrdesktop-Released) sabemos que de nuevo **Valve** trae una novedad para el escritorio Linux. Esta vez patrocinando y colaborando con la compañía **Collabora** que ha desarrollado el proyecto open-source **xrdesktop** para poder interactuar con los escritorios en Linux, como por ejemplo Gnome o KDE en los visores de realidad virtual mediante una implementación sobre el stack grágico X11.


Así lo explican en el [anuncio desde Collabora](https://www.collabora.com/news-and-blog/news-and-events/moving-the-linux-desktop-to-another-reality.html):


"En los primeros días de la realidad virtual en Linux, cuando se conectaba un HMD a un sistema este no sabía lo que era,  la pantalla se inicializaba como una pantalla de escritorio genérica y el administrador de ventanas le extendía el escritorio. Obviamente, esto no era deseable, pero aún podías ver ventanas de escritorio recortadas y perspectivas incorrectas en tu HMD. Solo los más valientes de nosotros estábamos lo suficientemente interesados ​​como para encontrar el cursor en la pantalla y mover las ventanas fuera del camino para usarlo para la representación en modo extendido. Si bien la situación estaba lejos de ser perfecta, el objetivo era claro: la manipulación de la ventana del escritorio en un entorno 3D renderizado con precisión, controlado con controladores VR.


El primer paso en esta dirección fue hacer que el administrador de ventanas dejara de extender el escritorio. Con su trabajo en la inclusión de drm, Keith Packard introdujo una propiedad que no es de escritorio para pantallas X11. Se especificó una extensión Vulkan VK_EXT_acquire_xlib_display, destinada a ser utilizada por los compositores de realidad virtual para permitir la representación en HMD directamente. Actualmente se está introduciendo una extensión equivalente para Wayland y se implementa en la pila de gráficos. [...]  



Hoy, nos emociona anunciar un nuevo proyecto de código abierto que permite la interacción con entornos de escritorio tradicionales, como GNOME y KDE, en realidad virtual. Patrocinado por Valve, xrdesktop hace que los administradores de ventanas conozcan la realidad virtual y puedan usar tiempos de ejecución de realidad virtual para representar ventanas de escritorio en el espacio 3D, con la capacidad de manipularlas con controladores de realidad virtual y generar entradas de mouse y teclado desde la realidad virtual."


Como podéis ver, se trata de un avance hacia una mejor implementación de los escritorios en la realidad virtual, esperemos que con un objetivo concreto en el futuro. Con Valve detrás, desde luego algo parece estar en camino. Toda la información del proyecto se puede encontrar en su [página de desarrollo en Gitlab](https://gitlab.freedesktop.org/xrdesktop/xrdesktop).


¿Qué piensas de este proyecto? ¿Crees que tendrá aplicaciones prácticas para los videojuegos en Linux?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

