---
author: Pato
category: Terror
date: 2020-09-24 21:01:42
excerpt: "<p>Ahora los dos juegos que les dieron la fama son Open Source</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Amnesia/amnesiapig.webp
joomla_id: 1259
joomla_url: frictional-games-publica-el-codigo-de-amnesia-the-dark-descent-y-a-machine-for-pigs-bajo-licencia-gplv3
layout: post
tags:
- terror
- amnesia
title: "Frictional Games publica el c\xF3digo de Amnesia:The Dark Descent y A Machine\
  \ for Pigs bajo licencia GPLv3"
---
Ahora los dos juegos que les dieron la fama son Open Source


Ayer vimos uno de esos movimientos raros de ver en la industria del videojuego, y más en un juego comercial publicado por una gran compañía como es Frictional Games.


Hablamos del anuncio que hizo el [estudio en su blog](https://frictionalgames.com/2020-09-amnesia-is-now-open-source/), donde explican que dado el éxito obtenido, y que **Anmesia: The Dark Descent** ya ha cumplido 10 años, el estudio siente que debe "devolver" algo a la comunidad, y lo hacen publicando todo el código de los dos títulos mencionados, a saber:


**Amnesia: The Dark Descent**:


<https://github.com/FrictionalGames/AmnesiaTheDarkDescent>


**Amnesia: A Machine for Pigs**:


<https://github.com/FrictionalGames/AmnesiaAMachineForPigs>


Ambos juegos están publicados bajo licencia [**GPLv3**](https://www.gnu.org/licenses/gpl-3.0.en.html), eso sí como ellos mismos explican, *esto no significa que el juego sea gratis de repente. Simplemente significa que las personas son libres de usar la fuente como quieran siempre que se adhieran a la licencia GPL3. El juego y todo su contenido sigue siendo propiedad de Frictional Games. Justo como antes.*


*Piensa en el lanzamiento como "libertad de expresión" (free speech), no como "cerveza gratis"(free beer).*


A partir de ahora todo el código está disponible para todo el que quiera estudiarlo y modearlo


Tienes toda la información en el post del blog de Frictional Games [en este enlace](https://frictionalgames.com/2020-09-amnesia-is-now-open-source/). A continuación, los vídeos promocionales de ambos juegos:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/u1nY_5-UrY4" width="560"></iframe></div>


 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/4CagMNLxa9M" width="560"></iframe></div>


Qué te parece el anuncio de Frictional Games? ¿echarás un ojo al código?


Cuéntamelo en los comentarios o en los canales de jugandoenlinux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

