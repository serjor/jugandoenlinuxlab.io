---
author: Pato
category: Hardware
date: 2021-07-21 20:55:53
excerpt: "<p>Se desvelan nuevas caracter\xEDsticas que ajustan la m\xE1quina a mejor</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/image_2021-07-16_17-48-51.webp
joomla_id: 1330
joomla_url: el-hardware-de-steam-deck-es-mejor-de-lo-que-se-pensaba
layout: post
tags:
- steamos
- hardware
- steam-deck
title: El hardware de Steam Deck es mejor de lo que se pensaba
---
Se desvelan nuevas características que ajustan la máquina a mejor


Parece que lo nuevo de Valve sigue mejorando con los días. Según hemos conocido, el PC... ¿Portatil? de Valve (aún estamos debatiendo si llamar consola a esto) tiene algunas características distintas a las que creíamos conocer. Hace un par de días supimos a través de un [hilo de reddit](https://www.reddit.com/r/Steam/comments/olp163/was_wondering_if_the_steam_deck_will_have_a/) en el que aparecía una respuesta de Gabe Newell a un usuario, que **todos los modelos de Steam Deck incluirán un puerto NVMe para discos duros**, **incluida la versión de 64Gb**, por lo que las posibilidades de ampliación de dicho modelo aumentan exponencialmente, aunque desde Valve reconocen que desaconsejan el instalar unidades de disco duro por parte de los usuarios, al ser un aparato muy complejo y fácil de dañar si no se sabe lo que se hace. Tampoco se sabe si Valve capará de alguna forma la posibilidad de ampliar el aparato mediante dicho puerto, aunque pensamos que siendo básicamente el mismo hardware en todos es dudoso que hagan algo así. Veremos cuanto tarda la comunidad en abrir un aparato y hacer un tutorial de ampliación. ¿Alguien se atreve?


Por otra parte, todos sabemos que **la Steam Deck se puede ampliar mediante tarjetas microSD**, pero lo que no sabíamos es que **cuando los chicos de IGN estuvieron probándola estaban ejecutando los juegos desde una tarjeta microSD**. A preguntas de un usuario a Lawrence Yang, uno de los diseñadores de la Steam Deck dejó bien claro que los juegos se ejecutarían con más fluidez desde un disco duro interno, pero que las pruebas de IGN fueron desde la tarjeta externa. Así pues parece que las tarjetas microSD también son capaces de defenderse con soltura.



> 
> Yep, games will load faster off internal storage, but games still play great off an SD card. When IGN came by, all the games they tried (and shot footage of) were played off a microSD card.
> 
> 
> — Lawrence Yang (@lawrenceyang) [July 17, 2021](https://twitter.com/lawrenceyang/status/1416485869091913728?ref_src=twsrc%5Etfw)


  





Lo que ahora **nos gustaría saber es de qué clase era la microSD que emplearon**, aunque no es difícil de imaginar que para lograr esa fluidez emplearon la tarjeta con mayor transferencia disponible en el mercado. Aquí tenéis el vídeo de IGN probando la máquina:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/oLtiRGTZvGM" title="YouTube video player" width="560"></iframe></div>


Por último, también hemos sabido gracias a un r[eporte de un usuario en Twitter](https://twitter.com/Locuza_/status/1417205868924375041?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1417205868924375041%7Ctwgr%5E%7Ctwcon%5Es1_c10&ref_url=https%3A%2F%2Fwww.3djuegos.com%2Fnoticias-ver%2F215525%2Fresulta-que-steam-deck-tiene-mejores-prestaciones-de-las%2F), que Valve ha hecho una corrección en las características de la Steam Deck que la configuración de memoria RAM que va a emplear la máquina **son 16GB de memoria LPDDR5, pero no en dual channel** como se publicitó en un primer momento, **si no en una configuración 32 bit Cuad Channel para una transferencia de 5500MT/s**. Es decir, todos sabemos que los procesadores de AMD son muy susceptibles a las velocidades de memoria RAM, y con esto lo que nos están asegurando es que no tendremos cuellos de botella debido a la velocidad de transferencia de esta.


Puedes ver las especificaciones (ya cambiadas) [en la web de Steam Deck](https://www.steamdeck.com/en/tech) (versión inglesa, en la versión española aún no lo han cambiado).


¿que te parecen las novedades en las características de la Steam Deck?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://twitter.com/JugandoenLinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

