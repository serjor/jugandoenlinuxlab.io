---
author: leillo1975
category: "Acci\xF3n"
date: 2017-04-03 07:48:14
excerpt: "<p>El conocido shooter Open Source llega a su versi\xF3n 0.8.2 con multitud\
  \ de novedades</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/bab2d5d59ac444db8043a4f3e32c9f0e.webp
joomla_id: 276
joomla_url: nueva-version-de-xonotic-con-importantes-mejoras
layout: post
tags:
- xonotic
- open-source
- multiplataforma
title: "Nueva versi\xF3n de Xonotic con importantes mejoras"
---
El conocido shooter Open Source llega a su versión 0.8.2 con multitud de novedades

Pegando mi vuelta diaria por mis webs de referencia acabo de leer en [La Mirada del Replicante](http://lamiradadelreplicante.com/2017/04/02/a-tiros-con-xonotic-0-8-2/) (excelente blog, por cierto) que ha sido liberada una nueva versión de Xonotic el pasado día **1 de abril** (y no es broma...):


 



> 
> (NOT an April Fool)  
>  Xonotic 0.8.2 has been released!  
>  Read more here:  
>  <https://t.co/UAcmiuzcsG>
> 
> 
> — Team Xonotic (@Xonotic) [1 de abril de 2017](https://twitter.com/Xonotic/status/848238414617079808)



 


Como muchos sabreis, [Xonotic](http://www.xonotic.org/) es un conocido Shooter de licencia GPL2 basado en el código de Quake 3, y que a su vez es un fork del vilipendiado Nexuiz. Xonotic es multiplataforma y está disponible en GNU-Linux, Windows y Mac, y cuenta con una buena base de jugadores, lo que lo hace un juego tremendamente atractivo. Se trata de un título muy rápido, totalmente arcade, y con ambiente futurista donde encontraremos múltiples y variados modos de juego donde poder "matarnos" a gusto.


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Xonotic.webp)


 


En esta actualización, en la que parece que no han perdido el tiempo, [hay muchisimas novedades](http://xonotic.org/posts/2017/xonotic-0-8-2-release/) y muy reseñables, entre las que podemos destacar nuevos y mejorados mapas, músicas, accesorios para el combate y los interesantes **mutadores**, que nos permiten introducir nuevas normas en los combates para añadir, por ejemplo, nuevos poderes, gravedad modificable, regeneración, etc. Se han mejorado los vehiculos, añadido un **menú rápido** pulsando F8, multitud de mejoras en la representación de la información en pantalla (HUD), montones de nuevos modelos de armas, o **minijuegos clásicos** para pasar el rato mientras esperamos a que el servidor nos de paso al juego. También es muy importante, aunque no tan visible para el jugador, el esfuerzo que se ha hecho en mejorar el diseño de su código para hacerlo más amigable y escalable para futuras versiones.


 


Podeis descargar Xonotic 0.8.2 de forma completamente gratuita desde su página web: **<http://www.xonotic.org/download/>**. Para jugarlo simplemente debereis descomprimirlo y ejecutar **"xonotic-linux-glx.sh"**


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/_thowri7mmE" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Hacen unos frags? Dinos lo que quieras sobre este juego en los comentarios, o participa en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 


**FUENTES:** [La mirada del Replicante](http://lamiradadelreplicante.com/2017/04/02/a-tiros-con-xonotic-0-8-2/), [Xonotic](http://lamiradadelreplicante.com/2017/04/02/a-tiros-con-xonotic-0-8-2/)

