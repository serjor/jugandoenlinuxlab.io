---
author: Pato
category: "Acci\xF3n"
date: 2017-01-23 17:42:59
excerpt: "<p>El estudio espa\xF1ol 'The Game Kitchen' est\xE1 desarrollando un nuevo\
  \ juego con identidad propia. A\xFAn est\xE1 en desarrollo temprano</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/fff4548682445ceca36c12620f894d26.webp
joomla_id: 186
joomla_url: blasphemous-el-nuevo-juego-de-los-creadores-de-the-last-door-casi-seguro-llegara-a-linux
layout: post
tags:
- accion
- indie
- proximamente
- exploracion
title: "'Blasphemous' el nuevo juego de los creadores de 'The Last Door' \"casi seguro\"\
  \ llegar\xE1 a Linux"
---
El estudio español 'The Game Kitchen' está desarrollando un nuevo juego con identidad propia. Aún está en desarrollo temprano

 La escena "indie" española sigue en plena efervescencia con desarrollos interesantes que llaman la atención. La semana pasada por casualidad dimos con un desarrollo que enseguida llamó mi atención por lo llamativo de su estética. 


'Blasphemous' [[web oficial](http://blasphemousgame.com/)] está siendo desarrollado por "The Game Kitchen" [[web oficial](http://thegamekitchen.com/)], estudio español responsable de la saga de juegos de aventuras "The Last Door" y parece ser un juego de acción y plataformas al mas puro estilo tradicional. Según sus propias palabras se trata de un juego de "acción y plataformas con combate intenso, mucha exploración y blasfemias!". El trasfondo que se le adivina de mazmorras, monstruos y cuidada estética es suficiente para que mi obsesión por los juegos de tipo "metroidvania" haga que salte con la pregunta: ¿y este juego lo podré jugar en Linux?


Manos a la obra, preguntamos al estudio y nos contestó que "casi seguro" llegará a Linux:



> 
> [@JugandoenLinux](https://twitter.com/JugandoenLinux) Game will (almost assuredly) be available for Linux.
> 
> 
> — Blasphemous (@BlasphemousGame) [19 de enero de 2017](https://twitter.com/BlasphemousGame/status/822068099688071168)



 El juego aún está en fase de desarrollo temprano y en la web oficial no hay aún mucha información. Tampoco tenemos un vídeo que echarnos a la boca, pero si algunos gifs animados de lo más sugerentes:


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Externo/Blasphemousaccion.webp) 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Externo/Blasphemousexplora.webp)


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Externo/Blasphemousblasphem.webp)


Desde luego el arte conceptual y la temática no me negaréis que no es sugerente. El estudio tiene previsto acudir a Kickstarter en busca de financiación durante el próximo mes de mayo y esperan poder lanzarlo al mercado a finales de 2018. Seguiremos este 'Blasphemous' tanto en su desarrollo como en su campaña de Kickstarter para ver qué nos deparan los desarrolladores y poder comentar con mas detalle qué nos ofrecerá este juego.


¿Te gustaría que este 'Blasphemous' llegase a Linux? 


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


Por cierto "the Game Kitchen"... ¿Para cuando la saga 'The Last Door' en Linux?

