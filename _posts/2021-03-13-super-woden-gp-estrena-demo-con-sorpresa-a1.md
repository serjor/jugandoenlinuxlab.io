---
author: leillo1975
category: Carreras
date: 2021-03-13 20:03:38
excerpt: "<p>Su creador , @ViJuDaGD , lo ha confirmado hace un rato.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperWodenGP/SWGP.webp
joomla_id: 1287
joomla_url: super-woden-gp-estrena-demo-con-sorpresa-a1
layout: post
tags:
- demo
- vijuda
- super-woden-gp
- rally
title: Super Woden GP ya tiene fecha de lanzamiento.
---
Su creador , @ViJuDaGD , lo ha confirmado hace un rato.


**ACTUALIZACIÓN 27-7-21:**


Hace unos meses, tal y como podeis ver en la noticia original (abajo), os comentabamos que el juego presumiblemente saldría este verano. Pues cumpliendo con lo prometido, su creador, Victor Justo ([ViJuDa](https://twitter.com/ViJuDaGD)), acaba de anunciar la fecha definitiva, **el 1 de Septiembre**, tal y como podeis verlo en este tweet:



> 
> [#SuperWodenGP](https://twitter.com/hashtag/SuperWodenGP?src=hash&ref_src=twsrc%5Etfw), les espera el 1 de Septiembre en Steam! ⬇️Trailer aquí⬇️  
> Un RT ayuda muchísimo! Gracias!❤️<https://t.co/dqru8kqGCn>[#gamedev](https://twitter.com/hashtag/gamedev?src=hash&ref_src=twsrc%5Etfw) [#indiegame](https://twitter.com/hashtag/indiegame?src=hash&ref_src=twsrc%5Etfw) [#isometricracer](https://twitter.com/hashtag/isometricracer?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/tjiCqki8wf](https://t.co/tjiCqki8wf)
> 
> 
> — ViJuDa (@ViJuDaGD) [July 27, 2021](https://twitter.com/ViJuDaGD/status/1419974579825647621?ref_src=twsrc%5Etfw)



 Lo dicho, en poco más de un mes podremos disfrutar de **este pedazo de juego, hecho por y para amantes de los coches clásicos y los juegos de antaño**. Nosotros, como sabeis seguimos colaborando en el proceso de testeo de la versión de Linux y os podemos asegurar que **el juego va a colmar con creces las espectativas** de muchos de los que os gusten los juegos de este tipo, tanto por el montón de contenido que tiene como por la calidad y cariño que se está poniendo en su desarrollo. Podeis ver algunas de sus características más reseñables en el **recién estrenado trailer de lanzamiento**:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/FRs33x1DQC0" style="display: block; margin-left: auto; margin-right: auto;" title="YouTube video player" width="780"></iframe></div>


Si estais interesados en este juego podeis demostrar vuestro interés en la versión nativa para GNU/Linux añadiendolo a vuestra lista de desados:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1534180/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 




---


**NOTICIA ORIGINAL 13-3-21:**


 A principios de año recibíamos con júbilo [la noticia de que este juego tendría finalmente versión nativa]({{ "/posts/woden-gp-un-arcade-de-carreras-retro-tendra-version-linux-a1" | absolute_url }}) para nuestro sistema, y hace unas semanas os mostrábamos una versión de desarrollo en [uno de los videos de nuestro canal.](https://youtu.be/2-oKZGyt5B4) Hasta ahora **teníamos la suerte de poder ayudar en el desarrollo** pudiendo probar compilaciones previas del juego, pero desde hoy mismo, ese privilegio, al menos en parte será para todo el que quiera probar esta "**Time Attack Demo**".


Además, si demuestras tu valía a los mandos del vehículo, y en esta **etapa de Rally** consigues el **mejor tiempo** (o el segundo si no superas el tiempo del desarrollador), **ganarás una copia del juego** cuando este finalmente sea lanzado. También hay que destacar que quien le gane a Victor (Vijuda) recibirá una sorpresa. Tendreis de plazo durante esta semana **hasta el próximo Sabado 20 de Marzo a las 21:30**.


![SWGPDemo](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperWodenGP/SWGPDemo.webp)


 En principio **el juego se espera para el verano si todo va sobre ruedas**, pero esta previsión puede cambiar en función de si existiese la posibilidad de lanzarse también para consolas. Sea como fuere, ya sabeis que toda la información sobre su lanzamiento final, así como los pormenores de su desarrollo los encontrareis aquí, en JugandoEnLinux.com. Teneis la demo de Super Woden GP en Steam, donde también podreis **agregar el juego a vuestra Lista de Deseados**:  
  
<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1534180/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


Ahora solo os queda pelear por el primer puesto y demostrar de que pasta estais hechos. También podeis dejarnos vuestras preguntas o impresiones en los comentarios en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

