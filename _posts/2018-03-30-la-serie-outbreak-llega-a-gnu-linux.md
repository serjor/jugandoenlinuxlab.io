---
author: Serjor
category: Terror
date: 2018-03-30 18:42:23
excerpt: "<p>Acci\xF3n y terror para compartir</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/884bd078062ee6bd157b294c752d7970.webp
joomla_id: 698
joomla_url: la-serie-outbreak-llega-a-gnu-linux
layout: post
tags:
- survival-horror
- outbreak
title: La serie Outbreak llega a GNU/Linux
---
Acción y terror para compartir

Nos enteramos gracias a [SteamKiwi](https://www.steamkiwi.com/article=207598/outbreak-the-new-nightmare-now-available-on-linux-and-os-x-w-cross-platform-multiplayer) de que Outbreak y Outbreak: The New Nightmare llegan a GNU/Linux y Mac, con soporte para juego cruzado entre las tres plataformas.


Además nos anuncian también que su próximo título, Outbreak: The Nightmare Chronicles también llegará a las tres plataformas.


Grandísimas noticias, ya que no tenemos la suerte de jugar a muchos survivals horrors cooperativos en nuestra plataforma preferida.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/HUvuq9jSEEg" width="560"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/644480/" style="border: 0px;" width="646"></iframe></div>


Y tú, ¿qué piensas de este anuncio? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

