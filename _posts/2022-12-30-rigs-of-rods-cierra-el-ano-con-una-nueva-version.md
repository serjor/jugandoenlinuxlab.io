---
author: leillo1975
category: "Simulaci\xF3n"
date: 2022-12-30 13:20:45
excerpt: "<p>El equipo de desarrollo de <span class=\"css-901oao css-16my406 r-poiln3\
  \ r-bcqeeo r-qvutc0\">@rigsofrods</span> nos trae novedades m\xE1s que notables.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/RigsOfRods/RigsOfRods02.webp
joomla_id: 1520
joomla_url: rigs-of-rods-cierra-el-ano-con-una-nueva-version
layout: post
tags:
- simulador
- open-source
- rigs-of-rods
- ogre
title: "Rigs of Rods cierra el a\xF1o con una nueva versi\xF3n"
---
El equipo de desarrollo de @rigsofrods nos trae novedades más que notables.


Si nos seguís sabréis que [en el mes de Abril]({{ "/posts/rigs-of-rods-estrena-nueva-version" | absolute_url }}) os hablábamos del lanzamiento de una nueva build de este **proyecto de software libre**, siendo en aquella ocasión el cambio más importante la posibilidad de navegar en su repositorio, e instalar y gestionar los diferentes mods desde dentro del propio juego. En esta ocasión los cambios van principalmente encaminados a añadir nuevas características al editor de creación de pistas y la gestión de la IAs como podréis ver en el video a continuación:  
  



<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/OagiMx2zwTA" title="YouTube video player" width="780"></iframe></div>


 


El [listado completo de cambios](https://forum.rigsofrods.org/threads/rigs-of-rods-2022-12-released.3635/) es el siguiente:


**Añadidos**


* Añadida la edición procedimental de carreteras mediante AngelScript
* Añadida y mejorada la IA
* Añadido el trabajo preliminar para las capacidades de edición de Terreno
* Añadidas varias mejoras y cambios en el selector
* Añadido modo poligonal como cVar
* Añadido un switch para los modos de renderizado en Herramientas
* Añadidos los modos de conducción "drag" y "crash" de la IA.
* Añadido key map Xbox 360 Wireless para Linux
* Añadida interfaz de depuración de colisiones en Herramientas
* Añadido zoom in y out del mapa topográfico con la rueda del ratón
* Se ha añadido el indicador `j` y se ha restaurado `i` por compatibilidad con versiones anteriores.
* Añadidas las funciones AngelScript de ImGui, la interfaz del monitor de scripts y el comando `loadscript
* Añadida la interfaz de usuario de los botones de actor
* Añadida la interfaz y vista de depuración de flexbody
* Añadidos paquetes Conan para Conan V2


**Cambiado**


* Cambiado el parser para ignorar la opción 'v' beam
* Cambiadas las rutas cortas de Conan para las acciones de GitHub en Windows.
* Cambiadas las animaciones de apoyo con "bloqueo de evento" para que permanezcan activas si el personaje abandona al actor.
* Cambiado el mini mapa de reconocimiento para que esté menos recargado.
* Cambiados los mapas de entrada de los mandos Xbox para que sean compatibles con los mandos Xbox Series vía Bluetooth.
* Cambiado el comportamiento de la interfaz de usuario si el cursor está enfocado.
* Cambiado el uso de materiales "nicemetal".
* Cambiado el menú superior para ocultarlo en cámara libre.
* Cambiado y limpiado el panel de control analógico


**Eliminado**


* Eliminado hotkey `E` COMMON_TOGGLE_RENDER_MODE
* Se han eliminado las etiquetas de color de los nombres de los actores en el menú superior.
* Eliminado retraso de EV_COMMON_RESET_SIMULATION_PACE y EV_COMMON_REPLAY_FORWARD
* Eliminado el código muerto en la preparación de los principales cambios del analizador sintáctico


**Corregido**


* Corregidos varios cuelgues durante la edición del terreno
* Se ha corregido un problema por el que los scripts no podían escribirse en el disco.
* Se ha corregido un problema por el que los controles no mostraban los valores predeterminados integrados.
* Corregido un problema con la recarga de un actor no recarga el guión
* Solucionado el conflicto entre la versión de Conan y libpng
* Se ha corregido la construcción con PCH desactivado
* Arreglado un problema cuando un personaje no salía de un actor de red oculto
* Se ha corregido un problema al generar actores con ganchos.
* Se ha corregido un problema al ocultar actores de red.
* Corrección de la comprobación de la versión del archivo de caché
* Corregido fallo al recargar un actor durante una carrera


Para quien no conozca [Rigs of Rods](https://www.rigsofrods.org/), hay que decir que se trata de una plataforma que nos permitirá **experimentar las físicas todo tipo de vehículos** (coches, camiones, barcos, trenes, aviones....) en diferentes entornos. Además de conducirlos y ver como se comportan, el juego posee un **complejo sistema de colisiones**, donde podremos observar como se deforman y rebotan de forma realista los objetos.


Si queréis [descargaros la ultima versión](https://www.rigsofrods.org/download) podéis hacerlo desde la sección de su web dedicada a ello o directamente desde [itch.io](https://rigs-of-rods.itch.io/rigs-of-rods). Puedes contarnos tu parecer sobre esta noticia en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux)[.](https://t.me/jugandoenlinux)

