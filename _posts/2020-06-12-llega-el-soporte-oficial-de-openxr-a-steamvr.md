---
author: Pato
category: Realidad Virtual
date: 2020-06-12 09:47:08
excerpt: "<p>Valve aumenta el soporte al estandar abierto en su plataforma de realidad\
  \ virtual</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SteamVR/SteamVR.webp
joomla_id: 1233
joomla_url: llega-el-soporte-oficial-de-openxr-a-steamvr
layout: post
tags:
- steam
- steamvr
- openxr
title: Llega el soporte oficial de OpenXR a SteamVR
---
Valve aumenta el soporte al estandar abierto en su plataforma de realidad virtual


Valve está decidida a apoyar la realidad virtual como nuevo estandar de juego, y como tal hace tiempo que se encuentra involucrada en todo tipo de desarrollos tanto de videojuegos, con su [Half-Life: Alyx](index.php/homepage/realidad-virtual/1222-half-life-alyx-nativo-en-linux-con-soporte-workshop-y-con-vulkan) como bandera y máximo exponente de lo que es jugar en realidad virtual, como en [soporte y drivers](https://store.steampowered.com/app/250820/SteamVR/) para la plataforma i**ncluyendo en esta a Linux.**


Ahora, anuncian el reciente y futuro soporte a **OpenXR**, el nuevo estandar para la realidad virtual promovido por **Kronos Group** (el consorcio que está detrás de la API abierta Vulkan) y prácticamente todas las grandes empresas del sector como son AMD, ARM, Epic, Facebook, Google, HTC, Microsoft, NVIDIA, Qualcomm, Unity...


De este modo, según Valve los desarrolladores podrán construir sus aplicaciones de Realidad Virtual de forma universal para prácticamente todas las plataformas con soporte VR lo que incluye, como es obvio a Linux.


Si bien es cierto que el soporte aún no es completo pues se está trabajando aún en el, afirman que el 95% de las especificaciones ya se ejecutan correctamente en la plataforma de SteamVR. Además, para los desarrolladores se ha habilitado [un subforo en Steam](https://steamcommunity.com/app/250820/discussions/8/) donde poder encontrar información y soluciones para todo el que lo requiera.


Si quieres saber más sobre **SteamVR** puedes visitar su [página web en Steam](https://store.steampowered.com/app/250820/SteamVR/). Tienes toda la información sobre el soporte de **OpenXR** y enlaces en el [post del anuncio](https://steamcommunity.com/games/250820/announcements) en Steam.


¿Qué te parece la llegada del estandar OpenXR a SteamVR? ¿Crees que la Realidad Virtual tiene futuro en Linux?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

