---
author: Pato
category: Hardware
date: 2020-11-25 11:42:02
excerpt: "<p>La consola con sistema Linux en su interior ser\xE1 lanzada en pr\xF3\
  ximos d\xEDas o semanas aunque solo llegar\xE1 a unos pocos elegidos</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/Ataricat.webp
joomla_id: 1264
joomla_url: atari-anuncia-la-llegada-de-nuevos-juegos-indie-para-la-futura-vcs
layout: post
tags:
- indie
- atari
- atarivcs
title: Atari anuncia la llegada de nuevos juegos indie para la futura VCS
---
La consola con sistema Linux en su interior será lanzada en próximos días o semanas aunque solo llegará a unos pocos elegidos


 


Una vez que todo parece indicar que **Atari** ha conseguido solventar al menos de manera parcial la escasez de componentes y los problemas de transporte y distribución, la llegada de su consola, la **Atari VCS** parece ya inminente aunque de momento afirman que solo podrán suministrar las unidades reservadas a los que aportaron a la campaña de financiación y a un número indeterminado de los que la reservaron posteriormente, quedando el resto pendiente para final de año o principios de 2021.


En cualquier caso, y ya centrada en el inminente lanzamiento Atari ha publicado en una serie de post en su blog en Medium algunos títulos indie que esperan estar disponibles en la tienda de la consola para su lanzamiento o fechas cercanas. Estos son:


### Neko Ghost, Jump!


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/mgM1shyVcSA" width="560"></iframe></div>


### The Spirit of the Samurai


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/VrvZMP6Scys" width="560"></iframe></div>


### SuperMash


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/n_A54BaG23w" width="560"></iframe></div>


 


Es de esperar que Atari vaya desvelando más títulos en próximas fechas, centrándose como parece en títulos de corte indie.


Tienes toda la información en el [blog de Atari en Medium](https://atarivcs.medium.com).

