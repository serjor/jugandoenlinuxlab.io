---
author: Pato
category: Aventuras
date: 2017-03-14 20:52:02
excerpt: "<p>El juego promete soporte para Linux y versi\xF3n sin DRM</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8b072d4f47fa16702f8c7356d2c88ee8.webp
joomla_id: 249
joomla_url: pine-un-mas-que-interesante-juego-estilo-zelda-esta-en-campana-en-kickstarter
layout: post
tags:
- accion
- aventura
- kickstarter
title: "'Pine' un m\xE1s que interesante juego estilo \"Zelda\" est\xE1 en campa\xF1\
  a en Kickstarter"
---
El juego promete soporte para Linux y versión sin DRM

'Pine' Es el prometedor y ambicioso juego de Twirlbound Studios [[web oficial](http://twirlbound.com/)] que está buscando financiación en Kickstarter.


'Pine' [[web oficial](http://twirlbound.com/pine/)] es un juego donde los enemigos aprenderán sobre tu estilo de juego para adaptarse y ponerte las cosas difíciles. En Pine los humanos nunca alcanzaron la cima de la cadena alimentaria. El juego se desarrollará dentro de una isla viva llamada Albamare donde el entorno reacciona, evoluciona y se adapta a ti. 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/SK5fOpmSx1Y" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Jugarás como Hue, un miembro de una tribu en vías de extinción y deberás encontrar un nuevo hogar donde poder subsistir.


'Pine' reconoce la inspiración en aventuras como las de Link en los Zelda, el sistema de némesis en Shadow of Mordor, el progreso según las elecciones de Fable o el sistema de combate de Bloodborne.


A modo de resumen, el juego ofrecerá:


· Una aventura de acción en 3D.


· Explora la isla de Albamare en un entorno de munso abierto.


· Interactúa con diferentes especies que evolucionan de distintas formas.


· Toma el rol de Hue, y encuentra un nuevo hogar para tu especie.


· Los enemigos aprenderán de tus movimientos utilizando una IA avanzada.


Tal y como se puede ver en su web de Kickstarter, el icono de Linux aparece bien visible, y además, en el [apartado de preguntas frecuentes](https://www.kickstarter.com/projects/twirlbound/pine-an-action-adventure-game-that-adapts-to-you/faqs) dejan bien claro que el juego saldrá para Linux. Es más, en su canal de Twitter anuncian una versión sin DRM:



> 
> Confirming a DRM-free version! Also, only a few early bird hours left! Go get it! [#gamedev](https://twitter.com/hashtag/gamedev?src=hash) [#pine](https://twitter.com/hashtag/pine?src=hash) [#kickstarter](https://twitter.com/hashtag/kickstarter?src=hash) <https://t.co/0S66wUeTvJ> [pic.twitter.com/OiDIXSq5E3](https://t.co/OiDIXSq5E3)
> 
> 
> — Twirlbound (@Twirlbound) [14 de marzo de 2017](https://twitter.com/Twirlbound/status/841684122028429313)



 'Pine' lleva solo unos días de campaña y en el momento de escribir estas líneas lleva ya recaudados más de 38.000€ de los 100.000 que piden, con lo que es prácticamente seguro que llegarán a la meta e incluso podrán ampliar los objetivos adicionales.


Si quieres aportar al proyecto, puedes hacerlo en:


<div class="resp-iframe"><iframe height="360" src="https://www.kickstarter.com/projects/twirlbound/pine-an-action-adventure-game-that-adapts-to-you/widget/video.html" width="640"></iframe></div>


 ¿qué te parece este 'Pine? ¿Piensas aportar?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

