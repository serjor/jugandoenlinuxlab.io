---
author: leillo1975
category: "Simulaci\xF3n"
date: 2020-03-27 12:09:36
excerpt: "<p>@SCSsoftware ha anunciado su nueva DLC</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Colorado/ATS_Colorado01.webp
joomla_id: 1197
joomla_url: colorado-sera-el-proximo-estado-de-american-truck-simulator
layout: post
tags:
- dlc
- american-truck-simulator
- ats
- scs-software
- colorado
title: "\"Colorado\" ser\xE1 el pr\xF3ximo estado de American Truck Simulator"
---
@SCSsoftware ha anunciado su nueva DLC


Hemos dicho por activa y por pasiva que el estudio checo [SCS Software](https://scssoft.com/) es sin duda alguna uno de los más trabajadores que conocemos, y ultimamente parece que han metido la marcha más larga pues **no paran de sacar DLCs expandiendo territorio** en sus dos juegos actuales. En esta ocasión le toca una vez más a American Truck Simulator (ATS) , y si bien hace unos pocos meses, en navidad,  anunciaban [Idaho]({{ "/posts/idaho-sera-el-proximo-estado-que-veamos-en-american-truck-simulator" | absolute_url }}) e nuestra hiperdeseada [Iberia](index.php/homepage/generos/simulacion/1142-la-proxima-expansion-de-euro-truck-simulator-2-sera-iberia) (ETS2), ahora avanzan al centro de EEUU hacía [Colorado](https://es.wikipedia.org/wiki/Colorado), entre [Utah](index.php/component/k2/14-simulacion/1251-lanzada-la-dlc-utah-para-american-truck-simulator) y [Nuevo Mexico](index.php/component/k2/20-analisis/656-analisis-american-truck-simulator-dlc-new-mexico). Podeis ver el anuncio que realizaban en Twitter:



> 
> After much guessing & debate from our colorful teaser trailer, today we are putting those theories to rest... ⛰️  
>   
> Introducing Colorado for American Truck Simulator ??   
>   
> See brand new pictures & more at our latest blog ?<https://t.co/ROXVGmIowH> [pic.twitter.com/wt6VaGeZTG](https://t.co/wt6VaGeZTG)
> 
> 
> — SCS Software (@SCSsoftware) [March 26, 2020](https://twitter.com/SCSsoftware/status/1243209715896770560?ref_src=twsrc%5Etfw)


  





 Como es de esperar encontraremos una naturaleza sin igual, y **entre impresionantes montañas, encontraremos valles, rios y llanuras**. Se espera que esta DLC sea lanzada después de Idaho y muy probablemente a **finales de año**. Por ahora poco más tenemos que contar, pero si quereis ver algunas capturas podeis pasaros por este [post en su Blog](https://blog.scssoft.com/2020/03/introducing-colorado.html). Por supuesto, ante cualquier nueva información relevante que nos llegue de esta expansión, o el resto que tienen pendientes, os informaremos cumplidamente. Podeis darnos veustra opinión sobre esta DLC en los comentarios de este artículo, en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

