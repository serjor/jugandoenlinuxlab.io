---
author: Pato
category: Arcade
date: 2019-06-13 18:13:37
excerpt: "<p>El juego de @zapakitul ser\xE1 acci\xF3n espacial arcade sin complejos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/159f7f3b6a3b2311ab075fe0b7d976ad.webp
joomla_id: 1065
joomla_url: space-mercs-estrena-nuevo-trailer-y-anuncia-fecha-de-lanzamiento
layout: post
tags:
- accion
- indie
- arcade
title: Space Mercs estrena nuevo trailer y anuncia fecha de lanzamiento.
---
El juego de @zapakitul será acción espacial arcade sin complejos

Bearded Giant Games sigue desvelándonos detalles de su nuevo juego de acción arcade espacial, y ahora nos trae un trailer donde nos muestra un poco de su jugabilidad y que no tiene desperdicio:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/R1gN_rTf6YQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Además de esto, también sabemos los requisitos, anunciados en su página de Steam:


* **SO:** Ubuntu 18.04
* **Procesador:** cualquiera
* **Gráficos:** Intel HD 4000


Como podéis ver, no pide prácticamente nada. Por otra parte, también se ha hecho pública **la fecha estimada de lanzamiento para el 31 de este próximo mes de Julio**. Si estás interesado en Space Mercs, **puedes descargar la demo** tal y como ya dijimos en un [artículo anterior](index.php/homepage/generos/arcade/33-arcade/1177-ya-puedes-descargar-la-demo-de-space-mercs-desde-la-bearded-giant-store-o-itch-io), donde además dimos mas datos sobre el juego o **puedes añadirlo ya mismo a tu lista de deseados en Steam**, cosa que ayudará al desarrollador a saber tu interés en su juego y a llevar mejor sus estadísticas de venta posteriores.


Toda la información en su [página de Steam en este enlace](https://store.steampowered.com/app/1088600/Space_Mercs/).

