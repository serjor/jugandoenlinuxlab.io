---
author: Serjor
category: Rol
date: 2018-04-06 13:39:42
excerpt: "<p>\xBFVolveremos a enfrentarnos a la Shodan original?</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/cd04214d115e7a754daa198b57282145.webp
joomla_id: 706
joomla_url: nightdive-studios-libera-el-codigo-de-system-shock-1
layout: post
tags:
- system-shock
- nightdive-studios
- codigo-fuente
title: "Nightdive Studios libera el c\xF3digo de System Shock 1"
---
¿Volveremos a enfrentarnos a la Shodan original?

Gracias a un post de Ryan C. Gordon en su [patreon](https://www.patreon.com/icculus) nos enteramos de que la gente de [NightDive Studios](http://www.nightdivestudios.com/) ha liberado el código fuente de System Shock 1 en [GitHub](https://github.com/NightDiveStudios/shockmac), en concreto el código fuente de la versión para Mac.


El caso es que Ryan en su post dejaba entrever que durante el mes de abril se dedicaría a terminar el port de un juego (no especifica cuál) que ya tiene casi terminado, y que cuando acabe dicho port, se preguntaba qué hacer, si portar System Shock o qué, sin confirmar absolutamente nada, pero sí haciendo referencia a que el código del juego está disponible (de hecho probablemente ha sido una coletilla para hacer saber a la gente que el código había sido liberado y poco más).


No obstante, volviendo al tema principal, en la página de GitHub del juego solamente está el código fuente, pero ya avisan de que para poder jugarlo hay que tener los archivos de datos disponibles, los cuales, si no tenemos el juego original, podemos conseguir en [GOG](https://www.gog.com/game/system_shock_enhanced_edition).


Confiemos en que finalmente alguien se anime a portar el juego, que aunque el remake [está en camino](http://systemshock.com/), siempre es interesante poder disfrutar de joyas tan influyentes como System Shock.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/QhRp4HT40PE" width="560"></iframe></div>


Y a ti, ¿te gustaría ver este juego portado a nuestra plataforma? Dínoslo en los comentarios o en los canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

