---
author: leillo1975
category: Ofertas
date: 2019-10-01 19:00:27
excerpt: "<p>Podr\xE1s hacerte con ellos por muy poco dinero en la p\xE1gina de @humble\
  \ Bundle</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0f3f7b3d024c0f56f0acc50bc50d9557.webp
joomla_id: 1115
joomla_url: llevate-un-buen-punado-de-juegos-humble-one-special-day-bundle-2019
layout: post
tags:
- humble-bundle
- oferta
- special-effect
title: "Ll\xE9vate un buen pu\xF1ado de juegos \"Humble One Special Day Bundle 2019\"\
  ."
---
Podrás hacerte con ellos por muy poco dinero en la página de @humble Bundle

Desde ahora mismo y hasta dentro de 21 días tienes la ocasión de comprar esta fantástica compilación de juegos en el "[Humble One Special Day Bundle 2019](https://www.humblebundle.com/games/one-special-day-2019-bundle?partner=jugandoenlinux)", un evento que pretende recaudar fondos para ayudar a personas con discapacidades físicas a que puedan jugar a videojuegos, por lo que además de ahorrarte una buena pasta a cambio de 7 grandes juegos, colaborarás con una labor social de inclusión. Para ello, la gente de Humble Bundle se ha asociado con desarrolladores y editores de juegos una vez más para apoyar el evento de recaudación de fondos "One SpecialEffect's One Special Day!" de la fundación "[Special Effect](http://onespecialday.org.uk/)".


Como suele ser habitual, hay 3 modalidades de pago:


-La primera es pagando **1$ (0.92€)** , donde te llevarás "**Broken Age**" (con soporte nativo) y "**Purrfect Date - Visual Novel/Dating Simulator**" ([Gold con Steam Play](https://www.protondb.com/app/520600))


-La segunda varía a medida que la gente dona, y **en este momento está en 5.09$ (4.67€)**, e incluye además de los anteriores "**The Swords of Ditto: Mormo's Curse**" (con soporte), **Stronghold Crusader 2** ([Platinum con Steam Play](https://www.protondb.com/app/232890)), y **Bomber Crew** (con soporte nativo)  



-Si pagais **10$ (9.18€)** os llevareis los fantáticos **Tannenberg** (con soporte nativo) y **[DIRT 4](index.php/homepage/analisis/20-analisis/1143-analisis-dirt-4)** (con soporte nativo)


Recordad que además de colaborar con la fundación Special Effect, como somos afiliados **también nos ayudareis a nosotros con unos céntimos en cada bundle** para ayudarnos a costear el Hosting y Dominio de nuestra página web. Para ellos solo teneis que seguir este enlace:


### [Humble One Special Day Bundle 2019Humble One Special Day Bundle 2019](https://www.humblebundle.com/games/one-special-day-2019-bundle?partner=jugandoenlinux)


 


Puedes dejar vuestra opinión sobre este bundle en los comentarios de la noticia,  o vuestros mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 


 

 

 