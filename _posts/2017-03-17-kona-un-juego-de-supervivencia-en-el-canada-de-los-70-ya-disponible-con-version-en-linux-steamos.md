---
author: Pato
category: Aventuras
date: 2017-03-17 19:01:43
excerpt: <p>El juego del estudio canadiense "Parabole" ha sido publicado oficialmente
  hoy mismo con soporte en Linux de lanzamiento</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/24051f1a73d7ccf80aa0e781a6178da9.webp
joomla_id: 256
joomla_url: kona-un-juego-de-supervivencia-en-el-canada-de-los-70-ya-disponible-con-version-en-linux-steamos
layout: post
tags:
- indie
- aventura
- steam
- terror
- supervivencia
title: "'Kona' un juego de supervivencia en el Canad\xE1 de los 70 ya disponible con\
  \ versi\xF3n en Linux/SteamOS"
---
El juego del estudio canadiense "Parabole" ha sido publicado oficialmente hoy mismo con soporte en Linux de lanzamiento

'Kona' es un juego al que le sigo la pista desde hace tiempo y hoy por fin me ha llegado la noticia:



> 
> Winter will be longer this year! Official launch of Kona for [@Windows](https://twitter.com/Windows) [#Mac](https://twitter.com/hashtag/Mac?src=hash) [@Linux](https://twitter.com/Linux) [@Xbox](https://twitter.com/Xbox) One [#PS4](https://twitter.com/hashtag/PS4?src=hash) on March 17th 2017! <https://t.co/DxpYkuGA6W>
> 
> 
> — Kona (@KonaGame) [17 de febrero de 2017](https://twitter.com/KonaGame/status/832591895280943104)



A falta de juegos de grandes editoras que cubran el género de misterio y detectives en Linux, la propuesta de 'Kona' [[web oficial](http://www.konagame.com/)] me parece desde luego interesante.


Nos encotramos en el norte de Canadá en el año 1970. Una extraña tormenta de nieve asola el lago Atamipek. El pueblo que hay a los alrededores es el escenario en el que tndremos que investigar los extraños sucesos que allí acontecen a la vez que tendremos que tratar de sobrevivir a los elementos y enemigos que nos iremos encontrando. Desde luego, para ser un desarrollo de muy bajo presupuesto no tiene mala pinta.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/iHdcOzC1UJc" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Argumento:


Octubre de 1970 W. Hamilton, un rico empresario que posee una finca en el norte de Canadá, denuncia varios actos vandálicos perpetrados en su propiedad. Incapaz de descubrir por sí mismo quién está detrás de ello, recurre a Carl Faubert, un renombrado detective privado, para que se ocupe del asunto. En Kona, Carl se ve atrapado por una inesperada tormenta de nieve en el norte profundo. El jugador encarna al detective Faubert, que deberá tratar de aclarar los misteriosos sucesos que ocurren en aquel lugar.


Características:


· Sumérgete en un relato interactivo surrealista de misterio e investiga la inquietante tranquilidad en la que está sumido el pueblo.  
· Explora el enorme y gélido norte, y enfréntate a los elementos para sobrevivir.  
· Disfruta de una banda sonora ambiental de la mano del grupo de música folk de Quebec CuréLabel.  
· Vive la historia a través de un narrador omnisciente, en tercera persona.  
· Regresa al pasado con una apariencia vintage y rememora a las comunidades rurales de los 70.


En cuanto a los requisitos, los recomendados son:


SO: Ubuntu 12.04+ 64-bit, SteamOS+  
Procesador: i5 2.5 ghz+  
Gráficos: GeForce GTX 660 / ATI Radeon 7850


Como ves, tampoco pide demasiado. Si estás interesado, puedes encontrar Kona en su página de Steam traducido al español:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/365160/" width="646"></iframe></div>


¿Que te parece Kona? ¿Te gustan los juegos de supervivencia o te va más ir en plan detective?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

