---
author: Pato
category: "Acci\xF3n"
date: 2017-11-08 18:07:24
excerpt: "<p>Lucha contra otros jugadores en l\xEDnea en este juego de Acci\xF3n espacial\
  \ multijugador</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f14a855e71266fce000280ce97ff2b84.webp
joomla_id: 523
joomla_url: starblast-ya-esta-disponible-con-version-linux-steamos-de-salida
layout: post
tags:
- accion
- indie
- multijugador
- casual
- masivo
title: "'Starblast' ya est\xE1 disponible con versi\xF3n Linux/SteamOS de salida"
---
Lucha contra otros jugadores en línea en este juego de Acción espacial multijugador

Un nuevo lanzamiento. Hoy nos llega 'Starblast', un juego de acción multijugador masivo que hará las delicias de los que buscan una nueva vuelta de tuerca a los juegos de acción espacial.


'Starblast' [[web oficial](https://neuronality.com/starblast.html)] es un juego de acción espacial multijugador en línea. *Toma el control de tu nave espacial, mina asteroides, incrementa tus estadísticas, mejora tus naves con mejoras y nuevas armas y lucha contra otros jugadores para robarles sus gemas.*


El juego ofrece 31 modelos únicos de naves, cientos de mapas, 10 clases de armas y poderes especiales. Tienes a tu disposición 4 modos de juego: Supervivencia, por equipos, deathmatch y un modo sorpresa que se desvelará hoy, día de lanzamiento.


*Puedes crear arenas privadas o si lo prefieres, eventos públicos especiales con una gran cantidad de opciones de personalización. El Campeonato "Pro-Deathmatch" está siempre online con la competición e-sport de Starblast.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/q9wuV3P8Adc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Si te animas a echar unos disparos en el espacio con/contra otros jugadores, tienes disponible este 'Starblast' traducido al español en su página de Steam con un 10% de descuento por su lanzamiento:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/673260/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

