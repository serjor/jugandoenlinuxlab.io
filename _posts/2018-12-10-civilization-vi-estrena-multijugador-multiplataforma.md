---
author: leillo1975
category: Estrategia
date: 2018-12-10 20:30:02
excerpt: <p>@AspyrMedia ha lanzado un parche que permite jugar entre SO's diferentes.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/435f56c3ee714a62eba91b0d67860eb3.webp
joomla_id: 927
joomla_url: civilization-vi-estrena-multijugador-multiplataforma
layout: post
tags:
- multijugador
- aspyr-media
- civilization-vi
- firaxis
- multiplataforma
- 2k-games
title: '"Civilization VI" estrena multijugador multiplataforma.'
---
@AspyrMedia ha lanzado un parche que permite jugar entre SO's diferentes.

Finalmente lo han logrado, bastante tarde pero lo han logrado. Tras [22 meses después](index.php/homepage/generos/estrategia/8-estrategia/334-sid-meier-s-civilization-vi-ya-esta-disponible-en-linux-steamos) de su lanzamiento, numerosas actualizaciones importantes, una expansión fenomenal como [Rise & Fall](index.php/homepage/analisis/20-analisis/829-analisis-civilization-vi-rise-fall-dlc), y el anuncio de otra en ciernes ([Gathering Storm](index.php/homepage/generos/estrategia/8-estrategia/1031-gathering-storm-sera-la-nueva-dlc-de-civilization-vi)) ; hoy al fin ha llegado una de las **principales demandas de los jugadores** Linuxeros, algo tan elemental y necesario como poder disputar partidas online con jugadores de otros sistemas operativos, como los usuarios de Windows.


Tan ansiado anuncio se ha producido finalmente esta misma tarde con un [mensaje en los foros de Steam](https://steamcommunity.com/app/289070/discussions/0/144512942755435118/?ctp=125#c1743353798898257598), aunque hay que aclarar que los usuarios que juguen con un **procesador i3** (basicamente los "culpables" de que no haya salido el parche antes) podrían tener problemas. También es cierto que la propia Aspyr ha dado una solución temporal que consiste en agregar una partición adicional de intercambio (swap) de 2GB (no es la mejor de las soluciones, pero algo es algo...).


También se ha detectado que algunos usuarios tienen algún que otro problema con el Launcher del juego, aunque que se soluciona añadiendo el siguiente parametro de lanzamiento en Steam:


`LD_PRELOAD=/usr/lib/libfreetype.so %command%`


Nos gustaría desde aquí **agradecer a Aspyr que no haya abandonado** en el empeño de dotar de esta funcionalidad tan necesaria después de tantos esfuerzos y contratiempos. Probablemente muchos otros desarrolladores hubiesen abandonado. Mientras esperamos ansiosos por su próxima gran Expansión, Gathering Storms, podremos disfrutar de la compañía de usuarios de otros sistemas en uno de nuestros juegos favoritos: [Civilization VI](index.php/homepage/analisis/20-analisis/352-analisis-sid-meier-s-civilization-vi).


Podeis comprar este juego en la **[tienda de Humble Bundle](https://www.humblebundle.com/store/sid-meiers-civilization-6?partner=jugandoenlinux)**, así como su expansión **[Rise & Fall](https://www.humblebundle.com/store/sid-meiers-civilization-vi-rise-and-fall?partner=jugandoenlinux)**; y os dejamos en compañía del trailer de Gathering Storms:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/trNUE32O-do" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 ¿Sois jugadores de Civilization VI? ¿Habeis probado ya el modo Multijugador? ¿Qué os parece el poder jugar contra usuarios de otros sistemas? Opina en los comentarios o charla con nosotros en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

