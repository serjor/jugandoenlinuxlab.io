---
author: P_Vader
category: "Acci\xF3n"
date: 2021-06-16 15:37:58
excerpt: "<p>Y que mejor manera de probarlo que jugar una partida de los viernes con\
  \ la comunidad @JugandoEnLinux .</p>\r\n"
image: https://cdn.cloudflare.steamstatic.com/steam/apps/550/ss_ba2ea2eda245f89626277457ae2ab76ba997f46a.1920x1080.jpg
joomla_id: 1316
joomla_url: l4d2-se-actualiza-con-sorpote-para-vulkan
layout: post
tags:
- vulkan
- fps
- l4d2
title: L4D2 se actualiza con sorpote para Vulkan
---
Y que mejor manera de probarlo que jugar una partida de los viernes con la comunidad @JugandoEnLinux .


Gracias al aviso de CharDSon en nuestro canal de [Telegram](https://telegram.me/jugandoenlinux), hemos sabido que **L4D2 ha recibido [una actualización](https://steamcommunity.com/games/L4D2/announcements/detail/4625740823862061052) que entre otras cosas permite el uso de Vulkan** usando DXVK, como ya hizo con Portal 2, permitiendo la traducción de DirectX9 a Vulkan, en lugar de OpenGL como hace por defecto. Es una gran noticia que Valve continue exprimiendo el uso de DXVK de formas insospechadas hasta no hace mucho.


Desde la comunidad JugandoEnLinux nos apasiona este juego y **vamos a testear esta nueva actualización como es debido ¡con la clásica partida de los viernes!.   
El viernes 18/6/2021 a las 22:00 (utc+2) estais invitados a acompañarnos en un exterminio zombi**, con la ayuda de Vulkan**.** Pasate por nuestro canal de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) para organizarnos y **no olvides tener preparado [Mumble](https://www.mumble.info/)** con nuestro servidor [mumble.jugandoenlinux.com](http://mumble.jugandoenlinux.com:64738/)


Recuerda que este nueva característica no viene activado por defecto y para ello tendrás que añadir "-vulkan" a los parámetros de lanzamiento:


![l4d2 parametros vulkan](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/l4d2/l4d2_parametros_vulkan.webp)


¡Mira como nos lo pasamos con este juego! Estos son Zombies de verdad [Rick Grimes](https://es.wikipedia.org/wiki/Rick_Grimes):


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/zyDTmd28x48" title="YouTube video player" width="780"></iframe></div>
