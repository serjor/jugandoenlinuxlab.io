---
author: Pato
category: Estrategia
date: 2018-02-02 17:54:55
excerpt: "<p>La colonizaci\xF3n de Marte te espera... Esperemos que la versi\xF3n\
  \ de Linux \"despegue\" a tiempo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e208d5104268085aecc7ece94f6f710e.webp
joomla_id: 633
joomla_url: surviving-mars-llegara-el-proximo-15-de-marzo-y-lanza-un-nuevo-trailer
layout: post
tags:
- espacio
- proximamente
- simulador
- estrategia
title: "'Surviving Mars' llegar\xE1 el pr\xF3ximo 15 de marzo y lanza un nuevo trailer\
  \ (ACTUALIZADO)"
---
La colonización de Marte te espera... Esperemos que la versión de Linux "despegue" a tiempo

**ACTUALIZACIÓN 13-2-18:** Paradox acaba de publicar en su canal de Youtube un **nuevo trailer** llamado "Life on Mars" con motivo de la apertura de la fase de **precompra**. En el podemos ver varios aspectos de este interesante juego que no habíamos visto hasta la fecha. **El juego tendrá [3 versiones diferentes](https://www.paradoxplaza.com/surviving-mars/SUSM01GSK-MASTER.html?utm_source=youtube&utm_medium=video&utm_content=video&utm_campaign=suma_suma_youtube_all_2018212_pre)** (Standard a 39.99€; Deluxe a 49.99; y First Colony, a 74.99€) y un **"Season Pass". Podeis ver este interesante trailer justo aquí debajo:**


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/7Js-9zMQ1oQ" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>




---


**NOTICIA ORIGINAL:** El juego de estrategia y gestión de "ciudades" con un enfoque un tanto particular 'Surviving Mars', del que [ya anunciamos](index.php/homepage/generos/estrategia/item/439-paradox-interactive-mueve-ficha-anuncia-surviving-mars-y-lanzaran-battletech) su próximo lanzamiento hace unos meses llegará el próximo día 15 de Marzo, tal y como han anunciado los chicos de Haemimont Games y Paradox Interactive en [un anuncio](http://steamcommunity.com/games/464920/announcements/detail/1653253902590607263) en Steam. se trata de un juego donde tendremos que ponernos en la piel de una agencia espacial para llevar a cabo la tarea de colonizar Marte, nuestro planeta rojo.


*"Surviving Mars es un constructor de ciudades de ciencia ficción que trata de colonizar Marte y sobrevivir al intento. Elige una agencia espacial para obtener recursos y apoyo financiero antes de decidir la ubicación de tu colonia. Construye cúpulas e infraestructuras, investiga nuevas posibilidades y utiliza drones para desbloquear maneras más elaboradas de modelar y ampliar tu asentamiento. Cultiva tus propios alimentos, extrae minerales o relájate en el bar después de una larga jornada de trabajo. Pero lo más importante es mantener con vida a tus colonos, una tarea harto compleja en este nuevo y extraño planeta."*


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/u3swV4JJKAc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Entre las características principales, tendremos que gestionar un entorno sostenible dentro de un ambiente hostil y tratar de gestionar nuestra colonia para poder colonizar el planeta con la menor pérdida de colonos posible.


en cuanto a los requisitos de sistema, son:


**Mínimo:**  

+ **SO:** Ubuntu 14 x86 o superior
+ **Procesador:** Dual-core CPU
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** GeForce 600/AMD Radeon 7000 o superior con 1GB de video RAM
+ **Almacenamiento:** 10 GB de espacio disponible


**Recomendado:**  

+ **SO:** Ubuntu 14 x86 o superior
+ **Procesador:** quad-core CPUs
+ **Memoria:** 8 MB de RAM
+ **Gráficos:** GeForce 970 o equivalente con 4GB  de video RAM
+ **Almacenamiento:** 10 GB de espacio disponible


Si quieres saber más sobre este 'Surviving Mars' tienes toda la información disponible en su página de Steam donde podrás comprarlo traducido al español:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/464920/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>
