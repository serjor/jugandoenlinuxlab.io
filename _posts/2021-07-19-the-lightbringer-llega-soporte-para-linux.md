---
author: P_Vader
category: Plataformas
date: 2021-07-19 18:08:32
excerpt: "<p>Despu\xE9s de unos muy buenos comentarios por parte de nuestra comunidad,\
  \ finalmente @GetLightbringer se estrena con soporte para Linux</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/lightbringer/Lightbringer.webp
joomla_id: 1328
joomla_url: the-lightbringer-llega-soporte-para-linux
layout: post
tags:
- plataformas
- vista-isometrica
- buena-trama
title: The Lightbringer llega con soporte para Linux (ACTUALIZADO)
---
Después de unos muy buenos comentarios por parte de nuestra comunidad, finalmente @GetLightbringer se estrena con soporte para Linux


**ACTUALIZADO 7-10-2021:** Como habían prometido  **ha llegado **finalmente** el día de su lanzamiento y este juego tan coqueto llega con su soporte para Linux desde el primer día**. Como ya os contamos, los desarrolladores agradecieron enormemente la retroalimentación recibida por los usuarios de nuestra sistema operativo y han cuidado que funcione correctamente.


Recordad que tenéis la demo para probarlo, que siempre es de agradecer para tantear un juego. Personalmente lo que he probado con la demo, ya os dije que me gustó bastante y ya está en la lista de deseados. Hay pendiente de subsanar un pequeño fallo de unity que obvia [archivos para otros sistemas al subirlo a steam](https://steamcommunity.com/app/1561660/discussions/0/3074251201349596946/?ctp=3#c3085520348624169681), pero nada importante.


¡Ánimo y probarlo!

![LightBringer](https://media.zordixpublishing.com/2021/03/Screenshot_219_2.jpg)

---


**ORIGINAL: 19-07-2021:** Hace un tiempo tras ser preguntados en el [foro de steam](https://steamcommunity.com/app/1561660/discussions/0/3074251201349596946/) por un posible soporte para Linux, los encargados del desarrollo de The Lightbringer dejaron alguna demo para que la probásemos y **tras la buena retroalimentación recibida por parte de nuestra comunidad, han decidido que darán [soporte para Linux](https://store.steampowered.com/news/app/1561660/view/4548056813620341864)** en su juego.



> 
> Tras algunos comentarios de la comunidad, hemos decidido apoyar oficialmente a Linux, ya que queremos que el mayor número de personas pueda experimentar este viaje poético.
>

El juego es un **plataformas tridimensional con puzles y con una vista casi isométrica.** Lo que hemos podido probar en su demo tiene un aire a Zelda con un diseño muy bien cuidado. A lo largo del juego la hermana de la protagonista **nos va desvelando la trama de manera muy poética** (en inglés ahora mismo).


 <div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/FNZ-XVLqaho" title="YouTube video player" width="780"></iframe></div>


Las características que según sus creadores definen al juego son:


* **Una aventura como en los viejos tiempos:**   
The Lightbringer se inspira en gran medida en los anteriores Zelda games, pues trata de invocar esa sensación de querer explorar cada rincón.
* **Jugabilidad semiisométrica:**   
con una cámara giratoria en una vista isométrica, tendrás que analizar el campo desde varios ángulos diferentes para encontrar soluciones a tus obstáculos. Recuerda que, muchas veces, hay más de lo que crees...
* **Un viaje poético:**   
el espíritu de tu hermana guía todo tu viaje narrando el juego completamente en verso.
* **Mazmorras peligrosas:**   
explora mazmorras oscuras llenas de peligro. ¡Ten cuidado con las trampas, los secretos y ese molesto cieno!


 **Tienes disponible la [demo en Steam](https://store.steampowered.com/app/1561660/The_Lightbringer/)** y puedes añadirlo a tu lista de deseados como vamos a hacer nosotros, porque lo poco que lo hemos probado, nos ha gustado.


<div class="steam-iframe"><iframe height="200" src="https://store.steampowered.com/widget/1561660/" style="border: 0px;" width="646"></iframe></div>


¿Has probado ya la demo de The Lightbringer? Cuéntanos tus impresiones en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 


 

