---
author: Pato
category: Plataformas
date: 2020-02-11 21:34:41
excerpt: "<p>El excelente juego de @motiontwin nos trae jugosas novedades</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DeadCells/Dead-Cells-The-Bad-seed.webp
joomla_id: 1168
joomla_url: dead-cells-se-expande-con-su-nuevo-dlc-the-bad-seed
layout: post
tags:
- accion
- indie
- plataformas
title: Dead Cells se expande con su nuevo DLC The Bad Seed
---
El excelente juego de @motiontwin nos trae jugosas novedades


Hoy nos hacemos eco de la expansión de uno de los mejores juegos en su género que tenemos disponibles en nuestro sistema favorito.


Hablamos de **Dead Cells**, el excelente juego de acción y plataformas de Motion Twin que ahora se expande con un nuevo DLC titulado **The Bad Seed**, en el que tendremos que explorar el relajante Invernadero, abrirnos camino a través de la pútrida Ciénaga y enfréntarnos a un nuevo jefe.


*Podrás jugar en:*


* ***El Invernadero en Ruinas**: un lugar relajante y tranquilo habitado por un pacífico clan de setas con el deseo lógico de asesinar al Decapitado.*
* ***La Ciénaga de los Desterrados**: un entorno nocivo gobernado por una banda de tres mutantes moradores de los árboles que van armados con palos afilados, hombres rana escurridizos que disparan dardos y un grupo de letales chupasangre.*
* ***El Nido**: el territorio de la Garrapata madre, si has visto Starship Troopers: Las brigadas del espacio, te haces una idea...*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/MqlDDiEL6tE" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Dead Cells es un juego inspirado en los clásicos Castlevania con elementos rogue-like donde no tendremos check points si no muerte permanente, con lo que tendremos que perfeccionar nuestras técnicas de combate al estilo de los Souls, matar, morir, aprender y repetir.


Si aceptas el reto, tienes este excelente **Dead Cells: The Bad Seed** [en Steam](https://store.steampowered.com/app/1204130/Dead_Cells_The_Bad_Seed/), o si aún no lo tienes, puedes obtener el juego base en [Humble Bundle](https://www.humblebundle.com/store/dead-cells?partner=jugandoenlinux) (enlace patrocinado) o también [en Steam](https://store.steampowered.com/app/588650/Dead_Cells/) donde está ahora mismo **con un 30% de descuento**.


¿Que te parece Dead Cells? ¿Jugarás a "The Bad Seed"?


Cuéntanoslo en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

