---
author: Pato
category: Hardware
date: 2021-07-15 17:24:40
excerpt: "<p>Valve vuelve a intentar entrar en el mercado del hardware con un dispositivo\
  \ versatil de la mano de AMD</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/SteamDeck1.webp
joomla_id: 1326
joomla_url: valve-anuncia-steam-deck-su-propia-consola-portatil
layout: post
tags:
- steamos
- steam
- hardware
- steam-deck
title: 'Valve anuncia STEAM DECK, su propia consola portatil '
---
Valve vuelve a intentar entrar en el mercado del hardware con un dispositivo versatil de la mano de AMD


Era un rumor cada vez más fuerte. Casi un secreto a voces que Valve estaba detrás de su propia "Steam Machine". Las referencias al hardware eran cada vez mayores, y tras las enigmáticas palabras de Gabe en la ya famosa conferencia, por fin ha salido a la luz lo que han estado preparando.


#### Steam Deck, portatil con suficiente poder


Tras la aparición en Steam de un apartado de hardware nuevo, nos anuncian la que será la nueva portatil de Valve, que correrá con una APU de AMD con CPU Zen 2 4c/6t, 2.4-3.5 GHz personalizada, y una **GPU 8 RDNA 2 CUs, 16 GB de RAM LPDDR5 y 64 GB, 250 o 512GB** de almacenamiento según la versión que elijamos.


Respecto a los mandos, traerá los típicos **analógicos**, dos **t*rackpads* hápticos** al estilo de los del Steam Controller aunque esta vez cuadrados, **gatillos**, **palancas laterales, los correspondientes botones frontales** **y giroscopio**.


En cuanto a la pantalla será de **7 pulgadas táctil, 1280x800 LCD 400 nits** con sensor de luz ambiental, y vendrá con **conector USB-C con soporte Display port 1.4** para poder conectar una pantalla externa a una resolución de hasta 8K @ 60 Hz o 4K @ 120 Hz, USB 3.2 Gen 2.


También **se anuncia que tendrá un dock oficial** al que podremos conectar para expandir las posibilidades de conexión con **puertos USB o red cableada.**Este dock se tendrá que adquirir aparte y estará disponible para reserva mañana mismo a partir de las 19:00h UTC+2.


La conectividad corre a cargo de WIFI 2,4 y 5 GHz y bluetooth 5.0, audio estereo con DSP, matriz de micrófono dual y soportará expansión por tarjetas microSD.


En cuanto al tamaño y peso, será de 298mm x 117mm x 49mm y 669 gramos respectivamente.


#### Un sistema para dominar el juego


Vamos a lo mollar. Aunque Valve asegura que podremos instalar el sistema que deseemos, lo cierto es que el dispositivo vendrá con un nuevo sistema **SteamOS 3.0 basado en Arch** del que ya habían rumores de desarrollo interno, aunque nada se sabía a ciencia cierta hasta ahora pues lo han llevado de forma bastante discréta. Por lo demás, parece que tendrá un modo **escritorio a cargo de KDE Plasma**.


De cara a los desarrolladores, Valve ha puesto a su disposición un completo kit de desarrollo para asegurarse de que los juegos funcionen corréctamente en el dispositivo. En cualquier caso, para los juegos que no sean nativos, el dispositivo tirará de Proton, la capa de compatibilidad de Steam con la que aseguran que el aparato podrá correr "gran cantidad" de juegos.


#### Precio comedido para lo que ofrece


Si bien es cierto que un aparato de estas características no puede salir muy barato, lo cierto es que se puede **pre-comprar ya para recibir las unidades en Diciembre de este mismo 2021** a un **precio de partida de 419€**.


Si quieres ver toda la información, puedes visitar la página del anuncio en Steam [en este enlace](https://store.steampowered.com/steamdeck), o la [página web oficial de la "Steam Deck"](https://www.steamdeck.com/es/) donde puedes ver la información más exhaustiva y hacer la reserva de la máquina.


¿Qué te parece la Steam Deck? ¿Piensas reservar una?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

