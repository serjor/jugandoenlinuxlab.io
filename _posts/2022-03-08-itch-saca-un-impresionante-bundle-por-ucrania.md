---
author: P_Vader
category: Ofertas
date: 2022-03-08 13:54:12
excerpt: "<p>Todo lo recaudado ir\xE1 para ayuda humanitaria ante la cat\xE1strofe\
  \ humanitaria.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ItchBundleUcrania/itch_bundle_ukraine.webp
joomla_id: 1447
joomla_url: itch-saca-un-impresionante-bundle-por-ucrania
layout: post
tags:
- itch-io
- bundle
title: Itch saca un impresionante bundle por Ucrania
---
Todo lo recaudado irá para ayuda humanitaria ante la catástrofe humanitaria.


Ya sabemos que Itch siempre se posiciona en causas justas y no duda en apoyarlas de la mejor manera que puede. En esta ocasión [quieren recaudar por lo menos un millón](https://itch.io/b/1316/bundle-for-ukraine) de dólares para dos organizaciones de ayuda humanitaria ante la catastrófica situación que vive Ucrania. Normalmente este paquete te costaría 6500 dólares, pero lo tienes a partir de 10, te ahorras un 99%.


 


El catalogo de [**videojuegos, juegos de rol, libros o música es impresionante**](https://itch.io/b/1316/bundle-for-ukraine). Algunos de los desarrolladores se han dado de alta expresamente en Itch para poder incluir su producto dentro de este paquete. **En total son 1000 productos de los cuales 600 son videojuegos ¡y que videojuegos!**


[![itch bundle ukraine Screenshot](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ItchBundleUcrania/itch_bundle_ukraine_Screenshot.webp)](https://itch.io/b/1316/bundle-for-ukraine)


* **SkateBIRD**
* CrossCode
* Figment
* **Sundered**
* A short Hike
* Baba is you
* **Celeste**
* Jotun
* **Hidden Folks**
* ....


 


De verdad que la calidad es brutal y la causa se merece todo nuestro apoyo. **Las organizaciones que destinarán el dinero serán [International Medical Corps](https://internationalmedicalcorps.org/country/ukraine/) que da soporte sanitario en el terreno y [Voices of Children](https://voices.org.ua/en/who-we-are/) que ayuda a los niños a superar la situación traumática que están viviendo**, al 50% cada una de todo lo recaudado. Esta promoción durará 10 días.


**¿Te animas a apoyar esta causa**? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

