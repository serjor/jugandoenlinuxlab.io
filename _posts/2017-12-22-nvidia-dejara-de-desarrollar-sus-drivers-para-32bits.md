---
author: leillo1975
category: Hardware
date: 2017-12-22 11:07:44
excerpt: "<p>El fabricante de gr\xE1ficas solo crear\xE1 nuevos drivers de 64 bits.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9fc5e2a09509b5613281f31393bb0737.webp
joomla_id: 583
joomla_url: nvidia-dejara-de-desarrollar-sus-drivers-para-32bits
layout: post
tags:
- nvidia
- privativo
- driver
- 32bits
- 64bits
title: "NVIDIA dejar\xE1 de desarrollar sus drivers para 32bits."
---
El fabricante de gráficas solo creará nuevos drivers de 64 bits.

Parece que poco a poco los desarrolladores de software están moviendo ficha para olvidar definitivamente los 32 bits. Como bien sabeis, multitud de compañías están abandonando definitivamente estos desarrollos. Hace algunos meses veíamos como Cannonical solo sacaba la versión de 64 bits de su ultima release de Ubuntu, y softwares de todo tipo solo se compilan ya para esta arquitectura, entre ellos muchos juegos. Es algo lógico pensar que casi 15 años después del lanzamiento de [AMD64](https://es.wikipedia.org/wiki/AMD_Athlon_64) se empezase a tomar en serio el dejar de dar soporte a los 32 bits.


![AMD64](https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/KL_AMD_Athlon_64_Venice.jpg/1024px-KL_AMD_Athlon_64_Venice.webp)


*El primer procesador x86 de 64bits fué fabricado por AMD*


El tiempo pasa y es inexorable, por lo que Nvidia tampoco escapa a esta situación y también se suma al carro. De esta forma, **su versión 390 será la ultima que se podrá instalar para 32 bits** tanto en Windows, como FreeBSD, y por supuesto nuestro querido GNU/Linux. A partir de ahora simplemente se dedicarán a dotar de soporte de seguridad hasta 2019.


A nivel personal, sinceramente, no tiene mucho sentido seguir instalando sistemas operativos de 32 bits, obviamente siempre que tengamos la posibilidad de hacerlo con 64bits, ya que las ventajas de este último son muchas, especialmente en el **manejo de la memoria**, ya que como seguro que muchos sabreis, los 32bits solo pueden manejar 3GB y en 64Bits esa cantidad aumenta hasta los 512GB. Los 64bits también permiten usar con más facilidad aplicaciones más pesadas y sobre el papel tienen que ser más rápidas.


Y tu, ¿usas sistemas de 64 bits o de 32? ¿Qué te parece este movimiento de Nvidia? Únete a este debate dejando tu opinión en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).


***Fuentes**: [Planeta Diego](https://planetadiego.com/2017/12/22/nvidia-tambien-abandona-el-soporte-a-los-32-bits-en-sus-controladores-para-windows-linux-y-freebsd/), [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=32-bit-NVIDIA-Drop-Dropping)*

