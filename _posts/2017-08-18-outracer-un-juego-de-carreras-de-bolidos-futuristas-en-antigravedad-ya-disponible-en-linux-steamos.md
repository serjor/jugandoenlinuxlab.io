---
author: Pato
category: Carreras
date: 2017-08-18 12:59:20
excerpt: "<p>\"\xBFTienes lo que hay que tener para pasarlos a todos?\"</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4693ba70fe7ccf8c5461d5f42e16af89.webp
joomla_id: 436
joomla_url: outracer-un-juego-de-carreras-de-bolidos-futuristas-en-antigravedad-ya-disponible-en-linux-steamos
layout: post
tags:
- carreras
- indie
title: "'Outracer' un juego de carreras de b\xF3lidos futuristas en \"antigravedad\"\
  \ ya disponible en Linux/SteamOS"
---
"¿Tienes lo que hay que tener para pasarlos a todos?"

Probablemente los viejunos del lugar recuerden aquel juegazo que LucasArts se sacó de la manga con Star Wars Episode I: Racer. Yo lo disfruté en su momento con una aceleradora gráfica de aquella época, pero tras las geniales "carreras de vainas" muy pocos han llegado a la experiencia de juego, las emocionantes carreras, el carisma (solo con decir Star Wars es meterte de lleno en otro universo) y el multijugador de este.


Pues bien, ahora nos llega Outracer [[web oficial](https://outracer.com/)] donde tendremos que correr con naves en "gravedad cero"  en circuitos futuristas, y en seguida me ha venido a la cabeza el paralelismo. ¿Resistirá la comparación?


Outracer es el desarrollo de "Outlier Interactive", estudio formado por una sola persona, y en el tendremos que enfrentarnos a unos desafiantes contrincantes en circuitos futuristas suspendidos en el aire con nuestras naves de gravedad cero.


Tendremos que dominar nuestra máquina, conducirla con precisión y acelerar a velocidades de vértigo. Si los oponentes no te derrotan las impredecibles condiciones del circuito serán otro obstáculo a tener en cuenta.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/J83qTA88uaE" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


**Características:**


* Corre con vehículos de Anti-gravedad a través de pistas estilo "montaña rusa"!
* Unas 20 pistas de alta velocidad para ponerte a prueba
* 8 Vehículos únicos donde elegir
* Corre contra hasta 30 oponentes controlados por la IA!
* Corre con tus amigos en el modo pantalla partida para hasta 4 jugadores!
* Crea tus propias pistas con el poderoso editor de pistas del juego
* Juega al modo Hipervelocidad y alcanza velocidades nunca vistas, ¡si sobrevives lo suficiente!
* Corre con tu propia música utilizando canciones personalizadas!


Outracer no está traducido al español, pero si te atraen las carreras de este estilo y no es problema para ti, tienes Outracer disponible en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/669350/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Piensas darle una oportunidad a Outracer? ¿Te gustan los juegos de velocidad?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

