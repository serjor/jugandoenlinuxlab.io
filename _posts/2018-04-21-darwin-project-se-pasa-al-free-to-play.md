---
author: Pato
category: "Acci\xF3n"
date: 2018-04-21 21:14:32
excerpt: <p>El juego de Scavengers Studio tiene bastantes papeletas de llegar a nuestro
  sistema</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ac4dbc5bf3793d268025746e30ceec34.webp
joomla_id: 717
joomla_url: darwin-project-se-pasa-al-free-to-play
layout: post
tags:
- accion
- battle-royale
title: Darwin Project se pasa al "Free to Play"
---
El juego de Scavengers Studio tiene bastantes papeletas de llegar a nuestro sistema

Esta semana ya avisamos de que el battle royale 'Darwin Project' tenía posibilidades de llegar a Linux/SteamOS, e incluso ha habido bastante movimiento en los repositorios del juego para linux en SteamDB. Sin embargo, en un movimiento bastante sorpresivo, el juego ha pasado de ser de pago a ser "free to play", con el consiguiente desconcierto de los propios usuarios y de los que han comprado el juego en los últimos días produciéndose una sucesión de reclamaciones de devolución del dinero de compra en los comentarios del anuncio.


Así puede leerse en el [anuncio oficial](https://steamcommunity.com/gid/103582791461263257/announcements/detail/2521329249666273872https://steamcommunity.com/games/544920/announcements/detail/2521329249666273872) en el que los desarrolladores han publicado en los foros del juego en Steam, donde explican que todo aquel que quiera, podrá recuperar su dinero independientemente de la fecha de compra, o si no como compensación, todo aquel que haya comprado el juego obtendrá un "pack fundador" con 2 sets legendarios, 3 hachas Legendarias, 3 arcos Legendarios, una colección completa de "monos" y 5 regalos de Fans, todo ello lo recibirán el próximo día 24 de Abril.


*'Darwin Project' tiene lugar en un futuro apocalíptico distóptico en el norte de las islas de Canadá. Como preparación para una edad de hielo, se lanzará un nuevo proyecto mitad experimento científico, mitad entretenimiento que propondrá a 10 participantes sobrevivir al frío y la muerte en una arena traicionera.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/RllGB0j27f0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Personalmente, este tipo de movimientos me parecen cuanto menos "jugar" con la base de usuarios que desde un primer momento han apoyado tu proyecto, aunque entiendo que bajar la barrera de entrada para aumentar la audiencia a costa de quitar el precio es algo que puede ayudar a despegar al propio juego. En cuanto a nosotros, como aún no tenemos el juego en nuestro sistema favorito, no podremos quejarnos de poderlo jugar gratuitamente. ¿No creéis?


Mas información en el [anuncio del juego](https://steamcommunity.com/games/544920/announcements/detail/2521329249666273872) en Steam.


 ¿Qué te parece el cambio de modelo de negocio de Project Darwin? ¿Esperas que llegue a Linux próximamente?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

