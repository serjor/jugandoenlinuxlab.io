---
author: Pato
category: "Acci\xF3n"
date: 2017-08-31 20:20:40
excerpt: "<p>Rockfish Games acaba de anunciar que ha lanzado \"oficialmente\" una\
  \ versi\xF3n \"no oficial\" del juego</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/24795b9d45550d61cc9f6bee7a09a629.webp
joomla_id: 448
joomla_url: everspace-sigueen-desarrollo-para-linux-pero-tienen-problemas-con-graficas-amd
layout: post
tags:
- accion
- indie
- espacio
- exploracion
title: "'EVERSPACE' sigue en desarrollo para Linux, pero tienen problemas con gr\xE1\
  ficas AMD [Actualizaci\xF3n]"
---
Rockfish Games acaba de anunciar que ha lanzado "oficialmente" una versión "no oficial" del juego

**[Actualización]** Rockfish Games ha anunciado en el hilo del juego donde se estaba hablando del desarrollo del juego para Linux que han lanzado una versión "no oficial" del juego para poder probarlo:



> 
> *Os alegrará saber que oficialmente no-oficialmente hemos lanzado la versión de Linux. Por favor, dadnos feedback aquí: [http://steamcommunity.com/app/396750/discussions/0](http://steamcommunity.com/app/396750/discussions/0/)/ o para problemas específicos, que requieran tratamiento abrir un hilo en la sección "bugs y problemas técnicos".*
> 
> 
> 


Puedes ver el anuncio [en este hilo](http://steamcommunity.com/app/396750/discussions/0/333656722966793785/?tscn=1504877332).


**[Artículo original]**


El desarrollo de 'Everspace' [[Steam](http://store.steampowered.com/app/396750)] [[web oficial](https://everspace-game.com/)]  sigue con su tortuoso camino hacia nuestros sistemas. Ya hemos comentado en [anteriores ocasiones](index.php/component/search/?searchword=everspace&searchphrase=all&Itemid=446) que Rockfish Games estaba teniendo más problemas de los previstos para traernos este magnífico juego a Linux. 


Tras estos meses de desarrollo los desarrolladores al fin han conseguido que el juego se ejecute sin problemas aparentes con gráficas Nvidia, pero sin embargo con gráficas AMD la cosa parece que está mas complicada, de hecho están valorando el lanzar una versión Linux solo con soporte para gráficas Nvidia para los "backers" del juego que los apoyaron en su campaña de financiación y quieren una versión para nuestro sistema favorito, aunque no pongan el icono de soporte de SteamOS. Puedes ver el anuncio [en este enlace](http://steamcommunity.com/app/396750/discussions/0/333656722966793785/?ctp=18#c1471969431587175057).


*'EVERSPACE' es un "shooter espacial" para un jugador desarrollado en Unreal Engine que combina elementos roguelike con gráficos de primera calidad y una historia cautivadora. Emprende un desafiante viaje a través de un universo hermosamente creado, lleno de sorpresas y en constante cambio. Tus habilidades, técnicas y talento para la improvisación serán puestos a prueba continuamente, mientras aprendes más sobre tu existencia a través de encuentros con personajes interesantes que irán aportando sus piezas del rompecabezas a la historia.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/ItPzK4dzVKU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Seguiremos de cerca el desarrollo de este juego para traeros todas las novedades al respecto.

