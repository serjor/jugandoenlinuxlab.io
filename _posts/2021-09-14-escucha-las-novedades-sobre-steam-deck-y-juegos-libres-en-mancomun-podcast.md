---
author: Pato
category: Noticia
date: 2021-09-14 17:58:04
excerpt: "<p>Nuestro compa\xF1ero Leillo repasa las novedades que se cuecen en nuestro\
  \ mundillo videojueguil</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Mancomun/mancomunpodc.webp
joomla_id: 1359
joomla_url: escucha-las-novedades-sobre-steam-deck-y-juegos-libres-en-mancomun-podcast
layout: post
tags:
- podcast
- mancomun-podcast
title: "Escucha las novedades sobre Steam Deck y juegos libres en Mancom\xFAn Podcast"
---
Nuestro compañero Leillo repasa las novedades que se cuecen en nuestro mundillo videojueguil


Desde hace algún tiempo Leillo viene colaborando con [Mancomún podcast](https://www.mancomun.gal/es/), un programa producido desde tierras gallegas, y en el que se tocan temas de diversa índole siempre relacionados con la temática libre, el movimiento FLOSS, Linux etc.


Hoy os traemos el último episodio en el que participó Leillo (para los que no entiendan gallego, esta vez el podcast fue en español), en el que trataron temas muy interesantes y que tiene por título "Steam Deck y novedades de juegos libres, con Leillo". Puedes escucharlo [en este enlace](https://www.mancomun.gal/wp-content/uploads/2021/08/63-Steam-Deck-e-novidades-nos-videoxogos-libres-con-Leillo.mp3).


Escúchalo y cuéntanos que te parece en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

