---
author: leillo1975
category: "An\xE1lisis"
date: 2019-08-27 14:32:21
excerpt: "<p>Repasamos la \xFAltima expansi\xF3n de @SCSSoftware</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2446fed04528c87c6be7708f0052c2c9.webp
joomla_id: 1074
joomla_url: analisis-american-truck-simulator-washington-dlc
layout: post
tags:
- dlc
- american-truck-simulator
- ats
- scs-software
- expansion
- washington
title: "An\xE1lisis: American Truck Simulator - Washington DLC."
---
Repasamos la última expansión de @SCSSoftware

Quien nos iba decir que en un año veríamos dos expansiones para American Truck Simulator. Parece que en SCS Software han puesto el pié en el acelerador y en esta ocasión nos ha traido Washington antes de que comenzase el verano, para que pudiésemos disfrutarla a gusto en temporada estival. Si recordais a finales del año pasado, 2018, nos llegaba **Oregon**, y para finales de este año se espera [Utah](index.php/homepage/generos/simulacion/14-simulacion/1195-utah-sera-la-nueva-expansion-de-american-truck-simulator).


![Washington Road map small copy](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Washington/Washington_Road_map_small_copy.webp)


*El nuevo mapa completa la costa oeste de los Estados Unidos*


 


En primer lugar **nos gustaría pediros disculpas por la tardanza de este análisis**, pero los quehaceres y problemas de la vida diaria, nos han impedido lanzar este artículo con más premura. Volviendo al lio, si recordais, [cuando analizábamos Oregon,](index.php/homepage/analisis/20-analisis/998-analisis-american-truck-simulator-dlc-oregon) podíais ver que los desarrolladores habían cambiado bastante el rumbo de desarrollo, sobre todo en lo tocante al **clima y paisajes representados**, **pasando de las zonas aridas o desérticas a otras mucho más verdes**, con predominancia de bosques de coníferas, montañas, prados.... Obviamente este movimiento obligó a SCS Software a crear nuevos assets que suponemos que llevarían su tiempo. En Washington encontramos una **continuación de este trabajo** y creemos que es quizás por eso que el proceso de creación se ha reducido tanto. Pero no os penseis ni por un momento que por esta razón el trabajo que nos presentan no está a  la altura, ni mucho menos. Como mínimo hay que decir que **desprende tanta calidad como Oregon**, y complementa muy bien lo visto hasta ahora, ofreciendo al jugador un territorio rico que explorar y novedades reseñables.


![ATS Paisaje copy](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Washington/ATS_Paisaje_copy.webp)


*Encontraremos lugares perfectos para disfrutar de unas buenas vistas*


 


Y es por eso que a este estado se le llama el "**Siempre Verde**" (Evergreen), nombre que hace justicia a lo que encontraremos en esta expansión.  Tan pronto como en el estado de Oregon, dejemos atrás a la populosa ciudad de Portland y atravesemos el río Columbia, nos encontraremos en este nuevo terrotorio, entrando por la ciudad de Vancouver (no confundir con su homónima Canadiense). Como antes dijimos, al igual que en Oregon, durante nuestros viajes por este estado surcaremos a través de sus **3800 millas de carreteras,** frondosos **bosques**, atravesaremos multiples **puentes**, subiremos escarpadas **montañas** por retorcidas carreteras y avistaremos hermosos **lagos**, todo en conjunción con la mágnífica belleza del estado más al norte de la costa oeste.


Pero no todo lo que encontraremos en nuestro camino será naturaleza, sinó que también encontraremos **zonas densamente pobladas, como la bahía de Seattle**, donde además de la citada y espectacular ciudad pasaremos también por **Olympia**, su capital, **Tacoma** o **Everett**. Todo esto a través de únicas e **intrincadas autopistas** que se entrecruzan entre si, y que sin el bendito GPS nos costaría un riñón poder dominar. Hablando de Seattle, ciudad conocida por ser la cuna del "[Grunge](https://youtu.be/MyPXE4TWvug)" y por su famosa [Aguja espacial](https://es.wikipedia.org/wiki/Space_Needle), se echa de menos que esta última no se pueda visitar desde más cerca (o al menos yo no he encontrado la manera.) Vale, es poco probable que podamos acceder en la vida real a sus inmediaciones con nuestro vehículo de grandes dimensiones, pero estaría bien que esta expansión se permitiese esa licencia y poder verla en detalle. Hay que reseñar que en esta zona lás zonas portuarias son enormes y reflejan con fidelidad las reales.


![ATS Ciudad copy](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Washington/ATS_Ciudad_copy.webp)


*La recreación de las ciudades, tanto grandes como pequeñas está especialmente cuidada*


 


Al igual que en el caso de Oregon, **el trabajo realizado con los puentes es soberbio**, atravesando un total de 20 reseñables infraestructuras de este tipo, muchisimos de ellos recreados con "assets" exclusivos para la ocasión. Siendo Washington un estado tan pródigo en accidentes geográficos, no es de extrañar que encontremos puentes singulares por su excepcional arquitectura, así como otros más "especiales" como  **levadizos** de todo tipo, o incluso **giratorios** como el que se encuentra al norte, cerca de Everett.


Es de reseñar también el **Keystone-Port Townsend Ferry** que podemos encontrar también al noroeste y que nos permitirá cruzar la bahía de Seattle más facilmente, siendo la primera estructura de este tipo que nos encontramos en American Truck Simulator. También llama mucho la atención el pasar por la enorme e impresionante **presa Grand Coulee**, que se encuntra en el centro del estado interrumpiendo el paso al antes mencionado rio Columbia. Podremos vislumbrar a lo lejos los **montes Rainier** (en el centro) y **Santa Helena** (más al sur), dos volcanes situados en sendos parques naturales, tambien fielmente reflejados.


![ATS Ferry copy](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Washington/ATS_Ferry_copy.webp)


*Podremos coger el Ferry para realizar el trayecto Keystone-Port Townsend* 


 


Como siempre **se han adaptado las empresas y cargas al nuevo territorio**, continuando con las explotaciones madereras, donde además de las cargas existentes en esta DLC podemos adquirir por tan solo 5€ "[Forest Machinery](https://store.steampowered.com/app/1076080/American_Truck_Simulator__Forest_Machinery/)", que nos permitirá llevar todo tipo cargamentos especiales de maquinaria dedicada a la tala de árboles, Pero también SCS ha estado dándole protagonismo a constructores de yates, grandes granjas, almacenes de materiales....  Se han creado también aparcamientos y zonas de descanso para camiones exclusivas de Washington.


A nivel técnico, el juego no se distancia nada de lo visto en anteriores ocasiones, al menos en el aspecto gráfico, lo cual no es malo, pues los gráficos de por si son extremadamente buenos, pero si hay que destacar entre otras muchas mejoras la inclusión de las voces en el GPS, algo que ciertamente se echaba muchísimo de menos y que facilitará la atención a la carretera y aumentará las dosis de realismo del juego. Podremos escuchar las voces en castellano con voces de hombre y mujer, pero también en otros muchos idiomas, y como es lógico en Inglés, si queremos aumentar la inmersión en el juego. Para ser justos esta característica no es exclusiva de "Washington", sinó que pertenece a la [versión 1.35](https://blog.scssoft.com/2019/06/american-truck-simulator-update-135.html) del juego, que fué hecha pública días antes, pero que obviamente se preparó para acompañar a esta expansión.


![ATS Puente copy](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Washington/ATS_Puente_copy.webp)


*Los puentes, al igual que en Oregon, están fielmente recreados, incluso los más singulares*


 


Con esta expansión SCS Software cierra un ciclo, **completando la costa oeste de los Estados Unidos**. Además pone un **punto de inflexión**, ya que como sabeis, su proxima expansión será en Utah, hacía el sur, volviendo de nuevo hacia tierras más áridas. Ciertamente Washington no ofrece una experiencia completamente nueva a American Truck Simulator, pero la completa haciéndola mucho más rica y dándole horas y horas de agradable conducción a todos los fanáticos de este juego. Aunque suena a tópico, en JugandoEnLinux.com os recomendamos sin ningún género de duda que os hagais esta expansión si habeis disfrutado de las anteriores, pues estamos bien seguros que nos os va a defraudar. Además debeis saber que tiene un **precio realmente ajustado, 12€**, que hace de su compra algo obligado para todos los fans.


Podeis comprar American Truck Simulator : Washington DLC en la **[Humble Store](https://www.humblebundle.com/store/american-truck-simulator-washington?partner=jugandoenlinux) (enlace patrocinado)** o en [Steam](https://store.steampowered.com/app/1015160/American_Truck_Simulator__Washington/). Os dejamos con la primera aproximación que tuvimos en su día a este juego (y que continua en el [video2](https://youtu.be/4P2FtzKvQPA) y [video3](https://youtu.be/4gYZlAhI564)):


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/ARlgHwDwi5Y" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Os gustan los juegos de SCS Software? ¿Qué os ha parecido esta DLC? ¿La habeis jugado ya? Cuéntanoslo en los comentarios o deja tus mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

