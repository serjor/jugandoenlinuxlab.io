---
author: leillo1975
category: "Simulaci\xF3n"
date: 2017-07-20 15:02:00
excerpt: "<p>El desarrollador dice que le falta motivaci\xF3n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b069b892c6725bd357423bc8f6c17d01.webp
joomla_id: 414
joomla_url: kart-racing-pro-no-llegara-a-linux-steamos
layout: post
tags:
- cancelacion
- simulador
- soporte
- kart-racing-pro
- karts
- racing
- piboso
title: "KART RACING PRO no llegar\xE1 a Linux/SteamOS"
---
El desarrollador dice que le falta motivación

Cuando se están a punto de cumplir dos años de la aparición en [Steam Greenlight](http://steamcommunity.com/sharedfiles/filedetails/?id=478961813) de **[Kart Racing Pro](http://www.kartracing-pro.com/)** y dias después de la [publicación oficial en la conocida tienda](http://store.steampowered.com/app/415600/Kart_Racing_Pro/) acabamos de recibir la decepcionante y temida noticia de que el juego no va a ser portado finalmente a nuestros sistemas.


 


Al ser preguntado en un [tema de su foro oficial](http://forum.kartracing-pro.com/index.php?topic=1575.0) por ello, el desarrollador ha contestado lo siguiente:


 



> 
> *No progress, unfortunately. There were, and sadly there still are, more important tasks to complete. There isn't much motivation in completing a native Linux / SteamOS version of KRP. Very few requests, and there are reports that it works perfectly fine using Wine.*
> 
> 
> **Traducción**
> 
> 
> *No hay progreso, desafortunadamente. Había, y tristemente todavía hay, tareas más importantes que completar. No hay mucha motivación en completar una versión nativa de KRP para Linux/SteamOS. Pocas peticiones, y hay reportes de que funciona perfectamente bien usando Wine.*
> 
> 
>  
> 
> 
> 


Kart Racing Pro es un juego con varios años de desarrollo a sus espaldas, y fué aprovado por la comunidad hace casi dos. Entre los votos positivos había algunos/muchos (no hay forma de saberlo) que provenían del anuncio del desarrollador **(PiBoSo)** a partir de una [pregunta de un usuario](http://steamcommunity.com/workshop/filedetails/discussion/478961813/535152276584750953/#c535152276591035532) , donde contestaba que **las versiones de Linux y Mac estaban planeadas y serían lanzadas después de la de PC (Windows)**. También había informaciones de este tipo tanto en los [foros de Steam](http://steamcommunity.com/app/415600/discussions/0/350532536102916748/#c350532536103428649) , como en el tema del [foro oficial](http://forum.kartracing-pro.com/index.php?topic=1575.0) del que antes hablamos.


 


Supongo que en este momento algunas personas estarán decepcionadas al ver que han ayudado a llegar a Steam con su voto a un juego que muy probablemente no votarían si no se anunciase que tendría soporte. Al menos es así como me siento yo. Por desgracia no es la primera vez que nos encontramos con situaciones semejantes donde los desarrolladores prometen cosas para conseguir la aprobación o fondos en sitios como Greenlight, Kickstarter, etc; y luego se queda todo en auga de borrajas cuando consiguen su objetivo.


 


Personalmente me parece una pena, porque he podido probar la versión trial del juego y funciona muy bien, siendo la conducción realista y a la vez divertida, tal y como lo es en la realidad. Como amante de los **simuladores de carreras**, veo con tristeza como pasa el tiempo y no tenemos un juego decente con soporte en nuestros equipos, si exceptuamos, claro, el magnífico [DIRT Rally](index.php/homepage/analisis/item/362-analisis-dirt-rally) .


 


Este caso me recuerda lo que pasó en su día con Project Cars, que cuando necesitaba apoyos prometió soporte para Linux/SteamOS y después dió la callada por respuesta. Al menos, en este caso, **el desarrollador ha dado la cara** y ha sido sincero para admitir que no está trabajando en la versión para Linux, lo cual, a pesar de todo, hay que agradecer.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/fQgM1D7BtIo" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


¿Qué os parece esta cancelación? ¿Hubieseis comprado este juego si estuviese disponible? Cuentanoslo en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux)

