---
author: Pato
category: Rol
date: 2017-12-26 14:13:51
excerpt: <p>El juego es obra de los creadores de Bastion o Transistor</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d6f224fb534a83b00aac145bc92cdd0f.webp
joomla_id: 588
joomla_url: el-excelente-pyre-ya-esta-traducido-al-espanol-en-su-ultima-actualizacion
layout: post
tags:
- accion
- indie
- rol
- equipos
title: "El excelente 'Pyre' ya est\xE1 traducido al espa\xF1ol en su \xFAltima actualizaci\xF3\
  n"
---
El juego es obra de los creadores de Bastion o Transistor

Buenas noticias para los amantes de los buenos juegos de rol. El estudio Supergiant Games, responsables de juegos notables como [Bastion](http://store.steampowered.com/app/107100/Bastion/) o [Transistor](http://store.steampowered.com/app/237930/Transistor/) ambos disponibles en Linux nos han traducido el también excelente y galardonado 'Pyre' al español, tal y como nos anunciaron en su [última actualización](http://steamcommunity.com/games/462770/announcements/detail/1587947907187134933) donde además de a nuestro idioma el juego recibe soporte para idioma chino, ruso, francés y alemán.


*"**Pyre** es un RPG por equipos en el que intentarás conducir a la libertad a una banda de exiliados alcanzando la victoria en las ancestrales competiciones que se libran en un vasto y místico purgatorio en el modo campaña o desafía a un amigo a un frenético combate ritual en el modo enfrentamiento.. ¿Quién regresará entre laureles y quién pasará el resto de sus días condenado al exilio?"*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/9jBbq6c9EEQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Características:


* Un nuevo mundo de los creadores de Bastion y Transistor


¡Adéntrate en el mundo más vasto e imaginativo jamás concebido por **Supergiant**! Conoce a un reparto coral de personajes que luchan por recuperar su libertad mientras te abres paso por el místico purgatorio de Downside.
* Un sistema de batallas entre grupos de tres repleto de acción


Enfréntate a un variopinto elenco de adversarios en tensos enfrentamientos de alto riesgo en los que cada victoria (¡o derrota!) acerca a los exiliados un poco más a la iluminación. Elige a tres miembros de tu grupo para cada ritual y equípalos con arcanos talismanes y poderosos talentos o "Masteries".
* Una historia con múltiples sendas que recorrer y sin fin de la partida


En **Pyre**, nunca perderás tu progreso. No importa si prevaleces o fracasas: el viaje continúa. El compromiso de **Supergiant** con la narración interactiva se expresa a través de una historia que cada jugador experimentará de una manera única.
* Desafía a un amigo en el modo enfrentamiento


Además de la campaña para un jugador, **Pyre** ofrece un modo enfrentamiento para dos jugadores en el que podrás desafiar a un amigo (o un rival controlado por IA) a uno de los frenéticos combates rituales del juego. Selecciona tu triunvirato entre más de 20 personajes únicos.
* Una suntuosa presentación que te llevará a otro mundo


Una vez más, **Pyre** valida el galardonado talento del equipo responsable **Bastion** y **Transistor**. Desde las vívidas ilustraciones pintadas a mano hasta la evocadora banda sonora original, cada aspecto de la presentación de **Pyre** te sumerge en su mundo de fantasía.
* Controles intuitivos y personalizables


Juega con mando o ratón y teclado usando controles totalmente personalizables adaptados para PC. El juego ofrece un amplio margen para adaptar el nivel de desafío tanto en la campaña como en el modo enfrentamiento.


 Si te gustan los juegos de Supergiant Games, no puedes dejar escapar este 'Pyre', ahora además con la ventaja de que está traducido al español. Lo tienes disponible en su página de Steam con un 50% de descuento por las rebajas de invierno:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/462770/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

