---
author: Serjor
category: Noticia
date: 2019-08-30 15:46:26
excerpt: "<p>La versi\xF3n 435 pasa a estable a trav\xE9s de la revisi\xF3n 435.21</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f6734d3b41eb0d3fb0f1e952e057ffea.webp
joomla_id: 1105
joomla_url: nuevas-versiones-de-los-drivers-propietarios-de-nvidia-disponibles
layout: post
tags:
- drivers
- nvidia
title: Nuevas versiones de los drivers propietarios de NVIDIA disponibles
---
La versión 435 pasa a estable a través de la revisión 435.21

Vía Phoronix nos [enteramos](https://www.phoronix.com/scan.php?page=news_item&px=NVIDIA-435-Linux-Updates) de que NVIDIA ha liberado nuevos drivers para GNU/Linux. Por un lado tenemos la versión estable 435.21, aunque en la rama *short lived*, por lo que probablemente aquellas distribuciones que buscan estabilidad no la agregarán a sus repositorios, y por otro la versión beta 435.19.02.


Dentro del driver estable nos encontramos con todos los cambios de la versión beta anterior, la 435.17, más la corrección de un bug que hacía que el servidor gráfico X.Org fallara al usar la funcionalidad HardDPMS.


En la nueva rama beta por contra, los cambios respecto a la versión 435.17 son mejoras generales en el rendimiento y soporte de 8 bits a la extensión VK_NV_cooperative_matrix.


Interesante actualización de drivers, ya que se trae a un primer plano todos los cambios acumulados los últimos meses en su driver beta.


Y tú, ¿vas a actualizar tus drivers? Cuéntanos tu experiencia con esta nueva versión en los comentarios o en nuestros canales de [Telegram](https://t.me/jugandoenlinux) y [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)

