---
author: Pato
category: Noticia
date: 2020-05-27 21:57:49
excerpt: "<p>Adem\xE1s, tenemos novedades sobre, Sundered, Jotun, PUBG, The Crew 2\
  \ y Mortal Kombat</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/ThisWeekonStadia-May26.webp
joomla_id: 1223
joomla_url: novedades-stadia-anunciado-the-elder-scrolls-online-y-sube-la-resolucion
layout: post
tags:
- jotun
- stadia
- sundered
- pubg
- thecrew-2
- teso
- mortal-kombat
title: "Novedades Stadia: anunciado The Elder Scrolls Online, y sube la resoluci\xF3\
  n"
---
Además, tenemos novedades sobre, Sundered, Jotun, PUBG, The Crew 2 y Mortal Kombat


Toca repasar las novedades que anunciaron los chicos de Stadia ayer mismo, comenzando por la disponibilidad para compra ya mismo **Jotun: Valhalla Edition** en la tienda de Stadia a un precio de 14,99€, además de **Sundered: Eldritch Edition**, que también tenemos disponible a un precio de 19,99€.


Como novedad, nos anuncian la llegada de **The Elder Scrolls Online a Stadia Pro** para el próximo día 16 de junio, con juego cruzado entre Stadia y PC,  y la novedad de poder elegir un personale de Morrowind como regalo. También podremos conservar el progreso si cambiamos de plataforma.


Por otra parte, llega el modo "ranked" a **PUBG**, de modo que ahora los jugadores pueden elegir jugar con un set de reglas más exigentes y competitivas, y la acción será recompensada. Eso sí, el modo "ranked" solo está disponible para los jugadores que usen el mando, quedando fuera los que jueguen con teclado y ratón.


Siguiendo con las expansiones, Mortal Kombat 11: Aftermath se amplía con nueva historia y novedades tanto para un solo jugador como para multijugador. Se añaden tres nuevos luchadores:Sheeva, Fujin, y Robocop. Mortal Kombat 11: Aftermath se puede comprar en la tienda de Stadia por 39,99€, o junto a Mortal Kombat y el Kombat Pack en el Mortal Kombat: Aftermath Kollection por 59,99€.


Por último, nos anuncian la disponibilidad del add on para The Crew 2: Hobbies que ya está disponible sin coste extra para los poseedores del juego.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/BP_OH0J-nWc" width="560"></iframe></div>


Todas estas novedades las puedes ver en el [post del anuncio](https://community.stadia.com/t5/Stadia-Community-Blog/This-Week-on-Stadia-Myths-monsters-motorsports-and-Mortal-Kombat/ba-p/23244) de Stadia.


 

