---
author: Pato
category: Hardware
date: 2019-03-19 10:35:54
excerpt: "<p>La consola con n\xFAcleo Linux actualiza su hardware y promete mas informaci\xF3\
  n en pr\xF3ximas fechas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2cf1f8d39e0698fbf0c2be15e9fa8cfb.webp
joomla_id: 998
joomla_url: atari-vcs-montara-un-procesador-con-nucleos-zen-y-una-grafica-vega
layout: post
tags:
- hardware
- atari
- atarivcs
title: "Atari VCS montar\xE1 un procesador con n\xFAcleos Zen y una gr\xE1fica Vega"
---
La consola con núcleo Linux actualiza su hardware y promete mas información en próximas fechas

Hace ya algún tiempo que no teníamos noticias sobre la nueva consola de Atari de la que [ya hemos informado](index.php/homepage/hardware/tag/AtariVCS) en otras ocasiones, pues como ya sabéis el sistema que correrá bajo el capó será un núcleo Linux. Pues como ya iba siendo hora, y teniendo en cuenta que la consola estaba prevista para su lanzamiento al gran público para este mismo año Atari ha publicado una actualización en su blog de Medium en el que nos ofrece suculenta información de primera mano.


En primer lugar, nos anuncian que la consola está siendo objeto de revisión a nivel hardware por parte de sus asociados en AMD y ya podemos decir que la actualización no es moco de pavo, pues pone a la nueva consola muy cerca de la potencia de la próxima generación de consolas que vendrá de la mano de Sony y Microsoft.


Hablamos de que la Atari VCS pasa de tener una APU basada en la familia Bristol Ridge de poca potencia y bajo consumo a **una APU con dos núcleos "Zen" de 14 nm y una gráfica Vega**, aunque no especifican qué serie será. Lo que si está claro es que el nuevo "corazon" de esta máquina sertá mucho más competente, entregará mucha mas potencia tanto a nivel de CPU como gráfica y será capaz de mover mejores gráficos y a la vez reducir temperaturas y producir menos ruido por disipación gracias a esta nueva tecnología.


Este cambio de CPU conlleva mejoras como la integración de la red Ethernet, soporte para vídeo 4K nativo con DHCP y soporte completo para contenido DRM para plataformas como Netflix o HBO entre otras.


Por otra parte, prometen ofrecer mas información y actualizaciones sobre hardware, partners, distribución y mucho más en los próximos meses.


Con todos estos cambios ya en marcha, el lanzamiento de la consola se retrasará como es lógico aunque las espectativas de lanzamiento para EEUU están en el final de este mismo año 2019.


Puedes leer toda la información de esta actualización en el [blog de Atari VCS](https://medium.com/@atarivcs/more-power-is-coming-to-the-atari-vcs-20c02f3aefc2) en Medium.


¿Qué te parece el cambio de la APU a núcleos ZEN y gráfica Vega? ¿Piensas que puede triunfar el concepto y tecnología de esta Atari VCS con Hardware AMD y núcleo Linux?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

