---
author: Pato
category: Rol
date: 2018-05-08 16:32:37
excerpt: <p>Obsidian Entertainment nos vuelve a traer un juego de rol de primera magnitud</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9139b86f02108abdcc0129521eca5e85.webp
joomla_id: 726
joomla_url: pillars-of-eternity-ii-deadfire-ya-disponible-en-linux-steamos
layout: post
tags:
- fantasia
- rol
title: '''Pillars of Eternity II: Deadfire'' ya disponible en Linux/SteamOS'
---
Obsidian Entertainment nos vuelve a traer un juego de rol de primera magnitud

Todos los fans del buen rol están de enhorabuena. Acaba de lanzarse de manera oficial 'Pillars of Eternity II: Deadfire' del que ya os hemos hablado en anteriores ocasiones.


Se trata de un juego de rol basado en los títulos de la vieja escuela pero con la jugabilidad adaptada a los nuevos tiempos y un buen puñado de mejoras en múltiples facetas que harán las delicias de todo buen amante del rol con mayúsculas. No en vano el juego levantó con bastante éxito una campaña de [crowfunding](https://www.fig.co/campaigns/deadfire?utm_campaign=CF17&utm_medium=twitter&utm_source=Obsidian)  donde llegaron a recaudar un 400% de lo que pedían.


*Persigue a un Dios renegado por tierra y mar en la continuación del multigalardonado RPG Pillars of Eternity. Lidera tu nave a través de un peligroso viaje descubriendo el vasto archipiélago inexplorado de Deadfire. Haz que el mundo se doblegue a tu voluntad, mientras exploras infinitud de posibilidades, incluyendo un detallado sistema de personalización de personajes, total libertad de exploración y constantes decisiones significativas.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/aB-Pw9FD-v8" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 **Características principales:**


• Sumérgete en una profunda experiencia RPG para un jugador: enriquecido con tecnología y características de vanguardia, Deadfire parte de la base del clásico juego D&D, con gráficos mejorados y mecánicas de juego más profundas, brindándote una aventura enteramente hecha a mano donde tus decisiones verdaderamente importan.  
   
 • Descubre la nueva región de Deadfire: planea el curso de tu navegación y explora las exóticas islas de la región del archipiélago. Descubre nuevos lugares, interactúa con sus habitantes y participa en multitud de misiones en cada puerto.  
   
 • Construye y personaliza tu equipo: elige entre 7 compañeros diferentes para que te acompañen en tu búsqueda y asígnales diferentes clases y complejas habilidades a cada uno. Presencia cómo interactúan y se desarrollan sus relaciones personales con la adición del nuevo sistema de compañía.   
   
 • Lidera a tu nave a través de los mares: sirviéndote de refugio en el mar, tu nave es mucho más que un simple vehículo de exploración. Mejora tu nave y tu tripulación y elige qué habilidades desarrollar para sobrevivir a los peligrosos encuentros que te esperan por el camino.


 


En cuanto a los requisitos, estos son:


**Mínimo:**  

+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** Ubuntu 14.04 LTS 64-bit o superior
+ **Procesador:** Intel Core i3-2100T @ 2.50 GHz / AMD Phenom II X3 B73
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** ATI Radeon HD 4850 o NVIDIA GeForce 9600 GT
+ **Almacenamiento:** 45 GB de espacio disponible


**Recomendado:**  

+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** Ubuntu 14.04 LTS 64-bit o superior
+ **Procesador:** Intel Core i5-2400 @ 3.10 GHz / AMD Phenom II X6 1100T
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** Radeon HD 7700 o NVIDIA GeForce GTX 570
+ **Almacenamiento:** 45 GB de espacio disponible


Además de esto, Obsidian ya [ha anunciado](https://eternity.obsidian.net/news/pillars-of-eternity-ii-deadfire-post-launch-content-announced) las que serán las próximas expansiones del juego y que vendrán a ser:


* "**Beast of Winter**" (DLC) llegará en Julio de 2018 y nos llevará a una isla habitada por una misteriosa tribu.
* "**Seeker, Slayer, Survivor**" (DLC) llegará en Septiembre de 2018 y nos llevará a una isla donde tendremos que poner a prueba nuestras dotes de combate.
* "**The Forgotten Sanctum**" (DLC) llegará en Noviembre de 2018 y pondrá a prueba nuestras alianzas y moralidad al enfrentarnos a los magos de Eora.


Estos tres paquetes estarán disponibles también en un pase de temporada con un precio de 29,99$ que está ya a la venta, o de forma individual por un precio de 9,99$ cuando sean lanzados. Los Backers que compraron la "Collectors Edition" también recibirán los tres DLCs.


Si te gustan los juegos de Rol con mayúsculas, tienes 'Pillars of Eternity II: Deadfire' en [GOG](https://www.gog.com/game/pillars_of_eternity_ii_deadfire), [Obsidian.net](http://eternity.obsidian.net/buy) y Steam, donde está traducido al español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/560130/258525/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>
 



