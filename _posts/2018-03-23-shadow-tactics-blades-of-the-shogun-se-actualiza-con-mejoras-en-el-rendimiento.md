---
author: Serjor
category: Estrategia
date: 2018-03-23 17:14:48
excerpt: "<p>La actualizaci\xF3n se centra sobretodo en los tiempos de carga</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/bedd85fe36f92bf9288d87a48e3bbfff.webp
joomla_id: 691
joomla_url: shadow-tactics-blades-of-the-shogun-se-actualiza-con-mejoras-en-el-rendimiento
layout: post
tags:
- shadow-tactics
- actualizacion
title: 'Shadow Tactics: Blades of the Shogun se actualiza con mejoras en el rendimiento'
---
La actualización se centra sobretodo en los tiempos de carga

Normalmente no soy de escribir este tipo de noticias, pero las cosas como son, este juego me pierde.


El caso es que Shadow Tactics se actualiza a la versión 2.2, o lo que es lo mismo, según el anuncio que podéis leer [aquí](http://steamcommunity.com/app/418240/), importantes mejoras en rendimiento, sobretodo en lo que tiempos de carga se refiere. Eso sí, ellos mismos indican en el anuncio que no hay nuevo contenido para el juego de camino.


Aún no he podido probar el parche (acabo de ver que el juego se estaba actualizando y al ver los cambios he ido de cabeza a la redacción del artículo), pero sí que es cierto que aún con un SSD los tiempos de carga eran un poco demasiado, sobretodo porque aunque el juego luce muy bien, no es que sea un AAA de los gráficos.


En fin, ahora sí que no tengo excusa para terminarme el juego y escribir un análisis del que por ahora está en mi Top 5 de juegos, por encima de otras grandes joyas, pero el porqué os lo explicaré en el análisis ;-)


Os dejo con un gameplay que hicimos del juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/VYHupWaSLJU" width="560"></iframe></div>


Y tú, ¿has podido probar ya el parche? Cuéntanoslo en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

