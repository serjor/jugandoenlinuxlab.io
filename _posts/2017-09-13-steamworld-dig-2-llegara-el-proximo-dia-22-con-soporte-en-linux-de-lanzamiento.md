---
author: Pato
category: Aventuras
date: 2017-09-13 17:53:24
excerpt: <p>La fecha de lanzamiento ha sido anunciada "por sorpresa"</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d29f724a45ead59fbed342a9b3ad72f4.webp
joomla_id: 461
joomla_url: steamworld-dig-2-llegara-el-proximo-dia-22-con-soporte-en-linux-de-lanzamiento
layout: post
tags:
- accion
- indie
- aventura
title: "'SteamWorld Dig 2' llegar\xE1 el pr\xF3ximo d\xEDa 22 con soporte en Linux\
  \ de lanzamiento"
---
La fecha de lanzamiento ha sido anunciada "por sorpresa"

Se nota que estamos en plenas fechas de lanzamientos de cara a la próxima campaña navideña. Según han anunciado los chicos del estudio responsable del '[SteamWorld Dig](http://store.steampowered.com/app/252410/SteamWorld_Dig/)' original y su secuela '[SteamWorld Heist](http://store.steampowered.com/app/322190/SteamWorld_Heist/)', (ambos disponibles en Linux) Image&Form su nuevo juego 'SteamWorld Dig 2' [[web oficial](http://imageform.se/game/steamworld-dig-2/)] será lanzado el próximo día 22 de este mismo mes de Septiembre y nos llegará con soporte en Linux desde su mismo lanzamiento, y además traducido al español.


Así se recoge en el [anuncio](http://steamcommunity.com/games/571310/announcements/detail/1464090033924123385) que han publicado en Steam y que nos emplaza a dicha fecha para hacernos con este nuevo título de esta notable saga.


Sinopsis de Steam



> 
> *¡Coge tu pico y perfora en la secuela del premiado SteamWorld Dig! Un juego de plataforma y aventuras mineras forjado en las llamas de Metroidvania.*
> 
> 
> *Un steambot solitario y su extraño acompañante deben excavar hasta lo más profundo, hacerse con muchas riquezas y explorar un mundo subterráneo lleno de peligros, todo para encontrar a su amigo perdido. Aunque les queda poco tiempo...*
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/eoBtT4djX-k" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Si quieres ver más detalles del juego puedes verlo en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/571310/" width="646"></iframe></div>


 

