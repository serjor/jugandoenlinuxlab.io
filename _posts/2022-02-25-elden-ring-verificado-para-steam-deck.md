---
author: Pato
category: Rol
date: 2022-02-25 13:32:44
excerpt: "<p>Adem\xE1s, hay movimientos en los repositorios de Apex Legends que apuntan\
  \ a un posible soporte en Steam Deck</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/EldenRing/elden-ring-new-header_900.webp
joomla_id: 1437
joomla_url: elden-ring-verificado-para-steam-deck
layout: post
tags:
- accion
- rol
- steam-deck
- elden-ring
title: Elden Ring Verificado para Steam Deck
---
Además, hay movimientos en los repositorios de Apex Legends que apuntan a un posible soporte en Steam Deck


A pocas horas ya del lanzamiento de **Steam Deck** (al menos del final del embargo de las reviews y la llegada masiva de correos a los que la han reservado) se van desvelando algunos títulos triple A que van apareciendo como verificados para la máquina de Valve.


Es el caso del último bombazo de From Software, **Elden Ring** que desde anoche está disponible para su compra, y que **ya aparece como "verificado" para Steam Deck**. De forma que cuando los primeros afortunados reciban su portátil podrán jugar a este hit. Puedes ver todos los juegos verificados en la web de SteamDB [en este enlace](https://steamdb.info/instantsearch/?query=elden%20ring&refinementList%5Boslist%5D%5B0%5D=Steam%20Deck%20Verified).


![Eldenringverified](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/Eldenringverified.webp)


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/Y3FJ2U8AGWI" title="YouTube video player" width="560"></iframe></div>


 


También hay que decir que desde el lanzamiento el juego **está recibiendo críticas variadas** debido sobre todo a lo que parece una falta de optimización con caídas de frames y microstutering, si bien se espera que From Software solucione estos problemas con próximas actualizaciones.


Si quieres adquirir Elden Ring puedes visitar [su página en Steam](https://store.steampowered.com/app/1245620/ELDEN_RING/).


Por otra parte, desde hace días estamos tras la pista de **Apex Legends**, pues [en sus repositorios](https://steamdb.info/app/1172470/history/) están apareciendo multitud de referencias a un posible soporte para Steam Deck, así como una implementación de Easy Anti Cheat del juego para Linux desde hace unas horas. Este sería otro de los títulos sorpresa que hoy recibiríamos los usuarios de GNU-Linux y los futuros poseedores de una Steam Deck.


Seguiremos pendientes en las próximas horas para informar de los posibles títulos que puedan aparecer de cara al lanzamiento de la máquina de Valve.

