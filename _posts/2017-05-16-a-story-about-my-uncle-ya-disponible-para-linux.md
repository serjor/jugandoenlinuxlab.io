---
author: Serjor
category: Plataformas
date: 2017-05-16 18:02:23
excerpt: "<p>Fantas\xEDa y habilidad se juntan en una b\xFAsqueda por lo desconocido</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/319a6b58175c8348e5b537a311344d30.webp
joomla_id: 323
joomla_url: a-story-about-my-uncle-ya-disponible-para-linux
layout: post
tags:
- plataformas
- fps
- a-story-about-my-uncle
title: A Story About My Uncle ya disponible para Linux
---
Fantasía y habilidad se juntan en una búsqueda por lo desconocido

En "A story about my uncle" encarnaremos a un joven que busca a su tío desaparecido, para lo cuál tendrá que recorrer un mundo de fantasía que jamás había imaginado, y donde para poder avanzar hay que hacer uso de extraños artilugios que su tío construyo.


Es un plataformas en primera persona que lleva algún tiempo en windows, pero desde el pasado día 12 de mayo lo tenemos disponible para Linux y Mac.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/cToIhIIlWnE" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/278360/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


Y tú, ¿descubrirás que le pasó al desaparecido tío? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

