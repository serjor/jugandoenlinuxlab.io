---
author: Serjor
category: "Acci\xF3n"
date: 2017-12-22 18:44:56
excerpt: "<p>El estudio acaba de lanzar una nueva versi\xF3n del juego para testear\
  \ con soporte para la API Vulkan</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3e8e1949f1c3300c7f060866b463e01c.webp
joomla_id: 587
joomla_url: apocryph-un-old-school-fps-tiene-version-pre-alpha
layout: post
tags:
- beta
- fps
title: "Apocryph, un old-school FPS tiene versi\xF3n pre-alpha (actualizaci\xF3n)"
---
El estudio acaba de lanzar una nueva versión del juego para testear con soporte para la API Vulkan

**(Actualización 26/12/2017)** - Para todos los que estéis probando y reportando los bugs y errores de Apocryph, los responsables del juego han lanzado una nueva versión pre-alfa para GNU/Linux **con soporte para Vulkan**. Puedes ver el post con el enlace a la descarga de esta versión [en este enlace](http://steamcommunity.com/app/596240/discussions/0/2425614539585202334/?tscn=1514298987#c1620599015885957062).




---


**(Artículo original)**


Leemos en [gamingonlinux.com](https://www.gamingonlinux.com/articles/gory-fps-apocryph-now-has-a-linux-test-build-to-try-pretty-damn-good.10925) que Apocryph, un FPS de la vieja escuela, de estos que nos recuerdan a los buentos tiempos de Doom o Heretic, ha sacado una pre-alpha con soporte para GNU/Linux.


De la página de [Steam](http://store.steampowered.com/app/596240/Apocryph_an_oldschool_shooter/):



> 
> Apocryph es un juego de disparos en primera persona en un brutal mundo de fantasía oscura. Toma sus raíces en los juegos de disparos de fantasía de la vieja escuela, así que prepárate para una intensa acción FPS de espada y hechicería en medio de castillos olvidados, santuarios malvados y mazmorras decrépitas.
> 
> 
> El mundo de Apocryph es un lugar sombrío y violento que alberga magia, rituales oscuros y peligros en cada esquina. Dondequiera que vaya, se enfrentará a monstruos implacables, trampas y pasadizos bloqueados, y solo podrá depender de usted y de su equipo para sobrevivir a las pruebas.
> 
> 
> * Niveles estáticos, cuidadosamente hechos a mano y centros de nivel, todos repletos de secretos, monstruos y trampas.
> * Tu personaje puede manejar hasta 9 armas diferentes con modos de disparo alternativos, incluidos armamentos cuerpo a cuerpo, hechizos, bastones y artefactos.
> * Los objetos pueden ayudarte en situaciones difíciles, las pociones de salud ayudarán a tu salud, los frascos de maná pueden restaurar tu maná, los objetos de combate pueden dañar a un oponente.
> * También hay reliquias más raras y poderosas que se pueden encontrar, como la temible Obliterator Mask, que te envuelve en un traje de armadura casi impenetrable, te permite golpear enemigos con tus simples puños y enviar poderosas ondas a través de las paredes y el piso , o la Esencia de la Segadora, que te convierte en un avatar de la Muerte misma, aumentando tu velocidad, otorgándote alas para realizar dobles saltos y una guadaña que rompe y destruye a cualquier oponente.
> * Algunas armas y objetos se pueden usar para aturdir a los enemigos, congelarlos, dañar el área de efecto o incluso proporcionar medidas de defensa.
> * El juego usa un sistema gore sofisticado. Un poderoso golpe final puede arrancar una parte del enemigo directamente de él, mientras que críticamente sobre dañar a un oponente literalmente los hace volar en pedazos, con las extremidades volando a través de la habitación y pegándose a las paredes.
> 
> 
> Usted controla al Inquisidor, una vez una persona de alto rango en la jerarquía religiosa de Xilai. Los Xilai eran conocidos por muchas cosas, como su arte, artes mágicas y unidad. Pero todos los mundos les temían por una razón diferente: los Xilai adoraban a la Muerte misma. Durante siglos asaltaron innumerables mundos, cosechando almas en nombre de su temida Señora. Los eventos que precedieron al juego te llevaron al exilio, y ahora, después de muchos años, regresas, solo para encontrar fortalezas y tierras invadidas por demoníacos engendros y viles bestias. ¿Por qué estabas exiliado? ¿Qué podría causar esta desolación? Es hora de encontrar algunas respuestas ... y cosechar algún engendro demoníaco en nombre de Pale Mistress.
> 
> 
> Apocryph se encuentra actualmente en desarrollo temprano. Todavía hay muchos elementos, biomas y salas para agregar al juego, así como modos de juego, avances de la trama y ventajas. Síguenos en las redes sociales.
> 
> 
> 


La desarrolladora está buscando gente que les ayude a probar el juego, así que si estás interesado, puedes encontrar el enlace a la versión para GNU/Linux en [este](http://steamcommunity.com/app/596240/discussions/0/2425614539585202334/?tscn=1513851002#c1620599015864484959) comentario de los foros de Steam.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Vtz90mQ5ueA" style="display: block; margin-left: auto; margin-right: auto; border: 0px;" width="560"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/596240/" style="display: block; margin-left: auto; margin-right: auto; border: 0px;" width="646"></iframe></div>


Y tú, ¿eres de la época dorada de los FPS? Cuéntanos qué te parece este Apocryph en los comentarios o en nuestros grupos de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

