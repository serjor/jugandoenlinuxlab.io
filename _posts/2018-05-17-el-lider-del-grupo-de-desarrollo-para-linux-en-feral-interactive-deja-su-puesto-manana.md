---
author: Pato
category: Noticia
date: 2018-05-17 17:47:35
excerpt: "<p>Marc Di Luzio lo comunic\xF3 en su c\xEDrculo \xEDntimo hace unos d\xED\
  as. El grupo seg\xFAn sus palabras \"queda en buenas manos\"</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/feb7e9c589993a5a644987614f09b605.webp
joomla_id: 737
joomla_url: el-lider-del-grupo-de-desarrollo-para-linux-en-feral-interactive-deja-su-puesto-manana
layout: post
title: "El lider del grupo de desarrollo para Linux en Feral Interactive deja su puesto\
  \ ma\xF1ana"
---
Marc Di Luzio lo comunicó en su círculo íntimo hace unos días. El grupo según sus palabras "queda en buenas manos"

Sorpresa mayúscula la que nos llevamos hace unos minutos. Marc Di Luzio, el **lider del equipo de desarrollo para Linux dentro de Feral Interactive** dejará su puesto de trabajo dentro de la compañía mañana mismo. Así lo ha comunicado el mismo vía twitter:



 *"Mañana será mi último día en Feral. He tenido una experiencia asombrosa: 5 años hasta ahora, la mayoría como el lider de Linux, ayudando a lanzar mas de 20 Títulos AAA, y haciendo tantos buenos amigos por el camino [...]"*


Sin embargo, tal y como el mismo se apresura a aclarar: *"antes de que nadie entre en pánico, el equipo de desarrollo en Linux queda en muy buenas manos."*


Hace unos días al parecer ya lo comunicó a su círculo íntimo, y han surgido rumores de que su salida se debe a ciertas diferencias con la dirección, aunque no hay nada confirmado en ese sentido.


Marc nos deja con una extensa carrera en la que nos ha traído entre otros, títulos como **los dos Tomb Raider's, los F1 2015 y 2017, XCOM, Mad Max, Hitman, Deus Ex: Mankind Divided, Grid Autosport, Company of Heroes 2** y muchos otros títulos importantes de los que gracias a Feral disfrutamos hoy día y sin los que el juego en Linux no sería lo mismo.


Además, bajo su mando Feral nos ha traído el "[Game Mode](index.php/homepage/generos/software/item/830-feral-lanza-gamemode-una-utilidad-open-source-para-mejorar-el-rendimiento-en-los-juegos)" de reciente aparición y que ha ayudado a ciertos títulos a exprimir más si cabe nuestro hardware.


Marc no ha aclarado aún si tras abandonar Feral se involucrará en algún otro proyecto relacionado con el juego en Linux, y aunque no sería descartable dada su amplia experiencia en el sector, habrá que seguirle la pista. Lo único que sí ha dicho es que se va a "diferentes terrenos, pero no muy lejos del nido, y siempre le gusta hablar de Linux".


En cuanto a Feral, esperemos que este cambio en la "cupula" del desarrollo para Linux signifique una mayor apuesta si cabe por nuestro sistema favorito.


Desde Jugando en Linux le deseamos la mejor de las suertes en su nueva andadura allá donde vaya, agradeciéndole la dedicación y entrega que ha demostrado en su carrera profesional.


¡Gracias Marc!


Fuente: [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=MDL-Departs-Feral)

