---
author: Pato
category: "Simulaci\xF3n"
date: 2018-02-13 12:24:43
excerpt: "<p>Ya est\xE1 disponible el juego de Milkstone Studios, creadores de entre\
  \ otros de Pharaonic o la saga White Noise</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6d4f9be9bbf8e6f07458f60414b62a53.webp
joomla_id: 642
joomla_url: farm-together-llegara-a-linux-steamos-en-acceso-anticipado-el-proximo-22-de-febrero
layout: post
tags:
- indie
- simulador
- granjas
title: "'Farm Together' llegar\xE1 a Linux/SteamOS en acceso anticipado el pr\xF3\
  ximo 22 de Febrero (actualizaci\xF3n)"
---
Ya está disponible el juego de Milkstone Studios, creadores de entre otros de Pharaonic o la saga White Noise

**(Actualización 22/02/18)**


Como estaba previsto, ya tenemos disponible este simpático juego de gestión de granjas donde podremos divertirnos cultivando todo tipo de cosas, ya sea en solitario o en compañía de otros compañeros, pues tiene modo de juego cooperativo. Recordar que aún está en acceso anticipado por lo que puede ser que algunas cosas aún estén por pulir y algunos contenidos aún no estén implementados en el juego.


Si te llama la atención 'Farm Together', tienes el enlace a la tienda al final del artículo.




---


**(Artículo original)**


Si no tienes bastante con los últimos juegos de gestión agrícola que están llegando a nuestro sistema favorito, o buscas un punto de vista mas desenfadado ahora podrás ponerte manos a la obra con un nuevo título de corte mas "casual" de la mano de Milkstone Studios: 'Farm Together'


*¡De la mano los creadores de Avatar Farm llega **Farm Together**, la experiencia granjística definitiva!*  
  
*¡Empieza desde cero, con un pequeño huerto, y termina con una impresionante granja que se extiende más alla de donde alcanza la vista!*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/hmR0G77eyMw" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Este título recuerda de algún modo a Stardew Valley también disponible en Linux, aunque el enfoque desde luego es totalmente distinto pues este se centra mas en la gestión y mantenimiento de la granja mientras que Stardew es mucho más amplio en su planteamiento.


Milkstone Studios es un veterano ya en cuanto a soporte en Linux, ya que son los creadores de otros conocidos títulos como [Pharaonic](http://store.steampowered.com/app/386080/Pharaonic/), [Little Racers: Street](http://store.steampowered.com/app/262690/Little_Racers_STREET/), [Ziggurat](http://store.steampowered.com/app/308420/Ziggurat/) o la saga [White Noise](http://store.steampowered.com/app/503350/White_Noise_2/) todos ellos con soporte en nuestro sistema favorito.


En cuanto a 'Farm Together', a preguntas que les hemos hecho en Twitter el estudio afirma el soporte en Linux y además de lanzamiento:



> 
> That's the idea, yes :)
> 
> 
> — Milkstone Studios (@milkstone) [February 13, 2018](https://twitter.com/milkstone/status/963381135836409856?ref_src=twsrc%5Etfw)



Puedes ver toda la información en su página de Steam y donde estará disponible en acceso anticipado el próximo 22 de Febrero:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/673950/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 Estaremos atentos a las novedades sobre 'Farm together' y trataremos de traeros incluso una review, aquí en jugandoenlinux.com.


¿Qué te parece 'Farm Together'? ¿Te gustan los juegos de gestión agrícola?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

