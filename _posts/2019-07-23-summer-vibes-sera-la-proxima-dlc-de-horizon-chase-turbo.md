---
author: leillo1975
category: Carreras
date: 2019-07-23 07:46:32
excerpt: "<p><span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\"\
  >@AquirisGS</span> acaba de lanzar esta expansi\xF3n.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a4d75127215f52c18ccd5844527298c4.webp
joomla_id: 1080
joomla_url: summer-vibes-sera-la-proxima-dlc-de-horizon-chase-turbo
layout: post
tags:
- carreras
- dlc
- retro
- horizon-chase-turbo
- aquiris
title: "\"Summer Vibes\" ser\xE1 la pr\xF3xima DLC de Horizon Chase Turbo (ACTUALIZADO)"
---
@AquirisGS acaba de lanzar esta expansión.

**ACTUALIZACIÓN 17-9-2019:**


Finalmente , y casi ya finalizado el verano, ha llegado esta DLC para el fantástico Horizon Chase Turbo. Desde hoy mismo podremos disfrutar en nuestros PC's de este interesante añadido para este juego. El anuncio nos llegaba a través de Twitter a traves de la cuenta de Aquiris:



> 
> This summer doesn’t need to end. Make it last forever.  
> Summer Vibes, [#HorizonChaseTurbo](https://twitter.com/hashtag/HorizonChaseTurbo?src=hash&ref_src=twsrc%5Etfw) first DLC is now available on [#XboxOne](https://twitter.com/hashtag/XboxOne?src=hash&ref_src=twsrc%5Etfw), [#NintendoSwitch](https://twitter.com/hashtag/NintendoSwitch?src=hash&ref_src=twsrc%5Etfw), [#Steam](https://twitter.com/hashtag/Steam?src=hash&ref_src=twsrc%5Etfw) and also in [#PS4](https://twitter.com/hashtag/PS4?src=hash&ref_src=twsrc%5Etfw).   
> -Drive the most classic arcade roadster through sunny roads.  
> -5 exclusive unlockable skins  
> -12 remixed tracks [pic.twitter.com/EkdMDisXP0](https://t.co/EkdMDisXP0)
> 
> 
> — Horizon Chase Turbo (@Horizon_Chase) [September 17, 2019](https://twitter.com/Horizon_Chase/status/1174028935350358016?ref_src=twsrc%5Etfw)



 En JugandoEnLinux **intentaremos ofreceros un Stream exclusivo proximamente** para que podais ver en directo las novedades que ofrece esta DLC, por lo que os encomendamos a que permanezcais atentos a nuestras cuentas de redes sociales ([Twitter](https://twitter.com/JugandoenLinux) y [Mastodon](https://mastodon.social/@jugandoenlinux)), así como en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


**Podeis adquirir Horizon Chase Turbo: Summer Vibes DLC en [Steam](https://store.steampowered.com/app/1137940/Horizon_Chase_Turbo__Summer_Vibes/) a tan solo 1.99€.**


 




---


**NOTICIA ORIGINAL:**  
  
En este verano tan caluroso siempre vienen bien este tipo de anuncios, y es que como sabeis, Horizon Chase Turbo, **uno de los títulos más destacables del año pasado** , y que aquí en JeL [analizamos](index.php/homepage/analisis/20-analisis/888-analisis-horizon-chase-turbo) destacando su frescura; llega de nuevo a la actualidad en forma de DLC. El título será **"Summer Vibes"** y estará proximamente con nosotros, aunque aun no se ha concretado la fecha, pero sabiendo que el juego ya está disponible para PS4, se supone que será más pronto que tarde.


Para quien no lo conozca, Horizon Chase Turbo es un arcade de  carreras con inspiración Retro que recuerda en gran medida a clásicos como Out Run, la mítica recreativa de SEGA. Decir esto es altamente apropiado, pues casualmente esta expansión viene, aun más, a aumentar ese parecido, estableciendo un claro homenaje a dicho juego. Las caracterísiticas que podremos encontrar en este "Summer Vibes" serán las siguientes:


* 1 descapotable exclusivo, homenaje a los clásicos, llamado Breeze
* 5 apariencias únicas y desbloqueables, que nos evocarán a conocidos personajes del cine, la televisión o los videojuegos.
* 12 pistas rediseñadas basadas en la campaña principal, que irán de "Costa a Costa", como en un "conocido" juego
* Todas las pistas tienen una tabla de clasificación (¡lógico!).


Por supuesto cuando sepamos algo sobre su lanzamiento os informaremos cumplidamente. También intentaremos, si es posible, ofreceros nuestras impresiones sobre esta expansión con un artículo y/o Stream especial en nuestros canales de [Youtube](https://www.youtube.com/c/jugandoenlinuxcom) y [Twitch](https://www.twitch.tv/jugandoenlinux). Os dejamos con el trailer de "Summer Vibes" para que se os haga la boca agua como a nosotros:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/pzjINP45vbE" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Hebeis jugado a Horizon Chase Turbo? ¿Qué os parece el juego?, ¿y esta expansión? Cuéntanoslo en los comentarios o deja tus mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

