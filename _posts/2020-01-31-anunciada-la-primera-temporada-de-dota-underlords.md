---
author: Pato
category: Estrategia
date: 2020-01-31 11:26:19
excerpt: "<p>El juego \"Battle Chess\" de Valve saldr\xE1 pronto de acceso anticipado</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DotaUnderlords/DOTAeno.webp
joomla_id: 1158
joomla_url: anunciada-la-primera-temporada-de-dota-underlords
layout: post
tags:
- rts
- valve
- dota-underlords
title: Anunciada la primera temporada de DOTA Underlords
---
El juego "Battle Chess" de Valve saldrá pronto de acceso anticipado


Todo lo que se mueve en torno al popular juego de Valve DOTA (Defense Of The Ancient) es noticia, y esta vez hablamos de [DOTA Underlords]({{ "/tags/dota-underlords" | absolute_url }}), el "Battle Chess" que se hizo popular con la explosión de ese tipo de juegos.


En su [último comunicado](https://steamcommunity.com/games/underlords/announcements/detail/1701733568674145732), los chicos de Valve han anunciado que **están trabajando para lanzar la que será la primera temporada del juego**. En concreto, *el equipo de DOTA está enfocado en añadir características, crear contenido y pulir la interfaz de usuario. Con la temporada 1, Underlords introducirá nuevo contenido con «City Crawl», nuevas recompensas con un pase de batalla completo y una nueva forma de juego con rotación de héroes y objetos.*


**DOTA Underlords saldrá de acceso anticipado el próximo día 25 de Febrero**, cuando se lanzará esta primera temporada. Además, han aprovechado para anunciar la llegada de un nuevo personaje llamado Enno, un "ratero" que lanzará dardos y veneno por todas partes, robará objetos a los enemigos o saldrá huyendo si se ve en apuros, todo dependiendo del humor en el que se encuentre. 


Si quieres saber más sobre Enno puede visitar [su página web](https://underlords.com/enno) de presentación.


Tienes todos los detalles de actualización de DOTA Underlords en el [blog del juego](https://steamcommunity.com/app/1046930/allnews/).


¿Juegas a DOTA Underlords? ¿Vas a probar las nuevas posibilidades de Enno?


Cuéntanoslo en nuestros canales de [Telegram](https://t.me/jugandoenlinux) y [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)


 

