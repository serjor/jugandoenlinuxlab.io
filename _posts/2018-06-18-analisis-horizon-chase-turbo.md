---
author: leillo1975
category: "An\xE1lisis"
date: 2018-06-18 13:16:46
excerpt: "<p>Repasamos en profundidad lo \xFAltimo de @AquirisGS</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/423c23827096d15f6a3f90fd1a691b32.webp
joomla_id: 766
joomla_url: analisis-horizon-chase-turbo
layout: post
tags:
- analisis
- retro
- horizon-chase-turbo
- aquiris
title: "An\xE1lisis: Horizon Chase Turbo"
---
Repasamos en profundidad lo último de @AquirisGS

Cada vez son más los juegos que tiran del **efecto nostalgia**, y sin duda entre ellos estaban los juegos clásicos de carreras, títulos que debían encontrarse en cualquier salón recreativo que se preciara o en una buena colección de juegos para ordenador o consola. Entre estos juegos el máximo exponente era el imprescindible **Out Run**, y es ahí de donde mama este Horizon Chase Turbo de [Aquiris Game Studio](https://www.aquiris.com.br/).


Para quien no conozca la compañía, se trata de una **desarrolladora Brasileña** conocida por su fantástico [Ballistic Overkill](https://www.humblebundle.com/store/ballistic-overkill?partner=jugandoenlinux), un shooter en primera persona con una jugabilidad y velocidad endiabladas que llegó a nuestros PC Linuxeros con bastante aceptación. Ahora, y tras su paso por las plataformas móviles con el nombre de "Horizon Chase" (sin el turbo, a secas), han decidido llevarlo un paso más adelante y portarlo a las grandes plataformas, empezando por los PC's y PS4, dejando para más adelante XBOX One y Switch.


Si quereis una descripción corta del juego se puede decir que se trata un **juego de coches con gráficos sencillos y coloristas**, y con una jugabilidad anclada en los ochenta-noventa. Pero decir tan solo eso sería tremendamente injusto, por que el juego presenta muchos elementos, que aunque no originales, si los hacen un juego distinto y particular.


Si empezamos hablando de su jugabilidad, obviamente sus influencias son claras y como mencionamos antes no oculta su amor por Out Run, pues su forma de conducirlo es exactamente igual, aunque con algunas ligeras diferencias, como la **posibilidad de usar "Nitros"**, o el tener que **reponer gasolina para evitar quedarnos tirados en la carretera**. A cambio no tendremos que cambiar de marcha, y este proceso se hará de forma automática. Como guiño a Mario Kart podemos realizar una buena salida si empezamos a acelerar en el momento justo, o por lo contrario calársenos el coche y perder unos segundos preciosos.


Pero no todo esta basado/copiado de Out Run, pues en este caso no nos encontramos con un modo arcade en el que recorreremos todos los escenarios de un tirón (algo que hemos echado en falta), si no que nos encontraremos con circuitos en los que deberemos dar varias vueltas y tratar de adelantar a todos los coches antes de llegar a la meta. Esa es sin duda la diferencia más notable que vamos a encontrar con repecto al clásico. Además dispondremos de modos de juego entre los que destaca sin duda **"Vuelta al Mundo"**, donde recorreremos nuestro planeta en multitud de  **diferentes localizaciones** (California, Brasil, Hawai, Japón, Chile, Rusia...)


![HCT Mundo](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HorizonChaseTurbo/Analisis/HCT_Mundo.webp)


*La cantidad de localizaciones que ofrece Horizon Chase Turbo es abrumadora*


En cada una de estas recorreremos varios circuitos en diferentes lugares, otorgándonos más puntos obviamente si llegamos primero y recogemos "monedas" y gasolina. Si conseguimos completar todos los circuitos en primer lugar desbloquearemos un coche. También en cada una de las localizaciones encontraremos una **carrera bonus** que si la ganamos nos permitirá escoger una mejora para todos nuestros vehículos.


También podremos desbloquear automóviles si acumulamos dinero (puntos) con nuestro progreso en las carreras. En cuanto a los diferentes coches hay que decir que el número de estos es sencillamente brutal (30), y **encontraremos claros guiños** (aunque sin licencia) **a coches reales y de peliculas y series de ficción** (el coche de Mad Max o Delorean, por ejemplo). Cada uno de estos tendrá sus propias características, teniendo más o menos potenciados elementos como la Velocidad Máxima, Aceleración, Manejo, Gasolina o Nitro. Esto será especialmente últil según el tipo de circuito que abordemos o nuestras preferencias personales.


El juego **es muy rápido**, dando una sensación de velocidad muy elevada, poniendo a prueba nuestros reflejos constantemente, aunque no llega a ser tan difícil como los juegos antíguos, donde un error nos hacía perder mucho tiempo y nos abocaba directamente al fracaso. Otro punto a su favor es el poder comparar nuestra condución con un coche fantasma que reflejará nuestra última vuelta en el circuito en el que estemos compitiendo.


Los **gráficos** no rebosan un detalle espectacular, pero **destacan por su sencillez y colorido**. Que conste, que esto no quiere decir que sean malos ni mucho menos, ya que se trata de una estética buscada con una complicidad a lo retro pero sin querer alejarse demasiado de los estándares. Supongo que parte de culpa lo puede tener que el juego fuese desarrollado en primer lugar pensando en las limitaciones de los dispositvos móviles, aunque a mi juicio, **esa estética le da caracter propio**, garantizándole una seña distintiva con respecto a otros juegos de carreras. Personalmente puedo decir que el aspecto gráfico (dirección artística), junto con su jugabilidad son los dos aspectos que más me agradan del juego. También hay que destacar que el título tiene algunos efectos como niebla, lluvia, nieve, amaneceres y puestas de sol que dan más variedad a lo puesto en escena.


 ![HCT Circuito](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HorizonChaseTurbo/Analisis/HCT_Circuito.webp)


 *Junto con su jugabilidad, el diseño artístico del juego es una de sus mejores bazas*


Algo a tener muy en cuenta es que el juego **funciona de manera muy fluida**, incluso activando los todos los efectos y calidades no tiene problemas en mostrarnos una **buena tasa de frames**, algo que en un juego de velocidad hay que tener en cuenta, aunque también es cierto que al tratarse de un juego con unos gráficos tan sencillos es algo normal, pese a deshonrosas excepciones.


Es en el **apartado musical y sonoro** donde más se puede decir que la influencia es totalmente clásica, escuchando temas que nos llevan claramente al pasado (años 80), y que están basados en temas del antes mencionado Out Run. En los efectos de motor, frenadas, derrapes, etc también tenemos que decir lo mismo.


El juego, aunque puede pecar de **un poco repetitivo en su mecánica**, tiene una **alta rejugabilidad**, que nos hará querer ser los primeros en todos los circuitos del juego (más de 100) para desbloquear todos los coches y mejoras. También el descubrir nuevas localizaciones y como están plasmadas en la pantalla puede llevarnos a jugar y jugar hasta completarlas todas. Otro elemento que alargará la vida del juego es la existencia del modo de torneos y de resistencia.


![HCT Fantasma](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HorizonChaseTurbo/Analisis/HCT_Fantasma.webp)


*Podremos tener referencia de nuestra conducción gracias al coche fantasma*


El **modo Torneos** nos permite jugar cuatro carreras seguidas en tres niveles de difucultad difetentes. Si conseguimos ganar el torneo en un nivel de dificultad desbloqueará un coche especial y la posibilidad de correr el siguiente torneo con una dificultad más alta. Este modo es perfecto para cuando tenemos un ratito y no nos queremos complicar mucho la vida.


En el caso del **modo Resistencia**, solo podremos acceder a él cuando completamos la Vuelta al Mundo o si desbloqueamos todos los Torneos del juego en el modo de dificultad más alto. En él, deberemos llegar como mínimo de quintos para no quedar eliminados. En el primer nivel correremos una docena de carreras del tirón, en el segundo se correrían 36 carreras y en el modo más difícil todos los circuitos del modo “Vuelta al mundo”.... mejor no pensar cuanto tiempo puede llevar conseguir este logro, si es que alguno tuvo el suficiente aguante...


En el **apartado Multijugador**, solo es posible jugar **de forma local a pantalla partida**, donde podremos medirnos hasta con 4 amigos, algo ideal si podemos disfrutarlo en un monitor grande o en nuestro televisor. S**e echa de menos un Multijugador Online**, algo que a nuestro juicio es algo esencial en cualquier juego de este tipo que se precie, y tampoco se entiende mucho el que no esté implementado. Cierto es que el pretender ser un juego retro podría conllevar que este tipo de modo Multijugador no estuviese presente, pero sería el complemento perfecto para unos piques rápidos a través de internet. Quizás una actualización o parche con esta funcionalidad sería algo genial... (por si los desarrolladores leen esto)


![HCT Multi](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HorizonChaseTurbo/Analisis/HCT_Multi.webp)


*Aunque se echa de menos un modo Online, el poder jugar con 3 amigos en la misma pantalla es una gozada*


Si de veras os gustan los juegos de conducción clásicos, vais a disfrutar como enanos con Horizon Chase Turbo. Si por lo contrario estos títulos de antaño os dan igual, lo jugareis hasta la saciedad gracias a su sencilla pero efectiva mecánica de juego, y su cantidad de coches y circuitos. Nosotros en JugandoEnLinux.com os lo recomendamos sin dudarlo en ambos casos, pues es un juego enorme a pesar de su reducido precio (unos 17€), lo cual es otro aliciente más para comparlo.


Si quereis haceros una idea más exacta de como es "Horizon Chase Turbo" podeis ver el siguiente video donde vereis como lo pasamos en grande jugándolo (como se suele decir, para muestra un botón):


 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/it7ckGPM-XQ" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Podeis comprar Horizon Chase Turbo en la [tienda de Humble Bundle](https://www.humblebundle.com/store/horizon-chase-turbo?partner=jugandoenlinux) (patrocinado) y [Steam](https://store.steampowered.com/app/389140/Horizon_Chase_Turbo/).


 


 ¿Qué os parece esta propuesta de Aquiris? ¿Os gustan los juegos de conducción clásicos? Déjanos tus impresiones sobre él en los comentarios o en nuestros canales de [Telegram](https://t.me/jugandoenlinux) y [Discord](https://discord.gg/fgQubVY).

