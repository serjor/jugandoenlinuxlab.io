---
author: Pato
category: Noticia
date: 2020-02-14 12:03:37
excerpt: "<p>Sin embargo a\xFAn no sabemos fecha para la llegada de su modalidad gratuita</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/Stadianews022020.webp
joomla_id: 1170
joomla_url: stadia-anuncia-nuevos-juegos-y-sigue-los-pasos-de-otras-plataformas-con-exclusivos-temporales
layout: post
title: Stadia anuncia nuevos juegos y sigue los pasos de otras plataformas con exclusivos
  temporales
---
Sin embargo aún no sabemos fecha para la llegada de su modalidad gratuita


Stadia comienza a moverse tras un estreno de la plataforma que hasta ahora no ha cumplido con lo esperado en cuanto a número de juegos o características anunciadas y que a fecha de hoy aún no están disponibles. Es algo que los usuarios de la plataforma de Google han estado demandando últimamente, ya que la compañía ha estado guardando un silencio preocupante respecto a sus novedades a pesar de que anunciaron el lanzamiento de nada menos que **120 juegos durante este año 2020**, 10 de ellos exclusivos.


Ahora, en un [nuevo comunicado](https://community.stadia.com/t5/Stadia-Community-Blog/New-games-coming-to-Stadia/ba-p/15052) han anunciado tres nuevos juegos que están a punto de llegar a Stadia en forma de exclusivas temporales, en lo que será al parecer su programa "**first on Stadia**" además de los ya anunciados **DOOM Eternal, Get Packed, Orcs Must Die! 3 y Baldur’s Gate 3.**


El primero de los juegos "first on Stadia" anunciados será **Lost Words: Beyond the page**, un juego atmoférico de puzzles guiados por una historia en la que nos adentraremos en el diario de una adolescente. El guión correrá a cargo de la conocida escritora Rhianna Pratchett:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/DoxT8wvjqVc" width="560"></iframe></div>


El segundo de los juegos anunciados es **Stacks on Stacks (on Stacks)**un juego donde tendremos que construir torres usando piezas en 3D:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/tu-_1blRpBM" width="560"></iframe></div>


El tercero de los juegos anunciados bajo la premisa de exclusividad temporal es **Spitlings**, un juego multijugador competitivo con varios modos de juego y un estilo visual desenfadado para jugar con amigos u otros jugadores y echar unas risas:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/TGo9AvzDO6o" width="560"></iframe></div>


Por otra parte, también han anunciado la llegada a Stadia de otros juegos como:


**Panzer Dragoon: Remake**, el archi-conocido juego de lucha sobre dragones que ha sido adaptado a los tiempos que corren con nuevos gráficos y jugabilidad.


![Panzer dragoon](/https://community.stadia.com/t5/image/serverpage/image-id/1493iB6F95CE0C55E71CA/image-dimensions/714x402?v=1.webp)


**Serious Sam Collection**: se trata de la llegada a Stadia de la afamada serie clásica de Sam "el Serio" Stone donde poder soltar adrenalina mientras disparamos a todo lo que se mueva:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/J52VjtGa0pI" width="560"></iframe></div>


Además de esto, también recuerdan que **Borderlands 3** recibirá la expansión Moxxi’s Heist y el evento "Broken Heart's Day".


Esperemos que Stadia siga introduciendo novedades de cara a potenciar su catálogo, donde ya disponemos de grandes franquicias que podemos jugar desde nuestro sistema favorito (aunque de momento solo en la modalidad de suscripción) como por ejemplo **Destiny 2, Farming Simulator 19 o Rise of the Tomb Raider** entre otros. Por cierto, este mismo mes tendremos como novedades en la plataforma a **Gylt y Metro Exodus**.


Si quieres saber más, puedes visitar la página de Stadia <https://stadia.google.com>


¿Qué te parecen las novedades de Stadia? ¿juegas en Streaming en tu equipo Linux?


Cuéntanoslo en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

