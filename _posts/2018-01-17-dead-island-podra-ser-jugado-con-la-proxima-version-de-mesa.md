---
author: leillo1975
category: Software
date: 2018-01-17 15:15:51
excerpt: "<p>El cl\xE1sico de Techland funcionar\xE1 con drivers libres.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e1ef6b1511175103949b82a229ea0710.webp
joomla_id: 609
joomla_url: dead-island-podra-ser-jugado-con-la-proxima-version-de-mesa
layout: post
tags:
- gallium3d
- open-source
- dead-island
- mesa-18
- techland
title: "Dead Island podr\xE1 ser jugado con la pr\xF3xima versi\xF3n de Mesa."
---
El clásico de Techland funcionará con drivers libres.

Ahora mismo acabo de leer una noticia en [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Dead-Island-Gallium-Fixed) relaccionada con uno de mis juegos favoritos, el magnífico **Dead Island**, que hace 6 años llenó de zombies  los escritorios y televisores de medio mundo con su excepcional mezcla de acción, Terror y Rol. Gracias al trabajo y los parches introducidos en Mesa, los usuarios de AMD podrán disfrutar de este título con los driver Gallium3D. Hasta ahora tenían que soportar crasheos producidos la extensión ARB_get_program_binary. En las últimas versiones de Mesa esta extensión ha sido incluida y ahora el juego funcionará correctamente, por lo que en cuanto salga la proxima versión (**Mesa 18**) que se espera pronto, el juego será soportado. Estos parches ya habían sido incluidos en el mes de Noviembre, pero no habían sido revisados y aprobados hasta ahora; por lo que si usais drivers *radeon* o *nouveau* pronto os beneficiareis de ellos  de forma oficial.


Desde JugandoEnLinux.com también nos gustaría recomendaros **Dead Island Definitive Edition** y su continuación, **Dead Island Riptide Definitive Edition**. Los juegos, desarrollados por los polacos [Techland](http://techland.pl/), creadores del también asombroso **Dying Light;** se pueden considerar uno de los mejores referentes en los juegos de zombies, y como comentabamos al principio mezcla de forma magistral acción, rol y visceras. En él podremos potenciar nuestras habilidadades, así como mejorar y crear nuestras propias armas a lo largo de una historia con montones de misiones secundarias que podremos superar con variados personajes. El juego, original por presentar la acción en una isla paradisíaca a plena luz del día, ha sido un antes y un después en los juegos de zombies, y aun hoy, despues de más de un lustro resulta tremendamente divertido, especialmente si se disfruta en compañía en su fantástico modo cooperativo.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/BIi-WDzeU4w" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Os informaremos de la salida oficial de Mesa 18 para que os podais benefiar tanto de esta corrección, como de las muchas mejoras que presentará. También nos gustaría animar a los miembros de nuestra comunidad a jugar en cooperativo a este juego. ¿Has jugado alguna vez a Dead Island? ¿Que te parece su planteamiento? Dejanos tu opinión en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

