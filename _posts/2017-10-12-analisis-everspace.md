---
author: Serjor
category: "An\xE1lisis"
date: 2017-10-12 15:32:29
excerpt: "<p>Acci\xF3n fren\xE9tica y dificultad se combinan en un t\xEDtulo para\
  \ los amantes de los juegos espaciales</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1eca156427374c8abf4738e42b464eef.webp
joomla_id: 483
joomla_url: analisis-everspace
layout: post
tags:
- accion
- analisis
- roguelike
- everspace
title: "An\xE1lisis: EVERSPACE"
---
Acción frenética y dificultad se combinan en un título para los amantes de los juegos espaciales

A comienzos de septiembre aparecía un [mensaje](http://steamcommunity.com/app/396750) en los foros de steam donde ROCKFISH Games nos anunciaba el lanzamiento de la versión no oficial (o beta) de Everspace para GNU/Linux.



> 
> **Advertencia:** A fecha de la redacción de este análisis, Everspace aún no aparece oficialmente para GNU/Linux como plataforma soportada por algunos problemas entre Unreal Engine y las gráficas de AMD. No obstante, para gráficas NVIDIA el juego funciona, y funciona muy bien.
> 
> 
> 


Pero, más allá de su funcionamiento, ¿qué es Everspace? Quizás responder que es un roguelike, si bien es cierto al 100% y no mentiríamos, al menos desde mi punto de vista, no sería del todo preciso. Everspace es un roguelike en 3D situado en el espacio, por lo que dispondremos de un control de seis grados de libertad, dónde cada mazmorra, o cada instancia donde tiene lugar la acción, es generado de manera procedural. Además durante el camino tendremos que hacernos acopio de recursos naturales, armas y power-ups, podremos comerciar y fabricar nuevos elementos, y si morimos, comenzamos desde el principio.


Hasta aquí sí se podría decir que es un roguelike, todos sus elementos están ahí y el planteamiento básico se mantiene de principio a ¿fin? (supongo, no me lo he podido terminar, aún) pero al menos no se siente como tal. Hay quién podría decir que es un FTL en 3D, aunque sería un poco vago.


Y es que quizás es su jugabilidad 3D la que hace que el juego parezca una arcade espacial. Everspace tiene un sistema de control que, personalmente, mejor responde a las acciones del usuario. Es un juego que da la oportunidad de jugar con mando o con teclado y ratón, y aunque he intentado jugarlo con mando (en mi caso un steam controller), el control que ofrece el teclado y ratón es muy superior, cosa que la propia desarrolladora sabe y recomienda también usar el teclado y ratón.


La forma de manejar la nave es muy natural. Sí que es cierto que, por la disposición de las teclas, el movimiento vertical cuesta un poco y en el fragor de la batalla hacer uso de ese desplazamiento cuesta. No obstante con práctica o un buen remapeo estas digitaciones se pueden mejorar y acompañar así a un control muy natural. El juego no es un simulador espacial, y no lo pretende, así que las inercias están adaptadas para un juego muy rápido con movimientos más propios de un entorno con algo más de gravedad o algún tipo de resistencia, aunque no os quedéis sin motor de inercia, porque la nave deja de compensar y el movimiento deja de ser natural para volverse incómodo.


Y es que este control es imprescindible para desenvolverse en unos combates en lo que de buenas a primeras tenemos todas las de perder. Porque el juego en su conjunto es difícil. Los combates, sobretodo en las primeras partidas, aunque no imposibles, son complicados de llevar. No obstante el juego te deja avanzar, pero según avanzamos las circunstancias se van volviendo más y más adversas. Podemos derrotar a las naves enemigas, sí, pero muy posiblemente nuestro escudo haya podido caer y antes de que se recuperara habremos recibido algún daño que reduce capacidad del casco, el cuál, si no lo reparamos, irá reduciendo su resistencia para absorber impactos y morir en la inmensidad del espacio.


![everspace mapa estelar](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Everespace/everspace%20mapa%20estelar.webp)


Lo de riesgo bajo es una metáfora, no os fiéis  



Y no es solo que tengamos problemas con el casco, para movernos de mazmorra en mazmorra (o de planeta en planeta) tenemos que hacerlo a través de saltos al hiperespacio. Estos saltos consumen combustible, así que hay que rellenar el depósito antes de que se agote, o con gran índice de probabilidad nos desintegraremos durante el salto. Además, y entre salto y salto habrá ocasiones en las que las facciones enemigas inhiban el motor de cálculo de saltos de la nave, con lo que está no podrá hacer los preparativos. Para poder continuar deberemos hackear el inhibidor, el cuál suele estar muy protegido, así que eso de ir pegando saltos mientras haya combustible para evitar el combate no es una opción, hay que pelear, sí o sí.


![everespace copkit](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Everespace/everespace%20copkit.webp)


De camino a la gloria, o no...  



Para poder avanzar el juego nos irá proporcionando ítems en forma de recursos naturales, dinero o componentes con los que mejorar la nave, así que en ocasiones podemos pasar de estar completamente desauciados a, con mucha mucha suerte, recomponer la nave, rellenar el arsenal de misiles y hacernos con alguna mejora que nos de margen en el combate para poder llegar hasta nuestro destino final.


Aunque llegar a ese destino final no es tan sencillo como parece, y vamos a morir, vamos a morir muchas veces. Y es cuando morimos cuando el juego se acerca muy discretamente a las mecánicas clásicas de los RPG, y así como al morir perdemos todas las mejoras para la nave que hayamos conseguido, sí que conservaremos todo el dinero acumulado, y antes de volver a comenzar nuestra aventura, **deberemos** gastar todo el dinero que hayamos acumulado en comprar mejoras para la nave y compensar así la dificultad del juego, porque sino lo perderemos.


![everespace upgrades](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Everespace/everespace%20upgrades.webp)


A parte de comprar mejoras, si tenemos dinero suficiente podremos comprar nuevos modelos de naves  



Además, todo esto está acompañado de una estética muy lucida, con un ambientación que si bien no pretende ser fiel a la realidad, sabe reflejar sin grandes alardes cómo sería volar por el espacio. Junto con esta estética encontramos también una historia que a pesar de que no cuenta nada nuevo tengo ganas de averiguar más sobre ella, porque debido a mi torpeza aún no sé cómo acaba.


![everespace modo foto](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Everespace/everespace%20modo%20foto.webp)


El juego trae un modo foto con el que podemos parar la acción y tomar así una foto espectacular, y un respiro...


No obstante y cómo no podía ser de otra manera, no todo es tan bueno como suena. Quizás sea cosa mía y no le pase a nadie más, pero en ocasiones he perdido combates simplemente porque el punto de mira de la nave se camuflaba con la acción y no era capaz de encontrarlo dentro de la pantalla, así que no sabía dónde estaba apuntando exactamente, y eso en este juego significa morir.


También su sistema de generación de mapas es un poco limitado. En teoría es procedural, y no lo niego, pero hay muy poca variedad de opciones y muchas veces el entorno se vuelve repetitivo, y aunque sí que es cierto que la disposición de los elementos varía y no hay dos instancias iguales, son tan parecidas que se podría decir que son iguales. Cierto que esto no impide disfrutar del resto del conjunto, pero al juntarlo con el hecho de morir tantas veces y empezar casi de cero constantemente hace que haya un punto de tedio o de monotonía.


¿Es esto Everspace? Quizás falta añadir algo que no se puede explicar, y es que al menos en mi caso el juego tiene *eso* que te hace continuar y te lleva a comenzar una partida más. Como comentábamos en el apartado del control, la nave se lleva muy bien gracias a unos controles que responden inmediatamente, así que cuando mueres, mueres porque has fallado, mueres porque no has sabido plantear el combate, mueres porque no llevabas la equipación adecuada, en el fondo, mueres porque merecías morir, y es ahí donde el juego engancha. Es un juego que te pica y reta, te dice que si hubieras cambiado tu forma de jugar hubieras seguido vivo, que si hubieras disparado un misil más hubieras acabado con tu enemigo, que si fueras más ágil hubieras podido esquivar esa oleada, y eso es lo que te mantiene pegado al juego, al menos en mi caso lo consigue.


Y ahora sí que podemos decir que esto es Everspace, un roguelike escondido en un arcade espacial, con discretos guiños a los RPG y con una gran capacidad de adicción.


Por último os dejo con un gameplay del juego en su versión para GNU/Linux. Eso sí, usando los drivers privativos de NVIDIA, no está garantizado (aún) que funcione con AMD.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/-Qzk8XnR0wY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/396750/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


Y tú, ¿te apuntas a una aventura espacial hacia la muerte? Cuéntanos qué te parece este Everspace en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

