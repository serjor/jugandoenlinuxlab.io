---
author: Serjor
category: "Acci\xF3n"
date: 2018-04-03 21:37:59
excerpt: "<p>De la mano de la productora Devolver Digital llega este juego al sistema\
  \ del ping\xFCino</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0d4be46703e4caec7901c928d2f5e122.webp
joomla_id: 702
joomla_url: ruiner-disponible-en-gnu-linux-a-partir-del-6-de-abril
layout: post
tags:
- devolver-digital
- ruiner
title: RUINER, disponible en GNU/Linux a partir del 6 de abril (ACTUALIZADO)
---
De la mano de la productora Devolver Digital llega este juego al sistema del pingüino

**ACTUALIZACIÓN 7-4-18:** Finalmente el juego ha salido en el día de ayer en **modo Beta**, tal y como estaba previsto. El anuncio se ha realizado a través de Steam en la siguiente [noticia](https://steamcommunity.com/games/464060/announcements/detail/1662266809368512328). Por lo que se ve tiene algunos problemas de [pequeños tirones](https://steamcommunity.com/app/464060/discussions/0/154644928862239593/?tscn=1523026836#c1696043806562132820) pero nada importante. Confiemos que se solucionen pronto y cuando sea lanzado de forma oficial podamos disfrutarlo sin ellos.




---


**NOTICIA ORIGINAL:** Según leemos en twitter:



> 
> Mother, where's my tux? ( ͡° ͜ʖ ͡°)ﾉ⌐■-■  
>   
> Ruiner is coming to Linux THIS FRIDAY.[#ruinergame](https://twitter.com/hashtag/ruinergame?src=hash&ref_src=twsrc%5Etfw) [#ruiner](https://twitter.com/hashtag/ruiner?src=hash&ref_src=twsrc%5Etfw) [#linux](https://twitter.com/hashtag/linux?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/ZWKt8i7XGb](https://t.co/ZWKt8i7XGb)
> 
> 
> — RUINER (@ruinergame) [3 de abril de 2018](https://twitter.com/ruinergame/status/981256712928808961?ref_src=twsrc%5Etfw)







Ruiner, un juego ambientado en un futuro distópico en el que tendremos que avanzar a base de matar a todo el que se nos ponga en medio, tendrá versión para GNU/Linux a partir del viernes 6 de abril, y que nos trae un planteamiento jugable interesante en vista isomética en la que la historia es un tanto larga de explicar, así que os ponemos la sinopsis de steam directamente:



> 
> RUINER es un emocionante y brutal juego de disparos ambientado en el año 2091 en la cibermetrópolis de Rengkok. Un sociópata trastornado se rebela contra el corrupto sistema para rescatar a su hermano secuestrado y descubrir la verdad guiado por una misteriosa amiga hacker. Combina reflejos sobrenaturales, herramientas mejoradas y el arsenal de enemigos muertos con el objetivo de desmantelar a los titanes corporativos del tráfico de virtualidad en CIELO.
> 
> 
> Combate brutal y sofisticado: RUINER permite combatir a la velocidad del rayo empleando un delicado equilibrio entre golpes incontenibles, ultracontundentes y elegantes para superar a todo tipo de rivales salvajes. Ábrete paso entre multitudes o ralentiza el tiempo y selecciona cuidadosamente tus puntos de ataque antes de liberar una tormenta de violencia en el campo de batalla.
> 
> 
> Un arsenal ciberpunk: Equípate con todo tipo de armas de fuego altamente tecnológicas y armas cuerpo a cuerpo para acabar con todo aquel que ose plantarte cara. Emplea dispositivos superavanzados como escudos de energía y aumentos de sprint, y cambia de arma o hackea a tus rivales para que se unan a tu bando.
> 
> 
> Acción con argumento: La confrontación violenta es una forma de vida, y la confianza una moneda devaluada en Rengkok. Tendrás que vagar por un mundo sin sentido con la ayuda de tu misteriosa amiga hacker.
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/jkjIsnjKhew" width="560"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/464060/" style="border: 0px;" width="646"></iframe></div>


Y tú, ¿te harás con este frenético juego? Cuéntanoslo en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

