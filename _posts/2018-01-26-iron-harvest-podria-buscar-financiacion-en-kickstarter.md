---
author: leillo1975
category: Estrategia
date: 2018-01-26 14:28:13
excerpt: "<p><span style=\"text-decoration: underline;\"><strong>ACTUALIZACI\xD3N\
  \ 23-2-18:</strong></span> Finalmente King Art Games acaba de publicar en twitter\
  \ que el <strong>13 de Marzo</strong> comenzar\xE1 su campa\xF1a de financiaci\xF3\
  n en <strong>Kickstarter</strong>. Desde JugandoEnLinux.com les deseamos toda la\
  \ suerte del mundo y que consigan sus objetivos, pues a nuestro juicio, su prometedor\
  \ y original juego lo merece. Este es el tweet del anuncio:</p>\r\n<blockquote class=\"\
  twitter-tweet\" data-partner=\"tweetdeck\">\r\n<p dir=\"ltr\" lang=\"en\">Iron Harvest\
  \ is coming to Kickstarter March 13th.<br />Please share &amp; save the date! <a\
  \ href=\"https://twitter.com/hashtag/ironharvest?src=hash&amp;ref_src=twsrc%5Etfw\"\
  >#ironharvest</a> <a href=\"https://twitter.com/hashtag/Kickstarter?src=hash&amp;ref_src=twsrc%5Etfw\"\
  >#Kickstarter</a> <a href=\"https://twitter.com/hashtag/madewithunity?src=hash&amp;ref_src=twsrc%5Etfw\"\
  >#madewithunity</a> <a href=\"https://t.co/ShB9MRwUmS\">pic.twitter.com/ShB9MRwUmS</a></p>\r\
  \n\u2014 KING Art Games (@KingArtGames) <a href=\"https://twitter.com/KingArtGames/status/967019977739067394?ref_src=twsrc%5Etfw\"\
  >February 23, 2018</a></blockquote>\r\n<script src=\"https://platform.twitter.com/widgets.js\"\
  \ async=\"\" type=\"text/javascript\" charset=\"utf-8\"></script>\r\n<p>&nbsp;</p>\r\
  \n<hr />\r\n<p><span style=\"text-decoration: underline;\"><strong>NOTICIA ORIGINAL:</strong></span>\
  \ Hace ya bastante tiempo que le sigo la pista a este juego, concretamente desde\
  \ que en su d\xEDa cubrimos la salida de <a href=\"index.php/homepage/generos/rol/item/237-mas-rpg-para-linux-steamos-con-the-dwarves\"\
  \ target=\"_blank\" rel=\"noopener\">The Dwarves</a> y su posterior <a href=\"index.php/homepage/analisis/item/247-analisis-the-dwarves\"\
  \ target=\"_blank\" rel=\"noopener\">an\xE1lisis</a>. Pero la informaci\xF3n ofrecida\
  \ por <a href=\"https://kingart-games.com/\">King Art Games</a> hasta la fecha ha\
  \ sido poca y escueta. De \xE9l sabemos que se trata de un juego de estrategia en\
  \ tiempo real donde nos encontramos en un pasado dist\xF3pico a principios del siglo\
  \ XX.</p>\r\n<p>Europa se encuentra dividida despu\xE9s de una gran guerra en 3\
  \ facciones, el <strong>Imperio Saj\xF3n</strong>, con una poderosa industria, ciudades\
  \ desarrolladas y una fuerte tradici\xF3n militar; el <strong>Rusviet</strong> es\
  \ enorme, poderoso, y tiene un potencial industrial, militar y poblacional inigualable,\
  \ pero se encuentra sumido en problemas pol\xEDticos que desestabilizan el pais,\
  \ sobre todo despu\xE9s de la gran guerra. En medio de estas dos potencias se encuentra\
  \ la rep\xFAblica de <strong>Polania</strong>, un basto pais con una gran riqueza\
  \ agr\xEDcola y una gran historia que tiene que lidiar con el Rusviet por parte\
  \ de su territorio. Para m\xE1s inri, un <strong>enemigo desconocido</strong> acecha\
  \ Europa con idea de sumirla en otra gran guerra y tomar el control. Como veis la\
  \ historia se sit\xFAa m\xE1s o menos despu\xE9s de la primera guerra mundial, pero\
  \ obviamente en un mundo diferente donde las <strong>enormes m\xE1quinas</strong>\
  \ que se usaron en la gran guerra son ahora parte de la vida diaria, y la tecnolog\xED\
  a tom\xF3 rumbos insospechados. Todo este mundo est\xE1 creado por el artista polaco\
  \ <a href=\"https://www.artstation.com/jakubrozalski\" target=\"_blank\" rel=\"\
  noopener\">Jakub R\xF3\u017Calski</a>.</p>\r\n<p><img src=\"images/Articulos/Iron_Harvest/IH_screenshot01_copiar.jpg\"\
  \ alt=\"IH screenshot01 copiar\" class=\"img-responsive\" style=\"display: block;\
  \ margin-left: auto; margin-right: auto;\" /></p>\r\n<p>Como podeis observar se\
  \ trata de una <strong>historia y entornos apasionantes</strong>, de los que confieso,\
  \ me he sentido atraido desde que lo conoc\xED hace poco m\xE1s de un a\xF1o. En\
  \ cuanto a su disponibilidad para nuestro sistema, en una ocasi\xF3n, a trav\xE9\
  s de twiter, King Art nos coment\xF3 que tendr\xEDa soporte como todos sus \xFA\
  ltimos juegos. En cuanto al t\xEDtulo de este art\xEDculo, pues hay que decir que\
  \ <strong>King Art tiene en mente realizar una campa\xF1a de Crowfunding</strong>\
  \ para recaudar dinero y poder permanecer independientes el mayor tiempo posible.\
  \ Para ello ha propuesto una <a href=\"https://www.surveymonkey.de/r/ironharvest\"\
  \ target=\"_blank\" rel=\"noopener\">encuesta</a> a sus seguidores para decidir\
  \ como quieren que esta sea, con preguntas referentes a las ediciones de su juego,\
  \ acceso a betas, alphas, ediciones f\xEDsicas, y por supuesto que <strong>plataformas\
  \ quieren que salga el juego</strong>, entre las que por supuesto se encuentra Linux,\
  \ y donde si os gusta el juego deber\xEDais marcar. El anuncio en twitter donde\
  \ nos indicaban la encuesta sobre Kickstarter es este:</p>\r\n<blockquote class=\"\
  twitter-tweet\" data-lang=\"es\">\r\n<p dir=\"ltr\" lang=\"en\"><a href=\"https://twitter.com/hashtag/IronHarvest?src=hash&amp;ref_src=twsrc%5Etfw\"\
  >#IronHarvest</a> We\u2019re thinking about doing a Kickstarter campaign to stay\
  \ independent for as long as possible. Doing it right means to ask you guys what\
  \ you actually want. If you have 5 minutes, please let us know what you think in\
  \ our Kickstarter survey: <a href=\"https://t.co/b59HDd59QO\">https://t.co/b59HDd59QO</a>\
  \ <a href=\"https://t.co/ayJpISabXh\">pic.twitter.com/ayJpISabXh</a></p>\r\n\u2014\
  \ KING Art Games (@KingArtGames) <a href=\"https://twitter.com/KingArtGames/status/956547529830752256?ref_src=twsrc%5Etfw\"\
  >25 de enero de 2018</a></blockquote>\r\n<script src=\"https://platform.twitter.com/widgets.js\"\
  \ async=\"\" type=\"text/javascript\" charset=\"utf-8\"></script>\r\n<p>King Art\
  \ Games son un estudio alem\xE1n, y son los desarrolladores de t\xEDtulos como \"\
  The Dwarves\", \"<a href=\"index.php/homepage/generos/terror/item/676-black-mirror-la-aventura-de-terror-gotico-ya-esta-disponible-en-linux-steamos\"\
  \ target=\"_blank\" rel=\"noopener\">Black Mirror</a>\" o la saga de \"The Book\
  \ of Unwritten Tales\", y desde hace tiempo se han caracterizado por dar soporte\
  \ a nuestro sistema en sus proyectos, lo cual, por supuesto, es de agradecer y apoyar.\
  \ Desde JugandoEnLinux.com \"marcaremos\" de cerca este interesante proyecto y os\
  \ mantendremos informados de cuantas noticias y actualizaciones genere. Os dejo\
  \ con el espectacular trailer del juego para que veiais de lo que os hablo:</p>\r\
  \n<p><iframe src=\"https://www.youtube.com/embed/AT29OGk_Byc\" width=\"800\" height=\"\
  450\" style=\"display: block; margin-left: auto; margin-right: auto;\" allowfullscreen=\"\
  allowfullscreen\"></iframe></p>\r\n<p>&nbsp;</p>\r\n<p>\xBFQue os parece el mundo\
  \ que representa Iron Harvest? \xBFNo os parece alucinante su ambientaci\xF3n. Deja\
  \ tus respuestas en los comentarios o en nuestro grupo de <a href=\"https://t.me/jugandoenlinux\"\
  \ target=\"_blank\" rel=\"noopener\">Telegram</a>.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3cee03f16e025a4c4456f628550bf95f.webp
joomla_id: 620
joomla_url: iron-harvest-podria-buscar-financiacion-en-kickstarter
layout: post
tags:
- rts
- kickstarter
- king-art
- iron-harvest
title: "Iron Harvest podr\xEDa buscar financiaci\xF3n en Kickstarter (ACTUALIZADO)"
---
**ACTUALIZACIÓN 23-2-18:** Finalmente King Art Games acaba de publicar en twitter que el **13 de Marzo** comenzará su campaña de financiación en **Kickstarter**. Desde JugandoEnLinux.com les deseamos toda la suerte del mundo y que consigan sus objetivos, pues a nuestro juicio, su prometedor y original juego lo merece. Este es el tweet del anuncio:



> 
> Iron Harvest is coming to Kickstarter March 13th.  
> Please share & save the date! [#ironharvest](https://twitter.com/hashtag/ironharvest?src=hash&ref_src=twsrc%5Etfw) [#Kickstarter](https://twitter.com/hashtag/Kickstarter?src=hash&ref_src=twsrc%5Etfw) [#madewithunity](https://twitter.com/hashtag/madewithunity?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/ShB9MRwUmS](https://t.co/ShB9MRwUmS)
> 
> 
> — KING Art Games (@KingArtGames) [February 23, 2018](https://twitter.com/KingArtGames/status/967019977739067394?ref_src=twsrc%5Etfw)



 




---


**NOTICIA ORIGINAL:** Hace ya bastante tiempo que le sigo la pista a este juego, concretamente desde que en su día cubrimos la salida de [The Dwarves](index.php/homepage/generos/rol/item/237-mas-rpg-para-linux-steamos-con-the-dwarves) y su posterior [análisis](index.php/homepage/analisis/item/247-analisis-the-dwarves). Pero la información ofrecida por [King Art Games](https://kingart-games.com/) hasta la fecha ha sido poca y escueta. De él sabemos que se trata de un juego de estrategia en tiempo real donde nos encontramos en un pasado distópico a principios del siglo XX.


Europa se encuentra dividida después de una gran guerra en 3 facciones, el **Imperio Sajón**, con una poderosa industria, ciudades desarrolladas y una fuerte tradición militar; el **Rusviet** es enorme, poderoso, y tiene un potencial industrial, militar y poblacional inigualable, pero se encuentra sumido en problemas políticos que desestabilizan el pais, sobre todo después de la gran guerra. En medio de estas dos potencias se encuentra la república de **Polania**, un basto pais con una gran riqueza agrícola y una gran historia que tiene que lidiar con el Rusviet por parte de su territorio. Para más inri, un **enemigo desconocido** acecha Europa con idea de sumirla en otra gran guerra y tomar el control. Como veis la historia se sitúa más o menos después de la primera guerra mundial, pero obviamente en un mundo diferente donde las **enormes máquinas** que se usaron en la gran guerra son ahora parte de la vida diaria, y la tecnología tomó rumbos insospechados. Todo este mundo está creado por el artista polaco [Jakub Różalski](https://www.artstation.com/jakubrozalski).


![IH screenshot01 copiar](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Iron_Harvest/IH_screenshot01_copiar.webp)


Como podeis observar se trata de una **historia y entornos apasionantes**, de los que confieso, me he sentido atraido desde que lo conocí hace poco más de un año. En cuanto a su disponibilidad para nuestro sistema, en una ocasión, a través de twiter, King Art nos comentó que tendría soporte como todos sus últimos juegos. En cuanto al título de este artículo, pues hay que decir que **King Art tiene en mente realizar una campaña de Crowfunding** para recaudar dinero y poder permanecer independientes el mayor tiempo posible. Para ello ha propuesto una [encuesta](https://www.surveymonkey.de/r/ironharvest) a sus seguidores para decidir como quieren que esta sea, con preguntas referentes a las ediciones de su juego, acceso a betas, alphas, ediciones físicas, y por supuesto que **plataformas quieren que salga el juego**, entre las que por supuesto se encuentra Linux, y donde si os gusta el juego deberíais marcar. El anuncio en twitter donde nos indicaban la encuesta sobre Kickstarter es este:



> 
> [#IronHarvest](https://twitter.com/hashtag/IronHarvest?src=hash&ref_src=twsrc%5Etfw) We’re thinking about doing a Kickstarter campaign to stay independent for as long as possible. Doing it right means to ask you guys what you actually want. If you have 5 minutes, please let us know what you think in our Kickstarter survey: <https://t.co/b59HDd59QO> [pic.twitter.com/ayJpISabXh](https://t.co/ayJpISabXh)
> 
> 
> — KING Art Games (@KingArtGames) [25 de enero de 2018](https://twitter.com/KingArtGames/status/956547529830752256?ref_src=twsrc%5Etfw)



King Art Games son un estudio alemán, y son los desarrolladores de títulos como "The Dwarves", "[Black Mirror](index.php/homepage/generos/terror/item/676-black-mirror-la-aventura-de-terror-gotico-ya-esta-disponible-en-linux-steamos)" o la saga de "The Book of Unwritten Tales", y desde hace tiempo se han caracterizado por dar soporte a nuestro sistema en sus proyectos, lo cual, por supuesto, es de agradecer y apoyar. Desde JugandoEnLinux.com "marcaremos" de cerca este interesante proyecto y os mantendremos informados de cuantas noticias y actualizaciones genere. Os dejo con el espectacular trailer del juego para que veiais de lo que os hablo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/AT29OGk_Byc" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Que os parece el mundo que representa Iron Harvest? ¿No os parece alucinante su ambientación. Deja tus respuestas en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

