---
author: leillo1975
category: Carreras
date: 2020-08-27 07:38:12
excerpt: "<p><span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\"\
  >@ViJuDaGD nos confirma que habr\xE1 Woden GP2 y tendr\xE1 soporte nativo.&nbsp;</span></p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/WodenGP/woden.webp
joomla_id: 1251
joomla_url: woden-gp-un-arcade-de-carreras-retro-tendra-version-linux-a1
layout: post
tags:
- carreras
- retro
- unity3d
- woden-gp
- vijuda
title: "Woden GP, un arcade de carreras retro, tendr\xE1 versi\xF3n Linux (ACTUALIZACI\xD3\
  N) "
---
@ViJuDaGD nos confirma que habrá Woden GP2 y tendrá soporte nativo. 


**ACTUALIZADO 4-1-21:**   
Probablemente os habreis preguntado que ha pasado con este juego, y es que en pleno verano os vinimos con la exclusiva que este **juego indie de carreras de coches con regustillo retro** tendría versión nativa. Tras varios intentos por parte de su desarrollador de portarlo, finalmente ha sido imposible por el momento traerlo a nuestro sistema, basicamente por la forma en que está programado desde su inicio.


El caso es que [ViJuDa](https://twitter.com/ViJuDaGD), animado por la acogida que ha tenido su juego, **se ha animado a crear una segunda parte, esta vez contando desde un principio con la versión de Linux en sus planes**, lo cual nos permite asegurar que **SI habrá versión de Woden GP2 en nuestro sistema**. Podemos deciros también que **estamos participando en el proceso de desarrollo testeando versiones preliminares del juego**, y por lo que hemos visto hasta ahora funciona genial. Además gracias al desarrollo de esta segunda parte, Victor nos comenta que **sería posible modificar la primera parte para que funcionase en GNU/Linux**, por lo que no descarta el port, aunque este proceso sería laborioso y ahora está enfrascado en el desarrollo de la secuela.


Podemos deciros que **Woden GP2 será un juego mucho más ambicioso** que su predecesor y para ello os podemos adelantar las siguientes características:


*-Gran salto grafico y sonoro*  
*-Planteamiento de modo carrera a lo Gran Turismo, donde compramos y vendemos coches, y debemos de ganar competiciones para ir sumando créditos y adquirir más vehículos.*  
*-Una gran cantidad de coches, que a este momento del desarrollo, casi cuatripica a los vistos en el primero, es decir un total de 50 coches y subiendo!*  
*-Ciclo día y noche, y clima.*  
*-Una IA avanzada donde los rivales compiten con coches con las mismas prestaciones que los del jugador.*  
*-Taller de pintura*  
*-Multijugador local para hasta 4 jugadores a pantalla dividida.*  
*-Competiciones agrupadas en 4 categorias : Fin de semana, Ligas, Rallys y resistencia.*


Como veis el desarrollador tiene mucho trabajo por delante, y su lanzamiento se espera para Abril o Mayo. Nosotros por supuesto estaremos muy atentos a todas las novedades que vaya generando tanto este nuevo proyecto, como el port del primer juego. Os dejamos con un gameplay de una versión muy preliminar del juego:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/gwLgogoxi7A" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


 




---


**NOTICIA ORIGINAL:**  
  
 Ayer por la noche, viendo un fantástico video de [Gustingorriz](https://youtu.be/KYiYbY6BObA), descubría un llamativo juego de **estilo noventero** que inmediatamente cautivó mi atención. Decidido a investigar un poco más entré en su página en [Steam](https://store.steampowered.com/app/1337330/Woden_GP/?l=spanish) y vi que estaba desarrollado por una compañía indie llamada [ViJuDa](https://twitter.com/WodenVigo). Mi sorpresa fué mayuscula cuando vi que dicha compañía estaba fundada por alguien de mi ciudad, Vigo. Ese alguien se llama **Victor Justo Dacruz** y enseguida nos poníamos en contacto con el para preguntarle por su juego, y por supuesto la pregunta de rigor. ¿Tendrá Woden GP versión para nuestro sistema? La respuesta ya la teneis en el títular, como podeis ver en este tweet:



> 
> Dos juegazos que son los pilares de las influencias de Woden GP! Woden GP es un humilde homenaje a aquellos juegos que tanto echaba de menos. Prometo versión para linux , oficialmente, si no es el día del lanzamiento, será muy pronto
> 
> 
> — ViJuDa (@WodenVigo) [August 26, 2020](https://twitter.com/WodenVigo/status/1298719417669304323?ref_src=twsrc%5Etfw)


  





Como podeis leer, aunque no promete el soporte para el día oficial de lanzamiento, el 1 de Septiembre, en un futuro **podremos ejecutar nativamente su juego en nuestros sistemas basados en GNU/Linux**. El título, desarrollado con el **motor Unity3D** que tantas alegrias nos da, es como comentaba en el título un **arcade de carreras de perspectiva isométrica** al más puro estilo de los años 90, y con el estilo visual tan llamativo que le da, entre otras cosas, por estar **dibujado a mano**. El juego recuerda inmediatamente a referencias como el [World Rally Championship](https://en.wikipedia.org/wiki/World_Rally_(1993_video_game)) (de las españolas Zigurat-Gaelco) o [Ironman Super Offroad](https://es.wikipedia.org/wiki/Super_Off_Road). 


En el podremos encontrar una **gran cantidad de coches y categorías,** cada uno con sus caracterísiticas y skins únicos. El juego incluirá también montones de escenarios entre los que veremos **desiertos, ciudades o montañas**. Partes de estos escenarios están inspirados en zonas de la cuidad de Vigo, y **la publicidad que podremos ver en ellos corresponde a negocios locales**, al no poder contar con licencias oficiales. A nivel de modos de juego encontraremos un **modo historia**, donde contaremos con los consejos de nuestro Jefe de Equipo. También podremos disputar **etapas de Rally** con cuenta atras, o el tipico **Time Atack**. A nivel multijugador, existe un **modo de pantalla partida**, tal y como podemos ver en el material gráfico facilitado, y será compatible con **Remote Play Together.**


Si quereis ir añadiendo el juego a vuestra lista de deseos podeis hacerlo en [Steam](https://store.steampowered.com/app/1337330/Woden_GP/?l=spanish). En JugandoEnLinux.com , estaremos atentos para informaros en cuanto el juego tenga soporte natvo, y **si nos es posible realizaremos un video o stream exclusivo para todos vosotros** en nuestros canales de [Youtube](https://www.youtube.com/c/jugandoenlinuxcom) y [Twitch](https://www.twitch.tv/jugandoenlinux). Para ir calentando os dejamos con este video de su canal oficial:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="420" src="https://www.youtube.com/embed/HW6Ywmb-SiE" style="display: block; margin-left: auto; margin-right: auto;" width="750"></iframe></div>


¿Qué os parece este Woden GP? ¿Os van los juegos de temática retro? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

