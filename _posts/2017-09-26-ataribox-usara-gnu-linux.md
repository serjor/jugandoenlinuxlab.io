---
author: Serjor
category: Hardware
date: 2017-09-26 20:42:25
excerpt: "<p>Atari conf\xEDa en el crowdfunding para este proyecto</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/377829b055e89e3afb894e7528a10996.webp
joomla_id: 466
joomla_url: ataribox-usara-gnu-linux
layout: post
tags:
- ataribox
title: "Ataribox usar\xE1 GNU/Linux"
---
Atari confía en el crowdfunding para este proyecto

Leemos en [muycomputer.com](https://www.muycomputer.com/2017/09/26/ataribox-linux-chip-amd/) una interesantísima noticia, y es que la Ataribox, la nueva *máquina* de Atari funcionará con GNU/Linux.


Para quién no lo conozca, la [Ataribox](www.ataribox.com) es un top-box que se suma al carro de la moda comenzada por la NES y SNES mini de Nintendo, en la que quieren tirar de nostalgia para traer un PC de gama media con estética retro.


Este PC tendrá un precio que oscilará entre los 250 y 300 doláres (está por ver si harán conversión 1:1 o cuál será el precio en el mercado europeo) montará la APU bristol, un AMD con gráfica Radeon, permitirá mover juegos poco exigentes (ya han confirmado que nada de AAA) o hacer streaming de contenidos, y contará con contenido de la propia Atari así como de terceros.


Esperemos que la jugada le salga bien a Atari, que para los que ya tenemos unos años su 2600 fue la primera consola en casa.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/5WNsmCpdhhY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Y tú, ¿qué piensas de esta Ataribox? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

