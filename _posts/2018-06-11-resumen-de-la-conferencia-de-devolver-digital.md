---
author: Pato
category: Noticia
date: 2018-06-11 20:40:03
excerpt: "<p>Repasamos los juegos que han aparecido en el v\xEDdeo de esta pasada\
  \ madrugada</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1659cc75227694996edfedee430cb4a4.webp
joomla_id: 765
joomla_url: resumen-de-la-conferencia-de-devolver-digital
layout: post
tags:
- e3-2018
title: Resumen de la conferencia de Devolver Digital
---
Repasamos los juegos que han aparecido en el vídeo de esta pasada madrugada

Devolver Digital es una editora que se puede llamar "Linux friendly" puesto que casi todos sus títulos acaban llegando a nuestro sistema favorito. Desde hace un año realizan su propia "conferencia" que a fin de cuentas es un vídeo con un estilo característico. Los que visteis el del año pasado sabréis a lo que me refiero.


Este año vuelven a las andadas, con lo que vamos a ver los vídeos de los juegos que han aparecido y que tenemos muchas papeletas de verlos en nuestros PCs:


**SCUM**


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/fm62zthm388" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>



**My Friend Pedro- Bananas**


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/zaPBAKg3VT4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


**METAL WOLF CHAOS**


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/XiMQ9hCc0JA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Además de estos, en la cuenta de Twitter de Devolver Digital han publicado la nueva web de Serious Sam 4: Planet Badass donde tenemos nuevas imágenes:


![Planetbadass1](http://www.serioussam.com/images/screenshots/SS4_kamikazeattack.webp)


![PlanetBadass2](http://www.serioussam.com/images/screenshots/SS4_processed.webp)


![Planet badass3](http://www.serioussam.com/images/screenshots/SS4_mgnaar.webp)


![Planetbadass4](http://www.serioussam.com/images/screenshots/screenshots/SS4_motoshoot.webp)


![Planetbadass5](http://www.serioussam.com/images/screenshots/SS4_oilrig.webp)


![planetbadass6](http://www.serioussam.com/images/screenshots/SS4_werebull.webp)


Puedes visitar la web en [este enlace](http://www.serioussam.com/).


Por otra parte, si quieres ver la conferencia al completo, aquí tienes el vídeo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/_4ytFiRVMwg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 ¿Qué te ha parecido la conferencia de Devolver Digital? ¿Y los juegos?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

