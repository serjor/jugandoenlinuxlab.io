---
author: Serjor
category: "V\xEDdeos JEL"
date: 2022-01-09 13:55:46
excerpt: "<p>Hoy os traemos una explicaci\xF3n de las posibilidades que nos ofrece\
  \ el uso de <a href=\"https://pipewire.org/\">pipewire</a>, al poder usar fuentes\
  \ de audio de diferentes servidores de sonido en programas tipo <a href=\"https://kx.studio/Applications:Carla\"\
  >Carla</a>.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Videotutoriales/pipewire_carla_obs900px.webp
joomla_id: 1408
joomla_url: ejemplo-de-como-usar-pipewire-para-hacer-videos-con-obs-usando-plugins-de-audio-via-carla
layout: post
tags:
- streaming
- videos
- pipewire
- obs
- carla
- tutorial
- videotutorial
title: "Ejemplo de c\xF3mo usar pipewire para hacer v\xEDdeos con OBS usando plugins\
  \ de audio v\xEDa Carla"
---
Hoy os traemos una explicación de las posibilidades que nos ofrece el uso de [pipewire](https://pipewire.org/), al poder usar fuentes de audio de diferentes servidores de sonido en programas tipo [Carla](https://kx.studio/Applications:Carla).


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/pjJ5YqK60Fk" title="YouTube video player" width="560"></iframe></div>

