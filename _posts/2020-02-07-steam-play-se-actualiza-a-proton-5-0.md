---
author: leillo1975
category: Software
date: 2020-02-07 23:58:09
excerpt: "<p><span class=\"username txt-mute\">Nueva versi\xF3n de esta herramienta\
  \ de compatibilidad</span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Proton/valve-Proton.webp
joomla_id: 1167
joomla_url: steam-play-se-actualiza-a-proton-5-0
layout: post
tags:
- wine
- steam-play
- dxvk
- proton
- d9vk
title: "(ACTUALIZACI\xD3N) Steam Play se actualiza a Proton 5.0-9"
---
Nueva versión de esta herramienta de compatibilidad


**ACTUALIZACIÓN 11-6-20:** No ha pasado ni una semana desde la ultima actualización y ya tenemos nueva versión de Proton, la [5.0-9](https://github.com/ValveSoftware/Proton/releases/tag/proton-5.0-9), que tan solo trae dos cambios, aunque tremendamente importantes:


-Arreglada la ejecución de juegos que requieren el cliente de **EA Origin**.  
 -Arreglado el fallo en **Final Fantasy XII: La era del Zodíaco**.


Como veis, sobre todo el primero de los cambios amplía en gran medida el catálogo de juegos que podremos jugar. Seguimos informando...




---


**ACTUALIZACIÓN 6-6-20:** Apenas un mes después de la revisón 5.0-7, Valve ha liberado la siguiente iteración, la revisión [5.0-8](https://github.com/ValveSoftware/Proton/releases/tag/proton-5.0-8), con los siguientes cambios:


* Los tiempos de carga de Streets of Rage 4 han mejorado dramáticamente
* Solucionados crashes en Detroit: Become Human, Planet Zoo, Jurassic World: Evolution, Unity of Command II y Splinter Cell Blacklist
* Mejoras de rendimiento para DOOM Eternal, Detroit: Become Human y We Happy Few
* Soporte de la última versión de Steam SDK, lo que soluciona problemas en juegos como Scrap Mechanic y Mod and Play
* Solucionados algunos erroes del lanzador de Rockstar en algunos sistemas
* FAudio y DXVK han sido actualizados a las versiones 20.06 y 1.7 respectivamente
* Añadidos los últimos cambios en vkd3d
* Ahora en KDE los juegos que están a pantalla completa no impedirán hacer alt-tab fuera del juego
* Solucionados problemas de falta información sobre el tiempo de ping en algunos juegos multijugador como Path of Exile y Wolcen
* Solucionado el crash al lanzar STEINS;GATE 0 y TOXIKK
* Solucionado un tema de enlaces externos en Lords Mobile
* Se ha mejorado el rendimiento de gstreamer
* Solucionado un crash en WRC 7 al usar un volante. Avisan de que algunos efectos de force feedback pueden requerir un kernel >= al 5.7


Una lista bastante larga para ser una revisión menor




---


**ACTUALIZACIÓN 1-5-20**: Después de un periodo de versiones candidatas, finalmente los desarrolladores de esta herramienta de compatibilidad han lanzado una nueva revisión, llegando a la [5.0-7](https://github.com/ValveSoftware/Proton/releases/tag/proton-5.0-7) que incluye los siguientes y no menores cambios:


-**Street Fighter 5** y **Streets of Rage 4** ahora se pueden jugar.  
-Actualizado **DXVK a v1.6.1**.  
-**Correcciones para el sonido** en TrackMania Nations Forever, TrackMania Ultimate Forever y Zusi 3 Aerosoft.  
-Solucionado el bloqueo en Plebby Quest: The Crusades.  
-Se corrigió la conexión a Gearbox SHiFT en **Borderlands 3**.  
-**Grand Theft Auto 4** ahora es jugable.  
-**Actualizaciones a vkd3d** para mejorar la compatibilidad y el rendimiento de Direct3D 12.  
-Mejora la experiencia de depuración del desarrollador.


Como veis finalmente **Street Fighter V finalmente se puede jugar en Linux**, algo que había prometido Valve hace años cuando todo esto de Steam en Linux empezó, aunque con "ligeras variaciones". También hay que destacar la colaboración de [Dotemu](index.php/component/k2/tag/DotEmu) y [Lizardcube](index.php/component/k2/tag/Lizardcube) con Valve, que indirectamente ha perjudicado a [Ethan Lee](index.php/component/k2/tag/Ethan%20Lee), que al menos de momento se ha quedado sin poder portar **Streets of Rage 4** a pesar de usar este FNA.




---


**ACTUALIZACIÓN 15-4-20**: Ahora mismito nos acaba de llegar el aviso de que ha sido lanzada una nueva versión de Proton, concretamente la [5.0-6](https://github.com/ValveSoftware/Proton/releases/tag/proton-5.0-6) que incluye las siguientes novedades:  



- Arreglados los fallos del DRM de DOOM Eternal . Este juego requiere los últimos controladores de video.  
 - Mejoras en el rendimiento y los gráficos para Resident Evil 2 en los modos Direct3D 11 y 12.  
 - Arregladas las regresiones de Protón 5.0 en Rock of Ages, Dead Space y Elder Scrolls Online.  
 - Arreglados Fallout 3, Panzer Corps que se colgaban en el arranque.  
 - Arreglados los enlaces externos del navegador web en algunos juegos, incluyendo Football Manager 2020 y Age of Empires II: HD Edition.  
 - Mejorada la apariencia del Lanzador Rockstar.  
 - Ignoradas las tabletas Wacom que se presentan como joysticks.  
 - Arreglada la caída de DmC Devil May Cry con los controles con vibración.  
 - Arreglado el error de VR cuando los usuarios tienen un ajuste personalizado XDG_CONFIG_HOME.


Seguiremos atentos a la proxima versión




---


**ACTUALIZACIÓN 21-3-20**: Nueva vuelta de tuerca, que alcanza la revisión numero 5.0-5, que añade los [siguientes cambios](https://github.com/ValveSoftware/Proton/wiki/Changelog):


-**Arreglado el cierre de algunos juegos** en Proton 5.0-4.  
 -Corregido **error de red en "Granblue Fantasy: Versus"**.  
 -Soporte para los **últimos SDKs de OpenVR**.  
 -Añade soporte para las **nuevas extensiones de Vulkan** usadas por algunos títulos recientes.


En este caso, como podeis observar es una actualización menor, pero no un paso menos importante en la evolución de Proton.




---


**ACTUALIZACIÓN 11-3-20:** Una vez más Valve nos trae una nueva revisión de Proton, concretamente la **5.0-4**, que incluye una serie de [cambios de bastante calado](https://github.com/ValveSoftware/Proton/wiki/Changelog), pues afectan a juegos muy conocidos. Estos son:


-El **lanzador Origin** de EA ahora es funcional y **Jedi Fallen Order** es jugable.  
-**Se reparó el bloqueo** en el lanzamiento en **Grand Theft Auto V Online**.  
-Solucionados los **fallos de DRM de Denuvo** en **Just Cause 3 y Batman Arkham Knight**.  
-Actualizado a **DXVK a v1.5.5**.  
-**Mejora** el rendimiento al emular cambios de **resolución de pantalla**.  
-**Mejora** el rendimiento en **Monster Hunter World**.  
-Soluciona el **problema del cursor del mouse** sobre la pérdida de enfoque en **Ryse: Son of Rom**e.  
-**Mejora** los tiempos de **lanzamiento de los juegos**.


Como veis más de uno se va a poner muy contento con todas estas novedades. Ya podeis decirnos que tal os funciona en los comentarios.


 




---


**ACTUALIZACIÓN 22-2-20:** Nueva revisión de la versión 5 de Proton, y de nuevo con correcciones menores, aunque no por ello menos importantes. La [lista de cambios](https://github.com/ValveSoftware/Proton/releases/tag/proton-5.0-3) en esta ocasión es la siguiente:


-Soporte para el **modo Direct3D 12 en Metro Exodus.**  
 -Arreglado el cierre en el lanzamiento de **The Walking Dead: Saints and Sinners** y **eFootball PES 2020**.  
 -Arreglado **Automobilista** que no cargaba algunos "assets".  
 -Arreglada la regresión del ratón de "high polling rate".




---


**ACTUALIZACIÓN 13-2-20**: Acaba de publicarse una nueva revisión de esta versión , concretamente la [5.0-2](https://github.com/ValveSoftware/Proton/releases/tag/proton-5.0-2), que basicamente viene a corregir algunos de los bugs que presentaba la versión anterior, que como sabeis abría una nueva rama dentro de este proyecto. En esta ocasión encontraremos los siguientes cambios:


-Arregla los frecuentes "crasheos" introducidos en el Protón 5.0-1 relacionados con la reproducción de video y audio.  
 -Arregla el cierre del Planet Coaster en el lanzamiento.  
 -Arregla el problema gráfico de Subnautica.


Como veis son cambios menores pero que siempre se agradecen.




---


**NOTICIA ORIGINAL**: Después de una buena tempora exprimiendo al [versión 4.11](index.php/homepage/noticias-news/1083-proton-se-actualiza-a-la-version-4-11-con-una-cantidad-enorme-de-mejoras), hasta llegar incluso a las **12 revisiones**, finalmente y tal y como se esperaba, **la gente de Valve se ha sincronizado con sus socios de Wine actualizando su herramienta a la versión 5.0**. Gracias a esto, Proton incluye ahora montones y montones de cambios que bien seguro nos vendrán de maravilla para sacar el mayor partido posible a nuestra colección de juegos. Estos [cambios](https://github.com/ValveSoftware/Proton/wiki/Changelog#50-1) son los siguientes:


*-**Actualiza a Wine a la versión 5.0**. Desde el último gran lanzamiento de Protón, Wine ha visto **más de 3500 cambios**, que ahora están integrados en Protón. 207 parches de Protón 4.11 fueron o bien enviados por adelantado o ya no son necesarios.*  
 *-**Los juegos de Direct3D 9 ahora usarán DXVK para el renderizado por defecto**. Los usuarios sin soporte de Vulkan pueden volver al renderizador wined3d basado en OpenGL con [la opción de configuración](https://github.com/ValveSoftware/proton/#runtime-config-options) `PROTON_USE_WINED3D`.*  
 *-**Mejorada la integración del cliente Steam**. Esto hace que **se puedan jugar más juegos que usan Denuvo**, incluyendo Just Cause 3, Batman: Arkham Knight, Abzu, y más.*  
 *-**Los nuevos entornos de Proton reportarán una nueva versión del sistema operativo**, que algunos juegos nuevos requieren. Los entornos existentes no se cambiarán automáticamente.*  
 *-**Wine 5.0 incluye los inicios del soporte multi-monitor real**. Esperen grandes mejoras en esta área pronto.*  
 *-**Soporte de sonido envolvente mejorado** para los juegos más antiguos.*  
 *-**Actualizado DXVK a la v1.5.4.***  
 *-**Actualizado FAudio a la versión 20.02.***  
 *-Nota para los usuarios que construyen Proton: esta rama tiene nuevos submódulos, por favor asegúrese de `git submodule update --init`*


Como veis Proton es cada vez más complejo, y a medida que van cayendo las versiones más y más compatible, proporcionandonos a los jugadores que usamos GNU/Linux un aumento en la compatibilidad, y un mayor numero de opciones y posibilidades. Os recordamos que al cambiar la versión de la 4.11 a la 5.0, es necesario seleccionar en Steam los Parametros, y luego en la sección de Steam Play seleccionar la versión 5.0..... y sin más os dejo que estoy deseando probarlo, algo que debeis hacer vosotros también sin más dilación.

