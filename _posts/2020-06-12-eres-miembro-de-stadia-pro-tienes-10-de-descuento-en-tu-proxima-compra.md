---
author: Pato
category: Noticia
date: 2020-06-12 08:48:37
excerpt: "<p>Adem\xE1s, todos los nuevos usuarios que se registren recibir\xE1n un\
  \ mes gratis de Stadia Pro</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/ThisWeekonStadia0-Save10Off-final.webp
joomla_id: 1232
joomla_url: eres-miembro-de-stadia-pro-tienes-10-de-descuento-en-tu-proxima-compra
layout: post
tags:
- ofertas
- stadia
title: "\xBFEres miembro de Stadia Pro? tienes 10\u20AC de descuento en tu pr\xF3\
  xima compra"
---
Además, todos los nuevos usuarios que se registren recibirán un mes gratis de Stadia Pro


 


"***Si eres un suscriptor nuevo o actual de Stadia, o si alguna vez has sido miembro de Stadia Pro tendrás 10€ en tu próxima compra de un juego***". Así de claro comienza el post oficial de [Stadia](https://stadia.google.com/store) con el cual nos anuncian que la plataforma comienza a incentivar de forma directa la compra de cualquier juego en la plataforma.


Al igual que otras tiendas online (EGS por ejemplo) [Stadia](https://stadia.google.com/store) se apunta a la "moda de regalar" dinero a todos los suscriptores de forma que cualquier juego que compres, cualquiera tendrá este descuento.


Por otra parte, se acaba el periodo de dos meses gratis para los usuarios que se registraron en Abril, por lo que la plataforma pone en marcha una nueva promoción por la que **todos los nuevos usuarios registrados en la plataforma a partir de ahora tendrán un mes de Stadia Pro gratis**.


Por lo demás, el resto del post está dedicado a anunciar nuevas características del servicio para dispositivos móviles Android, de forma que ahora se añade soporte oficial para los teléfonos:


* OnePlus 5 y 5T
* OnePlus 6 y 6T
* OnePlus 7, 7 Pro, 7 Pro 5G, 7T, 7T Pro, 7T Pro 5G


También anuncian que la limitación de juego a ciertos dispositivos se ha acabado, y si bien la experiencia puede no ser óptima según el dispositivo, ahora ya es posible descargar la app y probar el servicio en dispositivos "no oficiales".


Por último, ya están implementados los controles táctiles en pantalla de forma que ya no es necesario emparejar un mando al teléfono o tableta, y además se ha añadido la posibilidad de **cambiar la resolución de juego en cada dispositivo** individualmente ya que con anterioridad si cambiabas la resolución esta se aplicaba a todos tus dispositivos de juego de manera global.


Recordar que **cualquier nuevo suscriptor de Stadia Pro tiene acceso directo a cinco nuevos juegos**: [SUPERHOT](https://stadia.com/link/qgnVvF8SBMCCiss5A), [Get Packed](https://stadia.com/link/iMWaGYqK37zx8Bf69), [Little Nightmares](https://stadia.com/link/WCnyoTe4EVLoJxBA7), [Power Rangers: Battle for the Grid](https://stadia.com/link/EMFMHaQfKcmEbrTj6), y [Panzer Dragoon: Remake](https://stadia.com/link/a8JPrwiQEFW3AoAp6). 


Tienes toda la información en el post oficial [en el blog de Stadia](https://community.stadia.com/t5/Stadia-Community-Blog/This-Week-on-Stadia-It-s-a-great-time-to-buy-the-game-you-want/ba-p/24303).


¿Que te parecen las promociones de Stadia? ¿Que piensas comprar con tus 10€ de regalo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

