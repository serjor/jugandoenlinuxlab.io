---
author: Serjor
category: Editorial
date: 2017-08-21 04:00:00
excerpt: "<p>Podr\xEDamos ver llegar algunos juegos que de otra manera se hubieran\
  \ quedado a las puertas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/66e8215c782a44521482ab938e79c393.webp
joomla_id: 441
joomla_url: ryan-c-gordon-se-ofrece-para-portar-juegos-de-manera-gratuita-a-las-desarrolladoras
layout: post
tags:
- icculus
- patreon
- ryan-c-gordon
title: Ryan C. Gordon se ofrece para portar juegos de manera gratuita a las desarrolladoras
---
Podríamos ver llegar algunos juegos que de otra manera se hubieran quedado a las puertas

Es lo que tiene el poder de Patreon, que a veces amaneces con correos como este:


![icculus patreon](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/icculus-patreon.webp)


Y es que Ryan Gordon, más conocido como [Icculus](https://twitter.com/icculus), ha anunciado en [su página de Patreon](https://www.patreon.com/icculus/posts) que se han alcanzado todos los objetivos de financiación que había marcado y que se pondrá a portar juegos para GNU/Linux que tenía en su lista de tareas pendientes a sabiendas que no van a dar rendimiento económico.


Pero lo mejor de todo no es eso, sino que pide a la comunidad que le pongamos en contacto con empresas desarrolladoras que están planteándose portar sus juegos a GNU/Linux pero que no se atreven a dar el paso, para poder hablar con ellas ya que se ofrece a hacer el port de manera gratuita.


Así que ya sabéis, si tenéis algún contacto en alguna empresa del sector y no saben si lanzarse a GNU/Linux, podéis pasarle el [enlace del post](https://www.patreon.com/posts/request-for-13971037). O directamente toca hacer copy&paste del link en todos los foros de steam donde la desarrolladora diga que el port a GNU/Linux solo se hará si hay algo de interés, quizás le pueda salir gratis.


Y tú, ¿ya sabes a quién le vas a decir que hable con Ryan para que le porte el juego? Coméntanos con quienes te pondrás en contacto en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

