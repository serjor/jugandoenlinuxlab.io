---
author: leillo1975
category: Podcast
date: 2020-04-21 08:54:11
excerpt: "<p>Impresiones sobre Stadia en Linux, The Witcher 3 y las \xFAltimas noticias\
  \ ocupar\xE1n nuestro tiempo.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Podcast/podcast_jel.webp
joomla_id: 1213
joomla_url: ya-esta-disponible-el-episodio-1-de-nuestro-podcast
layout: post
tags:
- artifact
- minetest
- stadia
- xplane-11
- podast
- the-witcher-3
- noticias
- f1-2020
title: "Ya est\xE1 disponible el Episodio 1 de nuestro Podcast"
---
Impresiones sobre Stadia en Linux, The Witcher 3 y las últimas noticias ocuparán nuestro tiempo.


 Después de la magnífica acogida que habeis dado a nuestro episodio piloto, el "[Episodio Zero](index.php/homepage/noticias-news/1195-jugando-en-linux-presenta-episodio-zero-el-primer-capitulo-de-nuestro-podcast)", ya teníamos ganas de lanzar este "primer" episodio en el que como podeis comprobar hemos pegado un salto de calidad considerable. Esto no se debe a nuestro buen hacer, ni a nuestra experiencia, sinó que es **gracias a la colaboración desinteresada de Jorge Lama** ( [@raivenra](https://twitter.com/raivenra)  ) que nos ha aportado su "granito" de arena y su paciencia en la producción de sonido para elaborar este nuevo podcast.


En este episodio repasaremos algunas de las **últimas noticias** en el panorama "gaming" de Linux, charlaremos largo y tendido sobre nuestras **impresiones tras probar el servicio de Stadia** en la nube, y nuestro colega [@ser_jor](https://twitter.com/ser_jor) nos hablará de las **virtudes de "The Witcher 3"**. Además durante el programa **habrá una sorpresa para el más rápido**.


Podeis escuchar el "Episodio 1" de nuestro podcast en [Ivoox](https://www.ivoox.com/50207417) y nuestro canal de [Youtube](https://youtu.be/D52nnBYIcDY). Como siempre estamos deseando conocer vuestras impresiones y opiniones sobre lo grabado en este podcast, por lo que agradeceremos cualquier comentario que hagais sobre él.

