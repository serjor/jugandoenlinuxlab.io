---
author: Pato
category: Aventuras
date: 2018-08-14 15:26:44
excerpt: <p>El juego incluye incluso la ROM para cartucho o emulador</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/67fbf663b0b6a7c227eb8b80a52fdec6.webp
joomla_id: 830
joomla_url: tanglewood-llega-a-linux-steamos-desde-megadrive-hoy-mismo
layout: post
tags:
- accion
- indie
- aventura
title: '''Tanglewood'' llega a Linux/SteamOS desde Megadrive hoy mismo'
---
El juego incluye incluso la ROM para cartucho o emulador

A veces no hace falta nada mas que ofrecer algo distinto y original para triunfar. Tanglewood es un juego **programado para ser lanzado en la consola Megadrive**, y como tal podemos decir que esto si es retro.


Matt Phillips lanzó una exitosa [campaña en Kickstarter](https://www.kickstarter.com/projects/63454976/tanglewood-an-original-game-for-the-sega-genesis-m) para poder llevar a cabo este juego y lanzarlo en la Sega Megadrive/Genesis en formato cartucho, y ya de paso en el resto de sistemas de PC. Con esto en mente, Tanglewood es un juego retro de verdad, con unos gráficos llamativos y una jugabilidad típica de la época de los 16 bits:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/e6NvgnMTNCo" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


*TANGLEWOOD es un nuevo y original juego de plataformas y puzzles lanzado originalmente para SEGA Mega Drive y Genesis, y ahora también para Windows, Mac, Linux, Everdrive y tus emuladores Mega Drive favoritos. Una verdadera experiencia desafiante de plataforma de 16 bits que hará que los fanáticos de la era de los juegos de oro se sientan como en casa.*


TANGLEWOOD está programado en lenguaje ensamblador 68000 puro, utilizando herramientas y procesos de desarrollo originales de la década de 1990. Incluye incluso una ROM que puedes cargar en tu cartucho o emulador preferido y podrás jugar como si estuvieras delante de la consola. También tienes la posibilidad de comprar un cartucho original a través de su [página web oficial](https://tanglewoodgame.com/index.html).


Si quieres saber más, o comprar Tanglewood para tu Megadr... err sistema Linux favorito, puedes visitar su [página de Steam](https://store.steampowered.com/app/837190/TANGLEWOOD/).

