---
author: Serjor
category: Puzzles
date: 2017-08-09 14:18:07
excerpt: "<p>Valve anuncia un nuevo juego durante el evento de Dota2 m\xE1s importante\
  \ del a\xF1o</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e9fe550ba3c0c201d69e3bc0a455b623.webp
joomla_id: 427
joomla_url: artifact-un-nuevo-juego-de-cartas-de-la-mano-de-valve
layout: post
tags:
- valve
- cartas
- artifact
title: Artifact, un nuevo juego de cartas, de la mano de Valve
---
Valve anuncia un nuevo juego durante el evento de Dota2 más importante del año

Interesante noticia la que nos hemos encontrado esta mañana revisando twitter, y es que vía ESL nos hemos enterado de que Valve ha presentando durante [The International](http://www.dota2.com/international/overview/) su futuro juego Artifact:



> 
> There's a new Valve game on the horizon!  
> Get hyped for [@PlayArtifact](https://twitter.com/PlayArtifact) - the Dota 2 Card Game! [#TI7](https://twitter.com/hashtag/TI7?src=hash) [pic.twitter.com/QWjPHqJqOK](https://t.co/QWjPHqJqOK)
> 
> 
> — ESL Dota2 (@ESLDota2) [9 de agosto de 2017](https://twitter.com/ESLDota2/status/895092377408483328)



Buscando por Internet no hemos visto la pregunta del millón, si es que vendrá a GNU/Linux, así que esperemos que desde Valve nos respondan al siguiente tweet:



> 
> [@PlayArtifact](https://twitter.com/PlayArtifact) [#Linux](https://twitter.com/hashtag/Linux?src=hash) version?
> 
> 
> — Ser Jor (@ser_jor) [9 de agosto de 2017](https://twitter.com/ser_jor/status/895264009380909056)



De buenas a primeras poco podemos decir de este juego, pero parece que será un juego de cartas a lo Hearthstone o Faeria emplazado en el universo de Dota2, y que su fecha de lanzamiento está prevista para el 2018. Eso sí, esperamos que venga con soporte para Vulkan desde el primer día, e imaginamos que con la inversión que está haciendo valve en el mundo de las VR, algo haya, aunque en un juego de cartas me cuesta ver cómo integrarlo.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/DA5mY8XqrHU" width="560"></iframe></div>


La verdad es que sorprende que Valve siga haciendo videojuegos, y aunque el tipo de juego no es de los que nos tiene acostumbrados, es de agradecer que sigan pensando que sí, así algún día nos traerán HL 3, Portal 3 o L4D 3 (tienen un verdadero problema con las terceras partes, la verdad)


Y tú, ¿le darás una oportunidad a este juego? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

