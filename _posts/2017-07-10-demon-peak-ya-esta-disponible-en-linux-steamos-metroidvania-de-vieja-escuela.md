---
author: Pato
category: "Acci\xF3n"
date: 2017-07-10 10:30:04
excerpt: "<p>El t\xEDtulo llega \"sin acceso temprano\" pero los desarrolladores advierten\
  \ que a\xFAn est\xE1n desarroll\xE1ndolo y pueden presentarse problemas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/13b5e0deaf19b06816d21e67ad4e211c.webp
joomla_id: 402
joomla_url: demon-peak-ya-esta-disponible-en-linux-steamos-metroidvania-de-vieja-escuela
layout: post
tags:
- accion
- indie
- plataformas
- aventura
- metroidvania
title: "'Demon Peak' ya est\xE1 disponible en Linux/SteamOS: Metroidvania de vieja\
  \ escuela"
---
El título llega "sin acceso temprano" pero los desarrolladores advierten que aún están desarrollándolo y pueden presentarse problemas

De nuevo nos llega otro llamativo juego de estilo "metroidvania" con un apartado artístico "pixel art" inspirado por los juegos de acción y plataformas de la vieja escuela. Demon Peak es un juego para un solo jugador que se enfoca en el combate cuerpo a cuerpo con movimientos y habilidades, y donde tendremos que explorar áreas donde encontraremos habilidades, poderes y jefes finales a los que derrotar.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/qV62zkw6Ff8" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Demon Peak superó una campaña en Greenlight donde por cierto no anunciaban versión para Linux, y aunque las reviews que está recibiendo son positivas los desarrolladores advierten en su [último comunicado sobre el estado del juego](http://steamcommunity.com/games/demonpeak/announcements/detail/1419047855390566748) que aún están trabajando en el, y que puede presentar problemas o falta de algunas características. El por qué no lo han lanzado entonces como "acceso temprano" es algo que muchos usuarios están preguntando.


Aún y con eso, en su página de Steam no tienen disponible demo alguna, pero sí en su [página de itch.io](https://demonpeak.itch.io/demonpeak) donde puedes descargarla y probar el juego a ver qué tal te funciona y si te gusta lo que ves.


Demon Peak no está disponible en español, pero si eso no es problema para ti, y dado el tipo de juego que es, lo tienes disponible en Steam rebajado un 20% por su lanzamiento hasta el próximo día 14:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/525380/" width="646"></iframe></div>


¿Qué te parece Demon Peak? ¿Piensas jugarlo? ¿Te gustan los juegos "Metroidvania"?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

