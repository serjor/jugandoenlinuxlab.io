---
author: Pato
category: Estrategia
date: 2020-09-02 16:42:36
excerpt: "<p>El juego de&nbsp;<span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x\
  \ r-bcqeeo r-qvutc0\">@MimimiProd&nbsp;</span>nos trae estrategia y acci\xF3n en\
  \ el salvaje Oeste</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DesperadosIII/desperados1.webp
joomla_id: 1253
joomla_url: desperados-iii-llega-a-linux-junto-a-su-ultimo-dlc-dinero-para-los-buitres
layout: post
tags:
- estrategia
- estrategia-en-tiempo-real
- desperados-iii
title: "Desperados III llega a Linux junto a su \xFAltimo DLC \"Dinero para los buitres\""
---
El juego de @MimimiProd nos trae estrategia y acción en el salvaje Oeste


Los aficionados a la estrategia y el salvaje Oeste hoy están de enhorabuena, pues el estudio germano **Mimimi Games**, que ya nos ha traído otros títulos como [The Last Tinker: City of Colours](https://store.steampowered.com/app/260160/The_Last_Tinker_City_of_Colors/?snr=1_1056_4_creator_1057&curator_clanid=32976195) o [Shadow Tactics: Blades of the Shogun](https://store.steampowered.com/app/418240/Shadow_Tactics_Blades_of_the_Shogun/?snr=1_1056_4_creator_1057&curator_clanid=32976195) acaba de anunciar la disponibilidad de su último juego "**Desperados III**" junto al lanzamiento de su último DLC para nuestro sistema favorito:




> 
> We've just added the first [#DLC](https://twitter.com/hashtag/DLC?src=hash&ref_src=twsrc%5Etfw) mission + another FREE content update featuring another Baron's Challenge as well as full [#Mac](https://twitter.com/hashtag/Mac?src=hash&ref_src=twsrc%5Etfw) & [#Linux](https://twitter.com/hashtag/Linux?src=hash&ref_src=twsrc%5Etfw) 🍏🐧 support!   
>   
> What a day for [#Desperados3](https://twitter.com/hashtag/Desperados3?src=hash&ref_src=twsrc%5Etfw) - yeeehaw! 🤠🏇♥️ <https://t.co/bKJu52MMfP>
> 
> 
> — Mimimi (@MimimiProd) [September 2, 2020](https://twitter.com/MimimiProd/status/1301150715797270528?ref_src=twsrc%5Etfw)



 **Desperados III** es un juego de estrategia y acción táctica ambientado en el lejano Oeste en el que tendremos que desarrollar nuestra historia de venganza, ya que se trata de la precuela del también disponible para Linux [Desperados: Wanted dead or alive](https://store.steampowered.com/app/260730/Desperados_Wanted_Dead_or_Alive/).


*En esta esperadísima precuela del clásico Desperados: Wanted Dead or Alive, John Cooper se alía con la novia a la fuga Kate, el sicario Doc McCoy, el cazador Héctor y la misteriosa mujer de Nueva Orleans Isabelle. Cooper se embarca en una misión de venganza que lleva al grupo por diferentes ciudades rurales, pantanos y riberas hasta un enfrentamiento final y apoteósico solo digno de las auténticas leyendas del salvaje oeste.*


Ahora, con la salida del DLC "Money for the vultures" tendremos que intentar hacernos con el tesoro de Vincent DeVitt y tratar de sobrevivir...


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/kR62B8F_Trk" width="560"></iframe></div>


Combina las habilidades especiales de tu equipo para superar los complicados desafíos a tu manera.  
  



* Juega con cinco personajes únicos provistos de habilidades especiales
* Disfruta de total libertad de decisiones con infinitas maneras de superar cada obstáculo
* Derrota a enormes grupos de enemigos con estrategia y ejecución cuidadosas
* Disfruta de escenarios clásicos del salvaje oeste como ciudades fronterizas, pantanos misteriosos, ciudades modernas en expansión y muchos más
* Tú decides: ataques no letales o mortales, armas sigilosas o implacables
* Ajusta el juego a tu estilo con las diferentes dificultades y repite los niveles para completar desafíos
* En el modo Enfrentamiento podrás pausar el juego cuando quieras y reaccionar a emboscadas y ataques por sorpresa


Si quieres saber más sobre Desperados III, puedes visitar su [página web](https://desperadosgame.com), o comprarlo en [Humble Bundle](https://www.humblebundle.com/store/desperados-iii?partner=jugandoenlinux) (enlace de afiliado) o en su [página de Steam](https://store.steampowered.com/app/610370/Desperados_III/).


¿Te embarcarás en esta aventura a través del salvaje Oeste? 


Cuéntamelo en los comentarios o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

