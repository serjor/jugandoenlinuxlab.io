---
author: leillo1975
category: Rol
date: 2020-12-18 09:19:02
excerpt: "<p>El juego de @inXile_ent ha visto finalmente la luz en nuestro sistema.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Wasteland3/WastelandLinuxRelease.webp
joomla_id: 1266
joomla_url: wasteland-3-finalmente-disponible-en-gnu-linux
layout: post
tags:
- rol
- postapocaliptico
- inxile-entertainment
- nativos
- wasteland-3
title: Wasteland 3 finalmente disponible en GNU/Linux
---
El juego de @inXile_ent ha visto finalmente la luz en nuestro sistema.


Los fanáticos de los **juegos de rol postapocalípticos** vuelven a estar de enhorabuena, pues la compañía fundada por [Brian Fargo](https://en.wikipedia.org/wiki/Brian_Fargo), [InXile Entertaiment](https://www.inxile-entertainment.com/), tras varios retrasos ha dado soporte para nuestro sistema operativo a su último juego. Wasteland 3 se esperaba para hace más de medio año, concretamente para el **19 de Mayo**, tal y como anunciamos en otro [artículo]({{ "/posts/wasteland-3-ya-tiene-fecha-de-salida-en-linux" | absolute_url }}) de nuestra web.


Nos enterábamos de la noticia gracias a un anuncio en [reddit](https://www.reddit.com/r/linux_gaming/comments/kf69cr/wasteland_3_now_available_for_mac_linux/) que enlazaba con una nota a [su web](https://www.inxile-entertainment.com/post/wasteland-3-now-available-for-mac-linux) donde se muestran los detalles de este lanzamiento. En ella nos informan que **el juego cooperativo aun presenta [problemas](https://inxile.zendesk.com/hc/en-us/articles/360054502611-Mac-and-Linux-Co-op-Connection-Information)** que serán solucionados a principios del próximo año. Por el momento, si compramos el juego en GOG no podremos hacer uso de este modo, y si lo hacemos en Steam solo podremos jugar con jugadores de Mac y Linux.


Para quien no conozca esta saga de juegos, Wasteland es el **precursor e inspiración de juegos como Fallout o el reciente [Atom RPG](index.php/component/search/?searchword=atom%20rpg&searchphrase=all&Itemid=828)**. La verdad es que con los tiempos que corren se agradece que los desarrolladores de un juego de este calado se molesten, aunque con retraso, en darnos soporte nativo. En Wasteland 3 encontraremos las siguientes características:


*"En Wasteland 3 estás al mando de un equipo de Rangers del Desierto, representantes de la ley en un mundo postnuclear que trabajan para reconstruir la sociedad de sus cenizas. Un siglo después del bombardeo, luchas fútilmente para salvar tu querida Arizona. El autoproclamado patriarca de Colorado se pone en contacto contigo por radio y promete ayudarte si haces un trabajo del que solo un forastero se puede encargar: rescatar su tierra de sus tres ambiciosos y sanguinarios hijos.*  
   
 *Tendrás que viajar desde el abrasador desierto a las montañas nevadas en una misión urgente para empezar desde cero: construir una nueva base, encontrar un vehículo para la nieve, entrenar nuevos reclutas y sobrevivir a las heladas estepas; todo mientras decides en quién puedes confiar en esta región destrozada por la corrupción, facciones enfrentadas, sectas de perturbados, bandas criminales y rivalidades fraternales. Fórjate una reputación tomando decisiones que tendrán un profundo impacto en Colorado, sus habitantes y tu historia. ¿Te convertirás en el salvador de Colorado o en su peor pesadilla?*  
   
 *Wasteland 3 es un juego RPG de inXile entertainment en el que formarás tu propio equipo para disfrutar de un exigente combate táctico por turnos y una historia profunda y repleta de giros y decisiones que te mantendrán enganchado, tanto si ya eres un entusiasta de Wasteland como si te acabas de iniciar en la saga. Crea tu propio equipo de hasta seis Rangers y personaliza sus habilidades para adecuarlas a tu estilo de juego. Recibirás incluso tu propio camión de batalla, que podrás modificar hasta convertirlo en una bestia de guerra a rebosar de armas para arrasar con tus enemigos."*


Si os interesa el juego podeis adquirirlo en la tienda de [Humble Bundle (enlace patrocinado)](https://www.humblebundle.com/store/wasteland-3?partner=jugandoenlinux), en [GOG.com](https://www.gog.com/game/wasteland_3) y por supuesto en [Steam](https://store.steampowered.com/app/719040/Wasteland_3/). Os dejamos con el trailer del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/gagEMFUtKlo" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


 


Como veis, un estupendo juego perfecto para regalar o regalaroslo estas navidades. ¿Conocías la saga de juegos Wasteland? ¿Qué opinión te merece esta última parte? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


