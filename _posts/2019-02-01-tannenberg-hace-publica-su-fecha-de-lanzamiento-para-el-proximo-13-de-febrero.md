---
author: Pato
category: "Acci\xF3n"
date: 2019-02-01 16:55:42
excerpt: <p>El juego de @WW1GameSeries es el sucesor de Verdun</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8314bd53da760011d5b996a85e1de2a1.webp
joomla_id: 963
joomla_url: tannenberg-hace-publica-su-fecha-de-lanzamiento-para-el-proximo-13-de-febrero
layout: post
tags:
- accion
- fps
- igm
title: "'Tannenberg' hace p\xFAblica su fecha de lanzamiento para el pr\xF3ximo 13\
  \ de Febrero"
---
El juego de @WW1GameSeries es el sucesor de Verdun

Buenas noticias para los que buscan acción en la Primera Guerra Mundial. '**Tannenberg**', el juego de Blackmill Games y sucesor de "Verdun" llegará a nuestro sistema favorito el próximo día 13 de Febrero, tal y como han anunciado en su [último comunicado](https://steamcommunity.com/gid/[g:1:28900041]/announcements/detail/1711833042452028990).


El juego de **acción en primera persona** está ambientado en la famosa batalla del mismo nombre de la Primera Gran Guerra y en sus localizaciones históricas, incluyendo bosques, lagos, colinas y vegetación.


*"Tannenberg es la última entrega en la Serie de Juegos WW1 que comenzó con el FPS Verdun, expandiendo el enfoque para abarcar el Frente Oriental. La guerra entre el Imperio Ruso y los Países Centroeuropeos ofrece una nueva experiencia para jugadores nuevos y veteranos por igual, con cinco escuadrones, más de 25 armas, cinco mapas abiertos que brindan libertad táctica a los jugadores y un modo de juego para 64 jugadores con soporte completo de bot IA para que puedas experimentar batallas épicas en cualquier momento!"*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/sE32n9kUddU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Características:


     - Ambiente auténtico WW1; Precisión en todo, desde mapas y armamento hasta uniformes.  
     - FPS basado en escuadrones tácticos; Juega como ruso, rumano, austrohúngaro, alemán y pronto tropas búlgaras.  
     - Majestuoso frente oriental; Grandes mapas cada uno con variantes climáticas de nieve a sol de verano.  
     - Modo de juego de maniobra de 64 jugadores; Captura sectores estratégicos en grandes batallas.  
     - Nunca luches solo; Bots IA para que puedas unirte a batallas épicas en cualquier momento.


Tannenberg no está disponible en español, pero si no es problema para ti, puedes ver toda la información en su [página web](https://www.ww1gameseries.com/tannenberg/), o comprarlo en [Humble Bundle](https://www.humblebundle.com/store/tannenberg?partner=jugandoenlinux) (enlace patrocinado) o en su [página de Steam](https://store.steampowered.com/app/633460/Tannenberg/)


¿Qué te parece la acción de Tannenberg? ¿Te embarcarás en una batalla de la 1GM?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux)

