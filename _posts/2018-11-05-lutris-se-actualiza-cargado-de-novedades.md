---
author: leillo1975
category: Software
date: 2018-11-05 09:29:48
excerpt: "<p>@LutrisGaming alcanza la versi\xF3n 0.4.22</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/31d056b7a668d052ce81de69c4a46262.webp
joomla_id: 890
joomla_url: lutris-se-actualiza-cargado-de-novedades
layout: post
tags:
- wine
- lutris
- gamemode
- dxvk
- proton
title: Lutris se actualiza cargado de novedades
---
@LutrisGaming alcanza la versión 0.4.22

A estas alturas seguro que la gran mayoría de vosotros conoce esta aplicación, pero si queda alguien por ahí que aun no conozca la excelencias de este magnífico programa vamos a hablaros un poco de él. [Lutris](https://lutris.net/) es un gestor abierto que p**ermite administrar tus juegos entre las diferentes plataformas y unificarlos todos en una misma aplicación.** Con él podras disponer en su interfaz de todos tus juegos tanto nativos como emulados (wine, mame, Dolphin, PCSX2.... ), y además configurarlos facilmente. Por poner un ejemplo, es una especie de [PlayOnLinux](https://www.playonlinux.com/es/), pero que no se queda solo en Wine.


Si bien permite todo esto que os acabo de comentar, obviamente, y teniendo en cuenta los tiempos que corren, con Lutris podrás sacarle todo el partido a Wine, facilitando enormemente la instalación de juegos gracias a sus **"installers" creados por la comunidad** que automatizan el proceso de forma que no haya que hacer casi nada más que sentarse a esperar. También podremos gracias a su interfaz configurar cada uno de nuestros juegos probando diferentes versiones de Wine (stagin, pba, esync...), así como activar facilmente DXVK y muchas cosas más.


En cuanto a la razón de esta noticia, es que hoy mismo se ha lanzado una [nueva versión](https://github.com/lutris/lutris/releases/tag/v0.4.22) que **incluye importantes mejoras y añadidos** que os pasamos a detallar:



> 
> -Usa lspci en lugar de xrandr para detectar tarjetas de vídeo  
> -Detecta si Vulkan es compatible con el sistema para juegos DXVK  
>  -Añade soporte experimental de "playtime"  
>  -Detecta Proton y lo añadie a las versiones de Wine  
>  -Corrige el "runtime" de ser descargado cuando no es necesario  
>  -Añade experimentalmente el icono de la bandeja con los últimos juegos jugados  
> -Añade soporte para Feral Gamemode  
>  -Evita que el monitor de procesos abandone los juegos prematuramente  
>  -Limpieza de código
> 
> 
> 


Como veis se trata de una actualización muy jugosa que los que somos usuarios de este magnífico software sabremos sacarle provecho. **Si quereis descargarlo** simplemente pinchad en [este enlace para poder instalarlo en vuestra distribución](https://lutris.net/downloads/). Nosotros desde aquí os animamos a que lo probeis y nos deis vuestra opinión sobre Lutris en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

