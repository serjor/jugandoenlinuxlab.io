---
author: leillo1975
category: "Acci\xF3n"
date: 2018-02-20 14:09:32
excerpt: <p>El veterano shooter se puede disfrutar gratuitamente.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f3051eb70b962b646ad926757115bce0.webp
joomla_id: 653
joomla_url: el-mod-dystopia-llega-finalmente-a-linux-steamos
layout: post
tags:
- mod
- dystopia
- source
- half-life-2
- puny-human
title: El mod Dystopia llega finalmente a Linux/SteamOS
---
El veterano shooter se puede disfrutar gratuitamente.

Hoy tenemos el día un tanto Ciberpunk. Esta mañana os hablamos del nuevo proyecto de CD Projekt Red ([Cyberpunk 2077](index.php/homepage/generos/rol/item/774-cyberpunk-2077-en-linux-si-por-favor)), y ahora de este ya experimentado **mod de Half Life 2**. Este juego dispone desde hace unos dias de soporte oficial añadiendo en [una de sus últimas actualizaciones](http://steamcommunity.com/games/dystopia/announcements/detail/2738622771821837383) un cliente para Linux. Para poder jugar a [Dystopia](http://www.dystopia-game.com/), unicamente tenemos que poseer una copia de Half Life 2. algo que seguramente la gran mayoría de vosotros teneis, pues se trata de un **mod completamente gratuito**. Este mod es desarrollado por [Puny Human](http://www.punyhumangames.com/), compañía que ha lanzado ya algunos juegos como [Galacide](http://www.galacide.com/) o [Blade Symphony](https://www.blade-symphony.com/), y colaborado en otros como [Wasteland 3](https://wasteland3.inxile-entertainment.com/) y [The Bards Tale IV](https://bardstale.inxile-entertainment.com/).


Los requisitos técnicos se supone que deben ser los mismos que Half Life 2, por lo que **el mod funcionará correctamente en casi cualquier máquina**, sea cual sea. En cuanto a lo que nos vamos a encontrar en Dystopia su descripción en Steam reza lo siguiente:



> 
> *Dystopia es una conversión total de Source Engine, en continuo desarrollo por el Team Dystopia, que pone al jugador en tensas situaciones de combate en un mundo de alta tecnología conectado en redes informáticas. Como los mercenarios Punk, o la fuerza de seguridad Corporativa, el jugador luchará en el mundo físico para ganar acceso, vía terminales, al ciberespacio.*  
>  *El ciberespacio es una representación tridimensional de la red mundial. Dentro del ciberespacio los jugadores lanzarán programas para hackear sistemas conectados al mundo físico mientras rechazan deckers enemigos y defienden sistemas críticos. El juego progresa por objetivos entrelazados, algunos se completan en el mundo físico o el ciberespacio, otros solo por una combinación bien coordinada de los dos.*  
>  *El jugador estará absorto en batallas llenas de acción, siendo un mercenario equipado hasta los dientes con lo último en tecnología militar, o un decker con rápidos reflejos corriendo para infiltrar un nodo en el ciberespacio. Solo con un hábil uso del arsenal de alta tecnología que disponemos y un inteligente juego en equipo, los jugadores podrán conectarse al ciberespacio y machacar a los enemigos.*  
>  *El Team Dystopia es un grupo de jugadores con talento y dedicación que están creando un juego lleno de acción multijugador. El juego hace uso de temas y conceptos del género ciberpunk e implementa ideas nacidas de años de experiencia en videojuegos. Con un trabajado sistema de juego, esperamos producir la próxima etapa en videojuegos.*
> 
> 
> *-Dos capas de juego entrelazadas; mundos conectados de realidad y ciberespacio.*
> 
> 
> *-Un mundo detallado y futurista.*
> 
> 
> *-Un mundo virtual o ciberespacio 3D.*
> 
> 
> *-Sistema de configuración único.*
> 
> 
> *-10 mapas de asalto.*
> 
> 
> *-4 mapas de Phistball.*
> 
> 
> *-12 poderosas armas.*
> 
> 
> *-14 implantes.*
> 
> 
> 


Por último, decir que en JugandoEnLinux estamos realizando en estos momentos una votación en nuestro grupo de [Telegram](https://t.me/jugandoenlinux) para decidir que juego jugaremos el viernes por la noche, como es habitual; y entre las opciones está este Mod, por lo que si os apetece jugarlo podeis participar aportando vuestro voto. Os dejamos con un trailer del juego para que tengais una idea más aproximada de lo que os vais a encontrar:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/McIMai3GDk8" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>



 Si quereis jugar a Dystopia podeis descargarlo desde Steam (necesario [Half Life 2](http://store.steampowered.com/app/220/HalfLife_2/)):


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/17580/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Estais dispuestos a echar unos tiros en Dystopia? Respóndenos en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

