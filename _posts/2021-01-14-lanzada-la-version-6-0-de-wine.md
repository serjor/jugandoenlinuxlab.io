---
author: leillo1975
category: Software
date: 2021-01-14 17:03:02
excerpt: "<p>Los desarrolladores de @<span class=\"highlight\">Wine</span>HQ culminan\
  \ un a\xF1o de trabajos.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/WineLogo.webp
joomla_id: 1269
joomla_url: lanzada-la-version-6-0-de-wine
layout: post
tags:
- wine
- vulkan
- open-source
- proton
- codeweavers
- wined3d
title: "Lanzada la versi\xF3n 6.0 de Wine"
---
Los desarrolladores de @WineHQ culminan un año de trabajos.


 Finalmente, la capa de compatibilidad más famosa entre los Linuxeros, y la base de la conocidas herramientas [Proton]({{ "/posts/steam-play-se-actualiza-con-proton-5-13-4" | absolute_url }}) y [Crossover](https://www.codeweavers.com/crossover), ya ha llegado en su **sexta versión** (después de unas cuantas versiones candidatas), y como cada principio de año ya está entre nosotros. Este fantástico software desarollado desde hace un montón de años por la compañía [Codeweavers](https://www.codeweavers.com/), ha experimentado durante el último año **numerosas mejoras** que ahora se aunan en esta **nueva versión estable**, y que servirá como **cimiento de las próximas revisiones**.


Entre las [novedades](https://www.winehq.org/announce/6.0) más importantes que podemos encontrar en esta v.6 están:


- **Los módulos centrales que ahora están en formato PE** (Portable Executable), lo que permitirá que "un número de esquemas de protección de copia que comprueban que los archivos DLL en el disco coinciden con el contenido de la memoria"


- **El backend de Vulkan para WineD3D comienza a funcionar en modo experimental**, sustituyendo el basado en OpenGL. En esta versión, el soporte de shaders en el renderizador de Vulkan se limita a los modelos de sombreadores 4 y 5. En la práctica, eso limita su utilidad a las aplicaciones Direct3D 10 y 11. El renderizador de Vulkan puede activarse configurando el registro de "renderizador" de Direct3D a "vulkan".


- **Mayor soporte para DirectShow y Media Foundation**, siendo este ahora mucho más completo que antes, y permitiendo que mucho más contenido como video y audio de juegos pueda ser reproducido sin problemas.


- La biblioteca del motor **XACT3** (Cross-platform Audio Creation) y las interfaces se implementan a través de **FAudio**.


- Mejor soporte para la opción de **multimonitor**, gracias a **XrandR 1.4**


- Soporte para **dispositivos de entrada en bruto** y  **controlador inicial de kernel USB** basado en la biblioteca LibUSB, para proporcionar acceso a los dispositivos USB.


- Se implementa la **historia de la posición del ratón**, para los juegos que quieren posiciones más precisas del ratón. Se implementan notificaciones de dispositivos **Plug & Play**.


También podeis encontrar otras mejoras como el soporte para la API de WebSocket, soporte inicial para ARM64 en macOS, y varias mejoras de herramientas para desarrolladores. Pero si quereis entrar en detalle es mejor que consulteis [aquí la lista completa de cambios](https://www.winehq.org/announce/6.0).


Es probable que si sois de los que usais la versión experimental de Wine no noteis grandes cambios, pero por el contrario si sois usuarios de la versión estable (5.0), noteis una gran diferencia y mayor compatibilidad que con esta última. Por supuesto estaría bien tener vuestro feedback sobre lo que os parece esta versión de Wine y el proyecto en general, por lo que si quereis podeis dejar un mensaje en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

