---
author: Pato
category: Aventuras
date: 2018-12-28 10:18:15
excerpt: "<p>Los responsables del juego financiado en Kickstarter no dan opci\xF3\
  n a devolver la inversi\xF3n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1989b63796ddb198c6c6f884b17b0892.webp
joomla_id: 942
joomla_url: cancelada-la-version-linux-de-bloodstained-ritual-of-the-night
layout: post
tags:
- accion
- plataformas
- aventura
- roguevania
title: "Cancelada la versi\xF3n Linux de 'Bloodstained: Ritual of the Night'"
---
Los responsables del juego financiado en Kickstarter no dan opción a devolver la inversión

Cuando en Mayo de 2015 Koji Igarashi anunció que iba a desarrollar un nuevo juego de tipo roguevania, género que casi inventó el con juegos como Castlevania: Sinfony of the Night, y que además iba a dar soporte para Linux algunos no nos lo podíamos creer.


Como seguidor de la saga (tengo casi todo lo que salió de Mercury Steam para PC) era un sueño hecho realidad, sobre todo por que el anuncio de la campaña en Kickstarter para financiar parte del desarrollo daba a entender que la cosa iba muy en serio.


Del anuncio de 'Bloodstained: Ritual of the Night' hace ya casi 4 años, durante los cuales han sucedido multitud de "catastróficas desdichas", la última de las cuales nos la anunciaron ayer con **la cancelación de las versiones para Linux y Mac,** incumpliendo así su promesa inicial de soporte ya que ni siquiera supeditaban el desarrollo para Linux a ningún objetivo concreto ya fuera económico o de otra clase.


Resumiendo un poco, tras superar con gran éxito la campaña de financiación (superaron los 4.000.000€ solo en Kickstarter cuando solo pedían 500.000€) y añadir fondos de otras campañas en otras plataformas, el proyecto recaudó mas de 5.500.000€ tras lo cual comenzó un desarrollo que en teoría iba a durar un par de años tras los cuales tendríamos el juego entre las manos. No solo no tuvimos nada, si no que [en 2016 Iga anunció](https://www.kickstarter.com/projects/iga/bloodstained-ritual-of-the-night/posts/1666487) que el proyecto se iba a retrasar a la primera mitad de 2018 para cumplir con los estándares de calidad que quería para el juego.


El desarrollo en un principio vibrante, fue perdiendo fuelle, o al menos es la sensación que teníamos los que seguimos el proyecto de cerca. En Octubre de 2016 [Iga anunció un acuerdo](https://www.kickstarter.com/projects/iga/bloodstained-ritual-of-the-night/posts/1698207) de publicación con 505 Games, lo que ya daba a entender que algo estaba pasando dentro del estudio de desarrollo. ¿Qué necesidad tienes de que entre una nueva editora cuando has recaudado semejante cantidad de dinero? ¿Y todo para un juego que en principio no iba a necesitar de tecnologías punteras o un desarrollo tan caro?


Es cierto que a esas alturas ya habían lanzado una demo para Windows, si bien ésta había dejado a mas de uno con mal sabor de boca. No estaba cumpliendo las expectativas, o al menos es lo que los usuarios daban a entender en los foros. Tras esto fueron sucediéndose acontecimientos, como las [respuestas](https://www.kickstarter.com/projects/iga/bloodstained-ritual-of-the-night/posts/1965881) a ciertas preguntas que daban a entender que el juego podría tener futuros DLCs, la implicación de 505 Games y con el paso del tiempo la cancelación del juego para varias plataformas anunciadas inicialmente como Vita o WiiU dado que el ciclo de vida de esas consolas se acabó en el transcurso del desarrollo del juego y la incorporación de Switch como nueva incorporación. Importante decir que **se dio la opción de cambiar la plataforma o pedir la devolución del dinero a los afectados**. De los sistemas iniciales de desarrollo quedaron Pc (Windows, Mac, Linux) PS4 y XBOX One.


Tras incumplir nuevamente los plazos, [en Agosto Iga volvió a anunciar](https://www.kickstarter.com/projects/iga/bloodstained-ritual-of-the-night/posts/2266944) un retraso en el lanzamiento del juego, que se iba a 2019 y ahora [en su último comunicado](https://www.kickstarter.com/projects/iga/bloodstained-ritual-of-the-night/posts/2368304) nos dice que **las versiones de Linux y Mac se cancelan**. ¿Los motivos? Supuestos problemas con el soporte de cierto "middleware" que no especifica, y las dificultades de desarrollar el apartado online para estos sistemas. Es difícil de creer. A finales de 2018.


Además, tenemos el agravante de que, a **diferencia de lo ocurrido con Vita o WiiU** **ni siquiera da la opción a los que aportaron fondos al proyecto por la promesa de soporte en Linux de pedir el reembolso de la inversión**, tan solo se puede pedir el cambio a otra plataforma soportada. A partir de aquí, surgen aún mas dudas respecto al juego:


¿Como pensaban cumplir la promesa de soporte en nuestro sistema allá por 2015 cuando lanzaron la campaña de financiación? ¿Tenían idea de lo que estaban haciendo, alguna planificación del desarrollo o solo pensaban en coger el dinero y luego ya veremos? Actualmente, el juego sigue en desarrollo para Windows, PS4, Xbox y Switch (y que no se caiga alguna de estas plataformas) ¿Qué pasa con los usuarios que no tienen ninguno de estos sistemas? ¿Como piensa Iga/505 Games que este tipo de anuncios no afecte a la reputación del juego? ¿serán capaces de cumplir las expectativas y los "altos estándares de calidad" que supuestamente persiguen retraso tras retraso? ¿Se retrasará nuevamente el juego, o se lanzará en un estado cuanto menos lamentable? (muchos usuarios comentan que este desarrollo se parece sospechosamente al malogrado **Mighty Nº9**, aunque al menos éste último si que cumplió su promesa y lo podemos jugar en Linux).


Sinceramente, este tipo de jugadas no hacen si no reafirmar la postura que algunos mantenemos de que **no es recomendable financiar proyectos bajo la promesa de soporte en nuestro sistema favorito**, aunque personalmente rompí esta regla con Bloodstained bajo la ingenua creencia de que Igarashi cumpliría y podría jugar un juego suyo con soporte nativo en Linux. Por mi parte, y si al final no es posible pedir la devolución de ningún modo espero que al menos el juego sea jugable bajo Steam Link/Proton. De lo contrario, de decepcionado pasaré oficialmente al estado ingenuo. Al menos nos queda el derecho a la pataleta y denunciar la situación.


Así no, Iga. Así no.


PD: Si eres de los afectados, puedes mandar un correo a [bloodstained@fangamer.com](mailto:bloodstained@fangamer.com) y pedir el cambio de plataforma, o intentar pedir un reembolso.


Cuéntame lo que piensas en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

