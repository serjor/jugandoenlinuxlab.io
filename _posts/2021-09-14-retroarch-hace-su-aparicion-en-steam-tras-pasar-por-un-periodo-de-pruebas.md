---
author: Pato
category: Software
date: 2021-09-14 17:17:03
excerpt: "<p>Ya puedes disfrutar de tus emuladores y otro software desde esta plataforma</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/retroarch/Retroarch.webp
joomla_id: 1358
joomla_url: retroarch-hace-su-aparicion-en-steam-tras-pasar-por-un-periodo-de-pruebas
layout: post
tags:
- emulador
- retro
- retroarch
title: "Retroarch hace su aparici\xF3n en Steam tras pasar por un periodo de \"pruebas\""
---
Ya puedes disfrutar de tus emuladores y otro software desde esta plataforma


Desde hace casi un año **Retroarch** ha estado en Steam, si bien en "modo playtest", pero ahora parece que se hace oficial. Esta mañana nos ha saltado aviso de que estaba disponible, y efectivamente la plataforma ya está de manera definitiva en Steam.


**¿Qué es Retroarch?**


*"es una plataforma y un «frontend» multiplataforma de código abierto para emuladores, motores de juegos, reproductores de medios y otras aplicaciones."*


Es decir, permite ejecutar distintos tipos de juegos clásicos en todo tipo de ordenadores y plataformas, haciendo uso de la API Libretro y desde una única interfaz. el proyecto está muy vivo y en constante evolución y actualización añadiendo gran cantidad de características, como:


* Soporte para mandos
* Multitud de plataformas
* Logros y juego en red
* Personalización de gran cantidad de parámetros
* Descarga de distintos módulos
* Ejecución de juegos desde discos originales (próximamente)
* Procesado de shaders, fotogramas, latencia etc...


Como muestra, puedes ver los cambios introducidos en la última versión [en este enlace](https://www.libretro.com/index.php/retroarch-1-9-9-released/).


Hasta ahora Retroarch había estado disponible de forma oficial desde su [página web](https://www.retroarch.com/?page=interface), pero ahora también lo tendremos disponible a través de su página de Steam.


¿Alguien dijo "a tiempo para Steam Deck"?


Si quieres saber más, puedes visitar Retroarch en [Steam en este enlace](https://store.steampowered.com/app/1118310/RetroArch/).

