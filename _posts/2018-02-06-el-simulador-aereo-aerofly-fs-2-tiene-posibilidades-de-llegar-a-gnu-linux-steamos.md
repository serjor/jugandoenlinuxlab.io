---
author: Pato
category: "Simulaci\xF3n"
date: 2018-02-06 12:52:18
excerpt: "<p>La compa\xF1\xEDa IPAC public\xF3 un anuncio buscando Testers en Octubre</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c19239c5413540b5ee1f19187e11115e.webp
joomla_id: 636
joomla_url: el-simulador-aereo-aerofly-fs-2-tiene-posibilidades-de-llegar-a-gnu-linux-steamos
layout: post
tags:
- simulador
- realista
- mundo-abierto
- simulacion
title: "El simulador a\xE9reo 'Aerofly FS 2' tiene posibilidades de llegar a GNU-Linux/SteamOS"
---
La compañía IPAC publicó un anuncio buscando Testers en Octubre

Como dice el dicho "¡Qué tiempos para estar vivo!". Nos llegan noticias gracias a [gamingonlinux.com](https://www.gamingonlinux.com/articles/aerofly-fs-2-flight-simulator-is-coming-to-linux-developer-needs-testers.11176) de que otro simulador, 'Aerofly FS 2' tiene posibilidades de llegar a Linux. El caso es que la compañía detrás del título "IPAC" publicó un post en los foros de Steam el pasado mes de Octubre preguntando por un par de testers para la versión de Linux de este simulador aéreo, que por cierto y a tenor de un anuncio reciente ya dispone de una versión en MacOS y soporte para la Realidad Virtual.


El caso es que este post ha pasado inadvertido hasta que alguien ha dado el aviso y Liam lo ha hecho público, tras lo cual no se han hecho esperar las peticiones para ser beta-tester del título en cuestión incluyéndome, cómo no a mi mismo ;-) aunque debido al tiempo transcurrido desde el anuncio no sabemos si en su momento ya consiguieron cubrir las plazas, dejaron de lado la idea del port debido a la ausencia de respuesta o aún esperan a conseguir a alguien para esa tarea.


Como gran aficionado a la simulación aérea (aún tengo pendiente el hacer algún directo y algún vídeo gameplay de XPlane y War Thunder, cosa que algún día cumpliré) no puedo más que alegrarme por ver que hay interés en traer más títulos de este tipo a GNU-Linux, y más teniendo en cuenta lo pequeño que es el nicho de la simulación aérea en nuestro sistema favorito.


Si tienes interés en participar o mostrar tu apoyo al estudio para que traigan este 'Aerofly FS 2' a Linux/SteamOS, puedes hacerlo [en este hilo](http://steamcommunity.com/app/434030/discussions/0/1483232961039269518/?tscn=1517936844) del foro en Steam.


Sinopsis de Steam:


*"'Aerofly FS 2' [[web oficial](https://www.aerofly.com/index.html)] te permite explorar el mundo de la aviación en una calidad nunca antes vista. Vuela una gran selección de aviones altamente detallados con cabinas 3D interactivas totalmente animadas.*


*'Aerofly FS 2' es un simulador de vuelo de nueva generación abierto a añadidos y tiene un alto valor en físicas de vuelo realistas, aviones recreados con un alto nivel de detalle y bellos escenarios fotorealistas. En Aerofly FS 2 se incluye una cobertura de datos de elevación e imágenes aéreas. Al mismo tiempo, Aerofly FS2 tiene una interfaz de usuario intuitiva y no requiere virtualmente ningún tiempo de entrenamiento.*


*Toma tu asiento en la cabina y disfruta volando sobre el famoso puente Golden Gate o visita la isla de Alcatraz. Aprende las bases de vuelo en tu escuela de vuelo o salta directamente al asiento del Capitán y pilota el majestuoso 747 en uno de los aeropuertos con más tráfico del mundo."*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/cKgH_8JxFz0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Hay que tener en cuenta que 'Aerofly FS 2' aunque ya está a la venta oficialmente [en su página de Steam](http://store.steampowered.com/app/434030/Aerofly_FS_2_Flight_Simulator/) aún sigue recibiendo mejoras, por lo que es de esperar que el título vaya mejorando y añadiendo contenido con el tiempo, y también esperemos que al final consiga llegar a nuestro sistema favorito. Por supuesto os mantendremos informados de las novedades al respecto.


¿Qué te parece este 'Aerofly FS 2'? ¿Te gustaría volar con alguno de sus aviones desde tu sistema Linux?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

