---
author: leillo1975
category: Carreras
date: 2017-07-14 04:00:00
excerpt: <p>Un desarrollador lo ha confirmado en los foros de Steam</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/27dd7cddd3c2da4d8b388b00e3c63c22.webp
joomla_id: 407
joomla_url: el-port-de-gas-guzzlers-extreme-ha-sido-cancelado
layout: post
tags:
- cancelacion
- gas-guzzlers-extreme
- port
title: El port de Gas Guzzlers Extreme ha sido cancelado
---
Un desarrollador lo ha confirmado en los foros de Steam

Después de mucho tiempo sin noticias por parte de los desarrolladores del juego, tenemos que dar una mala noticia.  La desarrolladora [Gamepires](http://www.gamepires.com/) del arcade de carreras [Gas Guzzlers Extreme](http://store.steampowered.com/app/243800/Gas_Guzzlers_Extreme/)[,](http://www.gamepires.com/) había anunciado hace más de 3 años su intención de [portar el juego a nuestro sistema junto con dos nuevas DLC](http://steamcommunity.com/app/243800/discussions/0/846965705280714470/?ctp=2#c540736145596778775).


 


Finalmente, tras una desesperante espera [acaban de anunciar que el juego](http://steamcommunity.com/app/243800/discussions/0/846965705280714470/?ctp=10#c1457328927834747319) no recibirá el soporte prometido y no tendremos la oportunidad de disfrutar este título. Concretamente han dicho que *"Desafortunadamente el juego ha sido cancelado de nuevo, el juego no ha funcionado, y no puedo hablar sobre los detalles. El port se suponía que sería portado por otro equipo".* Como veis una auténtica pena, porque el juego parecía bastante divertido


 


Gas Guzzlers Extreme es como antes dije un arcade de carreras donde podremos luchar con otros competidores con variadas armas, powerups, mejoras, así como una buena ristra de coches. Un título con unas valoraciones bastante positivas teniendo en cuenta su ajustado precio. Esperemos que en un futuro no tengamos que asistir a más situaciones como esta con respecto a promesas de soporte. Os dejo con un video del juego para que os hagais a una idea:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/P-Qq74Kbi7A" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


¿Qué opinais de esta cancelación? Podeis dejar vuestra opinión en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

