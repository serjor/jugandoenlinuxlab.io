---
author: Pato
category: "Acci\xF3n"
date: 2020-03-06 23:02:42
excerpt: "<p>Vuelve Gordon Freeman en todo su esplendor</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/BlackMesa/BlackMesa10.webp
joomla_id: 1183
joomla_url: ya-podemos-disfrutar-en-linux-black-mesa-1-0-el-remake-de-half-life-que-estabamos-esperando
layout: post
tags:
- accion
- indie
- aventura
- fps
- shooter
title: "Ya podemos disfrutar en Linux Black Mesa 1.0, el remake de Half Life que est\xE1\
  bamos esperando"
---
Vuelve Gordon Freeman en todo su esplendor


Hace unos minutos [nos ha llegado la noticia](https://store.steampowered.com/app/362890/Black_Mesa/) de que ya está disponible la versión 1.0 de **Black Mesa**, el remake de Half Life hecho por fans con el beneplácito de Valve. Y la verdad es que la espera parece que ha merecido la pena.


Tal y como reza en su [página web oficial](https://www.crowbarcollective.com/games/black-mesa), Black Mesa viene a ser una especie de "remaster" que pone al día uno de los juegos clásicos con mejor valoración de todos los tiempos. Y por lo que podemos decir, el objetivo se ha cumplido con creces.


*Eres Gordon Freeman, un físico teórico en el Centro de Investigación Black Mesa. Cuando un experimento rutinario sale terriblemente mal, debes abrirte paso a través de una invasión alienígena interdimensional y un equipo de limpieza militar sediento de sangre para salvar al equipo científico ... ¡y al mundo!*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/YkBrNGeFwoU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Entre las características destacadas, tenemos diecinueve capítulos de combates a través de laboratorios de alto secreto, paisajes desérticos ásperos, escabulléndose por ferrocarriles abandonados y saltando a través de distintas dimensiones.


Gráficos y efectos alucinantes, nunca antes vistos en Source Engine.


Enfréntate a un ejército de enemigos clásicos, actualizado con nuevas características y una IA mejorada.  



Maneja un arsenal militar, prototipos científicos y la icónica palanca a través de entornos increíblemente detallados.


Juega en solitario o con el modo multijugador junto a tus amigos en diez mapas icónicos del universo Half-Life, que incluyen:


Bounce  
Gasworks  
Stalkyard  
Undertow  
Crossfire


Crea tus propios mods, modelos y mapas con el Black Mesa Source SDK y compártelos con la comunidad en Steam Workshop.


En cuanto a los requisitos, son:


**MÍNIMO:**


+ **SO:** Ubuntu 14.0 o superior
+ **Procesador:** 2.6 procesador Dual Core  o superior
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** 2 GB Tarjeta dedicada o superior
+ **Red:** Conexión de banda ancha a Internet
+ **Almacenamiento:** 20 GB de espacio disponible
+ **Notas adicionales:**  Nouveau display driver No soportado


**RECOMENDADO:**


+ **SO:** Ubuntu 14.0 o superior
+ **Procesador:** 3.2 Procesador Quad Core o Superior
+ **Memoria:** 6 GB de RAM
+ **Gráficos:** 3 GB Tarjeta dedicada o superior
+ **Red:** Conexión de banda ancha a Internet
+ **Almacenamiento:** 20 GB de espacio disponible
+ **Notas adicionales:** Nouveau display driver no soportado


Si quieres saber más, o comprar Black Mesa lo tienes disponible en su [página de Steam](https://store.steampowered.com/app/362890/Black_Mesa/) a tan solo 17,99€.


¿Qué te parece el remake Black Mesa? ¿Te volverás a meter en la piel de Gordon Freeman?


Cuentamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

