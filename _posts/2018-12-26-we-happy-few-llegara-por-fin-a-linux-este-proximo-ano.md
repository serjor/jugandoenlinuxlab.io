---
author: Pato
category: "Acci\xF3n"
date: 2018-12-26 11:31:53
excerpt: "<p>@CompulsionGames da nuevos detalles sobre la versi\xF3n para Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4f4d9fa294e26f44f90908a15cafba41.webp
joomla_id: 938
joomla_url: we-happy-few-llegara-por-fin-a-linux-este-proximo-ano
layout: post
tags:
- accion
- terror
- supervivencia
- mundo-abierto
- compulsion-games
- gearbox
title: "'We Happy Few' llegar\xE1 por fin a Linux este pr\xF3ximo a\xF1o (ACTUALIZADO)"
---
@CompulsionGames da nuevos detalles sobre la versión para Linux

**ACTUALIZADO 25-1-19:** Con motivo de la salida de su [nueva versión 1.6 para Windows](https://steamcommunity.com/games/320240/announcements/), los desarrolladores de "We Happy Few" han comunicado entre los detalles de este nuevo parche que **continuan trabajando en la versión de Linux y Mac**. Tal y como prometieron en su campaña de Kickstarter, Compulsion Games ha vuelto a insistir en que junto con el modo Sandbox tienen **ya una versión de Linux que estará lista para ser probada este nuevo año**.  Esta **incluirá por supuesto todas las caracteristicas existentes hasta la fecha**, como es de esperar. Como veis esto no difiere nada de la noticia original, pero nos hace confiar en que no se han olvidado de nosotros y que van a cumplir con su palabra a pesar de su reciente adquisición por parte de Microsoft.


Desde JugandoEnLinux.com estaremos atentos para ofreceros "la noticia" de la salida de tan esperado juego, aunque sea en estado Beta, tan pronto como se produzca.




---


**NOTICIA ORIGINAL:** o hace mucho que [hablábamos](index.php/homepage/noticias/39-noticia/1020-microsoft-compra-obsidian-entertainment-e-inxile-entertainment) de la compra de Gearbox por parte de Microsoft Studios, y las repercusiones que esto podría acarrear respecto a los desarrollos para Linux que esperamos de este y otros estudios pero parece que tanto Gearbox como Compulsion Games piensan cumplir con su promesa de traernos "We Happy Few" a nuestro sistema favorito.


Gracias a [gamingonlinux.com](https://www.gamingonlinux.com/articles/compulsion-games-confirm-a-linux-version-of-we-happy-few-is-coming-next-year.13227) nos enteramos que en el último comunicado del año, **Compulsion Games** habla expresamente de que ya tienen un prototipo del juego corriendo en Linux y que este próximo año lo lanzarán con el objetivo de pulir la versión para nuestro sistema.


*We Happy few es un juego de acción y aventura ambientado en una Inglaterra alternativa de los años 60, en la que encontrarás una sociedad retrofuturística dominada por el consumo de antidepresivos. Escóndete, lucha y encuentra una manera de salir de este mundo delirante.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/UGRHLmBNO_w" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Puedes ver el [comunicado completo](https://steamcommunity.com/games/320240/announcements/detail/1703948570352656766) (en inglés) en este enlace.


Si quieres saber mas sobre We Happy Few puedes visitar su [página web oficial](http://www.wehappyfewgame.com/) o su [página de Steam](https://store.steampowered.com/app/320240/We_Happy_Few/).


¿Qué te parece We Happy Few? ¿Piensas jugarlo cuando salga en Linux?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

