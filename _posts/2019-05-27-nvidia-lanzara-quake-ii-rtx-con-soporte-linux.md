---
author: leillo1975
category: "Acci\xF3n"
date: 2019-05-27 10:27:26
excerpt: "<p>El m\xEDtico juego de ID Software ya se puede descargar gratuitamente\
  \ con soporte RTX</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8cba5d214c5f756cb3dfba3d009c9a8f.webp
joomla_id: 1049
joomla_url: nvidia-lanzara-quake-ii-rtx-con-soporte-linux
layout: post
tags:
- vulkan
- nvidia
- id-software
- rtx
- quake
title: "Nvidia lanzar\xE1 \"Quake II RTX\" con soporte Linux (ACTUALIZADO)"
---
El mítico juego de ID Software ya se puede descargar gratuitamente con soporte RTX

**ACTUALIZADO 6-6-19**: Finalmente Nvidia [acaba de lanzar](https://www.nvidia.com/en-us/geforce/news/quake-ii-rtx-ray-traced-remaster-out-now-for-free/) el juego tal y como habían dicho hace poco más de una semana. Tal y como podemos ver en el tweet del anuncio:



> 
> ¡Quake II RTX ya está disponible!  
>   
> Disfruta de este clásico que vuelve a la vida con sombras, reflejos e iluminación global por ray tracing en tiempo real. ?  
>   
> ➡️ Más info: <https://t.co/zerkcyVUiJ>   
> ➡️ Steam: <https://t.co/5OJrfqt3rR> [pic.twitter.com/1BiqZQgZgN](https://t.co/1BiqZQgZgN)
> 
> 
> — NVIDIA GeForce ES (@NVIDIAGeForceES) [June 6, 2019](https://twitter.com/NVIDIAGeForceES/status/1136618943169355777?ref_src=twsrc%5Etfw)


  





Como veis es posible decargarlo desde [Steam](https://store.steampowered.com/app/1089130/Quake_II_RTX/), pero también desde el sitio web de [Nvidia](https://www.nvidia.com/es-es/geforce/campaigns/quake-II-rtx/). También está disponible el [código fuente en Github](https://github.com/NVIDIA/Q2RTX) por si se quiere implementar RTX en mods o juegos basados en el motor de Quake 2. Ahora solo queda disfrutarlo, pero para eso ya sabeis, necesitais una gráfica Nvidia que soporte la tecnología RTX....




---


**NOTICIA ORIGINAL**: Los viejos tiempos....... vuelven!!! Pedazo partidas nos echábamos en el ciber hace ya más de 20 años a este **clásico entre los clásicos**, y que divertido era (y es). Trás una fantástica y aterradora primera parte, John Carmack y sus huestes volvían a la carga con este FPS que hacía las delicias de los muchos amantes de este género que tan de moda estaba a finales de los 90. Según el tweet que hace un par de horitas publicaban desde Nvidia, el próximo **6 de Junio** podremos disfrutar de este título con aspecto renovado y aprovechando esa tecnología que tanto ha dado que hablar en los últimos tiempos, el **Ray Tracing**. Aquí teneis el tweet:



> 
> ¡El remaster de Quake II con RTX se lanza el 6 de junio! ?  
>   
> Más info ➡️ <https://t.co/deotC9fcxW> [pic.twitter.com/0LvF3dndqA](https://t.co/0LvF3dndqA)
> 
> 
> — NVIDIA GeForce ES (@NVIDIAGeForceES) [May 27, 2019](https://twitter.com/NVIDIAGeForceES/status/1132919583541792768?ref_src=twsrc%5Etfw)







Pues sin más vamos a ir a la chicha del asunto, y es que si recordais, **hace unos meses se publicaba un video del Quake 2 con Ray Tracing**, y lo que en principio parecía que iba a ser una demo interna se ha convertido en una realidad que estará disponible para todo el publico proximamente. Eso si, necesitarás tener un hardware adecuado, con soporte para Ray Tracing, por lo que necesitarás **como mínimo disponer de una RTX-2060** para poder usarlo. **Podremos jugar de forma gratuita los tres primeros niveles**, pero si tenemos una copia del juego, cosa que estoy seguro que muchísimos de vosotros cumplis, tendremos el juego completo con esta nueva tecnología. Además de esto **Nvidia dejará el código en Github para facilitar a los modder adaptar sus creaciones al trazado de rayos**. La lista de mejoras que tendrá esta nueva versión es enorme tal y como podeis ver:


*-Renderización mejorada de la iluminación global, con tres preselecciones de calidad seleccionables, incluyendo un "two-bounce" GI.*  
 *-Soporte multijugador*  
 *-Opciones de hora del día que cambian radicalmente la apariencia de algunos niveles*  
 *-Nuevos modelos de armas y texturas*  
 *-Nuevos entornos dinámicos (superficie y espacio Stroggos)*  
 *-Mejor dispersión atmosférica basada físicamente, incluyendo ajustes para el cielo de Stroggos*  
 *-Reflexión en tiempo real del reproductor y del modelo de arma sobre superficies de agua y cristal, y sombras del modelo de reproductor, para los propietarios del juego completo (la versión original de Shareware no incluye modelos de reproductor).*  
 *-Tecnología mejorada de eliminación del trazado de rayos*  
 *-Las más de 3.000 texturas originales del juego han sido actualizadas con una mezcla de texturas mod-pack Q2XP y nuestras propias mejoras.*  
 *-Efectos actualizados con nuevos sprites y animaciones de partículas*  
 *-Iluminación dinámica para elementos como luces intermitentes, señales, interruptores, ascensores y objetos en movimiento.*  
 *-Aproximación de cáusticos para mejorar los efectos de la iluminación del agua*  
 *-Modo de captura de pantalla de alta calidad que hace que sus capturas de pantalla se vean aún mejor*  
 *-Soporte para el antiguo renderizador OpenGL, lo que le permite cambiar entre RTX ON y RTX OFF.*  
 *-Modo de proyección cilíndrica para un campo de visión gran angular en pantallas panorámicas*


Impresionante, verdad. Nosotros ya estamos deseando poder catarlo. Teneis más detalles en el [anuncio oficial](https://www.nvidia.com/en-us/geforce/news/quake-ii-rtx-june-6-release-date/). Si aun no dispones de este clasíco de los shotters, que sepas que puedes comprarlo a muy buen precio en la [Humble Store](https://www.humblebundle.com/store/quake-ii?partner=jugandoenlinux) (patrocinado). Os dejamos con el video para que podais ver Quake II RTX:


 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/unGtBbhaPeU" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


¿Te gusta Quake II? ¿Qué te parecen este tipo de "remasterizaciones"? Cuéntanoslo en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

