---
author: Son Link
category: "Exploraci\xF3n"
date: 2022-06-20 19:13:54
excerpt: "<p>Iron Gate Studio acaba de publicar el \xFAltimo parche de @ValheimGame.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/ValheimLogo.webp
joomla_id: 1476
joomla_url: nuva-version-de-valheim-0-209-8
layout: post
tags:
- steam
- unity3d
- valheim
title: "Nueva versi\xF3n de Valheim: 0.209.8"
---
Iron Gate Studio acaba de publicar el último parche de @ValheimGame.


Hoy se ha publicado una nueva versión de Valheim con varias novedades, siendo la más importante la sincronización de los archivos de guardado en **Steam Cloud**.


El problema radicaba cuando en un mismo ordenador se jugaba en varias cuentas, provocando que en algún momento al sincronizarlas con Steam Cloud el progreso de una partida se perdiese. Para ello ahora los archivos se guardan en la carpeta de cada usuario (**Steam/[YourIDNumber]/892970**) en lugar de en **AppData**. Podrás ver que personajes y mundos se encuentran en la Nube y cuales en local, o si se han movido o no de la antigua localización.


**Personaje en local** (antigua localización):


![Personaje en local](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/0.209.8/Personaje_en_local.webp)


**Personaje en la nube**:


![Personaje en la nube](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/0.209.8/Personaje_en_la_nube.webp)


**Mundo en local** (antigua localización):


![Mundo en local](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/0.209.8/Mundo_en_local.webp)


**Mundo en la nube:**


![Mundo en la nube](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/0.209.8/Mundo_en_la_nube.webp)


Por otro lado en junto a la antigua localización se creara una nueva carpeta llamada _local donde se guardaran los datos de los servidores dedicados y maquinas multicuenta.


 Para cualquier duda puedes consultar la [FAQ](https://steamcommunity.com/app/892970/discussions/0/4615641483004444994) o contactar con ellos a través de Discord


Ademas de esto este es resto de novedades:


### Nuevo contenido:


* El Maypole ahora está habilitado en el menú de Decoración
* Coronas de flores
* Nuevos colores para los estandartes (naranja, blanco, amarillo y morado)


### Correcciones y mejoras:


* Unity ha sido actualizado a la versión 2020.3.33, lo que debería de solucionar varios fallos relacionados con el.
* Los animales domesticados ahora siguen al jugador dentro del campo de fuerza de Haldor.
* Ajustes en el pelo de Yagluth para que ya no vuele en el aire.
* El comando de la consola "exclusivefullscreen" es ahora conmutable.
* Los mods ahora pueden establecer una bandera **isModded** para que los jugadores y nuestro soporte sepan que el juego está modificado.
* Se ha corregido la sugerencia de la interfaz de usuario del inventario.
* Se ha añadido el comando de servidor de la consola 'recall' (teletransporta a otros jugadores a tu posición).


### Steam Cloud:


* Los archivos guardados en la nube se almacenarán ahora en Steam/[YourIDNumber]/892970 en lugar de AppData.
* Los archivos locales se almacenarán ahora en "worlds_local" y "characters_local" en AppData y ya no se sincronizarán con la nube para evitar conflictos de sincronización y pérdida de datos cuando se usen varias cuentas en la misma máquina, y cuando se usen servidores dedicados.
* Los archivos que aún se encuentren en la antigua estructura de archivos se moverán a la nube de Steam o a la nueva carpeta local cuando se utilicen y se guardará una copia de seguridad
* Los archivos de guardado de los mundos ahora pueden ser renombrados y se cargarán correctamente.
* Los mundos grandes (300mb o más) ahora se sincronizan correctamente.
* Ahora el almacenamiento máximo en la Nube para Valheim es mucho mayor.
