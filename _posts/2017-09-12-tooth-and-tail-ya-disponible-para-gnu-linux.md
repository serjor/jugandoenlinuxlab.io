---
author: Serjor
category: Estrategia
date: 2017-09-12 20:19:40
excerpt: "<p>Un RTS en un mundo de fantas\xEDa animal</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d5df152e20908e75d0017d93f2a154f3.webp
joomla_id: 459
joomla_url: tooth-and-tail-ya-disponible-para-gnu-linux
layout: post
tags:
- rts
- tooth-and-tail
title: Tooth and Tail ya disponible para GNU/Linux
---
Un RTS en un mundo de fantasía animal

Ya sea gracias a un miembro de nuestro canal de Telegram, o porque Steam nos lo recuerda, hoy nos enteramos sí o sí de que, cómo ya os [anunciamos](index.php/homepage/apt-get-update/item/531-una-semana-mas-os-traemos-un-apt-get-con-las-ultimas-novedades) hace unos meses, Tooth and Tail, un juego de los chicos de Pocketwatch Games que recientemente nos traían su "Mónaco: What is yours is mine" de manera [gratuita](index.php/homepage/generos/accion/item/578-monaco-what-s-yours-is-mine-gratis-durante-unas-horas-mas), hace su aparición con soporte para GNU/Linux desde el primer día.


![tooth and tail](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ToothAndTail/tooth_and_tail.webp)


Este Tooth and Tail es un RTS en el que nuestro ejército estará formado por animales de lo más simpáticos, pero armados hasta los dientes (o colmillos, según se mire) cuyas principales características son mapas generados de manera procedural en partidas rápidas que duran entre los 5 y 10 minutos. Según los propios desarrolladores, la aleatoriedad de los mapas obliga al jugador a tener que adaptarse en vez de desarrollar un patrón que le sirva para todo. Además, y como no podía ser de otra manera en estos tiempos, con multiplayer, tanto online como local, a pantalla dividida.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/nC99U9cJGR4" width="560"></iframe></div>


Así que ya sabes, si te gusta la estrategia en tiempo real y buscas un juego donde las batallas no se alarguen demasiado, este es tu juego.


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/286000/" width="646"></iframe></div>


Y tú, ¿liderarás a una tropa de roedores cual flautista de Hamelín? Cuéntanos qué opinas de este juego en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

