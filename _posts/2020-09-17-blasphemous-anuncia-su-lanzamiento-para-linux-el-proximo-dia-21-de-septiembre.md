---
author: Pato
category: "Acci\xF3n"
date: 2020-09-17 18:31:10
excerpt: "<p>Prep\xE1rate a cumplir tu penitencia en Cvstodia con el grandioso juego\
  \ de&nbsp;@BlasphemousGame</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Blasphemous2.webp
joomla_id: 1256
joomla_url: blasphemous-anuncia-su-lanzamiento-para-linux-el-proximo-dia-21-de-septiembre
layout: post
tags:
- accion
- indie
- plataformas
- proximamente
title: "Blasphemous anuncia su lanzamiento para Linux el pr\xF3ximo d\xEDa 21 de Septiembre"
---
Prepárate a cumplir tu penitencia en Cvstodia con el grandioso juego de @BlasphemousGame


 


Tras incumplir el plazo que se dieron en agosto y del que [nos hicimos eco en jugandoenlinux.com]({{ "/posts/blasphemous-tendra-version-nativa-dentro-de-muy-poco" | absolute_url }}), parece que ahora si, el estudio sevillano The Game Ktichen tiene todo preparado para lanzar la versión para nuestro sistema favorito.


La estimación era que el juego llegase a nuestros sistemas el día que llegase la actualización **Stir of Dawn**, pero al parecer ciertos problemas han retrasado el lanzamiento, pero esta misma tarde han confimado mediante un mensaje en twitter que El Penitente está ya a las puertas linuxeras de Cvstodia:



> 
> Calling all members of the Mac and Linux congregation...  
>   
> The gates of Cvstodia open to you Monday 21st September on Steam! [pic.twitter.com/XaQjnlnjci](https://t.co/XaQjnlnjci)
> 
> 
> — Blasphemous (@BlasphemousGame) [September 17, 2020](https://twitter.com/BlasphemousGame/status/1306602276702879744?ref_src=twsrc%5Etfw)


  





*Una terrible maldición conocida como el Milagro ha caído sobre la tierra de Cvstodia y sus habitantes. Eres el Penitente, el único superviviente de la masacre de la hermandad del Lamento Mudo. Estás atrapado en un ciclo de penitencia de muerte y resurrección constante, y solo tú puedes librar al mundo de este destino tan terrible y llegar al origen de tu angustia.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/Ctm-GClM6wo" width="560"></iframe></div>


**Características principales:**


**Explora un mundo no lineal:** Derrota a enemigos terroríficos y supera trampas mortales mientras exploras una gran cantidad de zonas, y busca la redención en Cvstodia, un mundo oscuro y barroco.  
  
**Combate brutal:** Masacra a los enemigos con el poder de Mea Culpa, una espada nacida del remordimiento. Consigue nuevos combos devastadores y movimientos especiales para purgar a todos los que se crucen en tu camino.  
  
**Ejecuciones:** Libera tu ira y deléitate desmembrando a tus adversarios de forma sangrienta con ejecuciones animadas hasta el último píxel.  
  
**Personaliza tu personaje:** Descubre y equípate reliquias, cuentas de rosario, plegarias y corazones de espada para conseguir nuevas habilidades y mejoras de atributos para poder sobrevivir. Prueba con distintas combinaciones para encontrar una que encaje con tu estilo de juego.  
  
**Batallas épicas contra jefes:** Para llegar a tu objetivo tendrás que derrotar a hordas de criaturas gigantes y perversas. Aprende sus movimientos, sobrevive a sus ataques devastadores y consigue la victoria.  
  
**Desbloquea los misterios de Cvstodia:** El mundo está lleno de almas atormentadas. Algunas te ayudarán y otras te pedirán algo a cambio. Descubre la historia y el destino de estos personajes torturados para conseguir recompensas y aprender más sobre el mundo oscuro que recorres.


En cuanto al juego en sí, nos llega con la expansión gratuita **Stir of Dawn**, y con traducción de voces en español con un elenco de actores de lujo:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/vKfree2PJ_A" width="560"></iframe></div>


Si no sufren nuevos retrasos, Blasphemous estará disponible este próximo día 21 para nuestros sistemas, y lo puedes conseguir en [Humble Bundle](https://www.humblebundle.com/store/blasphemous?partner=jugandoenlinux) (enlace patrocinado) y en [Steam](https://store.steampowered.com/app/774361/Blasphemous/).


Os dejo con el anuncio de **Stir of Dawn**.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/9QdGzJhunb4" width="560"></iframe></div>


 

