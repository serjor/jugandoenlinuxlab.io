---
author: Pato
category: "Acci\xF3n"
date: 2017-05-24 06:48:36
excerpt: "<p>El juego es obra de los creadores de \"The Last Door\". La versi\xF3\
  n de Linux est\xE1 \"garantizada\"</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3b44b5000d3ada04c028c395cd05cbfb.webp
joomla_id: 336
joomla_url: blasphemous-ya-esta-en-campana-en-kickstarter-y-tiene-una-pinta-fantastica
layout: post
tags:
- accion
- indie
- plataformas
- kickstarter
- gore
title: "'Blasphemous' ya est\xE1 en campa\xF1a en Kickstarter y tiene una pinta fant\xE1\
  stica"
---
El juego es obra de los creadores de "The Last Door". La versión de Linux está "garantizada"

Hace ya unos meses que [publicamos aquí](index.php/homepage/generos/accion/item/286-blasphemous-el-nuevo-juego-de-los-creadores-de-the-last-door-casi-seguro-llegara-a-linux) en Jugando en Linux que el estudio sevillano "The Game Kitchen" estaba desarrollando su nuevo juego 'Blasphemous' [[web oficial](http://blasphemousgame.com/)], y que muy probablemente este juego llegaría a Linux. Pues desde ayer 'Blasphemous' está en campaña en Kickstarter y tal y como publican en su página, **la versión de Linux está "garantizada".**


"The Game Kitchen" es la compañía que creó el más que notable '[The Last Door](http://store.steampowered.com/app/284390/The_Last_Door__Collectors_Edition/)' también disponible en Linux, por lo que realmente podemos decir que no son palabras vacías. 'Blasphemous' es un juego de acción en 2D obscuro y brutal de tipo "hack & Slash" con una profunda y evocadora narrativa, y en el que tendremos que explorar un mundo intrincado y con niveles no lineales. 


Con un transfondo pseudo-religioso, 'Blasphemous' nos pone en la piel del "Penitente", un personaje atormentado que luchará contra criaturas diabólicas y seres grotescos con el transfondo de una sociedad ultra-religiosa. En la tierra de Ortodoxia se ha desatado la corrupción, y "el Penitente" tedrá que luchar para contener las hordas de seres que han corrompido su alma.


<div class="resp-iframe"><iframe height="360" src="https://www.kickstarter.com/projects/828401966/blasphemous-dark-and-brutal-2d-non-linear-platform/widget/video.html" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Algunos medios ya hablan de que el estilo de acción y "pixel art" de este 'Blasphemous' es como si "Castlevania" se mezclara con "Dark Souls".


En el momento de escribir estas líneas, 'Blasphemous' ya lleva recaudados más de 35.000$ de los 50.000$ que piden para llevar a buen puerto este juego en menos de 24 horas, lo que prácticamente significa que la campaña será un éxito, e incluso se alcanzarán varios de los objetivos de la campaña. Si estás interesado en apoyar el proyecto, puedes hacerlo en su página de Kickstarter:


<div class="resp-iframe"><iframe height="420" src="https://www.kickstarter.com/projects/828401966/blasphemous-dark-and-brutal-2d-non-linear-platform/widget/card.html?v=2" width="220"></iframe></div>


¿Qué te parece este 'Blasphemous'? ¿Piensas apoyar la campaña?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

