---
author: Pato
category: Arcade
date: 2017-05-31 10:12:09
excerpt: "<p>Parece que Virtual Programming est\xE1 a cargo del port</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c4416d79eef6dd018bcee3cd8b8ba561.webp
joomla_id: 351
joomla_url: micro-machines-world-series-llegara-a-linux-steamos-el-proximo-23-de-junio
layout: post
tags:
- proximamente
- arcade
- virtual-progamming
- codemasters
- micro-machines
title: "'Micro Machines World Series' llegar\xE1 a Linux/SteamOS el pr\xF3ximo 23\
  \ de Junio"
---
Parece que Virtual Programming está a cargo del port

Hace tan solo unos días que le hicimos un [exhaustivo seguimiento](index.php/homepage/generos/arcade/item/461-codemasters-no-descarta-traer-micro-machines-world-series-a-linux-steamos), y hace tan solo unos minutos que Virtual Programming ha publicado en Twitter un esclarecedor mensaje:



> 
> 23rd June... ??[#linux](https://twitter.com/hashtag/linux?src=hash) [#mac](https://twitter.com/hashtag/mac?src=hash) [#linuxgaming](https://twitter.com/hashtag/linuxgaming?src=hash) [#macgaming](https://twitter.com/hashtag/macgaming?src=hash) [#gamer](https://twitter.com/hashtag/gamer?src=hash) [#micromachines](https://twitter.com/hashtag/micromachines?src=hash) [#car](https://twitter.com/hashtag/car?src=hash) [#racing](https://twitter.com/hashtag/racing?src=hash) <https://t.co/m6xjZmW8oy>
> 
> 
> — Virtual Programming (@virtualprog) [May 31, 2017](https://twitter.com/virtualprog/status/869837928570204160)



Parece claro que la versión del juego para Linux está a cargo de Virtual Programing, compañía responsable de otros ports como "Witcher 2", "Bioshock Infinite" o ArmA3". Según se lee, el juego de Codemasters sobre la conocidísima franquicia "Micro Machines World Tour" llegará a Linux/SteamOS completamente traducido al español el próximo día 23 de Junio, a diferencia de la información oficial que aparece en la página de Steam del juego donde aparece como lanzamiento el día 20.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/LJ9krpTcGuE" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


Si estás interesado en precomprarlo, lo tienes ya disponible con un 10% de descuento sobre su precio oficial:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/535850/" width="646"></iframe></div>


 ¿Piensas jugar a 'Micro Machines World Tour? ¿Te gustaría echar unas partidas en línea con nosotros?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

