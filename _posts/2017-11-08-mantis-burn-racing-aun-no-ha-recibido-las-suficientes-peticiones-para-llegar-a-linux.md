---
author: Pato
category: Carreras
date: 2017-11-08 15:49:09
excerpt: <p>Los desarrolladores no ven viable el port si no hay apoyo por parte de
  los usuarios</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2cf20e200ca2c770a044a65b100b0636.webp
joomla_id: 519
joomla_url: mantis-burn-racing-aun-no-ha-recibido-las-suficientes-peticiones-para-llegar-a-linux
layout: post
tags:
- carreras
- indie
- casual
- racing
title: "'Mantis Burn Racing' a\xFAn no ha recibido las suficientes peticiones para\
  \ llegar a Linux"
---
Los desarrolladores no ven viable el port si no hay apoyo por parte de los usuarios

Ya hablamos de 'Mantis Burn Racing' [hace tiempo](index.php/homepage/apt-get-update/item/396-apt-get-update-beat-cop-planetscape-torment-88-heroes-meganoid) cuando supimos que los desarrolladores estaban abiertos a portar el juego a Linux si hay suficiente demanda. De eso ya hace unos cuantos meses, y por lo visto no ha habido el suficiente "apoyo" por parte de la comunidad para que el juego acabe llegando a nuestro sistema favorito.


En concreto, en un post dentro de los foros del juego en Steam los desarrolladores en respuesta a las preguntas de si estaba o no el port en marcha afirman que con el nivel de demanda que se ha visto en el mismo hilo de petición no ven viable realizar dicho port. 


"*Este número [de peticiones] no son suficientes para nosotros para desarrollar una versión del juego para Linux, desafortunadamente. Definitivamente nos gustaría hacerlo, pero somos un equipo pequeño y no tenemos los recursos para tomar muchos riesgos financieros. Estamos escuchando y esperamos ver mas y mas gente pidiendo una versión para Linux. Gracias por vuestros comentarios y seguir enviándolos!.*


Puedes ver el post [en este enlace](http://steamcommunity.com/app/446100/discussions/0/135512625250997449/?tscn=1510137147#c1483235412195910567).


'Mantis Burn Racing' es un juego de carreras en vista isométrica del estilo de "Micromachines" donde corres en distintos escenarios.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/dHdawEAIQlY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


¿Que te parece 'Mantis Burn Racing? Si te gusta y te interesa que el juego llegue a Linux, pásate [por el hilo de la petición](http://steamcommunity.com/app/446100/discussions/0/135512625250997449/?tscn=1510137147) y muestra tu apoyo. 

