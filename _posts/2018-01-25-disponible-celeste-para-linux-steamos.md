---
author: leillo1975
category: Plataformas
date: 2018-01-25 16:57:46
excerpt: <p>El juego ha sido portado por Ethan Lee.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e083ac4df288f0b9f03ec1adbe79e026.webp
joomla_id: 617
joomla_url: disponible-celeste-para-linux-steamos
layout: post
tags:
- retro
- ethan-lee
- celeste
- matt-makes-games-inc
title: Disponible Celeste para Linux/SteamOS.
---
El juego ha sido portado por Ethan Lee.

La primera vez que supe de este juego fué durante un reciente video donde se anunciaban las novedades para la genial [Nintendo Switch](https://www.nintendo.com/games/detail/celeste-switch). Pocos días después me sorprendia comprobar mediante un tweet de nuestro admirado [Ethan Lee](http://www.flibitijibibo.com/) ([@**flibitijibibo**](https://twitter.com/flibitijibibo)) que el juego estaría disponible en nuestro sistema. El tweet en cuestion decía lo siguiente:



> 
> Looks like I can say it now: [@celeste_game](https://twitter.com/celeste_game?ref_src=twsrc%5Etfw) will be available at launch for Linux! Special perks include DualShock 4/Switch Pro Controller detection with GUI prompts and DS4 light bar support! [pic.twitter.com/QecFxKU7KJ](https://t.co/QecFxKU7KJ)
> 
> 
> — Ethan Lee (@flibitijibibo) [23 de enero de 2018](https://twitter.com/flibitijibibo/status/955833780606046208?ref_src=twsrc%5Etfw)



 Como sabeis **Ethan Lee** es un conocido porter de juegos Retro y entre sus trabajos están los geniales [WonderBoy: The Dragon's Trap](index.php/homepage/analisis/item/564-analisis-wonder-boy-the-dragon-s-trap), Fez, Dust: An Elisyan Tail, [Transistor](index.php/homepage/analisis/item/544-analisis-transistor), Apotheon, Mercenary Kings, [Owlboy](index.php/homepage/generos/aventuras/item/295-owlboy-el-galardonado-juego-de-aventuras-ya-disponible-en-linux) , Bastion, [Pyre](index.php/homepage/generos/rol/item/540-pyre-de-supergiant-games-ya-disponible-para-gnu-linux)...., por lo que la calidad de este trabajo está garantizada.


En cuanto a [Celeste](http://www.celestegame.com/), el juego está desarrollado y Editado por [Matt Makes Games](http://www.mattmakesgames.com/), y según la descripción que podemos encontrar en Steam podemos encontrar lo siguiente:



> 
> Acerca de este juego
> --------------------
> 
> 
> Ayuda a Madeline a sobrevivir a los demonios de su interior en su viaje a la cima de la montaña Celeste; un ajustadísimo plataforma diseñado a mano por los creadores del clásico multijugador TowerFall.
> 
> 
> * Aventura de un jugador marcada por la narración; un encantador elenco de personajes y un argumento conmovedor de descubrimiento personal.
> * Gran montaña con más de 700 pantallas de duros desafíos y retorcidos secretos.
> * Brutales capítulos "cara B" para desbloquear; para los más osados.
> * Finalista en “Excellence in Audio” de IGF; más de 2 horas de música original con un asombroso piano y pegadizos ritmos de sintetizador.
> * Pastel
> 
> 
> Los mandos son sencillos y accesibles: salta, acelera en el aire y escala, pero con varias capas de profundidad de expresión que habrá que dominar, pues cada muerte enseña una lección. Las reapariciones relámpago te permiten seguir escalando mientras desvelas misterios y te enfrentas a numerosos peligros.   
>   
> *Es el momento, Madeline. Respira hondo. Tú puedes hacerlo.*
> 
> 
>  
> 
> 
> 


Los requisitos mínimos del juego, como es de esperar no son nada elevados y funcionará perfectamente en cualquier equipo:


* **SO:** glibc 2.17+, 32/64-bit
* **Procesador:** Intel Core i3 M380
* **Memoria:** 2 GB de RAM
* **Gráficos:** OpenGL 3.0+ (2.1 with ARB extensions acceptable)
* **Almacenamiento:** 1200 MB de espacio disponible


Si quereis haceros una idea de como es el juego podeis ver el siguiente gameplay:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/7XKSMt7NW6Q" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Puedes comprar Celeste para Linux/SteamOS en [itch.io](https://mattmakesgames.itch.io/celeste) y Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/504230/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


 ¿Os molan los juegos Retro?¿Qué os parece Celeste? Dinos lo que quieras sobre este juego en los comentarios, o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

