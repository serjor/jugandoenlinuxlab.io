---
author: Pato
category: Noticia
date: 2018-06-05 17:33:47
excerpt: "<p>Acomp\xE1\xF1anos el pr\xF3ximo d\xEDa 12 durante la retransmisi\xF3\
  n del Pc Gaming Show</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1da7a0ac7a34d319d36fde2ba2a083ea.webp
joomla_id: 755
joomla_url: e3-2018-en-jugandoenlinux-com-horarios-y-eventos
layout: post
tags:
- e3-gaming-show
title: E3 2018 en jugandoenlinux.com - horarios y eventos
---
Acompáñanos el próximo día 12 durante la retransmisión del Pc Gaming Show

Estamos tan solo a unos días de que comience el mayor evento sobre videojuegos del año, y aunque en Jugando en Linux sabemos que hay muchas conferencias y desarrollos que allí se mostrarán y que nunca llegarán a nuestro sistema favorito, pensamos que dado que formamos parte ya de este mundillo del videojuego por derecho propio estaría bien que "retransmitiéramos" las conferencias que nos resulten interesantes para todos vosotros.


Pero lo primero es empezar por el horario de eventos:




|  |  |  |
| --- | --- | --- |
| **DIA**  | **CONFERENCIA** | **HORA PENINSULAR ESPAÑOLA** |
| 9 Junio | Electronic Arts | 20:00h |
| 10 Junio | Microsoft | 22:00h |
| 11 Junio | Bethesda | 3:30h |
| 11 Junio | Square Enix | 19:00h |
| 11 Junio | Ubisoft | 22:00h |
| 12 Junio |  PC Gaming Show | 0:00h |
| 12 Junio |  Sony | 3:00h |
| 12 Junio |  Nintendo | 18:00h |
|  |  |  |


En principio, **en Jugando en Linux tenemos interés en ver el PC Gaming Show**, el próximo Martes día 12 a las 0:00h y os invitamos a verla con nosotros. ¿Donde? en nuestro canal en "[Twitch](https://www.twitch.tv/jugandoenlinux)" donde estaremos comentando todo lo que vaya surgiendo en nuestro chat en el canal. ¿Te apuntas? ¡Te estaremos esperando!


Pero... hay mas conferencias y probablemente también las veremos. ¿Cual quieres ver tu? ¿Te interesaría que la pusiésemos también en nuestro canal de Twitch?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

