---
author: Pato
category: "Acci\xF3n"
date: 2018-05-19 18:25:15
excerpt: "<p>Se trata de un juego de acci\xF3n \"Beat'Em Up\" con hasta 4 jugadores</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d12fc2ba4cae84a5ddd14475d5a89895.webp
joomla_id: 742
joomla_url: son-of-a-witch-ya-esta-disponible-el-linux-steamos
layout: post
tags:
- accion
- indie
- roguelike
- beat-em-up
- cooperativo
title: "'Son of a Witch' ya est\xE1 disponible el Linux/SteamOS"
---
Se trata de un juego de acción "Beat'Em Up" con hasta 4 jugadores

Leemos en [Linuxgameconsortium](https://linuxgameconsortium.com/linux-gaming-news/son-of-a-witch-co-op-beat-em-up-launches-linux-66581/) que Son of a Witch ya está disponible para su compra en Linux/SteamOS. Se trata de un juego de acción "rogue-like" de 1 a 4 jugadores con "gráficos estilo cartoon", controles fáciles y mecánicas de juego profundas. Combina docenas de armas diferentes, objetos mágicos, pociones y hechizos con efectos únicos para derrotar las facciones enemigas y 24 jefes. ¿Puedes encontrar el artefacto antiguo Goblin al final de la mazmorra?


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/kO0PTPC_dZU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 El juego recuerda a títulos como Battleblock Theater o Foul Play, con un componente multijugador que hace atractivo el jugarlo con amigos, ya sea en local o en línea con diversión y risas aseguradas.


Si quieres echarle un vistazo puedes verlo en su página de Steam:


7 <div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/539400/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Que te parece 'Son of a Witch? ¿Te gustan los juegos de acción-rogue-like cooperativos?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

