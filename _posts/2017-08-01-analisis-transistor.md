---
author: yodefuensa
category: "An\xE1lisis"
date: 2017-08-01 15:52:00
excerpt: <p>"<em>Un juego sobresaliente de Supergiant Games"</em></p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3d9a979b1c7ae6adda1ab5edd6c87151.webp
joomla_id: 422
joomla_url: analisis-transistor
layout: post
tags:
- indie
- rol
- analisis
title: "An\xE1lisis: Transistor"
---
"*Un juego sobresaliente de Supergiant Games"*

Transistor salió en 2014 de la mano del estudio Supergiant Games, pequeño estudio indie fundado en 2009 ([web oficial](http://www.supergiantgames.com/)) del cual pudimos disfrutar su primer trabajo, Bastion.


Si has jugado a Bastion y te gustó, te gustará Transistor pues nos encontramos ante los mismos elementos que lo hacían único. La voz de un narrador acompañándonos en esta aventura, la vista isométrica, un característico estilo visual y una banda sonora magnifica. En definitiva casi el mismo cóctel, esto no por ello es malo pues venimos de un juego sobresaliente. Pero basta ya de fijarnos en Bastion. Vamos con lo que nos interesa: Transistor


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/RT55lch6y_U" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


El juego comienza con nosotros una artista llamada Red que ha perdido la voz. Frente a ella el cuerpo sin vida de una persona cuya voz ha quedado atrapada en una espada gigante. Sin más preámbulos nos embarcamos en una venganza personal.


A partir de entonces el juego nos suelta sin saber mucho más. Con el combate pasa algo parecido, algún comentario, nos indicara como funciona, pero el como jugarlo será descubrimiento nuestro.


El sistema de combate es la diferencia de este juego frente a cualquier otro. Una apuesta arriesgada, al que cuesta cogerle el hilo, pero muy flexible a su manera. Se nos pone ante nosotros 4 botones principales. Podemos usarlas como un ActionRPG. La novedad es que al pulsar el gatillo, detenemos el tiempo, pudiendo usar estas habilidades o desplazarnos por la zona de combate.  
 Debemos decidir a quien atacar, que golpes usar detras de otros para hacer combos, desplazarnos para golpear por la espalda o sencillamente huir. Esta habilidad de parar el tiempo nos penalizará sin poder usar las habilidades hasta que se regenere nuestra barra de energía. Por lo tanto no es solo usarlo a diestro y siniestro debemos pensar que acciones nos combienen más si no podremos atacar en los segundos siguientes.

![Imagen1](https://steamuserimages-a.akamaihd.net/ugc/836958517713037666/B616699BD165AFCECA4FA9548BFADDA53921D713/?interpolation=lanczos-none&output-format=jpeg&output-quality=95&fit=inside|1024:576&composite-to%3D%2A%2C%2A%7C1024%3A576&background-color=black.webp)


A esto una variedad de habilidades que cambian según las combinemos con otras. Tenemos un juego al que no solo tendremos que buscarnos el estilo de combate que más nos guste y se adapte a nosotros si no el más util para cada ocasión.

![imagen2](https://steamuserimages-a.akamaihd.net/ugc/836958517713035755/CFC9F9E9683F6C27D6A86728DB7438561F72BE33/?interpolation=lanczos-none&output-format=jpeg&output-quality=95&fit=inside|1024:576&composite-to%3D%2A%2C%2A%7C1024%3A576&background-color=black.webp)


La dificultad del juego no es demasiado alta, y ademas es un juego corto, con solo 5-7 horas de duración, pero la opción de rejugarlo y los “limitadores” consiguen que sea un reto. Estos limitadores son opcionales y son diferentes funciones que complican el combate a cambio de más experiencia. Como ya he dicho es un juego corto pero esta pensado para ser rejugado, no descubriremos nada nuevo, pero sí un nivel de dificultad más desafiante. Podemos decir que nos quedamos a medias a la hora de descubrir el sistema de combate si no lo rejugamos.


![imagen3](https://steamuserimages-a.akamaihd.net/ugc/836958517713036809/B1937ECFB08AB182F34E29B79F4E6879576E46FB/?interpolation=lanczos-none&output-format=jpeg&output-quality=95&fit=inside|1024:576&composite-to%3D%2A%2C%2A%7C1024%3A576&background-color=black.webp)

En el apartado técnico nos encontramos ante un juego que se nota que ha sido tratado con mimo. Escenarios dibujados a mano. Que cuentan con detallitos dejandonos poder decidir si queremos que nieve o el color del cielo. Sin embargo pese a que los escenarios punk de la ciudad son una delicia, a veces nos cuesta un poco distinguir por donde seguir.

Respecto al apartado sonoro. Es sin duda su punto fuerte, una magnifica banda sonora.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/-zA1jRmAYfU" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>

Como puedes suponer al ver los gráficos no estamos ante un juego muy exigente, de hecho sus requisitos son bastante comedidos.


**SO:** glibc 2.15+, 32/64-bit


* **Procesador:** Dual Core CPU - 2.6ghz
* **Memoria:** 4 GB de RAM
* **Gráficos:** OpenGL 3.0+ (2.1 with ARB extensions acceptable)
* **Almacenamiento:** 3 GB de espacio disponible


En definitiva Transistor se trata de una pequeña obra de arte que merece estar en cualquier librería de un jugador linuxero. Supergiant Games desmuestra su buen hacer marcandose otro juego sobresaliente. Al que si podemos poner alguna pega, es que se nota que el estudio podría haber hecho algo más. Quizá sea una virtud que la historía sea corta sin llegar a dejar que nos cansemos del juego. Aun así el juego es sobresaliente y sin duda debemos estar pendientes de los próximos lanzamientos de esta editora.


Transistor está traducido al español y lo puedes encontrar en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/237930/43629/" width="646"></iframe></div>


Este análisis ha sido escrito por un invitado (Yodefuensa). Si quieres, puedes enviarnos tus propios artículos y análisis para que los valoremos y los publiquemos. Puedes hacerlo desde nuestro apartado "[Colabora](index.php/contacto/envianos-tu-articulo)" 


Cuéntanos qué te parece Transistor en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

