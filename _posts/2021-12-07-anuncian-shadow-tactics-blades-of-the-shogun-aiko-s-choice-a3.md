---
author: P_Vader
category: Estrategia
date: 2021-12-07 09:41:22
excerpt: "<p>Mimimi Games acaba de lanzar esta nueva expansi\xF3n independiente de\
  \ su famoso juego.</p>\r\n"
image: https://cdn.cloudflare.steamstatic.com/steamcommunity/public/images/clans/40165985/3401a8e0c07f6deda250e727f62b0aabc08c365c.png
joomla_id: 1317
joomla_url: anuncian-shadow-tactics-blades-of-the-shogun-aiko-s-choice-a3
layout: post
tags:
- shadow-tactics
- expansion
title: 'Anuncian  Shadow Tactics: Blades of the Shogun - Aiko''s Choice (ACTUALIZADO
  3)'
---
Mimimi Games acaba de lanzar esta nueva expansión independiente de su famoso juego.


**ACTUALIZACIÓN 7-12-2021:**


Aunque en sus últimos comentarios esperábamos cierto retraso en la versión para Linux, finalmente no ha sido así y ya tenemos la esperada expansión de Shadow Tactics, coincidiendo con su quinto aniversario:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/ZMR7iWpP6ds" title="YouTube video player" width="770"></iframe></div>


 Una de las mejoras del juego ha sido su banda sonora en el que han contado en esta ocasión con una gran orquesta y aseguran que se nota.


También confirmaron que en el aspecto técnico no han podido añadir las características mejoradas que usaron en Desperados 3, dado que hubiera supuesto una gran actualización sobre el código que hubiera hecho imposible desarrollar esta expansión en los plazos previstos.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1579380/" style="border: 0px;" width="646"></iframe></div>


---

**ACTUALIZACIÓN 2-9-2021:**

[Mimimi games anuncia](https://store.steampowered.com/news/app/418240/view/2949282742500329932) mas detalles sobre su nueva expansión. Si fuiste jugador avanzado del juego principal, **tal vez quieras observar con atención a un viejo amigo en su nuevo tráiler oficial**...

<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/mkS3C4VSYQQ" title="YouTube video player" width="780"></iframe></div>


La expansión incluye **tres misiones principales completas**, ambientadas en entornos totalmente nuevos, **y tres misiones intermedias** más cortas.


Algo que no quedó claro cuando anunciaron la expansión era si junto con Aiko como protagonista incluirían alguno de los otros personajes. Las dudas se despejan con este anuncio, **volveremos a contar con el equipo completo de personajes con todas sus características y habilidades**, para por fin descubrir el pasado de Aiko.


La historia **volverá al inicio del [periodo Edo](https://es.wikipedia.org/wiki/Per%C3%ADodo_Edo) del antiguo Japón con sus Shogun**, como en el juego original.


**Noticia original 19-6-2021:**


Si aun no conoces Shadow Tactics te recomiendo que leas [el analisis que hizo @serjor]({{ "/posts/analisis-shadow-tactics-blades-of-the-shogun" | absolute_url }}) en su día, en el que detalla esta joya de juego.


Hace unos meses **[Mimimi Games anunció una nueva expasión](https://www.mimimi.games/aikos-choice-will-be-pc-exclusive/) independiente de Shadow Tactics exclusivamente para PC, incluyendo plan para Linux**, con la sorpresa de que  dejaban fuera a Mac (por ser 64 bits) y consolas, ya que suponía demasiado esfuerzo.


Después de un tiempo sin dar mas detalles, **[acaban de desvelar la trama de esta nueva expansión](https://store.steampowered.com/news/app/418240/view/2990934608189750805) que estoy seguro que hará felices a los amantes de este juego**, donde me incluyo sin lugar a dudas.

En esta ocasión **se centrarán en una de las protagonistas del juego principal: la hábil kunoichi Aiko.** Para mi gusto uno de mis personajes favoritos y fascinantes del juego ¡Un acierto en la elección!

![shadow tactics aiko](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/shadowtactics/shadow_tactics_aiko.webp)


En esta nueva trama **deberá enfrentarse a su peor enemiga, lady Chiyo la astuta maestra de espías**:

>
> Derrotar a los enemigos desde las sombras es algo que lady Chiyo ha convertido en un arte. Fue una de las primeras que estableció las enseñanzas de las kunoichi, las shinobi femeninas. De hecho, ¡es la antigua maestra de Aiko y le enseñó todo lo que sabe!
>

 ![Shadow Tactics Lady Chiyo](https://cdn.cloudflare.steamstatic.com/steamcommunity/public/images/clans/24163932/bcf9ea96cc168884be7e060d196a3da4168a3c97.png)


La fecha **prevista de lanzamiento será para finales de 2021**. El soporte para Linux no debería de tardar en llegar después del lanzamiento como nos tiene acostumbrados Mimimi Games. **¡Si te gusto el juego principal no [olvides añadirlo a tu lista de deseados!](https://store.steampowered.com/app/1579380/Shadow_Tactics_Blades_of_the_Shogun__Aikos_Choice/?snr=1_5_9_)**


¿Te gustó en su día Shadow Tactics: Blades of the Shogun? ¿Esperas ansioso esta nueva expansión como nosotros? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).
