---
author: Pato
category: "Acci\xF3n"
date: 2018-01-10 16:32:50
excerpt: "<p>Adem\xE1s el juego recibir\xE1 diversas mejoras para solucionar algunos\
  \ problemas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/98cd27a9ec6668fd255be4380c71b8bd.webp
joomla_id: 600
joomla_url: everspace-actualizara-su-motor-a-unreal-engine-4-17-con-su-proximo-parche
layout: post
tags:
- accion
- espacio
- exploracion
title: "'Everspace' actualizar\xE1 su motor a Unreal Engine 4.17 con su pr\xF3ximo\
  \ parche"
---
Además el juego recibirá diversas mejoras para solucionar algunos problemas

Como ya sabéis, estamos siguiendo el desarrollo de Everspace con atención desde hace algún tiempo, e incluso le hemos hecho [un análisis](https://www.jugandoenlinux.com/index.php/homepage/analisis/item/605-analisis-everspace) que nos desvela lo que es a todas luces un magnífico juego de acción con naves espaciales con tintes de exploración y "rogue-like".


También sabéis que el juego aún no ha llegado de forma "oficial" a Linux/SteamOS [debido a los problemas](https://www.jugandoenlinux.com/index.php/homepage/generos/accion/item/570-everspace-sigueen-desarrollo-para-linux-pero-tienen-problemas-con-graficas-amd) que los desarrolladores han encontrado para hacerlo funcionar correctamente con gráficas AMD.


Ahora, gracias a la excelente comunicación de los desarrolladores con la comunidad nos enteramos que el juego recibirá una actualización a la versión 4.17 de Unreal Engine en un próximo parche, al igual que otras correcciones y mejoras, como la respuesta del ratón y las colisiones con ciertos elementos. Esperemos que el cambio de versión de Unreal traiga la solución para los problemas con AMD y por fin tengamos una versión "oficial" del juego próximamente.


Podéis ver el anuncio original en [este enlace](http://steamcommunity.com/app/396750/discussions/0/1473096694440345684/?ctp=24#c1620600279679389838).


*EVERSPACE' es un "shooter espacial" para un jugador que combina elementos roguelike con gráficos de primera calidad y una historia cautivadora. Emprende un desafiante viaje a través de un universo hermosamente creado, lleno de sorpresas y en constante cambio. Tus habilidades, técnicas y talento para la improvisación serán puestos a prueba continuamente, mientras aprendes más sobre tu existencia a través de encuentros con personajes interesantes que irán aportando sus piezas del rompecabezas a la historia.*


Podéis ver una sesión de juego grabada por Leillo y otra por Serjor para su análisis:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/G6zj57jcJSM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/-Qzk8XnR0wY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Si estás interesado en 'Everspace' y tienes una gráfica Nvidia, lo tienes disponible en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="http://store.steampowered.com/widget/396750/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 Gracias por el aviso **@Txema**

