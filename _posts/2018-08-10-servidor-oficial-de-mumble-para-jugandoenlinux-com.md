---
author: Serjor
category: Web
date: 2018-08-10 12:52:22
excerpt: "<p>Gracias a los dioses n\xF3rdicos, nuestras conversaciones pueden ser\
  \ libres</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2dee5982cbdc2b7b7b3c545e34831370.webp
joomla_id: 824
joomla_url: servidor-oficial-de-mumble-para-jugandoenlinux-com
layout: post
tags:
- voip
- open-source
- mumble
title: Servidor oficial de mumble para jugandoenlinux.com
---
Gracias a los dioses nórdicos, nuestras conversaciones pueden ser libres

Tal y como dice el titular, ¡tenemos servidor mumble de jugandoenlinux! [mumble.jugandoenlinux.com:60601](mumble://mumble.jugandoenlinux.com:60601) (ojo que no es el puerto estándar de mumble)


La verdad es que nos hace mucha ilusión poder disponer de una alternativa open source para poder hablar durante las partidas, y no hubiera sido posible sin la ayuda de la comunidad, ya que los gastos del servidor corren a cuenta de un usuario que no quería decir su nombre (aunque haya una pista en el artículo).


Así que ya sabéis, a partir de ahora es más probable que nos escuchemos a través de mumble que de discord, el cuál sigue siendo un canal oficial de JEL, pero preferimos opciones no privativas.

