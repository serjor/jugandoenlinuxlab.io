---
author: Son Link
category: "Exploraci\xF3n"
date: 2022-02-16 18:21:16
excerpt: "<p>La pr\xF3xima gran actualizaci\xF3n cada vez m\xE1s cerca y con soporte\
  \ para Steam Deck.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/portada-beta.webp
joomla_id: 1430
joomla_url: valheim-0-207-15-beta-publica
layout: post
tags:
- beta
- valheim
title: "Valheim 0.207.15 Beta P\xFAblica"
---
La próxima gran actualización cada vez más cerca y con soporte para Steam Deck.


Como contamos recientemente en el [artículo por el primer año de Valheim]({{ "/posts/1-ano-de-valheim" | absolute_url }}) la próxima gran actualización del juego esta cerca, y es por ello que la gente de **Iron Gate** han publicado una Beta Publica para que todo aquel que quiera probarlo y así dar feedback para ir depurandolo antes de su publicación. Esta actualización trae muchos cambios, los cuales pasaré a resumir más adelante.


Para participar en la Beta hay que seguir los siguientes pasos:


* Lo primero es hacer una copia de seguridad de los datos de guardado, por si el juego falla o salimos de la beta. Estos se encuentra en la siguiente ruta: `**~/.config/unity3d/IronGate/Valheim**`
* Dentro de Steam, en la lista de juegos de tu biblioteca de juegos pulsa con el botón del ratón, o en la rueda de arriba a la derecha si estas en la pagina del juego (en tu biblioteca de juegos) y seleccionar **Propiedades**.
* Una vez abierta la ventana seleccionamos Betas y en el campo de abajo introducimos el código **yesimadebackups** y pulsamos en ***Verificar código***. Una vez hecho podremos seleccionar la beta en el selector de arriba. Una vez hecho se bajara la actualización.


![activar beta](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/activar-beta.webp)


Para reportar fallos, así como cualquier otra cosa que creamos importante, se puede hacer bien a través [del subforo para tal fin en Steam](https://steamcommunity.com/app/892970/discussions/5/) o en la sala #valheim-public-test en su servidor de [Discord](https://discord.gg/valheim).


Recordar que al entrar a la partida se va a regenerar algunas zonas del mapa, por lo que la carga del juego llevara algo de tiempo extra la primera vez que entres, incluso algunas localizaciones de los jefes puede cambiar un poco. Aparte que una vez empezado a jugar no podremos volver a la rama estable del juego hasta la salida de la actualización, por lo que si quieres volver tendrás que salir de la beta y copiar la copia de seguridad previamente hecha.


Las Cuevas de Escarcha, las nuevas zonas que podremos explorar, solo estarán disponibles en los biomas de Montaña que no se hayan explorado. En ellas nos encontraremos con los fenrings (de algún tienen que salir), si es que eres lo suficientemente valiente como para entrar y descubrir sus secretos.


### Arreglos y mejoras:


* Ahora se puede pausar el juego en el modo individual.
* Compatibilidad total con los mandos.
* Entrada de texto en el **Gamepad cuando se ejecuta en el Big Picture.**
* **Correcciones en la interfaz de usuario, en los ajustes y en el Steam Deck.**
* Limitador de FPS y reducción del rendimiento de fondo.
* Ahora las señales se pueden construir sin un banco de trabajo.
* Correcciones de errores y mejoras en la calidad de vida.
* Ajuste del limitador de FPS y opción para reducir el uso de la GPU cuando se minimiza, menú FPS limitado a 60
* Varias correcciones de la interfaz de usuario
* Recetas para Jack-o-turnip + cosas de Yule deshabilitadas


### Calidad de vida:


* El marcador de edificio es más sutil e indica la rotación de piezas.
* Ahora es posible fabricar objetos apilables con el inventario lleno si hay ranuras de apilamiento disponibles.
* Se ha corregido un error por el que a veces los objetos apilables no se cogen cuando se tiene el inventario lleno y se pulsa “Coger todo” en un cofre
* Ahora se puede cerrar el chat usando la tecla Escape, el ratón o el botón B del gamepad


### Soporte para el gamepad:


* ¡Soporte completo para el mando!
* Leyenda del mando visible en el menú de pausa y en los ajustes
* Entrada de texto con el mando cuando se ejecuta en el modo Big Picture de Steam. El chat, los personajes, las señales, las mascotas, etc., ahora pueden nombrarse con el mando.
* Asignación del mando a todas las funciones del juego.
* Los menús contextuales ahora mostrarán siempre los botones del mando cuando se utilice éste.
* La ventana de habilidades se puede desplazar con el gamepad.
* La artesanía se puede cancelar si se pulsa el botón de nuevo (como con el ratón).
* Se han corregido algunas descripciones de herramientas del gamepad que faltaban o estaban mal colocadas.


### Steam Deck:


* El **diseño del mando de Steam Deck se muestra cuando se juega en ella**
* Carga de los **ajustes por defecto adaptados al Steam Deck**


### Localización:


* Mejoras en varios idiomas traducidos por la comunidad


Estos son solo algunos de los cambios que trae. Puedes encontrar más información [en este articulo](https://store.steampowered.com/news/app/892970/view/3131694291117614096) en la página de noticias del juego.


 

