---
author: Pato
category: Rol
date: 2018-05-09 08:30:32
excerpt: "<p>El juego ya est\xE1 disponible en Linux/SteamOS</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e9c1f51a97744aa713c8967fc1a2c65b.webp
joomla_id: 728
joomla_url: albion-online-llegara-a-steam-el-proximo-dia-16-de-mayo
layout: post
tags:
- sandbox
- mmorpg
- mmo
title: "Albion Online llegar\xE1 a Steam el pr\xF3ximo d\xEDa 16 de Mayo (Actualizaci\xF3\
  n)"
---
El juego ya está disponible en Linux/SteamOS

**(Actualización 16/05/18)**


Tal y como estaba estipulado, ya tenemos disponible para su compra **Albion Online**. Puedes visitar su página de Steam desde el enlace más abajo.




---


**(Post original):**


Desde hace ya unos meses que [tenemos disponible](index.php/homepage/generos/aventuras/item/535-albion-online-ya-disponible-para-gnu-linux) en Linux '**Albion Online**', un MMORPG donde podemos "perdernos" junto a otros jugadores dentro de un basto mundo de fantasía medieval, sin embargo faltaba su desembarco en Steam cosa que Sandbox Interactive ha confirmado que sucederá el próximo día 16 de Mayo. Y para celebrarlo darán a los nuevos y antiguos jugadores un **bono de fama de una semana**.


Del 16 a 22 de mayo, todos los jugadores recibirán un 25% más de fama para todas las actividades en el mundo de Albion - recolección, fabricación, matar monstruos y todo lo demás. El bono es automático, así que prepárate para entrar en el juego y subir tus niveles de fama a la luna!


Por otra parte, han lanzado un nuevo trailer lleno de acción dentro del juego para la ocasión:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/kixfZq_VttA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Toda la información del anuncio [en este enlace](https://steamcommunity.com/games/761890/announcements/detail/1671276544955954520).


**Sinopsis:**


*Albion Online es un MMORPG de mundo abierto ubicado en un mundo de fantasía medieval. El juego tiene una economía basada en los jugadores en la que casi todos los elementos son fabricados por jugadores. Combina piezas de armadura y armas con su estilo de juego en un sistema único y sin clases, "Eres lo que vistes". Explorar el mundo, aventurarse en batallas emocionantes, conquistar territorios y construir casas.*


*Fábrica. Comercia. Conquista.*  
*Entra ahora y conviértete en parte de un mundo de fantasía viviente donde todos importan.*


¿Eres de los que ya ha jugado a Albion Online o esperas a su llegada a Steam? Sea como sea, si quieres jugarlo puedes comprar alguno de los paquetes disponibles en su [página web oficial](https://albiononline.com/en/home) y jugarlo ya mismo, o esperar al día 16 donde estará disponible en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/761890/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

