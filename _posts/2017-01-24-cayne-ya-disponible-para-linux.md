---
author: Serjor
category: Aventuras
date: 2017-01-24 22:00:00
excerpt: "<p>Descargable de manera gratu\xEDta desde steam</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f5207093f4bafcb21c0c017e46b49f6e.webp
joomla_id: 189
joomla_url: cayne-ya-disponible-para-linux
layout: post
tags:
- steam
- gratis
- aventura-grafica
- cayne
title: CAYNE ya disponible para Linux
---
Descargable de manera gratuíta desde steam

Cómo ya os lo [adelantábamos](index.php/item/281-stasis-ya-esta-en-fase-de-testeo-cayne-llegara-el-proximo-dia-24), y tal y como nos prometían, [CAYNE](http://www.playcayne.com/) ha sido lanzadado para Linux.


Este juego de horror, un point and click de vista isométrica, nos pone en la piel de Hadley, una mujer embarazada que se despierta en unas instalaciones de las que tiene que escapar si no quiere que le quiten a su futuro bebé.


Propuesta interesante que afortunadamente podremos disfrutar de manera gratuíta.


 


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/9f99QQf4l_w" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/532840/" width="646"></iframe></div>


 


Así mismo os recordamos que los chicos de [The Brotherhood](http://www.thebrotherhoodgames.com/) tienen una campaña en [Kickstarter](https://www.kickstarter.com/projects/bischoff/beautiful-desolation-isometric-post-apocalyptic-ad) para financiar la secuela de este juego, [BEAUTIFUL DESOLATION](http://www.desolationgame.com/), así que si este juego os gusta, no dudéis hacer una aportación para que podamos disfrutar de su secuela.


 


Y tú, ¿piensas jugar a CAYNE? Déjanos tu opinión en un comentario, en nuestro [foro](index.php/foro/categorias), en el canal de [telegram](https://t.me/jugandoenlinux) o nuestro grupo de [discord](https://discord.gg/ftcmBjD).

