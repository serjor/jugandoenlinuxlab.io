---
author: Serjor
category: Rol
date: 2018-08-09 15:00:42
excerpt: "<p>La beta ya est\xE1 disponible para descargar desde Steam</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6f37918316c428e83362ed5eebb78719.webp
joomla_id: 823
joomla_url: los-creadores-de-city-of-shroud-piden-ayuda-para-traer-el-juego-a-gnu-linux
layout: post
tags:
- beta
- rpg
- tactico
- city-of-shroud
title: Los creadores de City of Shroud piden ayuda para traer el juego a GNU/Linux
---
La beta ya está disponible para descargar desde Steam

Interesante noticia la que nos traen los chicos de [Abyssal Arts](https://www.abyssalarts.com/), y es que según [comentan en los foros de steam](https://steamcommunity.com/app/449100/discussions/0/1744468917519729897/), están interesados en traer el juego [City of the Shroud](https://www.abyssalarts.com/shroud?) a GNU/Linux, aunque para ello necesitan ayuda, ya que como ellos mismos reconocen, dado el equipo que son, ni tienen los recursos ni el conocimiento para testear debidamente el juego en la plataforma del pingüino, así que piden abiertamente ayuda a la comunidad para hacerlo posible.


El juego tiene una mecánica muy curiosa, es una especie de RPG táctico donde para atacar tenemos que crear el combo en una rueda de acciones donde vamos eligiendo el tipo de habilidad (o algo así, porque sinceramente no me ha quedado claro para qué sirve cada color), y en el que según las decisiones que vayamos tomando la historia terminará de una manera u otra.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Fp95C4IqAiE" width="560"></iframe></div>


Hay que reconocer que aunque en un primer momento pueda parecer que estén pidiendo a la comunidad que les haga el trabajo de testeo, tenemos que admitir que el tema del soporte es lo que impide que muchos estudios traigan sus juegos a GNU/Linux, y siendo sinceros y pidiendo ayuda parece una forma de llegar a un win-win, ayudando a depurar el juego en nuestro sistema podemos tener el juego en nuestro sistema. ¿El inconveniente? Que parece que el testeo implica que haya que tener el juego, por lo que es complicado que vaya a haber muchos jugadores que hagan dualboot que se hayan comprado el juego para jugarlo en Windows.


 <div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/449100/" style="border: 0px;" width="646"></iframe></div>


Y tú, ¿estarías dispuesto a echar una mano para que llegue a nuestro sistema? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

