---
author: Serjor
category: "Acci\xF3n"
date: 2018-08-01 10:22:16
excerpt: "<p>Para los \"funders\" el Battle Pass ser\xE1 para siempre</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/159fc61fd0fc009a5ca070136f874aef.webp
joomla_id: 812
joomla_url: crazy-justice-ya-tiene-activa-la-pagina-en-steam-y-anuncian-que-el-modo-battle-royale-sera-gratuito
layout: post
tags:
- early-access
- battle-royale
- crazy-justice
title: "Crazy Justice ya tiene activa la p\xE1gina en Steam, y anuncian que el modo\
  \ Battle Royale ser\xE1 gratuito"
---
Para los "funders" el Battle Pass será para siempre

Parece que ya queda menos para que podamos disfrutar de Crazy Justice, ese juego que por los vídeos parece una mezcla de Fortnite y Borderlands, y que lleva unos cuantos retrasos a sus espaldas.



> 
> [#ICYMI](https://twitter.com/hashtag/ICYMI?src=hash&ref_src=twsrc%5Etfw) [#STEAMPAGE](https://twitter.com/hashtag/STEAMPAGE?src=hash&ref_src=twsrc%5Etfw) <https://t.co/sqGYXG6PRg> has been ACTIVATED! Be prepared because in the near future EA (early access) will be available! Who’s gonna rule the [#CrazyJustice](https://twitter.com/hashtag/CrazyJustice?src=hash&ref_src=twsrc%5Etfw) world? [#Wishlist](https://twitter.com/hashtag/Wishlist?src=hash&ref_src=twsrc%5Etfw) the [#game](https://twitter.com/hashtag/game?src=hash&ref_src=twsrc%5Etfw) NOW! [#Gaming](https://twitter.com/hashtag/Gaming?src=hash&ref_src=twsrc%5Etfw) [#Gamers](https://twitter.com/hashtag/Gamers?src=hash&ref_src=twsrc%5Etfw) [#Steam](https://twitter.com/hashtag/Steam?src=hash&ref_src=twsrc%5Etfw) [#NintendoSwitch](https://twitter.com/hashtag/NintendoSwitch?src=hash&ref_src=twsrc%5Etfw) [#XboxOne](https://twitter.com/hashtag/XboxOne?src=hash&ref_src=twsrc%5Etfw) [#PlayStation4](https://twitter.com/hashtag/PlayStation4?src=hash&ref_src=twsrc%5Etfw) [#CJ](https://twitter.com/hashtag/CJ?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/CHJqU2mZEK](https://t.co/CHJqU2mZEK)
> 
> 
> — Black Riddles Studio (@BlackRiddles) [1 de agosto de 2018](https://twitter.com/BlackRiddles/status/1024582995825577984?ref_src=twsrc%5Etfw)







La verdad es que la sensación es un tanto agridulce, porque al menos en mi caso, yo me compré el juego pensando en el modo Battle Royale, y de haberlo sabido, quizás me hubiera ahorrado la compra, no obstante, para aquellos que nos hicimos con el juego la gente de Black Riddles Studio nos ofrece lo siguiente:



> 
> We are happy to announce [#CrazyJustice](https://twitter.com/hashtag/CrazyJustice?src=hash&ref_src=twsrc%5Etfw) ’s [#BattleRoyale](https://twitter.com/hashtag/BattleRoyale?src=hash&ref_src=twsrc%5Etfw) & [#SkillRoyale](https://twitter.com/hashtag/SkillRoyale?src=hash&ref_src=twsrc%5Etfw) mode will be FREE-2-PLAY! It’s great news, isn’t it?! Our appreciated [#backers](https://twitter.com/hashtag/backers?src=hash&ref_src=twsrc%5Etfw) will get REWARDS and FOUNDER’S Pack! <https://t.co/sqGYXG6PRg> Thanks again for your continuous support! [#Blessed](https://twitter.com/hashtag/Blessed?src=hash&ref_src=twsrc%5Etfw) [#Gamers](https://twitter.com/hashtag/Gamers?src=hash&ref_src=twsrc%5Etfw) [#Gaming](https://twitter.com/hashtag/Gaming?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/ZVneJ045wN](https://t.co/ZVneJ045wN)
> 
> 
> — Black Riddles Studio (@BlackRiddles) [1 de agosto de 2018](https://twitter.com/BlackRiddles/status/1024599593206652928?ref_src=twsrc%5Etfw)







Que tampoco está tan mal, aunque lo dicho, quizás algunos, de haberlo sabido, nos habríamos ahorrado la compra.


Podéis visitar su página en steam aquí:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/834730/" style="border: 0px;" width="646"></iframe></div>


 


Y tú, ¿tienes ganas de jugar a este Crazy Justice? Cuéntanoslo en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

