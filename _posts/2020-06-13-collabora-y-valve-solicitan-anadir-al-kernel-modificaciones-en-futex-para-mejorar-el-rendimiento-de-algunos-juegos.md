---
author: Serjor
category: Software
date: 2020-06-13 07:42:30
excerpt: "<p>Estos cambios permitir\xEDan mejorar el rendimiento en algunos juegos\
  \ ejecutados v\xEDa Wine/Proton</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Proton/valve-Proton.webp
joomla_id: 1234
joomla_url: collabora-y-valve-solicitan-anadir-al-kernel-modificaciones-en-futex-para-mejorar-el-rendimiento-de-algunos-juegos
layout: post
tags:
- wine
- valve
- linux
- kernel
- proton
- collabora
title: "Collabora y Valve solicitan a\xF1adir al kernel modificaciones en futex para\
  \ mejorar el rendimiento de algunos juegos"
---
Estos cambios permitirían mejorar el rendimiento en algunos juegos ejecutados vía Wine/Proton


Leemos en [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Futex2-System-Call-RFC) que la gente de Collabora y Valve ya tienen listo su trabajo sobre [futex2](https://lkml.org/lkml/2019/7/30/1399), que iniciaron el año pasado, y ahora ya han abierto el periodo de discusiones sobre esta nueva implementación.


¿Y qué es futex2 y por qué nos alegramos de que pueda acabar siendo incorporado a Linux? Futex es el acrónimo inglés de "fast userspace mutex", que en castellano vendría a poder traducirse como "mecanismo rápido de bloqueo en el espacio de memoria de usuario" (que nadie se enfade por haber traducido y explicado a la vez cada término). Este mecanismo de bloqueo permite a un hilo que se ejecuta en el espacio de memoria del usuario solicitar al kernel un acceso exclusivo sobre alguna dirección de memoria (ejemplo chorra, puntos de vida del personaje principal), de cara a que ningún otro hilo en ejecución quiera escribir a la vez cuantos puntos de vida gana o pierde el personaje (imaginemos que a la vez que nos da una bala, tocamos un corazón que regenera vida, ambos eventos intentarían modificar los puntos de vida a la vez, y eso está prohibido, no se puede modificar a la vez una misma dirección de memoria o un mismo dato, leer sí, se puede leer a la vez las veces que se quiera). Para evitar la escritura concurrente, lo que hacen los hilos o los procesos es pedir permiso, y a través de futex crean un marcador por el que preguntan. Si está libre, lo bloquean, hacen sus cambios en las variables que interesan (los puntos de vida) y lo liberan. Si está bloqueado, esperan a que se libere.


Ahora ya me dejo de rollos, y vamos a qué es futex2, en condiciones. Linux ya tiene la operación futex, pero el problema es que solamente sirve para esperar a un único objeto. En Windows (si estamos hablando de Collabora y Valve, se veía venir, ¿no?) existe la opción de esperar a múltiples objetos a la vez, con lo que Wine tuvo que usar algún mecanismo para dar soporte a esta API a la hora de traducir las llamadas. Hasta la fecha Wine ha venido usando ESYNC/eventfd. La función eventfd permite hacer lo mismo que lo que se quiere hacer con futex2, pero puede acabar generando cuellos de botella, con lo que para mejorar el rendimiento, la gente de Collabora ha propuesto añadir a la función futex que provee el kernel, una opción para que se puedan esperar a varios objetos a la vez, con lo que la CPU se vería liberada al usar el mecanismo apropiado.


Según comentan ellos mismos, con esta implementación son capaces de mejorar un 1.5% en Shadow Of The Tomb Raider y un 4% en Beat Saber.


Podéis ver el RFC [aquí](https://lore.kernel.org/patchwork/cover/1194338/) con una explicación bastante mejor.

