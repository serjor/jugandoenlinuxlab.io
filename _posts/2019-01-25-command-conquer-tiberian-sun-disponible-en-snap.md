---
author: leillo1975
category: Estrategia
date: 2019-01-25 14:24:34
excerpt: "<p>El cl\xE1sico de la estrat\xE9gia puede ser descargado en formato de\
  \ paquetes.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/426eebb550cd46ce73cc8dcc4d1699c9.webp
joomla_id: 958
joomla_url: command-conquer-tiberian-sun-disponible-en-snap
layout: post
tags:
- estrategia
- wine
- clasico
- tiempo-real
- snap
title: '"Command & Conquer: Tiberian Sun" disponible en Snap.'
---
El clásico de la estratégia puede ser descargado en formato de paquetes.

Menudo alegrón me he llevado al ver este juego en la tienda de Snaps ([Snapcraft](https://snapcraft.io/cnctsun)). Uno de mis juegos favoritos de todos los tiempos disponible para jugar en linux sin complicaciones. Como la gran mayoría de vosotros sabeis, [Command & Conquer](https://es.wikipedia.org/wiki/Command_%26_Conquer) es una **serie de juegos de Estrategia en Tiempo Real** que lleva entre nosotros la friolera de 24 años, siendo el primero de sus juegos (**Tiberian Dawn**) lanzado en 1995 con un rotundo éxito por [Westwood Studios](https://es.wikipedia.org/wiki/Westwood_Studios). En él, dos bandos, GDI y NOD, se disputaban el control de la tierra y su principal recurso, el Tiberio.  Poco después le siguó el no menos aclamado **Red Alert,** donde se cambiaba de escenario, y aliados y comunistas combatían en una realidad alternativa donde la guerra fría no era tan fria. Conocido es también la adaptación de la novela de Frank Herbert al terreno de la estrategia, **Dune 2000**, donde Atreides, Harkonnen y Ordos cambiaban la lucha del Tiberio por la Especia. Todos estos juegos marcaron un antes y un después de la estrategia en tiempo real, siendo su fama más que justificada.


Casi llegando al fin del pasado siglo, en 1999, era lanzada la "segunda parte" de la saga Tiberiana, con "Tiberian Sun" que es el título que hoy nos ocupa, en el que s**e llevaba los gráficos del juego a un nuevo nivel** y en el que podíamos ver un pseudo 3D isométrico con elevaciones de terreno, iluminación dinámica y montones de efectos que en aquella época dejaban alucinados a más de uno (yo entre ellos). El juego venía acompañado también de **cinemáticas impresionantes** que no se habían visto nunca hasta el momento y que conseguían sumergirnos en la historia de forma sobresaliente.


Como muchos sabreis, **todos estos juegos fueron liberados por Electronic Arts hace unos cuantos años**, haciendo las delicias de muchos aficionados. Esto ha sido aprovechado por la comunidad y con proyectos como el conocido [OpenRA](index.php/homepage/generos/estrategia/8-estrategia/715-la-actualizacion-de-openra-nos-trae-la-campana-harkonnen-completa), un motor Open Source que recrea y mejora estos juegos (por cierto, recomendadísimo). Gracias a esta liberación **un usuario ha creado un paquete Snap que conjuntamente con Wine nos permite instalar y jugar facilmente a este clásico de nuevo**. Según la descripción del paquete en Snapcraft, en Tiberian Sun encontraremos:


"C&C Tiberian Sun: desarrollado por Westwood Studios y lanzado en 1999. La historia principal sigue a la segunda gran guerra entre la Iniciativa de Defensa Global (GDI) de las Naciones Unidas y la organización terrorista global conocida como la Hermandad de NOD. La historia tiene lugar 30 años después de que el GDI ganara la Primera Guerra de Tiberio en Command & Conquer.


El modo de juego tiene principios similares a Tiberian Dawn, pero con unidades y estructuras más nuevas y mejoradas, además de tres nuevos tipos de tiberio que se pueden cosechar. Tiberian Sun depende en gran medida de las tecnologías de ciencia ficción, e introduce un nuevo motor de juego isométrico con terreno de diferentes niveles para dar la impresión de un verdadero entorno 3D. La iluminación dinámica permite ciclos día/noche y efectos especiales, como tormentas de iones. Los mapas muestran paisajes urbanos donde las unidades pueden esconderse o luchar en combate urbano. Algunos edificios y unidades acorazadas se renderizan con vóxeles, aunque la infantería todavía se renderiza con sprites.


Durante la campaña, se pueden elegir diferentes rutas, algunas de las cuales pueden llevar a misiones opcionales que pueden afectar la dificultad de la misión principal, o suministrar al jugador con unidades y tecnologías adicionales. Tiberian Sun es el último juego de la serie que ofrece la función de "split-route""


El juego además **se puede jugar en resoluciones actuales** por lo que podreis disfrutar de en una calidad mucho mayor, y **viene acompañado por su expansión "Firestorm**". Podeis descargar completamente gratis "Command & Conquer: Tiberian Sun" de [Snapcraft](https://snapcraft.io/cnctsun), o usando una terminal y tecleando:


`sudo snap install cnctsun`


Aquí os dejo un gameplay de Tiberian Sun con la campaña de GDI en Español:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/ga_7xXYelCY" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Jugaste en su día a este magnífico clásico? Si no lo has hecho, ¿le darás una oportunidad? Déjanos tu respuestas u opiniones sobre Tiberian Sun en los comentarios o charla sobre el en nuestro canal de [Telegram](https://t.me/jugandoenlinux)

