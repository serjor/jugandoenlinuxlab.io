---
author: Serjor
category: Web
date: 2019-02-09 15:38:21
excerpt: "<p>Despu\xE9s de las propuestas de la comunidad, llega el momento de elegir\
  \ al favorito</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9ea2fd39c455df0cf2159afcc971c01a.webp
joomla_id: 967
joomla_url: jel-awards-2018-entramos-en-la-fase-de-seleccion-del-ganador
layout: post
tags:
- goty
- jel-awards
- '2018'
title: "JEL AWARDS 2018, entramos en la fase de selecci\xF3n del ganador."
---
Después de las propuestas de la comunidad, llega el momento de elegir al favorito

Después de la primera ronda de selección de candidatos que [os propusimos](index.php/homepage/web/36-web/1072-ayudanos-a-buscar-el-goty-del-2018-comienzan-los-jel-awards), toca la batalla final, la selección del mejor juego del año 2018 según los lectores de jugandoenlinux.com


En esta ocasión, y por motivos de limitaciones de limesurvey, hemos optado por usar google forms, cosa que no nos hace especial ilusión, pero a falta de conocer mejores opciones para hacer encuestas anónimas, hemos pensado que es la opción menos mala.


Podéis encontrar la encuesta en [este enlace](https://goo.gl/forms/21JyxgCPXoNIOahc2), y permanecerá abierta durante el mes de febrero.


¡¡Vota por tu juego favorito!!

