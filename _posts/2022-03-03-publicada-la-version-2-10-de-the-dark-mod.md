---
author: leillo1975
category: "Acci\xF3n"
date: 2022-03-03 15:54:35
excerpt: "<p>El juego #OpenSource acaba de recibir esta nueva actualizaci\xF3n por\
  \ parte de Broken Glass Studios.&nbsp;</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DARKMOD/TheDarkMod-iris.webp
joomla_id: 1444
joomla_url: publicada-la-version-2-10-de-the-dark-mod
layout: post
tags:
- idtech-4
- the-dark-mod
- broken-glass-studios
title: "Publicada la versi\xF3n 2.10 de \"The Dark Mod\""
---
El juego #OpenSource acaba de recibir esta nueva actualización por parte de Broken Glass Studios. 


 Ya no es la primera vez que os hablamos de este juego  libre en [nuestra web]({{ "/tags/the-dark-mod" | absolute_url }}), pero para quien no conozca este título, hay que decir que es un **juego de sigilo** basado en el clásico [Thief](https://es.wikipedia.org/wiki/Thief:_The_Dark_Project), donde nos pondremos en la piel de un ladrón que tendrá que recorrer entre las sombras un **mundo distópico que mezcla la edad media con la época victoriana**. El juego, que **comenzó en 2009 siendo un mod de Doom 3**, se convirtió en un juego independiente en 2013 y usa el motor libre [IdTech 4](https://es.wikipedia.org/wiki/Id_Tech_4) (Enemy Territory, Quake 4, Doom 3...).


 Esta nueva versión, [la 2.10](https://www.thedarkmod.com/posts/the-dark-mod-2-10-has-been-released/), no es para nada menor, e introduce cambios sustanciales en el juego que mejorarán mucho la experiencia del jugador, a la vez que ayudarán a los creadores de mapas en su cometido. Podeis consultar la lista completa en [este enlace](https://wiki.thedarkmod.com/index.php?title=What%27s_new_in_TDM_2.10), pero lo más destacable es lo siguiente:


* **Mejora en los tiempos de carga del juego**, agilizando en gran medida la carga de texturas aprovechando los múltiples núcleos de la CPU y eliminando los retrasos artificiales.
* **Mejoras de rendimiento**: Como parte de las mejoras de rendimiento en curso, las sombras suaves de los stencils con antialising serán más rápidas de renderizar.
* **Manejo de objetos:** El manejo de objetos ahora producirá menos ruido, o ninguno, si se mantiene pulsado el botón "creep". Ahora también puedes controlar la distancia a la que se lanza un objeto en función del tiempo que mantengas pulsado el botón "lanzar". Por último, arrastrar cuerpos inconscientes debería sentirse mejor ahora
* **Interacciones del menú principal:** Se ha añadido información sobre herramientas para muchos de los elementos que se encuentran en el menú de ajustes. También se ha reorganizado el procedimiento para cambiar de misión para que sea más intuitivo para los recién llegados.
* **Cambios en ventanas y entradas:** La versión de Linux se basa ahora en la biblioteca GLFW para crear ventanas y manejar el teclado y el ratón, eliminando muchos de los problemas de integración.
* **Luces volumétricas ("God Rays")**: A diferencia de añadir geometría translúcida falsa alrededor de las luces, las luces volumétricas coinciden automáticamente con su luz de origen, incluyendo la geometría del volumen de luz, las texturas de proyección y desvanecimiento, e incluso las sombras.
* **Personalización de la interfaz gráfica del menú principal:** Los mapeadores pueden cambiar gran parte del menú principal desde un único archivo .gui de "personalización", en lugar de tener que reemplazar los archivos principales.
* **Nuevos Assetts**, como los muebles de madera de lujo o la IA de los autómatas


Os dejamos con el trailer del juego, que a pesar de tener ya unos unos añitos, ilustra perfectamente lo que podeis encontrar en el juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/brJqHnXmpgE" title="YouTube video player" width="720"></iframe></div>


¿Te gustan los juegos de sigilo? ¿Habías jugado antes a "The Dark Mod"? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://t.me/jugandoenlinux), o nuestras salas de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org).

