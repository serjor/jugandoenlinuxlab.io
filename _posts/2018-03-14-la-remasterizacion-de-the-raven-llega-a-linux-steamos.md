---
author: leillo1975
category: Aventuras
date: 2018-03-14 15:31:37
excerpt: <p>@KingArtGames acaba de lanzarla en Steam</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f3fa6960b52e3c842fd94fee5eb122ab.webp
joomla_id: 679
joomla_url: la-remasterizacion-de-the-raven-llega-a-linux-steamos
layout: post
tags:
- king-art
- the-raven
- remasterizacion
title: "La remasterizaci\xF3n de \"The Raven\" llega a Linux/SteamOS"
---
@KingArtGames acaba de lanzarla en Steam

Una de cal y otra de arena. Si ayer nos hacíamos eco de la [negativa de King Art Games a lanzar Iron Harvest para Linux](index.php/homepage/generos/estrategia/item/798-king-art-games-no-garantiza-iron-harvest-para-linux), hoy nos sorprenden con la salida de su remasterización de su clásico "The Raven". Esta remasterización es completamente gratuita para los que ya posean el juego, algo a agradecer a sus desarrolladores.


Según acabamos de leer en el [anuncio oficial](http://steamcommunity.com/games/736810/announcements/detail/1647627572663232861) en Steam, el juego dispondrá ahora de  completas animaciones remasterizadas, iluminación y pelo en resolución FullHD y subtítulos en varios idiomas, entre los que se encuentra por supuesto el castellano. Para quien no conozca "The Raven" se trata de una aventura gráfica lanzada originalmente en el año 2013 y según su descripción en Steam nos vamos a encontrar:



> 
> The Raven Remastered es una remasterización completa de la clásica aventura de misterio que te permitirá explorar una emocionante historia de crimen desde la perspectiva de varios personajes.  
>   
> Londres, 1964. Han robado un antiguo rubí del Museo Británico. En la escena del crimen se ha encontrado una pluma de cuervo. ¿Alguien está imitando al Cuervo, el legendario ladrón desaparecido hace años?  
>   
> El agente de policía Anton Jakob Zellner se encuentra con un misterioso asesinato que hasta ahora solo había leído en sus novelas policíacas favoritas. Nada es lo que parece. Todos tienen algo que ocultar. Y el Cuervo siempre va un paso por delante...  
>   
> CARACTERÍSTICAS PRINCIPALES  
> Una aventura clásica de misterio de KING Art, los creadores de la serie Book of Unwritten Tales  
> Disfruta de una emocionante historia de crimen desde la perspectiva de varios personajes.  
> Lugares clásicos del género, desde un tren de los Alpes suizos a un majestuoso crucero y el Museo del Cairo.  
> Un atractivo elenco de extravagantes personajes con voces de doblaje de alta calidad.  
> Animaciones, iluminación y cabellos plenamente remasterizados en resolución Full HD.
> 
> 
> 


Os dejamos con el trailer del juego remasterizado:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/nk32FQ0O-2g" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 Si no teneis el juego podeis haceros con el en [GOG.com](https://www.gog.com/game/the_raven_remastered?pp=b2a10a6c3dcadb10c8ffd734c1bab896d55cf0ec) y Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/736810/215232/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


 ¿Habeis jugado ya a The Raven? ¿Os gustan las aventuras gráficas? Contéstanos en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

