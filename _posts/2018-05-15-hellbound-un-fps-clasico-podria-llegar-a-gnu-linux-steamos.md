---
author: leillo1975
category: "Acci\xF3n"
date: 2018-05-15 07:13:57
excerpt: "<p>La compa\xF1\xEDa argentina @SaibotStudios ha conseguido financiarse\
  \ grancias a su campa\xF1a de Kickstarter</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/178044b1339d92618b3b1af938fc75de.webp
joomla_id: 734
joomla_url: hellbound-un-fps-clasico-podria-llegar-a-gnu-linux-steamos
layout: post
tags:
- kickstarter
- retro
- hellbound
- saibot-studios
- argentina
title: "Hellbound, un fps cl\xE1sico, podr\xEDa llegar a GNU-Linux/SteamOS (ACTUALIZADO)"
---
La compañía argentina @SaibotStudios ha conseguido financiarse grancias a su campaña de Kickstarter

**ACTUALIZADO 8-6-18**: Finalmente se ha conseguido superar el objetivo de financiación (40000$) de la [campaña de Kickstarter](https://www.kickstarter.com/projects/tobiasrusjan/hellbound-a-90s-first-person-shooter-made-20-years) que necesitaban los chicos de Saibot Studios para poder llevar a buen puerto su juego, por lo que desde JugandoEnLinux queremos felicitarles. Podeis ver el tweet del anuncio aquÍ:



> 
> We f\*\*\*ing did it guys! I can't believe it ?  
>   
> We're going to create somehting BIG with this!  
>   
> Get ready for the real s\*\*t! ??<https://t.co/AdGnwoBuH8>  
>   
> Thank you once again! You guys rock ❤️  
>   
> Cheers from the happy-crazy team at [@SaibotStudios](https://twitter.com/SaibotStudios?ref_src=twsrc%5Etfw) ![#FPS](https://twitter.com/hashtag/FPS?src=hash&ref_src=twsrc%5Etfw) [#DOOM](https://twitter.com/hashtag/DOOM?src=hash&ref_src=twsrc%5Etfw) [#quake](https://twitter.com/hashtag/quake?src=hash&ref_src=twsrc%5Etfw) [#indiegame](https://twitter.com/hashtag/indiegame?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/Wu7H7ZLbTo](https://t.co/Wu7H7ZLbTo)
> 
> 
> — Hellbound (@HellboundGame) [June 8, 2018](https://twitter.com/HellboundGame/status/1005094759290023936?ref_src=twsrc%5Etfw)


 


Como sabeis, los desarrolladores han ligado el soporte para nuestros sistemas a una buena campaña de financiación. Esperemos que esta cantidad siga creciendo y los jugadores linux tengamos la oportunidad de disfrutar de este juego.


 




---


**NOTICIA ORIGINAL**: Como sabeis, ultimamente llevamos una buena ristra de FPS's retro. [Ion Maiden](index.php/homepage/generos/accion/item/786-3d-realms-vuelve-ion-maiden-llega-a-gnu-linux-steamos-en-acceso-anticipado) o [Apocryph](index.php/homepage/generos/accion/item/842-apocryph-anuncia-su-lanzamiento-para-el-proximo-27-de-abril) son buena muestra de ello. Quizás el **efecto nostalgia** para muchos (entre los que me incluyo), o el descubrimiento de una jugabilidad endiablada para otros sea parte de esta bendita fiebre por volver al pasado. ...Y es aquí donde entran los bonairenses [Saibot Studios](http://www.saibotstudios.com/), presentando sin complejos un FPS al estilo noventero pero con la tecnología y potencia actuales.


Recientemente han publicado su [campaña en Kickstarter](https://www.kickstarter.com/projects/tobiasrusjan/hellbound-a-90s-first-person-shooter-made-20-years) donde desgranan con detalle lo que quieren que sea su juego y donde invertirán el dinero recaudado. El juego tiene referencias claras de los grandes clásicos de los 90, como Duke Nukem, Quake o Doom, y es así como pretende ser. A lo largo de la acción nos "encargaremos" de incontables monstruos y demonios gracias a una buena colección de "originales" armas como bates, escopetas o lanzamisiles. El juego hará uso del motor **Unreal Engine 4**, lo que en teoría facilita el trabajo a la hora de dotar de soporte para Linux, y es a la vez garantía de calidad en cuanto a gráficos se refiere gracias a su enorme potencial.


![Hellbound2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Hellbound/Hellbound2.webp)


En cuanto a su versión para nuestros sistemas, nos hemos puesto en contacto con la compañía y esto es concretamente lo que nos ha contestado **Tobías Rusjan**, el director, diseñador y programador del proyecto:



> 
> *""Si, queremos portear el juego a Linux eventualmente, como lo hicimos en nuestros juegos anteriores de la saga [Doorways](https://store.steampowered.com/bundle/1182/Doorways_All_Chapters_Collection/).*  
>  *Pero actualmente estamos trabajando solo con Windows para poder tener más control de lo que hacemos.*  
>  *Hemos agregado Objetivos Adicionales (Stretch Goals) a la campaña de Kickstarter para portear el juego a Linux (también a Mac y consolas).*  
>  *Esperamos poder alcanzarlos.""*
> 
> 
> 


Hablando sobre las tiendas en las que podremos adquirir el juego en caso de que tengamos soporte nos ha comentado:



> 
> *""- Si la campaña de Kickstarter va bien, el juego final (pago) estará disponible en Steam.*  
>  *Quizá lo subamos también a otras tiendas en-línea, pero comenzaremos solo por Steam, que es donde se concentra la mayor cantidad de jugadores de PC del mundo.""*
> 
> 
>  
> 
> 
> 


Actualmente [se puede disfrutar del modo supervivencia de Hellbound](https://store.steampowered.com/app/802200/Hellbound_Survival_Mode/) en Acceso Anticipado de forma gratuita para los usuarios de Windows... Esperemos que finalmente los usuarios de GNU-Linux y SteamOS corramos la misma suerte y podamos divertirnos con el juego al completo. Aquí os dejamos con el trialer del juego:



> 
> *<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/CyabhVn1SfQ" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>*
> 
> 
>  
> 
> 
> 


 


Es un orgullo para JugandoEnLinux.com tener la posibilidad de ofrecer información sobre títulos desarrollados por compañías de habla hispana, y desde aquí nos gustaría desearles a Saibot Studios la mayor de las suertes con su nuevo proyecto. También esperamos que consigan la financiación suficiente como para que les sea posible traernos una pizca más de los 90 a nuestros Linuxeros escritorios. Ya sabeis que en esto último podeis tomar parte y [colaborar](https://www.kickstarter.com/projects/tobiasrusjan/hellbound-a-90s-first-person-shooter-made-20-years). Desde aquí os mantendremos informados de cualquier noticia que se produzca alrededor de este título.


¿Os gusta lo que propone Saibot Studios con Hellbound? ¿Qué opinión os merece el resurgimiento de este estilo de juego? Cuéntamelo en los comentarios, o en nuestros grupos de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

