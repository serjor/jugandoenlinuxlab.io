---
author: Jugando en Linux
category: Apt-Get Update
date: 2022-04-08 21:48:49
excerpt: ''
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Kombinera/kombinera.webp
joomla_id: 1459
joomla_url: nuevos-juegos-con-soporte-nativo-08-04-2022
layout: post
title: Nuevos juegos con soporte nativo 08/04/2022
---

Lo cierto es que esta semana tenemos la sección de novedades algo escasa, sobre todo si buscamos títulos que "merezcan la pena"... 


### Kombinera


DESARROLLADOR: Graphite Lab


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/smNvaNSnAR0" title="YouTube video player" width="720"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1827740/" width="646"></iframe></div>




---


 


### OFERTAS


### HYPERCHARGE: Unboxed


DESARROLLADOR: Digital Cybercherries


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/ll8ARE024go" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/523660/" width="646"></iframe></div>


 




---


### 7 Days to Die


DESARROLLADOR: The Fun Pimps


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/V5dWpZXrnwY" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/251570/30741/" width="646"></iframe></div>




---


### Papers, Please


DESARROLLADOR: Lucas Pope


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/_QP5X6fcukM" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/239030/" width="646"></iframe></div>




---


### PAYDAY 2


DESARROLLADOR:OVERKILL - a Starbreeze Studio.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/o3AxEVMGnBo" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/218620/" width="646"></iframe></div>




---


### Dust: An Elysian Tail


DESARROLLADOR: Humble Hearts LLC


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/W-ephv29Zds" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/236090/" width="646"></iframe></div>




---


###  Pillars of Eternity II: Deadfire


DESARROLLADOR: Obsidian Entertainment


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/W63cZxreR7g" title="YouTube video player" width="720"></iframe></div>


[Enlace a la página de la oferta en GOG.](https://www.gog.com/en/game/pillars_of_eternity_2_game)

