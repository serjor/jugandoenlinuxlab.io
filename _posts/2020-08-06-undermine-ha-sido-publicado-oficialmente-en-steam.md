---
author: Serjor
category: Aventuras
date: 2020-08-06 15:29:06
excerpt: "<p>Despu\xE9s de su paso en estado de Early Access y valorado de manera\
  \ muy positiva, llega a su versi\xF3n estable</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Undermine/undermine.webp
joomla_id: 1246
joomla_url: undermine-ha-sido-publicado-oficialmente-en-steam
layout: post
tags:
- roguelike
- undermine
title: UnderMine ha sido publicado oficialmente en Steam
---
Después de su paso en estado de Early Access y valorado de manera muy positiva, llega a su versión estable


La gente de [Thorium](https://undermine.game/) nos informa de que UnderMine ya ha salido oficialmente del acceso anticipado y ha llegado su primera versión estable.


UnderMine es un roguelike en el que encarnaremos a varios mineros (varios porque esto va de morir, morir muchas veces, y en cada iteración nos encarnaremos en un nuevo explorador), con la suicida misión de descubrir cuál es el origen de unos extraños temblores. Para ello deberemos llegar a lo más profundo de la mina a través de varios niveles, en la que encontraremos una serie de peligros y enemigos que tendremos que ir superando, mientras que recogemos oro y otros elementos que nos ayudarán en nuestra aventura.


Si soléis visitar nuestro canal de [YouTube](https://www.youtube.com/channel/UC4FQomVeKlE-KEd3Wh2B3Xw) podréis encontrar un gameplay del mismo, en el que os mostramos una pequeña pincelada de lo que el juego ofrece.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/KYQGb7Z50bs" width="560"></iframe></div>


Personalmente no soy muy fan de este tipo de juegos, pero reconozco que durante las pruebas el juego me ha entretenido bastante. Unas mecánicas sencillas pero sólidas, una ambientación simpaticota, una música de fondo que no sólo no molesta, sino que acompaña muy bien, y unos niveles, que si bien son aleatorios, están bien equilibrados y aparentemente, variados.


Si os interesa, podéis haceros con el en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/656350/" style="border: 0px;" width="646"></iframe></div>


Y tú, ¿tienes ganas de jugar a UnderMine? Dinos qué te parece en los comentarios, o en nuestros canales de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux)

