---
author: P_Vader
category: Plataformas
date: 2021-12-13 18:37:32
excerpt: "<p>Thunderlotus cierra la historia de Spiritfarer con dos nuevos personajes\
  \ a modo de actualizaci\xF3n y lanza su edici\xF3n final.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Spiritfarer/Spiritfarer_Farewell_Edition.webp
joomla_id: 1394
joomla_url: publicado-spiritfarer-farewell-edition-con-su-tercera-y-ultima-actualizacion-para-despedirse
layout: post
tags:
- indie
- plataformas
- construccion
- gestion
- dibujo-a-mano
- gran-banda-sonora
title: "Publicado Spiritfarer Farewell Edition con su tercera y \xFAltima actualizaci\xF3\
  n para despedirse."
---
Thunderlotus cierra la historia de Spiritfarer con dos nuevos personajes a modo de actualización y lanza su edición final.


Thunderlotus acaba de **publicar su tercera y última gran actualización gratuita de Spiritfarer**, cumpliendo con lo prometido de traerlas a lo largo de 2021. Con esta última **cierran finalmente la historia de Stella y a partir de ahora el juego se lanzará como la edición de despedida Spiritfarer Farewell**, en el que se incluirán la totalidad de las actualizaciones. Si ya tenías el juego base recibirás esta edición con el juego completo, incluido la última actualización Jackie & Daria.


 


Ademas viene acompañado con la celebración de que **sobrepasaron el millón de copias vendidas**. No es de extrañar que hayan llegado a esa mágica cifra dado **la calidad del juego como podéis [leer en el análisis]({{ "/posts/analisis-spiritfarer" | absolute_url }}) que hice del juego y ver en la [partida que echamos](https://www.youtube.com/watch?v=cKtonim3AjE)** para enseñaroslo.


 


Ya dentro de la actualización en si, en esta ocasión nos presentan a dos nuevos espíritus. **Jackie que es una hiena muy divertida, aunque algo vulgar**, y es cuidadora del instituto de salud Overbrook, un hospital en ruinas. Por otro lado tenemos a **Daria la murciélago, una paciente con cierta tendencia a ser algo errática** en su comportamiento.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/aVbhDBYadPo" title="YouTube video player" width="780"></iframe></div>


Tras descubrir una extraña neblina cerca de Oxbury, recibirás una carta de Jackie pidiéndote ayuda en Overbrook y de esa manera dará comienzo las nuevas aventuras de estos dos nuevos personajes.


Todo lo incluido en esta actualización es:


**Nuevo contenido**:


* Nuevos espíritus: Jackie, la hiena y Daria, la murciélago
* Nueva Isla: Overbrook
* Nuevo evento: Palacio de la mente
* Actualización de la nueva estación: Colmena
* Nuevas recetas de dulces.
* Nuevas travesuras (misiones secundarias) relacionadas con Ovebrook.


**Mejoras de calidad**:


* Manual del jugador: lee todas las sugerencias, consejos y trucos recibidos anteriormente para Spiritfarer desde el menú de pausa.


**Ajustes en la narrativa**:


* Los dos últimos mascarones de proa cuestan 1 flor extra cada uno.
* Múltiples logros tienen requisitos actualizados:
	+ Spiritfared: El logro ahora requiere 13 espíritus liberados para completarlo, en lugar de 11.
	+ Se mejoraron todos los edificios (se agregó la casa de Jackie)
	+ Logros relacionados con el estado de ánimo:
		- Todos los espíritus extáticos, 5 espíritus extáticos y cualquier espíritu (se agregaron Jackie y Daria a los requisitos o espíritus potenciadores).
	+ Todas las colecciones recopiladas (actualizado con nuevas recetas).


 


**Sinceramente creo que esta obra de arte es un regalo perfecto para estas naviades** ¿Y tu, te lo piensas regalar? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

