---
author: leillo1975
category: Rol
date: 2020-07-12 16:32:13
excerpt: "<p>La noticia ha sido confirmada por el desarrollador <span class=\"css-901oao\
  \ css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\">@moonraygame. <br /></span></p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Moonray/Moonray01.webp
joomla_id: 1238
joomla_url: moonray-un-rpg-de-accion-en-3-persona-llegara-a-linux
layout: post
tags:
- accion
- acceso-anticipado
- rpg
- unreal-engine
- fig
- moonray
- element-115
title: "Moonray, un RPG de acci\xF3n en 3\xAA persona, llegar\xE1 a Linux"
---
La noticia ha sido confirmada por el desarrollador @moonraygame.   



 A principios del mes de Mayo, en plena época de confinamiento, descubrimos este original y llamativo proyecto, con un **más que notable apartado artístico**. Como suele ser habitual nos pusimos a buscar información sobre él, sobre todo animados por ver que **el juego está desarrollado con el Unreal Engine**, que como todos sabeis tiene soporte para crear juegos nativos para nuestro sistema, entre otros. Dejamos una pregunta en sus [foros de Steam](https://steamcommunity.com/app/1300040/discussions/0/2264691750500130128/#c2644126542304627330), e inmediatamente obtuvimos la respuesta por parte del desarrollador, [Element 115](https://element115.design/moonray),  que nos indicó que no tenían ningún plan de desarrollar una versión para Linux, pero que tampoco estaban cerrados a ello.


Hoy, tras un par de meses, se volvían a poner en contacto con nosotros para confirmarnos que tras algunas deliveraciones, **finalmente han tomado la decisión de ponerse manos a la obra para que los jugadores linuxeros también podamos disfrutar de este título**:


***"We've discussed this more and are planning on a Linux build as well, so you should be good to go"** ("Hemos discutido esto un poco más y también estamos planeando una compilación de Linux, por lo que debería estar listo para empezar.")*


También es importante resaltar que desde hace poco **el juego está buscando apoyos en [Fig.com](https://www.fig.co/campaigns/moonray)**, donde está teniendo un éxito más que notable (en este momento ha superado en un 569% el umbral de financiación), y donde si os gusta el proyecto, por supuesto podreis aportar vuestro granito de arena.


Se esperaba **lanzar el juego en Acceso Anticipado** este mismo mes de Julio, pero finalmente este evento se verá retrasado hasta el **primer cuarto del año que viene**, cuando se podrá adquirir en [Steam](https://store.steampowered.com/app/1300040/Moonray/); y **lanzándose la versión final un año después**. En este título encontrareis según su descripción oficial un juego con las **siguientes características**:


*Moonray es un juego de rol de acción en tercera persona ambientado en un mundo de ciencia ficción surrealista. Explora un paisaje hermoso y mortal en una búsqueda para recuperar el elemento sagrado y recuperar el control del planeta.*


*Con una banda sonora de clase mundial del reconocido productor Ten Walls y una increíble historia del virtuoso artista de cómics Grim Wilkins, Moonray es un viaje impresionante con intensos combates y un estilo inolvidable.*


*Mejora tus habilidades, armas y habilidades para sobrevivir al brutal ataque del culto. Lucha a través de grandes espacios abiertos y corredores estrechos e intrincados como en una lucha titánica para recuperar el elemento sagrado miium.*


*Explora un paisaje surrealista de ciencia ficción para desbloquear todos los misterios ocultos en Moonray.*


Por supuesto nosotros permanceremos atentos a las noticias que el juego vaya generando para informaros puntualmente sobre el avance de este proyecto. Os dejamos con un trailer del juego:  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/ANbGHCFFp7s" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


¿Os parece atractivo este proyecto? ¿Os gustan los juegos de rol en 3ª persona? Podeis darnos vurstra opinión sobre Moonray en los comentarios de este artículo, en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

