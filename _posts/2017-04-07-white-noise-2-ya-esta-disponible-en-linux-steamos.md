---
author: Pato
category: Terror
date: 2017-04-07 16:41:39
excerpt: "<p>\xBFTe van los sustos?</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f203d630ce0c3265f9c9461092194e6b.webp
joomla_id: 284
joomla_url: white-noise-2-ya-esta-disponible-en-linux-steamos
layout: post
tags:
- indie
- terror
- supervivencia
- equipos
title: "'White Noise 2' ya est\xE1 disponible con soporte en Linux/SteamOS en su lanzamiento"
---
¿Te van los sustos?

'White Noise 2' nos llega hoy para ofrecernos una experiencia terrorífica... ¡y a la vez muy divertida!


El juego es obra de los chicos de "Milkstone Studios" [[web oficial](http://www.milkstonestudios.com/)]  estudio que ya nos ha traido a Linux unos cuantos juegos como "Pharaonic" o "Little Racers Street" y que ahora nos propone la secuela de los anteriores White Noise y White Noise Online, pero esta vez en un juego asimétrico de 4 para 1 con el que la diversión entre los jugadores o en solitario puede estar bastante bien.


El juego básicamente consiste en que hay un grupo de investigadores que están en una propiedad investigando extraños sucesos, y durante su investigación se encontrarán con algunas sorpresas, o sobresaltos. La ambientación opresiva y la luz de las linternas en la oscuridad harán el resto.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/X2yeJ_0C_2E" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Sinopsis de Steam:



> 
> *¡Vive una nueva experiencia en el horror con White Noise 2!*  
> *-White Noise 2 es la secuela del exitoso White Noise Online. Forma parte del equipo de investigadores, ¡o controla a la criatura y devóralos a todos!* 
> 
> 
> *White Noise 2 ofrece una experiencia de terror asimétrico 4vs1 que no te dejará indiferente.*
> 
> 
>   
> *Juega con tus amigos*  
> *White Noise 2 ofrece un sencillo sistema de matchmaking que te permite jugar con tus amigos sin complicaciones. Entra en la partida, elige si quieres ser el monstruo, un investigador, o dejarlo al azar, ¡y a jugar!*
> 
> 
>   
> *Los investigadores han vuelto*  
> *Como investigador, explora el escenario, coopera con tus compañeros, y asegúrate de no perderte, ¡o serás presa fácil para la criatura! Evita perder la cordura y vigila el nivel de batería de tu linterna para no quedarte totalmente a oscuras.*
> 
> 
>   
> *¡Y la criatura también!*  
> *Como criatura, acecha a tus presas y cázalas sin ser vista. La luz te aturde, así que utiliza tus poderes para evitarla y provocar el caos entre los investigadores, separándolos para que sean presa fácil. Invoca totems que te avisarán de la presencia de investigadores cercanos, y evita que recojan las 8 cintas antes de que acabes con ellos!*
> 
> 
>   
> *Vuelve de la tumba*  
> *En White Noise 2, la muerte no es el fin de la partida. Levántate como un espectro y aprovecha tus capacidades de exploración mejoradas para ayudar a los investigadores restantes en la búsqueda.*
> 
> 
>   
> *Desbloquea nuevos objetos*  
> *Desbloquea nuevos personajes y linternas a medida que juegas, incluyendo los personajes clásicos de White Noise Online. Cada personaje tiene diferentes especializaciones y atributos. ¿Prefieres un estilo de juego más sigiloso, o te centras en la velocidad aunque se te oiga a kilómetros? ¿Mejor duración de linterna, o mejorar tus habilidades de exploración?*
> 
> 
>   
> *Revive los mejores momentos*  
> *Por supuesto, la aclamada pantalla de repetición ha vuelto en White Noise 2. Comprueba la ruta que los jugadores siguieron. ¡Cuando la terrorífica partida termina, échate unas risas viendo cómo el equipo caminaba en círculos, o uno de los jugadores se perdía!*
> 
> 
> 


No me diréis que con estas premisas el juego no promete... ¿quién no ha jugado a los sustos alguna vez?


En cuanto a los requisitos, estos son:


**Mínimos:**


SO: Ubuntu 12.10 o superior  
Procesador: Dual Core   
Memoria: 2 GB de RAM  
Gráficos: Tarjeta OpenGL 2   
Almacenamiento: 1 GB de espacio disponible  
Notas adicionales: Para tarjetas gráficas AMD, se recomiendan drivers MESA superiores a 10.1.5


**Recomendados:**


SO: Ubuntu 16.10 o superior  
Procesador: Quad Core  
Memoria: 4 GB de RAM  
Gráficos: GeForce 660 o superior  
Red: Conexión de banda ancha a Internet  
Almacenamiento: 1 GB de espacio disponible


Si quieres disfrutar de este 'White Noise 2' lo tienes disponible en español en su web de Steam:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/503350/" width="646"></iframe></div>


 


¿Qué te parece la propuesta de 'White Noise 2'? ¿Serás investigador o criatura?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en Telegram o Discord.

