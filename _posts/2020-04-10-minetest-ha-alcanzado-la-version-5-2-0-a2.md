---
author: leillo1975
category: "Acci\xF3n"
date: 2020-04-10 13:04:15
excerpt: "<p><span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\"\
  >Interesantes novedades en este desarrollo de @MinetestProject. </span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Minetest/Minetest_4.webp
joomla_id: 1206
joomla_url: minetest-ha-alcanzado-la-version-5-2-0-a2
layout: post
tags:
- open-source
- software-libre
- minetest
- clon
- minecraft
title: "Minetest ha alcanzado la versi\xF3n 5.4.0"
---
Interesantes novedades en este desarrollo de @MinetestProject. 


****ACTUALIZADO 24-2-21**:** Se acaba de lanzar una nueva revisión, la 5.4.0, de este motor de juegos con voxels libre, que incluye [numerosos cambios](https://dev.minetest.net/Changelog#5.3.0_.E2.86.92_5.4.0), entre los que [destacan los siguientes](https://forum.minetest.net/viewtopic.php?f=18&t=26322&sid=a3b4113b36cc1907aad5f706b8495329):


  
-Se ha añadido soporte para mostrar objetos adjuntos mientras se está en primera persona (Jordach)  
-ContentDB: Resolución automática de dependencias, actualización de todo y colas de descarga  
 -Mejora de la asignación de entrada, permite cambiar las asignaciones del ratón  
 -Montones de mejoras en Formspec, incluyendo modelos 3D, más estilos y optimizaciones  
 -Muchas correcciones de errores, incluyendo varios parches de seguridad


Las descargas están como siempre en su sección correspondiente de su [página web](https://www.minetest.net/downloads/).




---


 **ACTUALIZADO 11-7-20**: Hace 3 meses exactos os hablábamos de la ultima actualización de este juego open source, y hoy volvemos para daros la noticia del lanzamiento de la versión 5.3.0, donde encontraremos montones de importantes características y fixes. A nuestro juicio las principales novedades están en la **mejora de la usabilidad de la API para la creación de Mods** y los **cambios introducidos  para mejorar el control** por parte del usuario; pero si quereis más concreción os recomendamos que leais la [lista de cambios](https://dev.minetest.net/Changelog#5.2.0_.E2.86.92_5.3.0), donde podreis ver las diferencias con respecto a la versión anterior. Os dejamos con un video de Minetest que recientemente nos ha enviado nuestro amigo **P_Vader "& Family"**:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/_-UKopX3qNM" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>




---


 **NOTICIA ORIGINAL**: Ha llovido más de un año desde [la ultima vez que os hablamos de este clon libre del conocidisimo Minecraft](index.php/component/k2/5-accion/1106-minetest-llega-oficialmente-a-la-version-5-0), y en ese tiempo sus desarrolladores han trabajado duro para traernos una nueva versión cargadita de novedades. Aunque el aviso nos llegó a través de nuestro amigo @P_Vader, el anuncio oficial era lanzado en Twitter hace algunos dias:



> 
> [#Minetest](https://twitter.com/hashtag/Minetest?src=hash&ref_src=twsrc%5Etfw) 5.2.0 has been released!  
>   
> Includes improvements to GUI (a HTML-like rich text element, more styling), graphics (sky rendering API, object shading), tool wear, and more  
>   
> Changelog: <https://t.co/GMNYB4myVr>  
> Downloads: <https://t.co/jsNzG6wqtz>
> 
> 
> — Minetest (@MinetestProject) [April 5, 2020](https://twitter.com/MinetestProject/status/1246843315200327680?ref_src=twsrc%5Etfw)







La lista de cambios de esta versión con respecto a la anterior es enorme y [podeis consultarla en este enlace](https://dev.minetest.net/Changelog#5.1.0_.E2.86.92_5.2.0), pero entre todas ellas destacan las siguientes:


-**Mejoras en la GUI:** un elemento de texto enriquecido similar a HTML, un mejor estilo,


-**Mejores gráficos:** nueva API de representación de cielo y sombreado de objetos


-**Desgaste de punzón**


-Correcciones de **buscador de ruta**


-T**raducciones** nuevas y actuliazadas para Minetest


Como siempre la mejor forma de ver todas estas novedades de este "**motor de juegos de voxeles de código abierto"**, es descargándoos su ultima versión y divirtiéndoos con ella, por lo que podeis iros a la [sección de descargas de su página oficial](https://www.minetest.net/downloads/) y obtnerla para Linux o cualquiera de los otros sistemas que soporta (Windows, MacOS, FreeBSD y Android). Si quereis estar más al tanto del proyecto y colaborar podeis visitar su [página de Github](https://github.com/minetest/minetest).


Y tu, ¿has jugado ya a Minetest? ¿Cual es tu opinión comparándolo con Minecraft? Puedes darnos tu opinión sobre Minetest en los comentarios de este artículo, en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

