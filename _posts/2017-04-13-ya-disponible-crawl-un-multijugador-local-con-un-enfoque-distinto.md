---
author: Pato
category: "Acci\xF3n"
date: 2017-04-13 10:33:08
excerpt: "<p>Enfr\xE9ntate a tus amigos con estilo \"retro\"</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6a3568ec75354456ab1e6b6fc1540ba0.webp
joomla_id: 288
joomla_url: ya-disponible-crawl-un-multijugador-local-con-un-enfoque-distinto
layout: post
tags:
- accion
- indie
- multijugador
- rol
- local
title: Ya disponible 'Crawl' un multijugador local con un enfoque distinto
---
Enfréntate a tus amigos con estilo "retro"

Repasando las novedades que han aparecido estos días he encontrado este 'Crawl' [[web oficial](http://www.powerhoof.com/crawl/)] que parte de una premisa distinta en cuanto a su jugabilidad en su multijugador local.


La esencia del juego es que debemos recorrer en solitario una serie de mazmorras creadas aleatoriamente buscando la salida, y enfrentándonos a todos los monstruos y enemigos que nos encontremos en nuestro camino hasta llegar al jefe final, pero, y aquí es donde tiene gracia el asunto, los enemigos estarán gobernados por hasta tres jugadores mas (en multijugador local) que se enfrentarán a ti con el objetivo de impedir que consigas tu objetivo.


Si alguno de tus "amigos-enemigos" consigue derrotarte tomará tu rol, y entonces todos comenzareis a luchar contra el tratando de impedir su progreso. Así, el juego se convierte en una "carrera" por ver quién llega más lejos y consigue más experiencia.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/WY0N12w3csI?list=PLzVQuv63wFJDieyYkAZBr91pIMN50wBe2" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 También puedes jugar en solitario jugando contra bots, pero la gracia está en jugar junto a otros amigos y echar unas risas, cosa que los juegos cada vez están descuidando más.


En cuanto a su estética, es totalmente "pixel retro", lo que hace que no pida apenas requisitos para moverlo. Eso sí, utilizar gamepads es áltamente recomendable para poder disfrutar el título con garantías.


¿Qué te parece la propuesta de 'Crawl'? para mi es de esas ideas que te preguntas cómo no se le había ocurrido a alguien con anterioridad.


Si te interesa este 'Crawl' debes saber que no está disponible en español, pero si eso no es problema para ti, lo tienes disponible en su página de Steam, con un 20% de descuento por su lanzamiento hasta el día 18:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/293780/" width="646"></iframe></div>


 

