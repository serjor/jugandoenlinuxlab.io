---
author: Pato
category: Editorial
date: 2017-08-28 18:22:15
excerpt: "<p>\xBFNos sigues en Twitter? si no es as\xED... \xA1ya est\xE1s tardando!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6e6b736f77dd07b0a373e582bb4b1e3c.webp
joomla_id: 445
joomla_url: bases-de-los-sorteos-de-videojuegos-del-aniversario-de-jugandoenlinux-com
layout: post
title: Bases de los sorteos de videojuegos del aniversario de Jugandoenlinux.com y
  primer sorteo
---
¿Nos sigues en Twitter? si no es así... ¡ya estás tardando!

En primer lugar, dejar claros algunos puntos:


1.- Los sorteos se llevarán a cabo mediante sistemas de sorteos externos y ajenos a nosotros, y se sortearán entre los usuarios y visitantes de Jugando en Linux que cumplan con los requisitos que pedimos. En ningún caso los editores de jugandoenlinux.com entrarán a formar parte de los sorteos.


2.- Los juegos se entregarán "tal cual", mediante sistemas de regalos o claves de Steam u otros servicios similares. Es posible que requiramos cierta información o que te unas a estos servicios para entregarte el premio.


3.- No se permitirá el cambio del juego por dinero. Tampoco se permitirán devoluciones. 


4.- Si por cualquier circunstancia un juego concreto no lo pudiéramos entregar, Jugando en Linux se reserva el derecho de cambiarlo por otro.


5.- Cualquier situación que pueda darse y que no esté estipulada en estos puntos, Jugando en Linux se reserva el derecho de arbitrar y emitir un fallo que será definitivo al respecto.


Una vez que hemos dejado las cosas claras, ¡vamos con el primer sorteo!


¿Nos sigues en Twitter? si no es así ya estás tardando, por que el primer sorteo va a ser allí. Los juegos que sortearemos para dos afortunados son:


¡[Tiny and Big: Grandpa's Leftovers](http://store.steampowered.com/app/205910/Tiny_and_Big_Grandpas_Leftovers/) y [Proteus](http://store.steampowered.com/app/219680/Proteus/)! (son claves de Steam)


Sigue estos pasos:


1.- Entra en nuestro [canal de Twitter](https://twitter.com/JugandoenLinux) y si no le has dado ya, dale un "me gusta".


2.- A continuación, entra en el hilo del sorteo y **publica un pantallazo de tu escritorio Linux con la web de jugandoenlinux.com abierta** y el hastag [#sorteojugandoenlinux](https://twitter.com/hashtag/sorteojugandoenlinux?src=hash)


Eso es todo. Fácil, ¿no?


A cada comentario con un pantallazo se le asignará el número con el orden en que se ha publicado, y luego se sorteará un número al azar. Si el número resulta no ser válido se repetirá el sorteo que se podrá seguir en directo mañana miércoles en nuestro canal de Twitch a las 22:30 hora española. (GTM+2)


¿Tienes dudas? déjamelas en los comentarios y las responderemos.


¿A qué esperas? ¡díselo a todos tus amigos linuxeros y mucha suerte!

