---
author: Pato
category: Noticia
date: 2018-10-16 13:36:47
excerpt: "<p>Hace unos d\xEDas expres\xF3 su deseo de trabajar en el nuevo juguete\
  \ de Valve... y se ha cumplido</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e840dda4af545e4c1b65faeabcfc7bf7.webp
joomla_id: 877
joomla_url: ethan-lee-pone-en-modo-mantenimiento-sus-otros-desarrollos-y-comienza-a-trabajar-en-proton
layout: post
tags:
- valve
- ethan-lee
- proton
title: Ethan Lee "pone en modo mantenimiento" sus otros desarrollos y comienza a trabajar
  en Proton
---
Hace unos días expresó su deseo de trabajar en el nuevo juguete de Valve... y se ha cumplido

Ethan Lee de sobra es conocido por los asiduos a Jugando en Linux ya que es un muy conocido "porter" que ha traído muy buenos juegos a nuestro sistema favorito.


Pyre, Full Metal Furies, Celeste, Wonder Boy u Owlboy son solo una muestra de los títulos en los que ha trabajado, además del desarrollo de conocidas herramientas de compatibilidad que él mismo ha desarrollado y mantiene, como FNA. Sin embargo el lanzamiento de Proton por parte de Valve supuso todo un cambio para Ethan que poco después mostró un gran interés en la nueva herramienta de Valve, llegando a [mostrar su deseo](https://www.gamingonlinux.com/articles/game-porter-ethan-lee-gives-his-thoughts-on-valves-steam-play-and-proton.12542) de trabajar en Proton si "alguien" se lo permitía.


Pues bien, parece que su deseo se ha cumplido a tenor de sus declaraciones en twitter:



> 
> Oh, by the way, for those wondering about my _official_ work, starting tomorrow I'm working on Proton in an official capacity, in partnership with CodeWeavers. Task #1 is FAudio integration!
> 
> 
> — Ethan Lee (@flibitijibibo) [October 15, 2018](https://twitter.com/flibitijibibo/status/1051967505160388614?ref_src=twsrc%5Etfw)


  





Ethan de momento se centrará en la integración de su librería Faudio sobre Wine, aunque reconoce que tiene ya una lista de prioridades en las que trabajar.


Por otra parte, deja FNA en "modo mantenimiento" pues realmente quiere centrarse en su nuevo trabajo a la vez que tratará de seguir con su listado de "ports"


Si quieres saber mas sobre Ethan Lee, puedes visitar su página web personal: [http://www.flibitijibibo.com](http://www.flibitijibibo.com/index.php?page=Home)

