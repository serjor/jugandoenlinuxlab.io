---
author: Pato
category: Rol
date: 2018-01-23 10:57:22
excerpt: "<p>Uno de los responsables del t\xEDtulo as\xED lo ha dicho en los foros\
  \ del juego</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/46276ded2fdd3245a9a8536c50ae55dd.webp
joomla_id: 615
joomla_url: battle-chasers-nightwar-deberia-tener-una-beta-para-linux-manana-mismo
layout: post
tags:
- indie
- rol
- jrpg
- turnos
title: "'Battle Chasers: Nightwar' deber\xEDa tener una beta para Linux ma\xF1ana\
  \ mismo"
---
Uno de los responsables del título así lo ha dicho en los foros del juego

Hace tiempo que seguimos la pista de este 'Battle Chasers: Nightwar', el excelente juego de rol de Airship Syndicate que llegó con polémica tras una campaña de financiación donde se prometía una versión para Linux y que a pocos días del lanzamiento hizo desaparecer el logo de soporte a nuestro sistema favorito.


Todo esto ya os lo contamos hace unos meses [en otro artículo](index.php/homepage/generos/rol/item/595-el-notable-battle-chasers-nightwar-llegara-a-linux-en-forma-de-beta), y desde entonces los desarrolladores no han soltado muchas prendas de si tendríamos pronto una versión beta. Pero hace tan solo unas horas han anunciado en el hilo de seguimiento del foro del juego que deberían tener una versión beta del juego para Linux tan pronto como mañana mismo. Podéis ver el anuncio [en este enlace](http://steamcommunity.com/app/451020/discussions/0/1520386297704229504/?tscn=1516662794#c1693785035827671495).


Battle Chasers: Nightwar [[web oficial](http://www.battlechasers.com/)] es un RPG inspirado en los mejores juegos del género. Adéntrate en las mazmorras, lucha por turnos con el formato clásico de los JRPG, y sumérgete en una historia que progresará a medida que explores el mundo del juego.


* Combate por turnos con un sistema exclusivo de sobrecarga de maná e increíbles estallidos de batalla.
* Explora un mundo repleto de mazmorras, jefes, y amigos y enemigos que aparecerán al azar.
* Mazmorras aleatorias llenas de trampas, rompecabezas y secretos. Usa las habilidades exclusivas de cada héroe para sobrevivir.
* Crea tu equipo con tres de los seis héroes disponibles de los famosos cómics Battle Chasers, cada uno con sus propias características, ventajas, objetos y habilidades.
* Descubre el complejo sistema de creación y usa el exclusivo sistema de sobrecarga de ingredientes para crear objetos épicos.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Wz2MRHAJiyE" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Si Airship Syndicate cumple su palabra, tendrás la oportunidad de probar 'Battle Chasers: Nightwar' en fase beta para Linux/SteamOS mañana mismo desde su página de Steam, donde está completamente traducido al español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/451020/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Qué te parece 'Battle Chasers: Nightwar'? ¿Que piensas sobre la demora en el lanzamiento de este juego?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram.](https://telegram.me/jugandoenlinux)


Gracias @Txema por el aviso.

