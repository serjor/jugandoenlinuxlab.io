---
author: Pato
category: Hardware
date: 2017-11-27 19:02:49
excerpt: "<p>La consola con sistema Linux sigue desvelando detalles de cara a su pr\xF3\
  xima campa\xF1a</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a6026d97db0c19ba76f2c5c141efd324.webp
joomla_id: 553
joomla_url: atari-muestra-las-primeras-imagenes-del-joystick-de-la-ataribox
layout: post
tags:
- proximamente
- ataribox
title: "Atari muestra las primeras im\xE1genes del joystick de la Ataribox"
---
La consola con sistema Linux sigue desvelando detalles de cara a su próxima campaña

Seguimos recibiendo detalles a cuentagotas sobre la futura consola/microPC de Atari. Tal y como ya os hemos contado en artículos anteriores, la [Ataribox](https://ataribox.com/) como ya se la conoce será [una consola con sistema operativo Linux](index.php/homepage/hardware/item/588-ataribox-usara-gnu-linux) personalizado que será capaz de reproducir juegos tanto de la propia Atari como de terceros, [podrá reproducir contenidos](index.php/homepage/hardware/item/613-ataribox-podra-reproducir-contenido-de-servicios-de-streaming-como-hulu-hbo-go-o-netflix) de diversas plataformas de Streaming así como otras plataformas de juegos, y que contará con hardware personalizado de AMD.


Además de esto, también se sabe que Atari llevará a cabo una campaña de financiación en la conocida plataforma de crowfunding indiegogo para recabar fondos y dar la oportunidad a los fans de la marca de apoyarlos en esta aventura hardware. Dicha campaña se debería llevar a cabo "a finales de este año" según las propias palabras de los responsables de Ataribox, sin embargo aún no sabemos fechas definitivas. Pero a falta de más información, ahora nos han desvelado el aspecto de los joysticks de la consola:


![Atarijoy2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/Atarijoy2.webp)


![Atarijoy3](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/Atarijoy3.webp)


Como puedes ver es un joy bastante parecido por no decir casi igual que los de las consolas Atari de los 90. ¿Ves algún detalle interesante en ellos?


¿Qué te parece el joystick de la Ataribox? ¿Esperas a poder echar el guante a esta consola?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

