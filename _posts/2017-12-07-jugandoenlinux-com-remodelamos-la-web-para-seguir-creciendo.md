---
author: Pato
category: Web
date: 2017-12-07 18:44:05
excerpt: <p>Por fin realizamos los cambios que la web necesitaba</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8e7bd2cc5d4f67673f13afdbefce0937.webp
joomla_id: 568
joomla_url: jugandoenlinux-com-remodelamos-la-web-para-seguir-creciendo
layout: post
title: jugandoenlinux.com - Remodelamos la web para seguir creciendo
---
Por fin realizamos los cambios que la web necesitaba

¡Lo prometido es deuda! hace ya unos meses que venimos anunciando en "petit comité" que la web estaba pendiente de una remodelación que nos permitiese introducir más elementos, mejor optimizados y con una mejor organización.


Pues bien. El momento ha llegado, aunque aún faltan algunas cosillas por hacer. Para empezar, ahora toda la web es "autoajustable", es decir que se adapta al formato de lectura que tengamos en cada momento. Por tanto podremos verla correctamente tanto si la vemos desde una pantalla, una tablet o un smartphone.


El módulo de "destacados" de la cabecera ahora ocupa toda la pantalla, dando más relevancia a la información que creemos que así lo merece. Además, se ha añadido un apartado exclusivo para los análisis que tanto nos cuesta hacer. Es un trabajo arduo que lleva horas de preparación y que hasta ahora salvo por el periodo que pasa en "destacado" no ha tenido mayor relevancia, y es algo que debíamos solucionar dándole la importancia que merece.


Por otra parte, ahora tenemos tres secciones centrales en vez de dos, lo que nos permite introducir nuevos elementos, como un apartado para los últimos comentarios en la web, y otro con las **ofertas de la Comunidad,** apartado que esperamos que nos ayudéis a llevar a cabo introduciendo vuestras propias [**ofertas desde el foro**](index.php/foro/ofertas). ¡Si! ¡todas las ofertas que encontréis interesantes las podéis publicar allí y aparecerán en el frontal de la web! así todo el que nos visite podrá ver de un vistazo qué ofertas interesantes hay en nuestro sistema favorito.


Por otra parte, se le ha dado un buen lavado de cara al foro, con un nuevo aspecto más acorde a la web, y esperamos introducir un par de sorpresas dentro de poco que esperamos que os gusten aún más. Por que no, ¡aún no hemos terminado de introducir mejoras y novedades! Y todo gracias a todos los que nos habéis apoyado y nos seguís apoyando en esta aventura. A nuestros colaboradores, a los que nos aportan sus donativos [a través de Paypal](https://www.paypal.com/donate/?token=D26zI67xK2b0d1sArws5z7FSzWlc9_V1dl5iqSHeaimlq2dRHc2ORwvAinmcY1CQvyyixm&country.x=ES&locale.x=ES) (si puedes, páganos un café, ¡que tenemos que mantenernos despiertos y aún no cubrimos gastos!), y a tí que nos lees, gracias por estar ahí, y no utilizar adblocks en nuestra web. ¡Gracias a todos de corazón!


Por supuesto, aún debemos "pulir" algunos aspectos, retocar aquí y allá y puede que se nos haya pasado algo por alto. Para ello os rogamos que todo lo que penséis que está mal en la web nos lo hagáis saber en el [apartado correspondiente del foro](index.php/foro/errores) o directamente en nuestro [canal de Telegram](https://telegram.me/jugandoenlinux).


Desde siempre hemos dicho que **jugandonenlinux.com** era un proyecto para y por la comunidad de jugadores que jugamos en GNU/Linux, SteamOS etc... así que esperamos que os gusten los cambios que hemos introducido, y los que iremos introduciendo en un futuro próximo.


Gracias por seguir ahí.


El equipo de jugandoenlinux.com

