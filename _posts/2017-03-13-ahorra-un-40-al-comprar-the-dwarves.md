---
author: leillo1975
category: Rol
date: 2017-03-13 08:53:06
excerpt: <p>Aprovecha esta oportunidad de hacerte con este gran juego</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3d3b7d5d68132cc424920deb43e754bb.webp
joomla_id: 246
joomla_url: ahorra-un-40-al-comprar-the-dwarves
layout: post
tags:
- rol
- steam
- oferta
- dwarves
title: Ahorra un 40% al comprar The Dwarves
---
Aprovecha esta oportunidad de hacerte con este gran juego

Hace algunos meses [anunciabamos](index.php/homepage/generos/rol/item/237-mas-rpg-para-linux-steamos-con-the-dwarves) la salida para GNULinux/SteamOS de este RPG de [King Art Games](http://www.kingart-games.com/), y poco después le dedicabamos un [completo análisis](index.php/homepage/analisis/item/247-analisis-the-dwarves). En esta ocasión nos dirigimos a vosotros para anunciaros que en este momento teneis una buena oportunidad de hacerlos con este fantástico juego por mucho menos de lo que suele ser habitual, concretamente **24€**, en vez de los 40€ habituales, ya que hasta mañana a las 6 de la tarde, hora española está de oferta con un **40% de descuento**.


 


Para quien no lo conozca, como ya se ha escrito aquí, se trata de un juego de Rol protagonizado por Enanos, donde deberemos comandar nuestro grupo de personajes de todas las clases, contra hordas de Orcos, no-muertos y Albos, y donde podremos parar la acción en cualquier momento, lo que le otorga un componente estratégico importante.. Durante la partida deberemos afrontar diversas situaciones y decisiones que condicionarán la partida, teniendo el juego un estilo narrativo peculiar. Esta basado en el universo creado por [Markus Heitz](https://en.wikipedia.org/wiki/Markus_Heitz) en su saga homónima de novelas, por lo que hará las delicias de todos los amantes de la fantasía.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/F5X7MXfLxEg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Si sois fans de los juegos de rol y fantasía y quereis haceros con este magnífico juego a un precio realmente bueno, solo teneis que pasaros por la tienda de Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/403970/79083/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>

