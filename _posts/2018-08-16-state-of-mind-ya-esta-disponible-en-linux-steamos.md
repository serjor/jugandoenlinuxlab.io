---
author: Pato
category: Aventuras
date: 2018-08-16 14:05:15
excerpt: "<p>El juego de @daedalic sali\xF3 ayer con soporte en Linux de salida</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7586bcdd7a745d843a3897512742ccdb.webp
joomla_id: 835
joomla_url: state-of-mind-ya-esta-disponible-en-linux-steamos
layout: post
tags:
- accion
- indie
- aventura
title: "'State of Mind' ya est\xE1 disponible en Linux/SteamOS"
---
El juego de @daedalic salió ayer con soporte en Linux de salida

Ayer mismo nos llegó 'State of Mind', el nuevo juego de Daedalic Entertainment del que ya dijimos [en un artículo anterior](index.php/homepage/generos/aventuras/item/837-state-of-mind-lo-nuevo-de-daedalic-entertainment-tendra-version-linux) que llegaría a lo largo de este mismo verano.


State of Mind [[web oficial](https://www.daedalic.com/state-of-mind)] es un juego en forma de thriller futurista que se adentra en el transhumanismo. El juego explora temas de separación, descoyuntura y reunificación en un mundo dividido entre una realidad material distópica y un futuro virtual utópico.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/Gh-lBwDbM30" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


**Características:**


* Experimenta una visión alarmantemente realista del futuro cercano
* Sumérgete en un enmarañado thriller de ciencia ficción que entremezcla la realidad distópica con la utopía digital
* Descubre una conspiración global en una sociedad donde el digitalismo, la vigilancia y el transhumanismo están por todas partes
* Explora un mundo que cuenta con un estilo visual único y variado que combina entornos realistas con personajes minimalistas
* Asume el papel del periodista Richard Nolan y de otros cinco personajes jugables adicionales
* Emplea tu destreza, capacidad de deducción e investigación para reconstruir el pasado de Richard


En cuanto a los requisitos, son:


**MÍNIMO:**  

+ **SO:** Ubuntu 16.04, 18.04
+ **Procesador:** 2.8 Ghz Dual Core CPU
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** NVIDIA GeForce 660 / AMD Radeon 7870 o similar, con al menos 2 GB de VRAM
+ **Almacenamiento:** 23 GB de espacio disponible



 **RECOMENDADO:**  

+ **SO:** Ubuntu 16.04, 18.04
+ **Procesador:** 3 Ghz Quad Core CPU
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** NVIDIA GeForce GTX 770 / AMD Radeon R9 300 o similar, al menos 4 GB RAM
+ **Almacenamiento:** 23 GB de espacio disponible


'State of Mind' viene traducido al español, y lo puedes comprar en [Humble Bundle](https://www.humblebundle.com/store/state-of-mind?partner=jugandoenlinux) (enlace de afiliado), [GoG](https://www.gog.com/game/state_of_mind) o su [página de Steam](https://store.steampowered.com/app/437).


