---
author: Serjor
category: "Exploraci\xF3n"
date: 2017-03-20 21:32:13
excerpt: "<p>Esto abrir\xEDa la posibilidad de traer el juego a GNU/Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/84c42b9986b8cecdea81ed6abb66c108.webp
joomla_id: 259
joomla_url: los-desarrolladores-de-star-citizen-podrian-dejar-dx12-en-favor-de-vulkan
layout: post
tags:
- star-citizen
title: "Los desarrolladores de Star Citizen podr\xEDan dejar DX12 en favor de Vulkan"
---
Esto abriría la posibilidad de traer el juego a GNU/Linux

Nos hacemos eco de una [noticia](http://www.gamasutra.com/view/news/294152/Star_Citizen_dev_plans_to_drop_DirectX_support_in_favor_of_Vulkan.php) aparecida en Gamasutra, y es que los desarrolladores de Star Citizen han comentado que, gracias a las similitudes entre ambas APIs, el equipo se está planteando abandonar DirectX 12 para pasar a usar Vulkan, y poco a poco dejar de usar DirectX 11.


Comentan que como DX12 solo está soportado en Windows 10, esto dejaría fuera a todos los usuarios de Windows 7 y 8(.1), y además, también comentan que si se deciden finalmente a usar Vulkan, Linux entraría en sus apuestas, aunque no lo confirman.


Esperemos que finalmente se decidan a dar el paso, ya que es un juego con un gran potencial, y que personalmente, si cumple lo que promete, me encantaría poder disfrutar.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/HaJx4TdshFA" width="560"></iframe></div>


Y a ti, ¿te interesa Star Citizen? Dinos qué te parece el juego y la opción de que llegue a nuestro sistema preferido en los comentarios, o en nuestros grupos de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

