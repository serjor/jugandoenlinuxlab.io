---
author: leillo1975
category: Estrategia
date: 2018-01-05 09:34:44
excerpt: <p>Importantes novedades acerca del desarrollo de <span class="username u-dir"
  dir="ltr">@<b class="u-linkComplex-target">openRA</b></span></p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/64cf547a8aac0c429c1de171c29426f0.webp
joomla_id: 593
joomla_url: la-actualizacion-de-openra-nos-trae-la-campana-harkonnen-completa
layout: post
tags:
- open-source
- command-conquer
- openra
- red-alert
- dune-2000
- tiberian-dawn
- tiberian-sun
title: "La actualizaci\xF3n de OpenRA nos trae la campa\xF1a Harkonnen completa (ACTUALIZADO\
  \ 2)"
---
Importantes novedades acerca del desarrollo de @**openRA**

**ACTUALIZACIÓN 19-6-18:** Acabamos de leer en un [anuncio](http://www.openra.net/news/devblog-20180610/) realizado en su página oficial algunos temas sobre como va el desarrollo del juego. La novedad más destacable es que a partir de ahora **distribuirán sus paquetes mediante AppImage**, para evitar tener que preparar diferentes instaladores según que distribución y evitar la diferencia de versiones entre ellas. También han logrado la **paridad entre el SDK de Mods y los principales mods para OpenRA**, pudiendo empaquetar en AppImage, al igual que en Windows y Mac, además se integrarán correctamente con las listas de servidores para que no aparezca más como Mod Desconocido, permitiendo que el creador pueda incluir sus propios textos. También **se han mejorado las Herramientas y Documentación para las actualizaciones en los mods**, facilitando que los cambios entre versiones sean menos traumáticas y más fáciles de corregir en estos.


Se ha estado trabajando en el rendimiento en el renderizador de gráficos, **duplicando la tasa de frames**, algo especialmente útil para la integración de nuevos mods como Tiberian Sun, que exige más potencia, y muy sobre todo en dispositivos poco potentes como puede ser la Raspberry Pi.


En cuanto al trabajo con **Tiberian Sun** comentan que aunque ya han conseguido hacer jugables algunos aspectos del juego, aun les queda mucho por hacer, y además está solamente en manos de una una persona con el tiempo limitado, por lo que el equipo de OpenRA pide ayuda a la comunidad por si hay alguien con los conocimientos necesarios para echar una mano.


 




---


**ACTUALIZACIÓN 19-2-2018:** A principios de este año os hablábamos de la salida de una "**test release**" del juego. Ahora se trata de la **release (20180218)** en si mismo, por lo que de alguna manera oficializan la anterior y corrigen algunos bugs. A partir de ahora el objetivo principal va a ser el dotar de soporte a [Tiberian Sun](https://es.wikipedia.org/wiki/Command_%26_Conquer:_Tiberian_Sun), que para quien no lo conozca es la continuación de Tiberian Dawn y por supuesto un juego más moderno, completo y detallado. Esperemos ver en próximas releases avances en este juego. Si quereis conocer los detalles de esta última actualización podeis ver este [enlace a su página](http://www.openra.net/news/release-20180218/). Por supuesto os recomendamos que actualiceis si sois usuarios de OpenRA.




---


**NOTICIA ORIGINAL:** Aunque con unos días de retraso, os traemos esta noticia sobre la nueva actualización de [OpenRA](http://www.openra.net/). El día de año nuevo fué lanzada una nueva build que viene cargada de importantes novedades, siendo la más importante la que da título a esta noticia. Como seguro que sabreis, Dune 2000 constaba de 3 facciones diferentes: los Atreides, los Ordos y Harkonnen, siendo esta última añadida en esta versión. También incluye multitud de no menos importantes novedades y mejoras que podeis consultar en [su web](http://www.openra.net/news/playtest-20180102/).


Para quien no conozca OpenRA, se trata de un **proyecto Open Source** que ha creado un motor que recrea y moderniza los juegos clásicos de [Command & Conquer](https://es.wikipedia.org/wiki/Command_%26_Conquer), que como muchos sabreis algunos **fueron liberados hace unos años por Electronic Arts**. Este motor se salta las limitaciones técnicas de estos juegos adaptándolos a la actualidad y permitiendo más y mejores opciones, entre las que destaca la posibilidad de resoluciones mayores a las de los juegos originales, juego multijugador en linea, editor de misiones, interfaz renovada....


![openralogo](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/OpenRA/openralogo.webp)


Con OpenRA podremos disfrutar de los juegos **Tiberian Dawn**, **Red Alert** y **Dune 2000**, así como de diferentes mods. Tan solo debemos ejecutar el que queramos y podremos descargar de forma totalmente libre y gratuita los juegos, pudiendo jugar las misiones de sus campañas, luchar en escaramuzas contra nuestra CPU, o jugar online a través de internet. Además si disponemos de los discos originales de los juegos podremos ver los videos previos a las misiones.También conviene destacar que ese está trabajando en dar soporte a la siguiente generación de juegos de Command & Conquer, intentando dar soporte al grandísimo **Tiberian Sun**, que tantas horas de diversión me proporcionó en su día.


Como veis se trata de un lavado de cara completo a estos juegos de estrategia en tiempo real que marcaron un antes y un después en este género. Desde JugandoEnLinux os iremos informando de las novedades que vaya lanzando este interesante proyecto para que podais disfrutarlas a medida que vayan siendo hechas públicas.


¿Has jugado a los clásicos de Command & Conquer? ¿Qué te parece este iniciativa? Contéstanos en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

