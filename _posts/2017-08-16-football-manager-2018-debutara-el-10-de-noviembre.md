---
author: Serjor
category: "Simulaci\xF3n"
date: 2017-08-16 07:20:06
excerpt: "<p>Con soporte para el d\xEDa 1 y con ofertas para la pre-compra</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6fe1c589e796bff64c81da223cb0c48f.webp
joomla_id: 431
joomla_url: football-manager-2018-debutara-el-10-de-noviembre
layout: post
tags:
- simulador
- football-manager
- sega
title: "Football Manager 2018 debutar\xE1 el 10 de noviembre"
---
Con soporte para el día 1 y con ofertas para la pre-compra

Buenas noticias para los linuxeros amantes de los simuladores de gestión futbollística, y es que gracias a [GOL](https://www.gamingonlinux.com/articles/football-manager-2018-release-date-announced-with-linux-support-once-again.10157) hemos podido saber que la nueva edición de Football Manager para la recién estrenada temporada de fútbol estará disponible a partir del 10 de noviembre de este año, con versión para GNU/Linux desde el día 1, lo cuál es una gran noticia, y confirma el apoyo de Sega en nuestra plataforma preferida.


También podemos leer en dicho artículo que de cara al lanzamiento del juego hay una promoción en marcha, y es que si pre-compramos el juego, y ojocuidao, **en una tienda autorizada por SEGA** podremos acceder a una beta dos semanas antes del lanzamiento oficial del juego, y a pesar de ser una beta, no perderemos los datos del modo individual una vez se lance de manera oficial.


Además, si tenemos la versión del 2017 en Steam, con la pre-compra del juego antes del 9 de noviembre recibiremos un descuento del 25%, así que si os interesa este juego y aún no tenéis el FM2017 podéis aprovechar la oferta del día que hay en Steam en el momento de escribir el artículo, y haceros con la versión del 2017 con una rebaja del 80%


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/482730/" width="646"></iframe></div>


Y por si fuera poco, el juego incluye también la versión Touch, con soporte para Linux también desde el día 1.


Para todos los datos podéis ir a la [página oficial](http://www.footballmanager.com/games/football-manager-2018) del juego


Y tú, ¿eres más de liga o de champions? Cuéntanos cuál sería tu alineación ideal en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

