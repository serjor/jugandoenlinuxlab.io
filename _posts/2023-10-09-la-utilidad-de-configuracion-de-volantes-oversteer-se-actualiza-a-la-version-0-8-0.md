---
author: leillo1975
category: "Software"
date: 09-10-2023 16:28:55
excerpt: "@bernat_arlandis ha añadido soporte a multitud de nuevos dispositivos"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Oversteer/Oversteer.webp
joomla_id: 1547
joomla_url: la-utilidad-de-configuracion-de-volantes-oversteer-se-actualiza-a-la-version-0-8-0
layout: post
tags:
- software
- volantes
- oversteer
title: "La utilidad de configuración de volantes Oversteer se actualiza a la versión 0.8.0"
---
Ya ha pasado un buen trecho desde la última vez que os hablamos de esta **herramienta indispensable** para todos aquellos que disfrutamos de los juegos de conducción con volante en Linux, y volvemos a vosotros para informaros que esta app vuelve a actualizarse trayendo montones de novedades entre las que **destaca la compatibilidad con más modelos de volantes**. Además de eso también hay **correcciones y mejoras importantes** en modelos que ya disfrutaban de soporte. La lista completa de cambios de la [versión 0.8.0](https://github.com/berarma/oversteer/releases/tag/0.8.0) es la siguiente:

*   Dispositivo añadido: Thrustmaster Ferrari F1 wheel attachment.
*   Dispositivo añadido: Thrustmaster Force Feedback.
*   Dispositivo añadido: Open FFBoard.
*   Dispositivo añadido: Thrustmaster TX Racing Wheel.
*   Dispositivo añadido: Thrustmaster T80.
*   Dispositivo añadido: Thrustmaster Ferrari 458.
*   Dispositivo añadido: Logitech Pro Racing Wheel (XBox/PC).
*   Arreglado el mapeo de pedales para Logitech G923 (XBox/PC).
*   Arreglado el mapeo de controles para Thrustmaster T150.
*   Arreglado el atributo wmclass en el fichero desktop (gracias @BlueManCZ).
*   Arreglados problemas sincronizando ajustes al dispositivo.
*   Arreglada la ruta de instalación para que use el prefijo `/usr/local` en lugar de `/usr`.
*   Permitido sobrescribir ajustes del perfil desde la línea de comandos.
*   Comprueba disponibilidad del FFB antes de intentar usarlo para evitar errores.
*   Cambiada la manera de detectar dispositivos para evitar problemas en algunas instalaciones.
*   Añadida la traducción al Polaco (gracias @mmarusiak).
*   Incrementado el número de botones mostrados a 30.
*   Mejoras en las reglas UDEV para que sean más específicas y simples (gracias @alxwk).
*   Actualización a Meson 0.56.
*   Otras correcciones menores.

Como seguro que muchos sabréis, está utilidad está desarrollada por un miembro de nuestra comunidad, Bernat ([Berarma](https://github.com/berarma)), creador de otros proyectos relacionados con el soporte a dispositivos para juegos de conducción como el driver de volantes de Logitech [new-lg4ff]({{ "/tags/new-lg4ff" | absolute_url }}) y las utilidades de testeo de Force Feedback [ffbtools](https://github.com/berarma/ffbtools). Como podéis leer, si disponéis de alguno de estos nuevos volantes, es "impepinable" la instalación de Oversteer, así como su actualización si ya lo usáis por los bugs y mejoras introducidas. Tenéis instrucciones de como hacerlo en la [página del proyecto](https://github.com/berarma/oversteer). 

**Nota**: Se recomienda desinstalar la versión anterior antes de instalar la nueva, ya que la ubicación de instalación probablemente cambiará.