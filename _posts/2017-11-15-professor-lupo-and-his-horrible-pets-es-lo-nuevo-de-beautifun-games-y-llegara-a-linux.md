---
author: Pato
category: Aventuras
date: 2017-11-15 12:31:42
excerpt: "<p>El estudio espa\xF1ol sigue tray\xE9ndonos sus nuevos desarrollos a nuestro\
  \ sistema favorito</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0f18514092300971a1d9467fe5706101.webp
joomla_id: 536
joomla_url: professor-lupo-and-his-horrible-pets-es-lo-nuevo-de-beautifun-games-y-llegara-a-linux
layout: post
tags:
- indie
- aventura
- proximamente
- puzzles
title: "'Professor Lupo and his Horrible Pets' es lo nuevo de BeautiFun Games \xA1\
  y llegar\xE1 a Linux!"
---
El estudio español sigue trayéndonos sus nuevos desarrollos a nuestro sistema favorito

¡Buenas noticias! BeautiFun Games, estudio español responsable de juegos como [Megamagic: Wizards of the Neon Age](http://store.steampowered.com/app/422650/Megamagic_Wizards_of_the_Neon_Age/) o el notable [Nihilumbra](http://store.steampowered.com/app/252670/Nihilumbra/) ha anunciado el desarrollo de su próximo juego '**Professor Lupo and his Horrible Pets**', del que ya tenemos su primer vídeo promocional.



> 
> *¡Conviértete en el becario más épico de todos los tiempos y salva la humanidad de la devastación, o al menos tu propio cuello, en este trepidante juego de puzles y aventuras infestado de alienígenas!*
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/gFeZw-ODWGc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>



> 
> *El profesor Lupo es un científico que ha viajado por toda la galaxia con el objetivo de capturar y estudiar los aliens más horribles y ha vuelto a la Tierra para venderlos al mejor postor. Mientras la subasta alienígena tiene lugar, la nave espacial es atacada, provocando que las bestias escapen y queden esparcidas a lo largo y ancho de la Estación Espacial Aurora. Tu trabajo como becario de la nave siempre ha sido estudiar y cuidar de estas horripilantes y letales criaturas. No obstante, esta vez tendrás que añadir una tarea a tu lista de quéhaceres: ¡encuentra una salida o conviértete en la cena de tus mascotas!*
> 
> 
> 


El concepto de juego me recuerda poderosamente a un juego de recreativas de mi niñez, donde un profesor iba "limpiando" alienígenas con un spray.


En cuanto a la versión para Linux/SteamOS, el juego ha aparecido ya en la [base de datos](https://steamdb.info/app/741520/) de Steam con su correspondiente apartado para Linux, y en su página de Steam aparece ya el apartado referente a los requisitos del sistema del juego para Linux/SteamOS, y además nos lo han confirmado oficialmente:



> 
> Y-E-S! :)
> 
> 
> — BeautiFun Games (@BeautiFunGames) [November 15, 2017](https://twitter.com/BeautiFunGames/status/930796387872190464?ref_src=twsrc%5Etfw)



 


Si quieres saber más sobre 'Professor Lupo and his Horrible Pets', puedes visitar su [página de Steam](http://store.steampowered.com/app/741520/Professor_Lupo_and_his_Horrible_Pets/) donde anuncian la llegada del juego para comienzos de este próximo año 2018.


¿Que te parece este 'Professor Lupo and his Horrible Pets'? ¿Has jugado a alguno de los juegos de BeautiFun Games?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

