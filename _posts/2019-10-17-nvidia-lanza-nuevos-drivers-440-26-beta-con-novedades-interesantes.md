---
author: Pato
category: Software
date: 2019-10-17 17:02:07
excerpt: "<p>A\xF1ade soporte para decodificar por VP9 y refresco variable sobre HDMI\
  \ 2.1 para monitores Gsync entre otras caracter\xEDsticas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/eb9f7999667d680af9a85b74d74700ee.webp
joomla_id: 1122
joomla_url: nvidia-lanza-nuevos-drivers-440-26-beta-con-novedades-interesantes
layout: post
tags:
- drivers
- nvidia
title: Nvidia lanza nuevos drivers 440.26 beta con novedades interesantes
---
Añade soporte para decodificar por VP9 y refresco variable sobre HDMI 2.1 para monitores Gsync entre otras características

Nos llegan noticias de que **Nvidia** acaba de lanzar un nuevo **driver beta para Linux con numeración 440.26**, y que trae novedades interesantes entre las que destacan el soporte para decodificación VP9 para el driver VDPAU, el refresco variable sobre HDMI 2.1 para monitores que soporten frecuencia adaptativa GSYNC y enlazado paralelo de shaders GLSL.


Además, trae mejoras específicas para los juegos Saints Row IV y Saints Row: The Third, soporte para nuevas extensiones *multicast* de Vulkan y las correspondientes mejoras y parches a todos los niveles.


Una vez que sea estable, el driver debería aparecer en pocos días en los repositorios habituales de cada distribución.


Si quieres saber más en profundidad lo que trae este nuevo driver tienes toda la información [en este enlace](https://www.nvidia.com/drivers/results/152601#driverInfo-152601=0).

