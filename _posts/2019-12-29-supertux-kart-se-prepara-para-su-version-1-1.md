---
author: Serjor
category: Carreras
date: 2019-12-29 21:59:44
excerpt: "<p>La versi\xF3n 1.1-RC1 prepara el camino para una mejor y m\xE1s divertida\
  \ experiencia</p>\r\n"
image: /https://jugandoenlinux.com/media/k2/items/cache/a9ea13237c0502159263f017095ec8e6_XL.webp
joomla_id: 1145
joomla_url: supertux-kart-se-prepara-para-su-version-1-1
layout: post
tags:
- open-source
- supertuxkart
- supertux-kart
- linuxgaming
title: "SuperTux Kart se prepara para su versi\xF3n 1.1 (ACTUALIZADO)"
---
La versión 1.1-RC1 prepara el camino para una mejor y más divertida experiencia


**Actualización 05-01-2020**: La versión 1.1 final ya ha sido [liberada](http://blog.supertuxkart.net/2020/01/supertuxkart-11-released.html)


 


Leemos en [Lignux](https://lignux.com/el-juego-de-carreras-supertuxkart-publica-su-version-1-1-rc1/) que el juego open source de Karting **SuperTux Kart** tiene una nueva versión a punto de salir, y para ello han liberado la versión 1.1-rc1, que por el hecho de ser una RC probablemente sea la versión final, pero podemos esperar algunos fallos.


La lista de cambios, la cuál podemos encontrar [aquí](http://blog.supertuxkart.net/2019/12/supertuxkart-11-release-candidate.html) es bastante interesante:


* Online mejorado
* Podemos activar una opción para ver los powerups del resto de corredores
* Modo contrareloj para el modo historia
* Mejoras en la interfaz de usuario
* Soporte de emojis en el chat
* Soporte para mostrar la bandera del país de cada corredor
* Nuevo circuito: Pumpkin Park
* Mejoras en la versión para Android y soporte para iOS
* Y en general un montón de mejoras y correción de errores


Si queréis acceder a esta nueva versión, podéis descargarla desde la sección [Downloads](https://supertuxkart.net/Download) de la página oficial.


Y tú, ¿has podido probar ya esta versión 1.1 RC1? Cuéntanos tu experiencia en los comentarios, o en nuestros canales de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux)

