---
author: leillo1975
category: Plataformas
date: 2021-02-05 10:14:06
excerpt: "<p>El juego #OpenSource se inspira en los cl\xE1sicos del g\xE9nero.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperBombinhas/SuperBombinhas.webp
joomla_id: 1274
joomla_url: disfruta-de-super-bombinhas-un-plataformas-libre
layout: post
tags:
- plataformas
- open-source
- super-bombinhas
- ruby
- victor-david-santos
title: Disfruta de Super Bombinhas, un plataformas libre
---
El juego #OpenSource se inspira en los clásicos del género.


Seguro que todos vosotros conoceis [SuperTux](https://www.supertux.org/), el juego inspirado en el conocidísimo [Super Mario Bros](https://es.wikipedia.org/wiki/Super_Mario_Bros.), pero son multitud los juegos libres que han tomado como referencia a los clásicos de los plataformas de los 80/90. Buenos ejemplos de ello son [Secret Maryo Chronicles](http://www.secretmaryo.org/) o [Open Surge]({{ "/posts/open-surge-un-juego-open-source-al-estilo-sonic" | absolute_url }}). Pues en esta lista se ha añadido un nuevo contendiente, **Super Bombinhas** (Super Bombitas en Español), un videojuego de **código abierto** programado en [Ruby](https://www.ruby-lang.org/es/) con licencia **GPL 3.0**.


Este colorido **plataformas 2D**, cuenta con **7 mundos en la misión principal más un nivel secreto desbloqueable**. Además cada uno de estos detallados mundos esta a su vez lleno de secretos que descubrir. A lo largo de nuestra aventura también **podremos ir habilitando nuevos personajes** con caracteríticas únicas para poder jugar con ellos.  



El juego, creado por el brasileño **Victor David Silva** ([@victords](https://github.com/victords)),  lleva en desarrollo sobre un año, y la última versión que podemos descargar es la [1.1.0](https://github.com/victords/super-bombinhas/releases/tag/v1.1.0), en la que entre otras cosas se ha habilitado el movimiento de cámara arriba y abajo con las teclas de flecha. En él están colaborando también músicos  como [Zergananda](https://soundcloud.com/zergananda) y el italiano [Francesco Corrado](https://soundcloud.com/franzcorradomusic), conocido Streamer del mundillo ([opensource_gaming](https://www.twitch.tv/opensource_gaming)) que también realiza bandas sonoras para otros juegos libres.


El juego se puede descargar desde la [página del proyecto](https://github.com/victords/super-bombinhas) y también desde [itch.io](https://victords.itch.io/super-bombinhas), encontrándolo en formato .deb (y .exe para Windows), aunque también está el código fuente para que cualquiera pueda compilarlo. Os dejamos con un video reciente del [canal de youtube de Francesco Corrado](https://www.youtube.com/channel/UCtWVpbOMKm06qjRjEI9pIFg) donde podeis verlo disfrutando de él:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/MBLOgUjEFZI" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


¿Te gustan los juegos de plataformas clásicos? ¿Qué opinión te merece este Super Bombinhas y este tipo de juegos libres? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

