---
author: Serjor
category: Carreras
date: 2017-01-26 21:26:54
excerpt: <p>La fuerza se une a wipeout en marte</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/71ab1197965a26d2e4379f8b23c36ebb.webp
joomla_id: 196
joomla_url: vector-36-el-juego-de-carreras-de-vainas-ya-disponible-para-linux
layout: post
tags:
- carreras
- vector-36
title: Vector 36, el juego de carreras de vainas ya disponible para Linux
---
La fuerza se une a wipeout en marte

Nos enteramos gracias a [LinuxGameConsortium](https://linuxgameconsortium.com/linux-gaming-news/vector-36-racing-simulation-launches-steam-linux-mac-pc-games-46737/) que [Vector 36](http://vector36.com/) ya está en [steam](http://store.steampowered.com/app/346460/?l=spanish) y disponible para la plataforma del pingüino.


En este juego de carreras donde podremos demostrar que la fuerza en nosotros es más intensa que en Anakin Skywalker, podremos correr por varias pistas en marte, y además, debemos configurar el Skimmer, que es como se llama el vehículo que pilotamos, para poder adaptarlo a nuestra forma de pilotar y marcar así la diferencia.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/qBkXTNkx_gk" width="560"></iframe></div>


 


Donde destaca realmente es en su apartado técnico, porque es uno de los pocos juegos que se puede disfrutar con gafas de realidad virtual, aunque eso sí, como punto negativo, el juego no está disponible en castellano, aunque en un juego de carreras de Skimmers, entendemos que no es lo más importante.


 


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/346460/" width="646"></iframe></div>


 


Y tú, ¿sentirás en vez de pensar en este juego? Déjanos tu opinión en un comentario, en nuestro [foro](index.php/foro/categorias), en el canal de [telegram](https://t.me/jugandoenlinux) o nuestro grupo de [discord](https://discord.gg/ftcmBjD).

