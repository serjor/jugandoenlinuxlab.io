---
author: Pato
category: Hardware
date: 2022-08-29 15:08:02
excerpt: "<p>Tendremos nuevas iteraciones de SteamOS y Steam Deck durante a\xF1os</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/overview-shared-library-spanish.webp
joomla_id: 1485
joomla_url: valve-prepara-el-desembarco-de-steam-deck-en-asia-y-desvela-datos-sobre-su-futuro-2
layout: post
tags:
- steamos
- steam-deck
title: Valve prepara el desembarco de Steam Deck en Asia y desvela datos sobre su
  futuro
---
Tendremos nuevas iteraciones de SteamOS y Steam Deck durante años


No desvelamos nada a nadie si decimos que Steam Deck está siendo un éxito sin precedentes en cuanto a hardware de Valve y ha supuesto todo un terremoto en el mercado de los PC-consola ultraportatil. Valve ha tenido que aumentar la producción varias veces y en varios órdenes de magnitud para poder satisfacer la demanda, con lo que ha podido incluso adelantar las fechas de entrega a los usuarios que la tenían reservada para el tercer y cuarto trimestre de este año.


Además de eso, han introducido centenares de cambios y mejoras en el sistema SteamOS 3.0 desde el lanzamiento de la máquina, y han ido aumentando el número de títulos que se pueden ejecutar en ella y ya se cuentan cerca de 5000 títulos entre compatibles y verificados.


No solo eso, si no que otras compañías dedicadas al hardware de PC-Consola ultraportatil como **GPD u OneXPlayer** se han visto superadas y ya están negociando con Valve para **introducir SteamOS 3.0 en sus máquinas**.


En este contexto, Valve se encuentra ahora en proceso de expansión hacia el mercado asiático, y aprovechando esto han editado un "libro" de unas 50 páginas con información relevante sobre la Steam Deck, su diseño y producción, las decisiones que llevaron a que sea como es y además una visión sobre el futuro de Steam Deck y su ecosistema.


En el apartado titulado "El Futuro: Más Steam Decks, mas SteamOS" se puede leer:



> 
> En el momento de escribir esto ya se han introducido cientos de cambios en el sistema operativo de Steam Deck desde que se lanzó el dispositivo por primera vez. Esto continuará durante la vida de Steam Deck, bien en las futuras versiones del producto.
> 
> 
>  Además, **la interfaz de Steam Deck llegará pronto al cliente de Steam de escritorio como una nueva versión de Big Picture,**[...] y el trabajo de compatibilidad realizado para Steam Deck se implementará también en otras plataformas como ChromeOS que pronto ofrecerá soporte para el juego en Steam para todos los usuarios de Chromebooks.
> 
> 
> De todas formas, esto es una línea de productos multigeneracional. Valve dará soporte a Steam Deck y SteamOS en el futuro próximo. Aprenderemos de la comunidad de Steam sobre nuevos usos para nuestro hardware en los que no habíamos pensado aún, y construiremos nuevas versiones para que sean más abiertas y capaces de lo que ha sido la primera versión de Steam Deck.
> 
> 
> 


 Blanco y en botella. ¿Significa esto que lanzarán una nueva versión de Steam Deck pronto?: Aunque sabemos que ya están pensando en nuevo hardware (alguien dijo Deckard?) la Deck apenas tiene un año de vida y es difícil pensar que vayan a lanzar una nueva versión como mínimo hasta que la actual generación gráfica esté llegando a su fin, cosa para la que aún quedan algunos años por delante.


Puedes ver el anuncio y descargar el libreto de Valve en su [post de su blog en Steam](https://store.steampowered.com/news/app/1675200/view/3401926123919972634).

