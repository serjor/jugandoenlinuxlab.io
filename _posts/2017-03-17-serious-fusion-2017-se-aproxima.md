---
author: Serjor
category: "Acci\xF3n"
date: 2017-03-17 20:07:26
excerpt: "<p>La beta ya est\xE1 disponible para los jugadores de Serious Sam HD: First\
  \ Encounter</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8b0dca3b357fcee94cf1b8a6c08994ea.webp
joomla_id: 258
joomla_url: serious-fusion-2017-se-aproxima
layout: post
tags:
- serious-sam
title: "'Serious Fusion 2017' se aproxima. Actualizaci\xF3n"
---
La beta ya está disponible para los jugadores de Serious Sam HD: First Encounter

Gracias GOL nos enteramos en [este artículo](https://www.gamingonlinux.com/articles/serious-sam-fusion-2017-update-for-the-first-encounter-due-soon-as-the-build-is-ready.9328) de que la gran actualización que Croteam está preparando para Serious Sam HD, Serious Sam Fusion 2017, está casi lista, de hecho, según las [palabras](https://steamcommunity.com/app/41000/discussions/0/133259956003821357/?tscn=1489694804#c133259956005467306) del propio desarrollador:


"La actualización ya está hecha, solamente falta el tema del papeleo, temas de marketing..."


Como [ya os informamos](index.php/homepage/generos/accion/item/333-croteam-anuncia-que-todos-los-serious-sam-soportaran-linux-steamos), esta actualización trae jugosas novedades, sobretodo para los linuxeros, ya que por un lado todos los juegos soportarán linux, incluyendo los de realidad virtual, y por otro apuestan completamente por Vulkan, así que no podemos estar más que encantados de que en los próximos días steam avise a los poseedores de este título de que tiene una actualización pendiente. Falta por ver qué pasa con aquellos que tenemos otros títulos de la saga Seriour Sam en nuestras bibliotecas.


 


Y ya que al parecer la actualización será para el Serious Sam HD, os dejamos con un trailer del juego y el enlace a su página en Steam:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/JTbwOukQlDY" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/41000/2536/" width="646"></iframe></div>


 


Actualización 20.03.2017: [Leemos en GOL](https://www.gamingonlinux.com/articles/serious-sam-fusion-2017-beta-for-the-first-encounter-is-now-available-with-linux-support.9352) que la beta de esta actualización ya está disponible para Serious Sam HD: The First Ecounter


 


Y tú, ¿ya te has descargado la beta? Déjanos un comentario al respecto o pasa por alguno de nuestros grupos de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

