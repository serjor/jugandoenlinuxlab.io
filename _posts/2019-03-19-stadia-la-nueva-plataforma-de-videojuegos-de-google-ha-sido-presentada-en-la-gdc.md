---
author: Serjor
category: Noticia
date: 2019-03-19 20:51:19
excerpt: '<p>Ya tenemos la fecha oficial de lanzamiento del servicio. #LinuxGaming</p>'
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/68fd1d661976b9d8b879f7809970f2e8.webp
joomla_id: 1000
joomla_url: stadia-la-nueva-plataforma-de-videojuegos-de-google-ha-sido-presentada-en-la-gdc
layout: post
tags:
- streaming
- gdc
- stadia
- google
title: "Stadia, la nueva plataforma de videojuegos de Google (ACTUALIZACI\xD3N)."
---
Ya tenemos la fecha oficial de lanzamiento del servicio. #LinuxGaming

**ACTUALIZACIÓN 15-10-19:** Acabamos de enterarnos gracias a @Txema que la nueva plataforma de videojuegos en Streaming de Google, Stadia, será lanzada el próximo día 19 de Noviembre. El anuncio podeis encontrarlo en el blog de Google, así como en sus redes sociales, tal y como podeis ver en este tweet:



> 
> Mark your calendars! Stadia will start arriving November 19.  
>   
> Need a quick guide on what exactly Stadia is? We got you covered. Become an expert in all things Stadia just in time for launch. [pic.twitter.com/iwbCQiJ0CH](https://t.co/iwbCQiJ0CH)
> 
> 
> — Stadia (@GoogleStadia) [October 15, 2019](https://twitter.com/GoogleStadia/status/1184108382921191425?ref_src=twsrc%5Etfw)



Si has sido uno de los primeros jugadores que ha pre-ordenado y recibido la Founders Edition, podrás comprar y jugar tus juegos favoritos a partir de las 18:00 CET del 19 de noviembre. En teoría estos pedidos irán llegado con aviso previo por email y según el orden con que se hayan realizado. Podras jugar, por ejemplo, a Red Dead Redemption 2, Mortal Kombat 11, Kine y mucho más en tu televisor, portátil, ordenador de sobremesa y en determinadas tabletas y teléfonos. Tanto si has pedido Stadia Founder's Edition como Premiere Edition, tendrás tres meses de Stadia Pro, con acceso a "Destiny 2: The Collection". 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/Pwb6d2wK3Qw" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Seguiremos atentos...




---


**NOTICIA ORIGINAL:** La verdad es que poco podemos aportar a todo lo que ya se ha dicho en las redes sociales de lo que esta tarde nos ha enseñado Google, pero por lo menos intentaremos hacer un resumen con lo más destacado.


Para empezar ya podemos poner nombre al famoso Project Stream, y es que la plataforma de juegos en la nube de Google se llama Stadia, y es lo que todos pensábamos que sería y un poco más.


Para empezar lo básico, es juego en la nube. Stadia es al videojuego lo que Netflix al cine y televisión, streaming en tiempo real y en prácticamente cualquier cosa que tenga una pantalla y conexión a internet, sin necesidad de mucho más que un mando. En este sentido, han presentado un mando, que no será obligatorio usar para jugar, pero que comentan que trae alguna ventaja frente a mandos convencionales, como que se conectará por WiFi (según parece para reducir latencias) y que el propio mando reconocerá en qué dispositivo queremos jugar en cada momento, permitiendo cambiar de pantalla poco más o menos al vuelo, haciendo de Stadia un servicio portable.


Y es que Stadia es un Steam Link hipervitaminado. Si hace unos días Valve anunciaba que podríamos jugar en cualquier parte a los juegos que tuviéramos en nuestro ordenador, Google sube la apuesta y nos permitirá jugar a cualquier juego de su catálogo (del que no hay datos, ni tampoco precio del servicio), y si Valve decía que para poder jugar necesitaríamos una buena conexión, Google saca pecho de su propia infraestructura y dicen que son capaces de obtener un rendimiento excelente gracias a sus nodos por todo el mundo, y a que son capaces llevar la comunicación por el camino más óptimo.


Y no solamente esto, sino que Stadia irá un poco más allá. Google parece haber identificado quienes son sus principales rivales, qué es lo que quiere el público y cómo integrar sus servicios para que no nos vayamos de su plataforma, así que han cubierto prácticamente todo el espectro del ecosistema gamer de hoy en día:


* Jugar en cualquier parte
* Jugar con los mejores gráficos posibles
* Ver jugar a la gente
* Facilitar la creación de contenido
* Compartir cualquier momento con cualquiera


Y según parece, su plataforma lo permitirá. Jugar en cualquier parte es lo que aún tienen que demostrar, pero como decíamos al inicio, es juego en nube, con lo que si tenemos conexión a internet potente (y recordemos que la red 5G en España se prevé que esté lista para el 2020), esto en principio debería cubrirse. Sobre la calidad gráfica, no hay muchas dudas. Durante su presentación han hecho gala de músculo, y la verdad, les sobra. El lado del servidor de Stadia correrá sobre centros de datos con GPUs dedicadas, lo que les permite usar múltiples GPUs para renderizar una instancia de un cliente. Evidentemente esto tiene el problema de qué pasará cuando miles de jugadores quieran jugar a la vez, ya que la infraestructura debe ser capaz de soportarlo, pero es algo que Google tendrá que resolver sobre la marcha. También han tomado nota de que Amazon y su servicio Twitch se está llevando una tarta que es muy suculenta, así que una de las opciones que ofrecerá Stadia será la de retransmitir nuestra partida a través de YouTube. Si ya tiene el vídeo que nos tiene que enviar a nuestra pantalla, poner ese mismo vídeo en nuestro canal de YouTube tampoco era tan difícil. ¿Ventaja sobre Twitch? No hay delay, y Google habría solventado uno de los grandes peros que tiene el streaming en directo, y es que los espectadores ven el vídeo con unos segundos de retardo, y sus comentarios siempre son respecto a cosas que ya han pasado. Por ejemplo en mi caso, en algún directo, esta diferencia de tiempo es "molesta" porque hay que esperar a que la gente escriba lo que para tí ha pasado hace un rato si quieres interactuar con los espectadores, y esto puede ser un cambio radical en la manera en la que los creadores de contenido hacen sus directos, porque las tomas de decisiones se pueden hacer en función de lo que los espectadores digan en tiempo real. Y por último, compartir. En la era de las redes sociales, donde se puede compartir cualquier enlace por infinidad de medios, Google se ha sacado de la chistera algo nuevo para compartir, el estado del juego en un momento dado. Posición, vidas, enemigos en pantalla... ¿Hemos hecho la jugada del siglo? Comparte con tus amigos el estado del juego justo antes de dicha jugada y rétales a hacerlo mejor.


Y después de esto, la gran pregunta que nos hacemos todos, si este servicio triunfa (algo que está por ver), ¿qué pasa con el juego en GNU/Linux? Si a día de hoy con motores que exportan a GNU/Linux ya tenemos problemas para que las desarrolladoras se fijen en el sistema del pingüino, con una nueva plataforma de este estilo, la cosa se puede complicar. En nuestro canal de Telegram hemos tenido opiniones de lo más diferente, puntos de vista diferentes con valoraciones positivas y negativas.


Sabemos que Stadia usará Vulkan, cosa que nos beneficia enormemente, que correrá sobre servidores Debian, así que el desarrollo será muy probablemente en GNU/Linux, o al menos habrá parte del mismo que habrá que hacerlo sobre él, y con componentes disponibles en GNU/Linux, pero también sabemos que el argumento más usado para no traer un videojuego a GNU/Linux es el del soporte, no la dificultad de hacer el port, así que no sería extraño que la respuesta fuera "juégalo en Linux a través de Stadia". Si por lo menos Vulkan se impone como API de desarrollo, Wine y Proton serán de gran ayuda, pero estaríamos perdiendo la batalla del juego nativo. Crucemos los dedos para que esto traiga juegos nativos a GNU/Linux.


En verano nos darán más datos, ya que por ahora simplemente han anunciado este servicio a desarrolladores de videojuegos, y durante la presentación gran parte de la información que daban iba orientada a ellos, más que al consumidor final, y hasta este verano no parece que vayan a ponerlo en el mercado, como poco. Eso sí, han prometido que el servicio estará listo este mismo año, así que solamente nos queda esperar a ver cómo se desarrollan los eventos entorno a Stadia.


Si os habéis perdido la presentación podéis verla aquí:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/nUih5C5rOrA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Y tú, ¿qué opinas de este nuevo servicio que nos ha presentado Google? ¿Será beneficioso para el juego en linux? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

