---
author: Pato
category: Arcade
date: 2017-03-10 15:48:29
excerpt: "<p>Un \"Shoot 'Em Up\" en vista cenital con una pinta de lo m\xE1s interesante</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d4ae67bb81032f6add66be1c1df07be9.webp
joomla_id: 244
joomla_url: ghost-blade-hd-un-bullet-hell-al-estilo-de-la-vieja-escuela-ya-disponible-en-linux-steamos
layout: post
tags:
- accion
- indie
- arcade
- shoot-em-up
- bullet-hell
title: '''Ghost Blade HD'' un "bullet hell" al estilo de la vieja escuela ya disponible
  en Linux/SteamOS'
---
Un "Shoot 'Em Up" en vista cenital con una pinta de lo más interesante

'Ghost Blade HD' [[web oficial](http://ghostbladegame.com/)] apareció entre las novedades lanzadas el día 8 en Steam. Es de esos títulos que no pueden faltar en la biblioteca de todo aquel que ha vivido la época dorada de las recreativas. Con unos gráficos realmente buenos, vista cenital y una fluidez de movimientos muy buena, este juego promete bastante a todo aquel que guste de disparar a todo lo que se mueva y esquivar miles de proyectiles. Para muestra un botón:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/uZy5_TLT-nk" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Además el juego ofrece multijugador cooperativo para dos personas, combate de naves de hasta 3 personas con distintos tipos de armamento, imponentes enemigos finales durante los 5 niveles que tiene el juego y mucho más.


Si quieres echarle un ojo lo tienes en su página de Steam, eso sí no está traducido al español, pero dado el tipo de juego que es tampoco creo que sea un grave problema:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/577910/" width="646"></iframe></div>


 ¿Te gustan los juegos arcade? ¿Qué te parece este Ghost Blade HD?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

