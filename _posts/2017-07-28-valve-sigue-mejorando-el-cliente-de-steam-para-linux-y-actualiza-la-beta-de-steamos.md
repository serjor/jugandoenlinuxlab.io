---
author: Pato
category: Editorial
date: 2017-07-28 14:30:54
excerpt: "<p>Entre las novedades del sistema est\xE1n diversas actualizaciones</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6a501db6788f8bc3147fbec59d7a4924.webp
joomla_id: 421
joomla_url: valve-sigue-mejorando-el-cliente-de-steam-para-linux-y-actualiza-la-beta-de-steamos
layout: post
tags:
- steamos
- steam
- valve
title: Valve sigue mejorando el cliente de Steam para Linux y actualiza la beta de
  SteamOS
---
Entre las novedades del sistema están diversas actualizaciones

Si alguien piensa que [Valve está "matando" a SteamOS](http://news.softpedia.com/news/valve-is-killing-its-projects-by-abandoning-them-including-steamos-517017.shtml), me parece que ese alguien anda un poco equivocado.  Sin ir más lejos, este mismo Miércoles ha sido publicada una nueva beta del sistema operativo con actualizaciones y novedades interesantes.


Entre ellas destaca la actualización al kernel 4.11.12 junto a su firmware, y la inclusión de dependencias para dar soporte inicial a Flatpak.


Para todo el que no esté al tanto, Flatpak es a groso modo un sistema de distribución de software "encapsulado" junto con todas las dependencias que este software necesita para funcionar en las distribuciones soportadas. ¿Significa esto que Valve piensa empaquetar los juegos en Flatpak?


Pues según ha comentado Jvert, uno de los responsables de desarrollo de SteamOS no se trataría de eso, si no de que quieren hacer más sencillo el poder instalar aplicaciones externas a Steam , y según sus palabras "*Flatpak parece un buen comienzo en esa dirección*".


Puedes ver el anuncio y los comentarios en el [post del anuncio de la beta](http://steamcommunity.com/groups/steamuniverse/discussions/1/1458455461499646564/). Está claro que SteamOS se mueve y Valve tiene planes para el sistema. Sin duda, *los muertos que vos matáis gozan de buena salud.*


Por otra parte, el cliente de Steam para Linux también sigue progresando. En concreto, en la actualización de la beta del día 25 anunciaron que se añadía la opción de seleccionar las librerías de "runtime" de Steam si estas son más actuales que las del propio sistema. También se ha actualizado el cargador de Vulkan en el "runtime" de Steam a la versión 1.0.54 que provee todas las extensiones para SteamVR, y se ha añadido el soporte para la caché de shaders para los drivers que lo soportan, como Mesa 17.1 y NVIDIA 381.26.08 o 384.59.


Ahora la caché de Shaders la gestionará Steam creándola en carpetas separadas para cada juego gestionado por Steam, y será detectada y manejada por el gestor. Puedes ver los detalles en el [post del anuncio](http://steamcommunity.com/groups/SteamClientBeta#announcements/detail/1447197096908335573).


¿Qué te parecen las novedades? ¿Piensas que Valve no está "matando" a SteamOS?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

