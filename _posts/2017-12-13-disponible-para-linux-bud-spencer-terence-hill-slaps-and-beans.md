---
author: leillo1975
category: Arcade
date: 2017-12-13 08:57:56
excerpt: <p>Trinity Team lo ha lanzado de forma oficial.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/eebad02b9c5415f9ba153e4096437663.webp
joomla_id: 572
joomla_url: disponible-para-linux-bud-spencer-terence-hill-slaps-and-beans
layout: post
tags:
- bud-spencer
- terence-hill
- slaps-and-beans
- trinity-team
- retro
title: Disponible para Linux "Bud Spencer & Terence Hill - Slaps And Beans" (ACTUALIZADO)
---
Trinity Team lo ha lanzado de forma oficial.

**ACTUALIZACIÓN 20-4-18**: Nos acaban de informar via mail que el juego [ha salido al fin de su fase de Acceso Anticipado](http://store.steampowered.com/news/?appids=564050), y **se puede adquirir en su versión final**. Según comenta Trinity Team, eso no significa que no continuen dando soporte, corrigiendo y añadiendo cosas, si no que ha llegado un estado en el que han implementado todo lo que querían desde un principio y tienen la mayor parte de los bugs corregidos. En estos meses **el juego ha recibido varias actualizaciones** donde se han arreglado multitud de errores, añadido minijuegos, aumentado fases, nuevas animaciones, personajes, mejoras en el rendimiento, etc.


Si sois seguidores de JugandoEnLinux.com sabreis que hace unos meses le dedicamos un [completo análisis](index.php/homepage/analisis/item/695-analisis-bud-spencer-terence-hill-slaps-and-beans) donde, a pesar de estar aun en Acceso Anticipado, pudimos comprobar que el juego vale mucho la pena y nos va a hacer pasar grandes momentos. Desde aquí nos gustaría recomendaros el juego si sois amantes de los juegos retro y de acción. Os dejamos con un "particular" trailer del juego, recien salido del horno:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/mdAj3DQAUYM" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Podeis comprar "Bud Spencer & Terence Hill - Slaps and Beans" en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/564050/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


 




---


**NOTICIA ORIGINAL**: Los más veteranos lectores de JugandoEnLinux.com quizás recuerden que hace casi 11 meses publicábamos un [nostálgico artículo](index.php/homepage/generos/arcade/item/282-bud-spencer-terence-hill-slaps-and-beans-llegara-a-gnu-linux) donde anunciabamos que en el desarrollo de Slaps and Beans estaba contemplada una versión del juego para nuestro sistema. Un par de semanas más tarde os dejábamos una [jugosa entrevista](index.php/homepage/entrevistas/item/301-entrevista-con-trinity-team-bud-spencer-terence-hill-slaps-and-beans) con Marco Agricola, uno de sus creadores, y que inaguraba ese tipo de artículos en nuestra web. En el día de hoy por fin tendremos la oportunidad de disfrutar de este juego, tal y como acaban de anunciar a través de twitter:



> 
> Guys, with great emotion we announce that Slaps and Beans is officially on sale as Early Access on Steam!... <https://t.co/ZIJLpiBWvj>
> 
> 
> — Slaps and Beans (@SchiaffiFagioli) [December 15, 2017](https://twitter.com/SchiaffiFagioli/status/941636606188032000?ref_src=twsrc%5Etfw)



 


Como sabreis, [Slaps and Beans](http://www.slapsandbeans.com/) es un juego desarrollado por la compañía Italiana **Trinity Team** gracias a una exitosa campaña de [Kickstarter](https://www.kickstarter.com/projects/1684041218/bud-spencer-and-terence-hill-slaps-and-beans) donde pudieron recaudar más de 200.000€ para financiarlo. En él se homenajea a estos tan célebres personajes del cine que tantas buenas horas nos hicieron (y hacen) pasar con sus más creativos e inverosímiles porrazos. El juego está desarrollado principalmente con Unity, lo que ha facilitado en gran medida crear su versión para Linux/SteamOS, y en él vamos a encontrar un **Arcade** de Scroll lateral al más puro estilo retro donde podremos manejar tanto a Bud Spencer como a Terence Hill y que permite el el juego colaborativo, no limitándose exclusivamente a la lucha, sinó a resolver pequeños problemas y minijuegos.


El juego saldrá en **Acceso Anticipado**, dato que debemos tener en cuenta a la hora de adquirirlo, y previsiblemente **la versión definitiva llegará a finales de Enero**. La descripción del juego que encontramos en Steam es la siguiente:


***¡El primer videojuego OFICIAL de Bud Spencer y Terence Hill!***


*Bud Spencer y Terence Hill en su primera aventura. ¡Una historia totalmente nueva! Ya sea en el Saloon del Viejo Oeste, el centro de Miami, o el parque de atracciones, tendrás una dosis completa de diálogos descacharrantes, tortazos, y por supuesto peleas en grupo.*


*Tortazos y Judías es un beat 'em up para uno o dos jugadores en modo cooperativo, con plataformas y minijuegos, en el que te meterás en el papel de los personajes de Bud Spencer y Terence Hill.*


*Los ingredientes clave de Tortazos y Judías son:*


* *Gráficos pixelados retro*
* *Sistema de pelea al estilo Bud Spencer & Terence Hill*
* *Modo cooperativo de dos jugadores*
* *Minijuegos*
* *Un cochecito rojo con la capota amarilla*
* *Montones de tortazos y de judías (obviamente)*


 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/_nwBM_-S5L4" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Los requisitos técnicos, como es de esperar en este tipo de juegos, no son nada elevados y funcionará en casi cualquier máquina. Podeis comprarlo en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/564050/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


También, desde JugandoEnLinux.com, nos gustaría anunciaros que **pronto podreis disfrutar de gameplays** en nuestro [canal de Youtube](https://www.youtube.com/channel/UC4FQomVeKlE-KEd3Wh2B3Xw), así como en unos días de **un análisis** más pormenorizado donde os describiremos todas las características más relevantes del juego  (gracias a Trinity Team por la copia facilitada).


¿Que os parece Slaps And Beans? ¿Os gustaban las peliculas de Bud Spencer y Terence Hill? Contestanos en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).


 

