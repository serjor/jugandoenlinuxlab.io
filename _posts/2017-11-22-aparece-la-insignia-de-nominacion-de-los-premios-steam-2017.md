---
author: Pato
category: Noticia
date: 2017-11-22 17:42:48
excerpt: <p>Se acerca el periodo para votar a tus favoritos</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c0463a74730bd2dbab86cbb4dec094a0.webp
joomla_id: 548
joomla_url: aparece-la-insignia-de-nominacion-de-los-premios-steam-2017
layout: post
tags:
- steam
title: "Aparece la insignia de Nominaci\xF3n de los premios Steam 2017"
---
Se acerca el periodo para votar a tus favoritos

Steam comienza a mover ficha para los premios Steam 2017, y lo hace "publicando" la insignia que se podrá obtener realizando diferentes acciones durante el periodo en que las votaciones estén abiertas.


En concreto, la insignia ofrece estas opciones para conseguirla:


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SteamStoreNew/InsigniapremiosSteam17.webp)


Como es normal desde que se celebran las votaciones, los juegos nominados estarán disponibles con descuentos durante todo el periodo de votación.


Puedes comprobar si ya tienes la insignia accediendo al apartado correspondiente en tu perfil.


Y tu, ¿vas a participar en las votaciones a los nominados de los premios Steam 2017? ¿conseguirás la insignia?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

