---
author: leillo1975
category: Ofertas
date: 2021-12-23 13:33:11
excerpt: "<p>Las principales tiendas digitales ofrecen su cat\xE1logo con importates\
  \ descuentos.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/rebajas_invierno_2021/Rebajas.webp
joomla_id: 1399
joomla_url: ya-estan-aqui-las-rebajas-de-navidad
layout: post
tags:
- steam
- gog
- rebajas
- itch-io
- fanatical
- navidad
title: "\xA1\xA1\xA1 Ya est\xE1n aqu\xED las rebajas de Navidad !!!"
---
Las principales tiendas digitales ofrecen su catálogo con importates descuentos.


 Ya está aqui de nuevo la navidad. El inexorable paso del tiempo no perdona y un año más nos preparamos para comer el marisco, el cordero y como no el riquisimo turrón. Con ella llegan también los regalos, y por supuesto los "tradicionales" descuentos en las principales tiendas online de videojuegos, que un año más nos ponen los dientes largos y son una buena alternativa para regalar o para regalarnos sin dejarnos un dineral.


![FanaticalWinter21](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/rebajas_invierno_2021/FanaticalWinter21.webp)


En esta ocasión os vamos a hablar de 4 tiendas, y para que no haya suspicacias lo haremos en orden alfabético, comenzando por [Fanatical](https://www.fanatical.com/es/linux-games), donde si gastamos más de 10€ tendremos un regalo extra y podremos encontrar juegos nativos como:


-[Baldurs Gate: The Complete Saga](https://www.fanatical.com/es/bundle/baldurs-gate-the-complete-saga) (I, II y Siege of Dragonspear con todas sus DLCs) por **11.23€ (-83%)**


-[Desperados III: Digital Deluxe Edition](https://www.fanatical.com/es/game/desperados-iii-digital-deluxe-edition) por **23,93€ (-60%)**


-[Two Point Hospital](https://www.fanatical.com/es/game/two-point-hospital) por **6.99€ (-80%)**


-[Overcooked! 2](https://www.fanatical.com/es/game/overcooked-2) por **4.82€ (-79%)**


![gog rebajas invierno 2021](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/rebajas_invierno_2021/gog_rebajas_invierno_2021.webp)


Continuamos con [GOG.com](https://www.gog.com/games?systems=linux&discounted=true), la conocida tienda de CD Projekt Red que se caracteriza por vender juegos sin DRM, y donde podremos comprar con fantásticos descuentos como los de:


-[The Witcher 2: Assassins of Kings Enhanced Edition](https://www.gog.com/game/the_witcher_2) por **2.99€ (-85%)**


-[Pillars of Eternity: Definitive Edition](https://www.gog.com/game/pillars_of_eternity_definitive_edition) por **7.39 (-80%)**


-[Wasteland 3: Colorado Collection](https://www.gog.com/game/wasteland_3) por **19.99 (-60%)**


-[Ion Fury](https://www.gog.com/game/ion_fury) por **8.39 (-60%)**


![itch rebajas invierno 2021](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/rebajas_invierno_2021/itch_rebajas_invierno_2021.webp)


Ahora vamos con la tienda de juegos indie por excelencia, donde podremos encontrar juegazos a grandes precios como los siguientes:


-[SpiritFarer](https://thunderlotus.itch.io/spiritfarer) por **14.99$ (-50%)**


-[ΔV: Rings of Saturn](https://koder.itch.io/dv-rings-of-saturn) por **5.79$ (-42%)**


-[Gone Home](https://gonehome.itch.io/gonehome) por **2.99$ (-80%)**


-[Oxenfree](https://night-school-studio.itch.io/oxenfree) por **0.99$ (-90%)**


![steam rebajas invierno 2021](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/rebajas_invierno_2021/steam_rebajas_invierno_2021.webp)


Por último, vamos ahora con el gigante de las tiendas digitales, la todopoderosa [Steam](https://store.steampowered.com/linux), donde como también encontraremos grandes descuentos como los que podeis ver a continuación:  
  
-[Metro Exodus Gold Edition](https://store.steampowered.com/sub/298833/) por **14.79€ (-63%)**


-[Shadow of the Tomb Raider Definitive Edition](https://store.steampowered.com/bundle/12231/Shadow_of_the_Tomb_Raider_Definitive_Edition/) por **13.05€ (-85%)**


-[Valheim](https://store.steampowered.com/app/892970/Valheim/) por **12.59€ (-25%)**


-[Blasphemous](https://store.steampowered.com/app/774361/Blasphemous/) por **6.24€ (-75%)**


 


Bueno señores, esta ha sido nuestra humilde selección. Espero que encontreis ese juego que tanto buscabais por el mejor precio posible y disfruteis como enanos en estas navidades. Si habeis encontrado una oferta interesante que no esté en esta lista y os apetece compartirla, podeis hacerlo en los comentarios de esta noticia, en nuestro grupo de JugandoEnLinux en [Telegram](https://t.me/jugandoenlinux), o nuestras salas de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org).

