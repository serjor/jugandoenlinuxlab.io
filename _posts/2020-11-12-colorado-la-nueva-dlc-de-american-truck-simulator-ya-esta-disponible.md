---
author: leillo1975
category: "Simulaci\xF3n"
date: 2020-11-12 08:45:47
excerpt: "<p>@SCSsoftware acaba de lanzar esta nueva expansi\xF3n.&nbsp;</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Colorado/Colorado_con_Logo_900.webp
joomla_id: 1261
joomla_url: colorado-la-nueva-dlc-de-american-truck-simulator-ya-esta-disponible
layout: post
tags:
- dlc
- american-truck-simulator
- scs-software
- expansion
- colorado
title: "Colorado, la nueva DLC de American Truck Simulator, ya est\xE1 disponible"
---
@SCSsoftware acaba de lanzar esta nueva expansión. 


 No han pasado ni cuatro meses desde que los desarrolladores de **American Truck Simulator** han lanzado su anterior expansión de mapa, [Idaho]({{ "/posts/disponible-idaho-dlc-para-american-truck-simulator" | absolute_url }}), de la que os hablamos en verano, cuando el estudio checo ya nos está presentando su nuevo trabajo para este simulador. Como siempre el anuncio nos llega a través de sus redes sociales, como este de Twitter:


 



> 
> We are very excited to announce that Colorado for American Truck Simulator has now been RELEASED! 🇺🇸  
>   
> 💵 Haul cargo on the Million Dollar Highway  
> 🔍 Discovers new & unique industries   
> 🚛 Visit natural & manmade landmarks   
> ➕ Much More!   
>   
> Buy it now at: <https://t.co/rKIv5AQxgK> [pic.twitter.com/38fS6c0l7D](https://t.co/38fS6c0l7D)
> 
> 
> — SCS Software (@SCSsoftware) [November 12, 2020](https://twitter.com/SCSsoftware/status/1326955054402822151?ref_src=twsrc%5Etfw)


  





Esta nueva expansión de mapa, que [os anticipamos]({{ "/posts/colorado-sera-el-proximo-estado-de-american-truck-simulator" | absolute_url }}) al inicio de la primavera , y que ya podeis comprar en la [web de Steam](https://store.steampowered.com/app/1209471/American_Truck_Simulator__Colorado/?utm_source=scs_blog&utm_campaign=co_event_announcement), nos lleva aún más al **interior de los Estados Unidos**, situándose al norte de [Nuevo México](index.php/component/k2/1-blog-principal/analisis/656-analisis-american-truck-simulator-dlc-new-mexico) y al este de [Utah](index.php/component/k2/1-blog-principal/simulacion/1251-lanzada-la-dlc-utah-para-american-truck-simulator); **dando así continuidad a las carreteras existentes** de estas anteriores DLCs. Colorado, apodado **"estado Centenario"**, cuya capital es **Denver**, es uno de los estados anexionados por los EEUU en la guerra con México en el siglo XIX, y es llamado así por el rio del mismo que lo cruza del centro del estado al oeste. Además está **presidido por las imponentes Montañas Rocosas**, siendo su paisaje en gran parte montañoso, con impresionantes **cañones**. No por esto último deja de ser también bastante variado, pues encontraremos **extensas praderas verdes** o incluso el ya habitual **paisaje árido**.


![Colorado Road map small](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Colorado/Colorado_Road_map_small.webp)


Según la descripción oficial, en Colorado DLC encontraremos lo siguiente:


*-Olvídate del mundo real en la inmensidad de las Grandes Llanuras*  
 *-Vaga por las impresionantes Montañas Rocosas*  
 *-Transporta la carga en la autopista del millón de dólares...*  
 *-Descubre las importantes industrias de Colorado como la agricultura y una mina de oro*  
 *-Entrega la carga desde y hacia el interior de un gran avión de carga en el Aeropuerto Internacional de Denver*  
*-Admira la representación exacta de ciudades como Denver y Colorado Springs*  
 *-Visita puntos de referencia como el Monumento a las Cuatro Esquinas y obten una vista de pájaro de él con la característica de punto de vista*  
 *-Desbloquea los logros específicos de Steam en Colorado*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/SiTFH7WXapU" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


 En JugandoEnLinux.com **intentaremos , si SCS Software nos lo facilita , ofreceros una serie de Streams** en nuestros canales de [Youtube](https://www.youtube.com/c/jugandoenlinuxcom) y [Twitch](https://www.twitch.tv/jugandoenlinux) recorriendo las carreteras de este estado, además de un un **completo análisis como ha sido habitual hasta la DLC de Utah**, por lo que os recomendamos que esteis atentos a nuestras redes sociales de [Twitter](https://twitter.com/JugandoenLinux) y [Mastodon](https://mastodon.social/@jugandoenlinux). Podeis comprar Colorado DLC para American Truck Simulator en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1209471/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Te gustan los juegos de SCS Software? ¿Qué te parece la adicíón de este estado a American Truck Simulator? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

