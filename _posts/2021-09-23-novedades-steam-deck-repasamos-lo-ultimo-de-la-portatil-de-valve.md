---
author: Pato
category: Hardware
date: 2021-09-23 09:03:42
excerpt: "<p>Las primeras impresiones con los dev kits son muy positivas, se actualizan\
  \ las preguntas frecuentes con novedades interesantes y mucho m\xE1s...</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/steamdeckdevkit1.webp
joomla_id: 1364
joomla_url: novedades-steam-deck-repasamos-lo-ultimo-de-la-portatil-de-valve
layout: post
tags:
- hardware
- steam-deck
title: "Novedades Steam Deck: repasamos lo \xFAltimo de la port\xE1til de Valve"
---
Las primeras impresiones con los dev kits son muy positivas, se actualizan las preguntas frecuentes con novedades interesantes y mucho más...


A falta de pocos meses para que la (el?) portátil de Valve empiece a llegar a los usuarios, las novedades en torno a la **Steam Deck** comienzan a salir a la luz. Hace poco más de una semana, a través del [blog de Steam Deck en Steam](https://store.steampowered.com/news/app/1675180/view/2963920750895461227) supimos que Valve comenzó a enviar los primeros "**dev kits**" o unidades para desarrolladores para que todos los interesados puedan testear sus juegos de cara al lanzamiento de la Steam Deck.


Esas primeras unidades ya están en manos de algunos de los desarrolladores más conocidos y por lo visto en sus reacciones, las primeras impresiones con la máquina les está resultando de lo más positivas. Como ejemplo, y [aparte del tweet](https://twitter.com/XboxP3/status/1426324572626845697) de **Phil Spencer**, mandamás de Xbox afirmando que ya tiene una y que pudo jugar a **Halo, Age of Empires** y pudo ejecutar **XCloud** en la Steam Deck sin problemas, están saliendo a la luz declaraciones como la de los chicos de **Re-Logic**, desarrolladores de [**Terraria**](https://store.steampowered.com/app/105600/Terraria/) afirmando que el juego se ejecuta sin ningún problema en la portatil.



> 
> Terraria + [@Steam](https://twitter.com/Steam?ref_src=twsrc%5Etfw) Deck, perfect combination for your adventures! [pic.twitter.com/WZvt0VOhSD](https://t.co/WZvt0VOhSD)
> 
> 
> — Terraria (@Terraria_Logic) [September 10, 2021](https://twitter.com/Terraria_Logic/status/1436421498466619398?ref_src=twsrc%5Etfw)



 También Mike Rose, fundador de **No More Robots**, creadores de [**Descenders**](https://store.steampowered.com/app/681280/Descenders/) twittea que les ha llegado el kit y que su juego funciona "increíblemente bien" con todo en ultra consiguiendo entre 50 y 60 fps.



> 
> Valve sent over a Steam Deck (thanks lovelies!!) so I've been going through our games and checking whether they work without any additional dev on our part  
>   
> Descenders: Works flawlessly!  
>   
> In fact it runs \*incredibly\* well. With full Ultra gfx on, I get around 50-60fps [pic.twitter.com/RjT3Q233lX](https://t.co/RjT3Q233lX)
> 
> 
> — Mike Rose (@RaveofRavendale) [September 21, 2021](https://twitter.com/RaveofRavendale/status/1440347232725700614?ref_src=twsrc%5Etfw)



 Otro ejemplo lo tenemos con Cliff Harris, fundador de **Positech Games** que afirma que su juego [**Democracy**](https://store.steampowered.com/app/1410710/Democracy_4/) funciona sin problemas "out of the box", con un gran sonido y un framerate "perfecto". Hay que tener en cuenta que este juego además **no es nativo**.



> 
> First impressions are that this is really really cool. And Democracy 4 seems to run ok on it out-of-the-box, although the steam paddle-finger doodads work 1000x better than the thumbsticks for my game. The sound is REALLY good. Framerate is perfect. [#democracy4](https://twitter.com/hashtag/democracy4?src=hash&ref_src=twsrc%5Etfw) [#steamdeck](https://twitter.com/hashtag/steamdeck?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/AOenhHOuiQ](https://t.co/AOenhHOuiQ)
> 
> 
> — cliffski (@cliffski) [September 21, 2021](https://twitter.com/cliffski/status/1440366610938617858?ref_src=twsrc%5Etfw)



 Por último, los chicos de [Xplane](https://twitter.com/XPlaneOfficial/status/1440374955720409091) también tienen un dev kit y afirman que su simulador de vuelo se puede ejecutar sin problemas en la Steam Deck. "Incluso funcionan los joysticks!".


Por otra parte, Lawrence Yang, desarrollador de Valve en declaraciones para [**PC Gamer**](https://www.pcgamer.com/uk/if-the-steam-deck-doesnt-run-your-entire-library-at-launch-valve-sees-that-as-a-bug/) afirmó que realmente **quieren que se pueda ejecutar todo el catálogo de Steam** en la Steam Deck, y que si algún juego falla **lo considerarán como un bug**, y querrán arreglarlo. En estos casos, como es de esperar la pregunta es obvia: además de los juegos que deberán ejecutarse con Proton... ¿Se refiere también a los juegos que se ejecutan con algún tipo de software "anti-cheat"?.


En ese sentido, hoy tenemos una actualización de la [página de Steam Deck](https://www.steamdeck.com/es/) en el apartado "preguntas y respuestas" que ofrece respuestas muy interesantes a preguntas sobre cuestiones como múltiples cuentas, ejecución de juegos externos a través de Proton, modo desconectado, formato (**Ext4**) y uso de la tarjeta SD, sistema de arranque dual, acceso al menú de la Bios y mucho más. Y os adelantamos que en cuestión de sistemas anti-cheats, **Valve afirma que las mejoras de Proton para Steam Deck,** como la compatibilidad de los sistemas anti-trampas **también estarán disponibles en las versiones de Proton para el escritorio**. Puedes ver las "preguntas frecuentes" [**en este enlace**](https://www.steamdeck.com/es/faq).


Para terminar, recomendaros si estáis interesados en profundizar en todo lo relacionado con ciertos aspectos del **presente y del futuro** de la Steam Deck, recomendaros la entrevista de Boiling Steam a James Ramey de Codeweavers en su podcast 15 titulado "[Steam Deck, Proton, en 2021 y más allá](https://boilingsteam.com/podcast-15-with-james-ramey-from-codeweavers-steam-deck-proton-2021-and-beyond/)" con mucha información interesante sobre el desarrollo de proton, su posible implementación con los sistemas antitrampas y donde se deja entrever que Valve puede estar ya pensando en una futura Steam Deck capaz de ejecutar juegos a 4k. 


Por lo que estamos viendo, **estamos ante una máquina que apunta a un lanzamiento muy potente**, con un muy buen desempeño y en la que Valve parece estar poniendo toda la carne en el asador. Esperemos que esto sea el revulsivo que el juego en Linux necesita para conseguir ser un sistema de primer nivel para los videojuegos.


Os dejamos además el último vídeo promocional de Steam Deck:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/AlWgZhMtlWo" title="YouTube video player" width="560"></iframe></div>


 

