---
author: Pato
category: Software
date: 2022-03-31 21:47:03
excerpt: "<p>La lista no para de crecer aunque a\xFAn queda mucho hasta llegar siquiera\
  \ al n\xFAmero de los que tienen soporte nativo en Linux</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/overview-shared-library-spanish.webp
joomla_id: 1457
joomla_url: los-juegos-verificados-para-steam-deck-ya-supera-el-millar-y-valve-implementa-el-reporte-de-los-usuarios
layout: post
tags:
- steamos
- steam-deck
title: Los juegos verificados para Steam Deck ya superan el millar, y Valve implementa
  el reporte de los usuarios
---
La lista no para de crecer aunque aún queda mucho hasta llegar siquiera al número de los que tienen soporte nativo en Linux


Valve sigue con su programa de verificación con el objetivo final de testear toda la vasta biblioteca de juegos de Steam para la Steam Deck. Si bien el ritmo parece haber descendido, creemos que en parte por los problemas que se han dado con algunos "falsos positivos" u otros títulos que aún con buen soporte no han pasado el corte de las pruebas, es cierto que la lista no ha parado de crecer.


Respecto a esto, esta semana Valve implementó un sistema para que los propios usuarios puedan reportar los juegos de su biblioteca de cara a tenerlo en cuenta en la verificación de los títulos que ya han sido verificados o que aún faltan por verificar.



>
> *Hemos añadido una nueva característica a Steam Deck para hacer preguntas directas sobre las experiencias de los jugadores y poder conocer así tu opinión. Esta característica es optativa: te preguntaremos una vez si estás dispuesto a responder a las preguntas, pero puedes dejar de participar en cualquier momento.*
>
>
>


Todo comienza con la petición por parte del sistema para que el usuario de su consentimiento para recabar la información necesaria:


![permission](https://cdn.akamai.steamstatic.com/steamcommunity/public/images/clans/40893422/489855182931364e5037c4406a99e53829055348.png)


Acto seguido, en ciertos títulos veremos una opción para dejar nuestra valoración acerca de como funciona el juego en cuestión bajo SteamOS en la Deck:


![permission2](https://cdn.akamai.steamstatic.com/steamcommunity/public/images/clans/40893422/7f6c04bdd2eee0a16274a20fc664e9f83a7284c0.png)

Puedes ver el anuncio oficial de esta característica en el post oficial en Steam [en este enlace](https://store.steampowered.com/news/app/1675200/view/3131696829170735771).


En otro orden de cosas, **la lista de juegos verificados para Steam Deck ya ha superado el millar**. Si quieres ver la lista de todos los que están disponibles puedes visitar [este enlace desde el propio Steam](https://store.steampowered.com/search/?sort_by=Released_DESC&deck_compatibility=3).

