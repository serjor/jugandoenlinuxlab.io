---
author: Pato
category: Hardware
date: 2019-06-12 14:16:27
excerpt: "<p>La consola de <span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x\
  \ r-bcqeeo r-qvutc0\" dir=\"auto\">@TheAtari_VCS</span> con coraz\xF3n Linux de\
  \ momento solo se puede comprar en EEUU</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/91c14cbfa735c2522cedce0eda8810b9.webp
joomla_id: 1063
joomla_url: atari-abre-su-campana-de-reservas-en-su-web-y-en-diversas-tiendas-fisicas
layout: post
tags:
- atari
- atarivcs
title: "Atari VCS abre su campa\xF1a de reservas en su web y en diversas tiendas f\xED\
  sicas"
---
La consola de @TheAtari_VCS con corazón Linux de momento solo se puede comprar en EEUU

Parece que Atari, la mítica firma de ocio digital está mas cerca de lanzar su nueva consola, la **Atari VCS** después de más de 25 años de su último lanzamiento de hardware doméstico. Ayer fue anunciada su puesta a la venta en fase de reserva solo para Estados Unidos en un artículo de [cnet.com](https://www.cnet.com/news/atari-vcs-opens-pre-orders-at-249-for-retro-console-featuring-4k-gaming/) en el que se anunció la disponibilidad de dos modelos base, la **Atari VCS 400 Onyx con un precio de 249$** y la **Atari VCS 800 a un precio de 389$**. La diferencia respecto a ambos modelos responde tan solo a la diferencia de la memoria RAM que llevará cada modelo, siendo de 4GB el primero y 8GB el segundo, siendo por lo demás máquinas idénticas en cuando a su configuración y especificaciones.


Aparte de esto, también está disponible para reserva **un paquete que incluye la consola Atari VCS 800, su característico joystick y el mando convencional por 389$**, aunque estos mandos se pueden conseguir por separado a un precio de 49$ el joystick y 59$ el gamepad.


Recordar que esta Atari VCS quiere ser una máquina que ejecute juegos retro y también juegos actuales gracias a la [actualización de hardware](index.php/homepage/hardware/15-hardware/1144-atari-desvela-el-procesador-de-su-proxima-atari-vcs) que sus responsables llevaron a cabo para que la consola estuviese a la altura de los tiempos que corren y ademas poder reproducir contenido multimedia a través de plataformas vía streaming a resoluciones 4K.


Como ya hemos comentado en otras ocasiones la Atari VCS viene con una filosofía "abierta" en la que **el sistema operativo será un Linux modificado por Atari**, y que mediante el bootloader **será capaz de ejecutar otros sistemas** (Linux o no) y juegos dentro de un entorno "sandbox" a través de medios externos.


![kevlar gold on](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/kevlar-gold-on.webp)


Como hemos dicho, la campaña de reservas ya está abierta para Estados Unidos desde su [página web oficial](https://atarivcs.com/), en los establecimientos de [Walmart](https://uberstrategist.link/AtariVCS-Walmart) donde ofrecen una versión exclusiva estilo "Kevlar Gold" o en [Gamestop](https://www.gamestop.com/consoles/atari-vcs-800-onyx-all-in-bundle-only-at-gamestop/) donde se puede reservar la versión negra "Onyx", **con estimación de entrega para Marzo de 2020** y con fechas aún por concretar para la reserva de la Atari VCS en otros países.


Si quieres saber más, puedes visitar su [web oficial](https://atarivcs.com/) o [nuestros artículos](index.php/buscar?searchword=atari%20vcs&ordering=newest&searchphrase=all&limit=20) donde hemos cubierto todo el desarrollo de esta consola con corazón Linux.


Ahora solo nos falta que vayan desvelando los títulos que traerá la máquina y los disponibles para su compra de cara al lanzamiento oficial, cosa que ya han anunciado que desvelarán en próximas fechas de este mismo año 2019. Por cierto, como curiosidad todos los que acudieron a la campaña de financiación de la Atari VCS en indiegogo recibirán el modelo 800 de la consola.


Estaremos atentos a las novedades que surjan al respecto.

