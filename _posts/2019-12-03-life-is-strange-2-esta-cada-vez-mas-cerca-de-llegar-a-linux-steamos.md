---
author: Pato
category: Aventuras
date: 2019-12-03 21:29:14
excerpt: "<p>El port a cargo de @feralgames ya tiene fecha de salida</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/LifeIsStrange/lifeisstrange2Logo2.webp
joomla_id: 1135
joomla_url: life-is-strange-2-esta-cada-vez-mas-cerca-de-llegar-a-linux-steamos
layout: post
tags:
- aventura
- feral-interactive
- life-is-strange-2
title: "Life is Strange 2 est\xE1 cada vez mas cerca de llegar a Linux/SteamOS (ACTUALIZADO)."
---
El port a cargo de @feralgames ya tiene fecha de salida


**ACTUALIZADO 17-12-19:**


Acabamos de recibir un correo electrónico anunciando la fecha definitiva de la salida de Life Is Strange 2... y será dentro de dos días, concretamente el **jueves, 19 de Diciembre**, momento desde el cual podamos disfrutar de este juego. Desarrollado originalmente por [DONTNOD Entertainment](http://dont-nod.com/) y publicado por Square Enix en Windows y la consola, esta es la tan esperada secuela del Premio BAFTA 2015, su primera parte. El anuncio también se realizaba a través de twitter:



> 
> Life is Strange 2 journeys onto macOS and Linux on 19th December. ?  
>   
> On the run and sharing a terrible secret, brothers Sean and Daniel can rely only on each other in a world turned against them. With the police on their trail, they must keep moving on the long road to Mexico… [pic.twitter.com/XR6NH1bJhj](https://t.co/XR6NH1bJhj)
> 
> 
> — Feral Interactive (@feralgames) [December 17, 2019](https://twitter.com/feralgames/status/1206922237753864197?ref_src=twsrc%5Etfw)


  





Os dejamos con el trailer que acompaña este anuncio:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/Fshknn1o49M" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**NOTICIA ORIGINAL:**


Desde hace ya [más de un año]({{ "/posts/life-is-strange-2-tendra-version-nativa-para-gnu-linux" | absolute_url }}) que sabemos que Feral está portando el último juego de la franquicia Life is Strange 2, y hoy con el lanzamiento del  episodio 5 del juego llamado "Wolves" (lobos) para otras plataformas, Feral nos anuncia la próxima llegada del juego junto a todos los episodios:



> 
> Life is Strange 2 for macOS and Linux is nearing the end of its journey. We will have some more info for you shortly.  
>   
> This is the trip that could bond Sean and Daniel forever… or tear their brotherhood apart. <https://t.co/UlMjfhLQDS>
> 
> 
> — Feral Interactive (@feralgames) [December 3, 2019](https://twitter.com/feralgames/status/1201910459814862848?ref_src=twsrc%5Etfw)



 Según nos anuncian, tendremos noticias del port próximamente. No es extraño que Feral nos haga esperar para recibir los juegos, sobre todo si son episódicos como es el caso que nos ocupa, ya que así pueden portar todo el juego de golpe y ahorran recursos.


Life is Strange 2 es un juego por episodios continuación del Life is Strange de Dontnod Entertainment, donde tendremos que adentrarnos en la dura historia de Sean y Daniel, dos hermanos con los que tendremos que tomar decisiones difíciles:



> 
> *Tras un trágico incidente, los hermanos Sean y Daniel Diaz se dan a la fuga por temor a la policía. Por si esto fuera poco, resulta que Daniel ahora puede mover objetos con la mente, así que los dos hermanos deciden poner rumbo a México. En la ciudad natal de su padre, Puerto Lobos, deberían estar a salvo.*
> 
> 
> 


 


 <div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/QODmEX26M2I" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Si quieres saber más de Life is Strange 2 y su profunda historia, puedes visitar su [página web oficial](https://lifeisstrange.square-enix-games.com/en-us), o su [pagina de Steam](https://store.steampowered.com/app/532210/Life_is_Strange_2/) donde estará disponible una vez que Feral termine el port.


Y tú, ¿tienes ganas de jugar a este Life Is Strange 2? Cuéntanoslos en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

