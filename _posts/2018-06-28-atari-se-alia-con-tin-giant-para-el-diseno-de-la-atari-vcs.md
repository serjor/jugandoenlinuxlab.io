---
author: Pato
category: Hardware
date: 2018-06-28 09:52:31
excerpt: "<p>El CEO de Tin Giant es Rob Wyatt, arquitecto de Xbox<span class=\"\"\
  >\xAE</span><span id=\"result_box\" class=\"\" lang=\"es\"></span> o Ps3 entre otras</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6b2d733e62f961aa8f8d2da1c77df197.webp
joomla_id: 788
joomla_url: atari-se-alia-con-tin-giant-para-el-diseno-de-la-atari-vcs
layout: post
tags:
- atari
- atarivcs
- tin-giant
title: "Atari se al\xEDa con Tin Giant para el dise\xF1o de la Atari VCS"
---
El CEO de Tin Giant es Rob Wyatt, arquitecto de Xbox® o Ps3 entre otras

Nueva actualización por parte de Atari en su campaña de financiación de Atari VCS, la próxima consola con sistema Linux que aún está en fase de desarrollo. En este nuevo comunicado, **la compañía ha anunciado la asociación con "Tin Giant"** para el diseño de la consola en un intento de acallar ciertos rumores que aseguraban recientemente que la campaña de financiación de Atari era poco menos que vaporware.


*"Hemos tenido muchas preguntas sobre desarrolladores de software, arquitectos de sistemas y diseñadores trabajando con nosotros para traer a la vida a la Atari VCS. Hemos traido lo mejor de lo mejor para trabajar junto a nuestro equipo en Atari para crear nuestro nuevo sistema de hardware. Estamos entusiasmados finalmente de retirar el telón y presentar a un miembro del equipo que ha sido un colaborador "tras las cortinas" hasta ahora.*


*Rob Wyatt, el **arquitecto de sistemas de la consola Xbox® original y diseñador de arquitectura de gráficos para Playstation3®** y otros sistemas, ha estado consultando con el equipo de Atari VCS durante muchos meses y hoy podemos darle oficialmente la bienvenida a Rob y su compañía Tin Giant al equipo.*


*Tin Giant se ha comprometido con Atari, además de AMD y el **ingeniero principal de hardware Joe Moak**, desde el año pasado para ayudar a definir los requisitos del producto Atari VCS,  los casos de uso, hardware, sistemas operativos y más. Para los seguidores y backers de Atari, significa que el sistema operativo VCS y la arquitectura final del sistema se encuentran en algunas de las manos más capaces y expertas del planeta."*  



Puedes ver el anuncio en la [última actualización de la campaña](https://www.indiegogo.com/projects/atari-vcs-game-stream-connect-like-never-before-family-computers/x/18828265#/updates/all) de la Atari VCS en indiegogo. Por cierto, a falta de 2 días de campaña ya ha logrado superar los 11.000 apoyos y cerca de 3.000.000$


<div class="resp-iframe"><iframe height="445px" src="https://www.indiegogo.com/project/atari-vcs-game-stream-connect-like-never-before-family-computers/embedded/18828265" style="display: block; margin-left: auto; margin-right: auto;" width="222px"></iframe></div>


 


¿Qué te parece el anuncio de la alianza de Atari con Tiny Giant? ¿Te parece que el sistema estará en buenas manos? y lo más importante... ¿Te interesa seguir recibiendo noticias de la Atari VCS, un sistema con base Linux aquí, en jugandoenlinux.com?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

