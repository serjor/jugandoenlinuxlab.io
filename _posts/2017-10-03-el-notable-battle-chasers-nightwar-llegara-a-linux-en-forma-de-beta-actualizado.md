---
author: Pato
category: Rol
date: 2017-10-03 20:50:39
excerpt: <p>El juego de <span class="username u-dir" dir="ltr">@<b class="u-linkComplex-target">AirshipSyn</b></span>
  finalmente ha llegado de forma oficial a GNU-Linux/SteamOS</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/cdfa54053805c4a7f07a2686eb2f2fc9.webp
joomla_id: 473
joomla_url: el-notable-battle-chasers-nightwar-llegara-a-linux-en-forma-de-beta-actualizado
layout: post
tags:
- indie
- rol
- jrpg
- turnos
title: "El notable 'Battle Chasers: Nightwar' probablemente llegar\xE1 a Linux en\
  \ forma de Beta (ACTUALIZADO)"
---
El juego de @**AirshipSyn** finalmente ha llegado de forma oficial a GNU-Linux/SteamOS

**ACTUALIZACIÓN 28-5-18**: @THQNordic acaba de anunciar en twitter que el esperado Battle Chasers finalmente [ha desembarcado oficialmente en Linux](https://steamcommunity.com/games/451020/announcements/detail/1666775029237548226). Podeis ver el anuncio justo aquí:



> 
> JRPG-style dungeon crawler Battle Chasers: Nightwar is Out Now for Linux! Link: <https://t.co/VXvLromfdW> The game is already out on PC and consoles - watch the Switch Release Trailer! <https://t.co/r8VfKIqCVG> [@AirshipSyn](https://twitter.com/AirshipSyn?ref_src=twsrc%5Etfw) [@JoeMadx](https://twitter.com/JoeMadx?ref_src=twsrc%5Etfw) [#BattleChasers](https://twitter.com/hashtag/BattleChasers?src=hash&ref_src=twsrc%5Etfw) [#JRPG](https://twitter.com/hashtag/JRPG?src=hash&ref_src=twsrc%5Etfw)
> 
> 
> — THQNordic (@THQNordic) [May 28, 2018](https://twitter.com/THQNordic/status/1001038208854093825?ref_src=twsrc%5Etfw)



 Podeis comprar el juego en Steam (en GOG.com aun no está disponible en nuestro sistema):


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/451020/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 




---


**NOTICIA ORIGINAL**: De nuevo traemos noticias agridulces con 'Battle Chasers: Nightwar'. El juego de rol de Airship Syndicate y THQ Nordic ha estado planificado para su lanzamiento en Linux, pero al igual que casos como [Ruiner](index.php/homepage/generos/accion/item/594-ruiner-llegara-a-linux-tras-su-lanzamiento-en-consolas) el icono de SteamOS desapareció hace unos días. Sin embargo, los desarrolladores [han abierto un hilo oficial](http://steamcommunity.com/app/451020/discussions/0/1520386297704229504/) en los foros del juego en Steam para aclarar la situación y de paso pedir la opinion de los usuarios de Linux:



> 
> *Hola a todos! Aunque el tag de Linux haya desaparecido, está aún en nuestros planes y está en desarrollo.*
> 
> 
> *Pedimos disculpas por la confusión, pero estamos dedicados a tenerlo hecho. Perdón por el desorden, y gracias por quedaros con nosotros!*
> 
> 
> *Tenemos curiosidad por una encuesta informal- Los usuarios de Linux prefieren una suerte de "Beta" para ayudarnos a testearlo, o preferiríais que esperásemos hasta que esté bien pulido para lanzarlo? Gracias por las respuestas.*
> 
> 
> 


 Ya sabéis, si estáis interesados en alguna de las opciones podéis pasaros por el hilo y dar vuestra opinión.


Battle Chasers: Nightwar [[web oficial](http://www.battlechasers.com/)] es un RPG inspirado en los mejores juegos del género. Adéntrate en las mazmorras, lucha por turnos con el formato clásico de los JRPG, y sumérgete en una historia que progresará a medida que explores el mundo del juego.


* Combate por turnos con un sistema exclusivo de sobrecarga de maná e increíbles estallidos de batalla.
* Explora un mundo repleto de mazmorras, jefes, y amigos y enemigos que aparecerán al azar.
* Mazmorras aleatorias llenas de trampas, rompecabezas y secretos. Usa las habilidades exclusivas de cada héroe para sobrevivir.
* Crea tu equipo con tres de los seis héroes disponibles de los famosos cómics Battle Chasers, cada uno con sus propias características, ventajas, objetos y habilidades.
* Descubre el complejo sistema de creación y usa el exclusivo sistema de sobrecarga de ingredientes para crear objetos épicos.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/khtSM7XRga8" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Battle Chasers: Nightwar estará disponible próximamente en su [página de Steam](http://store.steampowered.com/app/451020/Battle_Chasers_Nightwar/) completamente traducido al español.


¿Que te parece la propuesta de Battle Chasers: Nightwar? ¿Te gustan este tipo de juegos de Rol?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de Telegram o Discord.


 

