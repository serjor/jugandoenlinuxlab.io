---
author: leillo1975
category: Software
date: 2018-05-02 19:53:35
excerpt: "<p>Primera versi\xF3n estable de esta serie.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/24c01e452493eba0f9e741ef09a2d61a.webp
joomla_id: 724
joomla_url: nuevo-driver-de-nvidia-396-24
layout: post
tags:
- nvidia
- driver
- propietario
- '396'
title: 'Nuevo driver de Nvidia: 396.24.'
---
Primera versión estable de esta serie.

Gracias a [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=NVIDIA-396.24-Linux-Driver) nos enteramos que Nvidia acaba de lanzar una versión estable de su serie más nueva, la 396.24. Hasta ahora los drivers 396 estaban en fase beta, aunque ya se estaban utilizando mayoritariamente, principalmente por los jugadores de [Rise of the Tomb Raider](index.php/homepage/generos/accion/item/835-rise-of-tomb-raider-20-aniversario-disponible-en-gnu-linux-steamos) (ya que estaba como requisito para jugarlo, aunque funciona con los 390), y usuarios del complemento para Wine [DXVK](https://github.com/doitsujin/dxvk), ya que arreglaba bastantes problemas, sobre todo con la "emulación" de GTA V. Este driver incluye entre otras, [novedades](https://devtalk.nvidia.com/default/topic/1032986/b/t/post/5255724/#5255724) de calado, como son:



> 
> - Soporte para nuevas GPU's,  entre las que se encuentra la GTX-1050 con diseño Max-Q (nuevos chips para portátiles)
> 
> 
> - Coreregidas regresiones relacionadas con Metamodos de ViewPortIn y ViewPortOut en modos de RandR.
> 
> 
> - Mejorado el reporte de errores en el controlador Vulkan si libnvidia-glvkspirv.so, el nuevo compilador Vulkan SPIR-V añadido en 396.18, no puede ser encontrado en tiempo de ejecución.
> 
> 
> - Corregidas las regresiones de rendimiento y los problemas de corrupción en el nuevo compilador Vulkan SPIR-V.
> 
> 
> - Añadido soporte para X.Org xserver ABI 24 (xorg-server 1.20).
> 
> 
> 


 


Si quereis actualizar vuestros drivers podeis descargarlo en su [página web](https://devtalk.nvidia.com/default/topic/1032986/b/t/post/5255724/#5255724), o esperar un poco y hacerlo desde el [repositorio oficial de drivers de Nviida](index.php/foro/drivers-graficas-nvidia/19-instalar-el-ultimo-driver-propietario-de-nvidia-en-ubuntu-y-derivadas) (para usuarios de Ubuntu y derivadas).

