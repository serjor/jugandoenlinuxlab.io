---
author: Pato
category: "Acci\xF3n"
date: 2017-11-10 08:14:31
excerpt: <p>Croteam cumple su palabra y nos trae esta experiencia para disfrutar en
  Linux con las HTC Vive</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e4695529e9bbf8b60b6165d76397876b.webp
joomla_id: 527
joomla_url: desempolva-tus-gafas-vr-serious-sam-3-vr-bfe-llega-a-linux-steamos
layout: post
tags:
- accion
- steamos
- steamvr
title: 'Desempolva tus gafas VR: ''Serious Sam 3 VR: BFE'' llega a Linux/SteamOS'
---
Croteam cumple su palabra y nos trae esta experiencia para disfrutar en Linux con las HTC Vive

¡Croteam suma y sigue! tras habernos traído a Linux/SteamOS un [amplio catálogo](https://www.jugandoenlinux.com/index.php/component/search/?searchword=serious%20sam&searchphrase=all&Itemid=101) de la saga Serious Sam, y tras traernos '[Serious Sam VR The second encounter](https://www.jugandoenlinux.com/index.php/homepage/generos/accion/item/401-croteam-lanza-serious-sam-vr-the-second-encounter)' también para disfrutar con las gafas de realidad virtual, ahora le llega el turno a este 'Serious Sam 3 VR: BFE' donde "Sam el Serio" tendrá que vérselas con hordas y hordas de monstruos.


*"Serious Sam 3 VR: BFE es el glorioso regreso a la edad de oro de los shooters en primera persona donde los hombres eran hombres, la cobertura era para novatos y pulsar el gatillo hacía que las cosas hicieran bum. Ambientado en los desmoronados templos de una civilización antigua y las derrumbadas ciudades del Egipto del siglo XXII, Serious Sam 3 es una emocionante fusión de los frenéticos shooters con la jugabilidad de los juegos modernos."*


**Características:**


* Frenética acción arcade – Mantén pulsado el gatillo y acaba con las hordas de enemigos o afronta ser derrotado por las bestias salvajes de Mental. Sin sistemas de cobertura, sin escondite - eres solo tú contra ellos. Todos ellos.
* Temibles criaturas enemigas – Un nuevo batallón de inolvidables secuaces, incluyendo al ruidoso Srcapjack y el imponente Khnum, que se unen al Kamikaze decapitado, Gnaar, y al Hombre Toro Sirio para crear la más fiera oposición que nunca hayas tenido el placer de aniquilar.
* Espectaculares entornos – Lucha a través de los extensos campos de batalla del Egipto de un futuro próximo en un caos total. Las desmoronadas ciudades del mañana junto a las ruinas de los templos del mundo antiguo se convierten en tu patio de recreo destruible.
* Arsenal destructivo – Desata todo el arsenal de armas de Serious Sam, incluyendo un rifle de asalto con mira, la escopeta recortada de doble cañón, la explosiva escopeta automática, la devastadora minigun ¡y la todopoderosa descarga de balas de cañón en llamas! Lleva todas las armas de Sam a la vez y cambia de una a otra al vuelo para obtener toda la potencia de fuego que vas a necesitar.
* Caos cooperativo – Puedes ir a la guerra contra las hordas de Mental con hasta 16 jugadores online, ¡aniquila todo lo que se mueva a lo largo de 12 niveles de auténtica locura¡ ¡Intenta sobrevivir a una oleada tras otra en el frenético modo Survival o disfruta de un monstruoso safari en el modo Beast Hunt!
* Pura locura multijugador – Arroja el guante y disfruta de los increíbles modos versus como Deathmatch, Capture the Flag, Last Team Standing y My Burden. ¡El multijugador de Serious Sam elevado a la máxima potencia hará temblar hasta los cimientos del infierno!


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/0d0qf9ss2cE" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 En cuanto a los requisitos, son:


**MÍNIMO:**  
SO: Ubuntu 14.04  
Procesador: Intel Core i5 - 4590 o equivalente  
Memoria: 6 GB de RAM  
Gráficos: AMD R9 290 o NVIDIA GTX 970  
Almacenamiento: 10 GB de espacio disponible  
Notas adicionales: HTC Vive headset con controladores manuales. Conexión a internet requerida para activar el producto. Después una conexión permanente no es necesaria para poder jugar.


**RECOMENDADO:**  
SO: Ubuntu 14.04  
Procesador: Intel Core i7-6800 o equivalente  
Memoria: 8 GB de RAM  
Gráficos: AMD Fury o NVIDIA GTX 1070  
Almacenamiento: 10 GB de espacio disponible  
Notas adicionales: HTC Vive headset con controladores manuales. Conexión a internet requerida para activar el producto. Después una conexión permanente no es necesaria para poder jugar.


SIN COBERTURA. ¡VUELVE EL HOMBRE!


Si tienes tus gafas de VR y quieres disfrutar de este 'Serious Sam 3 VR: BFE' traducido al español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/567670/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Tienes unas gafas VR en Linux? ¿Piensas probarlo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

