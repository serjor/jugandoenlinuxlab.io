---
author: leillo1975
category: Rol
date: 2020-04-03 19:01:33
excerpt: "<p><span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\"\
  >@cloudsefi</span> completa la traducci\xF3n en el Workshop de Steam.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AtomRpg/ATOM_RPG_traduc.webp
joomla_id: 1201
joomla_url: ya-se-puede-empezar-a-jugar-a-atom-rpg-en-espanol
layout: post
tags:
- rol
- rpg
- atom-rpg
- postapocaliptico
- atom-team
- fallout
- wasteland
title: "Ya se puede jugar a ATOM RPG en espa\xF1ol (ACTUALIZADO)"
---
@cloudsefi completa la traducción en el Workshop de Steam.


**ACTUALIZACIÓN 8-5-20**: Ha pasado poco más de un mes desde que os informábamos de el comienzo del [proceso de traducción](https://steamcommunity.com/app/552620/discussions/1/1751277046394142303/) de este fantástico juego, y en ese tiempo, su traductor, [Sefirot](https://twitter.com/cloudsefi), no ha estado de brazos cruzados y linea a linea ha completado este proyecto que podeis encontrar en el [Workshop de ATOM RPG](https://steamcommunity.com/sharedfiles/filedetails/?id=2045985489), y que a partir de hoy podeis disfrutar en su totalidad del contenido del juego en nuestro idioma.


Personalmente, desde que tuve conocimiento de esta traducción he ido viendo el **progreso semana a semana**, con actualizaciones y reportes constantes, que incluso permitían jugar partes del juego completamente en español. Finalmente el fruto de este trabajo está aquí, y si antes ya era recomendable este título, gracias a esto, ahora lo es aun mucho más. Los fans del Rol postapocalíptico hispanohablantes están de enhorabuena. 


 




---


**NOTICIA ORIGINAL**:


Seguramente si sois seguidores de la web desde hace tiempo, conocereis a este juego postapocalíptico ambientado en lo que era antes la Unión Sovietica. Durante el periodo de Acceso Anticipado fuimos publicando los [reportes de su desarrollo en nuestras noticias](index.php/component/k2/7-rol/677-atom-rpg-post-apocalyptic-indie-game-llega-en-early-access), así como alguna que otra [actualización importante](index.php/component/k2/7-rol/1235-atom-rpg-se-actualiza-incluyendo-la-perspectiva-isometrica), y es que el juego realmente lo merece, pues hay un **trabajo excepcional detrás de él** que lo hace automaticamente un **referente para todos los aficionados del género**, así como todos los fans de las sagas **Fallout** o **Wasteland**, juegos en los que claramente se inspira.


El caso es que **los usarios que hablamos español estábamos en clara desventaja**, pues aunque consiguieramos entender los dialogos, el proceso de tener que estar traduciendo mentalmente mientras jugábamos podría ser ciertamente agotador, lo cual provoca que el tiempo de juego se multiplique y finalmente acabes abandonándolo una temporada al no avanzar al ritmo del juego.


Después de mucha insistencia a los desarrolladores, viendo que estos no estaban muy por la labor, y que muchos de los intentos por parte de algunos usuarios finalmente no acababan en nada, finalmente un usuario [@cloudsefi](https://twitter.com/cloudsefi) (**Sefirot**, me suena ese nick una barbaridad de hace tiempo) se lo tomó en serio y [se puso manos a la obra](https://www.elotrolado.net/hilo_ho-atom-rpg-estrategia-por-turnos-similar-a-fallout-1-y-2-traduccion-al-espanol-en-desarrollo_2356938), alcanzando hasta el momento lo [siguiente](https://steamcommunity.com/app/552620/discussions/1/1751277046394142303):


***Interfaz al 100% que consiste en:***  
*- Objetos y su descripción*  
*- Misiones (hay un apartado al lado del mapa donde sale una breve descripcion)*  
*- Localizaciones*  
*- Habilidades*  
*- Subtítulos cinemáticas*  
*-Texto flotante al combatir*   
  
***Diálogos al 25% aprox.:***  
*(Zonas completadas Tutorial,Otradnoye,la fábrica,bunker 317,dos puestos de guardia cercanos,la guarida del borracho,granja doncella de primavera,bosque rugiente,encuentro y conversación con los dos primeros compañeros.)*


Desde ya podeis [suscribiros en el Workshop del juego en Steam](https://steamcommunity.com/sharedfiles/filedetails/?id=2045985489) y **aplicarlo como mod en el juego**, de forma que en el siguiente reinicio de juego tendreis la **opción de poner el juego en Español**. Yo os puedo asegurar que he empezado una partida de nuevo y he jugado como un par de horas sin encontrar ningún texto en Inglés, por lo que si quereis comenzar la aventura, este mod os lo va a poner más fácil. Yo os dejo que voy a matar unas cuantas arañas mutantes en el Yermo. Podeis comprar ATOM RPG en la [tienda de Steam](https://store.steampowered.com/app/552620/ATOM_RPG_Postapocalyptic_indie_game/).


¿Conocías este juego de de [AtomTeam](https://atomrpg.com/)? ¿Te gustan los juegos de ambientación postapocaliptica? Podeis darnos vuestra opinión sobre ATOM RPG en los comentarios de este artículo, en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

