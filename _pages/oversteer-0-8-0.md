---
layout: page
title: La utilidad de configuración de volantes Oversteer se actualiza a la versión 0.8.0
permalink: index.php/homepage/software/1547-la-utilidad-de-configuracion-de-volantes-oversteer-se-actualiza-a-la-version-0-8-0
---

<script>
	window.location = '{% link _posts/2023-10-09-la-utilidad-de-configuracion-de-volantes-oversteer-se-actualiza-a-la-version-0-8-0.md %}'
</script>
<noscript>
	Este articulo se movió a {% link _posts/2023-10-09-la-utilidad-de-configuracion-de-volantes-oversteer-se-actualiza-a-la-version-0-8-0.md %}
</noscript>