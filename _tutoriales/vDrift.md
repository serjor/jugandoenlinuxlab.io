---
title: vdrift
description: Compilar e instalar VDrift en Ubuntu
layout: page
---
He de reconocer que no conocía Vdrift ( <https://vdrift.net/> ) a pesar de ser de coches y libre, y sinceramente me está sorprendiendo muy gratamente. Pero hay un problema, no hay una versión en los repositorios. Obviamente, por desgracia tampoco podemos acceder a un flatpak, Snap o AppImage de el, algo que sería totalmente aconsejable en este juego por su sencillez y al tener unas dependencias que no están muy actualizadas.

Os voy a describir los pasos que he seguido para compilarlo, tras pelearme mucho con él, y desde aquí me gustaría darle las gracias a @SonLink y a @gnuxero26 por su ayuda.

-En primer lugar la última versión estable me arroja errores al compilarla, además de estar completamente desfasada, por lo que en este caso usaremos la última versión del código que encontraremos en la página del proyecto en Github:

<https://github.com/VDrift/vdrift>

-Al descomprimir el archivo vdrift-master.zip se nos creará una carpeta vdrft-master que contendrá el código dentro. Este código solo es el motor que mueve el juego, Las pistas, coches y demás datos importantes del juego se descargan de la página de Sourceforge de Vdrift, concretamente aquí:

<http://svn.code.sf.net/p/vdrift/code/vdrift-data/>

Para descargarnos los coches, pistas y demás datos lo más sencillo es usar SVN (sudo apt install subversion) usando la terminal:

`svn checkout https://svn.code.sf.net/p/vdrift/code/vdrift-data/ [/RUTA/DE/DESCARGA]`

-Una vez termine la descarga, que le llevará bastante tiempo, renombraremos la carpeta "vdrift-data" a "data" y la meteremos en la carpeta "vdrift-master" que anteriormente descomprimimos.

-Después tendremos que instalar las dependencias necesarias con el siguiente comando en una terminal:

`sudo apt-get install g++ scons libsdl2-dev libsdl2-image-dev libbullet-dev libvorbis-dev libcurl4-gnutls-dev`

-Una vez instaladas las dependencias abriremos la carpeta  "vdrift-master" y  abriremos una terminal en ella (botón derecho del ratón el la carpeta y seleccionar abrir una terminal). Una vez dentro ejecutaremos el siguiente comando para que comience a compilar:

`scons arch=a64 release=1 extbullet=1 prefix=[/RUTA/DE/INSTALACION]`


-Una vez termine de compilar, tendremos que instalarlo en [/RUTA/DE/INSTALACION]:

`scons install prefix=[/RUTA/DE/INSTALACION]`

(Por ejemplo si lo instalamos en nuestra carpeta personal sería "/home/[usuario]/VDRIFT")

Con esto ya estaría instalado el juego y tan solo tendríamos que escribir "vdrift" en la terminal para comenzar a jugar. En caso de que no os arrancase iríais a la carpeta de instalación de VDrift y encontraríais dos carpetas "bin" y "share". Entraríais en "bin"  y dentro tendríais el ejecutable "vdrift" (en una terminal "./vdrift")

Espero que os sea útil. 


**NOTAS AÑADIDAS DE BERNAT:**

Puede que te encuentres con un error como este al intentar compilar:

```sh
scons: Reading SConscript files ...
  File "/home/berarma/Descargas/vdrift/SConstruct", line 94

    print 'Building a universal binary require access to an ' + \

                                                            ^

SyntaxError: Missing parentheses in call to 'print'. Did you mean print('Building a universal binary require access to an ' + \)?
```

Es debido a que estás usando scons sobre python3. Si tienes instalado python2 puedes compilarlo sin instalar nada más ejecutando este comando en la terminal para crear un alias:

`alias scons2="/usr/bin/env python2 $(which scons)"`


Ahora vuelve a intentarlo sustituyendo el comando scons por scons2 en las instrucciones.

Si descargar los datos con SVN es muy lento, puedes descargar los datos del juego desde aquí: <www.mediafire.com/file/bhfin2d7r4yyr4h/vdrift-data.7z>

